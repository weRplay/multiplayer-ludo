﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>
struct DefaultComparer_t1959;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>::.ctor()
extern "C" void DefaultComparer__ctor_m12791_gshared (DefaultComparer_t1959 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m12791(__this, method) (( void (*) (DefaultComparer_t1959 *, const MethodInfo*))DefaultComparer__ctor_m12791_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UIVertex>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m12792_gshared (DefaultComparer_t1959 * __this, UIVertex_t191  ___x, UIVertex_t191  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m12792(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t1959 *, UIVertex_t191 , UIVertex_t191 , const MethodInfo*))DefaultComparer_Compare_m12792_gshared)(__this, ___x, ___y, method)
