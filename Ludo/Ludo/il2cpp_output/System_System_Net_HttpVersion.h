﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Version
struct Version_t945;

#include "mscorlib_System_Object.h"

// System.Net.HttpVersion
struct  HttpVersion_t944  : public Object_t
{
};
struct HttpVersion_t944_StaticFields{
	// System.Version System.Net.HttpVersion::Version10
	Version_t945 * ___Version10_0;
	// System.Version System.Net.HttpVersion::Version11
	Version_t945 * ___Version11_1;
};
