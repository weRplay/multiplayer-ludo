﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_4.h"

// System.Void System.Array/InternalEnumerator`1<System.Double>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m10606_gshared (InternalEnumerator_1_t1796 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m10606(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1796 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m10606_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Double>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m10607_gshared (InternalEnumerator_1_t1796 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m10607(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1796 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m10607_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Double>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m10608_gshared (InternalEnumerator_1_t1796 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m10608(__this, method) (( void (*) (InternalEnumerator_1_t1796 *, const MethodInfo*))InternalEnumerator_1_Dispose_m10608_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Double>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m10609_gshared (InternalEnumerator_1_t1796 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m10609(__this, method) (( bool (*) (InternalEnumerator_1_t1796 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m10609_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Double>::get_Current()
extern "C" double InternalEnumerator_1_get_Current_m10610_gshared (InternalEnumerator_1_t1796 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m10610(__this, method) (( double (*) (InternalEnumerator_1_t1796 *, const MethodInfo*))InternalEnumerator_1_get_Current_m10610_gshared)(__this, method)
