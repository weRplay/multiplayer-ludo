﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t5;
// Player
struct Player_t11;
// PlayerTwo
struct PlayerTwo_t12;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// CountdownTimer
struct  CountdownTimer_t10  : public MonoBehaviour_t2
{
	// UnityEngine.GameObject CountdownTimer::myTimeLeftText
	GameObject_t5 * ___myTimeLeftText_2;
	// UnityEngine.GameObject CountdownTimer::opponentsTimeLeftText
	GameObject_t5 * ___opponentsTimeLeftText_3;
	// System.Int32 CountdownTimer::CountdownNumber
	int32_t ___CountdownNumber_8;
};
struct CountdownTimer_t10_StaticFields{
	// System.Single CountdownTimer::timeLeft
	float ___timeLeft_4;
	// System.Boolean CountdownTimer::showTimer
	bool ___showTimer_5;
	// Player CountdownTimer::playerScript
	Player_t11 * ___playerScript_6;
	// PlayerTwo CountdownTimer::playerTwoScript
	PlayerTwo_t12 * ___playerTwoScript_7;
};
