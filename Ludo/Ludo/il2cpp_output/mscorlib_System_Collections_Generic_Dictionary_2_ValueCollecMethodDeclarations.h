﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_5MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m11923(__this, ___host, method) (( void (*) (Enumerator_t300 *, Dictionary_2_t116 *, const MethodInfo*))Enumerator__ctor_m11840_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m11924(__this, method) (( Object_t * (*) (Enumerator_t300 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11841_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::Dispose()
#define Enumerator_Dispose_m11925(__this, method) (( void (*) (Enumerator_t300 *, const MethodInfo*))Enumerator_Dispose_m11842_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::MoveNext()
#define Enumerator_MoveNext_m1537(__this, method) (( bool (*) (Enumerator_t300 *, const MethodInfo*))Enumerator_MoveNext_m11843_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Current()
#define Enumerator_get_Current_m1536(__this, method) (( PointerEventData_t108 * (*) (Enumerator_t300 *, const MethodInfo*))Enumerator_get_Current_m11844_gshared)(__this, method)
