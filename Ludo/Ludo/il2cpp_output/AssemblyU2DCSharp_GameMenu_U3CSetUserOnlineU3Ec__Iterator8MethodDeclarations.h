﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameMenu/<SetUserOnline>c__Iterator8
struct U3CSetUserOnlineU3Ec__Iterator8_t27;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void GameMenu/<SetUserOnline>c__Iterator8::.ctor()
extern "C" void U3CSetUserOnlineU3Ec__Iterator8__ctor_m90 (U3CSetUserOnlineU3Ec__Iterator8_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameMenu/<SetUserOnline>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CSetUserOnlineU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m91 (U3CSetUserOnlineU3Ec__Iterator8_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameMenu/<SetUserOnline>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CSetUserOnlineU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m92 (U3CSetUserOnlineU3Ec__Iterator8_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMenu/<SetUserOnline>c__Iterator8::MoveNext()
extern "C" bool U3CSetUserOnlineU3Ec__Iterator8_MoveNext_m93 (U3CSetUserOnlineU3Ec__Iterator8_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMenu/<SetUserOnline>c__Iterator8::Dispose()
extern "C" void U3CSetUserOnlineU3Ec__Iterator8_Dispose_m94 (U3CSetUserOnlineU3Ec__Iterator8_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMenu/<SetUserOnline>c__Iterator8::Reset()
extern "C" void U3CSetUserOnlineU3Ec__Iterator8_Reset_m95 (U3CSetUserOnlineU3Ec__Iterator8_t27 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
