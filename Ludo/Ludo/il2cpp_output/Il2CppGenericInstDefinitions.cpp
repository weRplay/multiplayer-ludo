﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern const Il2CppType Object_t_0_0_0;
static const Il2CppType* GenInst_Object_t_0_0_0_Types[] = { &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0 = { 1, GenInst_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0 = { 2, GenInst_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 4, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType Player_t11_0_0_0;
static const Il2CppType* GenInst_Player_t11_0_0_0_Types[] = { &Player_t11_0_0_0 };
extern const Il2CppGenericInst GenInst_Player_t11_0_0_0 = { 1, GenInst_Player_t11_0_0_0_Types };
extern const Il2CppType PlayerTwo_t12_0_0_0;
static const Il2CppType* GenInst_PlayerTwo_t12_0_0_0_Types[] = { &PlayerTwo_t12_0_0_0 };
extern const Il2CppGenericInst GenInst_PlayerTwo_t12_0_0_0 = { 1, GenInst_PlayerTwo_t12_0_0_0_Types };
extern const Il2CppType Text_t47_0_0_0;
static const Il2CppType* GenInst_Text_t47_0_0_0_Types[] = { &Text_t47_0_0_0 };
extern const Il2CppGenericInst GenInst_Text_t47_0_0_0 = { 1, GenInst_Text_t47_0_0_0_Types };
extern const Il2CppType GameObject_t5_0_0_0;
static const Il2CppType* GenInst_GameObject_t5_0_0_0_Types[] = { &GameObject_t5_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t5_0_0_0 = { 1, GenInst_GameObject_t5_0_0_0_Types };
extern const Il2CppType BoardManager_t1_0_0_0;
static const Il2CppType* GenInst_BoardManager_t1_0_0_0_Types[] = { &BoardManager_t1_0_0_0 };
extern const Il2CppGenericInst GenInst_BoardManager_t1_0_0_0 = { 1, GenInst_BoardManager_t1_0_0_0_Types };
extern const Il2CppType CountdownTimer_t10_0_0_0;
static const Il2CppType* GenInst_CountdownTimer_t10_0_0_0_Types[] = { &CountdownTimer_t10_0_0_0 };
extern const Il2CppGenericInst GenInst_CountdownTimer_t10_0_0_0 = { 1, GenInst_CountdownTimer_t10_0_0_0_Types };
extern const Il2CppType BoxCollider2D_t38_0_0_0;
static const Il2CppType* GenInst_BoxCollider2D_t38_0_0_0_Types[] = { &BoxCollider2D_t38_0_0_0 };
extern const Il2CppGenericInst GenInst_BoxCollider2D_t38_0_0_0 = { 1, GenInst_BoxCollider2D_t38_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t39_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t39_0_0_0_Types[] = { &Rigidbody2D_t39_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t39_0_0_0 = { 1, GenInst_Rigidbody2D_t39_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t50_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t50_0_0_0_Types[] = { &SpriteRenderer_t50_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t50_0_0_0 = { 1, GenInst_SpriteRenderer_t50_0_0_0_Types };
extern const Il2CppType BaseInputModule_t70_0_0_0;
static const Il2CppType* GenInst_BaseInputModule_t70_0_0_0_Types[] = { &BaseInputModule_t70_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInputModule_t70_0_0_0 = { 1, GenInst_BaseInputModule_t70_0_0_0_Types };
extern const Il2CppType RaycastResult_t103_0_0_0;
static const Il2CppType* GenInst_RaycastResult_t103_0_0_0_Types[] = { &RaycastResult_t103_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t103_0_0_0 = { 1, GenInst_RaycastResult_t103_0_0_0_Types };
extern const Il2CppType IDeselectHandler_t277_0_0_0;
static const Il2CppType* GenInst_IDeselectHandler_t277_0_0_0_Types[] = { &IDeselectHandler_t277_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t277_0_0_0 = { 1, GenInst_IDeselectHandler_t277_0_0_0_Types };
extern const Il2CppType ISelectHandler_t276_0_0_0;
static const Il2CppType* GenInst_ISelectHandler_t276_0_0_0_Types[] = { &ISelectHandler_t276_0_0_0 };
extern const Il2CppGenericInst GenInst_ISelectHandler_t276_0_0_0 = { 1, GenInst_ISelectHandler_t276_0_0_0_Types };
extern const Il2CppType BaseEventData_t71_0_0_0;
static const Il2CppType* GenInst_BaseEventData_t71_0_0_0_Types[] = { &BaseEventData_t71_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseEventData_t71_0_0_0 = { 1, GenInst_BaseEventData_t71_0_0_0_Types };
extern const Il2CppType Entry_t75_0_0_0;
static const Il2CppType* GenInst_Entry_t75_0_0_0_Types[] = { &Entry_t75_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t75_0_0_0 = { 1, GenInst_Entry_t75_0_0_0_Types };
extern const Il2CppType IPointerEnterHandler_t264_0_0_0;
static const Il2CppType* GenInst_IPointerEnterHandler_t264_0_0_0_Types[] = { &IPointerEnterHandler_t264_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t264_0_0_0 = { 1, GenInst_IPointerEnterHandler_t264_0_0_0_Types };
extern const Il2CppType IPointerExitHandler_t265_0_0_0;
static const Il2CppType* GenInst_IPointerExitHandler_t265_0_0_0_Types[] = { &IPointerExitHandler_t265_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t265_0_0_0 = { 1, GenInst_IPointerExitHandler_t265_0_0_0_Types };
extern const Il2CppType IPointerDownHandler_t266_0_0_0;
static const Il2CppType* GenInst_IPointerDownHandler_t266_0_0_0_Types[] = { &IPointerDownHandler_t266_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t266_0_0_0 = { 1, GenInst_IPointerDownHandler_t266_0_0_0_Types };
extern const Il2CppType IPointerUpHandler_t267_0_0_0;
static const Il2CppType* GenInst_IPointerUpHandler_t267_0_0_0_Types[] = { &IPointerUpHandler_t267_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t267_0_0_0 = { 1, GenInst_IPointerUpHandler_t267_0_0_0_Types };
extern const Il2CppType IPointerClickHandler_t268_0_0_0;
static const Il2CppType* GenInst_IPointerClickHandler_t268_0_0_0_Types[] = { &IPointerClickHandler_t268_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t268_0_0_0 = { 1, GenInst_IPointerClickHandler_t268_0_0_0_Types };
extern const Il2CppType IInitializePotentialDragHandler_t269_0_0_0;
static const Il2CppType* GenInst_IInitializePotentialDragHandler_t269_0_0_0_Types[] = { &IInitializePotentialDragHandler_t269_0_0_0 };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t269_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t269_0_0_0_Types };
extern const Il2CppType IBeginDragHandler_t270_0_0_0;
static const Il2CppType* GenInst_IBeginDragHandler_t270_0_0_0_Types[] = { &IBeginDragHandler_t270_0_0_0 };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t270_0_0_0 = { 1, GenInst_IBeginDragHandler_t270_0_0_0_Types };
extern const Il2CppType IDragHandler_t271_0_0_0;
static const Il2CppType* GenInst_IDragHandler_t271_0_0_0_Types[] = { &IDragHandler_t271_0_0_0 };
extern const Il2CppGenericInst GenInst_IDragHandler_t271_0_0_0 = { 1, GenInst_IDragHandler_t271_0_0_0_Types };
extern const Il2CppType IEndDragHandler_t272_0_0_0;
static const Il2CppType* GenInst_IEndDragHandler_t272_0_0_0_Types[] = { &IEndDragHandler_t272_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t272_0_0_0 = { 1, GenInst_IEndDragHandler_t272_0_0_0_Types };
extern const Il2CppType IDropHandler_t273_0_0_0;
static const Il2CppType* GenInst_IDropHandler_t273_0_0_0_Types[] = { &IDropHandler_t273_0_0_0 };
extern const Il2CppGenericInst GenInst_IDropHandler_t273_0_0_0 = { 1, GenInst_IDropHandler_t273_0_0_0_Types };
extern const Il2CppType IScrollHandler_t274_0_0_0;
static const Il2CppType* GenInst_IScrollHandler_t274_0_0_0_Types[] = { &IScrollHandler_t274_0_0_0 };
extern const Il2CppGenericInst GenInst_IScrollHandler_t274_0_0_0 = { 1, GenInst_IScrollHandler_t274_0_0_0_Types };
extern const Il2CppType IUpdateSelectedHandler_t275_0_0_0;
static const Il2CppType* GenInst_IUpdateSelectedHandler_t275_0_0_0_Types[] = { &IUpdateSelectedHandler_t275_0_0_0 };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t275_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t275_0_0_0_Types };
extern const Il2CppType IMoveHandler_t278_0_0_0;
static const Il2CppType* GenInst_IMoveHandler_t278_0_0_0_Types[] = { &IMoveHandler_t278_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoveHandler_t278_0_0_0 = { 1, GenInst_IMoveHandler_t278_0_0_0_Types };
extern const Il2CppType ISubmitHandler_t279_0_0_0;
static const Il2CppType* GenInst_ISubmitHandler_t279_0_0_0_Types[] = { &ISubmitHandler_t279_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t279_0_0_0 = { 1, GenInst_ISubmitHandler_t279_0_0_0_Types };
extern const Il2CppType ICancelHandler_t280_0_0_0;
static const Il2CppType* GenInst_ICancelHandler_t280_0_0_0_Types[] = { &ICancelHandler_t280_0_0_0 };
extern const Il2CppGenericInst GenInst_ICancelHandler_t280_0_0_0 = { 1, GenInst_ICancelHandler_t280_0_0_0_Types };
extern const Il2CppType List_1_t263_0_0_0;
static const Il2CppType* GenInst_List_1_t263_0_0_0_Types[] = { &List_1_t263_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t263_0_0_0 = { 1, GenInst_List_1_t263_0_0_0_Types };
extern const Il2CppType IEventSystemHandler_t1821_0_0_0;
static const Il2CppType* GenInst_IEventSystemHandler_t1821_0_0_0_Types[] = { &IEventSystemHandler_t1821_0_0_0 };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t1821_0_0_0 = { 1, GenInst_IEventSystemHandler_t1821_0_0_0_Types };
extern const Il2CppType Transform_t3_0_0_0;
static const Il2CppType* GenInst_Transform_t3_0_0_0_Types[] = { &Transform_t3_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t3_0_0_0 = { 1, GenInst_Transform_t3_0_0_0_Types };
extern const Il2CppType PointerEventData_t108_0_0_0;
static const Il2CppType* GenInst_PointerEventData_t108_0_0_0_Types[] = { &PointerEventData_t108_0_0_0 };
extern const Il2CppGenericInst GenInst_PointerEventData_t108_0_0_0 = { 1, GenInst_PointerEventData_t108_0_0_0_Types };
extern const Il2CppType AxisEventData_t105_0_0_0;
static const Il2CppType* GenInst_AxisEventData_t105_0_0_0_Types[] = { &AxisEventData_t105_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisEventData_t105_0_0_0 = { 1, GenInst_AxisEventData_t105_0_0_0_Types };
extern const Il2CppType BaseRaycaster_t104_0_0_0;
static const Il2CppType* GenInst_BaseRaycaster_t104_0_0_0_Types[] = { &BaseRaycaster_t104_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t104_0_0_0 = { 1, GenInst_BaseRaycaster_t104_0_0_0_Types };
extern const Il2CppType Camera_t122_0_0_0;
static const Il2CppType* GenInst_Camera_t122_0_0_0_Types[] = { &Camera_t122_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t122_0_0_0 = { 1, GenInst_Camera_t122_0_0_0_Types };
extern const Il2CppType EventSystem_t67_0_0_0;
static const Il2CppType* GenInst_EventSystem_t67_0_0_0_Types[] = { &EventSystem_t67_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSystem_t67_0_0_0 = { 1, GenInst_EventSystem_t67_0_0_0_Types };
extern const Il2CppType ButtonState_t111_0_0_0;
static const Il2CppType* GenInst_ButtonState_t111_0_0_0_Types[] = { &ButtonState_t111_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonState_t111_0_0_0 = { 1, GenInst_ButtonState_t111_0_0_0_Types };
extern const Il2CppType Int32_t56_0_0_0;
static const Il2CppType* GenInst_Int32_t56_0_0_0_PointerEventData_t108_0_0_0_Types[] = { &Int32_t56_0_0_0, &PointerEventData_t108_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t56_0_0_0_PointerEventData_t108_0_0_0 = { 2, GenInst_Int32_t56_0_0_0_PointerEventData_t108_0_0_0_Types };
extern const Il2CppType RaycastHit_t283_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t283_0_0_0_Types[] = { &RaycastHit_t283_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t283_0_0_0 = { 1, GenInst_RaycastHit_t283_0_0_0_Types };
extern const Il2CppType Color_t128_0_0_0;
static const Il2CppType* GenInst_Color_t128_0_0_0_Types[] = { &Color_t128_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t128_0_0_0 = { 1, GenInst_Color_t128_0_0_0_Types };
extern const Il2CppType ICanvasElement_t285_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t285_0_0_0_Types[] = { &ICanvasElement_t285_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t285_0_0_0 = { 1, GenInst_ICanvasElement_t285_0_0_0_Types };
extern const Il2CppType Font_t142_0_0_0;
extern const Il2CppType List_1_t312_0_0_0;
static const Il2CppType* GenInst_Font_t142_0_0_0_List_1_t312_0_0_0_Types[] = { &Font_t142_0_0_0, &List_1_t312_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t142_0_0_0_List_1_t312_0_0_0 = { 2, GenInst_Font_t142_0_0_0_List_1_t312_0_0_0_Types };
static const Il2CppType* GenInst_Font_t142_0_0_0_Types[] = { &Font_t142_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t142_0_0_0 = { 1, GenInst_Font_t142_0_0_0_Types };
extern const Il2CppType ColorTween_t127_0_0_0;
static const Il2CppType* GenInst_ColorTween_t127_0_0_0_Types[] = { &ColorTween_t127_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorTween_t127_0_0_0 = { 1, GenInst_ColorTween_t127_0_0_0_Types };
extern const Il2CppType List_1_t187_0_0_0;
static const Il2CppType* GenInst_List_1_t187_0_0_0_Types[] = { &List_1_t187_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t187_0_0_0 = { 1, GenInst_List_1_t187_0_0_0_Types };
extern const Il2CppType UIVertex_t191_0_0_0;
static const Il2CppType* GenInst_UIVertex_t191_0_0_0_Types[] = { &UIVertex_t191_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t191_0_0_0 = { 1, GenInst_UIVertex_t191_0_0_0_Types };
extern const Il2CppType RectTransform_t18_0_0_0;
static const Il2CppType* GenInst_RectTransform_t18_0_0_0_Types[] = { &RectTransform_t18_0_0_0 };
extern const Il2CppGenericInst GenInst_RectTransform_t18_0_0_0 = { 1, GenInst_RectTransform_t18_0_0_0_Types };
extern const Il2CppType Canvas_t148_0_0_0;
static const Il2CppType* GenInst_Canvas_t148_0_0_0_Types[] = { &Canvas_t148_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t148_0_0_0 = { 1, GenInst_Canvas_t148_0_0_0_Types };
extern const Il2CppType CanvasRenderer_t147_0_0_0;
static const Il2CppType* GenInst_CanvasRenderer_t147_0_0_0_Types[] = { &CanvasRenderer_t147_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t147_0_0_0 = { 1, GenInst_CanvasRenderer_t147_0_0_0_Types };
extern const Il2CppType Component_t64_0_0_0;
static const Il2CppType* GenInst_Component_t64_0_0_0_Types[] = { &Component_t64_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t64_0_0_0 = { 1, GenInst_Component_t64_0_0_0_Types };
extern const Il2CppType Graphic_t145_0_0_0;
static const Il2CppType* GenInst_Graphic_t145_0_0_0_Types[] = { &Graphic_t145_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t145_0_0_0 = { 1, GenInst_Graphic_t145_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t324_0_0_0;
static const Il2CppType* GenInst_Canvas_t148_0_0_0_IndexedSet_1_t324_0_0_0_Types[] = { &Canvas_t148_0_0_0, &IndexedSet_1_t324_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t148_0_0_0_IndexedSet_1_t324_0_0_0 = { 2, GenInst_Canvas_t148_0_0_0_IndexedSet_1_t324_0_0_0_Types };
extern const Il2CppType Sprite_t49_0_0_0;
static const Il2CppType* GenInst_Sprite_t49_0_0_0_Types[] = { &Sprite_t49_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t49_0_0_0 = { 1, GenInst_Sprite_t49_0_0_0_Types };
extern const Il2CppType Type_t160_0_0_0;
static const Il2CppType* GenInst_Type_t160_0_0_0_Types[] = { &Type_t160_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t160_0_0_0 = { 1, GenInst_Type_t160_0_0_0_Types };
extern const Il2CppType Boolean_t298_0_0_0;
static const Il2CppType* GenInst_Boolean_t298_0_0_0_Types[] = { &Boolean_t298_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t298_0_0_0 = { 1, GenInst_Boolean_t298_0_0_0_Types };
extern const Il2CppType FillMethod_t161_0_0_0;
static const Il2CppType* GenInst_FillMethod_t161_0_0_0_Types[] = { &FillMethod_t161_0_0_0 };
extern const Il2CppGenericInst GenInst_FillMethod_t161_0_0_0 = { 1, GenInst_FillMethod_t161_0_0_0_Types };
extern const Il2CppType Single_t61_0_0_0;
static const Il2CppType* GenInst_Single_t61_0_0_0_Types[] = { &Single_t61_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t61_0_0_0 = { 1, GenInst_Single_t61_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t56_0_0_0_Types[] = { &Int32_t56_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t56_0_0_0 = { 1, GenInst_Int32_t56_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType SubmitEvent_t174_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t174_0_0_0_Types[] = { &SubmitEvent_t174_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t174_0_0_0 = { 1, GenInst_SubmitEvent_t174_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t176_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t176_0_0_0_Types[] = { &OnChangeEvent_t176_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t176_0_0_0 = { 1, GenInst_OnChangeEvent_t176_0_0_0_Types };
extern const Il2CppType OnValidateInput_t178_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t178_0_0_0_Types[] = { &OnValidateInput_t178_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t178_0_0_0 = { 1, GenInst_OnValidateInput_t178_0_0_0_Types };
extern const Il2CppType ContentType_t170_0_0_0;
static const Il2CppType* GenInst_ContentType_t170_0_0_0_Types[] = { &ContentType_t170_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t170_0_0_0 = { 1, GenInst_ContentType_t170_0_0_0_Types };
extern const Il2CppType LineType_t173_0_0_0;
static const Il2CppType* GenInst_LineType_t173_0_0_0_Types[] = { &LineType_t173_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t173_0_0_0 = { 1, GenInst_LineType_t173_0_0_0_Types };
extern const Il2CppType InputType_t171_0_0_0;
static const Il2CppType* GenInst_InputType_t171_0_0_0_Types[] = { &InputType_t171_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t171_0_0_0 = { 1, GenInst_InputType_t171_0_0_0_Types };
extern const Il2CppType TouchScreenKeyboardType_t327_0_0_0;
static const Il2CppType* GenInst_TouchScreenKeyboardType_t327_0_0_0_Types[] = { &TouchScreenKeyboardType_t327_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t327_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t327_0_0_0_Types };
extern const Il2CppType CharacterValidation_t172_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t172_0_0_0_Types[] = { &CharacterValidation_t172_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t172_0_0_0 = { 1, GenInst_CharacterValidation_t172_0_0_0_Types };
extern const Il2CppType Char_t326_0_0_0;
static const Il2CppType* GenInst_Char_t326_0_0_0_Types[] = { &Char_t326_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t326_0_0_0 = { 1, GenInst_Char_t326_0_0_0_Types };
extern const Il2CppType UILineInfo_t331_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t331_0_0_0_Types[] = { &UILineInfo_t331_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t331_0_0_0 = { 1, GenInst_UILineInfo_t331_0_0_0_Types };
extern const Il2CppType UICharInfo_t333_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t333_0_0_0_Types[] = { &UICharInfo_t333_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t333_0_0_0 = { 1, GenInst_UICharInfo_t333_0_0_0_Types };
extern const Il2CppType LayoutElement_t243_0_0_0;
static const Il2CppType* GenInst_LayoutElement_t243_0_0_0_Types[] = { &LayoutElement_t243_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutElement_t243_0_0_0 = { 1, GenInst_LayoutElement_t243_0_0_0_Types };
extern const Il2CppType Direction_t197_0_0_0;
static const Il2CppType* GenInst_Direction_t197_0_0_0_Types[] = { &Direction_t197_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t197_0_0_0 = { 1, GenInst_Direction_t197_0_0_0_Types };
extern const Il2CppType Vector2_t59_0_0_0;
static const Il2CppType* GenInst_Vector2_t59_0_0_0_Types[] = { &Vector2_t59_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t59_0_0_0 = { 1, GenInst_Vector2_t59_0_0_0_Types };
extern const Il2CppType CanvasGroup_t317_0_0_0;
static const Il2CppType* GenInst_CanvasGroup_t317_0_0_0_Types[] = { &CanvasGroup_t317_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasGroup_t317_0_0_0 = { 1, GenInst_CanvasGroup_t317_0_0_0_Types };
extern const Il2CppType Selectable_t134_0_0_0;
static const Il2CppType* GenInst_Selectable_t134_0_0_0_Types[] = { &Selectable_t134_0_0_0 };
extern const Il2CppGenericInst GenInst_Selectable_t134_0_0_0 = { 1, GenInst_Selectable_t134_0_0_0_Types };
extern const Il2CppType Navigation_t194_0_0_0;
static const Il2CppType* GenInst_Navigation_t194_0_0_0_Types[] = { &Navigation_t194_0_0_0 };
extern const Il2CppGenericInst GenInst_Navigation_t194_0_0_0 = { 1, GenInst_Navigation_t194_0_0_0_Types };
extern const Il2CppType Transition_t209_0_0_0;
static const Il2CppType* GenInst_Transition_t209_0_0_0_Types[] = { &Transition_t209_0_0_0 };
extern const Il2CppGenericInst GenInst_Transition_t209_0_0_0 = { 1, GenInst_Transition_t209_0_0_0_Types };
extern const Il2CppType ColorBlock_t140_0_0_0;
static const Il2CppType* GenInst_ColorBlock_t140_0_0_0_Types[] = { &ColorBlock_t140_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorBlock_t140_0_0_0 = { 1, GenInst_ColorBlock_t140_0_0_0_Types };
extern const Il2CppType SpriteState_t211_0_0_0;
static const Il2CppType* GenInst_SpriteState_t211_0_0_0_Types[] = { &SpriteState_t211_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteState_t211_0_0_0 = { 1, GenInst_SpriteState_t211_0_0_0_Types };
extern const Il2CppType AnimationTriggers_t129_0_0_0;
static const Il2CppType* GenInst_AnimationTriggers_t129_0_0_0_Types[] = { &AnimationTriggers_t129_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t129_0_0_0 = { 1, GenInst_AnimationTriggers_t129_0_0_0_Types };
extern const Il2CppType Animator_t289_0_0_0;
static const Il2CppType* GenInst_Animator_t289_0_0_0_Types[] = { &Animator_t289_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t289_0_0_0 = { 1, GenInst_Animator_t289_0_0_0_Types };
extern const Il2CppType Direction_t215_0_0_0;
static const Il2CppType* GenInst_Direction_t215_0_0_0_Types[] = { &Direction_t215_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t215_0_0_0 = { 1, GenInst_Direction_t215_0_0_0_Types };
extern const Il2CppType Image_t167_0_0_0;
static const Il2CppType* GenInst_Image_t167_0_0_0_Types[] = { &Image_t167_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t167_0_0_0 = { 1, GenInst_Image_t167_0_0_0_Types };
extern const Il2CppType MatEntry_t218_0_0_0;
static const Il2CppType* GenInst_MatEntry_t218_0_0_0_Types[] = { &MatEntry_t218_0_0_0 };
extern const Il2CppGenericInst GenInst_MatEntry_t218_0_0_0 = { 1, GenInst_MatEntry_t218_0_0_0_Types };
extern const Il2CppType Toggle_t224_0_0_0;
static const Il2CppType* GenInst_Toggle_t224_0_0_0_Types[] = { &Toggle_t224_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t224_0_0_0 = { 1, GenInst_Toggle_t224_0_0_0_Types };
static const Il2CppType* GenInst_Toggle_t224_0_0_0_Boolean_t298_0_0_0_Types[] = { &Toggle_t224_0_0_0, &Boolean_t298_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t224_0_0_0_Boolean_t298_0_0_0 = { 2, GenInst_Toggle_t224_0_0_0_Boolean_t298_0_0_0_Types };
extern const Il2CppType AspectMode_t229_0_0_0;
static const Il2CppType* GenInst_AspectMode_t229_0_0_0_Types[] = { &AspectMode_t229_0_0_0 };
extern const Il2CppGenericInst GenInst_AspectMode_t229_0_0_0 = { 1, GenInst_AspectMode_t229_0_0_0_Types };
extern const Il2CppType FitMode_t235_0_0_0;
static const Il2CppType* GenInst_FitMode_t235_0_0_0_Types[] = { &FitMode_t235_0_0_0 };
extern const Il2CppGenericInst GenInst_FitMode_t235_0_0_0 = { 1, GenInst_FitMode_t235_0_0_0_Types };
extern const Il2CppType Corner_t237_0_0_0;
static const Il2CppType* GenInst_Corner_t237_0_0_0_Types[] = { &Corner_t237_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t237_0_0_0 = { 1, GenInst_Corner_t237_0_0_0_Types };
extern const Il2CppType Axis_t238_0_0_0;
static const Il2CppType* GenInst_Axis_t238_0_0_0_Types[] = { &Axis_t238_0_0_0 };
extern const Il2CppGenericInst GenInst_Axis_t238_0_0_0 = { 1, GenInst_Axis_t238_0_0_0_Types };
extern const Il2CppType Constraint_t239_0_0_0;
static const Il2CppType* GenInst_Constraint_t239_0_0_0_Types[] = { &Constraint_t239_0_0_0 };
extern const Il2CppGenericInst GenInst_Constraint_t239_0_0_0 = { 1, GenInst_Constraint_t239_0_0_0_Types };
extern const Il2CppType RectOffset_t244_0_0_0;
static const Il2CppType* GenInst_RectOffset_t244_0_0_0_Types[] = { &RectOffset_t244_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t244_0_0_0 = { 1, GenInst_RectOffset_t244_0_0_0_Types };
extern const Il2CppType TextAnchor_t348_0_0_0;
static const Il2CppType* GenInst_TextAnchor_t348_0_0_0_Types[] = { &TextAnchor_t348_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAnchor_t348_0_0_0 = { 1, GenInst_TextAnchor_t348_0_0_0_Types };
extern const Il2CppType ILayoutElement_t293_0_0_0;
static const Il2CppType* GenInst_ILayoutElement_t293_0_0_0_Single_t61_0_0_0_Types[] = { &ILayoutElement_t293_0_0_0, &Single_t61_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t293_0_0_0_Single_t61_0_0_0 = { 2, GenInst_ILayoutElement_t293_0_0_0_Single_t61_0_0_0_Types };
extern const Il2CppType List_1_t294_0_0_0;
static const Il2CppType* GenInst_List_1_t294_0_0_0_Types[] = { &List_1_t294_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t294_0_0_0 = { 1, GenInst_List_1_t294_0_0_0_Types };
extern const Il2CppType List_1_t292_0_0_0;
static const Il2CppType* GenInst_List_1_t292_0_0_0_Types[] = { &List_1_t292_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t292_0_0_0 = { 1, GenInst_List_1_t292_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t379_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t379_0_0_0_Types[] = { &GcLeaderboard_t379_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t379_0_0_0 = { 1, GenInst_GcLeaderboard_t379_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t636_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t636_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t636_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t636_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t636_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t638_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t638_0_0_0_Types[] = { &IAchievementU5BU5D_t638_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t638_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t638_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t558_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t558_0_0_0_Types[] = { &IScoreU5BU5D_t558_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t558_0_0_0 = { 1, GenInst_IScoreU5BU5D_t558_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t554_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t554_0_0_0_Types[] = { &IUserProfileU5BU5D_t554_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t554_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t554_0_0_0_Types };
extern const Il2CppType LayoutCache_t398_0_0_0;
static const Il2CppType* GenInst_Int32_t56_0_0_0_LayoutCache_t398_0_0_0_Types[] = { &Int32_t56_0_0_0, &LayoutCache_t398_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t56_0_0_0_LayoutCache_t398_0_0_0 = { 2, GenInst_Int32_t56_0_0_0_LayoutCache_t398_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t403_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t403_0_0_0_Types[] = { &GUILayoutEntry_t403_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t403_0_0_0 = { 1, GenInst_GUILayoutEntry_t403_0_0_0_Types };
extern const Il2CppType GUIStyle_t402_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t402_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t402_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t402_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t402_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0 = { 2, GenInst_String_t_0_0_0_String_t_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t435_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t435_0_0_0_Types[] = { &ByteU5BU5D_t435_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t435_0_0_0 = { 1, GenInst_ByteU5BU5D_t435_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Object_t_0_0_0_Types[] = { &String_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0 = { 2, GenInst_String_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType MatchDirectConnectInfo_t495_0_0_0;
static const Il2CppType* GenInst_MatchDirectConnectInfo_t495_0_0_0_Types[] = { &MatchDirectConnectInfo_t495_0_0_0 };
extern const Il2CppGenericInst GenInst_MatchDirectConnectInfo_t495_0_0_0 = { 1, GenInst_MatchDirectConnectInfo_t495_0_0_0_Types };
extern const Il2CppType MatchDesc_t496_0_0_0;
static const Il2CppType* GenInst_MatchDesc_t496_0_0_0_Types[] = { &MatchDesc_t496_0_0_0 };
extern const Il2CppGenericInst GenInst_MatchDesc_t496_0_0_0 = { 1, GenInst_MatchDesc_t496_0_0_0_Types };
extern const Il2CppType NetworkID_t502_0_0_0;
extern const Il2CppType NetworkAccessToken_t504_0_0_0;
static const Il2CppType* GenInst_NetworkID_t502_0_0_0_NetworkAccessToken_t504_0_0_0_Types[] = { &NetworkID_t502_0_0_0, &NetworkAccessToken_t504_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t502_0_0_0_NetworkAccessToken_t504_0_0_0 = { 2, GenInst_NetworkID_t502_0_0_0_NetworkAccessToken_t504_0_0_0_Types };
extern const Il2CppType CreateMatchResponse_t489_0_0_0;
static const Il2CppType* GenInst_CreateMatchResponse_t489_0_0_0_Types[] = { &CreateMatchResponse_t489_0_0_0 };
extern const Il2CppGenericInst GenInst_CreateMatchResponse_t489_0_0_0 = { 1, GenInst_CreateMatchResponse_t489_0_0_0_Types };
extern const Il2CppType JoinMatchResponse_t491_0_0_0;
static const Il2CppType* GenInst_JoinMatchResponse_t491_0_0_0_Types[] = { &JoinMatchResponse_t491_0_0_0 };
extern const Il2CppGenericInst GenInst_JoinMatchResponse_t491_0_0_0 = { 1, GenInst_JoinMatchResponse_t491_0_0_0_Types };
extern const Il2CppType BasicResponse_t486_0_0_0;
static const Il2CppType* GenInst_BasicResponse_t486_0_0_0_Types[] = { &BasicResponse_t486_0_0_0 };
extern const Il2CppGenericInst GenInst_BasicResponse_t486_0_0_0 = { 1, GenInst_BasicResponse_t486_0_0_0_Types };
extern const Il2CppType ListMatchResponse_t498_0_0_0;
static const Il2CppType* GenInst_ListMatchResponse_t498_0_0_0_Types[] = { &ListMatchResponse_t498_0_0_0 };
extern const Il2CppGenericInst GenInst_ListMatchResponse_t498_0_0_0 = { 1, GenInst_ListMatchResponse_t498_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t618_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t618_0_0_0_Types[] = { &KeyValuePair_2_t618_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t618_0_0_0 = { 1, GenInst_KeyValuePair_2_t618_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
extern const Il2CppType ConstructorDelegate_t522_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_ConstructorDelegate_t522_0_0_0_Types[] = { &Type_t_0_0_0, &ConstructorDelegate_t522_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ConstructorDelegate_t522_0_0_0 = { 2, GenInst_Type_t_0_0_0_ConstructorDelegate_t522_0_0_0_Types };
extern const Il2CppType IDictionary_2_t625_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t625_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t625_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t625_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t625_0_0_0_Types };
extern const Il2CppType GetDelegate_t520_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GetDelegate_t520_0_0_0_Types[] = { &String_t_0_0_0, &GetDelegate_t520_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetDelegate_t520_0_0_0 = { 2, GenInst_String_t_0_0_0_GetDelegate_t520_0_0_0_Types };
extern const Il2CppType IDictionary_2_t626_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t626_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t626_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t626_0_0_0 = { 2, GenInst_Type_t_0_0_0_IDictionary_2_t626_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t678_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_KeyValuePair_2_t678_0_0_0_Types[] = { &String_t_0_0_0, &KeyValuePair_2_t678_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_KeyValuePair_2_t678_0_0_0 = { 2, GenInst_String_t_0_0_0_KeyValuePair_2_t678_0_0_0_Types };
extern const Il2CppType SetDelegate_t521_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_SetDelegate_t521_0_0_0_Types[] = { &Type_t_0_0_0, &SetDelegate_t521_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_SetDelegate_t521_0_0_0 = { 2, GenInst_Type_t_0_0_0_SetDelegate_t521_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t681_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t681_0_0_0_Types[] = { &KeyValuePair_2_t681_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t681_0_0_0 = { 1, GenInst_KeyValuePair_2_t681_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t524_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t524_0_0_0_Types[] = { &ConstructorInfo_t524_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t524_0_0_0 = { 1, GenInst_ConstructorInfo_t524_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType GUILayer_t387_0_0_0;
static const Il2CppType* GenInst_GUILayer_t387_0_0_0_Types[] = { &GUILayer_t387_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t387_0_0_0 = { 1, GenInst_GUILayer_t387_0_0_0_Types };
extern const Il2CppType PersistentCall_t586_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t586_0_0_0_Types[] = { &PersistentCall_t586_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t586_0_0_0 = { 1, GenInst_PersistentCall_t586_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t583_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t583_0_0_0_Types[] = { &BaseInvokableCall_t583_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t583_0_0_0 = { 1, GenInst_BaseInvokableCall_t583_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t56_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t56_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t56_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t56_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t298_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t298_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t298_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t298_0_0_0_Types };
extern const Il2CppType StrongName_t1565_0_0_0;
static const Il2CppType* GenInst_StrongName_t1565_0_0_0_Types[] = { &StrongName_t1565_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t1565_0_0_0 = { 1, GenInst_StrongName_t1565_0_0_0_Types };
extern const Il2CppType DateTime_t396_0_0_0;
static const Il2CppType* GenInst_DateTime_t396_0_0_0_Types[] = { &DateTime_t396_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t396_0_0_0 = { 1, GenInst_DateTime_t396_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t679_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t679_0_0_0_Types[] = { &DateTimeOffset_t679_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t679_0_0_0 = { 1, GenInst_DateTimeOffset_t679_0_0_0_Types };
extern const Il2CppType TimeSpan_t977_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t977_0_0_0_Types[] = { &TimeSpan_t977_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t977_0_0_0 = { 1, GenInst_TimeSpan_t977_0_0_0_Types };
extern const Il2CppType Guid_t680_0_0_0;
static const Il2CppType* GenInst_Guid_t680_0_0_0_Types[] = { &Guid_t680_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t680_0_0_0 = { 1, GenInst_Guid_t680_0_0_0_Types };
extern const Il2CppType Object_t54_0_0_0;
static const Il2CppType* GenInst_Object_t54_0_0_0_Types[] = { &Object_t54_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t54_0_0_0 = { 1, GenInst_Object_t54_0_0_0_Types };
extern const Il2CppType Vector3_t35_0_0_0;
static const Il2CppType* GenInst_Vector3_t35_0_0_0_Types[] = { &Vector3_t35_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t35_0_0_0 = { 1, GenInst_Vector3_t35_0_0_0_Types };
extern const Il2CppType ValueType_t1081_0_0_0;
static const Il2CppType* GenInst_ValueType_t1081_0_0_0_Types[] = { &ValueType_t1081_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t1081_0_0_0 = { 1, GenInst_ValueType_t1081_0_0_0_Types };
extern const Il2CppType IReflect_t2662_0_0_0;
static const Il2CppType* GenInst_IReflect_t2662_0_0_0_Types[] = { &IReflect_t2662_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t2662_0_0_0 = { 1, GenInst_IReflect_t2662_0_0_0_Types };
extern const Il2CppType _Type_t2660_0_0_0;
static const Il2CppType* GenInst__Type_t2660_0_0_0_Types[] = { &_Type_t2660_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t2660_0_0_0 = { 1, GenInst__Type_t2660_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t1742_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t1742_0_0_0_Types[] = { &ICustomAttributeProvider_t1742_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t1742_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t1742_0_0_0_Types };
extern const Il2CppType _MemberInfo_t2661_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t2661_0_0_0_Types[] = { &_MemberInfo_t2661_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t2661_0_0_0 = { 1, GenInst__MemberInfo_t2661_0_0_0_Types };
extern const Il2CppType IFormattable_t1744_0_0_0;
static const Il2CppType* GenInst_IFormattable_t1744_0_0_0_Types[] = { &IFormattable_t1744_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t1744_0_0_0 = { 1, GenInst_IFormattable_t1744_0_0_0_Types };
extern const Il2CppType IConvertible_t1747_0_0_0;
static const Il2CppType* GenInst_IConvertible_t1747_0_0_0_Types[] = { &IConvertible_t1747_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t1747_0_0_0 = { 1, GenInst_IConvertible_t1747_0_0_0_Types };
extern const Il2CppType IComparable_t1746_0_0_0;
static const Il2CppType* GenInst_IComparable_t1746_0_0_0_Types[] = { &IComparable_t1746_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t1746_0_0_0 = { 1, GenInst_IComparable_t1746_0_0_0_Types };
extern const Il2CppType IComparable_1_t2873_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2873_0_0_0_Types[] = { &IComparable_1_t2873_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2873_0_0_0 = { 1, GenInst_IComparable_1_t2873_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2878_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2878_0_0_0_Types[] = { &IEquatable_1_t2878_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2878_0_0_0 = { 1, GenInst_IEquatable_1_t2878_0_0_0_Types };
extern const Il2CppType Double_t661_0_0_0;
static const Il2CppType* GenInst_Double_t661_0_0_0_Types[] = { &Double_t661_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t661_0_0_0 = { 1, GenInst_Double_t661_0_0_0_Types };
extern const Il2CppType IComparable_1_t2886_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2886_0_0_0_Types[] = { &IComparable_1_t2886_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2886_0_0_0 = { 1, GenInst_IComparable_1_t2886_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2891_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2891_0_0_0_Types[] = { &IEquatable_1_t2891_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2891_0_0_0 = { 1, GenInst_IEquatable_1_t2891_0_0_0_Types };
extern const Il2CppType IComparable_1_t2899_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2899_0_0_0_Types[] = { &IComparable_1_t2899_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2899_0_0_0 = { 1, GenInst_IComparable_1_t2899_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2904_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2904_0_0_0_Types[] = { &IEquatable_1_t2904_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2904_0_0_0 = { 1, GenInst_IEquatable_1_t2904_0_0_0_Types };
extern const Il2CppType IEnumerable_t623_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t623_0_0_0_Types[] = { &IEnumerable_t623_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t623_0_0_0 = { 1, GenInst_IEnumerable_t623_0_0_0_Types };
extern const Il2CppType ICloneable_t2653_0_0_0;
static const Il2CppType* GenInst_ICloneable_t2653_0_0_0_Types[] = { &ICloneable_t2653_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t2653_0_0_0 = { 1, GenInst_ICloneable_t2653_0_0_0_Types };
extern const Il2CppType IComparable_1_t2919_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2919_0_0_0_Types[] = { &IComparable_1_t2919_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2919_0_0_0 = { 1, GenInst_IComparable_1_t2919_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2924_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2924_0_0_0_Types[] = { &IEquatable_1_t2924_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2924_0_0_0 = { 1, GenInst_IEquatable_1_t2924_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t56_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t56_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t56_0_0_0_Object_t_0_0_0 = { 2, GenInst_Int32_t56_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1877_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1877_0_0_0_Types[] = { &KeyValuePair_2_t1877_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1877_0_0_0 = { 1, GenInst_KeyValuePair_2_t1877_0_0_0_Types };
extern const Il2CppType Link_t1188_0_0_0;
static const Il2CppType* GenInst_Link_t1188_0_0_0_Types[] = { &Link_t1188_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t1188_0_0_0 = { 1, GenInst_Link_t1188_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t56_0_0_0_Object_t_0_0_0_Int32_t56_0_0_0_Types[] = { &Int32_t56_0_0_0, &Object_t_0_0_0, &Int32_t56_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t56_0_0_0_Object_t_0_0_0_Int32_t56_0_0_0 = { 3, GenInst_Int32_t56_0_0_0_Object_t_0_0_0_Int32_t56_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t56_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t56_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t56_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Int32_t56_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t1068_0_0_0;
static const Il2CppType* GenInst_Int32_t56_0_0_0_Object_t_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &Int32_t56_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t56_0_0_0_Object_t_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_Int32_t56_0_0_0_Object_t_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t1068_0_0_0_Types[] = { &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1068_0_0_0 = { 1, GenInst_DictionaryEntry_t1068_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t56_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1877_0_0_0_Types[] = { &Int32_t56_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t1877_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t56_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1877_0_0_0 = { 3, GenInst_Int32_t56_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1877_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t56_0_0_0_PointerEventData_t108_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &Int32_t56_0_0_0, &PointerEventData_t108_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t56_0_0_0_PointerEventData_t108_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_Int32_t56_0_0_0_PointerEventData_t108_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t304_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t304_0_0_0_Types[] = { &KeyValuePair_2_t304_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t304_0_0_0 = { 1, GenInst_KeyValuePair_2_t304_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t52_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t52_0_0_0_Types[] = { &RaycastHit2D_t52_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t52_0_0_0 = { 1, GenInst_RaycastHit2D_t52_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t56_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t56_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t56_0_0_0 = { 2, GenInst_Object_t_0_0_0_Int32_t56_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1908_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1908_0_0_0_Types[] = { &KeyValuePair_2_t1908_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1908_0_0_0 = { 1, GenInst_KeyValuePair_2_t1908_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t56_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t56_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t56_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t56_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t56_0_0_0_Int32_t56_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t56_0_0_0, &Int32_t56_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t56_0_0_0_Int32_t56_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t56_0_0_0_Int32_t56_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t56_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t56_0_0_0_KeyValuePair_2_t1908_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t56_0_0_0, &KeyValuePair_2_t1908_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t56_0_0_0_KeyValuePair_2_t1908_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t56_0_0_0_KeyValuePair_2_t1908_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t285_0_0_0_Int32_t56_0_0_0_Types[] = { &ICanvasElement_t285_0_0_0, &Int32_t56_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t285_0_0_0_Int32_t56_0_0_0 = { 2, GenInst_ICanvasElement_t285_0_0_0_Int32_t56_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t285_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &ICanvasElement_t285_0_0_0, &Int32_t56_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t285_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_ICanvasElement_t285_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1934_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1934_0_0_0_Types[] = { &KeyValuePair_2_t1934_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1934_0_0_0 = { 1, GenInst_KeyValuePair_2_t1934_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t1934_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_Types };
static const Il2CppType* GenInst_Font_t142_0_0_0_List_1_t312_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &Font_t142_0_0_0, &List_1_t312_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t142_0_0_0_List_1_t312_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_Font_t142_0_0_0_List_1_t312_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
static const Il2CppType* GenInst_List_1_t312_0_0_0_Types[] = { &List_1_t312_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t312_0_0_0 = { 1, GenInst_List_1_t312_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1944_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1944_0_0_0_Types[] = { &KeyValuePair_2_t1944_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1944_0_0_0 = { 1, GenInst_KeyValuePair_2_t1944_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t145_0_0_0_Int32_t56_0_0_0_Types[] = { &Graphic_t145_0_0_0, &Int32_t56_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t145_0_0_0_Int32_t56_0_0_0 = { 2, GenInst_Graphic_t145_0_0_0_Int32_t56_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t145_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &Graphic_t145_0_0_0, &Int32_t56_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t145_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_Graphic_t145_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t148_0_0_0_IndexedSet_1_t324_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &Canvas_t148_0_0_0, &IndexedSet_1_t324_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t148_0_0_0_IndexedSet_1_t324_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_Canvas_t148_0_0_0_IndexedSet_1_t324_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t324_0_0_0_Types[] = { &IndexedSet_1_t324_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t324_0_0_0 = { 1, GenInst_IndexedSet_1_t324_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1976_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1976_0_0_0_Types[] = { &KeyValuePair_2_t1976_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1976_0_0_0 = { 1, GenInst_KeyValuePair_2_t1976_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1980_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1980_0_0_0_Types[] = { &KeyValuePair_2_t1980_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1980_0_0_0 = { 1, GenInst_KeyValuePair_2_t1980_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1984_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1984_0_0_0_Types[] = { &KeyValuePair_2_t1984_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1984_0_0_0 = { 1, GenInst_KeyValuePair_2_t1984_0_0_0_Types };
extern const Il2CppType Enum_t624_0_0_0;
static const Il2CppType* GenInst_Enum_t624_0_0_0_Types[] = { &Enum_t624_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t624_0_0_0 = { 1, GenInst_Enum_t624_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t298_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t298_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t298_0_0_0 = { 2, GenInst_Object_t_0_0_0_Boolean_t298_0_0_0_Types };
extern const Il2CppType LayoutRebuilder_t246_0_0_0;
static const Il2CppType* GenInst_LayoutRebuilder_t246_0_0_0_Types[] = { &LayoutRebuilder_t246_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t246_0_0_0 = { 1, GenInst_LayoutRebuilder_t246_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Single_t61_0_0_0_Types[] = { &Object_t_0_0_0, &Single_t61_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t61_0_0_0 = { 2, GenInst_Object_t_0_0_0_Single_t61_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t2631_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t2631_0_0_0_Types[] = { &IAchievementDescription_t2631_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t2631_0_0_0 = { 1, GenInst_IAchievementDescription_t2631_0_0_0_Types };
extern const Il2CppType IAchievement_t605_0_0_0;
static const Il2CppType* GenInst_IAchievement_t605_0_0_0_Types[] = { &IAchievement_t605_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t605_0_0_0 = { 1, GenInst_IAchievement_t605_0_0_0_Types };
extern const Il2CppType IScore_t560_0_0_0;
static const Il2CppType* GenInst_IScore_t560_0_0_0_Types[] = { &IScore_t560_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t560_0_0_0 = { 1, GenInst_IScore_t560_0_0_0_Types };
extern const Il2CppType IUserProfile_t2630_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t2630_0_0_0_Types[] = { &IUserProfile_t2630_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t2630_0_0_0 = { 1, GenInst_IUserProfile_t2630_0_0_0_Types };
extern const Il2CppType AchievementDescription_t556_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t556_0_0_0_Types[] = { &AchievementDescription_t556_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t556_0_0_0 = { 1, GenInst_AchievementDescription_t556_0_0_0_Types };
extern const Il2CppType UserProfile_t553_0_0_0;
static const Il2CppType* GenInst_UserProfile_t553_0_0_0_Types[] = { &UserProfile_t553_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t553_0_0_0 = { 1, GenInst_UserProfile_t553_0_0_0_Types };
extern const Il2CppType GcAchievementData_t546_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t546_0_0_0_Types[] = { &GcAchievementData_t546_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t546_0_0_0 = { 1, GenInst_GcAchievementData_t546_0_0_0_Types };
extern const Il2CppType Achievement_t555_0_0_0;
static const Il2CppType* GenInst_Achievement_t555_0_0_0_Types[] = { &Achievement_t555_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t555_0_0_0 = { 1, GenInst_Achievement_t555_0_0_0_Types };
extern const Il2CppType GcScoreData_t547_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t547_0_0_0_Types[] = { &GcScoreData_t547_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t547_0_0_0 = { 1, GenInst_GcScoreData_t547_0_0_0_Types };
extern const Il2CppType Score_t557_0_0_0;
static const Il2CppType* GenInst_Score_t557_0_0_0_Types[] = { &Score_t557_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t557_0_0_0 = { 1, GenInst_Score_t557_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t407_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t407_0_0_0_Types[] = { &GUILayoutOption_t407_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t407_0_0_0 = { 1, GenInst_GUILayoutOption_t407_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t56_0_0_0_LayoutCache_t398_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &Int32_t56_0_0_0, &LayoutCache_t398_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t56_0_0_0_LayoutCache_t398_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_Int32_t56_0_0_0_LayoutCache_t398_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
static const Il2CppType* GenInst_LayoutCache_t398_0_0_0_Types[] = { &LayoutCache_t398_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutCache_t398_0_0_0 = { 1, GenInst_LayoutCache_t398_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2052_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2052_0_0_0_Types[] = { &KeyValuePair_2_t2052_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2052_0_0_0 = { 1, GenInst_KeyValuePair_2_t2052_0_0_0_Types };
static const Il2CppType* GenInst_GUIStyle_t402_0_0_0_Types[] = { &GUIStyle_t402_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t402_0_0_0 = { 1, GenInst_GUIStyle_t402_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t402_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t402_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t402_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t402_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2064_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2064_0_0_0_Types[] = { &KeyValuePair_2_t2064_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2064_0_0_0 = { 1, GenInst_KeyValuePair_2_t2064_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t56_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2068_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2068_0_0_0_Types[] = { &KeyValuePair_2_t2068_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2068_0_0_0 = { 1, GenInst_KeyValuePair_2_t2068_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &String_t_0_0_0, &String_t_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t648_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t648_0_0_0_Types[] = { &KeyValuePair_2_t648_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t648_0_0_0 = { 1, GenInst_KeyValuePair_2_t648_0_0_0_Types };
extern const Il2CppType Byte_t647_0_0_0;
static const Il2CppType* GenInst_Byte_t647_0_0_0_Types[] = { &Byte_t647_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t647_0_0_0 = { 1, GenInst_Byte_t647_0_0_0_Types };
extern const Il2CppType IComparable_1_t3089_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3089_0_0_0_Types[] = { &IComparable_1_t3089_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3089_0_0_0 = { 1, GenInst_IComparable_1_t3089_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3094_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3094_0_0_0_Types[] = { &IEquatable_1_t3094_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3094_0_0_0 = { 1, GenInst_IEquatable_1_t3094_0_0_0_Types };
extern const Il2CppType Behaviour_t354_0_0_0;
static const Il2CppType* GenInst_Behaviour_t354_0_0_0_Types[] = { &Behaviour_t354_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t354_0_0_0 = { 1, GenInst_Behaviour_t354_0_0_0_Types };
extern const Il2CppType Display_t444_0_0_0;
static const Il2CppType* GenInst_Display_t444_0_0_0_Types[] = { &Display_t444_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t444_0_0_0 = { 1, GenInst_Display_t444_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType ISerializable_t1769_0_0_0;
static const Il2CppType* GenInst_ISerializable_t1769_0_0_0_Types[] = { &ISerializable_t1769_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t1769_0_0_0 = { 1, GenInst_ISerializable_t1769_0_0_0_Types };
extern const Il2CppType IComparable_1_t3127_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3127_0_0_0_Types[] = { &IComparable_1_t3127_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3127_0_0_0 = { 1, GenInst_IComparable_1_t3127_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3132_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3132_0_0_0_Types[] = { &IEquatable_1_t3132_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3132_0_0_0 = { 1, GenInst_IEquatable_1_t3132_0_0_0_Types };
extern const Il2CppType Keyframe_t469_0_0_0;
static const Il2CppType* GenInst_Keyframe_t469_0_0_0_Types[] = { &Keyframe_t469_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t469_0_0_0 = { 1, GenInst_Keyframe_t469_0_0_0_Types };
extern const Il2CppType Int64_t662_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Int64_t662_0_0_0_Types[] = { &String_t_0_0_0, &Int64_t662_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int64_t662_0_0_0 = { 2, GenInst_String_t_0_0_0_Int64_t662_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int64_t662_0_0_0_Types[] = { &Object_t_0_0_0, &Int64_t662_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t662_0_0_0 = { 2, GenInst_Object_t_0_0_0_Int64_t662_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2118_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2118_0_0_0_Types[] = { &KeyValuePair_2_t2118_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2118_0_0_0 = { 1, GenInst_KeyValuePair_2_t2118_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t662_0_0_0_Types[] = { &Int64_t662_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t662_0_0_0 = { 1, GenInst_Int64_t662_0_0_0_Types };
extern const Il2CppType IComparable_1_t3151_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3151_0_0_0_Types[] = { &IComparable_1_t3151_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3151_0_0_0 = { 1, GenInst_IComparable_1_t3151_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3156_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3156_0_0_0_Types[] = { &IEquatable_1_t3156_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3156_0_0_0 = { 1, GenInst_IEquatable_1_t3156_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int64_t662_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Int64_t662_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t662_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int64_t662_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int64_t662_0_0_0_Int64_t662_0_0_0_Types[] = { &Object_t_0_0_0, &Int64_t662_0_0_0, &Int64_t662_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t662_0_0_0_Int64_t662_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int64_t662_0_0_0_Int64_t662_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int64_t662_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &Object_t_0_0_0, &Int64_t662_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t662_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int64_t662_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int64_t662_0_0_0_KeyValuePair_2_t2118_0_0_0_Types[] = { &Object_t_0_0_0, &Int64_t662_0_0_0, &KeyValuePair_2_t2118_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int64_t662_0_0_0_KeyValuePair_2_t2118_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int64_t662_0_0_0_KeyValuePair_2_t2118_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int64_t662_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &String_t_0_0_0, &Int64_t662_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int64_t662_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_String_t_0_0_0_Int64_t662_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2133_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2133_0_0_0_Types[] = { &KeyValuePair_2_t2133_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2133_0_0_0 = { 1, GenInst_KeyValuePair_2_t2133_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0_Types[] = { &NetworkID_t502_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0 = { 2, GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2155_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2155_0_0_0_Types[] = { &KeyValuePair_2_t2155_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2155_0_0_0 = { 1, GenInst_KeyValuePair_2_t2155_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t502_0_0_0_Types[] = { &NetworkID_t502_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t502_0_0_0 = { 1, GenInst_NetworkID_t502_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0_NetworkID_t502_0_0_0_Types[] = { &NetworkID_t502_0_0_0, &Object_t_0_0_0, &NetworkID_t502_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0_NetworkID_t502_0_0_0 = { 3, GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0_NetworkID_t502_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &NetworkID_t502_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &NetworkID_t502_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2155_0_0_0_Types[] = { &NetworkID_t502_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t2155_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2155_0_0_0 = { 3, GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2155_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t502_0_0_0_NetworkAccessToken_t504_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &NetworkID_t502_0_0_0, &NetworkAccessToken_t504_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t502_0_0_0_NetworkAccessToken_t504_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_NetworkID_t502_0_0_0_NetworkAccessToken_t504_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
static const Il2CppType* GenInst_NetworkAccessToken_t504_0_0_0_Types[] = { &NetworkAccessToken_t504_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkAccessToken_t504_0_0_0 = { 1, GenInst_NetworkAccessToken_t504_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2169_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2169_0_0_0_Types[] = { &KeyValuePair_2_t2169_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2169_0_0_0 = { 1, GenInst_KeyValuePair_2_t2169_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &String_t_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
static const Il2CppType* GenInst_ConstructorDelegate_t522_0_0_0_Types[] = { &ConstructorDelegate_t522_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorDelegate_t522_0_0_0 = { 1, GenInst_ConstructorDelegate_t522_0_0_0_Types };
static const Il2CppType* GenInst_GetDelegate_t520_0_0_0_Types[] = { &GetDelegate_t520_0_0_0 };
extern const Il2CppGenericInst GenInst_GetDelegate_t520_0_0_0 = { 1, GenInst_GetDelegate_t520_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t625_0_0_0_Types[] = { &IDictionary_2_t625_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t625_0_0_0 = { 1, GenInst_IDictionary_2_t625_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t678_0_0_0_Types[] = { &KeyValuePair_2_t678_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t678_0_0_0 = { 1, GenInst_KeyValuePair_2_t678_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t626_0_0_0_Types[] = { &IDictionary_2_t626_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t626_0_0_0 = { 1, GenInst_IDictionary_2_t626_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_Types[] = { &Object_t_0_0_0, &KeyValuePair_2_t1934_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0 = { 2, GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2175_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2175_0_0_0_Types[] = { &KeyValuePair_2_t2175_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2175_0_0_0 = { 1, GenInst_KeyValuePair_2_t2175_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_ConstructorDelegate_t522_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &Type_t_0_0_0, &ConstructorDelegate_t522_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_ConstructorDelegate_t522_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_Type_t_0_0_0_ConstructorDelegate_t522_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2183_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2183_0_0_0_Types[] = { &KeyValuePair_2_t2183_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2183_0_0_0 = { 1, GenInst_KeyValuePair_2_t2183_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t625_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t625_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t625_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t625_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2187_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2187_0_0_0_Types[] = { &KeyValuePair_2_t2187_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2187_0_0_0 = { 1, GenInst_KeyValuePair_2_t2187_0_0_0_Types };
static const Il2CppType* GenInst_Type_t_0_0_0_IDictionary_2_t626_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &Type_t_0_0_0, &IDictionary_2_t626_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0_IDictionary_2_t626_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_Type_t_0_0_0_IDictionary_2_t626_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2191_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2191_0_0_0_Types[] = { &KeyValuePair_2_t2191_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2191_0_0_0 = { 1, GenInst_KeyValuePair_2_t2191_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GetDelegate_t520_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &String_t_0_0_0, &GetDelegate_t520_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GetDelegate_t520_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_String_t_0_0_0_GetDelegate_t520_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &KeyValuePair_2_t1934_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_KeyValuePair_2_t1934_0_0_0_Types[] = { &Object_t_0_0_0, &KeyValuePair_2_t1934_0_0_0, &KeyValuePair_2_t1934_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_KeyValuePair_2_t1934_0_0_0 = { 3, GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_KeyValuePair_2_t1934_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &Object_t_0_0_0, &KeyValuePair_2_t1934_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_KeyValuePair_2_t2175_0_0_0_Types[] = { &Object_t_0_0_0, &KeyValuePair_2_t1934_0_0_0, &KeyValuePair_2_t2175_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_KeyValuePair_2_t2175_0_0_0 = { 3, GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_KeyValuePair_2_t2175_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_KeyValuePair_2_t678_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &String_t_0_0_0, &KeyValuePair_2_t678_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_KeyValuePair_2_t678_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_String_t_0_0_0_KeyValuePair_2_t678_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2213_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2213_0_0_0_Types[] = { &KeyValuePair_2_t2213_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2213_0_0_0 = { 1, GenInst_KeyValuePair_2_t2213_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t2698_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t2698_0_0_0_Types[] = { &_ConstructorInfo_t2698_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t2698_0_0_0 = { 1, GenInst__ConstructorInfo_t2698_0_0_0_Types };
extern const Il2CppType MethodBase_t696_0_0_0;
static const Il2CppType* GenInst_MethodBase_t696_0_0_0_Types[] = { &MethodBase_t696_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t696_0_0_0 = { 1, GenInst_MethodBase_t696_0_0_0_Types };
extern const Il2CppType _MethodBase_t2701_0_0_0;
static const Il2CppType* GenInst__MethodBase_t2701_0_0_0_Types[] = { &_MethodBase_t2701_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t2701_0_0_0 = { 1, GenInst__MethodBase_t2701_0_0_0_Types };
extern const Il2CppType ParameterInfo_t686_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t686_0_0_0_Types[] = { &ParameterInfo_t686_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t686_0_0_0 = { 1, GenInst_ParameterInfo_t686_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t2704_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t2704_0_0_0_Types[] = { &_ParameterInfo_t2704_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t2704_0_0_0 = { 1, GenInst__ParameterInfo_t2704_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t2705_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t2705_0_0_0_Types[] = { &_PropertyInfo_t2705_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t2705_0_0_0 = { 1, GenInst__PropertyInfo_t2705_0_0_0_Types };
extern const Il2CppType _FieldInfo_t2700_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t2700_0_0_0_Types[] = { &_FieldInfo_t2700_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t2700_0_0_0 = { 1, GenInst__FieldInfo_t2700_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t536_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t536_0_0_0_Types[] = { &DisallowMultipleComponent_t536_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t536_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t536_0_0_0_Types };
extern const Il2CppType Attribute_t428_0_0_0;
static const Il2CppType* GenInst_Attribute_t428_0_0_0_Types[] = { &Attribute_t428_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t428_0_0_0 = { 1, GenInst_Attribute_t428_0_0_0_Types };
extern const Il2CppType _Attribute_t2649_0_0_0;
static const Il2CppType* GenInst__Attribute_t2649_0_0_0_Types[] = { &_Attribute_t2649_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t2649_0_0_0 = { 1, GenInst__Attribute_t2649_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t539_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t539_0_0_0_Types[] = { &ExecuteInEditMode_t539_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t539_0_0_0 = { 1, GenInst_ExecuteInEditMode_t539_0_0_0_Types };
extern const Il2CppType RequireComponent_t537_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t537_0_0_0_Types[] = { &RequireComponent_t537_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t537_0_0_0 = { 1, GenInst_RequireComponent_t537_0_0_0_Types };
extern const Il2CppType ParameterModifier_t1346_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t1346_0_0_0_Types[] = { &ParameterModifier_t1346_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1346_0_0_0 = { 1, GenInst_ParameterModifier_t1346_0_0_0_Types };
extern const Il2CppType HitInfo_t561_0_0_0;
static const Il2CppType* GenInst_HitInfo_t561_0_0_0_Types[] = { &HitInfo_t561_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t561_0_0_0 = { 1, GenInst_HitInfo_t561_0_0_0_Types };
extern const Il2CppType Event_t189_0_0_0;
extern const Il2CppType TextEditOp_t579_0_0_0;
static const Il2CppType* GenInst_Event_t189_0_0_0_TextEditOp_t579_0_0_0_Types[] = { &Event_t189_0_0_0, &TextEditOp_t579_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t189_0_0_0_TextEditOp_t579_0_0_0 = { 2, GenInst_Event_t189_0_0_0_TextEditOp_t579_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t579_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0 = { 2, GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2238_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2238_0_0_0_Types[] = { &KeyValuePair_2_t2238_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2238_0_0_0 = { 1, GenInst_KeyValuePair_2_t2238_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t579_0_0_0_Types[] = { &TextEditOp_t579_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t579_0_0_0 = { 1, GenInst_TextEditOp_t579_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t579_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0_TextEditOp_t579_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t579_0_0_0, &TextEditOp_t579_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0_TextEditOp_t579_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0_TextEditOp_t579_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t579_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0_KeyValuePair_2_t2238_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t579_0_0_0, &KeyValuePair_2_t2238_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0_KeyValuePair_2_t2238_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0_KeyValuePair_2_t2238_0_0_0_Types };
static const Il2CppType* GenInst_Event_t189_0_0_0_Types[] = { &Event_t189_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t189_0_0_0 = { 1, GenInst_Event_t189_0_0_0_Types };
static const Il2CppType* GenInst_Event_t189_0_0_0_TextEditOp_t579_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &Event_t189_0_0_0, &TextEditOp_t579_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t189_0_0_0_TextEditOp_t579_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_Event_t189_0_0_0_TextEditOp_t579_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2252_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2252_0_0_0_Types[] = { &KeyValuePair_2_t2252_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2252_0_0_0 = { 1, GenInst_KeyValuePair_2_t2252_0_0_0_Types };
extern const Il2CppType KeySizes_t729_0_0_0;
static const Il2CppType* GenInst_KeySizes_t729_0_0_0_Types[] = { &KeySizes_t729_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t729_0_0_0 = { 1, GenInst_KeySizes_t729_0_0_0_Types };
extern const Il2CppType UInt32_t654_0_0_0;
static const Il2CppType* GenInst_UInt32_t654_0_0_0_Types[] = { &UInt32_t654_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t654_0_0_0 = { 1, GenInst_UInt32_t654_0_0_0_Types };
extern const Il2CppType IComparable_1_t3278_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3278_0_0_0_Types[] = { &IComparable_1_t3278_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3278_0_0_0 = { 1, GenInst_IComparable_1_t3278_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3283_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3283_0_0_0_Types[] = { &IEquatable_1_t3283_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3283_0_0_0 = { 1, GenInst_IEquatable_1_t3283_0_0_0_Types };
extern const Il2CppType BigInteger_t734_0_0_0;
static const Il2CppType* GenInst_BigInteger_t734_0_0_0_Types[] = { &BigInteger_t734_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t734_0_0_0 = { 1, GenInst_BigInteger_t734_0_0_0_Types };
extern const Il2CppType X509Certificate_t840_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t840_0_0_0_Types[] = { &X509Certificate_t840_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t840_0_0_0 = { 1, GenInst_X509Certificate_t840_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t1772_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t1772_0_0_0_Types[] = { &IDeserializationCallback_t1772_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t1772_0_0_0 = { 1, GenInst_IDeserializationCallback_t1772_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t844_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t844_0_0_0_Types[] = { &ClientCertificateType_t844_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t844_0_0_0 = { 1, GenInst_ClientCertificateType_t844_0_0_0_Types };
extern const Il2CppType UInt16_t652_0_0_0;
static const Il2CppType* GenInst_UInt16_t652_0_0_0_Types[] = { &UInt16_t652_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t652_0_0_0 = { 1, GenInst_UInt16_t652_0_0_0_Types };
extern const Il2CppType IComparable_1_t3306_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3306_0_0_0_Types[] = { &IComparable_1_t3306_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3306_0_0_0 = { 1, GenInst_IComparable_1_t3306_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3311_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3311_0_0_0_Types[] = { &IEquatable_1_t3311_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3311_0_0_0 = { 1, GenInst_IEquatable_1_t3311_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2289_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2289_0_0_0_Types[] = { &KeyValuePair_2_t2289_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2289_0_0_0 = { 1, GenInst_KeyValuePair_2_t2289_0_0_0_Types };
extern const Il2CppType IComparable_1_t3321_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3321_0_0_0_Types[] = { &IComparable_1_t3321_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3321_0_0_0 = { 1, GenInst_IComparable_1_t3321_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3326_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3326_0_0_0_Types[] = { &IEquatable_1_t3326_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3326_0_0_0 = { 1, GenInst_IEquatable_1_t3326_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t298_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t298_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t298_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t298_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t298_0_0_0_Boolean_t298_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t298_0_0_0, &Boolean_t298_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t298_0_0_0_Boolean_t298_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t298_0_0_0_Boolean_t298_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t298_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t298_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t298_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t298_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t298_0_0_0_KeyValuePair_2_t2289_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t298_0_0_0, &KeyValuePair_2_t2289_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t298_0_0_0_KeyValuePair_2_t2289_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t298_0_0_0_KeyValuePair_2_t2289_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t298_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t298_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t298_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t298_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2304_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2304_0_0_0_Types[] = { &KeyValuePair_2_t2304_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2304_0_0_0 = { 1, GenInst_KeyValuePair_2_t2304_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t974_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t974_0_0_0_Types[] = { &X509ChainStatus_t974_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t974_0_0_0 = { 1, GenInst_X509ChainStatus_t974_0_0_0_Types };
extern const Il2CppType Capture_t994_0_0_0;
static const Il2CppType* GenInst_Capture_t994_0_0_0_Types[] = { &Capture_t994_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t994_0_0_0 = { 1, GenInst_Capture_t994_0_0_0_Types };
extern const Il2CppType Group_t910_0_0_0;
static const Il2CppType* GenInst_Group_t910_0_0_0_Types[] = { &Group_t910_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t910_0_0_0 = { 1, GenInst_Group_t910_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t56_0_0_0_Int32_t56_0_0_0_Types[] = { &Int32_t56_0_0_0, &Int32_t56_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t56_0_0_0_Int32_t56_0_0_0 = { 2, GenInst_Int32_t56_0_0_0_Int32_t56_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2312_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2312_0_0_0_Types[] = { &KeyValuePair_2_t2312_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2312_0_0_0 = { 1, GenInst_KeyValuePair_2_t2312_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t56_0_0_0_Int32_t56_0_0_0_Int32_t56_0_0_0_Types[] = { &Int32_t56_0_0_0, &Int32_t56_0_0_0, &Int32_t56_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t56_0_0_0_Int32_t56_0_0_0_Int32_t56_0_0_0 = { 3, GenInst_Int32_t56_0_0_0_Int32_t56_0_0_0_Int32_t56_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t56_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &Int32_t56_0_0_0, &Int32_t56_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t56_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_Int32_t56_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t56_0_0_0_Int32_t56_0_0_0_KeyValuePair_2_t2312_0_0_0_Types[] = { &Int32_t56_0_0_0, &Int32_t56_0_0_0, &KeyValuePair_2_t2312_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t56_0_0_0_Int32_t56_0_0_0_KeyValuePair_2_t2312_0_0_0 = { 3, GenInst_Int32_t56_0_0_0_Int32_t56_0_0_0_KeyValuePair_2_t2312_0_0_0_Types };
extern const Il2CppType Mark_t1019_0_0_0;
static const Il2CppType* GenInst_Mark_t1019_0_0_0_Types[] = { &Mark_t1019_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t1019_0_0_0 = { 1, GenInst_Mark_t1019_0_0_0_Types };
extern const Il2CppType UriScheme_t1056_0_0_0;
static const Il2CppType* GenInst_UriScheme_t1056_0_0_0_Types[] = { &UriScheme_t1056_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t1056_0_0_0 = { 1, GenInst_UriScheme_t1056_0_0_0_Types };
extern const Il2CppType UInt64_t665_0_0_0;
static const Il2CppType* GenInst_UInt64_t665_0_0_0_Types[] = { &UInt64_t665_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t665_0_0_0 = { 1, GenInst_UInt64_t665_0_0_0_Types };
extern const Il2CppType SByte_t666_0_0_0;
static const Il2CppType* GenInst_SByte_t666_0_0_0_Types[] = { &SByte_t666_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t666_0_0_0 = { 1, GenInst_SByte_t666_0_0_0_Types };
extern const Il2CppType Int16_t667_0_0_0;
static const Il2CppType* GenInst_Int16_t667_0_0_0_Types[] = { &Int16_t667_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t667_0_0_0 = { 1, GenInst_Int16_t667_0_0_0_Types };
extern const Il2CppType Decimal_t664_0_0_0;
static const Il2CppType* GenInst_Decimal_t664_0_0_0_Types[] = { &Decimal_t664_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t664_0_0_0 = { 1, GenInst_Decimal_t664_0_0_0_Types };
extern const Il2CppType Delegate_t320_0_0_0;
static const Il2CppType* GenInst_Delegate_t320_0_0_0_Types[] = { &Delegate_t320_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t320_0_0_0 = { 1, GenInst_Delegate_t320_0_0_0_Types };
extern const Il2CppType IComparable_1_t3353_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3353_0_0_0_Types[] = { &IComparable_1_t3353_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3353_0_0_0 = { 1, GenInst_IComparable_1_t3353_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3354_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3354_0_0_0_Types[] = { &IEquatable_1_t3354_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3354_0_0_0 = { 1, GenInst_IEquatable_1_t3354_0_0_0_Types };
extern const Il2CppType IComparable_1_t3357_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3357_0_0_0_Types[] = { &IComparable_1_t3357_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3357_0_0_0 = { 1, GenInst_IComparable_1_t3357_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3358_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3358_0_0_0_Types[] = { &IEquatable_1_t3358_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3358_0_0_0 = { 1, GenInst_IEquatable_1_t3358_0_0_0_Types };
extern const Il2CppType IComparable_1_t3355_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3355_0_0_0_Types[] = { &IComparable_1_t3355_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3355_0_0_0 = { 1, GenInst_IComparable_1_t3355_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3356_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3356_0_0_0_Types[] = { &IEquatable_1_t3356_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3356_0_0_0 = { 1, GenInst_IEquatable_1_t3356_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t2702_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t2702_0_0_0_Types[] = { &_MethodInfo_t2702_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t2702_0_0_0 = { 1, GenInst__MethodInfo_t2702_0_0_0_Types };
extern const Il2CppType TableRange_t1121_0_0_0;
static const Il2CppType* GenInst_TableRange_t1121_0_0_0_Types[] = { &TableRange_t1121_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t1121_0_0_0 = { 1, GenInst_TableRange_t1121_0_0_0_Types };
extern const Il2CppType TailoringInfo_t1124_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t1124_0_0_0_Types[] = { &TailoringInfo_t1124_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1124_0_0_0 = { 1, GenInst_TailoringInfo_t1124_0_0_0_Types };
extern const Il2CppType Contraction_t1125_0_0_0;
static const Il2CppType* GenInst_Contraction_t1125_0_0_0_Types[] = { &Contraction_t1125_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t1125_0_0_0 = { 1, GenInst_Contraction_t1125_0_0_0_Types };
extern const Il2CppType Level2Map_t1127_0_0_0;
static const Il2CppType* GenInst_Level2Map_t1127_0_0_0_Types[] = { &Level2Map_t1127_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t1127_0_0_0 = { 1, GenInst_Level2Map_t1127_0_0_0_Types };
extern const Il2CppType BigInteger_t1148_0_0_0;
static const Il2CppType* GenInst_BigInteger_t1148_0_0_0_Types[] = { &BigInteger_t1148_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t1148_0_0_0 = { 1, GenInst_BigInteger_t1148_0_0_0_Types };
extern const Il2CppType Slot_t1198_0_0_0;
static const Il2CppType* GenInst_Slot_t1198_0_0_0_Types[] = { &Slot_t1198_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t1198_0_0_0 = { 1, GenInst_Slot_t1198_0_0_0_Types };
extern const Il2CppType Slot_t1205_0_0_0;
static const Il2CppType* GenInst_Slot_t1205_0_0_0_Types[] = { &Slot_t1205_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t1205_0_0_0 = { 1, GenInst_Slot_t1205_0_0_0_Types };
extern const Il2CppType StackFrame_t695_0_0_0;
static const Il2CppType* GenInst_StackFrame_t695_0_0_0_Types[] = { &StackFrame_t695_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t695_0_0_0 = { 1, GenInst_StackFrame_t695_0_0_0_Types };
extern const Il2CppType Calendar_t1219_0_0_0;
static const Il2CppType* GenInst_Calendar_t1219_0_0_0_Types[] = { &Calendar_t1219_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t1219_0_0_0 = { 1, GenInst_Calendar_t1219_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t1293_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t1293_0_0_0_Types[] = { &ModuleBuilder_t1293_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t1293_0_0_0 = { 1, GenInst_ModuleBuilder_t1293_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t2692_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t2692_0_0_0_Types[] = { &_ModuleBuilder_t2692_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t2692_0_0_0 = { 1, GenInst__ModuleBuilder_t2692_0_0_0_Types };
extern const Il2CppType Module_t1289_0_0_0;
static const Il2CppType* GenInst_Module_t1289_0_0_0_Types[] = { &Module_t1289_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t1289_0_0_0 = { 1, GenInst_Module_t1289_0_0_0_Types };
extern const Il2CppType _Module_t2703_0_0_0;
static const Il2CppType* GenInst__Module_t2703_0_0_0_Types[] = { &_Module_t2703_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t2703_0_0_0 = { 1, GenInst__Module_t2703_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t1299_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t1299_0_0_0_Types[] = { &ParameterBuilder_t1299_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t1299_0_0_0 = { 1, GenInst_ParameterBuilder_t1299_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t2693_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t2693_0_0_0_Types[] = { &_ParameterBuilder_t2693_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t2693_0_0_0 = { 1, GenInst__ParameterBuilder_t2693_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t516_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t516_0_0_0_Types[] = { &TypeU5BU5D_t516_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t516_0_0_0 = { 1, GenInst_TypeU5BU5D_t516_0_0_0_Types };
extern const Il2CppType Array_t_0_0_0;
static const Il2CppType* GenInst_Array_t_0_0_0_Types[] = { &Array_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_t_0_0_0 = { 1, GenInst_Array_t_0_0_0_Types };
extern const Il2CppType ICollection_t1069_0_0_0;
static const Il2CppType* GenInst_ICollection_t1069_0_0_0_Types[] = { &ICollection_t1069_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t1069_0_0_0 = { 1, GenInst_ICollection_t1069_0_0_0_Types };
extern const Il2CppType IList_t1028_0_0_0;
static const Il2CppType* GenInst_IList_t1028_0_0_0_Types[] = { &IList_t1028_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t1028_0_0_0 = { 1, GenInst_IList_t1028_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t1283_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t1283_0_0_0_Types[] = { &ILTokenInfo_t1283_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t1283_0_0_0 = { 1, GenInst_ILTokenInfo_t1283_0_0_0_Types };
extern const Il2CppType LabelData_t1285_0_0_0;
static const Il2CppType* GenInst_LabelData_t1285_0_0_0_Types[] = { &LabelData_t1285_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t1285_0_0_0 = { 1, GenInst_LabelData_t1285_0_0_0_Types };
extern const Il2CppType LabelFixup_t1284_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t1284_0_0_0_Types[] = { &LabelFixup_t1284_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t1284_0_0_0 = { 1, GenInst_LabelFixup_t1284_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t1281_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t1281_0_0_0_Types[] = { &GenericTypeParameterBuilder_t1281_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1281_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1281_0_0_0_Types };
extern const Il2CppType TypeBuilder_t1275_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t1275_0_0_0_Types[] = { &TypeBuilder_t1275_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t1275_0_0_0 = { 1, GenInst_TypeBuilder_t1275_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t2695_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t2695_0_0_0_Types[] = { &_TypeBuilder_t2695_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t2695_0_0_0 = { 1, GenInst__TypeBuilder_t2695_0_0_0_Types };
extern const Il2CppType MethodBuilder_t1282_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t1282_0_0_0_Types[] = { &MethodBuilder_t1282_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t1282_0_0_0 = { 1, GenInst_MethodBuilder_t1282_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t2691_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t2691_0_0_0_Types[] = { &_MethodBuilder_t2691_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t2691_0_0_0 = { 1, GenInst__MethodBuilder_t2691_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t1273_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t1273_0_0_0_Types[] = { &ConstructorBuilder_t1273_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t1273_0_0_0 = { 1, GenInst_ConstructorBuilder_t1273_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t2687_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t2687_0_0_0_Types[] = { &_ConstructorBuilder_t2687_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t2687_0_0_0 = { 1, GenInst__ConstructorBuilder_t2687_0_0_0_Types };
extern const Il2CppType PropertyBuilder_t1300_0_0_0;
static const Il2CppType* GenInst_PropertyBuilder_t1300_0_0_0_Types[] = { &PropertyBuilder_t1300_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyBuilder_t1300_0_0_0 = { 1, GenInst_PropertyBuilder_t1300_0_0_0_Types };
extern const Il2CppType _PropertyBuilder_t2694_0_0_0;
static const Il2CppType* GenInst__PropertyBuilder_t2694_0_0_0_Types[] = { &_PropertyBuilder_t2694_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyBuilder_t2694_0_0_0 = { 1, GenInst__PropertyBuilder_t2694_0_0_0_Types };
extern const Il2CppType FieldBuilder_t1279_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t1279_0_0_0_Types[] = { &FieldBuilder_t1279_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t1279_0_0_0 = { 1, GenInst_FieldBuilder_t1279_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t2689_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t2689_0_0_0_Types[] = { &_FieldBuilder_t2689_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t2689_0_0_0 = { 1, GenInst__FieldBuilder_t2689_0_0_0_Types };
extern const Il2CppType IContextProperty_t1737_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t1737_0_0_0_Types[] = { &IContextProperty_t1737_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t1737_0_0_0 = { 1, GenInst_IContextProperty_t1737_0_0_0_Types };
extern const Il2CppType Header_t1428_0_0_0;
static const Il2CppType* GenInst_Header_t1428_0_0_0_Types[] = { &Header_t1428_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t1428_0_0_0 = { 1, GenInst_Header_t1428_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t1764_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t1764_0_0_0_Types[] = { &ITrackingHandler_t1764_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t1764_0_0_0 = { 1, GenInst_ITrackingHandler_t1764_0_0_0_Types };
extern const Il2CppType IContextAttribute_t1751_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t1751_0_0_0_Types[] = { &IContextAttribute_t1751_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t1751_0_0_0 = { 1, GenInst_IContextAttribute_t1751_0_0_0_Types };
extern const Il2CppType IComparable_1_t3552_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3552_0_0_0_Types[] = { &IComparable_1_t3552_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3552_0_0_0 = { 1, GenInst_IComparable_1_t3552_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3557_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3557_0_0_0_Types[] = { &IEquatable_1_t3557_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3557_0_0_0 = { 1, GenInst_IEquatable_1_t3557_0_0_0_Types };
extern const Il2CppType IComparable_1_t3359_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3359_0_0_0_Types[] = { &IComparable_1_t3359_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3359_0_0_0 = { 1, GenInst_IComparable_1_t3359_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3360_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3360_0_0_0_Types[] = { &IEquatable_1_t3360_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3360_0_0_0 = { 1, GenInst_IEquatable_1_t3360_0_0_0_Types };
extern const Il2CppType IComparable_1_t3576_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3576_0_0_0_Types[] = { &IComparable_1_t3576_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3576_0_0_0 = { 1, GenInst_IComparable_1_t3576_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3581_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3581_0_0_0_Types[] = { &IEquatable_1_t3581_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3581_0_0_0 = { 1, GenInst_IEquatable_1_t3581_0_0_0_Types };
extern const Il2CppType TypeTag_t1484_0_0_0;
static const Il2CppType* GenInst_TypeTag_t1484_0_0_0_Types[] = { &TypeTag_t1484_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t1484_0_0_0 = { 1, GenInst_TypeTag_t1484_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType Version_t945_0_0_0;
static const Il2CppType* GenInst_Version_t945_0_0_0_Types[] = { &Version_t945_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t945_0_0_0 = { 1, GenInst_Version_t945_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t35_0_0_0_Vector3_t35_0_0_0_Types[] = { &Vector3_t35_0_0_0, &Vector3_t35_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t35_0_0_0_Vector3_t35_0_0_0 = { 2, GenInst_Vector3_t35_0_0_0_Vector3_t35_0_0_0_Types };
static const Il2CppType* GenInst_RaycastResult_t103_0_0_0_RaycastResult_t103_0_0_0_Types[] = { &RaycastResult_t103_0_0_0, &RaycastResult_t103_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t103_0_0_0_RaycastResult_t103_0_0_0 = { 2, GenInst_RaycastResult_t103_0_0_0_RaycastResult_t103_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t1068_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &DictionaryEntry_t1068_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1068_0_0_0_DictionaryEntry_t1068_0_0_0 = { 2, GenInst_DictionaryEntry_t1068_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1877_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1877_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1877_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1877_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1877_0_0_0_KeyValuePair_2_t1877_0_0_0_Types[] = { &KeyValuePair_2_t1877_0_0_0, &KeyValuePair_2_t1877_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1877_0_0_0_KeyValuePair_2_t1877_0_0_0 = { 2, GenInst_KeyValuePair_2_t1877_0_0_0_KeyValuePair_2_t1877_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1908_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1908_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1908_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1908_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1908_0_0_0_KeyValuePair_2_t1908_0_0_0_Types[] = { &KeyValuePair_2_t1908_0_0_0, &KeyValuePair_2_t1908_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1908_0_0_0_KeyValuePair_2_t1908_0_0_0 = { 2, GenInst_KeyValuePair_2_t1908_0_0_0_KeyValuePair_2_t1908_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1934_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1934_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1934_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1934_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1934_0_0_0_KeyValuePair_2_t1934_0_0_0_Types[] = { &KeyValuePair_2_t1934_0_0_0, &KeyValuePair_2_t1934_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1934_0_0_0_KeyValuePair_2_t1934_0_0_0 = { 2, GenInst_KeyValuePair_2_t1934_0_0_0_KeyValuePair_2_t1934_0_0_0_Types };
static const Il2CppType* GenInst_UIVertex_t191_0_0_0_UIVertex_t191_0_0_0_Types[] = { &UIVertex_t191_0_0_0, &UIVertex_t191_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t191_0_0_0_UIVertex_t191_0_0_0 = { 2, GenInst_UIVertex_t191_0_0_0_UIVertex_t191_0_0_0_Types };
static const Il2CppType* GenInst_UICharInfo_t333_0_0_0_UICharInfo_t333_0_0_0_Types[] = { &UICharInfo_t333_0_0_0, &UICharInfo_t333_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t333_0_0_0_UICharInfo_t333_0_0_0 = { 2, GenInst_UICharInfo_t333_0_0_0_UICharInfo_t333_0_0_0_Types };
static const Il2CppType* GenInst_UILineInfo_t331_0_0_0_UILineInfo_t331_0_0_0_Types[] = { &UILineInfo_t331_0_0_0, &UILineInfo_t331_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t331_0_0_0_UILineInfo_t331_0_0_0 = { 2, GenInst_UILineInfo_t331_0_0_0_UILineInfo_t331_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t662_0_0_0_Object_t_0_0_0_Types[] = { &Int64_t662_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t662_0_0_0_Object_t_0_0_0 = { 2, GenInst_Int64_t662_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Int64_t662_0_0_0_Int64_t662_0_0_0_Types[] = { &Int64_t662_0_0_0, &Int64_t662_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t662_0_0_0_Int64_t662_0_0_0 = { 2, GenInst_Int64_t662_0_0_0_Int64_t662_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2118_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2118_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2118_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2118_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2118_0_0_0_KeyValuePair_2_t2118_0_0_0_Types[] = { &KeyValuePair_2_t2118_0_0_0, &KeyValuePair_2_t2118_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2118_0_0_0_KeyValuePair_2_t2118_0_0_0 = { 2, GenInst_KeyValuePair_2_t2118_0_0_0_KeyValuePair_2_t2118_0_0_0_Types };
static const Il2CppType* GenInst_NetworkID_t502_0_0_0_NetworkID_t502_0_0_0_Types[] = { &NetworkID_t502_0_0_0, &NetworkID_t502_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkID_t502_0_0_0_NetworkID_t502_0_0_0 = { 2, GenInst_NetworkID_t502_0_0_0_NetworkID_t502_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2155_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2155_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2155_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2155_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2155_0_0_0_KeyValuePair_2_t2155_0_0_0_Types[] = { &KeyValuePair_2_t2155_0_0_0, &KeyValuePair_2_t2155_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2155_0_0_0_KeyValuePair_2_t2155_0_0_0 = { 2, GenInst_KeyValuePair_2_t2155_0_0_0_KeyValuePair_2_t2155_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2175_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2175_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2175_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2175_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2175_0_0_0_KeyValuePair_2_t2175_0_0_0_Types[] = { &KeyValuePair_2_t2175_0_0_0, &KeyValuePair_2_t2175_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2175_0_0_0_KeyValuePair_2_t2175_0_0_0 = { 2, GenInst_KeyValuePair_2_t2175_0_0_0_KeyValuePair_2_t2175_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t579_0_0_0_Object_t_0_0_0_Types[] = { &TextEditOp_t579_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t579_0_0_0_Object_t_0_0_0 = { 2, GenInst_TextEditOp_t579_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t579_0_0_0_TextEditOp_t579_0_0_0_Types[] = { &TextEditOp_t579_0_0_0, &TextEditOp_t579_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t579_0_0_0_TextEditOp_t579_0_0_0 = { 2, GenInst_TextEditOp_t579_0_0_0_TextEditOp_t579_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2238_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2238_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2238_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2238_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2238_0_0_0_KeyValuePair_2_t2238_0_0_0_Types[] = { &KeyValuePair_2_t2238_0_0_0, &KeyValuePair_2_t2238_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2238_0_0_0_KeyValuePair_2_t2238_0_0_0 = { 2, GenInst_KeyValuePair_2_t2238_0_0_0_KeyValuePair_2_t2238_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t298_0_0_0_Object_t_0_0_0_Types[] = { &Boolean_t298_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t298_0_0_0_Object_t_0_0_0 = { 2, GenInst_Boolean_t298_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t298_0_0_0_Boolean_t298_0_0_0_Types[] = { &Boolean_t298_0_0_0, &Boolean_t298_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t298_0_0_0_Boolean_t298_0_0_0 = { 2, GenInst_Boolean_t298_0_0_0_Boolean_t298_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2289_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2289_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2289_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2289_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2289_0_0_0_KeyValuePair_2_t2289_0_0_0_Types[] = { &KeyValuePair_2_t2289_0_0_0, &KeyValuePair_2_t2289_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2289_0_0_0_KeyValuePair_2_t2289_0_0_0 = { 2, GenInst_KeyValuePair_2_t2289_0_0_0_KeyValuePair_2_t2289_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2312_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2312_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2312_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2312_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2312_0_0_0_KeyValuePair_2_t2312_0_0_0_Types[] = { &KeyValuePair_2_t2312_0_0_0, &KeyValuePair_2_t2312_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2312_0_0_0_KeyValuePair_2_t2312_0_0_0 = { 2, GenInst_KeyValuePair_2_t2312_0_0_0_KeyValuePair_2_t2312_0_0_0_Types };
extern const Il2CppType ExecuteEvents_Execute_m18707_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_Execute_m18707_gp_0_0_0_0_Types[] = { &ExecuteEvents_Execute_m18707_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m18707_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m18707_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m18708_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_ExecuteHierarchy_m18708_gp_0_0_0_0_Types[] = { &ExecuteEvents_ExecuteHierarchy_m18708_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m18708_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m18708_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventList_m18710_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventList_m18710_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventList_m18710_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m18710_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m18710_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_CanHandleEvent_m18711_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_CanHandleEvent_m18711_gp_0_0_0_0_Types[] = { &ExecuteEvents_CanHandleEvent_m18711_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m18711_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m18711_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventHandler_m18712_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventHandler_m18712_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventHandler_m18712_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m18712_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m18712_gp_0_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t2618_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t2618_gp_0_0_0_0_Types[] = { &TweenRunner_1_t2618_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t2618_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t2618_gp_0_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t2620_gp_0_0_0_0;
static const Il2CppType* GenInst_IndexedSet_1_t2620_gp_0_0_0_0_Types[] = { &IndexedSet_1_t2620_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t2620_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t2620_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t2620_gp_0_0_0_0_Int32_t56_0_0_0_Types[] = { &IndexedSet_1_t2620_gp_0_0_0_0, &Int32_t56_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t2620_gp_0_0_0_0_Int32_t56_0_0_0 = { 2, GenInst_IndexedSet_1_t2620_gp_0_0_0_0_Int32_t56_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t2621_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t2621_gp_0_0_0_0_Types[] = { &ObjectPool_1_t2621_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t2621_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t2621_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m18790_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m18790_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m18790_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m18790_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m18790_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m18791_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m18791_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m18791_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m18791_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m18791_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m18792_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m18792_gp_0_0_0_0_Types[] = { &Component_GetComponents_m18792_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m18792_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m18792_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponents_m18794_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponents_m18794_gp_0_0_0_0_Types[] = { &GameObject_GetComponents_m18794_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m18794_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m18794_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m18795_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m18795_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m18795_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m18795_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m18795_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m18796_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m18796_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m18796_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m18796_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m18796_gp_0_0_0_0_Types };
extern const Il2CppType ResponseBase_ParseJSONList_m18800_gp_0_0_0_0;
static const Il2CppType* GenInst_ResponseBase_ParseJSONList_m18800_gp_0_0_0_0_Types[] = { &ResponseBase_ParseJSONList_m18800_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ResponseBase_ParseJSONList_m18800_gp_0_0_0_0 = { 1, GenInst_ResponseBase_ParseJSONList_m18800_gp_0_0_0_0_Types };
extern const Il2CppType NetworkMatch_ProcessMatchResponse_m18801_gp_0_0_0_0;
static const Il2CppType* GenInst_NetworkMatch_ProcessMatchResponse_m18801_gp_0_0_0_0_Types[] = { &NetworkMatch_ProcessMatchResponse_m18801_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_NetworkMatch_ProcessMatchResponse_m18801_gp_0_0_0_0 = { 1, GenInst_NetworkMatch_ProcessMatchResponse_m18801_gp_0_0_0_0_Types };
extern const Il2CppType U3CProcessMatchResponseU3Ec__Iterator0_1_t2625_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CProcessMatchResponseU3Ec__Iterator0_1_t2625_gp_0_0_0_0_Types[] = { &U3CProcessMatchResponseU3Ec__Iterator0_1_t2625_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CProcessMatchResponseU3Ec__Iterator0_1_t2625_gp_0_0_0_0 = { 1, GenInst_U3CProcessMatchResponseU3Ec__Iterator0_1_t2625_gp_0_0_0_0_Types };
extern const Il2CppType ThreadSafeDictionary_2_t2626_gp_0_0_0_0;
extern const Il2CppType ThreadSafeDictionary_2_t2626_gp_1_0_0_0;
static const Il2CppType* GenInst_ThreadSafeDictionary_2_t2626_gp_0_0_0_0_ThreadSafeDictionary_2_t2626_gp_1_0_0_0_Types[] = { &ThreadSafeDictionary_2_t2626_gp_0_0_0_0, &ThreadSafeDictionary_2_t2626_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t2626_gp_0_0_0_0_ThreadSafeDictionary_2_t2626_gp_1_0_0_0 = { 2, GenInst_ThreadSafeDictionary_2_t2626_gp_0_0_0_0_ThreadSafeDictionary_2_t2626_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ThreadSafeDictionary_2_t2626_gp_0_0_0_0_Types[] = { &ThreadSafeDictionary_2_t2626_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t2626_gp_0_0_0_0 = { 1, GenInst_ThreadSafeDictionary_2_t2626_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_ThreadSafeDictionary_2_t2626_gp_1_0_0_0_Types[] = { &ThreadSafeDictionary_2_t2626_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ThreadSafeDictionary_2_t2626_gp_1_0_0_0 = { 1, GenInst_ThreadSafeDictionary_2_t2626_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3640_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3640_0_0_0_Types[] = { &KeyValuePair_2_t3640_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3640_0_0_0 = { 1, GenInst_KeyValuePair_2_t3640_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t2632_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t2632_gp_0_0_0_0_Types[] = { &InvokableCall_1_t2632_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t2632_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t2632_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t2633_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t2633_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t2633_gp_0_0_0_0_InvokableCall_2_t2633_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2633_gp_0_0_0_0, &InvokableCall_2_t2633_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2633_gp_0_0_0_0_InvokableCall_2_t2633_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t2633_gp_0_0_0_0_InvokableCall_2_t2633_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2633_gp_0_0_0_0_Types[] = { &InvokableCall_2_t2633_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2633_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t2633_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2633_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2633_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2633_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t2633_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t2634_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t2634_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t2634_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t2634_gp_0_0_0_0_InvokableCall_3_t2634_gp_1_0_0_0_InvokableCall_3_t2634_gp_2_0_0_0_Types[] = { &InvokableCall_3_t2634_gp_0_0_0_0, &InvokableCall_3_t2634_gp_1_0_0_0, &InvokableCall_3_t2634_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t2634_gp_0_0_0_0_InvokableCall_3_t2634_gp_1_0_0_0_InvokableCall_3_t2634_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t2634_gp_0_0_0_0_InvokableCall_3_t2634_gp_1_0_0_0_InvokableCall_3_t2634_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t2634_gp_0_0_0_0_Types[] = { &InvokableCall_3_t2634_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t2634_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t2634_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t2634_gp_1_0_0_0_Types[] = { &InvokableCall_3_t2634_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t2634_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t2634_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t2634_gp_2_0_0_0_Types[] = { &InvokableCall_3_t2634_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t2634_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t2634_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t2635_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t2635_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t2635_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t2635_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t2635_gp_0_0_0_0_InvokableCall_4_t2635_gp_1_0_0_0_InvokableCall_4_t2635_gp_2_0_0_0_InvokableCall_4_t2635_gp_3_0_0_0_Types[] = { &InvokableCall_4_t2635_gp_0_0_0_0, &InvokableCall_4_t2635_gp_1_0_0_0, &InvokableCall_4_t2635_gp_2_0_0_0, &InvokableCall_4_t2635_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2635_gp_0_0_0_0_InvokableCall_4_t2635_gp_1_0_0_0_InvokableCall_4_t2635_gp_2_0_0_0_InvokableCall_4_t2635_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t2635_gp_0_0_0_0_InvokableCall_4_t2635_gp_1_0_0_0_InvokableCall_4_t2635_gp_2_0_0_0_InvokableCall_4_t2635_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t2635_gp_0_0_0_0_Types[] = { &InvokableCall_4_t2635_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2635_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t2635_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t2635_gp_1_0_0_0_Types[] = { &InvokableCall_4_t2635_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2635_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t2635_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t2635_gp_2_0_0_0_Types[] = { &InvokableCall_4_t2635_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2635_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t2635_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t2635_gp_3_0_0_0_Types[] = { &InvokableCall_4_t2635_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2635_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t2635_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t702_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t702_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t702_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t702_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t702_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t2636_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t2636_gp_0_0_0_0_Types[] = { &UnityEvent_1_t2636_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t2636_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t2636_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t2637_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t2637_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t2637_gp_0_0_0_0_UnityEvent_2_t2637_gp_1_0_0_0_Types[] = { &UnityEvent_2_t2637_gp_0_0_0_0, &UnityEvent_2_t2637_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t2637_gp_0_0_0_0_UnityEvent_2_t2637_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t2637_gp_0_0_0_0_UnityEvent_2_t2637_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t2638_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t2638_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t2638_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t2638_gp_0_0_0_0_UnityEvent_3_t2638_gp_1_0_0_0_UnityEvent_3_t2638_gp_2_0_0_0_Types[] = { &UnityEvent_3_t2638_gp_0_0_0_0, &UnityEvent_3_t2638_gp_1_0_0_0, &UnityEvent_3_t2638_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t2638_gp_0_0_0_0_UnityEvent_3_t2638_gp_1_0_0_0_UnityEvent_3_t2638_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t2638_gp_0_0_0_0_UnityEvent_3_t2638_gp_1_0_0_0_UnityEvent_3_t2638_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t2639_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t2639_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t2639_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t2639_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t2639_gp_0_0_0_0_UnityEvent_4_t2639_gp_1_0_0_0_UnityEvent_4_t2639_gp_2_0_0_0_UnityEvent_4_t2639_gp_3_0_0_0_Types[] = { &UnityEvent_4_t2639_gp_0_0_0_0, &UnityEvent_4_t2639_gp_1_0_0_0, &UnityEvent_4_t2639_gp_2_0_0_0, &UnityEvent_4_t2639_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t2639_gp_0_0_0_0_UnityEvent_4_t2639_gp_1_0_0_0_UnityEvent_4_t2639_gp_2_0_0_0_UnityEvent_4_t2639_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t2639_gp_0_0_0_0_UnityEvent_4_t2639_gp_1_0_0_0_UnityEvent_4_t2639_gp_2_0_0_0_UnityEvent_4_t2639_gp_3_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m18894_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m18894_gp_0_0_0_0_Types[] = { &Enumerable_Where_m18894_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m18894_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m18894_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m18894_gp_0_0_0_0_Boolean_t298_0_0_0_Types[] = { &Enumerable_Where_m18894_gp_0_0_0_0, &Boolean_t298_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m18894_gp_0_0_0_0_Boolean_t298_0_0_0 = { 2, GenInst_Enumerable_Where_m18894_gp_0_0_0_0_Boolean_t298_0_0_0_Types };
extern const Il2CppType Enumerable_CreateWhereIterator_m18895_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m18895_gp_0_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m18895_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m18895_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m18895_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m18895_gp_0_0_0_0_Boolean_t298_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m18895_gp_0_0_0_0, &Boolean_t298_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m18895_gp_0_0_0_0_Boolean_t298_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m18895_gp_0_0_0_0_Boolean_t298_0_0_0_Types };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2644_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2644_gp_0_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2644_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2644_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2644_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2644_gp_0_0_0_0_Boolean_t298_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2644_gp_0_0_0_0, &Boolean_t298_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2644_gp_0_0_0_0_Boolean_t298_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2644_gp_0_0_0_0_Boolean_t298_0_0_0_Types };
extern const Il2CppType Stack_1_t2646_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t2646_gp_0_0_0_0_Types[] = { &Stack_1_t2646_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t2646_gp_0_0_0_0 = { 1, GenInst_Stack_1_t2646_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2647_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2647_gp_0_0_0_0_Types[] = { &Enumerator_t2647_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2647_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2647_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2654_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2654_gp_0_0_0_0_Types[] = { &IEnumerable_1_t2654_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2654_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t2654_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m19013_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m19013_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m19013_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m19013_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m19013_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m19025_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19025_gp_0_0_0_0_Array_Sort_m19025_gp_0_0_0_0_Types[] = { &Array_Sort_m19025_gp_0_0_0_0, &Array_Sort_m19025_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19025_gp_0_0_0_0_Array_Sort_m19025_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m19025_gp_0_0_0_0_Array_Sort_m19025_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m19026_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m19026_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19026_gp_0_0_0_0_Array_Sort_m19026_gp_1_0_0_0_Types[] = { &Array_Sort_m19026_gp_0_0_0_0, &Array_Sort_m19026_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19026_gp_0_0_0_0_Array_Sort_m19026_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m19026_gp_0_0_0_0_Array_Sort_m19026_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m19027_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19027_gp_0_0_0_0_Types[] = { &Array_Sort_m19027_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19027_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m19027_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m19027_gp_0_0_0_0_Array_Sort_m19027_gp_0_0_0_0_Types[] = { &Array_Sort_m19027_gp_0_0_0_0, &Array_Sort_m19027_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19027_gp_0_0_0_0_Array_Sort_m19027_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m19027_gp_0_0_0_0_Array_Sort_m19027_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m19028_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19028_gp_0_0_0_0_Types[] = { &Array_Sort_m19028_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19028_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m19028_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m19028_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19028_gp_0_0_0_0_Array_Sort_m19028_gp_1_0_0_0_Types[] = { &Array_Sort_m19028_gp_0_0_0_0, &Array_Sort_m19028_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19028_gp_0_0_0_0_Array_Sort_m19028_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m19028_gp_0_0_0_0_Array_Sort_m19028_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m19029_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19029_gp_0_0_0_0_Array_Sort_m19029_gp_0_0_0_0_Types[] = { &Array_Sort_m19029_gp_0_0_0_0, &Array_Sort_m19029_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19029_gp_0_0_0_0_Array_Sort_m19029_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m19029_gp_0_0_0_0_Array_Sort_m19029_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m19030_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m19030_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19030_gp_0_0_0_0_Array_Sort_m19030_gp_1_0_0_0_Types[] = { &Array_Sort_m19030_gp_0_0_0_0, &Array_Sort_m19030_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19030_gp_0_0_0_0_Array_Sort_m19030_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m19030_gp_0_0_0_0_Array_Sort_m19030_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m19031_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19031_gp_0_0_0_0_Types[] = { &Array_Sort_m19031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19031_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m19031_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m19031_gp_0_0_0_0_Array_Sort_m19031_gp_0_0_0_0_Types[] = { &Array_Sort_m19031_gp_0_0_0_0, &Array_Sort_m19031_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19031_gp_0_0_0_0_Array_Sort_m19031_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m19031_gp_0_0_0_0_Array_Sort_m19031_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m19032_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19032_gp_0_0_0_0_Types[] = { &Array_Sort_m19032_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19032_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m19032_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m19032_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19032_gp_1_0_0_0_Types[] = { &Array_Sort_m19032_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19032_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m19032_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m19032_gp_0_0_0_0_Array_Sort_m19032_gp_1_0_0_0_Types[] = { &Array_Sort_m19032_gp_0_0_0_0, &Array_Sort_m19032_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19032_gp_0_0_0_0_Array_Sort_m19032_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m19032_gp_0_0_0_0_Array_Sort_m19032_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m19033_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19033_gp_0_0_0_0_Types[] = { &Array_Sort_m19033_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19033_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m19033_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m19034_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19034_gp_0_0_0_0_Types[] = { &Array_Sort_m19034_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19034_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m19034_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m19035_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m19035_gp_0_0_0_0_Types[] = { &Array_qsort_m19035_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m19035_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m19035_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m19035_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m19035_gp_0_0_0_0_Array_qsort_m19035_gp_1_0_0_0_Types[] = { &Array_qsort_m19035_gp_0_0_0_0, &Array_qsort_m19035_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m19035_gp_0_0_0_0_Array_qsort_m19035_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m19035_gp_0_0_0_0_Array_qsort_m19035_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m19036_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m19036_gp_0_0_0_0_Types[] = { &Array_compare_m19036_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m19036_gp_0_0_0_0 = { 1, GenInst_Array_compare_m19036_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m19037_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m19037_gp_0_0_0_0_Types[] = { &Array_qsort_m19037_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m19037_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m19037_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m19040_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m19040_gp_0_0_0_0_Types[] = { &Array_Resize_m19040_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m19040_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m19040_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m19042_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m19042_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m19042_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m19042_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m19042_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m19043_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m19043_gp_0_0_0_0_Types[] = { &Array_ForEach_m19043_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m19043_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m19043_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m19044_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m19044_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m19044_gp_0_0_0_0_Array_ConvertAll_m19044_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m19044_gp_0_0_0_0, &Array_ConvertAll_m19044_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m19044_gp_0_0_0_0_Array_ConvertAll_m19044_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m19044_gp_0_0_0_0_Array_ConvertAll_m19044_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m19045_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m19045_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m19045_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m19045_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m19045_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m19046_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m19046_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m19046_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m19046_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m19046_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m19047_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m19047_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m19047_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m19047_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m19047_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m19048_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m19048_gp_0_0_0_0_Types[] = { &Array_FindIndex_m19048_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m19048_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m19048_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m19049_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m19049_gp_0_0_0_0_Types[] = { &Array_FindIndex_m19049_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m19049_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m19049_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m19050_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m19050_gp_0_0_0_0_Types[] = { &Array_FindIndex_m19050_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m19050_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m19050_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m19051_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m19051_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m19051_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m19051_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m19051_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m19052_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m19052_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m19052_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m19052_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m19052_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m19053_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m19053_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m19053_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m19053_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m19053_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m19054_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m19054_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m19054_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m19054_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m19054_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m19055_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m19055_gp_0_0_0_0_Types[] = { &Array_IndexOf_m19055_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m19055_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m19055_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m19056_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m19056_gp_0_0_0_0_Types[] = { &Array_IndexOf_m19056_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m19056_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m19056_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m19057_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m19057_gp_0_0_0_0_Types[] = { &Array_IndexOf_m19057_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m19057_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m19057_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m19058_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m19058_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m19058_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m19058_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m19058_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m19059_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m19059_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m19059_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m19059_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m19059_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m19060_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m19060_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m19060_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m19060_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m19060_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m19061_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m19061_gp_0_0_0_0_Types[] = { &Array_FindAll_m19061_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m19061_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m19061_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m19062_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m19062_gp_0_0_0_0_Types[] = { &Array_Exists_m19062_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m19062_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m19062_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m19063_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m19063_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m19063_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m19063_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m19063_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m19064_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m19064_gp_0_0_0_0_Types[] = { &Array_Find_m19064_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m19064_gp_0_0_0_0 = { 1, GenInst_Array_Find_m19064_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m19065_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m19065_gp_0_0_0_0_Types[] = { &Array_FindLast_m19065_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m19065_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m19065_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t2655_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t2655_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t2655_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t2655_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t2655_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t2656_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t2656_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t2656_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t2656_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t2656_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t2657_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2657_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t2657_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2657_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2657_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t2658_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t2658_gp_0_0_0_0_Types[] = { &IList_1_t2658_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2658_gp_0_0_0_0 = { 1, GenInst_IList_1_t2658_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t2659_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t2659_gp_0_0_0_0_Types[] = { &ICollection_1_t2659_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t2659_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t2659_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t1748_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t1748_gp_0_0_0_0_Types[] = { &Nullable_1_t1748_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t1748_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1748_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t2666_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t2666_gp_0_0_0_0_Types[] = { &Comparer_1_t2666_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t2666_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t2666_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t2667_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t2667_gp_0_0_0_0_Types[] = { &DefaultComparer_t2667_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t2667_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t2667_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t2615_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t2615_gp_0_0_0_0_Types[] = { &GenericComparer_1_t2615_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t2615_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t2615_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2668_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2668_gp_0_0_0_0_Types[] = { &Dictionary_2_t2668_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2668_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t2668_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2668_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0_Types[] = { &Dictionary_2_t2668_gp_0_0_0_0, &Dictionary_2_t2668_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2668_gp_1_0_0_0_Types[] = { &Dictionary_2_t2668_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2668_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t2668_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3732_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3732_0_0_0_Types[] = { &KeyValuePair_2_t3732_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3732_0_0_0 = { 1, GenInst_KeyValuePair_2_t3732_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m19217_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m19217_gp_0_0_0_0_Types[] = { &Dictionary_2_t2668_gp_0_0_0_0, &Dictionary_2_t2668_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m19217_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m19217_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m19217_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m19222_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m19222_gp_0_0_0_0_Types[] = { &Dictionary_2_t2668_gp_0_0_0_0, &Dictionary_2_t2668_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m19222_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m19222_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m19222_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m19222_gp_0_0_0_0_Object_t_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m19222_gp_0_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m19222_gp_0_0_0_0_Object_t_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m19222_gp_0_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0_DictionaryEntry_t1068_0_0_0_Types[] = { &Dictionary_2_t2668_gp_0_0_0_0, &Dictionary_2_t2668_gp_1_0_0_0, &DictionaryEntry_t1068_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0_DictionaryEntry_t1068_0_0_0 = { 3, GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0_DictionaryEntry_t1068_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t2669_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t2669_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t2669_gp_0_0_0_0_ShimEnumerator_t2669_gp_1_0_0_0_Types[] = { &ShimEnumerator_t2669_gp_0_0_0_0, &ShimEnumerator_t2669_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t2669_gp_0_0_0_0_ShimEnumerator_t2669_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t2669_gp_0_0_0_0_ShimEnumerator_t2669_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t2670_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2670_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2670_gp_0_0_0_0_Enumerator_t2670_gp_1_0_0_0_Types[] = { &Enumerator_t2670_gp_0_0_0_0, &Enumerator_t2670_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2670_gp_0_0_0_0_Enumerator_t2670_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2670_gp_0_0_0_0_Enumerator_t2670_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3745_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3745_0_0_0_Types[] = { &KeyValuePair_2_t3745_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3745_0_0_0 = { 1, GenInst_KeyValuePair_2_t3745_0_0_0_Types };
extern const Il2CppType KeyCollection_t2671_gp_0_0_0_0;
extern const Il2CppType KeyCollection_t2671_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyCollection_t2671_gp_0_0_0_0_KeyCollection_t2671_gp_1_0_0_0_Types[] = { &KeyCollection_t2671_gp_0_0_0_0, &KeyCollection_t2671_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2671_gp_0_0_0_0_KeyCollection_t2671_gp_1_0_0_0 = { 2, GenInst_KeyCollection_t2671_gp_0_0_0_0_KeyCollection_t2671_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t2671_gp_0_0_0_0_Types[] = { &KeyCollection_t2671_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2671_gp_0_0_0_0 = { 1, GenInst_KeyCollection_t2671_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2672_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2672_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2672_gp_0_0_0_0_Enumerator_t2672_gp_1_0_0_0_Types[] = { &Enumerator_t2672_gp_0_0_0_0, &Enumerator_t2672_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2672_gp_0_0_0_0_Enumerator_t2672_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2672_gp_0_0_0_0_Enumerator_t2672_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t2672_gp_0_0_0_0_Types[] = { &Enumerator_t2672_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2672_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2672_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t2671_gp_0_0_0_0_KeyCollection_t2671_gp_1_0_0_0_KeyCollection_t2671_gp_0_0_0_0_Types[] = { &KeyCollection_t2671_gp_0_0_0_0, &KeyCollection_t2671_gp_1_0_0_0, &KeyCollection_t2671_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2671_gp_0_0_0_0_KeyCollection_t2671_gp_1_0_0_0_KeyCollection_t2671_gp_0_0_0_0 = { 3, GenInst_KeyCollection_t2671_gp_0_0_0_0_KeyCollection_t2671_gp_1_0_0_0_KeyCollection_t2671_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_KeyCollection_t2671_gp_0_0_0_0_KeyCollection_t2671_gp_0_0_0_0_Types[] = { &KeyCollection_t2671_gp_0_0_0_0, &KeyCollection_t2671_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyCollection_t2671_gp_0_0_0_0_KeyCollection_t2671_gp_0_0_0_0 = { 2, GenInst_KeyCollection_t2671_gp_0_0_0_0_KeyCollection_t2671_gp_0_0_0_0_Types };
extern const Il2CppType ValueCollection_t2673_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2673_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t2673_gp_0_0_0_0_ValueCollection_t2673_gp_1_0_0_0_Types[] = { &ValueCollection_t2673_gp_0_0_0_0, &ValueCollection_t2673_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2673_gp_0_0_0_0_ValueCollection_t2673_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2673_gp_0_0_0_0_ValueCollection_t2673_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2673_gp_1_0_0_0_Types[] = { &ValueCollection_t2673_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2673_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2673_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t2674_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2674_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2674_gp_0_0_0_0_Enumerator_t2674_gp_1_0_0_0_Types[] = { &Enumerator_t2674_gp_0_0_0_0, &Enumerator_t2674_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2674_gp_0_0_0_0_Enumerator_t2674_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2674_gp_0_0_0_0_Enumerator_t2674_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t2674_gp_1_0_0_0_Types[] = { &Enumerator_t2674_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2674_gp_1_0_0_0 = { 1, GenInst_Enumerator_t2674_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2673_gp_0_0_0_0_ValueCollection_t2673_gp_1_0_0_0_ValueCollection_t2673_gp_1_0_0_0_Types[] = { &ValueCollection_t2673_gp_0_0_0_0, &ValueCollection_t2673_gp_1_0_0_0, &ValueCollection_t2673_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2673_gp_0_0_0_0_ValueCollection_t2673_gp_1_0_0_0_ValueCollection_t2673_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t2673_gp_0_0_0_0_ValueCollection_t2673_gp_1_0_0_0_ValueCollection_t2673_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2673_gp_1_0_0_0_ValueCollection_t2673_gp_1_0_0_0_Types[] = { &ValueCollection_t2673_gp_1_0_0_0, &ValueCollection_t2673_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2673_gp_1_0_0_0_ValueCollection_t2673_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2673_gp_1_0_0_0_ValueCollection_t2673_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0_KeyValuePair_2_t3732_0_0_0_Types[] = { &Dictionary_2_t2668_gp_0_0_0_0, &Dictionary_2_t2668_gp_1_0_0_0, &KeyValuePair_2_t3732_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0_KeyValuePair_2_t3732_0_0_0 = { 3, GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0_KeyValuePair_2_t3732_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3732_0_0_0_KeyValuePair_2_t3732_0_0_0_Types[] = { &KeyValuePair_2_t3732_0_0_0, &KeyValuePair_2_t3732_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3732_0_0_0_KeyValuePair_2_t3732_0_0_0 = { 2, GenInst_KeyValuePair_2_t3732_0_0_0_KeyValuePair_2_t3732_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t2676_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t2676_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t2676_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2676_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2676_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t2677_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t2677_gp_0_0_0_0_Types[] = { &DefaultComparer_t2677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t2677_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t2677_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t2614_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t2614_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t2614_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2614_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t2614_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t2679_gp_0_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t2679_gp_0_0_0_0_Types[] = { &IDictionary_2_t2679_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t2679_gp_0_0_0_0 = { 1, GenInst_IDictionary_2_t2679_gp_0_0_0_0_Types };
extern const Il2CppType IDictionary_2_t2679_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t2679_gp_1_0_0_0_Types[] = { &IDictionary_2_t2679_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t2679_gp_1_0_0_0 = { 1, GenInst_IDictionary_2_t2679_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3783_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3783_0_0_0_Types[] = { &KeyValuePair_2_t3783_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3783_0_0_0 = { 1, GenInst_KeyValuePair_2_t3783_0_0_0_Types };
static const Il2CppType* GenInst_IDictionary_2_t2679_gp_0_0_0_0_IDictionary_2_t2679_gp_1_0_0_0_Types[] = { &IDictionary_2_t2679_gp_0_0_0_0, &IDictionary_2_t2679_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t2679_gp_0_0_0_0_IDictionary_2_t2679_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t2679_gp_0_0_0_0_IDictionary_2_t2679_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2681_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t2681_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2681_gp_0_0_0_0_KeyValuePair_2_t2681_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t2681_gp_0_0_0_0, &KeyValuePair_2_t2681_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2681_gp_0_0_0_0_KeyValuePair_2_t2681_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t2681_gp_0_0_0_0_KeyValuePair_2_t2681_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t2682_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t2682_gp_0_0_0_0_Types[] = { &List_1_t2682_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2682_gp_0_0_0_0 = { 1, GenInst_List_1_t2682_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2683_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2683_gp_0_0_0_0_Types[] = { &Enumerator_t2683_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2683_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2683_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t2684_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t2684_gp_0_0_0_0_Types[] = { &Collection_1_t2684_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t2684_gp_0_0_0_0 = { 1, GenInst_Collection_1_t2684_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t2685_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t2685_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t2685_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t2685_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t2685_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m19498_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m19498_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m19498_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m19498_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m19498_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m19498_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m19498_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m19498_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m19498_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m19498_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m19499_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m19499_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m19499_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m19499_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m19499_gp_0_0_0_0_Types };
extern const Il2CppGenericInst* g_Il2CppGenericInstTable[561] = 
{
	&GenInst_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Player_t11_0_0_0,
	&GenInst_PlayerTwo_t12_0_0_0,
	&GenInst_Text_t47_0_0_0,
	&GenInst_GameObject_t5_0_0_0,
	&GenInst_BoardManager_t1_0_0_0,
	&GenInst_CountdownTimer_t10_0_0_0,
	&GenInst_BoxCollider2D_t38_0_0_0,
	&GenInst_Rigidbody2D_t39_0_0_0,
	&GenInst_SpriteRenderer_t50_0_0_0,
	&GenInst_BaseInputModule_t70_0_0_0,
	&GenInst_RaycastResult_t103_0_0_0,
	&GenInst_IDeselectHandler_t277_0_0_0,
	&GenInst_ISelectHandler_t276_0_0_0,
	&GenInst_BaseEventData_t71_0_0_0,
	&GenInst_Entry_t75_0_0_0,
	&GenInst_IPointerEnterHandler_t264_0_0_0,
	&GenInst_IPointerExitHandler_t265_0_0_0,
	&GenInst_IPointerDownHandler_t266_0_0_0,
	&GenInst_IPointerUpHandler_t267_0_0_0,
	&GenInst_IPointerClickHandler_t268_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t269_0_0_0,
	&GenInst_IBeginDragHandler_t270_0_0_0,
	&GenInst_IDragHandler_t271_0_0_0,
	&GenInst_IEndDragHandler_t272_0_0_0,
	&GenInst_IDropHandler_t273_0_0_0,
	&GenInst_IScrollHandler_t274_0_0_0,
	&GenInst_IUpdateSelectedHandler_t275_0_0_0,
	&GenInst_IMoveHandler_t278_0_0_0,
	&GenInst_ISubmitHandler_t279_0_0_0,
	&GenInst_ICancelHandler_t280_0_0_0,
	&GenInst_List_1_t263_0_0_0,
	&GenInst_IEventSystemHandler_t1821_0_0_0,
	&GenInst_Transform_t3_0_0_0,
	&GenInst_PointerEventData_t108_0_0_0,
	&GenInst_AxisEventData_t105_0_0_0,
	&GenInst_BaseRaycaster_t104_0_0_0,
	&GenInst_Camera_t122_0_0_0,
	&GenInst_EventSystem_t67_0_0_0,
	&GenInst_ButtonState_t111_0_0_0,
	&GenInst_Int32_t56_0_0_0_PointerEventData_t108_0_0_0,
	&GenInst_RaycastHit_t283_0_0_0,
	&GenInst_Color_t128_0_0_0,
	&GenInst_ICanvasElement_t285_0_0_0,
	&GenInst_Font_t142_0_0_0_List_1_t312_0_0_0,
	&GenInst_Font_t142_0_0_0,
	&GenInst_ColorTween_t127_0_0_0,
	&GenInst_List_1_t187_0_0_0,
	&GenInst_UIVertex_t191_0_0_0,
	&GenInst_RectTransform_t18_0_0_0,
	&GenInst_Canvas_t148_0_0_0,
	&GenInst_CanvasRenderer_t147_0_0_0,
	&GenInst_Component_t64_0_0_0,
	&GenInst_Graphic_t145_0_0_0,
	&GenInst_Canvas_t148_0_0_0_IndexedSet_1_t324_0_0_0,
	&GenInst_Sprite_t49_0_0_0,
	&GenInst_Type_t160_0_0_0,
	&GenInst_Boolean_t298_0_0_0,
	&GenInst_FillMethod_t161_0_0_0,
	&GenInst_Single_t61_0_0_0,
	&GenInst_Int32_t56_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_SubmitEvent_t174_0_0_0,
	&GenInst_OnChangeEvent_t176_0_0_0,
	&GenInst_OnValidateInput_t178_0_0_0,
	&GenInst_ContentType_t170_0_0_0,
	&GenInst_LineType_t173_0_0_0,
	&GenInst_InputType_t171_0_0_0,
	&GenInst_TouchScreenKeyboardType_t327_0_0_0,
	&GenInst_CharacterValidation_t172_0_0_0,
	&GenInst_Char_t326_0_0_0,
	&GenInst_UILineInfo_t331_0_0_0,
	&GenInst_UICharInfo_t333_0_0_0,
	&GenInst_LayoutElement_t243_0_0_0,
	&GenInst_Direction_t197_0_0_0,
	&GenInst_Vector2_t59_0_0_0,
	&GenInst_CanvasGroup_t317_0_0_0,
	&GenInst_Selectable_t134_0_0_0,
	&GenInst_Navigation_t194_0_0_0,
	&GenInst_Transition_t209_0_0_0,
	&GenInst_ColorBlock_t140_0_0_0,
	&GenInst_SpriteState_t211_0_0_0,
	&GenInst_AnimationTriggers_t129_0_0_0,
	&GenInst_Animator_t289_0_0_0,
	&GenInst_Direction_t215_0_0_0,
	&GenInst_Image_t167_0_0_0,
	&GenInst_MatEntry_t218_0_0_0,
	&GenInst_Toggle_t224_0_0_0,
	&GenInst_Toggle_t224_0_0_0_Boolean_t298_0_0_0,
	&GenInst_AspectMode_t229_0_0_0,
	&GenInst_FitMode_t235_0_0_0,
	&GenInst_Corner_t237_0_0_0,
	&GenInst_Axis_t238_0_0_0,
	&GenInst_Constraint_t239_0_0_0,
	&GenInst_RectOffset_t244_0_0_0,
	&GenInst_TextAnchor_t348_0_0_0,
	&GenInst_ILayoutElement_t293_0_0_0_Single_t61_0_0_0,
	&GenInst_List_1_t294_0_0_0,
	&GenInst_List_1_t292_0_0_0,
	&GenInst_GcLeaderboard_t379_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t636_0_0_0,
	&GenInst_IAchievementU5BU5D_t638_0_0_0,
	&GenInst_IScoreU5BU5D_t558_0_0_0,
	&GenInst_IUserProfileU5BU5D_t554_0_0_0,
	&GenInst_Int32_t56_0_0_0_LayoutCache_t398_0_0_0,
	&GenInst_GUILayoutEntry_t403_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t402_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0,
	&GenInst_ByteU5BU5D_t435_0_0_0,
	&GenInst_String_t_0_0_0_Object_t_0_0_0,
	&GenInst_MatchDirectConnectInfo_t495_0_0_0,
	&GenInst_MatchDesc_t496_0_0_0,
	&GenInst_NetworkID_t502_0_0_0_NetworkAccessToken_t504_0_0_0,
	&GenInst_CreateMatchResponse_t489_0_0_0,
	&GenInst_JoinMatchResponse_t491_0_0_0,
	&GenInst_BasicResponse_t486_0_0_0,
	&GenInst_ListMatchResponse_t498_0_0_0,
	&GenInst_KeyValuePair_2_t618_0_0_0,
	&GenInst_Type_t_0_0_0_ConstructorDelegate_t522_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t625_0_0_0,
	&GenInst_String_t_0_0_0_GetDelegate_t520_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t626_0_0_0,
	&GenInst_String_t_0_0_0_KeyValuePair_2_t678_0_0_0,
	&GenInst_Type_t_0_0_0_SetDelegate_t521_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst_KeyValuePair_2_t681_0_0_0,
	&GenInst_ConstructorInfo_t524_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_GUILayer_t387_0_0_0,
	&GenInst_PersistentCall_t586_0_0_0,
	&GenInst_BaseInvokableCall_t583_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t56_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t298_0_0_0,
	&GenInst_StrongName_t1565_0_0_0,
	&GenInst_DateTime_t396_0_0_0,
	&GenInst_DateTimeOffset_t679_0_0_0,
	&GenInst_TimeSpan_t977_0_0_0,
	&GenInst_Guid_t680_0_0_0,
	&GenInst_Object_t54_0_0_0,
	&GenInst_Vector3_t35_0_0_0,
	&GenInst_ValueType_t1081_0_0_0,
	&GenInst_IReflect_t2662_0_0_0,
	&GenInst__Type_t2660_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t1742_0_0_0,
	&GenInst__MemberInfo_t2661_0_0_0,
	&GenInst_IFormattable_t1744_0_0_0,
	&GenInst_IConvertible_t1747_0_0_0,
	&GenInst_IComparable_t1746_0_0_0,
	&GenInst_IComparable_1_t2873_0_0_0,
	&GenInst_IEquatable_1_t2878_0_0_0,
	&GenInst_Double_t661_0_0_0,
	&GenInst_IComparable_1_t2886_0_0_0,
	&GenInst_IEquatable_1_t2891_0_0_0,
	&GenInst_IComparable_1_t2899_0_0_0,
	&GenInst_IEquatable_1_t2904_0_0_0,
	&GenInst_IEnumerable_t623_0_0_0,
	&GenInst_ICloneable_t2653_0_0_0,
	&GenInst_IComparable_1_t2919_0_0_0,
	&GenInst_IEquatable_1_t2924_0_0_0,
	&GenInst_Int32_t56_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1877_0_0_0,
	&GenInst_Link_t1188_0_0_0,
	&GenInst_Int32_t56_0_0_0_Object_t_0_0_0_Int32_t56_0_0_0,
	&GenInst_Int32_t56_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Int32_t56_0_0_0_Object_t_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_DictionaryEntry_t1068_0_0_0,
	&GenInst_Int32_t56_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1877_0_0_0,
	&GenInst_Int32_t56_0_0_0_PointerEventData_t108_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_KeyValuePair_2_t304_0_0_0,
	&GenInst_RaycastHit2D_t52_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t56_0_0_0,
	&GenInst_KeyValuePair_2_t1908_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t56_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t56_0_0_0_Int32_t56_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t56_0_0_0_KeyValuePair_2_t1908_0_0_0,
	&GenInst_ICanvasElement_t285_0_0_0_Int32_t56_0_0_0,
	&GenInst_ICanvasElement_t285_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_KeyValuePair_2_t1934_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0,
	&GenInst_Font_t142_0_0_0_List_1_t312_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_List_1_t312_0_0_0,
	&GenInst_KeyValuePair_2_t1944_0_0_0,
	&GenInst_Graphic_t145_0_0_0_Int32_t56_0_0_0,
	&GenInst_Graphic_t145_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_Canvas_t148_0_0_0_IndexedSet_1_t324_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_IndexedSet_1_t324_0_0_0,
	&GenInst_KeyValuePair_2_t1976_0_0_0,
	&GenInst_KeyValuePair_2_t1980_0_0_0,
	&GenInst_KeyValuePair_2_t1984_0_0_0,
	&GenInst_Enum_t624_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t298_0_0_0,
	&GenInst_LayoutRebuilder_t246_0_0_0,
	&GenInst_Object_t_0_0_0_Single_t61_0_0_0,
	&GenInst_IAchievementDescription_t2631_0_0_0,
	&GenInst_IAchievement_t605_0_0_0,
	&GenInst_IScore_t560_0_0_0,
	&GenInst_IUserProfile_t2630_0_0_0,
	&GenInst_AchievementDescription_t556_0_0_0,
	&GenInst_UserProfile_t553_0_0_0,
	&GenInst_GcAchievementData_t546_0_0_0,
	&GenInst_Achievement_t555_0_0_0,
	&GenInst_GcScoreData_t547_0_0_0,
	&GenInst_Score_t557_0_0_0,
	&GenInst_GUILayoutOption_t407_0_0_0,
	&GenInst_Int32_t56_0_0_0_LayoutCache_t398_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_LayoutCache_t398_0_0_0,
	&GenInst_KeyValuePair_2_t2052_0_0_0,
	&GenInst_GUIStyle_t402_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t402_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_KeyValuePair_2_t2064_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_KeyValuePair_2_t2068_0_0_0,
	&GenInst_String_t_0_0_0_String_t_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_KeyValuePair_2_t648_0_0_0,
	&GenInst_Byte_t647_0_0_0,
	&GenInst_IComparable_1_t3089_0_0_0,
	&GenInst_IEquatable_1_t3094_0_0_0,
	&GenInst_Behaviour_t354_0_0_0,
	&GenInst_Display_t444_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_ISerializable_t1769_0_0_0,
	&GenInst_IComparable_1_t3127_0_0_0,
	&GenInst_IEquatable_1_t3132_0_0_0,
	&GenInst_Keyframe_t469_0_0_0,
	&GenInst_String_t_0_0_0_Int64_t662_0_0_0,
	&GenInst_Object_t_0_0_0_Int64_t662_0_0_0,
	&GenInst_KeyValuePair_2_t2118_0_0_0,
	&GenInst_Int64_t662_0_0_0,
	&GenInst_IComparable_1_t3151_0_0_0,
	&GenInst_IEquatable_1_t3156_0_0_0,
	&GenInst_Object_t_0_0_0_Int64_t662_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Int64_t662_0_0_0_Int64_t662_0_0_0,
	&GenInst_Object_t_0_0_0_Int64_t662_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_Object_t_0_0_0_Int64_t662_0_0_0_KeyValuePair_2_t2118_0_0_0,
	&GenInst_String_t_0_0_0_Int64_t662_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_KeyValuePair_2_t2133_0_0_0,
	&GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2155_0_0_0,
	&GenInst_NetworkID_t502_0_0_0,
	&GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0_NetworkID_t502_0_0_0,
	&GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_NetworkID_t502_0_0_0_Object_t_0_0_0_KeyValuePair_2_t2155_0_0_0,
	&GenInst_NetworkID_t502_0_0_0_NetworkAccessToken_t504_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_NetworkAccessToken_t504_0_0_0,
	&GenInst_KeyValuePair_2_t2169_0_0_0,
	&GenInst_String_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_ConstructorDelegate_t522_0_0_0,
	&GenInst_GetDelegate_t520_0_0_0,
	&GenInst_IDictionary_2_t625_0_0_0,
	&GenInst_KeyValuePair_2_t678_0_0_0,
	&GenInst_IDictionary_2_t626_0_0_0,
	&GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0,
	&GenInst_KeyValuePair_2_t2175_0_0_0,
	&GenInst_Type_t_0_0_0_ConstructorDelegate_t522_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_KeyValuePair_2_t2183_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t625_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_KeyValuePair_2_t2187_0_0_0,
	&GenInst_Type_t_0_0_0_IDictionary_2_t626_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_KeyValuePair_2_t2191_0_0_0,
	&GenInst_String_t_0_0_0_GetDelegate_t520_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_KeyValuePair_2_t1934_0_0_0,
	&GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_Object_t_0_0_0_KeyValuePair_2_t1934_0_0_0_KeyValuePair_2_t2175_0_0_0,
	&GenInst_String_t_0_0_0_KeyValuePair_2_t678_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_KeyValuePair_2_t2213_0_0_0,
	&GenInst__ConstructorInfo_t2698_0_0_0,
	&GenInst_MethodBase_t696_0_0_0,
	&GenInst__MethodBase_t2701_0_0_0,
	&GenInst_ParameterInfo_t686_0_0_0,
	&GenInst__ParameterInfo_t2704_0_0_0,
	&GenInst__PropertyInfo_t2705_0_0_0,
	&GenInst__FieldInfo_t2700_0_0_0,
	&GenInst_DisallowMultipleComponent_t536_0_0_0,
	&GenInst_Attribute_t428_0_0_0,
	&GenInst__Attribute_t2649_0_0_0,
	&GenInst_ExecuteInEditMode_t539_0_0_0,
	&GenInst_RequireComponent_t537_0_0_0,
	&GenInst_ParameterModifier_t1346_0_0_0,
	&GenInst_HitInfo_t561_0_0_0,
	&GenInst_Event_t189_0_0_0_TextEditOp_t579_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0,
	&GenInst_KeyValuePair_2_t2238_0_0_0,
	&GenInst_TextEditOp_t579_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0_TextEditOp_t579_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t579_0_0_0_KeyValuePair_2_t2238_0_0_0,
	&GenInst_Event_t189_0_0_0,
	&GenInst_Event_t189_0_0_0_TextEditOp_t579_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_KeyValuePair_2_t2252_0_0_0,
	&GenInst_KeySizes_t729_0_0_0,
	&GenInst_UInt32_t654_0_0_0,
	&GenInst_IComparable_1_t3278_0_0_0,
	&GenInst_IEquatable_1_t3283_0_0_0,
	&GenInst_BigInteger_t734_0_0_0,
	&GenInst_X509Certificate_t840_0_0_0,
	&GenInst_IDeserializationCallback_t1772_0_0_0,
	&GenInst_ClientCertificateType_t844_0_0_0,
	&GenInst_UInt16_t652_0_0_0,
	&GenInst_IComparable_1_t3306_0_0_0,
	&GenInst_IEquatable_1_t3311_0_0_0,
	&GenInst_KeyValuePair_2_t2289_0_0_0,
	&GenInst_IComparable_1_t3321_0_0_0,
	&GenInst_IEquatable_1_t3326_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t298_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t298_0_0_0_Boolean_t298_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t298_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t298_0_0_0_KeyValuePair_2_t2289_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t298_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_KeyValuePair_2_t2304_0_0_0,
	&GenInst_X509ChainStatus_t974_0_0_0,
	&GenInst_Capture_t994_0_0_0,
	&GenInst_Group_t910_0_0_0,
	&GenInst_Int32_t56_0_0_0_Int32_t56_0_0_0,
	&GenInst_KeyValuePair_2_t2312_0_0_0,
	&GenInst_Int32_t56_0_0_0_Int32_t56_0_0_0_Int32_t56_0_0_0,
	&GenInst_Int32_t56_0_0_0_Int32_t56_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_Int32_t56_0_0_0_Int32_t56_0_0_0_KeyValuePair_2_t2312_0_0_0,
	&GenInst_Mark_t1019_0_0_0,
	&GenInst_UriScheme_t1056_0_0_0,
	&GenInst_UInt64_t665_0_0_0,
	&GenInst_SByte_t666_0_0_0,
	&GenInst_Int16_t667_0_0_0,
	&GenInst_Decimal_t664_0_0_0,
	&GenInst_Delegate_t320_0_0_0,
	&GenInst_IComparable_1_t3353_0_0_0,
	&GenInst_IEquatable_1_t3354_0_0_0,
	&GenInst_IComparable_1_t3357_0_0_0,
	&GenInst_IEquatable_1_t3358_0_0_0,
	&GenInst_IComparable_1_t3355_0_0_0,
	&GenInst_IEquatable_1_t3356_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t2702_0_0_0,
	&GenInst_TableRange_t1121_0_0_0,
	&GenInst_TailoringInfo_t1124_0_0_0,
	&GenInst_Contraction_t1125_0_0_0,
	&GenInst_Level2Map_t1127_0_0_0,
	&GenInst_BigInteger_t1148_0_0_0,
	&GenInst_Slot_t1198_0_0_0,
	&GenInst_Slot_t1205_0_0_0,
	&GenInst_StackFrame_t695_0_0_0,
	&GenInst_Calendar_t1219_0_0_0,
	&GenInst_ModuleBuilder_t1293_0_0_0,
	&GenInst__ModuleBuilder_t2692_0_0_0,
	&GenInst_Module_t1289_0_0_0,
	&GenInst__Module_t2703_0_0_0,
	&GenInst_ParameterBuilder_t1299_0_0_0,
	&GenInst__ParameterBuilder_t2693_0_0_0,
	&GenInst_TypeU5BU5D_t516_0_0_0,
	&GenInst_Array_t_0_0_0,
	&GenInst_ICollection_t1069_0_0_0,
	&GenInst_IList_t1028_0_0_0,
	&GenInst_ILTokenInfo_t1283_0_0_0,
	&GenInst_LabelData_t1285_0_0_0,
	&GenInst_LabelFixup_t1284_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1281_0_0_0,
	&GenInst_TypeBuilder_t1275_0_0_0,
	&GenInst__TypeBuilder_t2695_0_0_0,
	&GenInst_MethodBuilder_t1282_0_0_0,
	&GenInst__MethodBuilder_t2691_0_0_0,
	&GenInst_ConstructorBuilder_t1273_0_0_0,
	&GenInst__ConstructorBuilder_t2687_0_0_0,
	&GenInst_PropertyBuilder_t1300_0_0_0,
	&GenInst__PropertyBuilder_t2694_0_0_0,
	&GenInst_FieldBuilder_t1279_0_0_0,
	&GenInst__FieldBuilder_t2689_0_0_0,
	&GenInst_IContextProperty_t1737_0_0_0,
	&GenInst_Header_t1428_0_0_0,
	&GenInst_ITrackingHandler_t1764_0_0_0,
	&GenInst_IContextAttribute_t1751_0_0_0,
	&GenInst_IComparable_1_t3552_0_0_0,
	&GenInst_IEquatable_1_t3557_0_0_0,
	&GenInst_IComparable_1_t3359_0_0_0,
	&GenInst_IEquatable_1_t3360_0_0_0,
	&GenInst_IComparable_1_t3576_0_0_0,
	&GenInst_IEquatable_1_t3581_0_0_0,
	&GenInst_TypeTag_t1484_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_Version_t945_0_0_0,
	&GenInst_Vector3_t35_0_0_0_Vector3_t35_0_0_0,
	&GenInst_RaycastResult_t103_0_0_0_RaycastResult_t103_0_0_0,
	&GenInst_DictionaryEntry_t1068_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_KeyValuePair_2_t1877_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1877_0_0_0_KeyValuePair_2_t1877_0_0_0,
	&GenInst_KeyValuePair_2_t1908_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1908_0_0_0_KeyValuePair_2_t1908_0_0_0,
	&GenInst_KeyValuePair_2_t1934_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1934_0_0_0_KeyValuePair_2_t1934_0_0_0,
	&GenInst_UIVertex_t191_0_0_0_UIVertex_t191_0_0_0,
	&GenInst_UICharInfo_t333_0_0_0_UICharInfo_t333_0_0_0,
	&GenInst_UILineInfo_t331_0_0_0_UILineInfo_t331_0_0_0,
	&GenInst_Int64_t662_0_0_0_Object_t_0_0_0,
	&GenInst_Int64_t662_0_0_0_Int64_t662_0_0_0,
	&GenInst_KeyValuePair_2_t2118_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2118_0_0_0_KeyValuePair_2_t2118_0_0_0,
	&GenInst_NetworkID_t502_0_0_0_NetworkID_t502_0_0_0,
	&GenInst_KeyValuePair_2_t2155_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2155_0_0_0_KeyValuePair_2_t2155_0_0_0,
	&GenInst_KeyValuePair_2_t2175_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2175_0_0_0_KeyValuePair_2_t2175_0_0_0,
	&GenInst_TextEditOp_t579_0_0_0_Object_t_0_0_0,
	&GenInst_TextEditOp_t579_0_0_0_TextEditOp_t579_0_0_0,
	&GenInst_KeyValuePair_2_t2238_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2238_0_0_0_KeyValuePair_2_t2238_0_0_0,
	&GenInst_Boolean_t298_0_0_0_Object_t_0_0_0,
	&GenInst_Boolean_t298_0_0_0_Boolean_t298_0_0_0,
	&GenInst_KeyValuePair_2_t2289_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2289_0_0_0_KeyValuePair_2_t2289_0_0_0,
	&GenInst_KeyValuePair_2_t2312_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2312_0_0_0_KeyValuePair_2_t2312_0_0_0,
	&GenInst_ExecuteEvents_Execute_m18707_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m18708_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m18710_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m18711_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m18712_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t2618_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t2620_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t2620_gp_0_0_0_0_Int32_t56_0_0_0,
	&GenInst_ObjectPool_1_t2621_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m18790_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m18791_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m18792_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m18794_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m18795_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m18796_gp_0_0_0_0,
	&GenInst_ResponseBase_ParseJSONList_m18800_gp_0_0_0_0,
	&GenInst_NetworkMatch_ProcessMatchResponse_m18801_gp_0_0_0_0,
	&GenInst_U3CProcessMatchResponseU3Ec__Iterator0_1_t2625_gp_0_0_0_0,
	&GenInst_ThreadSafeDictionary_2_t2626_gp_0_0_0_0_ThreadSafeDictionary_2_t2626_gp_1_0_0_0,
	&GenInst_ThreadSafeDictionary_2_t2626_gp_0_0_0_0,
	&GenInst_ThreadSafeDictionary_2_t2626_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3640_0_0_0,
	&GenInst_InvokableCall_1_t2632_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t2633_gp_0_0_0_0_InvokableCall_2_t2633_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t2633_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t2633_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t2634_gp_0_0_0_0_InvokableCall_3_t2634_gp_1_0_0_0_InvokableCall_3_t2634_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t2634_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t2634_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t2634_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t2635_gp_0_0_0_0_InvokableCall_4_t2635_gp_1_0_0_0_InvokableCall_4_t2635_gp_2_0_0_0_InvokableCall_4_t2635_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t2635_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t2635_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t2635_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t2635_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t702_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t2636_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t2637_gp_0_0_0_0_UnityEvent_2_t2637_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t2638_gp_0_0_0_0_UnityEvent_3_t2638_gp_1_0_0_0_UnityEvent_3_t2638_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t2639_gp_0_0_0_0_UnityEvent_4_t2639_gp_1_0_0_0_UnityEvent_4_t2639_gp_2_0_0_0_UnityEvent_4_t2639_gp_3_0_0_0,
	&GenInst_Enumerable_Where_m18894_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m18894_gp_0_0_0_0_Boolean_t298_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m18895_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m18895_gp_0_0_0_0_Boolean_t298_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2644_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2644_gp_0_0_0_0_Boolean_t298_0_0_0,
	&GenInst_Stack_1_t2646_gp_0_0_0_0,
	&GenInst_Enumerator_t2647_gp_0_0_0_0,
	&GenInst_IEnumerable_1_t2654_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m19013_gp_0_0_0_0,
	&GenInst_Array_Sort_m19025_gp_0_0_0_0_Array_Sort_m19025_gp_0_0_0_0,
	&GenInst_Array_Sort_m19026_gp_0_0_0_0_Array_Sort_m19026_gp_1_0_0_0,
	&GenInst_Array_Sort_m19027_gp_0_0_0_0,
	&GenInst_Array_Sort_m19027_gp_0_0_0_0_Array_Sort_m19027_gp_0_0_0_0,
	&GenInst_Array_Sort_m19028_gp_0_0_0_0,
	&GenInst_Array_Sort_m19028_gp_0_0_0_0_Array_Sort_m19028_gp_1_0_0_0,
	&GenInst_Array_Sort_m19029_gp_0_0_0_0_Array_Sort_m19029_gp_0_0_0_0,
	&GenInst_Array_Sort_m19030_gp_0_0_0_0_Array_Sort_m19030_gp_1_0_0_0,
	&GenInst_Array_Sort_m19031_gp_0_0_0_0,
	&GenInst_Array_Sort_m19031_gp_0_0_0_0_Array_Sort_m19031_gp_0_0_0_0,
	&GenInst_Array_Sort_m19032_gp_0_0_0_0,
	&GenInst_Array_Sort_m19032_gp_1_0_0_0,
	&GenInst_Array_Sort_m19032_gp_0_0_0_0_Array_Sort_m19032_gp_1_0_0_0,
	&GenInst_Array_Sort_m19033_gp_0_0_0_0,
	&GenInst_Array_Sort_m19034_gp_0_0_0_0,
	&GenInst_Array_qsort_m19035_gp_0_0_0_0,
	&GenInst_Array_qsort_m19035_gp_0_0_0_0_Array_qsort_m19035_gp_1_0_0_0,
	&GenInst_Array_compare_m19036_gp_0_0_0_0,
	&GenInst_Array_qsort_m19037_gp_0_0_0_0,
	&GenInst_Array_Resize_m19040_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m19042_gp_0_0_0_0,
	&GenInst_Array_ForEach_m19043_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m19044_gp_0_0_0_0_Array_ConvertAll_m19044_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m19045_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m19046_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m19047_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m19048_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m19049_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m19050_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m19051_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m19052_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m19053_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m19054_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m19055_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m19056_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m19057_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m19058_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m19059_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m19060_gp_0_0_0_0,
	&GenInst_Array_FindAll_m19061_gp_0_0_0_0,
	&GenInst_Array_Exists_m19062_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m19063_gp_0_0_0_0,
	&GenInst_Array_Find_m19064_gp_0_0_0_0,
	&GenInst_Array_FindLast_m19065_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t2655_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t2656_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2657_gp_0_0_0_0,
	&GenInst_IList_1_t2658_gp_0_0_0_0,
	&GenInst_ICollection_1_t2659_gp_0_0_0_0,
	&GenInst_Nullable_1_t1748_gp_0_0_0_0,
	&GenInst_Comparer_1_t2666_gp_0_0_0_0,
	&GenInst_DefaultComparer_t2667_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t2615_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2668_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0,
	&GenInst_Dictionary_2_t2668_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3732_0_0_0,
	&GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m19217_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m19222_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m19222_gp_0_0_0_0_Object_t_0_0_0,
	&GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0_DictionaryEntry_t1068_0_0_0,
	&GenInst_ShimEnumerator_t2669_gp_0_0_0_0_ShimEnumerator_t2669_gp_1_0_0_0,
	&GenInst_Enumerator_t2670_gp_0_0_0_0_Enumerator_t2670_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3745_0_0_0,
	&GenInst_KeyCollection_t2671_gp_0_0_0_0_KeyCollection_t2671_gp_1_0_0_0,
	&GenInst_KeyCollection_t2671_gp_0_0_0_0,
	&GenInst_Enumerator_t2672_gp_0_0_0_0_Enumerator_t2672_gp_1_0_0_0,
	&GenInst_Enumerator_t2672_gp_0_0_0_0,
	&GenInst_KeyCollection_t2671_gp_0_0_0_0_KeyCollection_t2671_gp_1_0_0_0_KeyCollection_t2671_gp_0_0_0_0,
	&GenInst_KeyCollection_t2671_gp_0_0_0_0_KeyCollection_t2671_gp_0_0_0_0,
	&GenInst_ValueCollection_t2673_gp_0_0_0_0_ValueCollection_t2673_gp_1_0_0_0,
	&GenInst_ValueCollection_t2673_gp_1_0_0_0,
	&GenInst_Enumerator_t2674_gp_0_0_0_0_Enumerator_t2674_gp_1_0_0_0,
	&GenInst_Enumerator_t2674_gp_1_0_0_0,
	&GenInst_ValueCollection_t2673_gp_0_0_0_0_ValueCollection_t2673_gp_1_0_0_0_ValueCollection_t2673_gp_1_0_0_0,
	&GenInst_ValueCollection_t2673_gp_1_0_0_0_ValueCollection_t2673_gp_1_0_0_0,
	&GenInst_Dictionary_2_t2668_gp_0_0_0_0_Dictionary_2_t2668_gp_1_0_0_0_KeyValuePair_2_t3732_0_0_0,
	&GenInst_KeyValuePair_2_t3732_0_0_0_KeyValuePair_2_t3732_0_0_0,
	&GenInst_EqualityComparer_1_t2676_gp_0_0_0_0,
	&GenInst_DefaultComparer_t2677_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t2614_gp_0_0_0_0,
	&GenInst_IDictionary_2_t2679_gp_0_0_0_0,
	&GenInst_IDictionary_2_t2679_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3783_0_0_0,
	&GenInst_IDictionary_2_t2679_gp_0_0_0_0_IDictionary_2_t2679_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t2681_gp_0_0_0_0_KeyValuePair_2_t2681_gp_1_0_0_0,
	&GenInst_List_1_t2682_gp_0_0_0_0,
	&GenInst_Enumerator_t2683_gp_0_0_0_0,
	&GenInst_Collection_1_t2684_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t2685_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m19498_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m19498_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m19499_gp_0_0_0_0,
};
