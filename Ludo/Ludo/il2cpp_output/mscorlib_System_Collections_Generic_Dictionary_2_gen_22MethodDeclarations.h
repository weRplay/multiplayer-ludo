﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t2199;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1907;
// System.Collections.Generic.IDictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IDictionary_2_t2481;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t631;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t621;
// System.Collections.Generic.ICollection`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ICollection_1_t2482;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>[]
struct KeyValuePair_2U5BU5D_t2480;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t51;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>>
struct IEnumerator_1_t2483;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1067;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct KeyCollection_t2202;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ValueCollection_t2206;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__17.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor()
extern "C" void Dictionary_2__ctor_m16227_gshared (Dictionary_2_t2199 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m16227(__this, method) (( void (*) (Dictionary_2_t2199 *, const MethodInfo*))Dictionary_2__ctor_m16227_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m16229_gshared (Dictionary_2_t2199 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m16229(__this, ___comparer, method) (( void (*) (Dictionary_2_t2199 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16229_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m16231_gshared (Dictionary_2_t2199 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m16231(__this, ___dictionary, method) (( void (*) (Dictionary_2_t2199 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16231_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m16233_gshared (Dictionary_2_t2199 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m16233(__this, ___capacity, method) (( void (*) (Dictionary_2_t2199 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m16233_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m16235_gshared (Dictionary_2_t2199 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m16235(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t2199 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m16235_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m16237_gshared (Dictionary_2_t2199 * __this, SerializationInfo_t631 * ___info, StreamingContext_t632  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m16237(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2199 *, SerializationInfo_t631 *, StreamingContext_t632 , const MethodInfo*))Dictionary_2__ctor_m16237_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16239_gshared (Dictionary_2_t2199 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16239(__this, method) (( Object_t* (*) (Dictionary_2_t2199 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m16239_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16241_gshared (Dictionary_2_t2199 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16241(__this, method) (( Object_t* (*) (Dictionary_2_t2199 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m16241_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m16243_gshared (Dictionary_2_t2199 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m16243(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2199 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m16243_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m16245_gshared (Dictionary_2_t2199 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m16245(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2199 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m16245_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m16247_gshared (Dictionary_2_t2199 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m16247(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2199 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m16247_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m16249_gshared (Dictionary_2_t2199 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m16249(__this, ___key, method) (( bool (*) (Dictionary_2_t2199 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m16249_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m16251_gshared (Dictionary_2_t2199 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m16251(__this, ___key, method) (( void (*) (Dictionary_2_t2199 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m16251_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16253_gshared (Dictionary_2_t2199 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16253(__this, method) (( bool (*) (Dictionary_2_t2199 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m16253_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16255_gshared (Dictionary_2_t2199 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16255(__this, method) (( Object_t * (*) (Dictionary_2_t2199 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m16255_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16257_gshared (Dictionary_2_t2199 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16257(__this, method) (( bool (*) (Dictionary_2_t2199 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m16257_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16259_gshared (Dictionary_2_t2199 * __this, KeyValuePair_2_t2175  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16259(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2199 *, KeyValuePair_2_t2175 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m16259_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16261_gshared (Dictionary_2_t2199 * __this, KeyValuePair_2_t2175  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16261(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2199 *, KeyValuePair_2_t2175 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m16261_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16263_gshared (Dictionary_2_t2199 * __this, KeyValuePair_2U5BU5D_t2480* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16263(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2199 *, KeyValuePair_2U5BU5D_t2480*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m16263_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16265_gshared (Dictionary_2_t2199 * __this, KeyValuePair_2_t2175  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16265(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2199 *, KeyValuePair_2_t2175 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m16265_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m16267_gshared (Dictionary_2_t2199 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m16267(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2199 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m16267_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16269_gshared (Dictionary_2_t2199 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16269(__this, method) (( Object_t * (*) (Dictionary_2_t2199 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m16269_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16271_gshared (Dictionary_2_t2199 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16271(__this, method) (( Object_t* (*) (Dictionary_2_t2199 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m16271_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16273_gshared (Dictionary_2_t2199 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16273(__this, method) (( Object_t * (*) (Dictionary_2_t2199 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m16273_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m16275_gshared (Dictionary_2_t2199 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m16275(__this, method) (( int32_t (*) (Dictionary_2_t2199 *, const MethodInfo*))Dictionary_2_get_Count_m16275_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Item(TKey)
extern "C" KeyValuePair_2_t1934  Dictionary_2_get_Item_m16277_gshared (Dictionary_2_t2199 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m16277(__this, ___key, method) (( KeyValuePair_2_t1934  (*) (Dictionary_2_t2199 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m16277_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m16279_gshared (Dictionary_2_t2199 * __this, Object_t * ___key, KeyValuePair_2_t1934  ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m16279(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2199 *, Object_t *, KeyValuePair_2_t1934 , const MethodInfo*))Dictionary_2_set_Item_m16279_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m16281_gshared (Dictionary_2_t2199 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m16281(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2199 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m16281_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m16283_gshared (Dictionary_2_t2199 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m16283(__this, ___size, method) (( void (*) (Dictionary_2_t2199 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m16283_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m16285_gshared (Dictionary_2_t2199 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m16285(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2199 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m16285_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2175  Dictionary_2_make_pair_m16287_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t1934  ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m16287(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2175  (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t1934 , const MethodInfo*))Dictionary_2_make_pair_m16287_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m16289_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t1934  ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m16289(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t1934 , const MethodInfo*))Dictionary_2_pick_key_m16289_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::pick_value(TKey,TValue)
extern "C" KeyValuePair_2_t1934  Dictionary_2_pick_value_m16291_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t1934  ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m16291(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t1934  (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t1934 , const MethodInfo*))Dictionary_2_pick_value_m16291_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m16293_gshared (Dictionary_2_t2199 * __this, KeyValuePair_2U5BU5D_t2480* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m16293(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2199 *, KeyValuePair_2U5BU5D_t2480*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m16293_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Resize()
extern "C" void Dictionary_2_Resize_m16295_gshared (Dictionary_2_t2199 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m16295(__this, method) (( void (*) (Dictionary_2_t2199 *, const MethodInfo*))Dictionary_2_Resize_m16295_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m16297_gshared (Dictionary_2_t2199 * __this, Object_t * ___key, KeyValuePair_2_t1934  ___value, const MethodInfo* method);
#define Dictionary_2_Add_m16297(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2199 *, Object_t *, KeyValuePair_2_t1934 , const MethodInfo*))Dictionary_2_Add_m16297_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Clear()
extern "C" void Dictionary_2_Clear_m16299_gshared (Dictionary_2_t2199 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m16299(__this, method) (( void (*) (Dictionary_2_t2199 *, const MethodInfo*))Dictionary_2_Clear_m16299_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m16301_gshared (Dictionary_2_t2199 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m16301(__this, ___key, method) (( bool (*) (Dictionary_2_t2199 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m16301_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m16303_gshared (Dictionary_2_t2199 * __this, KeyValuePair_2_t1934  ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m16303(__this, ___value, method) (( bool (*) (Dictionary_2_t2199 *, KeyValuePair_2_t1934 , const MethodInfo*))Dictionary_2_ContainsValue_m16303_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m16305_gshared (Dictionary_2_t2199 * __this, SerializationInfo_t631 * ___info, StreamingContext_t632  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m16305(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2199 *, SerializationInfo_t631 *, StreamingContext_t632 , const MethodInfo*))Dictionary_2_GetObjectData_m16305_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m16307_gshared (Dictionary_2_t2199 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m16307(__this, ___sender, method) (( void (*) (Dictionary_2_t2199 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m16307_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m16309_gshared (Dictionary_2_t2199 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m16309(__this, ___key, method) (( bool (*) (Dictionary_2_t2199 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m16309_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m16311_gshared (Dictionary_2_t2199 * __this, Object_t * ___key, KeyValuePair_2_t1934 * ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m16311(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2199 *, Object_t *, KeyValuePair_2_t1934 *, const MethodInfo*))Dictionary_2_TryGetValue_m16311_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Keys()
extern "C" KeyCollection_t2202 * Dictionary_2_get_Keys_m16313_gshared (Dictionary_2_t2199 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m16313(__this, method) (( KeyCollection_t2202 * (*) (Dictionary_2_t2199 *, const MethodInfo*))Dictionary_2_get_Keys_m16313_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Values()
extern "C" ValueCollection_t2206 * Dictionary_2_get_Values_m16315_gshared (Dictionary_2_t2199 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m16315(__this, method) (( ValueCollection_t2206 * (*) (Dictionary_2_t2199 *, const MethodInfo*))Dictionary_2_get_Values_m16315_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m16317_gshared (Dictionary_2_t2199 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m16317(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2199 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m16317_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToTValue(System.Object)
extern "C" KeyValuePair_2_t1934  Dictionary_2_ToTValue_m16319_gshared (Dictionary_2_t2199 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m16319(__this, ___value, method) (( KeyValuePair_2_t1934  (*) (Dictionary_2_t2199 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m16319_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m16321_gshared (Dictionary_2_t2199 * __this, KeyValuePair_2_t2175  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m16321(__this, ___pair, method) (( bool (*) (Dictionary_2_t2199 *, KeyValuePair_2_t2175 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m16321_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::GetEnumerator()
extern "C" Enumerator_t2204  Dictionary_2_GetEnumerator_m16323_gshared (Dictionary_2_t2199 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m16323(__this, method) (( Enumerator_t2204  (*) (Dictionary_2_t2199 *, const MethodInfo*))Dictionary_2_GetEnumerator_m16323_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1068  Dictionary_2_U3CCopyToU3Em__0_m16325_gshared (Object_t * __this /* static, unused */, Object_t * ___key, KeyValuePair_2_t1934  ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m16325(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1068  (*) (Object_t * /* static, unused */, Object_t *, KeyValuePair_2_t1934 , const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m16325_gshared)(__this /* static, unused */, ___key, ___value, method)
