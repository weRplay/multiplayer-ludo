﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameMenu/<GetActiveUsers>c__Iterator9
struct U3CGetActiveUsersU3Ec__Iterator9_t28;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void GameMenu/<GetActiveUsers>c__Iterator9::.ctor()
extern "C" void U3CGetActiveUsersU3Ec__Iterator9__ctor_m96 (U3CGetActiveUsersU3Ec__Iterator9_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameMenu/<GetActiveUsers>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CGetActiveUsersU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m97 (U3CGetActiveUsersU3Ec__Iterator9_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameMenu/<GetActiveUsers>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetActiveUsersU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m98 (U3CGetActiveUsersU3Ec__Iterator9_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameMenu/<GetActiveUsers>c__Iterator9::MoveNext()
extern "C" bool U3CGetActiveUsersU3Ec__Iterator9_MoveNext_m99 (U3CGetActiveUsersU3Ec__Iterator9_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMenu/<GetActiveUsers>c__Iterator9::Dispose()
extern "C" void U3CGetActiveUsersU3Ec__Iterator9_Dispose_m100 (U3CGetActiveUsersU3Ec__Iterator9_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMenu/<GetActiveUsers>c__Iterator9::Reset()
extern "C" void U3CGetActiveUsersU3Ec__Iterator9_Reset_m101 (U3CGetActiveUsersU3Ec__Iterator9_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
