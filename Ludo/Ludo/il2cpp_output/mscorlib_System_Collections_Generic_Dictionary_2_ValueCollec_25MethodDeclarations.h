﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t2199;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_25.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m16375_gshared (Enumerator_t2207 * __this, Dictionary_2_t2199 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m16375(__this, ___host, method) (( void (*) (Enumerator_t2207 *, Dictionary_2_t2199 *, const MethodInfo*))Enumerator__ctor_m16375_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16376_gshared (Enumerator_t2207 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16376(__this, method) (( Object_t * (*) (Enumerator_t2207 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16376_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C" void Enumerator_Dispose_m16377_gshared (Enumerator_t2207 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16377(__this, method) (( void (*) (Enumerator_t2207 *, const MethodInfo*))Enumerator_Dispose_m16377_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16378_gshared (Enumerator_t2207 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16378(__this, method) (( bool (*) (Enumerator_t2207 *, const MethodInfo*))Enumerator_MoveNext_m16378_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" KeyValuePair_2_t1934  Enumerator_get_Current_m16379_gshared (Enumerator_t2207 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16379(__this, method) (( KeyValuePair_2_t1934  (*) (Enumerator_t2207 *, const MethodInfo*))Enumerator_get_Current_m16379_gshared)(__this, method)
