﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>
struct DefaultComparer_t2113;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m15059_gshared (DefaultComparer_t2113 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m15059(__this, method) (( void (*) (DefaultComparer_t2113 *, const MethodInfo*))DefaultComparer__ctor_m15059_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UILineInfo>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m15060_gshared (DefaultComparer_t2113 * __this, UILineInfo_t331  ___x, UILineInfo_t331  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m15060(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2113 *, UILineInfo_t331 , UILineInfo_t331 , const MethodInfo*))DefaultComparer_Compare_m15060_gshared)(__this, ___x, ___y, method)
