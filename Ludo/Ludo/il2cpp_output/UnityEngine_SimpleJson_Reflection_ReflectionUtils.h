﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object[]
struct ObjectU5BU5D_t60;

#include "mscorlib_System_Object.h"

// SimpleJson.Reflection.ReflectionUtils
struct  ReflectionUtils_t529  : public Object_t
{
};
struct ReflectionUtils_t529_StaticFields{
	// System.Object[] SimpleJson.Reflection.ReflectionUtils::EmptyObjects
	ObjectU5BU5D_t60* ___EmptyObjects_0;
};
