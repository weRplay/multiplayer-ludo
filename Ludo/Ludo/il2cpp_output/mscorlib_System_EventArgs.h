﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.EventArgs
struct EventArgs_t759;

#include "mscorlib_System_Object.h"

// System.EventArgs
struct  EventArgs_t759  : public Object_t
{
};
struct EventArgs_t759_StaticFields{
	// System.EventArgs System.EventArgs::Empty
	EventArgs_t759 * ___Empty_0;
};
