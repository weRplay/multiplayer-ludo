﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// DynamicScrollView
struct DynamicScrollView_t14;

#include "mscorlib_System_Object.h"

// DynamicScrollView/<MoveTowardsTarget>c__Iterator1
struct  U3CMoveTowardsTargetU3Ec__Iterator1_t13  : public Object_t
{
	// System.Single DynamicScrollView/<MoveTowardsTarget>c__Iterator1::<i>__0
	float ___U3CiU3E__0_0;
	// System.Single DynamicScrollView/<MoveTowardsTarget>c__Iterator1::time
	float ___time_1;
	// System.Single DynamicScrollView/<MoveTowardsTarget>c__Iterator1::<rate>__1
	float ___U3CrateU3E__1_2;
	// System.Single DynamicScrollView/<MoveTowardsTarget>c__Iterator1::from
	float ___from_3;
	// System.Single DynamicScrollView/<MoveTowardsTarget>c__Iterator1::target
	float ___target_4;
	// System.Int32 DynamicScrollView/<MoveTowardsTarget>c__Iterator1::$PC
	int32_t ___U24PC_5;
	// System.Object DynamicScrollView/<MoveTowardsTarget>c__Iterator1::$current
	Object_t * ___U24current_6;
	// System.Single DynamicScrollView/<MoveTowardsTarget>c__Iterator1::<$>time
	float ___U3CU24U3Etime_7;
	// System.Single DynamicScrollView/<MoveTowardsTarget>c__Iterator1::<$>from
	float ___U3CU24U3Efrom_8;
	// System.Single DynamicScrollView/<MoveTowardsTarget>c__Iterator1::<$>target
	float ___U3CU24U3Etarget_9;
	// DynamicScrollView DynamicScrollView/<MoveTowardsTarget>c__Iterator1::<>f__this
	DynamicScrollView_t14 * ___U3CU3Ef__this_10;
};
