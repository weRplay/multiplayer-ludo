﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_9MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m14278(__this, ___host, method) (( void (*) (Enumerator_t644 *, Dictionary_2_t413 *, const MethodInfo*))Enumerator__ctor_m12514_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14279(__this, method) (( Object_t * (*) (Enumerator_t644 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12515_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m14280(__this, method) (( void (*) (Enumerator_t644 *, const MethodInfo*))Enumerator_Dispose_m12516_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m14281(__this, method) (( bool (*) (Enumerator_t644 *, const MethodInfo*))Enumerator_MoveNext_m12517_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m14282(__this, method) (( GUIStyle_t402 * (*) (Enumerator_t644 *, const MethodInfo*))Enumerator_get_Current_m12518_gshared)(__this, method)
