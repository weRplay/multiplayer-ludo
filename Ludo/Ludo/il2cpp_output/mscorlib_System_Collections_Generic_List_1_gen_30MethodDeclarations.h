﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_18MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.ctor()
#define List_1__ctor_m3394(__this, method) (( void (*) (List_1_t590 *, const MethodInfo*))List_1__ctor_m3317_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.ctor(System.Int32)
#define List_1__ctor_m16979(__this, ___capacity, method) (( void (*) (List_1_t590 *, int32_t, const MethodInfo*))List_1__ctor_m10626_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::.cctor()
#define List_1__cctor_m16980(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m10628_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16981(__this, method) (( Object_t* (*) (List_1_t590 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m10630_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m16982(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t590 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m10632_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m16983(__this, method) (( Object_t * (*) (List_1_t590 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m10634_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m16984(__this, ___item, method) (( int32_t (*) (List_1_t590 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m10636_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m16985(__this, ___item, method) (( bool (*) (List_1_t590 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m10638_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m16986(__this, ___item, method) (( int32_t (*) (List_1_t590 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m10640_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m16987(__this, ___index, ___item, method) (( void (*) (List_1_t590 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m10642_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m16988(__this, ___item, method) (( void (*) (List_1_t590 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m10644_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16989(__this, method) (( bool (*) (List_1_t590 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m10646_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m16990(__this, method) (( bool (*) (List_1_t590 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m10648_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m16991(__this, method) (( Object_t * (*) (List_1_t590 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m10650_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m16992(__this, method) (( bool (*) (List_1_t590 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m10652_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m16993(__this, method) (( bool (*) (List_1_t590 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m10654_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m16994(__this, ___index, method) (( Object_t * (*) (List_1_t590 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m10656_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m16995(__this, ___index, ___value, method) (( void (*) (List_1_t590 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m10658_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Add(T)
#define List_1_Add_m16996(__this, ___item, method) (( void (*) (List_1_t590 *, BaseInvokableCall_t583 *, const MethodInfo*))List_1_Add_m10660_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m16997(__this, ___newCount, method) (( void (*) (List_1_t590 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m10662_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m16998(__this, ___collection, method) (( void (*) (List_1_t590 *, Object_t*, const MethodInfo*))List_1_AddCollection_m10664_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m16999(__this, ___enumerable, method) (( void (*) (List_1_t590 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m10666_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m3397(__this, ___collection, method) (( void (*) (List_1_t590 *, Object_t*, const MethodInfo*))List_1_AddRange_m10668_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::AsReadOnly()
#define List_1_AsReadOnly_m17000(__this, method) (( ReadOnlyCollection_1_t2272 * (*) (List_1_t590 *, const MethodInfo*))List_1_AsReadOnly_m10670_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Clear()
#define List_1_Clear_m17001(__this, method) (( void (*) (List_1_t590 *, const MethodInfo*))List_1_Clear_m10672_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Contains(T)
#define List_1_Contains_m17002(__this, ___item, method) (( bool (*) (List_1_t590 *, BaseInvokableCall_t583 *, const MethodInfo*))List_1_Contains_m10674_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m17003(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t590 *, BaseInvokableCallU5BU5D_t2271*, int32_t, const MethodInfo*))List_1_CopyTo_m10676_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Find(System.Predicate`1<T>)
#define List_1_Find_m17004(__this, ___match, method) (( BaseInvokableCall_t583 * (*) (List_1_t590 *, Predicate_1_t704 *, const MethodInfo*))List_1_Find_m10678_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m17005(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t704 *, const MethodInfo*))List_1_CheckMatch_m10680_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m17006(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t590 *, int32_t, int32_t, Predicate_1_t704 *, const MethodInfo*))List_1_GetIndex_m10682_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::GetEnumerator()
#define List_1_GetEnumerator_m17007(__this, method) (( Enumerator_t2274  (*) (List_1_t590 *, const MethodInfo*))List_1_GetEnumerator_m10684_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::IndexOf(T)
#define List_1_IndexOf_m17008(__this, ___item, method) (( int32_t (*) (List_1_t590 *, BaseInvokableCall_t583 *, const MethodInfo*))List_1_IndexOf_m10686_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m17009(__this, ___start, ___delta, method) (( void (*) (List_1_t590 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m10688_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m17010(__this, ___index, method) (( void (*) (List_1_t590 *, int32_t, const MethodInfo*))List_1_CheckIndex_m10690_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Insert(System.Int32,T)
#define List_1_Insert_m17011(__this, ___index, ___item, method) (( void (*) (List_1_t590 *, int32_t, BaseInvokableCall_t583 *, const MethodInfo*))List_1_Insert_m10692_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m17012(__this, ___collection, method) (( void (*) (List_1_t590 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m10694_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Remove(T)
#define List_1_Remove_m17013(__this, ___item, method) (( bool (*) (List_1_t590 *, BaseInvokableCall_t583 *, const MethodInfo*))List_1_Remove_m10696_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m3396(__this, ___match, method) (( int32_t (*) (List_1_t590 *, Predicate_1_t704 *, const MethodInfo*))List_1_RemoveAll_m10698_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m17014(__this, ___index, method) (( void (*) (List_1_t590 *, int32_t, const MethodInfo*))List_1_RemoveAt_m10700_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Reverse()
#define List_1_Reverse_m17015(__this, method) (( void (*) (List_1_t590 *, const MethodInfo*))List_1_Reverse_m10702_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Sort()
#define List_1_Sort_m17016(__this, method) (( void (*) (List_1_t590 *, const MethodInfo*))List_1_Sort_m10704_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m17017(__this, ___comparison, method) (( void (*) (List_1_t590 *, Comparison_1_t2275 *, const MethodInfo*))List_1_Sort_m10706_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::ToArray()
#define List_1_ToArray_m17018(__this, method) (( BaseInvokableCallU5BU5D_t2271* (*) (List_1_t590 *, const MethodInfo*))List_1_ToArray_m10708_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::TrimExcess()
#define List_1_TrimExcess_m17019(__this, method) (( void (*) (List_1_t590 *, const MethodInfo*))List_1_TrimExcess_m10710_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Capacity()
#define List_1_get_Capacity_m17020(__this, method) (( int32_t (*) (List_1_t590 *, const MethodInfo*))List_1_get_Capacity_m10712_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m17021(__this, ___value, method) (( void (*) (List_1_t590 *, int32_t, const MethodInfo*))List_1_set_Capacity_m10714_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Count()
#define List_1_get_Count_m17022(__this, method) (( int32_t (*) (List_1_t590 *, const MethodInfo*))List_1_get_Count_m10716_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::get_Item(System.Int32)
#define List_1_get_Item_m17023(__this, ___index, method) (( BaseInvokableCall_t583 * (*) (List_1_t590 *, int32_t, const MethodInfo*))List_1_get_Item_m10718_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Events.BaseInvokableCall>::set_Item(System.Int32,T)
#define List_1_set_Item_m17024(__this, ___index, ___value, method) (( void (*) (List_1_t590 *, int32_t, BaseInvokableCall_t583 *, const MethodInfo*))List_1_set_Item_m10720_gshared)(__this, ___index, ___value, method)
