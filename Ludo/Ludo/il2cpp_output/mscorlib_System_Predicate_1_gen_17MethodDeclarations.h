﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen_4MethodDeclarations.h"

// System.Void System.Predicate`1<UnityEngine.UI.Selectable>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m13378(__this, ___object, ___method, method) (( void (*) (Predicate_1_t1999 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m10801_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.UI.Selectable>::Invoke(T)
#define Predicate_1_Invoke_m13379(__this, ___obj, method) (( bool (*) (Predicate_1_t1999 *, Selectable_t134 *, const MethodInfo*))Predicate_1_Invoke_m10802_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.UI.Selectable>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m13380(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t1999 *, Selectable_t134 *, AsyncCallback_t181 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m10803_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.UI.Selectable>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m13381(__this, ___result, method) (( bool (*) (Predicate_1_t1999 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m10804_gshared)(__this, ___result, method)
