﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerTwo
struct PlayerTwo_t12;
// System.Collections.IEnumerator
struct IEnumerator_t51;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayerTwo::.ctor()
extern "C" void PlayerTwo__ctor_m171 (PlayerTwo_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerTwo::Start()
extern "C" void PlayerTwo_Start_m172 (PlayerTwo_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerTwo::MoveThePlayer(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void PlayerTwo_MoveThePlayer_m173 (PlayerTwo_t12 * __this, int32_t ___x, int32_t ___y, int32_t ___a, int32_t ___b, int32_t ___playerPosition, int32_t ___diceNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerTwo::AttemptMove(System.Int32,System.Int32)
extern "C" void PlayerTwo_AttemptMove_m174 (PlayerTwo_t12 * __this, int32_t ___xDir, int32_t ___yDir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerTwo::SendPlayer2Position(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" Object_t * PlayerTwo_SendPlayer2Position_m175 (PlayerTwo_t12 * __this, int32_t ___a, int32_t ___b, int32_t ___playerPosition, int32_t ___diceNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerTwo::ChangeTransform(System.Single,System.Single)
extern "C" void PlayerTwo_ChangeTransform_m176 (PlayerTwo_t12 * __this, float ___x, float ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator PlayerTwo::GetPlayer2Position()
extern "C" Object_t * PlayerTwo_GetPlayer2Position_m177 (PlayerTwo_t12 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
