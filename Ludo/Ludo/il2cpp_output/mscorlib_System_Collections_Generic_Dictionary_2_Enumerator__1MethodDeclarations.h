﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m15852(__this, ___dictionary, method) (( void (*) (Enumerator_t655 *, Dictionary_2_t512 *, const MethodInfo*))Enumerator__ctor_m12484_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15853(__this, method) (( Object_t * (*) (Enumerator_t655 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12485_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15854(__this, method) (( DictionaryEntry_t1068  (*) (Enumerator_t655 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m12486_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15855(__this, method) (( Object_t * (*) (Enumerator_t655 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m12487_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15856(__this, method) (( Object_t * (*) (Enumerator_t655 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m12488_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::MoveNext()
#define Enumerator_MoveNext_m15857(__this, method) (( bool (*) (Enumerator_t655 *, const MethodInfo*))Enumerator_MoveNext_m12489_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::get_Current()
#define Enumerator_get_Current_m15858(__this, method) (( KeyValuePair_2_t618  (*) (Enumerator_t655 *, const MethodInfo*))Enumerator_get_Current_m12490_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m15859(__this, method) (( String_t* (*) (Enumerator_t655 *, const MethodInfo*))Enumerator_get_CurrentKey_m12491_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m15860(__this, method) (( Object_t * (*) (Enumerator_t655 *, const MethodInfo*))Enumerator_get_CurrentValue_m12492_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::VerifyState()
#define Enumerator_VerifyState_m15861(__this, method) (( void (*) (Enumerator_t655 *, const MethodInfo*))Enumerator_VerifyState_m12493_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m15862(__this, method) (( void (*) (Enumerator_t655 *, const MethodInfo*))Enumerator_VerifyCurrent_m12494_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Object>::Dispose()
#define Enumerator_Dispose_m15863(__this, method) (( void (*) (Enumerator_t655 *, const MethodInfo*))Enumerator_Dispose_m12495_gshared)(__this, method)
