﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// MovingObject
struct MovingObject_t36;

#include "mscorlib_System_Object.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// MovingObject/<SmoothMovement>c__IteratorB
struct  U3CSmoothMovementU3Ec__IteratorB_t34  : public Object_t
{
	// UnityEngine.Vector3 MovingObject/<SmoothMovement>c__IteratorB::end
	Vector3_t35  ___end_0;
	// System.Single MovingObject/<SmoothMovement>c__IteratorB::<sqrRemainingDistance>__0
	float ___U3CsqrRemainingDistanceU3E__0_1;
	// UnityEngine.Vector3 MovingObject/<SmoothMovement>c__IteratorB::<newPostion>__1
	Vector3_t35  ___U3CnewPostionU3E__1_2;
	// System.Int32 MovingObject/<SmoothMovement>c__IteratorB::$PC
	int32_t ___U24PC_3;
	// System.Object MovingObject/<SmoothMovement>c__IteratorB::$current
	Object_t * ___U24current_4;
	// UnityEngine.Vector3 MovingObject/<SmoothMovement>c__IteratorB::<$>end
	Vector3_t35  ___U3CU24U3Eend_5;
	// MovingObject MovingObject/<SmoothMovement>c__IteratorB::<>f__this
	MovingObject_t36 * ___U3CU3Ef__this_6;
};
