﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameManager/<GetPlayer1Turn>c__Iterator4
struct U3CGetPlayer1TurnU3Ec__Iterator4_t21;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void GameManager/<GetPlayer1Turn>c__Iterator4::.ctor()
extern "C" void U3CGetPlayer1TurnU3Ec__Iterator4__ctor_m46 (U3CGetPlayer1TurnU3Ec__Iterator4_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager/<GetPlayer1Turn>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CGetPlayer1TurnU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m47 (U3CGetPlayer1TurnU3Ec__Iterator4_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager/<GetPlayer1Turn>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetPlayer1TurnU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m48 (U3CGetPlayer1TurnU3Ec__Iterator4_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager/<GetPlayer1Turn>c__Iterator4::MoveNext()
extern "C" bool U3CGetPlayer1TurnU3Ec__Iterator4_MoveNext_m49 (U3CGetPlayer1TurnU3Ec__Iterator4_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager/<GetPlayer1Turn>c__Iterator4::Dispose()
extern "C" void U3CGetPlayer1TurnU3Ec__Iterator4_Dispose_m50 (U3CGetPlayer1TurnU3Ec__Iterator4_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager/<GetPlayer1Turn>c__Iterator4::Reset()
extern "C" void U3CGetPlayer1TurnU3Ec__Iterator4_Reset_m51 (U3CGetPlayer1TurnU3Ec__Iterator4_t21 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
