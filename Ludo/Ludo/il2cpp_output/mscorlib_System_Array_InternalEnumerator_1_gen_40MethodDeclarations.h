﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_40.h"
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Networking.Types.NetworkID>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15610_gshared (InternalEnumerator_1_t2157 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15610(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2157 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15610_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Networking.Types.NetworkID>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15611_gshared (InternalEnumerator_1_t2157 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15611(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2157 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15611_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Networking.Types.NetworkID>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15612_gshared (InternalEnumerator_1_t2157 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15612(__this, method) (( void (*) (InternalEnumerator_1_t2157 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15612_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Networking.Types.NetworkID>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15613_gshared (InternalEnumerator_1_t2157 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15613(__this, method) (( bool (*) (InternalEnumerator_1_t2157 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15613_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Networking.Types.NetworkID>::get_Current()
extern "C" uint64_t InternalEnumerator_1_get_Current_m15614_gshared (InternalEnumerator_1_t2157 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15614(__this, method) (( uint64_t (*) (InternalEnumerator_1_t2157 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15614_gshared)(__this, method)
