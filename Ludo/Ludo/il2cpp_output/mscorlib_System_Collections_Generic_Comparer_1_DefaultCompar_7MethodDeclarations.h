﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>
struct DefaultComparer_t2381;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTimeOffset.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::.ctor()
extern "C" void DefaultComparer__ctor_m17848_gshared (DefaultComparer_t2381 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m17848(__this, method) (( void (*) (DefaultComparer_t2381 *, const MethodInfo*))DefaultComparer__ctor_m17848_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.DateTimeOffset>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m17849_gshared (DefaultComparer_t2381 * __this, DateTimeOffset_t679  ___x, DateTimeOffset_t679  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m17849(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2381 *, DateTimeOffset_t679 , DateTimeOffset_t679 , const MethodInfo*))DefaultComparer_Compare_m17849_gshared)(__this, ___x, ___y, method)
