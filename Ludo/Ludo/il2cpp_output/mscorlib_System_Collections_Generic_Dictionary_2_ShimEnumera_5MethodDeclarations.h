﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
struct ShimEnumerator_t2249;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2236;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m16787_gshared (ShimEnumerator_t2249 * __this, Dictionary_2_t2236 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m16787(__this, ___host, method) (( void (*) (ShimEnumerator_t2249 *, Dictionary_2_t2236 *, const MethodInfo*))ShimEnumerator__ctor_m16787_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m16788_gshared (ShimEnumerator_t2249 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m16788(__this, method) (( bool (*) (ShimEnumerator_t2249 *, const MethodInfo*))ShimEnumerator_MoveNext_m16788_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Entry()
extern "C" DictionaryEntry_t1068  ShimEnumerator_get_Entry_m16789_gshared (ShimEnumerator_t2249 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m16789(__this, method) (( DictionaryEntry_t1068  (*) (ShimEnumerator_t2249 *, const MethodInfo*))ShimEnumerator_get_Entry_m16789_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m16790_gshared (ShimEnumerator_t2249 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m16790(__this, method) (( Object_t * (*) (ShimEnumerator_t2249 *, const MethodInfo*))ShimEnumerator_get_Key_m16790_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m16791_gshared (ShimEnumerator_t2249 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m16791(__this, method) (( Object_t * (*) (ShimEnumerator_t2249 *, const MethodInfo*))ShimEnumerator_get_Value_m16791_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m16792_gshared (ShimEnumerator_t2249 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m16792(__this, method) (( Object_t * (*) (ShimEnumerator_t2249 *, const MethodInfo*))ShimEnumerator_get_Current_m16792_gshared)(__this, method)
