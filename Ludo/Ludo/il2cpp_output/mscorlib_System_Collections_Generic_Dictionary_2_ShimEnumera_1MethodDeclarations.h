﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>
struct ShimEnumerator_t1943;
// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t1932;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m12527_gshared (ShimEnumerator_t1943 * __this, Dictionary_2_t1932 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m12527(__this, ___host, method) (( void (*) (ShimEnumerator_t1943 *, Dictionary_2_t1932 *, const MethodInfo*))ShimEnumerator__ctor_m12527_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m12528_gshared (ShimEnumerator_t1943 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m12528(__this, method) (( bool (*) (ShimEnumerator_t1943 *, const MethodInfo*))ShimEnumerator_MoveNext_m12528_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Entry()
extern "C" DictionaryEntry_t1068  ShimEnumerator_get_Entry_m12529_gshared (ShimEnumerator_t1943 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m12529(__this, method) (( DictionaryEntry_t1068  (*) (ShimEnumerator_t1943 *, const MethodInfo*))ShimEnumerator_get_Entry_m12529_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m12530_gshared (ShimEnumerator_t1943 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m12530(__this, method) (( Object_t * (*) (ShimEnumerator_t1943 *, const MethodInfo*))ShimEnumerator_get_Key_m12530_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m12531_gshared (ShimEnumerator_t1943 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m12531(__this, method) (( Object_t * (*) (ShimEnumerator_t1943 *, const MethodInfo*))ShimEnumerator_get_Value_m12531_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m12532_gshared (ShimEnumerator_t1943 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m12532(__this, method) (( Object_t * (*) (ShimEnumerator_t1943 *, const MethodInfo*))ShimEnumerator_get_Current_m12532_gshared)(__this, method)
