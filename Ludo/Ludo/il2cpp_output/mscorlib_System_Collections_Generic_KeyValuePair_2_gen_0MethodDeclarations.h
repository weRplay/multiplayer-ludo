﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Object>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m15770(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t618 *, String_t*, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m12459_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Object>::get_Key()
#define KeyValuePair_2_get_Key_m3322(__this, method) (( String_t* (*) (KeyValuePair_2_t618 *, const MethodInfo*))KeyValuePair_2_get_Key_m12460_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Object>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m15771(__this, ___value, method) (( void (*) (KeyValuePair_2_t618 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m12461_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Object>::get_Value()
#define KeyValuePair_2_get_Value_m3323(__this, method) (( Object_t * (*) (KeyValuePair_2_t618 *, const MethodInfo*))KeyValuePair_2_get_Value_m12462_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Object>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m15772(__this, ___value, method) (( void (*) (KeyValuePair_2_t618 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m12463_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Object>::ToString()
#define KeyValuePair_2_ToString_m15773(__this, method) (( String_t* (*) (KeyValuePair_2_t618 *, const MethodInfo*))KeyValuePair_2_ToString_m12464_gshared)(__this, method)
