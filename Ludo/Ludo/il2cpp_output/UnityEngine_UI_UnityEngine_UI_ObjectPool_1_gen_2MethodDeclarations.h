﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
#define ObjectPool_1__ctor_m1977(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t257 *, UnityAction_1_t258 *, UnityAction_1_t258 *, const MethodInfo*))ObjectPool_1__ctor_m10962_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countAll()
#define ObjectPool_1_get_countAll_m13811(__this, method) (( int32_t (*) (ObjectPool_1_t257 *, const MethodInfo*))ObjectPool_1_get_countAll_m10964_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m13812(__this, ___value, method) (( void (*) (ObjectPool_1_t257 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m10966_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countActive()
#define ObjectPool_1_get_countActive_m13813(__this, method) (( int32_t (*) (ObjectPool_1_t257 *, const MethodInfo*))ObjectPool_1_get_countActive_m10968_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m13814(__this, method) (( int32_t (*) (ObjectPool_1_t257 *, const MethodInfo*))ObjectPool_1_get_countInactive_m10970_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Get()
#define ObjectPool_1_Get_m1978(__this, method) (( List_1_t292 * (*) (ObjectPool_1_t257 *, const MethodInfo*))ObjectPool_1_Get_m10972_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Component>>::Release(T)
#define ObjectPool_1_Release_m1979(__this, ___element, method) (( void (*) (ObjectPool_1_t257 *, List_1_t292 *, const MethodInfo*))ObjectPool_1_Release_m10974_gshared)(__this, ___element, method)
