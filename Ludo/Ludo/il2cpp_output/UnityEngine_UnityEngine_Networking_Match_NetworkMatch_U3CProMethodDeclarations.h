﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>
struct U3CProcessMatchResponseU3Ec__Iterator0_1_t2173;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::.ctor()
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m15753_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t2173 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m15753(__this, method) (( void (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t2173 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1__ctor_m15753_gshared)(__this, method)
// System.Object UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15754_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t2173 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15754(__this, method) (( Object_t * (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t2173 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m15754_gshared)(__this, method)
// System.Object UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m15755_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t2173 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m15755(__this, method) (( Object_t * (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t2173 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_System_Collections_IEnumerator_get_Current_m15755_gshared)(__this, method)
// System.Boolean UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::MoveNext()
extern "C" bool U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m15756_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t2173 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m15756(__this, method) (( bool (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t2173 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_MoveNext_m15756_gshared)(__this, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/<ProcessMatchResponse>c__Iterator0`1<System.Object>::Dispose()
extern "C" void U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m15757_gshared (U3CProcessMatchResponseU3Ec__Iterator0_1_t2173 * __this, const MethodInfo* method);
#define U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m15757(__this, method) (( void (*) (U3CProcessMatchResponseU3Ec__Iterator0_1_t2173 *, const MethodInfo*))U3CProcessMatchResponseU3Ec__Iterator0_1_Dispose_m15757_gshared)(__this, method)
