﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen_4MethodDeclarations.h"

// System.Void System.Comparison`1<UnityEngine.UI.Text>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m12450(__this, ___object, ___method, method) (( void (*) (Comparison_1_t1931 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m10811_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.UI.Text>::Invoke(T,T)
#define Comparison_1_Invoke_m12451(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t1931 *, Text_t47 *, Text_t47 *, const MethodInfo*))Comparison_1_Invoke_m10812_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.UI.Text>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m12452(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t1931 *, Text_t47 *, Text_t47 *, AsyncCallback_t181 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m10813_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.UI.Text>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m12453(__this, ___result, method) (( int32_t (*) (Comparison_1_t1931 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m10814_gshared)(__this, ___result, method)
