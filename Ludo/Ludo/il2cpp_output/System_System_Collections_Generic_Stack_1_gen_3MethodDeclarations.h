﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::.ctor()
#define Stack_1__ctor_m13797(__this, method) (( void (*) (Stack_1_t2030 *, const MethodInfo*))Stack_1__ctor_m10975_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m13798(__this, method) (( bool (*) (Stack_1_t2030 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m10976_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m13799(__this, method) (( Object_t * (*) (Stack_1_t2030 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m10977_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m13800(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t2030 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m10978_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13801(__this, method) (( Object_t* (*) (Stack_1_t2030 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m10979_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m13802(__this, method) (( Object_t * (*) (Stack_1_t2030 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m10980_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Peek()
#define Stack_1_Peek_m13803(__this, method) (( List_1_t294 * (*) (Stack_1_t2030 *, const MethodInfo*))Stack_1_Peek_m10981_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Pop()
#define Stack_1_Pop_m13804(__this, method) (( List_1_t294 * (*) (Stack_1_t2030 *, const MethodInfo*))Stack_1_Pop_m10982_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::Push(T)
#define Stack_1_Push_m13805(__this, ___t, method) (( void (*) (Stack_1_t2030 *, List_1_t294 *, const MethodInfo*))Stack_1_Push_m10983_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::get_Count()
#define Stack_1_get_Count_m13806(__this, method) (( int32_t (*) (Stack_1_t2030 *, const MethodInfo*))Stack_1_get_Count_m10984_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.Canvas>>::GetEnumerator()
#define Stack_1_GetEnumerator_m13807(__this, method) (( Enumerator_t2428  (*) (Stack_1_t2030 *, const MethodInfo*))Stack_1_GetEnumerator_m10985_gshared)(__this, method)
