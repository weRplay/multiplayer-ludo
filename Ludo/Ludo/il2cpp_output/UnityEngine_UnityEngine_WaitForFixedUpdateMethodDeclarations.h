﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.WaitForFixedUpdate
struct WaitForFixedUpdate_t366;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.WaitForFixedUpdate::.ctor()
extern "C" void WaitForFixedUpdate__ctor_m1998 (WaitForFixedUpdate_t366 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
