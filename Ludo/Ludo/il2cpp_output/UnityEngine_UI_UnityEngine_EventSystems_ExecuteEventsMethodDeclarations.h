﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.EventSystems.IPointerEnterHandler
struct IPointerEnterHandler_t264;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t71;
// UnityEngine.EventSystems.IPointerExitHandler
struct IPointerExitHandler_t265;
// UnityEngine.EventSystems.IPointerDownHandler
struct IPointerDownHandler_t266;
// UnityEngine.EventSystems.IPointerUpHandler
struct IPointerUpHandler_t267;
// UnityEngine.EventSystems.IPointerClickHandler
struct IPointerClickHandler_t268;
// UnityEngine.EventSystems.IInitializePotentialDragHandler
struct IInitializePotentialDragHandler_t269;
// UnityEngine.EventSystems.IBeginDragHandler
struct IBeginDragHandler_t270;
// UnityEngine.EventSystems.IDragHandler
struct IDragHandler_t271;
// UnityEngine.EventSystems.IEndDragHandler
struct IEndDragHandler_t272;
// UnityEngine.EventSystems.IDropHandler
struct IDropHandler_t273;
// UnityEngine.EventSystems.IScrollHandler
struct IScrollHandler_t274;
// UnityEngine.EventSystems.IUpdateSelectedHandler
struct IUpdateSelectedHandler_t275;
// UnityEngine.EventSystems.ISelectHandler
struct ISelectHandler_t276;
// UnityEngine.EventSystems.IDeselectHandler
struct IDeselectHandler_t277;
// UnityEngine.EventSystems.IMoveHandler
struct IMoveHandler_t278;
// UnityEngine.EventSystems.ISubmitHandler
struct ISubmitHandler_t279;
// UnityEngine.EventSystems.ICancelHandler
struct ICancelHandler_t280;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler>
struct EventFunction_1_t80;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler>
struct EventFunction_1_t81;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler>
struct EventFunction_1_t82;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler>
struct EventFunction_1_t83;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler>
struct EventFunction_1_t84;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler>
struct EventFunction_1_t85;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler>
struct EventFunction_1_t86;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler>
struct EventFunction_1_t87;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler>
struct EventFunction_1_t88;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler>
struct EventFunction_1_t89;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler>
struct EventFunction_1_t90;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler>
struct EventFunction_1_t91;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler>
struct EventFunction_1_t92;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler>
struct EventFunction_1_t93;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler>
struct EventFunction_1_t94;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler>
struct EventFunction_1_t95;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler>
struct EventFunction_1_t96;
// UnityEngine.GameObject
struct GameObject_t5;
// System.Collections.Generic.IList`1<UnityEngine.Transform>
struct IList_1_t281;
// System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>
struct List_1_t263;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.EventSystems.ExecuteEvents::.cctor()
extern "C" void ExecuteEvents__cctor_m328 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IPointerEnterHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m329 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t71 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IPointerExitHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m330 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t71 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IPointerDownHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m331 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t71 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IPointerUpHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m332 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t71 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IPointerClickHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m333 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t71 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IInitializePotentialDragHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m334 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t71 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IBeginDragHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m335 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t71 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IDragHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m336 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t71 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IEndDragHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m337 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t71 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IDropHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m338 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t71 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IScrollHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m339 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t71 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IUpdateSelectedHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m340 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t71 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.ISelectHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m341 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t71 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IDeselectHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m342 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t71 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.IMoveHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m343 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t71 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.ISubmitHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m344 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t71 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::Execute(UnityEngine.EventSystems.ICancelHandler,UnityEngine.EventSystems.BaseEventData)
extern "C" void ExecuteEvents_Execute_m345 (Object_t * __this /* static, unused */, Object_t * ___handler, BaseEventData_t71 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerEnterHandler> UnityEngine.EventSystems.ExecuteEvents::get_pointerEnterHandler()
extern "C" EventFunction_1_t80 * ExecuteEvents_get_pointerEnterHandler_m346 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerExitHandler> UnityEngine.EventSystems.ExecuteEvents::get_pointerExitHandler()
extern "C" EventFunction_1_t81 * ExecuteEvents_get_pointerExitHandler_m347 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerDownHandler> UnityEngine.EventSystems.ExecuteEvents::get_pointerDownHandler()
extern "C" EventFunction_1_t82 * ExecuteEvents_get_pointerDownHandler_m348 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerUpHandler> UnityEngine.EventSystems.ExecuteEvents::get_pointerUpHandler()
extern "C" EventFunction_1_t83 * ExecuteEvents_get_pointerUpHandler_m349 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IPointerClickHandler> UnityEngine.EventSystems.ExecuteEvents::get_pointerClickHandler()
extern "C" EventFunction_1_t84 * ExecuteEvents_get_pointerClickHandler_m350 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IInitializePotentialDragHandler> UnityEngine.EventSystems.ExecuteEvents::get_initializePotentialDrag()
extern "C" EventFunction_1_t85 * ExecuteEvents_get_initializePotentialDrag_m351 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IBeginDragHandler> UnityEngine.EventSystems.ExecuteEvents::get_beginDragHandler()
extern "C" EventFunction_1_t86 * ExecuteEvents_get_beginDragHandler_m352 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler> UnityEngine.EventSystems.ExecuteEvents::get_dragHandler()
extern "C" EventFunction_1_t87 * ExecuteEvents_get_dragHandler_m353 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IEndDragHandler> UnityEngine.EventSystems.ExecuteEvents::get_endDragHandler()
extern "C" EventFunction_1_t88 * ExecuteEvents_get_endDragHandler_m354 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDropHandler> UnityEngine.EventSystems.ExecuteEvents::get_dropHandler()
extern "C" EventFunction_1_t89 * ExecuteEvents_get_dropHandler_m355 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IScrollHandler> UnityEngine.EventSystems.ExecuteEvents::get_scrollHandler()
extern "C" EventFunction_1_t90 * ExecuteEvents_get_scrollHandler_m356 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IUpdateSelectedHandler> UnityEngine.EventSystems.ExecuteEvents::get_updateSelectedHandler()
extern "C" EventFunction_1_t91 * ExecuteEvents_get_updateSelectedHandler_m357 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISelectHandler> UnityEngine.EventSystems.ExecuteEvents::get_selectHandler()
extern "C" EventFunction_1_t92 * ExecuteEvents_get_selectHandler_m358 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDeselectHandler> UnityEngine.EventSystems.ExecuteEvents::get_deselectHandler()
extern "C" EventFunction_1_t93 * ExecuteEvents_get_deselectHandler_m359 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler> UnityEngine.EventSystems.ExecuteEvents::get_moveHandler()
extern "C" EventFunction_1_t94 * ExecuteEvents_get_moveHandler_m360 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ISubmitHandler> UnityEngine.EventSystems.ExecuteEvents::get_submitHandler()
extern "C" EventFunction_1_t95 * ExecuteEvents_get_submitHandler_m361 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.ICancelHandler> UnityEngine.EventSystems.ExecuteEvents::get_cancelHandler()
extern "C" EventFunction_1_t96 * ExecuteEvents_get_cancelHandler_m362 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::GetEventChain(UnityEngine.GameObject,System.Collections.Generic.IList`1<UnityEngine.Transform>)
extern "C" void ExecuteEvents_GetEventChain_m363 (Object_t * __this /* static, unused */, GameObject_t5 * ___root, Object_t* ___eventChain, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.ExecuteEvents::<s_HandlerListPool>m__0(System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>)
extern "C" void ExecuteEvents_U3Cs_HandlerListPoolU3Em__0_m364 (Object_t * __this /* static, unused */, List_1_t263 * ___l, const MethodInfo* method) IL2CPP_METHOD_ATTR;
