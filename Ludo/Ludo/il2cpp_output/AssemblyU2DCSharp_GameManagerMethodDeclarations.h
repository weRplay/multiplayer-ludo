﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameManager
struct GameManager_t22;
// System.Collections.IEnumerator
struct IEnumerator_t51;

#include "codegen/il2cpp-codegen.h"

// System.Void GameManager::.ctor()
extern "C" void GameManager__ctor_m70 (GameManager_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::.cctor()
extern "C" void GameManager__cctor_m71 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::Awake()
extern "C" void GameManager_Awake_m72 (GameManager_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::Start()
extern "C" void GameManager_Start_m73 (GameManager_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::InitGame()
extern "C" void GameManager_InitGame_m74 (GameManager_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::GameOver()
extern "C" void GameManager_GameOver_m75 (GameManager_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::GetNewPosition(System.Int32)
extern "C" void GameManager_GetNewPosition_m76 (GameManager_t22 * __this, int32_t ___DiceRoll, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::RollDice()
extern "C" void GameManager_RollDice_m77 (GameManager_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::Move_Dice()
extern "C" void GameManager_Move_Dice_m78 (GameManager_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::Move_OpponentsDice()
extern "C" void GameManager_Move_OpponentsDice_m79 (GameManager_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::Stop_Dice()
extern "C" void GameManager_Stop_Dice_m80 (GameManager_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::Stop_OpponentsDice()
extern "C" void GameManager_Stop_OpponentsDice_m81 (GameManager_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::Update()
extern "C" void GameManager_Update_m82 (GameManager_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::ResetButtonPressed()
extern "C" void GameManager_ResetButtonPressed_m83 (GameManager_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameManager::ResetBoard()
extern "C" Object_t * GameManager_ResetBoard_m84 (GameManager_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager::CallGetPlayerTurn()
extern "C" void GameManager_CallGetPlayerTurn_m85 (GameManager_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameManager::GetPlayer1Turn()
extern "C" Object_t * GameManager_GetPlayer1Turn_m86 (GameManager_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameManager::GetPlayer2Turn()
extern "C" Object_t * GameManager_GetPlayer2Turn_m87 (GameManager_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameManager::AutomaticMovePlayer1()
extern "C" Object_t * GameManager_AutomaticMovePlayer1_m88 (GameManager_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameManager::AutomaticMovePlayer2()
extern "C" Object_t * GameManager_AutomaticMovePlayer2_m89 (GameManager_t22 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
