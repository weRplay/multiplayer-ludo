﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameManager/<GetPlayer2Turn>c__Iterator5
struct U3CGetPlayer2TurnU3Ec__Iterator5_t23;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void GameManager/<GetPlayer2Turn>c__Iterator5::.ctor()
extern "C" void U3CGetPlayer2TurnU3Ec__Iterator5__ctor_m52 (U3CGetPlayer2TurnU3Ec__Iterator5_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager/<GetPlayer2Turn>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CGetPlayer2TurnU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m53 (U3CGetPlayer2TurnU3Ec__Iterator5_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager/<GetPlayer2Turn>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetPlayer2TurnU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m54 (U3CGetPlayer2TurnU3Ec__Iterator5_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager/<GetPlayer2Turn>c__Iterator5::MoveNext()
extern "C" bool U3CGetPlayer2TurnU3Ec__Iterator5_MoveNext_m55 (U3CGetPlayer2TurnU3Ec__Iterator5_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager/<GetPlayer2Turn>c__Iterator5::Dispose()
extern "C" void U3CGetPlayer2TurnU3Ec__Iterator5_Dispose_m56 (U3CGetPlayer2TurnU3Ec__Iterator5_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager/<GetPlayer2Turn>c__Iterator5::Reset()
extern "C" void U3CGetPlayer2TurnU3Ec__Iterator5_Reset_m57 (U3CGetPlayer2TurnU3Ec__Iterator5_t23 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
