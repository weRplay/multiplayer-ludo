﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>
struct Dictionary_2_t2153;
// System.Collections.Generic.IEqualityComparer`1<UnityEngine.Networking.Types.NetworkID>
struct IEqualityComparer_1_t2152;
// System.Collections.Generic.IDictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>
struct IDictionary_2_t2456;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t631;
// System.Collections.Generic.ICollection`1<UnityEngine.Networking.Types.NetworkID>
struct ICollection_1_t2457;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t621;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>[]
struct KeyValuePair_2U5BU5D_t2455;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t51;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>>
struct IEnumerator_1_t2458;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1067;
// System.Collections.Generic.Dictionary`2/KeyCollection<UnityEngine.Networking.Types.NetworkID,System.Object>
struct KeyCollection_t2158;
// System.Collections.Generic.Dictionary`2/ValueCollection<UnityEngine.Networking.Types.NetworkID,System.Object>
struct ValueCollection_t2162;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m15500_gshared (Dictionary_2_t2153 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m15500(__this, method) (( void (*) (Dictionary_2_t2153 *, const MethodInfo*))Dictionary_2__ctor_m15500_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m15502_gshared (Dictionary_2_t2153 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m15502(__this, ___comparer, method) (( void (*) (Dictionary_2_t2153 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m15502_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m15504_gshared (Dictionary_2_t2153 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m15504(__this, ___dictionary, method) (( void (*) (Dictionary_2_t2153 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m15504_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m15506_gshared (Dictionary_2_t2153 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m15506(__this, ___capacity, method) (( void (*) (Dictionary_2_t2153 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m15506_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m15508_gshared (Dictionary_2_t2153 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m15508(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t2153 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m15508_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m15510_gshared (Dictionary_2_t2153 * __this, SerializationInfo_t631 * ___info, StreamingContext_t632  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m15510(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2153 *, SerializationInfo_t631 *, StreamingContext_t632 , const MethodInfo*))Dictionary_2__ctor_m15510_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m15512_gshared (Dictionary_2_t2153 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m15512(__this, method) (( Object_t* (*) (Dictionary_2_t2153 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m15512_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m15514_gshared (Dictionary_2_t2153 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m15514(__this, method) (( Object_t* (*) (Dictionary_2_t2153 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m15514_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m15516_gshared (Dictionary_2_t2153 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m15516(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2153 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m15516_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m15518_gshared (Dictionary_2_t2153 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m15518(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2153 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m15518_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m15520_gshared (Dictionary_2_t2153 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m15520(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2153 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m15520_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m15522_gshared (Dictionary_2_t2153 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m15522(__this, ___key, method) (( bool (*) (Dictionary_2_t2153 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m15522_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m15524_gshared (Dictionary_2_t2153 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m15524(__this, ___key, method) (( void (*) (Dictionary_2_t2153 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m15524_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15526_gshared (Dictionary_2_t2153 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15526(__this, method) (( bool (*) (Dictionary_2_t2153 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15526_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15528_gshared (Dictionary_2_t2153 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15528(__this, method) (( Object_t * (*) (Dictionary_2_t2153 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15528_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15530_gshared (Dictionary_2_t2153 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15530(__this, method) (( bool (*) (Dictionary_2_t2153 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15530_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15532_gshared (Dictionary_2_t2153 * __this, KeyValuePair_2_t2155  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15532(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2153 *, KeyValuePair_2_t2155 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15532_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15534_gshared (Dictionary_2_t2153 * __this, KeyValuePair_2_t2155  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15534(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2153 *, KeyValuePair_2_t2155 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15534_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15536_gshared (Dictionary_2_t2153 * __this, KeyValuePair_2U5BU5D_t2455* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15536(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2153 *, KeyValuePair_2U5BU5D_t2455*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15536_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15538_gshared (Dictionary_2_t2153 * __this, KeyValuePair_2_t2155  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15538(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2153 *, KeyValuePair_2_t2155 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15538_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m15540_gshared (Dictionary_2_t2153 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m15540(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2153 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m15540_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15542_gshared (Dictionary_2_t2153 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15542(__this, method) (( Object_t * (*) (Dictionary_2_t2153 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15542_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15544_gshared (Dictionary_2_t2153 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15544(__this, method) (( Object_t* (*) (Dictionary_2_t2153 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15544_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15546_gshared (Dictionary_2_t2153 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15546(__this, method) (( Object_t * (*) (Dictionary_2_t2153 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15546_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m15548_gshared (Dictionary_2_t2153 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m15548(__this, method) (( int32_t (*) (Dictionary_2_t2153 *, const MethodInfo*))Dictionary_2_get_Count_m15548_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m15550_gshared (Dictionary_2_t2153 * __this, uint64_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m15550(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2153 *, uint64_t, const MethodInfo*))Dictionary_2_get_Item_m15550_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m15552_gshared (Dictionary_2_t2153 * __this, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m15552(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2153 *, uint64_t, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m15552_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m15554_gshared (Dictionary_2_t2153 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m15554(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2153 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m15554_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m15556_gshared (Dictionary_2_t2153 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m15556(__this, ___size, method) (( void (*) (Dictionary_2_t2153 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m15556_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m15558_gshared (Dictionary_2_t2153 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m15558(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2153 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m15558_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2155  Dictionary_2_make_pair_m15560_gshared (Object_t * __this /* static, unused */, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m15560(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2155  (*) (Object_t * /* static, unused */, uint64_t, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m15560_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::pick_key(TKey,TValue)
extern "C" uint64_t Dictionary_2_pick_key_m15562_gshared (Object_t * __this /* static, unused */, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m15562(__this /* static, unused */, ___key, ___value, method) (( uint64_t (*) (Object_t * /* static, unused */, uint64_t, Object_t *, const MethodInfo*))Dictionary_2_pick_key_m15562_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m15564_gshared (Object_t * __this /* static, unused */, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m15564(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, uint64_t, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m15564_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m15566_gshared (Dictionary_2_t2153 * __this, KeyValuePair_2U5BU5D_t2455* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m15566(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2153 *, KeyValuePair_2U5BU5D_t2455*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m15566_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m15568_gshared (Dictionary_2_t2153 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m15568(__this, method) (( void (*) (Dictionary_2_t2153 *, const MethodInfo*))Dictionary_2_Resize_m15568_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m15570_gshared (Dictionary_2_t2153 * __this, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m15570(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2153 *, uint64_t, Object_t *, const MethodInfo*))Dictionary_2_Add_m15570_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m15572_gshared (Dictionary_2_t2153 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m15572(__this, method) (( void (*) (Dictionary_2_t2153 *, const MethodInfo*))Dictionary_2_Clear_m15572_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m15574_gshared (Dictionary_2_t2153 * __this, uint64_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m15574(__this, ___key, method) (( bool (*) (Dictionary_2_t2153 *, uint64_t, const MethodInfo*))Dictionary_2_ContainsKey_m15574_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m15576_gshared (Dictionary_2_t2153 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m15576(__this, ___value, method) (( bool (*) (Dictionary_2_t2153 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m15576_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m15578_gshared (Dictionary_2_t2153 * __this, SerializationInfo_t631 * ___info, StreamingContext_t632  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m15578(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2153 *, SerializationInfo_t631 *, StreamingContext_t632 , const MethodInfo*))Dictionary_2_GetObjectData_m15578_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m15580_gshared (Dictionary_2_t2153 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m15580(__this, ___sender, method) (( void (*) (Dictionary_2_t2153 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m15580_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m15582_gshared (Dictionary_2_t2153 * __this, uint64_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m15582(__this, ___key, method) (( bool (*) (Dictionary_2_t2153 *, uint64_t, const MethodInfo*))Dictionary_2_Remove_m15582_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m15584_gshared (Dictionary_2_t2153 * __this, uint64_t ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m15584(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2153 *, uint64_t, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m15584_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Keys()
extern "C" KeyCollection_t2158 * Dictionary_2_get_Keys_m15586_gshared (Dictionary_2_t2153 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m15586(__this, method) (( KeyCollection_t2158 * (*) (Dictionary_2_t2153 *, const MethodInfo*))Dictionary_2_get_Keys_m15586_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Values()
extern "C" ValueCollection_t2162 * Dictionary_2_get_Values_m15588_gshared (Dictionary_2_t2153 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m15588(__this, method) (( ValueCollection_t2162 * (*) (Dictionary_2_t2153 *, const MethodInfo*))Dictionary_2_get_Values_m15588_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::ToTKey(System.Object)
extern "C" uint64_t Dictionary_2_ToTKey_m15590_gshared (Dictionary_2_t2153 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m15590(__this, ___key, method) (( uint64_t (*) (Dictionary_2_t2153 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m15590_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m15592_gshared (Dictionary_2_t2153 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m15592(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t2153 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m15592_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m15594_gshared (Dictionary_2_t2153 * __this, KeyValuePair_2_t2155  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m15594(__this, ___pair, method) (( bool (*) (Dictionary_2_t2153 *, KeyValuePair_2_t2155 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m15594_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::GetEnumerator()
extern "C" Enumerator_t2160  Dictionary_2_GetEnumerator_m15596_gshared (Dictionary_2_t2153 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m15596(__this, method) (( Enumerator_t2160  (*) (Dictionary_2_t2153 *, const MethodInfo*))Dictionary_2_GetEnumerator_m15596_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1068  Dictionary_2_U3CCopyToU3Em__0_m15598_gshared (Object_t * __this /* static, unused */, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m15598(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1068  (*) (Object_t * /* static, unused */, uint64_t, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m15598_gshared)(__this /* static, unused */, ___key, ___value, method)
