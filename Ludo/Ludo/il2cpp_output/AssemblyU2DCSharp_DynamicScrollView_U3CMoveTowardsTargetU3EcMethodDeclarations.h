﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DynamicScrollView/<MoveTowardsTarget>c__Iterator1
struct U3CMoveTowardsTargetU3Ec__Iterator1_t13;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void DynamicScrollView/<MoveTowardsTarget>c__Iterator1::.ctor()
extern "C" void U3CMoveTowardsTargetU3Ec__Iterator1__ctor_m18 (U3CMoveTowardsTargetU3Ec__Iterator1_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DynamicScrollView/<MoveTowardsTarget>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CMoveTowardsTargetU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m19 (U3CMoveTowardsTargetU3Ec__Iterator1_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object DynamicScrollView/<MoveTowardsTarget>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CMoveTowardsTargetU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m20 (U3CMoveTowardsTargetU3Ec__Iterator1_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean DynamicScrollView/<MoveTowardsTarget>c__Iterator1::MoveNext()
extern "C" bool U3CMoveTowardsTargetU3Ec__Iterator1_MoveNext_m21 (U3CMoveTowardsTargetU3Ec__Iterator1_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicScrollView/<MoveTowardsTarget>c__Iterator1::Dispose()
extern "C" void U3CMoveTowardsTargetU3Ec__Iterator1_Dispose_m22 (U3CMoveTowardsTargetU3Ec__Iterator1_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicScrollView/<MoveTowardsTarget>c__Iterator1::Reset()
extern "C" void U3CMoveTowardsTargetU3Ec__Iterator1_Reset_m23 (U3CMoveTowardsTargetU3Ec__Iterator1_t13 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
