﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Object>
struct List_1_t344;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m10721_gshared (Enumerator_t1802 * __this, List_1_t344 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m10721(__this, ___l, method) (( void (*) (Enumerator_t1802 *, List_1_t344 *, const MethodInfo*))Enumerator__ctor_m10721_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m10722_gshared (Enumerator_t1802 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m10722(__this, method) (( Object_t * (*) (Enumerator_t1802 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m10722_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m10723_gshared (Enumerator_t1802 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m10723(__this, method) (( void (*) (Enumerator_t1802 *, const MethodInfo*))Enumerator_Dispose_m10723_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m10724_gshared (Enumerator_t1802 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m10724(__this, method) (( void (*) (Enumerator_t1802 *, const MethodInfo*))Enumerator_VerifyState_m10724_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m10725_gshared (Enumerator_t1802 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m10725(__this, method) (( bool (*) (Enumerator_t1802 *, const MethodInfo*))Enumerator_MoveNext_m10725_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m10726_gshared (Enumerator_t1802 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m10726(__this, method) (( Object_t * (*) (Enumerator_t1802 *, const MethodInfo*))Enumerator_get_Current_m10726_gshared)(__this, method)
