﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CountdownTimer
struct CountdownTimer_t10;
// System.Collections.IEnumerator
struct IEnumerator_t51;

#include "codegen/il2cpp-codegen.h"

// System.Void CountdownTimer::.ctor()
extern "C" void CountdownTimer__ctor_m12 (CountdownTimer_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer::.cctor()
extern "C" void CountdownTimer__cctor_m13 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer::Start()
extern "C" void CountdownTimer_Start_m14 (CountdownTimer_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer::Update()
extern "C" void CountdownTimer_Update_m15 (CountdownTimer_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer::ShiftTimer()
extern "C" void CountdownTimer_ShiftTimer_m16 (CountdownTimer_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator CountdownTimer::AutomaticTurn()
extern "C" Object_t * CountdownTimer_AutomaticTurn_m17 (CountdownTimer_t10 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
