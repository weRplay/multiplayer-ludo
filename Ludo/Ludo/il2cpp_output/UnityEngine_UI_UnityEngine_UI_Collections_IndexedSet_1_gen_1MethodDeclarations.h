﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Collections.IndexedSet`1<System.Object>
struct IndexedSet_1_t1904;
// System.Collections.IEnumerator
struct IEnumerator_t51;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2022;
// System.Object[]
struct ObjectU5BU5D_t60;
// System.Predicate`1<System.Object>
struct Predicate_1_t1808;
// System.Comparison`1<System.Object>
struct Comparison_1_t1811;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::.ctor()
extern "C" void IndexedSet_1__ctor_m12042_gshared (IndexedSet_1_t1904 * __this, const MethodInfo* method);
#define IndexedSet_1__ctor_m12042(__this, method) (( void (*) (IndexedSet_1_t1904 *, const MethodInfo*))IndexedSet_1__ctor_m12042_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m12044_gshared (IndexedSet_1_t1904 * __this, const MethodInfo* method);
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m12044(__this, method) (( Object_t * (*) (IndexedSet_1_t1904 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m12044_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Add(T)
extern "C" void IndexedSet_1_Add_m12046_gshared (IndexedSet_1_t1904 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Add_m12046(__this, ___item, method) (( void (*) (IndexedSet_1_t1904 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m12046_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Remove(T)
extern "C" bool IndexedSet_1_Remove_m12048_gshared (IndexedSet_1_t1904 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Remove_m12048(__this, ___item, method) (( bool (*) (IndexedSet_1_t1904 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m12048_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<System.Object>::GetEnumerator()
extern "C" Object_t* IndexedSet_1_GetEnumerator_m12050_gshared (IndexedSet_1_t1904 * __this, const MethodInfo* method);
#define IndexedSet_1_GetEnumerator_m12050(__this, method) (( Object_t* (*) (IndexedSet_1_t1904 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m12050_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Clear()
extern "C" void IndexedSet_1_Clear_m12052_gshared (IndexedSet_1_t1904 * __this, const MethodInfo* method);
#define IndexedSet_1_Clear_m12052(__this, method) (( void (*) (IndexedSet_1_t1904 *, const MethodInfo*))IndexedSet_1_Clear_m12052_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Contains(T)
extern "C" bool IndexedSet_1_Contains_m12054_gshared (IndexedSet_1_t1904 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Contains_m12054(__this, ___item, method) (( bool (*) (IndexedSet_1_t1904 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m12054_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void IndexedSet_1_CopyTo_m12056_gshared (IndexedSet_1_t1904 * __this, ObjectU5BU5D_t60* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define IndexedSet_1_CopyTo_m12056(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t1904 *, ObjectU5BU5D_t60*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m12056_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Count()
extern "C" int32_t IndexedSet_1_get_Count_m12058_gshared (IndexedSet_1_t1904 * __this, const MethodInfo* method);
#define IndexedSet_1_get_Count_m12058(__this, method) (( int32_t (*) (IndexedSet_1_t1904 *, const MethodInfo*))IndexedSet_1_get_Count_m12058_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_IsReadOnly()
extern "C" bool IndexedSet_1_get_IsReadOnly_m12060_gshared (IndexedSet_1_t1904 * __this, const MethodInfo* method);
#define IndexedSet_1_get_IsReadOnly_m12060(__this, method) (( bool (*) (IndexedSet_1_t1904 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m12060_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<System.Object>::IndexOf(T)
extern "C" int32_t IndexedSet_1_IndexOf_m12062_gshared (IndexedSet_1_t1904 * __this, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_IndexOf_m12062(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t1904 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m12062_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Insert(System.Int32,T)
extern "C" void IndexedSet_1_Insert_m12064_gshared (IndexedSet_1_t1904 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define IndexedSet_1_Insert_m12064(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t1904 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m12064_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAt(System.Int32)
extern "C" void IndexedSet_1_RemoveAt_m12066_gshared (IndexedSet_1_t1904 * __this, int32_t ___index, const MethodInfo* method);
#define IndexedSet_1_RemoveAt_m12066(__this, ___index, method) (( void (*) (IndexedSet_1_t1904 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m12066_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * IndexedSet_1_get_Item_m12068_gshared (IndexedSet_1_t1904 * __this, int32_t ___index, const MethodInfo* method);
#define IndexedSet_1_get_Item_m12068(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t1904 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m12068_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::set_Item(System.Int32,T)
extern "C" void IndexedSet_1_set_Item_m12070_gshared (IndexedSet_1_t1904 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define IndexedSet_1_set_Item_m12070(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t1904 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m12070_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" void IndexedSet_1_RemoveAll_m12071_gshared (IndexedSet_1_t1904 * __this, Predicate_1_t1808 * ___match, const MethodInfo* method);
#define IndexedSet_1_RemoveAll_m12071(__this, ___match, method) (( void (*) (IndexedSet_1_t1904 *, Predicate_1_t1808 *, const MethodInfo*))IndexedSet_1_RemoveAll_m12071_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void IndexedSet_1_Sort_m12072_gshared (IndexedSet_1_t1904 * __this, Comparison_1_t1811 * ___sortLayoutFunction, const MethodInfo* method);
#define IndexedSet_1_Sort_m12072(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t1904 *, Comparison_1_t1811 *, const MethodInfo*))IndexedSet_1_Sort_m12072_gshared)(__this, ___sortLayoutFunction, method)
