﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AssetBundleCreateRequest
struct AssetBundleCreateRequest_t357;
// UnityEngine.AssetBundle
struct AssetBundle_t360;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.AssetBundleCreateRequest::.ctor()
extern "C" void AssetBundleCreateRequest__ctor_m1983 (AssetBundleCreateRequest_t357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.AssetBundle UnityEngine.AssetBundleCreateRequest::get_assetBundle()
extern "C" AssetBundle_t360 * AssetBundleCreateRequest_get_assetBundle_m1984 (AssetBundleCreateRequest_t357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AssetBundleCreateRequest::DisableCompatibilityChecks()
extern "C" void AssetBundleCreateRequest_DisableCompatibilityChecks_m1985 (AssetBundleCreateRequest_t357 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
