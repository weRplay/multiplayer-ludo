﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t8;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t9;
// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// GameMenu/<SetUserOnline>c__Iterator8
struct  U3CSetUserOnlineU3Ec__Iterator8_t27  : public Object_t
{
	// UnityEngine.WWWForm GameMenu/<SetUserOnline>c__Iterator8::<form>__0
	WWWForm_t8 * ___U3CformU3E__0_0;
	// System.String GameMenu/<SetUserOnline>c__Iterator8::<username>__1
	String_t* ___U3CusernameU3E__1_1;
	// UnityEngine.WWW GameMenu/<SetUserOnline>c__Iterator8::<w>__2
	WWW_t9 * ___U3CwU3E__2_2;
	// System.Int32 GameMenu/<SetUserOnline>c__Iterator8::$PC
	int32_t ___U24PC_3;
	// System.Object GameMenu/<SetUserOnline>c__Iterator8::$current
	Object_t * ___U24current_4;
};
