﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.ComponentModel.TypeConverterAttribute
struct TypeConverterAttribute_t931;
// System.String
struct String_t;

#include "mscorlib_System_Attribute.h"

// System.ComponentModel.TypeConverterAttribute
struct  TypeConverterAttribute_t931  : public Attribute_t428
{
	// System.String System.ComponentModel.TypeConverterAttribute::converter_type
	String_t* ___converter_type_1;
};
struct TypeConverterAttribute_t931_StaticFields{
	// System.ComponentModel.TypeConverterAttribute System.ComponentModel.TypeConverterAttribute::Default
	TypeConverterAttribute_t931 * ___Default_0;
};
