﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_Core_System_Func_2_gen_1MethodDeclarations.h"

// System.Void System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define Func_2__ctor_m1922(__this, ___object, ___method, method) (( void (*) (Func_2_t228 *, Object_t *, IntPtr_t, const MethodInfo*))Func_2__ctor_m13679_gshared)(__this, ___object, ___method, method)
// TResult System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::Invoke(T)
#define Func_2_Invoke_m13680(__this, ___arg1, method) (( bool (*) (Func_2_t228 *, Toggle_t224 *, const MethodInfo*))Func_2_Invoke_m13681_gshared)(__this, ___arg1, method)
// System.IAsyncResult System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Func_2_BeginInvoke_m13682(__this, ___arg1, ___callback, ___object, method) (( Object_t * (*) (Func_2_t228 *, Toggle_t224 *, AsyncCallback_t181 *, Object_t *, const MethodInfo*))Func_2_BeginInvoke_m13683_gshared)(__this, ___arg1, ___callback, ___object, method)
// TResult System.Func`2<UnityEngine.UI.Toggle,System.Boolean>::EndInvoke(System.IAsyncResult)
#define Func_2_EndInvoke_m13684(__this, ___result, method) (( bool (*) (Func_2_t228 *, Object_t *, const MethodInfo*))Func_2_EndInvoke_m13685_gshared)(__this, ___result, method)
