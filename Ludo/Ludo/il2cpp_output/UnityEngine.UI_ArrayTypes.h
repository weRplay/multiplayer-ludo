﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// UnityEngine.EventSystems.BaseInputModule[]
// UnityEngine.EventSystems.BaseInputModule[]
struct BaseInputModuleU5BU5D_t1801  : public Array_t { };
// UnityEngine.EventSystems.IEventSystemHandler[]
// UnityEngine.EventSystems.IEventSystemHandler[]
struct IEventSystemHandlerU5BU5D_t1817  : public Array_t { };
// UnityEngine.EventSystems.RaycastResult[]
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t1835  : public Array_t { };
// UnityEngine.EventSystems.BaseRaycaster[]
// UnityEngine.EventSystems.BaseRaycaster[]
struct BaseRaycasterU5BU5D_t1846  : public Array_t { };
// UnityEngine.EventSystems.EventTrigger/Entry[]
// UnityEngine.EventSystems.EventTrigger/Entry[]
struct EntryU5BU5D_t1852  : public Array_t { };
// UnityEngine.EventSystems.PointerEventData[]
// UnityEngine.EventSystems.PointerEventData[]
struct PointerEventDataU5BU5D_t1872  : public Array_t { };
// UnityEngine.EventSystems.PointerInputModule/ButtonState[]
// UnityEngine.EventSystems.PointerInputModule/ButtonState[]
struct ButtonStateU5BU5D_t1894  : public Array_t { };
// UnityEngine.UI.ICanvasElement[]
// UnityEngine.UI.ICanvasElement[]
struct ICanvasElementU5BU5D_t1919  : public Array_t { };
// UnityEngine.UI.Text[]
// UnityEngine.UI.Text[]
struct TextU5BU5D_t1926  : public Array_t { };
// UnityEngine.UI.Graphic[]
// UnityEngine.UI.Graphic[]
struct GraphicU5BU5D_t1967  : public Array_t { };
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>[]
// UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>[]
struct IndexedSet_1U5BU5D_t1971  : public Array_t { };
// UnityEngine.UI.InputField/ContentType[]
// UnityEngine.UI.InputField/ContentType[]
struct ContentTypeU5BU5D_t288  : public Array_t { };
// UnityEngine.UI.Selectable[]
// UnityEngine.UI.Selectable[]
struct SelectableU5BU5D_t1996  : public Array_t { };
// UnityEngine.UI.StencilMaterial/MatEntry[]
// UnityEngine.UI.StencilMaterial/MatEntry[]
struct MatEntryU5BU5D_t2008  : public Array_t { };
// UnityEngine.UI.Toggle[]
// UnityEngine.UI.Toggle[]
struct ToggleU5BU5D_t2016  : public Array_t { };
