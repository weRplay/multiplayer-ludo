﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__19MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m16839(__this, ___dictionary, method) (( void (*) (Enumerator_t2255 *, Dictionary_2_t580 *, const MethodInfo*))Enumerator__ctor_m16740_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16840(__this, method) (( Object_t * (*) (Enumerator_t2255 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16741_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16841(__this, method) (( DictionaryEntry_t1068  (*) (Enumerator_t2255 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16742_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16842(__this, method) (( Object_t * (*) (Enumerator_t2255 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16743_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16843(__this, method) (( Object_t * (*) (Enumerator_t2255 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16744_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::MoveNext()
#define Enumerator_MoveNext_m16844(__this, method) (( bool (*) (Enumerator_t2255 *, const MethodInfo*))Enumerator_MoveNext_m16745_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Current()
#define Enumerator_get_Current_m16845(__this, method) (( KeyValuePair_2_t2252  (*) (Enumerator_t2255 *, const MethodInfo*))Enumerator_get_Current_m16746_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m16846(__this, method) (( Event_t189 * (*) (Enumerator_t2255 *, const MethodInfo*))Enumerator_get_CurrentKey_m16747_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m16847(__this, method) (( int32_t (*) (Enumerator_t2255 *, const MethodInfo*))Enumerator_get_CurrentValue_m16748_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyState()
#define Enumerator_VerifyState_m16848(__this, method) (( void (*) (Enumerator_t2255 *, const MethodInfo*))Enumerator_VerifyState_m16749_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m16849(__this, method) (( void (*) (Enumerator_t2255 *, const MethodInfo*))Enumerator_VerifyCurrent_m16750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Dispose()
#define Enumerator_Dispose_m16850(__this, method) (( void (*) (Enumerator_t2255 *, const MethodInfo*))Enumerator_Dispose_m16751_gshared)(__this, method)
