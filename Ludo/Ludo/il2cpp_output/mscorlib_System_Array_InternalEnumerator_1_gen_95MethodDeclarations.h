﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m17699(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2364 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m10443_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17700(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2364 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m10445_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::Dispose()
#define InternalEnumerator_1_Dispose_m17701(__this, method) (( void (*) (InternalEnumerator_1_t2364 *, const MethodInfo*))InternalEnumerator_1_Dispose_m10447_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::MoveNext()
#define InternalEnumerator_1_MoveNext_m17702(__this, method) (( bool (*) (InternalEnumerator_1_t2364 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m10449_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::get_Current()
#define InternalEnumerator_1_get_Current_m17703(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2364 *, const MethodInfo*))InternalEnumerator_1_get_Current_m10451_gshared)(__this, method)
