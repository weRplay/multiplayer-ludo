﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.CachedInvokableCall`1<System.Boolean>
struct CachedInvokableCall_1_t701;
// UnityEngine.Object
struct Object_t54;
struct Object_t54_marshaled;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Object[]
struct ObjectU5BU5D_t60;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::.ctor(UnityEngine.Object,System.Reflection.MethodInfo,T)
extern "C" void CachedInvokableCall_1__ctor_m3388_gshared (CachedInvokableCall_1_t701 * __this, Object_t54 * ___target, MethodInfo_t * ___theFunction, bool ___argument, const MethodInfo* method);
#define CachedInvokableCall_1__ctor_m3388(__this, ___target, ___theFunction, ___argument, method) (( void (*) (CachedInvokableCall_1_t701 *, Object_t54 *, MethodInfo_t *, bool, const MethodInfo*))CachedInvokableCall_1__ctor_m3388_gshared)(__this, ___target, ___theFunction, ___argument, method)
// System.Void UnityEngine.Events.CachedInvokableCall`1<System.Boolean>::Invoke(System.Object[])
extern "C" void CachedInvokableCall_1_Invoke_m16889_gshared (CachedInvokableCall_1_t701 * __this, ObjectU5BU5D_t60* ___args, const MethodInfo* method);
#define CachedInvokableCall_1_Invoke_m16889(__this, ___args, method) (( void (*) (CachedInvokableCall_1_t701 *, ObjectU5BU5D_t60*, const MethodInfo*))CachedInvokableCall_1_Invoke_m16889_gshared)(__this, ___args, method)
