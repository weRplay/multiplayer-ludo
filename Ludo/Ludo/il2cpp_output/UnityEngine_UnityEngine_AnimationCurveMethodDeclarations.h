﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AnimationCurve
struct AnimationCurve_t470;
struct AnimationCurve_t470_marshaled;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t610;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C" void AnimationCurve__ctor_m2711 (AnimationCurve_t470 * __this, KeyframeU5BU5D_t610* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" void AnimationCurve__ctor_m2712 (AnimationCurve_t470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C" void AnimationCurve_Cleanup_m2713 (AnimationCurve_t470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C" void AnimationCurve_Finalize_m2714 (AnimationCurve_t470 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C" void AnimationCurve_Init_m2715 (AnimationCurve_t470 * __this, KeyframeU5BU5D_t610* ___keys, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void AnimationCurve_t470_marshal(const AnimationCurve_t470& unmarshaled, AnimationCurve_t470_marshaled& marshaled);
extern "C" void AnimationCurve_t470_marshal_back(const AnimationCurve_t470_marshaled& marshaled, AnimationCurve_t470& unmarshaled);
extern "C" void AnimationCurve_t470_marshal_cleanup(AnimationCurve_t470_marshaled& marshaled);
