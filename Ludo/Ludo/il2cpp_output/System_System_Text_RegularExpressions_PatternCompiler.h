﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.ArrayList
struct ArrayList_t741;

#include "mscorlib_System_Object.h"

// System.Text.RegularExpressions.PatternCompiler
struct  PatternCompiler_t1018  : public Object_t
{
	// System.Collections.ArrayList System.Text.RegularExpressions.PatternCompiler::pgm
	ArrayList_t741 * ___pgm_0;
};
