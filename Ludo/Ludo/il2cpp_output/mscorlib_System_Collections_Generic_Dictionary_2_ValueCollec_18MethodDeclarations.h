﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t2116;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_18.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15230_gshared (Enumerator_t2126 * __this, Dictionary_2_t2116 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m15230(__this, ___host, method) (( void (*) (Enumerator_t2126 *, Dictionary_2_t2116 *, const MethodInfo*))Enumerator__ctor_m15230_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15231_gshared (Enumerator_t2126 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15231(__this, method) (( Object_t * (*) (Enumerator_t2126 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15231_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int64>::Dispose()
extern "C" void Enumerator_Dispose_m15232_gshared (Enumerator_t2126 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15232(__this, method) (( void (*) (Enumerator_t2126 *, const MethodInfo*))Enumerator_Dispose_m15232_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int64>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15233_gshared (Enumerator_t2126 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15233(__this, method) (( bool (*) (Enumerator_t2126 *, const MethodInfo*))Enumerator_MoveNext_m15233_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int64>::get_Current()
extern "C" int64_t Enumerator_get_Current_m15234_gshared (Enumerator_t2126 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15234(__this, method) (( int64_t (*) (Enumerator_t2126 *, const MethodInfo*))Enumerator_get_Current_m15234_gshared)(__this, method)
