﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_3MethodDeclarations.h"

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
#define ObjectPool_1__ctor_m1622(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t152 *, UnityAction_1_t153 *, UnityAction_1_t153 *, const MethodInfo*))ObjectPool_1__ctor_m10962_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countAll()
#define ObjectPool_1_get_countAll_m12637(__this, method) (( int32_t (*) (ObjectPool_1_t152 *, const MethodInfo*))ObjectPool_1_get_countAll_m10964_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m12638(__this, ___value, method) (( void (*) (ObjectPool_1_t152 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m10966_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countActive()
#define ObjectPool_1_get_countActive_m12639(__this, method) (( int32_t (*) (ObjectPool_1_t152 *, const MethodInfo*))ObjectPool_1_get_countActive_m10968_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m12640(__this, method) (( int32_t (*) (ObjectPool_1_t152 *, const MethodInfo*))ObjectPool_1_get_countInactive_m10970_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Get()
#define ObjectPool_1_Get_m1633(__this, method) (( List_1_t187 * (*) (ObjectPool_1_t152 *, const MethodInfo*))ObjectPool_1_Get_m10972_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Release(T)
#define ObjectPool_1_Release_m1638(__this, ___element, method) (( void (*) (ObjectPool_1_t152 *, List_1_t187 *, const MethodInfo*))ObjectPool_1_Release_m10974_gshared)(__this, ___element, method)
