﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe_6MethodDeclarations.h"

// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
#define ThreadSafeDictionary_2__ctor_m3348(__this, ___valueFactory, method) (( void (*) (ThreadSafeDictionary_2_t673 *, ThreadSafeDictionaryValueFactory_2_t672 *, const MethodInfo*))ThreadSafeDictionary_2__ctor_m15887_gshared)(__this, ___valueFactory, method)
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::System.Collections.IEnumerable.GetEnumerator()
#define ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m16057(__this, method) (( Object_t * (*) (ThreadSafeDictionary_2_t673 *, const MethodInfo*))ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m15889_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::Get(TKey)
#define ThreadSafeDictionary_2_Get_m16058(__this, ___key, method) (( Object_t* (*) (ThreadSafeDictionary_2_t673 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_Get_m15891_gshared)(__this, ___key, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::AddValue(TKey)
#define ThreadSafeDictionary_2_AddValue_m16059(__this, ___key, method) (( Object_t* (*) (ThreadSafeDictionary_2_t673 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_AddValue_m15893_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::Add(TKey,TValue)
#define ThreadSafeDictionary_2_Add_m16060(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t673 *, Type_t *, Object_t*, const MethodInfo*))ThreadSafeDictionary_2_Add_m15895_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::get_Keys()
#define ThreadSafeDictionary_2_get_Keys_m16061(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t673 *, const MethodInfo*))ThreadSafeDictionary_2_get_Keys_m15897_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::TryGetValue(TKey,TValue&)
#define ThreadSafeDictionary_2_TryGetValue_m16062(__this, ___key, ___value, method) (( bool (*) (ThreadSafeDictionary_2_t673 *, Type_t *, Object_t**, const MethodInfo*))ThreadSafeDictionary_2_TryGetValue_m15899_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::get_Values()
#define ThreadSafeDictionary_2_get_Values_m16063(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t673 *, const MethodInfo*))ThreadSafeDictionary_2_get_Values_m15901_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::get_Item(TKey)
#define ThreadSafeDictionary_2_get_Item_m16064(__this, ___key, method) (( Object_t* (*) (ThreadSafeDictionary_2_t673 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_get_Item_m15903_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::set_Item(TKey,TValue)
#define ThreadSafeDictionary_2_set_Item_m16065(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t673 *, Type_t *, Object_t*, const MethodInfo*))ThreadSafeDictionary_2_set_Item_m15905_gshared)(__this, ___key, ___value, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Add_m16066(__this, ___item, method) (( void (*) (ThreadSafeDictionary_2_t673 *, KeyValuePair_2_t2191 , const MethodInfo*))ThreadSafeDictionary_2_Add_m15907_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::Clear()
#define ThreadSafeDictionary_2_Clear_m16067(__this, method) (( void (*) (ThreadSafeDictionary_2_t673 *, const MethodInfo*))ThreadSafeDictionary_2_Clear_m15909_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Contains_m16068(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t673 *, KeyValuePair_2_t2191 , const MethodInfo*))ThreadSafeDictionary_2_Contains_m15911_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define ThreadSafeDictionary_2_CopyTo_m16069(__this, ___array, ___arrayIndex, method) (( void (*) (ThreadSafeDictionary_2_t673 *, KeyValuePair_2U5BU5D_t2472*, int32_t, const MethodInfo*))ThreadSafeDictionary_2_CopyTo_m15913_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::get_Count()
#define ThreadSafeDictionary_2_get_Count_m16070(__this, method) (( int32_t (*) (ThreadSafeDictionary_2_t673 *, const MethodInfo*))ThreadSafeDictionary_2_get_Count_m15915_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::get_IsReadOnly()
#define ThreadSafeDictionary_2_get_IsReadOnly_m16071(__this, method) (( bool (*) (ThreadSafeDictionary_2_t673 *, const MethodInfo*))ThreadSafeDictionary_2_get_IsReadOnly_m15917_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Remove_m16072(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t673 *, KeyValuePair_2_t2191 , const MethodInfo*))ThreadSafeDictionary_2_Remove_m15919_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>>::GetEnumerator()
#define ThreadSafeDictionary_2_GetEnumerator_m16073(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t673 *, const MethodInfo*))ThreadSafeDictionary_2_GetEnumerator_m15921_gshared)(__this, method)
