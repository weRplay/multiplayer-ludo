﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>
struct ThreadSafeDictionary_2_t2179;
// SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<System.Object,System.Object>
struct ThreadSafeDictionaryValueFactory_2_t2177;
// System.Collections.IEnumerator
struct IEnumerator_t51;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t621;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>[]
struct KeyValuePair_2U5BU5D_t2200;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct IEnumerator_1_t2414;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"

// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
extern "C" void ThreadSafeDictionary_2__ctor_m15887_gshared (ThreadSafeDictionary_2_t2179 * __this, ThreadSafeDictionaryValueFactory_2_t2177 * ___valueFactory, const MethodInfo* method);
#define ThreadSafeDictionary_2__ctor_m15887(__this, ___valueFactory, method) (( void (*) (ThreadSafeDictionary_2_t2179 *, ThreadSafeDictionaryValueFactory_2_t2177 *, const MethodInfo*))ThreadSafeDictionary_2__ctor_m15887_gshared)(__this, ___valueFactory, method)
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m15889_gshared (ThreadSafeDictionary_2_t2179 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m15889(__this, method) (( Object_t * (*) (ThreadSafeDictionary_2_t2179 *, const MethodInfo*))ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m15889_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Get(TKey)
extern "C" Object_t * ThreadSafeDictionary_2_Get_m15891_gshared (ThreadSafeDictionary_2_t2179 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_Get_m15891(__this, ___key, method) (( Object_t * (*) (ThreadSafeDictionary_2_t2179 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_Get_m15891_gshared)(__this, ___key, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::AddValue(TKey)
extern "C" Object_t * ThreadSafeDictionary_2_AddValue_m15893_gshared (ThreadSafeDictionary_2_t2179 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_AddValue_m15893(__this, ___key, method) (( Object_t * (*) (ThreadSafeDictionary_2_t2179 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_AddValue_m15893_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(TKey,TValue)
extern "C" void ThreadSafeDictionary_2_Add_m15895_gshared (ThreadSafeDictionary_2_t2179 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define ThreadSafeDictionary_2_Add_m15895(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t2179 *, Object_t *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_Add_m15895_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Keys()
extern "C" Object_t* ThreadSafeDictionary_2_get_Keys_m15897_gshared (ThreadSafeDictionary_2_t2179 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Keys_m15897(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t2179 *, const MethodInfo*))ThreadSafeDictionary_2_get_Keys_m15897_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool ThreadSafeDictionary_2_TryGetValue_m15899_gshared (ThreadSafeDictionary_2_t2179 * __this, Object_t * ___key, Object_t ** ___value, const MethodInfo* method);
#define ThreadSafeDictionary_2_TryGetValue_m15899(__this, ___key, ___value, method) (( bool (*) (ThreadSafeDictionary_2_t2179 *, Object_t *, Object_t **, const MethodInfo*))ThreadSafeDictionary_2_TryGetValue_m15899_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Values()
extern "C" Object_t* ThreadSafeDictionary_2_get_Values_m15901_gshared (ThreadSafeDictionary_2_t2179 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Values_m15901(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t2179 *, const MethodInfo*))ThreadSafeDictionary_2_get_Values_m15901_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Item(TKey)
extern "C" Object_t * ThreadSafeDictionary_2_get_Item_m15903_gshared (ThreadSafeDictionary_2_t2179 * __this, Object_t * ___key, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Item_m15903(__this, ___key, method) (( Object_t * (*) (ThreadSafeDictionary_2_t2179 *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_get_Item_m15903_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::set_Item(TKey,TValue)
extern "C" void ThreadSafeDictionary_2_set_Item_m15905_gshared (ThreadSafeDictionary_2_t2179 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define ThreadSafeDictionary_2_set_Item_m15905(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t2179 *, Object_t *, Object_t *, const MethodInfo*))ThreadSafeDictionary_2_set_Item_m15905_gshared)(__this, ___key, ___value, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void ThreadSafeDictionary_2_Add_m15907_gshared (ThreadSafeDictionary_2_t2179 * __this, KeyValuePair_2_t1934  ___item, const MethodInfo* method);
#define ThreadSafeDictionary_2_Add_m15907(__this, ___item, method) (( void (*) (ThreadSafeDictionary_2_t2179 *, KeyValuePair_2_t1934 , const MethodInfo*))ThreadSafeDictionary_2_Add_m15907_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Clear()
extern "C" void ThreadSafeDictionary_2_Clear_m15909_gshared (ThreadSafeDictionary_2_t2179 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_Clear_m15909(__this, method) (( void (*) (ThreadSafeDictionary_2_t2179 *, const MethodInfo*))ThreadSafeDictionary_2_Clear_m15909_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool ThreadSafeDictionary_2_Contains_m15911_gshared (ThreadSafeDictionary_2_t2179 * __this, KeyValuePair_2_t1934  ___item, const MethodInfo* method);
#define ThreadSafeDictionary_2_Contains_m15911(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t2179 *, KeyValuePair_2_t1934 , const MethodInfo*))ThreadSafeDictionary_2_Contains_m15911_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void ThreadSafeDictionary_2_CopyTo_m15913_gshared (ThreadSafeDictionary_2_t2179 * __this, KeyValuePair_2U5BU5D_t2200* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define ThreadSafeDictionary_2_CopyTo_m15913(__this, ___array, ___arrayIndex, method) (( void (*) (ThreadSafeDictionary_2_t2179 *, KeyValuePair_2U5BU5D_t2200*, int32_t, const MethodInfo*))ThreadSafeDictionary_2_CopyTo_m15913_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_Count()
extern "C" int32_t ThreadSafeDictionary_2_get_Count_m15915_gshared (ThreadSafeDictionary_2_t2179 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_Count_m15915(__this, method) (( int32_t (*) (ThreadSafeDictionary_2_t2179 *, const MethodInfo*))ThreadSafeDictionary_2_get_Count_m15915_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::get_IsReadOnly()
extern "C" bool ThreadSafeDictionary_2_get_IsReadOnly_m15917_gshared (ThreadSafeDictionary_2_t2179 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_get_IsReadOnly_m15917(__this, method) (( bool (*) (ThreadSafeDictionary_2_t2179 *, const MethodInfo*))ThreadSafeDictionary_2_get_IsReadOnly_m15917_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool ThreadSafeDictionary_2_Remove_m15919_gshared (ThreadSafeDictionary_2_t2179 * __this, KeyValuePair_2_t1934  ___item, const MethodInfo* method);
#define ThreadSafeDictionary_2_Remove_m15919(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t2179 *, KeyValuePair_2_t1934 , const MethodInfo*))ThreadSafeDictionary_2_Remove_m15919_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Object,System.Object>::GetEnumerator()
extern "C" Object_t* ThreadSafeDictionary_2_GetEnumerator_m15921_gshared (ThreadSafeDictionary_2_t2179 * __this, const MethodInfo* method);
#define ThreadSafeDictionary_2_GetEnumerator_m15921(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t2179 *, const MethodInfo*))ThreadSafeDictionary_2_GetEnumerator_m15921_gshared)(__this, method)
