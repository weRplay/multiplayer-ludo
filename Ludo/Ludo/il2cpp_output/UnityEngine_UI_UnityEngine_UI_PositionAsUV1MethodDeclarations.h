﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.PositionAsUV1
struct PositionAsUV1_t262;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t187;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.PositionAsUV1::.ctor()
extern "C" void PositionAsUV1__ctor_m1454 (PositionAsUV1_t262 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.PositionAsUV1::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C" void PositionAsUV1_ModifyVertices_m1455 (PositionAsUV1_t262 * __this, List_1_t187 * ___verts, const MethodInfo* method) IL2CPP_METHOD_ATTR;
