﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityAction`1<System.String>
struct UnityAction_1_t1992;

#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"

// UnityEngine.Events.InvokableCall`1<System.String>
struct  InvokableCall_1_t2265  : public BaseInvokableCall_t583
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<System.String>::Delegate
	UnityAction_1_t1992 * ___Delegate_0;
};
