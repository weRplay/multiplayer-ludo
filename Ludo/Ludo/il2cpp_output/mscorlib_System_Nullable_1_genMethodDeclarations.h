﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Nullable_1_gen.h"
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Nullable`1<System.TimeSpan>::.ctor(T)
extern "C" void Nullable_1__ctor_m10435_gshared (Nullable_1_t1741 * __this, TimeSpan_t977  ___value, const MethodInfo* method);
#define Nullable_1__ctor_m10435(__this, ___value, method) (( void (*) (Nullable_1_t1741 *, TimeSpan_t977 , const MethodInfo*))Nullable_1__ctor_m10435_gshared)(__this, ___value, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::get_HasValue()
extern "C" bool Nullable_1_get_HasValue_m10436_gshared (Nullable_1_t1741 * __this, const MethodInfo* method);
#define Nullable_1_get_HasValue_m10436(__this, method) (( bool (*) (Nullable_1_t1741 *, const MethodInfo*))Nullable_1_get_HasValue_m10436_gshared)(__this, method)
// T System.Nullable`1<System.TimeSpan>::get_Value()
extern "C" TimeSpan_t977  Nullable_1_get_Value_m10437_gshared (Nullable_1_t1741 * __this, const MethodInfo* method);
#define Nullable_1_get_Value_m10437(__this, method) (( TimeSpan_t977  (*) (Nullable_1_t1741 *, const MethodInfo*))Nullable_1_get_Value_m10437_gshared)(__this, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Object)
extern "C" bool Nullable_1_Equals_m17860_gshared (Nullable_1_t1741 * __this, Object_t * ___other, const MethodInfo* method);
#define Nullable_1_Equals_m17860(__this, ___other, method) (( bool (*) (Nullable_1_t1741 *, Object_t *, const MethodInfo*))Nullable_1_Equals_m17860_gshared)(__this, ___other, method)
// System.Boolean System.Nullable`1<System.TimeSpan>::Equals(System.Nullable`1<T>)
extern "C" bool Nullable_1_Equals_m17861_gshared (Nullable_1_t1741 * __this, Nullable_1_t1741  ___other, const MethodInfo* method);
#define Nullable_1_Equals_m17861(__this, ___other, method) (( bool (*) (Nullable_1_t1741 *, Nullable_1_t1741 , const MethodInfo*))Nullable_1_Equals_m17861_gshared)(__this, ___other, method)
// System.Int32 System.Nullable`1<System.TimeSpan>::GetHashCode()
extern "C" int32_t Nullable_1_GetHashCode_m17862_gshared (Nullable_1_t1741 * __this, const MethodInfo* method);
#define Nullable_1_GetHashCode_m17862(__this, method) (( int32_t (*) (Nullable_1_t1741 *, const MethodInfo*))Nullable_1_GetHashCode_m17862_gshared)(__this, method)
// System.String System.Nullable`1<System.TimeSpan>::ToString()
extern "C" String_t* Nullable_1_ToString_m17863_gshared (Nullable_1_t1741 * __this, const MethodInfo* method);
#define Nullable_1_ToString_m17863(__this, method) (( String_t* (*) (Nullable_1_t1741 *, const MethodInfo*))Nullable_1_ToString_m17863_gshared)(__this, method)
