﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t480;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m14829_gshared (Enumerator_t2097 * __this, List_1_t480 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m14829(__this, ___l, method) (( void (*) (Enumerator_t2097 *, List_1_t480 *, const MethodInfo*))Enumerator__ctor_m14829_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m14830_gshared (Enumerator_t2097 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m14830(__this, method) (( Object_t * (*) (Enumerator_t2097 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m14830_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::Dispose()
extern "C" void Enumerator_Dispose_m14831_gshared (Enumerator_t2097 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m14831(__this, method) (( void (*) (Enumerator_t2097 *, const MethodInfo*))Enumerator_Dispose_m14831_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m14832_gshared (Enumerator_t2097 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m14832(__this, method) (( void (*) (Enumerator_t2097 *, const MethodInfo*))Enumerator_VerifyState_m14832_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m14833_gshared (Enumerator_t2097 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m14833(__this, method) (( bool (*) (Enumerator_t2097 *, const MethodInfo*))Enumerator_MoveNext_m14833_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UICharInfo>::get_Current()
extern "C" UICharInfo_t333  Enumerator_get_Current_m14834_gshared (Enumerator_t2097 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m14834(__this, method) (( UICharInfo_t333  (*) (Enumerator_t2097 *, const MethodInfo*))Enumerator_get_Current_m14834_gshared)(__this, method)
