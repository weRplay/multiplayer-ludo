﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe_6MethodDeclarations.h"

// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
#define ThreadSafeDictionary_2__ctor_m3346(__this, ___valueFactory, method) (( void (*) (ThreadSafeDictionary_2_t671 *, ThreadSafeDictionaryValueFactory_2_t670 *, const MethodInfo*))ThreadSafeDictionary_2__ctor_m15887_gshared)(__this, ___valueFactory, method)
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::System.Collections.IEnumerable.GetEnumerator()
#define ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m15981(__this, method) (( Object_t * (*) (ThreadSafeDictionary_2_t671 *, const MethodInfo*))ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m15889_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::Get(TKey)
#define ThreadSafeDictionary_2_Get_m15982(__this, ___key, method) (( Object_t* (*) (ThreadSafeDictionary_2_t671 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_Get_m15891_gshared)(__this, ___key, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::AddValue(TKey)
#define ThreadSafeDictionary_2_AddValue_m15983(__this, ___key, method) (( Object_t* (*) (ThreadSafeDictionary_2_t671 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_AddValue_m15893_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::Add(TKey,TValue)
#define ThreadSafeDictionary_2_Add_m15984(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t671 *, Type_t *, Object_t*, const MethodInfo*))ThreadSafeDictionary_2_Add_m15895_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::get_Keys()
#define ThreadSafeDictionary_2_get_Keys_m15985(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t671 *, const MethodInfo*))ThreadSafeDictionary_2_get_Keys_m15897_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::TryGetValue(TKey,TValue&)
#define ThreadSafeDictionary_2_TryGetValue_m15986(__this, ___key, ___value, method) (( bool (*) (ThreadSafeDictionary_2_t671 *, Type_t *, Object_t**, const MethodInfo*))ThreadSafeDictionary_2_TryGetValue_m15899_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::get_Values()
#define ThreadSafeDictionary_2_get_Values_m15987(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t671 *, const MethodInfo*))ThreadSafeDictionary_2_get_Values_m15901_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::get_Item(TKey)
#define ThreadSafeDictionary_2_get_Item_m15988(__this, ___key, method) (( Object_t* (*) (ThreadSafeDictionary_2_t671 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_get_Item_m15903_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::set_Item(TKey,TValue)
#define ThreadSafeDictionary_2_set_Item_m15989(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t671 *, Type_t *, Object_t*, const MethodInfo*))ThreadSafeDictionary_2_set_Item_m15905_gshared)(__this, ___key, ___value, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Add_m15990(__this, ___item, method) (( void (*) (ThreadSafeDictionary_2_t671 *, KeyValuePair_2_t2187 , const MethodInfo*))ThreadSafeDictionary_2_Add_m15907_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::Clear()
#define ThreadSafeDictionary_2_Clear_m15991(__this, method) (( void (*) (ThreadSafeDictionary_2_t671 *, const MethodInfo*))ThreadSafeDictionary_2_Clear_m15909_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Contains_m15992(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t671 *, KeyValuePair_2_t2187 , const MethodInfo*))ThreadSafeDictionary_2_Contains_m15911_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define ThreadSafeDictionary_2_CopyTo_m15993(__this, ___array, ___arrayIndex, method) (( void (*) (ThreadSafeDictionary_2_t671 *, KeyValuePair_2U5BU5D_t2468*, int32_t, const MethodInfo*))ThreadSafeDictionary_2_CopyTo_m15913_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::get_Count()
#define ThreadSafeDictionary_2_get_Count_m15994(__this, method) (( int32_t (*) (ThreadSafeDictionary_2_t671 *, const MethodInfo*))ThreadSafeDictionary_2_get_Count_m15915_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::get_IsReadOnly()
#define ThreadSafeDictionary_2_get_IsReadOnly_m15995(__this, method) (( bool (*) (ThreadSafeDictionary_2_t671 *, const MethodInfo*))ThreadSafeDictionary_2_get_IsReadOnly_m15917_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Remove_m15996(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t671 *, KeyValuePair_2_t2187 , const MethodInfo*))ThreadSafeDictionary_2_Remove_m15919_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::GetEnumerator()
#define ThreadSafeDictionary_2_GetEnumerator_m15997(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t671 *, const MethodInfo*))ThreadSafeDictionary_2_GetEnumerator_m15921_gshared)(__this, method)
