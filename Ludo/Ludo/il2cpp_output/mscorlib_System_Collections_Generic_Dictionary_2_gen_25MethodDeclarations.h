﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t2287;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1907;
// System.Collections.Generic.IDictionary`2<System.Object,System.Boolean>
struct IDictionary_2_t2496;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t631;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t621;
// System.Collections.Generic.ICollection`1<System.Boolean>
struct ICollection_1_t2497;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>[]
struct KeyValuePair_2U5BU5D_t2495;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t51;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>
struct IEnumerator_1_t2498;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1067;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Boolean>
struct KeyCollection_t2292;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Boolean>
struct ValueCollection_t2296;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_25.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__21.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor()
extern "C" void Dictionary_2__ctor_m17112_gshared (Dictionary_2_t2287 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m17112(__this, method) (( void (*) (Dictionary_2_t2287 *, const MethodInfo*))Dictionary_2__ctor_m17112_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m17113_gshared (Dictionary_2_t2287 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m17113(__this, ___comparer, method) (( void (*) (Dictionary_2_t2287 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m17113_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m17115_gshared (Dictionary_2_t2287 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m17115(__this, ___dictionary, method) (( void (*) (Dictionary_2_t2287 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m17115_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m17117_gshared (Dictionary_2_t2287 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m17117(__this, ___capacity, method) (( void (*) (Dictionary_2_t2287 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m17117_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m17119_gshared (Dictionary_2_t2287 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m17119(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t2287 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m17119_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m17121_gshared (Dictionary_2_t2287 * __this, SerializationInfo_t631 * ___info, StreamingContext_t632  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m17121(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2287 *, SerializationInfo_t631 *, StreamingContext_t632 , const MethodInfo*))Dictionary_2__ctor_m17121_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m17123_gshared (Dictionary_2_t2287 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m17123(__this, method) (( Object_t* (*) (Dictionary_2_t2287 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m17123_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m17125_gshared (Dictionary_2_t2287 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m17125(__this, method) (( Object_t* (*) (Dictionary_2_t2287 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m17125_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m17127_gshared (Dictionary_2_t2287 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m17127(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2287 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m17127_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m17129_gshared (Dictionary_2_t2287 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m17129(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2287 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m17129_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m17131_gshared (Dictionary_2_t2287 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m17131(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2287 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m17131_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m17133_gshared (Dictionary_2_t2287 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m17133(__this, ___key, method) (( bool (*) (Dictionary_2_t2287 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m17133_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m17135_gshared (Dictionary_2_t2287 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m17135(__this, ___key, method) (( void (*) (Dictionary_2_t2287 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m17135_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17137_gshared (Dictionary_2_t2287 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17137(__this, method) (( bool (*) (Dictionary_2_t2287 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17137_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17139_gshared (Dictionary_2_t2287 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17139(__this, method) (( Object_t * (*) (Dictionary_2_t2287 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17139_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17141_gshared (Dictionary_2_t2287 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17141(__this, method) (( bool (*) (Dictionary_2_t2287 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17141_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17143_gshared (Dictionary_2_t2287 * __this, KeyValuePair_2_t2289  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17143(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2287 *, KeyValuePair_2_t2289 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17143_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17145_gshared (Dictionary_2_t2287 * __this, KeyValuePair_2_t2289  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17145(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2287 *, KeyValuePair_2_t2289 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17145_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17147_gshared (Dictionary_2_t2287 * __this, KeyValuePair_2U5BU5D_t2495* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17147(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2287 *, KeyValuePair_2U5BU5D_t2495*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17147_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17149_gshared (Dictionary_2_t2287 * __this, KeyValuePair_2_t2289  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17149(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2287 *, KeyValuePair_2_t2289 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17149_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m17151_gshared (Dictionary_2_t2287 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m17151(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2287 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m17151_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17153_gshared (Dictionary_2_t2287 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17153(__this, method) (( Object_t * (*) (Dictionary_2_t2287 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17153_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17155_gshared (Dictionary_2_t2287 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17155(__this, method) (( Object_t* (*) (Dictionary_2_t2287 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17155_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17157_gshared (Dictionary_2_t2287 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17157(__this, method) (( Object_t * (*) (Dictionary_2_t2287 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17157_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m17159_gshared (Dictionary_2_t2287 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m17159(__this, method) (( int32_t (*) (Dictionary_2_t2287 *, const MethodInfo*))Dictionary_2_get_Count_m17159_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Item(TKey)
extern "C" bool Dictionary_2_get_Item_m17161_gshared (Dictionary_2_t2287 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m17161(__this, ___key, method) (( bool (*) (Dictionary_2_t2287 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m17161_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m17163_gshared (Dictionary_2_t2287 * __this, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m17163(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2287 *, Object_t *, bool, const MethodInfo*))Dictionary_2_set_Item_m17163_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m17165_gshared (Dictionary_2_t2287 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m17165(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2287 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m17165_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m17167_gshared (Dictionary_2_t2287 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m17167(__this, ___size, method) (( void (*) (Dictionary_2_t2287 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m17167_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m17169_gshared (Dictionary_2_t2287 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m17169(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2287 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m17169_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2289  Dictionary_2_make_pair_m17171_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m17171(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2289  (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_make_pair_m17171_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m17173_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m17173(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_pick_key_m17173_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::pick_value(TKey,TValue)
extern "C" bool Dictionary_2_pick_value_m17175_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m17175(__this /* static, unused */, ___key, ___value, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_pick_value_m17175_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m17177_gshared (Dictionary_2_t2287 * __this, KeyValuePair_2U5BU5D_t2495* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m17177(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2287 *, KeyValuePair_2U5BU5D_t2495*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m17177_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Resize()
extern "C" void Dictionary_2_Resize_m17179_gshared (Dictionary_2_t2287 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m17179(__this, method) (( void (*) (Dictionary_2_t2287 *, const MethodInfo*))Dictionary_2_Resize_m17179_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m17181_gshared (Dictionary_2_t2287 * __this, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_Add_m17181(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2287 *, Object_t *, bool, const MethodInfo*))Dictionary_2_Add_m17181_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Clear()
extern "C" void Dictionary_2_Clear_m17183_gshared (Dictionary_2_t2287 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m17183(__this, method) (( void (*) (Dictionary_2_t2287 *, const MethodInfo*))Dictionary_2_Clear_m17183_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m17185_gshared (Dictionary_2_t2287 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m17185(__this, ___key, method) (( bool (*) (Dictionary_2_t2287 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m17185_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m17187_gshared (Dictionary_2_t2287 * __this, bool ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m17187(__this, ___value, method) (( bool (*) (Dictionary_2_t2287 *, bool, const MethodInfo*))Dictionary_2_ContainsValue_m17187_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m17189_gshared (Dictionary_2_t2287 * __this, SerializationInfo_t631 * ___info, StreamingContext_t632  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m17189(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2287 *, SerializationInfo_t631 *, StreamingContext_t632 , const MethodInfo*))Dictionary_2_GetObjectData_m17189_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m17191_gshared (Dictionary_2_t2287 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m17191(__this, ___sender, method) (( void (*) (Dictionary_2_t2287 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m17191_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m17193_gshared (Dictionary_2_t2287 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m17193(__this, ___key, method) (( bool (*) (Dictionary_2_t2287 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m17193_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m17195_gshared (Dictionary_2_t2287 * __this, Object_t * ___key, bool* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m17195(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2287 *, Object_t *, bool*, const MethodInfo*))Dictionary_2_TryGetValue_m17195_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Keys()
extern "C" KeyCollection_t2292 * Dictionary_2_get_Keys_m17197_gshared (Dictionary_2_t2287 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m17197(__this, method) (( KeyCollection_t2292 * (*) (Dictionary_2_t2287 *, const MethodInfo*))Dictionary_2_get_Keys_m17197_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::get_Values()
extern "C" ValueCollection_t2296 * Dictionary_2_get_Values_m17199_gshared (Dictionary_2_t2287 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m17199(__this, method) (( ValueCollection_t2296 * (*) (Dictionary_2_t2287 *, const MethodInfo*))Dictionary_2_get_Values_m17199_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m17201_gshared (Dictionary_2_t2287 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m17201(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2287 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m17201_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ToTValue(System.Object)
extern "C" bool Dictionary_2_ToTValue_m17203_gshared (Dictionary_2_t2287 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m17203(__this, ___value, method) (( bool (*) (Dictionary_2_t2287 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m17203_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m17205_gshared (Dictionary_2_t2287 * __this, KeyValuePair_2_t2289  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m17205(__this, ___pair, method) (( bool (*) (Dictionary_2_t2287 *, KeyValuePair_2_t2289 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m17205_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::GetEnumerator()
extern "C" Enumerator_t2294  Dictionary_2_GetEnumerator_m17207_gshared (Dictionary_2_t2287 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m17207(__this, method) (( Enumerator_t2294  (*) (Dictionary_2_t2287 *, const MethodInfo*))Dictionary_2_GetEnumerator_m17207_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1068  Dictionary_2_U3CCopyToU3Em__0_m17209_gshared (Object_t * __this /* static, unused */, Object_t * ___key, bool ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m17209(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1068  (*) (Object_t * /* static, unused */, Object_t *, bool, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m17209_gshared)(__this /* static, unused */, ___key, ___value, method)
