﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Player/<SendPlayer1Position>c__IteratorC
struct U3CSendPlayer1PositionU3Ec__IteratorC_t40;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Player/<SendPlayer1Position>c__IteratorC::.ctor()
extern "C" void U3CSendPlayer1PositionU3Ec__IteratorC__ctor_m132 (U3CSendPlayer1PositionU3Ec__IteratorC_t40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Player/<SendPlayer1Position>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CSendPlayer1PositionU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m133 (U3CSendPlayer1PositionU3Ec__IteratorC_t40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Player/<SendPlayer1Position>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CSendPlayer1PositionU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m134 (U3CSendPlayer1PositionU3Ec__IteratorC_t40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Player/<SendPlayer1Position>c__IteratorC::MoveNext()
extern "C" bool U3CSendPlayer1PositionU3Ec__IteratorC_MoveNext_m135 (U3CSendPlayer1PositionU3Ec__IteratorC_t40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player/<SendPlayer1Position>c__IteratorC::Dispose()
extern "C" void U3CSendPlayer1PositionU3Ec__IteratorC_Dispose_m136 (U3CSendPlayer1PositionU3Ec__IteratorC_t40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player/<SendPlayer1Position>c__IteratorC::Reset()
extern "C" void U3CSendPlayer1PositionU3Ec__IteratorC_Reset_m137 (U3CSendPlayer1PositionU3Ec__IteratorC_t40 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
