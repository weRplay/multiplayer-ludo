﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_41MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m16801(__this, ___object, ___method, method) (( void (*) (Transform_1_t2234 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m16779_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m16802(__this, ___key, ___value, method) (( DictionaryEntry_t1068  (*) (Transform_1_t2234 *, Event_t189 *, int32_t, const MethodInfo*))Transform_1_Invoke_m16780_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m16803(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2234 *, Event_t189 *, int32_t, AsyncCallback_t181 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m16781_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m16804(__this, ___result, method) (( DictionaryEntry_t1068  (*) (Transform_1_t2234 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m16782_gshared)(__this, ___result, method)
