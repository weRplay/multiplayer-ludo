﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>
struct InvokableCall_1_t1995;
// System.Object
struct Object_t;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// UnityEngine.Events.UnityAction`1<UnityEngine.Vector2>
struct UnityAction_1_t1994;
// System.Object[]
struct ObjectU5BU5D_t60;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(System.Object,System.Reflection.MethodInfo)
extern "C" void InvokableCall_1__ctor_m13296_gshared (InvokableCall_1_t1995 * __this, Object_t * ___target, MethodInfo_t * ___theFunction, const MethodInfo* method);
#define InvokableCall_1__ctor_m13296(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t1995 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m13296_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
extern "C" void InvokableCall_1__ctor_m13297_gshared (InvokableCall_1_t1995 * __this, UnityAction_1_t1994 * ___callback, const MethodInfo* method);
#define InvokableCall_1__ctor_m13297(__this, ___callback, method) (( void (*) (InvokableCall_1_t1995 *, UnityAction_1_t1994 *, const MethodInfo*))InvokableCall_1__ctor_m13297_gshared)(__this, ___callback, method)
// System.Void UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Invoke(System.Object[])
extern "C" void InvokableCall_1_Invoke_m13298_gshared (InvokableCall_1_t1995 * __this, ObjectU5BU5D_t60* ___args, const MethodInfo* method);
#define InvokableCall_1_Invoke_m13298(__this, ___args, method) (( void (*) (InvokableCall_1_t1995 *, ObjectU5BU5D_t60*, const MethodInfo*))InvokableCall_1_Invoke_m13298_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<UnityEngine.Vector2>::Find(System.Object,System.Reflection.MethodInfo)
extern "C" bool InvokableCall_1_Find_m13299_gshared (InvokableCall_1_t1995 * __this, Object_t * ___targetObj, MethodInfo_t * ___method, const MethodInfo* method);
#define InvokableCall_1_Find_m13299(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t1995 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m13299_gshared)(__this, ___targetObj, ___method, method)
