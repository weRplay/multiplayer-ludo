﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t187;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t2417;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t51;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.UIVertex>
struct ICollection_1_t342;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex>
struct IEnumerable_1_t2419;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>
struct ReadOnlyCollection_1_t1950;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t185;
// System.Predicate`1<UnityEngine.UIVertex>
struct Predicate_1_t1951;
// System.Comparison`1<UnityEngine.UIVertex>
struct Comparison_1_t1953;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UIVertex.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_14.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor()
extern "C" void List_1__ctor_m1732_gshared (List_1_t187 * __this, const MethodInfo* method);
#define List_1__ctor_m1732(__this, method) (( void (*) (List_1_t187 *, const MethodInfo*))List_1__ctor_m1732_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Int32)
extern "C" void List_1__ctor_m3292_gshared (List_1_t187 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m3292(__this, ___capacity, method) (( void (*) (List_1_t187 *, int32_t, const MethodInfo*))List_1__ctor_m3292_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.cctor()
extern "C" void List_1__cctor_m12696_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m12696(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m12696_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12697_gshared (List_1_t187 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12697(__this, method) (( Object_t* (*) (List_1_t187 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12697_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m12698_gshared (List_1_t187 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m12698(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t187 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m12698_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m12699_gshared (List_1_t187 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m12699(__this, method) (( Object_t * (*) (List_1_t187 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m12699_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m12700_gshared (List_1_t187 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m12700(__this, ___item, method) (( int32_t (*) (List_1_t187 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m12700_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m12701_gshared (List_1_t187 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m12701(__this, ___item, method) (( bool (*) (List_1_t187 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m12701_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m12702_gshared (List_1_t187 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m12702(__this, ___item, method) (( int32_t (*) (List_1_t187 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m12702_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m12703_gshared (List_1_t187 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m12703(__this, ___index, ___item, method) (( void (*) (List_1_t187 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m12703_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m12704_gshared (List_1_t187 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m12704(__this, ___item, method) (( void (*) (List_1_t187 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m12704_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12705_gshared (List_1_t187 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12705(__this, method) (( bool (*) (List_1_t187 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12705_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m12706_gshared (List_1_t187 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m12706(__this, method) (( bool (*) (List_1_t187 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m12706_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m12707_gshared (List_1_t187 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m12707(__this, method) (( Object_t * (*) (List_1_t187 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m12707_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m12708_gshared (List_1_t187 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m12708(__this, method) (( bool (*) (List_1_t187 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m12708_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m12709_gshared (List_1_t187 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m12709(__this, method) (( bool (*) (List_1_t187 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m12709_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m12710_gshared (List_1_t187 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m12710(__this, ___index, method) (( Object_t * (*) (List_1_t187 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m12710_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m12711_gshared (List_1_t187 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m12711(__this, ___index, ___value, method) (( void (*) (List_1_t187 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m12711_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Add(T)
extern "C" void List_1_Add_m12712_gshared (List_1_t187 * __this, UIVertex_t191  ___item, const MethodInfo* method);
#define List_1_Add_m12712(__this, ___item, method) (( void (*) (List_1_t187 *, UIVertex_t191 , const MethodInfo*))List_1_Add_m12712_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m12713_gshared (List_1_t187 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m12713(__this, ___newCount, method) (( void (*) (List_1_t187 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m12713_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m12714_gshared (List_1_t187 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m12714(__this, ___collection, method) (( void (*) (List_1_t187 *, Object_t*, const MethodInfo*))List_1_AddCollection_m12714_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m12715_gshared (List_1_t187 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m12715(__this, ___enumerable, method) (( void (*) (List_1_t187 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m12715_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m12716_gshared (List_1_t187 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m12716(__this, ___collection, method) (( void (*) (List_1_t187 *, Object_t*, const MethodInfo*))List_1_AddRange_m12716_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1950 * List_1_AsReadOnly_m12717_gshared (List_1_t187 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m12717(__this, method) (( ReadOnlyCollection_1_t1950 * (*) (List_1_t187 *, const MethodInfo*))List_1_AsReadOnly_m12717_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear()
extern "C" void List_1_Clear_m12718_gshared (List_1_t187 * __this, const MethodInfo* method);
#define List_1_Clear_m12718(__this, method) (( void (*) (List_1_t187 *, const MethodInfo*))List_1_Clear_m12718_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool List_1_Contains_m12719_gshared (List_1_t187 * __this, UIVertex_t191  ___item, const MethodInfo* method);
#define List_1_Contains_m12719(__this, ___item, method) (( bool (*) (List_1_t187 *, UIVertex_t191 , const MethodInfo*))List_1_Contains_m12719_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m12720_gshared (List_1_t187 * __this, UIVertexU5BU5D_t185* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m12720(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t187 *, UIVertexU5BU5D_t185*, int32_t, const MethodInfo*))List_1_CopyTo_m12720_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::Find(System.Predicate`1<T>)
extern "C" UIVertex_t191  List_1_Find_m12721_gshared (List_1_t187 * __this, Predicate_1_t1951 * ___match, const MethodInfo* method);
#define List_1_Find_m12721(__this, ___match, method) (( UIVertex_t191  (*) (List_1_t187 *, Predicate_1_t1951 *, const MethodInfo*))List_1_Find_m12721_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m12722_gshared (Object_t * __this /* static, unused */, Predicate_1_t1951 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m12722(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1951 *, const MethodInfo*))List_1_CheckMatch_m12722_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m12723_gshared (List_1_t187 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1951 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m12723(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t187 *, int32_t, int32_t, Predicate_1_t1951 *, const MethodInfo*))List_1_GetIndex_m12723_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Enumerator_t1952  List_1_GetEnumerator_m12724_gshared (List_1_t187 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m12724(__this, method) (( Enumerator_t1952  (*) (List_1_t187 *, const MethodInfo*))List_1_GetEnumerator_m12724_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m12725_gshared (List_1_t187 * __this, UIVertex_t191  ___item, const MethodInfo* method);
#define List_1_IndexOf_m12725(__this, ___item, method) (( int32_t (*) (List_1_t187 *, UIVertex_t191 , const MethodInfo*))List_1_IndexOf_m12725_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m12726_gshared (List_1_t187 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m12726(__this, ___start, ___delta, method) (( void (*) (List_1_t187 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m12726_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m12727_gshared (List_1_t187 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m12727(__this, ___index, method) (( void (*) (List_1_t187 *, int32_t, const MethodInfo*))List_1_CheckIndex_m12727_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m12728_gshared (List_1_t187 * __this, int32_t ___index, UIVertex_t191  ___item, const MethodInfo* method);
#define List_1_Insert_m12728(__this, ___index, ___item, method) (( void (*) (List_1_t187 *, int32_t, UIVertex_t191 , const MethodInfo*))List_1_Insert_m12728_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m12729_gshared (List_1_t187 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m12729(__this, ___collection, method) (( void (*) (List_1_t187 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m12729_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool List_1_Remove_m12730_gshared (List_1_t187 * __this, UIVertex_t191  ___item, const MethodInfo* method);
#define List_1_Remove_m12730(__this, ___item, method) (( bool (*) (List_1_t187 *, UIVertex_t191 , const MethodInfo*))List_1_Remove_m12730_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m12731_gshared (List_1_t187 * __this, Predicate_1_t1951 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m12731(__this, ___match, method) (( int32_t (*) (List_1_t187 *, Predicate_1_t1951 *, const MethodInfo*))List_1_RemoveAll_m12731_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m12732_gshared (List_1_t187 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m12732(__this, ___index, method) (( void (*) (List_1_t187 *, int32_t, const MethodInfo*))List_1_RemoveAt_m12732_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Reverse()
extern "C" void List_1_Reverse_m12733_gshared (List_1_t187 * __this, const MethodInfo* method);
#define List_1_Reverse_m12733(__this, method) (( void (*) (List_1_t187 *, const MethodInfo*))List_1_Reverse_m12733_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort()
extern "C" void List_1_Sort_m12734_gshared (List_1_t187 * __this, const MethodInfo* method);
#define List_1_Sort_m12734(__this, method) (( void (*) (List_1_t187 *, const MethodInfo*))List_1_Sort_m12734_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m12735_gshared (List_1_t187 * __this, Comparison_1_t1953 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m12735(__this, ___comparison, method) (( void (*) (List_1_t187 *, Comparison_1_t1953 *, const MethodInfo*))List_1_Sort_m12735_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UIVertex>::ToArray()
extern "C" UIVertexU5BU5D_t185* List_1_ToArray_m1802_gshared (List_1_t187 * __this, const MethodInfo* method);
#define List_1_ToArray_m1802(__this, method) (( UIVertexU5BU5D_t185* (*) (List_1_t187 *, const MethodInfo*))List_1_ToArray_m1802_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::TrimExcess()
extern "C" void List_1_TrimExcess_m12736_gshared (List_1_t187 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m12736(__this, method) (( void (*) (List_1_t187 *, const MethodInfo*))List_1_TrimExcess_m12736_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m1658_gshared (List_1_t187 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m1658(__this, method) (( int32_t (*) (List_1_t187 *, const MethodInfo*))List_1_get_Capacity_m1658_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m1659_gshared (List_1_t187 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m1659(__this, ___value, method) (( void (*) (List_1_t187 *, int32_t, const MethodInfo*))List_1_set_Capacity_m1659_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t List_1_get_Count_m12737_gshared (List_1_t187 * __this, const MethodInfo* method);
#define List_1_get_Count_m12737(__this, method) (( int32_t (*) (List_1_t187 *, const MethodInfo*))List_1_get_Count_m12737_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t191  List_1_get_Item_m12738_gshared (List_1_t187 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m12738(__this, ___index, method) (( UIVertex_t191  (*) (List_1_t187 *, int32_t, const MethodInfo*))List_1_get_Item_m12738_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m12739_gshared (List_1_t187 * __this, int32_t ___index, UIVertex_t191  ___value, const MethodInfo* method);
#define List_1_set_Item_m12739(__this, ___index, ___value, method) (( void (*) (List_1_t187 *, int32_t, UIVertex_t191 , const MethodInfo*))List_1_set_Item_m12739_gshared)(__this, ___index, ___value, method)
