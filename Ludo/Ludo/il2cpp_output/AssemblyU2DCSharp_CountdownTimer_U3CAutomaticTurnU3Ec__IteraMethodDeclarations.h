﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// CountdownTimer/<AutomaticTurn>c__Iterator0
struct U3CAutomaticTurnU3Ec__Iterator0_t7;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void CountdownTimer/<AutomaticTurn>c__Iterator0::.ctor()
extern "C" void U3CAutomaticTurnU3Ec__Iterator0__ctor_m6 (U3CAutomaticTurnU3Ec__Iterator0_t7 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CountdownTimer/<AutomaticTurn>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CAutomaticTurnU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m7 (U3CAutomaticTurnU3Ec__Iterator0_t7 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object CountdownTimer/<AutomaticTurn>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CAutomaticTurnU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m8 (U3CAutomaticTurnU3Ec__Iterator0_t7 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean CountdownTimer/<AutomaticTurn>c__Iterator0::MoveNext()
extern "C" bool U3CAutomaticTurnU3Ec__Iterator0_MoveNext_m9 (U3CAutomaticTurnU3Ec__Iterator0_t7 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer/<AutomaticTurn>c__Iterator0::Dispose()
extern "C" void U3CAutomaticTurnU3Ec__Iterator0_Dispose_m10 (U3CAutomaticTurnU3Ec__Iterator0_t7 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void CountdownTimer/<AutomaticTurn>c__Iterator0::Reset()
extern "C" void U3CAutomaticTurnU3Ec__Iterator0_Reset_m11 (U3CAutomaticTurnU3Ec__Iterator0_t7 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
