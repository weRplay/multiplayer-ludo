﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t5;
// UnityEngine.UI.GridLayoutGroup
struct GridLayoutGroup_t17;
// UnityEngine.RectTransform
struct RectTransform_t18;
// UnityEngine.UI.ScrollRect
struct ScrollRect_t19;
// System.String[]
struct StringU5BU5D_t16;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// DynamicScrollView
struct  DynamicScrollView_t14  : public MonoBehaviour_t2
{
	// System.Int32 DynamicScrollView::noOfItems
	int32_t ___noOfItems_2;
	// UnityEngine.GameObject DynamicScrollView::item
	GameObject_t5 * ___item_4;
	// UnityEngine.UI.GridLayoutGroup DynamicScrollView::gridLayout
	GridLayoutGroup_t17 * ___gridLayout_5;
	// UnityEngine.RectTransform DynamicScrollView::scrollContent
	RectTransform_t18 * ___scrollContent_6;
	// UnityEngine.UI.ScrollRect DynamicScrollView::scrollRect
	ScrollRect_t19 * ___scrollRect_7;
};
struct DynamicScrollView_t14_StaticFields{
	// UnityEngine.GameObject DynamicScrollView::heading
	GameObject_t5 * ___heading_3;
	// System.String[] DynamicScrollView::players
	StringU5BU5D_t16* ___players_8;
};
