﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_37.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m15165_gshared (InternalEnumerator_1_t2119 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m15165(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2119 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m15165_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15166_gshared (InternalEnumerator_1_t2119 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15166(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2119 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15166_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m15167_gshared (InternalEnumerator_1_t2119 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m15167(__this, method) (( void (*) (InternalEnumerator_1_t2119 *, const MethodInfo*))InternalEnumerator_1_Dispose_m15167_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m15168_gshared (InternalEnumerator_1_t2119 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m15168(__this, method) (( bool (*) (InternalEnumerator_1_t2119 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m15168_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>::get_Current()
extern "C" KeyValuePair_2_t2118  InternalEnumerator_1_get_Current_m15169_gshared (InternalEnumerator_1_t2119 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m15169(__this, method) (( KeyValuePair_2_t2118  (*) (InternalEnumerator_1_t2119 *, const MethodInfo*))InternalEnumerator_1_get_Current_m15169_gshared)(__this, method)
