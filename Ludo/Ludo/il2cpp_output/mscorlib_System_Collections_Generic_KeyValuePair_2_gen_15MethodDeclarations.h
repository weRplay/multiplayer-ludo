﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m15268(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2133 *, String_t*, int64_t, const MethodInfo*))KeyValuePair_2__ctor_m15170_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::get_Key()
#define KeyValuePair_2_get_Key_m15269(__this, method) (( String_t* (*) (KeyValuePair_2_t2133 *, const MethodInfo*))KeyValuePair_2_get_Key_m15171_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m15270(__this, ___value, method) (( void (*) (KeyValuePair_2_t2133 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m15172_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::get_Value()
#define KeyValuePair_2_get_Value_m15271(__this, method) (( int64_t (*) (KeyValuePair_2_t2133 *, const MethodInfo*))KeyValuePair_2_get_Value_m15173_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m15272(__this, ___value, method) (( void (*) (KeyValuePair_2_t2133 *, int64_t, const MethodInfo*))KeyValuePair_2_set_Value_m15174_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Int64>::ToString()
#define KeyValuePair_2_ToString_m15273(__this, method) (( String_t* (*) (KeyValuePair_2_t2133 *, const MethodInfo*))KeyValuePair_2_ToString_m15175_gshared)(__this, method)
