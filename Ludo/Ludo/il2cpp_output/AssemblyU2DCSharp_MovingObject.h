﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.BoxCollider2D
struct BoxCollider2D_t38;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t39;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_LayerMask.h"

// MovingObject
struct  MovingObject_t36  : public MonoBehaviour_t2
{
	// System.Single MovingObject::moveTime
	float ___moveTime_2;
	// UnityEngine.LayerMask MovingObject::blockingLayer
	LayerMask_t37  ___blockingLayer_3;
	// UnityEngine.BoxCollider2D MovingObject::boxCollider
	BoxCollider2D_t38 * ___boxCollider_4;
	// UnityEngine.Rigidbody2D MovingObject::rb2D
	Rigidbody2D_t39 * ___rb2D_5;
	// System.Single MovingObject::inverseMoveTime
	float ___inverseMoveTime_6;
};
