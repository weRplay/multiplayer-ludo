﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_12.h"
#include "UnityEngine_UnityEngine_RaycastHit.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m12026_gshared (InternalEnumerator_1_t1901 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m12026(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1901 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m12026_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12027_gshared (InternalEnumerator_1_t1901 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12027(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1901 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12027_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m12028_gshared (InternalEnumerator_1_t1901 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m12028(__this, method) (( void (*) (InternalEnumerator_1_t1901 *, const MethodInfo*))InternalEnumerator_1_Dispose_m12028_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m12029_gshared (InternalEnumerator_1_t1901 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m12029(__this, method) (( bool (*) (InternalEnumerator_1_t1901 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m12029_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.RaycastHit>::get_Current()
extern "C" RaycastHit_t283  InternalEnumerator_1_get_Current_m12030_gshared (InternalEnumerator_1_t1901 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m12030(__this, method) (( RaycastHit_t283  (*) (InternalEnumerator_1_t1901 *, const MethodInfo*))InternalEnumerator_1_get_Current_m12030_gshared)(__this, method)
