﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t8;
// UnityEngine.WWW
struct WWW_t9;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t16;
// UnityEngine.GameObject
struct GameObject_t5;
// System.Object
struct Object_t;
// GameManager
struct GameManager_t22;

#include "mscorlib_System_Object.h"

// GameManager/<GetPlayer1Turn>c__Iterator4
struct  U3CGetPlayer1TurnU3Ec__Iterator4_t21  : public Object_t
{
	// UnityEngine.WWWForm GameManager/<GetPlayer1Turn>c__Iterator4::<form>__0
	WWWForm_t8 * ___U3CformU3E__0_0;
	// UnityEngine.WWW GameManager/<GetPlayer1Turn>c__Iterator4::<w>__1
	WWW_t9 * ___U3CwU3E__1_1;
	// System.String GameManager/<GetPlayer1Turn>c__Iterator4::<JsonString>__2
	String_t* ___U3CJsonStringU3E__2_2;
	// System.String[] GameManager/<GetPlayer1Turn>c__Iterator4::<array>__3
	StringU5BU5D_t16* ___U3CarrayU3E__3_3;
	// System.String GameManager/<GetPlayer1Turn>c__Iterator4::<ResetBool>__4
	String_t* ___U3CResetBoolU3E__4_4;
	// System.Single GameManager/<GetPlayer1Turn>c__Iterator4::<x>__5
	float ___U3CxU3E__5_5;
	// System.Single GameManager/<GetPlayer1Turn>c__Iterator4::<y>__6
	float ___U3CyU3E__6_6;
	// System.Int32 GameManager/<GetPlayer1Turn>c__Iterator4::<dn>__7
	int32_t ___U3CdnU3E__7_7;
	// System.Single GameManager/<GetPlayer1Turn>c__Iterator4::<Playerx>__8
	float ___U3CPlayerxU3E__8_8;
	// System.Single GameManager/<GetPlayer1Turn>c__Iterator4::<Playery>__9
	float ___U3CPlayeryU3E__9_9;
	// System.Int32 GameManager/<GetPlayer1Turn>c__Iterator4::<p>__10
	int32_t ___U3CpU3E__10_10;
	// UnityEngine.GameObject GameManager/<GetPlayer1Turn>c__Iterator4::<toInstantiate>__11
	GameObject_t5 * ___U3CtoInstantiateU3E__11_11;
	// UnityEngine.GameObject GameManager/<GetPlayer1Turn>c__Iterator4::<toInstantiate>__12
	GameObject_t5 * ___U3CtoInstantiateU3E__12_12;
	// System.String GameManager/<GetPlayer1Turn>c__Iterator4::<ResetBool>__13
	String_t* ___U3CResetBoolU3E__13_13;
	// System.Single GameManager/<GetPlayer1Turn>c__Iterator4::<x>__14
	float ___U3CxU3E__14_14;
	// System.Single GameManager/<GetPlayer1Turn>c__Iterator4::<y>__15
	float ___U3CyU3E__15_15;
	// System.Int32 GameManager/<GetPlayer1Turn>c__Iterator4::<dn>__16
	int32_t ___U3CdnU3E__16_16;
	// System.Single GameManager/<GetPlayer1Turn>c__Iterator4::<Playerx>__17
	float ___U3CPlayerxU3E__17_17;
	// System.Single GameManager/<GetPlayer1Turn>c__Iterator4::<Playery>__18
	float ___U3CPlayeryU3E__18_18;
	// System.Int32 GameManager/<GetPlayer1Turn>c__Iterator4::<p>__19
	int32_t ___U3CpU3E__19_19;
	// UnityEngine.GameObject GameManager/<GetPlayer1Turn>c__Iterator4::<toInstantiate>__20
	GameObject_t5 * ___U3CtoInstantiateU3E__20_20;
	// System.Int32 GameManager/<GetPlayer1Turn>c__Iterator4::$PC
	int32_t ___U24PC_21;
	// System.Object GameManager/<GetPlayer1Turn>c__Iterator4::$current
	Object_t * ___U24current_22;
	// GameManager GameManager/<GetPlayer1Turn>c__Iterator4::<>f__this
	GameManager_t22 * ___U3CU3Ef__this_23;
};
