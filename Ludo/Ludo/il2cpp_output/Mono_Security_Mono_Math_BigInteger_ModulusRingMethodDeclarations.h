﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Mono.Math.BigInteger/ModulusRing
struct ModulusRing_t733;
// Mono.Math.BigInteger
struct BigInteger_t734;

#include "codegen/il2cpp-codegen.h"

// System.Void Mono.Math.BigInteger/ModulusRing::.ctor(Mono.Math.BigInteger)
extern "C" void ModulusRing__ctor_m3468 (ModulusRing_t733 * __this, BigInteger_t734 * ___modulus, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Mono.Math.BigInteger/ModulusRing::BarrettReduction(Mono.Math.BigInteger)
extern "C" void ModulusRing_BarrettReduction_m3469 (ModulusRing_t733 * __this, BigInteger_t734 * ___x, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Multiply(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t734 * ModulusRing_Multiply_m3470 (ModulusRing_t733 * __this, BigInteger_t734 * ___a, BigInteger_t734 * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Difference(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t734 * ModulusRing_Difference_m3471 (ModulusRing_t733 * __this, BigInteger_t734 * ___a, BigInteger_t734 * ___b, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(Mono.Math.BigInteger,Mono.Math.BigInteger)
extern "C" BigInteger_t734 * ModulusRing_Pow_m3472 (ModulusRing_t733 * __this, BigInteger_t734 * ___a, BigInteger_t734 * ___k, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// Mono.Math.BigInteger Mono.Math.BigInteger/ModulusRing::Pow(System.UInt32,Mono.Math.BigInteger)
extern "C" BigInteger_t734 * ModulusRing_Pow_m3473 (ModulusRing_t733 * __this, uint32_t ___b, BigInteger_t734 * ___exp, const MethodInfo* method) IL2CPP_METHOD_ATTR;
