﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameMenu
struct GameMenu_t29;
// System.Collections.IEnumerator
struct IEnumerator_t51;

#include "codegen/il2cpp-codegen.h"

// System.Void GameMenu::.ctor()
extern "C" void GameMenu__ctor_m102 (GameMenu_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMenu::Start()
extern "C" void GameMenu_Start_m103 (GameMenu_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMenu::CallSetActiveUsers()
extern "C" void GameMenu_CallSetActiveUsers_m104 (GameMenu_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameMenu::SetUserOnline()
extern "C" Object_t * GameMenu_SetUserOnline_m105 (GameMenu_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator GameMenu::GetActiveUsers()
extern "C" Object_t * GameMenu_GetActiveUsers_m106 (GameMenu_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameMenu::Refresh()
extern "C" void GameMenu_Refresh_m107 (GameMenu_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
