﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m16048(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2187 *, Type_t *, Object_t*, const MethodInfo*))KeyValuePair_2__ctor_m12459_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::get_Key()
#define KeyValuePair_2_get_Key_m16049(__this, method) (( Type_t * (*) (KeyValuePair_2_t2187 *, const MethodInfo*))KeyValuePair_2_get_Key_m12460_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m16050(__this, ___value, method) (( void (*) (KeyValuePair_2_t2187 *, Type_t *, const MethodInfo*))KeyValuePair_2_set_Key_m12461_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::get_Value()
#define KeyValuePair_2_get_Value_m16051(__this, method) (( Object_t* (*) (KeyValuePair_2_t2187 *, const MethodInfo*))KeyValuePair_2_get_Value_m12462_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m16052(__this, ___value, method) (( void (*) (KeyValuePair_2_t2187 *, Object_t*, const MethodInfo*))KeyValuePair_2_set_Value_m12463_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::ToString()
#define KeyValuePair_2_ToString_m16053(__this, method) (( String_t* (*) (KeyValuePair_2_t2187 *, const MethodInfo*))KeyValuePair_2_ToString_m12464_gshared)(__this, method)
