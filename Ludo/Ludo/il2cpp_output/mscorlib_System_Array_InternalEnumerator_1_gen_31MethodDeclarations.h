﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_31.h"

// System.Void System.Array/InternalEnumerator`1<System.Byte>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m14476_gshared (InternalEnumerator_1_t2075 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m14476(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2075 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14476_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Byte>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14477_gshared (InternalEnumerator_1_t2075 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14477(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2075 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14477_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Byte>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m14478_gshared (InternalEnumerator_1_t2075 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m14478(__this, method) (( void (*) (InternalEnumerator_1_t2075 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14478_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Byte>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m14479_gshared (InternalEnumerator_1_t2075 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m14479(__this, method) (( bool (*) (InternalEnumerator_1_t2075 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14479_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Byte>::get_Current()
extern "C" uint8_t InternalEnumerator_1_get_Current_m14480_gshared (InternalEnumerator_1_t2075 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m14480(__this, method) (( uint8_t (*) (InternalEnumerator_1_t2075 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14480_gshared)(__this, method)
