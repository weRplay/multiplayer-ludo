﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t2199;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_23.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m16340_gshared (Enumerator_t2203 * __this, Dictionary_2_t2199 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m16340(__this, ___host, method) (( void (*) (Enumerator_t2203 *, Dictionary_2_t2199 *, const MethodInfo*))Enumerator__ctor_m16340_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16341_gshared (Enumerator_t2203 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16341(__this, method) (( Object_t * (*) (Enumerator_t2203 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16341_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::Dispose()
extern "C" void Enumerator_Dispose_m16342_gshared (Enumerator_t2203 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16342(__this, method) (( void (*) (Enumerator_t2203 *, const MethodInfo*))Enumerator_Dispose_m16342_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16343_gshared (Enumerator_t2203 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16343(__this, method) (( bool (*) (Enumerator_t2203 *, const MethodInfo*))Enumerator_MoveNext_m16343_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m16344_gshared (Enumerator_t2203 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16344(__this, method) (( Object_t * (*) (Enumerator_t2203 *, const MethodInfo*))Enumerator_get_Current_m16344_gshared)(__this, method)
