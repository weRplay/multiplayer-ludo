﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__21MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m17347(__this, ___dictionary, method) (( void (*) (Enumerator_t2307 *, Dictionary_2_t951 *, const MethodInfo*))Enumerator__ctor_m17245_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17348(__this, method) (( Object_t * (*) (Enumerator_t2307 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17246_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17349(__this, method) (( DictionaryEntry_t1068  (*) (Enumerator_t2307 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17247_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17350(__this, method) (( Object_t * (*) (Enumerator_t2307 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17248_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17351(__this, method) (( Object_t * (*) (Enumerator_t2307 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17249_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m17352(__this, method) (( bool (*) (Enumerator_t2307 *, const MethodInfo*))Enumerator_MoveNext_m17250_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_Current()
#define Enumerator_get_Current_m17353(__this, method) (( KeyValuePair_2_t2304  (*) (Enumerator_t2307 *, const MethodInfo*))Enumerator_get_Current_m17251_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m17354(__this, method) (( String_t* (*) (Enumerator_t2307 *, const MethodInfo*))Enumerator_get_CurrentKey_m17252_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m17355(__this, method) (( bool (*) (Enumerator_t2307 *, const MethodInfo*))Enumerator_get_CurrentValue_m17253_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyState()
#define Enumerator_VerifyState_m17356(__this, method) (( void (*) (Enumerator_t2307 *, const MethodInfo*))Enumerator_VerifyState_m17254_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m17357(__this, method) (( void (*) (Enumerator_t2307 *, const MethodInfo*))Enumerator_VerifyCurrent_m17255_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Dispose()
#define Enumerator_Dispose_m17358(__this, method) (( void (*) (Enumerator_t2307 *, const MethodInfo*))Enumerator_Dispose_m17256_gshared)(__this, method)
