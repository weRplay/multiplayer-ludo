﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Text.DecoderFallback
struct DecoderFallback_t1582;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Text.DecoderFallback::.ctor()
extern "C" void DecoderFallback__ctor_m9246 (DecoderFallback_t1582 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Text.DecoderFallback::.cctor()
extern "C" void DecoderFallback__cctor_m9247 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallback System.Text.DecoderFallback::get_ExceptionFallback()
extern "C" DecoderFallback_t1582 * DecoderFallback_get_ExceptionFallback_m9248 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallback System.Text.DecoderFallback::get_ReplacementFallback()
extern "C" DecoderFallback_t1582 * DecoderFallback_get_ReplacementFallback_m9249 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Text.DecoderFallback System.Text.DecoderFallback::get_StandardSafeFallback()
extern "C" DecoderFallback_t1582 * DecoderFallback_get_StandardSafeFallback_m9250 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
