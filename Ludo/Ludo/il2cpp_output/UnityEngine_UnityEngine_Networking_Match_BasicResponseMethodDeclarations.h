﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Networking.Match.BasicResponse
struct BasicResponse_t486;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Networking.Match.BasicResponse::.ctor()
extern "C" void BasicResponse__ctor_m2820 (BasicResponse_t486 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
