﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t8;
// UnityEngine.WWW
struct WWW_t9;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t16;
// System.Object
struct Object_t;
// Player
struct Player_t11;

#include "mscorlib_System_Object.h"

// Player/<GetPlayer1Position>c__IteratorD
struct  U3CGetPlayer1PositionU3Ec__IteratorD_t41  : public Object_t
{
	// UnityEngine.WWWForm Player/<GetPlayer1Position>c__IteratorD::<form>__0
	WWWForm_t8 * ___U3CformU3E__0_0;
	// UnityEngine.WWW Player/<GetPlayer1Position>c__IteratorD::<w>__1
	WWW_t9 * ___U3CwU3E__1_1;
	// System.String Player/<GetPlayer1Position>c__IteratorD::<JsonString>__2
	String_t* ___U3CJsonStringU3E__2_2;
	// System.String[] Player/<GetPlayer1Position>c__IteratorD::<array>__3
	StringU5BU5D_t16* ___U3CarrayU3E__3_3;
	// System.Single Player/<GetPlayer1Position>c__IteratorD::<x>__4
	float ___U3CxU3E__4_4;
	// System.Single Player/<GetPlayer1Position>c__IteratorD::<y>__5
	float ___U3CyU3E__5_5;
	// System.Int32 Player/<GetPlayer1Position>c__IteratorD::<p>__6
	int32_t ___U3CpU3E__6_6;
	// System.Int32 Player/<GetPlayer1Position>c__IteratorD::$PC
	int32_t ___U24PC_7;
	// System.Object Player/<GetPlayer1Position>c__IteratorD::$current
	Object_t * ___U24current_8;
	// Player Player/<GetPlayer1Position>c__IteratorD::<>f__this
	Player_t11 * ___U3CU3Ef__this_9;
};
