﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe_6MethodDeclarations.h"

// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::.ctor(SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionaryValueFactory`2<TKey,TValue>)
#define ThreadSafeDictionary_2__ctor_m3344(__this, ___valueFactory, method) (( void (*) (ThreadSafeDictionary_2_t669 *, ThreadSafeDictionaryValueFactory_2_t668 *, const MethodInfo*))ThreadSafeDictionary_2__ctor_m15887_gshared)(__this, ___valueFactory, method)
// System.Collections.IEnumerator SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::System.Collections.IEnumerable.GetEnumerator()
#define ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m15888(__this, method) (( Object_t * (*) (ThreadSafeDictionary_2_t669 *, const MethodInfo*))ThreadSafeDictionary_2_System_Collections_IEnumerable_GetEnumerator_m15889_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Get(TKey)
#define ThreadSafeDictionary_2_Get_m15890(__this, ___key, method) (( ConstructorDelegate_t522 * (*) (ThreadSafeDictionary_2_t669 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_Get_m15891_gshared)(__this, ___key, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::AddValue(TKey)
#define ThreadSafeDictionary_2_AddValue_m15892(__this, ___key, method) (( ConstructorDelegate_t522 * (*) (ThreadSafeDictionary_2_t669 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_AddValue_m15893_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Add(TKey,TValue)
#define ThreadSafeDictionary_2_Add_m15894(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t669 *, Type_t *, ConstructorDelegate_t522 *, const MethodInfo*))ThreadSafeDictionary_2_Add_m15895_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TKey> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Keys()
#define ThreadSafeDictionary_2_get_Keys_m15896(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t669 *, const MethodInfo*))ThreadSafeDictionary_2_get_Keys_m15897_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::TryGetValue(TKey,TValue&)
#define ThreadSafeDictionary_2_TryGetValue_m15898(__this, ___key, ___value, method) (( bool (*) (ThreadSafeDictionary_2_t669 *, Type_t *, ConstructorDelegate_t522 **, const MethodInfo*))ThreadSafeDictionary_2_TryGetValue_m15899_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.ICollection`1<TValue> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Values()
#define ThreadSafeDictionary_2_get_Values_m15900(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t669 *, const MethodInfo*))ThreadSafeDictionary_2_get_Values_m15901_gshared)(__this, method)
// TValue SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Item(TKey)
#define ThreadSafeDictionary_2_get_Item_m15902(__this, ___key, method) (( ConstructorDelegate_t522 * (*) (ThreadSafeDictionary_2_t669 *, Type_t *, const MethodInfo*))ThreadSafeDictionary_2_get_Item_m15903_gshared)(__this, ___key, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::set_Item(TKey,TValue)
#define ThreadSafeDictionary_2_set_Item_m15904(__this, ___key, ___value, method) (( void (*) (ThreadSafeDictionary_2_t669 *, Type_t *, ConstructorDelegate_t522 *, const MethodInfo*))ThreadSafeDictionary_2_set_Item_m15905_gshared)(__this, ___key, ___value, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Add_m15906(__this, ___item, method) (( void (*) (ThreadSafeDictionary_2_t669 *, KeyValuePair_2_t2183 , const MethodInfo*))ThreadSafeDictionary_2_Add_m15907_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Clear()
#define ThreadSafeDictionary_2_Clear_m15908(__this, method) (( void (*) (ThreadSafeDictionary_2_t669 *, const MethodInfo*))ThreadSafeDictionary_2_Clear_m15909_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Contains_m15910(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t669 *, KeyValuePair_2_t2183 , const MethodInfo*))ThreadSafeDictionary_2_Contains_m15911_gshared)(__this, ___item, method)
// System.Void SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define ThreadSafeDictionary_2_CopyTo_m15912(__this, ___array, ___arrayIndex, method) (( void (*) (ThreadSafeDictionary_2_t669 *, KeyValuePair_2U5BU5D_t2464*, int32_t, const MethodInfo*))ThreadSafeDictionary_2_CopyTo_m15913_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_Count()
#define ThreadSafeDictionary_2_get_Count_m15914(__this, method) (( int32_t (*) (ThreadSafeDictionary_2_t669 *, const MethodInfo*))ThreadSafeDictionary_2_get_Count_m15915_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::get_IsReadOnly()
#define ThreadSafeDictionary_2_get_IsReadOnly_m15916(__this, method) (( bool (*) (ThreadSafeDictionary_2_t669 *, const MethodInfo*))ThreadSafeDictionary_2_get_IsReadOnly_m15917_gshared)(__this, method)
// System.Boolean SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define ThreadSafeDictionary_2_Remove_m15918(__this, ___item, method) (( bool (*) (ThreadSafeDictionary_2_t669 *, KeyValuePair_2_t2183 , const MethodInfo*))ThreadSafeDictionary_2_Remove_m15919_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> SimpleJson.Reflection.ReflectionUtils/ThreadSafeDictionary`2<System.Type,SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate>::GetEnumerator()
#define ThreadSafeDictionary_2_GetEnumerator_m15920(__this, method) (( Object_t* (*) (ThreadSafeDictionary_2_t669 *, const MethodInfo*))ThreadSafeDictionary_2_GetEnumerator_m15921_gshared)(__this, method)
