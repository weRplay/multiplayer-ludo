﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MovingObject/<SmoothMovement>c__IteratorB
struct U3CSmoothMovementU3Ec__IteratorB_t34;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void MovingObject/<SmoothMovement>c__IteratorB::.ctor()
extern "C" void U3CSmoothMovementU3Ec__IteratorB__ctor_m121 (U3CSmoothMovementU3Ec__IteratorB_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MovingObject/<SmoothMovement>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CSmoothMovementU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m122 (U3CSmoothMovementU3Ec__IteratorB_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object MovingObject/<SmoothMovement>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CSmoothMovementU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m123 (U3CSmoothMovementU3Ec__IteratorB_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MovingObject/<SmoothMovement>c__IteratorB::MoveNext()
extern "C" bool U3CSmoothMovementU3Ec__IteratorB_MoveNext_m124 (U3CSmoothMovementU3Ec__IteratorB_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObject/<SmoothMovement>c__IteratorB::Dispose()
extern "C" void U3CSmoothMovementU3Ec__IteratorB_Dispose_m125 (U3CSmoothMovementU3Ec__IteratorB_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObject/<SmoothMovement>c__IteratorB::Reset()
extern "C" void U3CSmoothMovementU3Ec__IteratorB_Reset_m126 (U3CSmoothMovementU3Ec__IteratorB_t34 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
