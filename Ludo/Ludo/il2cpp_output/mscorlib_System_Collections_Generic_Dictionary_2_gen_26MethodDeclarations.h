﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1000;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t1874;
// System.Collections.Generic.IDictionary`2<System.Int32,System.Int32>
struct IDictionary_2_t2503;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t631;
// System.Collections.Generic.ICollection`1<System.Int32>
struct ICollection_1_t2402;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>[]
struct KeyValuePair_2U5BU5D_t2502;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t51;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Int32>>
struct IEnumerator_1_t2504;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1067;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Int32,System.Int32>
struct KeyCollection_t2314;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Int32>
struct ValueCollection_t2318;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_27.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__23.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor()
extern "C" void Dictionary_2__ctor_m17374_gshared (Dictionary_2_t1000 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m17374(__this, method) (( void (*) (Dictionary_2_t1000 *, const MethodInfo*))Dictionary_2__ctor_m17374_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m17375_gshared (Dictionary_2_t1000 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m17375(__this, ___comparer, method) (( void (*) (Dictionary_2_t1000 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m17375_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m17376_gshared (Dictionary_2_t1000 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m17376(__this, ___dictionary, method) (( void (*) (Dictionary_2_t1000 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m17376_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m17377_gshared (Dictionary_2_t1000 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m17377(__this, ___capacity, method) (( void (*) (Dictionary_2_t1000 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m17377_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m17378_gshared (Dictionary_2_t1000 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m17378(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t1000 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m17378_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m17379_gshared (Dictionary_2_t1000 * __this, SerializationInfo_t631 * ___info, StreamingContext_t632  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m17379(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1000 *, SerializationInfo_t631 *, StreamingContext_t632 , const MethodInfo*))Dictionary_2__ctor_m17379_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m17380_gshared (Dictionary_2_t1000 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m17380(__this, method) (( Object_t* (*) (Dictionary_2_t1000 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m17380_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m17381_gshared (Dictionary_2_t1000 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m17381(__this, method) (( Object_t* (*) (Dictionary_2_t1000 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m17381_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m17382_gshared (Dictionary_2_t1000 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m17382(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1000 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m17382_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m17383_gshared (Dictionary_2_t1000 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m17383(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1000 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m17383_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m17384_gshared (Dictionary_2_t1000 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m17384(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1000 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m17384_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m17385_gshared (Dictionary_2_t1000 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m17385(__this, ___key, method) (( bool (*) (Dictionary_2_t1000 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m17385_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m17386_gshared (Dictionary_2_t1000 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m17386(__this, ___key, method) (( void (*) (Dictionary_2_t1000 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m17386_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17387_gshared (Dictionary_2_t1000 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17387(__this, method) (( bool (*) (Dictionary_2_t1000 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m17387_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17388_gshared (Dictionary_2_t1000 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17388(__this, method) (( Object_t * (*) (Dictionary_2_t1000 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m17388_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17389_gshared (Dictionary_2_t1000 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17389(__this, method) (( bool (*) (Dictionary_2_t1000 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m17389_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17390_gshared (Dictionary_2_t1000 * __this, KeyValuePair_2_t2312  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17390(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1000 *, KeyValuePair_2_t2312 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m17390_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17391_gshared (Dictionary_2_t1000 * __this, KeyValuePair_2_t2312  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17391(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1000 *, KeyValuePair_2_t2312 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m17391_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17392_gshared (Dictionary_2_t1000 * __this, KeyValuePair_2U5BU5D_t2502* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17392(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1000 *, KeyValuePair_2U5BU5D_t2502*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m17392_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17393_gshared (Dictionary_2_t1000 * __this, KeyValuePair_2_t2312  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17393(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1000 *, KeyValuePair_2_t2312 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m17393_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m17394_gshared (Dictionary_2_t1000 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m17394(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1000 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m17394_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17395_gshared (Dictionary_2_t1000 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17395(__this, method) (( Object_t * (*) (Dictionary_2_t1000 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m17395_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17396_gshared (Dictionary_2_t1000 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17396(__this, method) (( Object_t* (*) (Dictionary_2_t1000 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m17396_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17397_gshared (Dictionary_2_t1000 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17397(__this, method) (( Object_t * (*) (Dictionary_2_t1000 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m17397_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m17398_gshared (Dictionary_2_t1000 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m17398(__this, method) (( int32_t (*) (Dictionary_2_t1000 *, const MethodInfo*))Dictionary_2_get_Count_m17398_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m17399_gshared (Dictionary_2_t1000 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m17399(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1000 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m17399_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m17400_gshared (Dictionary_2_t1000 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m17400(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1000 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_set_Item_m17400_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m17401_gshared (Dictionary_2_t1000 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m17401(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1000 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m17401_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m17402_gshared (Dictionary_2_t1000 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m17402(__this, ___size, method) (( void (*) (Dictionary_2_t1000 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m17402_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m17403_gshared (Dictionary_2_t1000 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m17403(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1000 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m17403_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2312  Dictionary_2_make_pair_m17404_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m17404(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2312  (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_make_pair_m17404_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::pick_key(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_key_m17405_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m17405(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_key_m17405_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m17406_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m17406(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_pick_value_m17406_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m17407_gshared (Dictionary_2_t1000 * __this, KeyValuePair_2U5BU5D_t2502* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m17407(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1000 *, KeyValuePair_2U5BU5D_t2502*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m17407_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Resize()
extern "C" void Dictionary_2_Resize_m17408_gshared (Dictionary_2_t1000 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m17408(__this, method) (( void (*) (Dictionary_2_t1000 *, const MethodInfo*))Dictionary_2_Resize_m17408_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m17409_gshared (Dictionary_2_t1000 * __this, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m17409(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1000 *, int32_t, int32_t, const MethodInfo*))Dictionary_2_Add_m17409_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Clear()
extern "C" void Dictionary_2_Clear_m17410_gshared (Dictionary_2_t1000 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m17410(__this, method) (( void (*) (Dictionary_2_t1000 *, const MethodInfo*))Dictionary_2_Clear_m17410_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m17411_gshared (Dictionary_2_t1000 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m17411(__this, ___key, method) (( bool (*) (Dictionary_2_t1000 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m17411_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m17412_gshared (Dictionary_2_t1000 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m17412(__this, ___value, method) (( bool (*) (Dictionary_2_t1000 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m17412_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m17413_gshared (Dictionary_2_t1000 * __this, SerializationInfo_t631 * ___info, StreamingContext_t632  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m17413(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1000 *, SerializationInfo_t631 *, StreamingContext_t632 , const MethodInfo*))Dictionary_2_GetObjectData_m17413_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m17414_gshared (Dictionary_2_t1000 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m17414(__this, ___sender, method) (( void (*) (Dictionary_2_t1000 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m17414_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m17415_gshared (Dictionary_2_t1000 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m17415(__this, ___key, method) (( bool (*) (Dictionary_2_t1000 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m17415_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m17416_gshared (Dictionary_2_t1000 * __this, int32_t ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m17416(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1000 *, int32_t, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m17416_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Keys()
extern "C" KeyCollection_t2314 * Dictionary_2_get_Keys_m17417_gshared (Dictionary_2_t1000 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m17417(__this, method) (( KeyCollection_t2314 * (*) (Dictionary_2_t1000 *, const MethodInfo*))Dictionary_2_get_Keys_m17417_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::get_Values()
extern "C" ValueCollection_t2318 * Dictionary_2_get_Values_m17418_gshared (Dictionary_2_t1000 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m17418(__this, method) (( ValueCollection_t2318 * (*) (Dictionary_2_t1000 *, const MethodInfo*))Dictionary_2_get_Values_m17418_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m17419_gshared (Dictionary_2_t1000 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m17419(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1000 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m17419_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m17420_gshared (Dictionary_2_t1000 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m17420(__this, ___value, method) (( int32_t (*) (Dictionary_2_t1000 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m17420_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m17421_gshared (Dictionary_2_t1000 * __this, KeyValuePair_2_t2312  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m17421(__this, ___pair, method) (( bool (*) (Dictionary_2_t1000 *, KeyValuePair_2_t2312 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m17421_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::GetEnumerator()
extern "C" Enumerator_t2316  Dictionary_2_GetEnumerator_m17422_gshared (Dictionary_2_t1000 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m17422(__this, method) (( Enumerator_t2316  (*) (Dictionary_2_t1000 *, const MethodInfo*))Dictionary_2_GetEnumerator_m17422_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1068  Dictionary_2_U3CCopyToU3Em__0_m17423_gshared (Object_t * __this /* static, unused */, int32_t ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m17423(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1068  (*) (Object_t * /* static, unused */, int32_t, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m17423_gshared)(__this /* static, unused */, ___key, ___value, method)
