﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>
struct Dictionary_2_t2153;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15634_gshared (Enumerator_t2160 * __this, Dictionary_2_t2153 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m15634(__this, ___dictionary, method) (( void (*) (Enumerator_t2160 *, Dictionary_2_t2153 *, const MethodInfo*))Enumerator__ctor_m15634_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15635_gshared (Enumerator_t2160 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15635(__this, method) (( Object_t * (*) (Enumerator_t2160 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15635_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1068  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15636_gshared (Enumerator_t2160 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15636(__this, method) (( DictionaryEntry_t1068  (*) (Enumerator_t2160 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15636_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15637_gshared (Enumerator_t2160 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15637(__this, method) (( Object_t * (*) (Enumerator_t2160 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15637_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15638_gshared (Enumerator_t2160 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15638(__this, method) (( Object_t * (*) (Enumerator_t2160 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15638_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15639_gshared (Enumerator_t2160 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15639(__this, method) (( bool (*) (Enumerator_t2160 *, const MethodInfo*))Enumerator_MoveNext_m15639_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Current()
extern "C" KeyValuePair_2_t2155  Enumerator_get_Current_m15640_gshared (Enumerator_t2160 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15640(__this, method) (( KeyValuePair_2_t2155  (*) (Enumerator_t2160 *, const MethodInfo*))Enumerator_get_Current_m15640_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_CurrentKey()
extern "C" uint64_t Enumerator_get_CurrentKey_m15641_gshared (Enumerator_t2160 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m15641(__this, method) (( uint64_t (*) (Enumerator_t2160 *, const MethodInfo*))Enumerator_get_CurrentKey_m15641_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m15642_gshared (Enumerator_t2160 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m15642(__this, method) (( Object_t * (*) (Enumerator_t2160 *, const MethodInfo*))Enumerator_get_CurrentValue_m15642_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m15643_gshared (Enumerator_t2160 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15643(__this, method) (( void (*) (Enumerator_t2160 *, const MethodInfo*))Enumerator_VerifyState_m15643_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m15644_gshared (Enumerator_t2160 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m15644(__this, method) (( void (*) (Enumerator_t2160 *, const MethodInfo*))Enumerator_VerifyCurrent_m15644_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m15645_gshared (Enumerator_t2160 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15645(__this, method) (( void (*) (Enumerator_t2160 *, const MethodInfo*))Enumerator_Dispose_m15645_gshared)(__this, method)
