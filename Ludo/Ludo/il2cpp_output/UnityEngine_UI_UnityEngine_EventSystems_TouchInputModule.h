﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UI_UnityEngine_EventSystems_PointerInputModule.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// UnityEngine.EventSystems.TouchInputModule
struct  TouchInputModule_t119  : public PointerInputModule_t115
{
	// UnityEngine.Vector2 UnityEngine.EventSystems.TouchInputModule::m_LastMousePosition
	Vector2_t59  ___m_LastMousePosition_12;
	// UnityEngine.Vector2 UnityEngine.EventSystems.TouchInputModule::m_MousePosition
	Vector2_t59  ___m_MousePosition_13;
	// System.Boolean UnityEngine.EventSystems.TouchInputModule::m_AllowActivationOnStandalone
	bool ___m_AllowActivationOnStandalone_14;
};
