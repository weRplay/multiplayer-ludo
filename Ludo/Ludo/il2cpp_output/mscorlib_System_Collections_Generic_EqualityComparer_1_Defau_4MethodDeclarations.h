﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct DefaultComparer_t2101;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m14906_gshared (DefaultComparer_t2101 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m14906(__this, method) (( void (*) (DefaultComparer_t2101 *, const MethodInfo*))DefaultComparer__ctor_m14906_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m14907_gshared (DefaultComparer_t2101 * __this, UICharInfo_t333  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m14907(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2101 *, UICharInfo_t333 , const MethodInfo*))DefaultComparer_GetHashCode_m14907_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.UICharInfo>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m14908_gshared (DefaultComparer_t2101 * __this, UICharInfo_t333  ___x, UICharInfo_t333  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m14908(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2101 *, UICharInfo_t333 , UICharInfo_t333 , const MethodInfo*))DefaultComparer_Equals_m14908_gshared)(__this, ___x, ___y, method)
