﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m17058(__this, ___l, method) (( void (*) (Enumerator_t2274 *, List_1_t590 *, const MethodInfo*))Enumerator__ctor_m10721_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17059(__this, method) (( Object_t * (*) (Enumerator_t2274 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m10722_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::Dispose()
#define Enumerator_Dispose_m17060(__this, method) (( void (*) (Enumerator_t2274 *, const MethodInfo*))Enumerator_Dispose_m10723_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::VerifyState()
#define Enumerator_VerifyState_m17061(__this, method) (( void (*) (Enumerator_t2274 *, const MethodInfo*))Enumerator_VerifyState_m10724_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::MoveNext()
#define Enumerator_MoveNext_m17062(__this, method) (( bool (*) (Enumerator_t2274 *, const MethodInfo*))Enumerator_MoveNext_m10725_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Events.BaseInvokableCall>::get_Current()
#define Enumerator_get_Current_m17063(__this, method) (( BaseInvokableCall_t583 * (*) (Enumerator_t2274 *, const MethodInfo*))Enumerator_get_Current_m10726_gshared)(__this, method)
