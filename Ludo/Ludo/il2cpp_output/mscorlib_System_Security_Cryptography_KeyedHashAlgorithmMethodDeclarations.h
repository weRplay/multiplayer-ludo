﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Security.Cryptography.KeyedHashAlgorithm
struct KeyedHashAlgorithm_t790;
// System.Byte[]
struct ByteU5BU5D_t435;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Security.Cryptography.KeyedHashAlgorithm::.ctor()
extern "C" void KeyedHashAlgorithm__ctor_m4405 (KeyedHashAlgorithm_t790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::Finalize()
extern "C" void KeyedHashAlgorithm_Finalize_m8890 (KeyedHashAlgorithm_t790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Byte[] System.Security.Cryptography.KeyedHashAlgorithm::get_Key()
extern "C" ByteU5BU5D_t435* KeyedHashAlgorithm_get_Key_m8891 (KeyedHashAlgorithm_t790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::set_Key(System.Byte[])
extern "C" void KeyedHashAlgorithm_set_Key_m8892 (KeyedHashAlgorithm_t790 * __this, ByteU5BU5D_t435* ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::Dispose(System.Boolean)
extern "C" void KeyedHashAlgorithm_Dispose_m8893 (KeyedHashAlgorithm_t790 * __this, bool ___disposing, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Security.Cryptography.KeyedHashAlgorithm::ZeroizeKey()
extern "C" void KeyedHashAlgorithm_ZeroizeKey_m8894 (KeyedHashAlgorithm_t790 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
