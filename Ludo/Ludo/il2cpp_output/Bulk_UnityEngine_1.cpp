﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityEngine.MonoBehaviour
struct MonoBehaviour_t2;
// System.String
struct String_t;
// UnityEngine.Coroutine
struct Coroutine_t188;
struct Coroutine_t188_marshaled;
// System.Collections.IEnumerator
struct IEnumerator_t51;
// System.Object
struct Object_t;
// UnityEngine.Object
struct Object_t54;
struct Object_t54_marshaled;
// UnityEngine.Component
struct Component_t64;
// UnityEngine.Transform
struct Transform_t3;
// UnityEngine.GameObject
struct GameObject_t5;
// System.Type
struct Type_t;
// System.Collections.Generic.List`1<UnityEngine.Component>
struct List_1_t292;
// System.Array
struct Array_t;
// UnityEngine.Transform/Enumerator
struct Enumerator_t449;
// UnityEngine.YieldInstruction
struct YieldInstruction_t365;
struct YieldInstruction_t365_marshaled;
// UnityEngine.PlayerPrefsException
struct PlayerPrefsException_t452;
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t309;
// UnityEngine.Collider
struct Collider_t310;
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t307;
// UnityEngine.Collider2D
struct Collider2D_t308;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t39;
// UnityEngine.AudioSettings/AudioConfigurationChangeHandler
struct AudioConfigurationChangeHandler_t457;
// System.IAsyncResult
struct IAsyncResult_t180;
// System.AsyncCallback
struct AsyncCallback_t181;
// UnityEngine.AudioSettings
struct AudioSettings_t458;
// UnityEngine.AudioClip/PCMReaderCallback
struct PCMReaderCallback_t459;
// System.Single[]
struct SingleU5BU5D_t460;
// UnityEngine.AudioClip/PCMSetPositionCallback
struct PCMSetPositionCallback_t461;
// UnityEngine.AudioClip
struct AudioClip_t462;
// UnityEngine.AnimationEvent
struct AnimationEvent_t465;
// UnityEngine.AnimationState
struct AnimationState_t466;
// UnityEngine.AnimationCurve
struct AnimationCurve_t470;
struct AnimationCurve_t470_marshaled;
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t610;
// UnityEngine.Animator
struct Animator_t289;
// UnityEngine.RuntimeAnimatorController
struct RuntimeAnimatorController_t340;
// UnityEngine.Font/FontTextureRebuildCallback
struct FontTextureRebuildCallback_t479;
// System.Action`1<UnityEngine.Font>
struct Action_1_t313;
// UnityEngine.Font
struct Font_t142;
// UnityEngine.Material
struct Material_t146;
// UnityEngine.TextGenerator
struct TextGenerator_t186;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t185;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t611;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t612;
// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t480;
// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t481;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t187;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t341;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t334;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t332;
// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t311;
// UnityEngine.Canvas
struct Canvas_t148;
// UnityEngine.Camera
struct Camera_t122;
// UnityEngine.CanvasGroup
struct CanvasGroup_t317;
// UnityEngine.CanvasRenderer
struct CanvasRenderer_t147;
// UnityEngine.Texture
struct Texture_t196;
// UnityEngine.RectTransform
struct RectTransform_t18;
// UnityEngine.Networking.Match.Request
struct Request_t483;
// UnityEngine.Networking.Match.ResponseBase
struct ResponseBase_t484;
// System.Collections.Generic.IDictionary`2<System.String,System.Object>
struct IDictionary_2_t613;
// UnityEngine.Networking.Match.Response
struct Response_t485;
// UnityEngine.Networking.Match.BasicResponse
struct BasicResponse_t486;
// UnityEngine.Networking.Match.CreateMatchRequest
struct CreateMatchRequest_t487;
// System.Collections.Generic.Dictionary`2<System.String,System.Int64>
struct Dictionary_2_t488;
// UnityEngine.Networking.Match.CreateMatchResponse
struct CreateMatchResponse_t489;
// UnityEngine.Networking.Match.JoinMatchRequest
struct JoinMatchRequest_t490;
// UnityEngine.Networking.Match.JoinMatchResponse
struct JoinMatchResponse_t491;
// UnityEngine.Networking.Match.DestroyMatchRequest
struct DestroyMatchRequest_t492;
// UnityEngine.Networking.Match.DropConnectionRequest
struct DropConnectionRequest_t493;
// UnityEngine.Networking.Match.ListMatchRequest
struct ListMatchRequest_t494;
// UnityEngine.Networking.Match.MatchDirectConnectInfo
struct MatchDirectConnectInfo_t495;
// UnityEngine.Networking.Match.MatchDesc
struct MatchDesc_t496;
// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>
struct List_1_t497;
// System.Collections.Generic.List`1<System.Object>
struct List_1_t344;
// UnityEngine.Networking.Match.ListMatchResponse
struct ListMatchResponse_t498;
// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>
struct List_1_t499;
// UnityEngine.Networking.Types.NetworkAccessToken
struct NetworkAccessToken_t504;
// UnityEngine.Networking.Match.NetworkMatch
struct NetworkMatch_t508;
// System.Uri
struct Uri_t509;
// UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.CreateMatchResponse>
struct ResponseDelegate_1_t614;
// UnityEngine.WWW
struct WWW_t9;
// UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<System.Object>
struct ResponseDelegate_1_t692;
// UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.JoinMatchResponse>
struct ResponseDelegate_1_t615;
// UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.BasicResponse>
struct ResponseDelegate_1_t616;
// UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.ListMatchResponse>
struct ResponseDelegate_1_t617;
// SimpleJson.JsonArray
struct JsonArray_t510;
// SimpleJson.JsonObject
struct JsonObject_t511;
// System.Collections.Generic.ICollection`1<System.String>
struct ICollection_1_t620;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t621;
// System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[]
struct KeyValuePair_2U5BU5D_t619;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>
struct IEnumerator_1_t622;
// SimpleJson.IJsonSerializerStrategy
struct IJsonSerializerStrategy_t515;
// System.Char[]
struct CharU5BU5D_t58;
// System.Text.StringBuilder
struct StringBuilder_t296;
// System.Collections.IEnumerable
struct IEnumerable_t623;
// SimpleJson.PocoJsonSerializerStrategy
struct PocoJsonSerializerStrategy_t514;
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate
struct ConstructorDelegate_t522;
// System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>
struct IDictionary_2_t625;
// System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>
struct IDictionary_2_t626;
// System.Enum
struct Enum_t624;
// SimpleJson.Reflection.ReflectionUtils/GetDelegate
struct GetDelegate_t520;
// SimpleJson.Reflection.ReflectionUtils/SetDelegate
struct SetDelegate_t521;
// System.Object[]
struct ObjectU5BU5D_t60;
// SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1
struct U3CGetConstructorByReflectionU3Ec__AnonStorey1_t523;
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2
struct U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t525;
// SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3
struct U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t526;
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4
struct U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t527;
// SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5
struct U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t528;
// System.Collections.Generic.IEnumerable`1<System.Reflection.ConstructorInfo>
struct IEnumerable_1_t627;
// System.Reflection.ConstructorInfo
struct ConstructorInfo_t524;
// System.Type[]
struct TypeU5BU5D_t516;
// System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>
struct IEnumerable_1_t628;
// System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo>
struct IEnumerable_1_t629;
// System.Reflection.MethodInfo
struct MethodInfo_t;
// System.Reflection.PropertyInfo
struct PropertyInfo_t;
// System.Reflection.FieldInfo
struct FieldInfo_t;
// UnityEngine.WrapperlessIcall
struct WrapperlessIcall_t530;
// UnityEngine.IL2CPPStructAlignmentAttribute
struct IL2CPPStructAlignmentAttribute_t531;
// UnityEngine.DisallowMultipleComponent
struct DisallowMultipleComponent_t536;
// UnityEngine.RequireComponent
struct RequireComponent_t537;
// UnityEngine.AddComponentMenu
struct AddComponentMenu_t538;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Single.h"
#include "UnityEngine_UnityEngine_Coroutine.h"
#include "mscorlib_System_Object.h"
#include "UnityEngine_UnityEngine_TouchPhase.h"
#include "UnityEngine_UnityEngine_TouchPhaseMethodDeclarations.h"
#include "UnityEngine_UnityEngine_IMECompositionMode.h"
#include "UnityEngine_UnityEngine_IMECompositionModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch.h"
#include "UnityEngine_UnityEngine_TouchMethodDeclarations.h"
#include "mscorlib_System_Int32.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_Input.h"
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
#include "mscorlib_System_Boolean.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_HideFlags.h"
#include "UnityEngine_UnityEngine_HideFlagsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "mscorlib_System_IntPtrMethodDeclarations.h"
#include "mscorlib_System_IntPtr.h"
#include "mscorlib_System_ArgumentExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentException.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Transform.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "mscorlib_System_Type.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_11.h"
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
#include "UnityEngine_UnityEngine_Transform_Enumerator.h"
#include "UnityEngine_UnityEngine_Transform_EnumeratorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform.h"
#include "UnityEngine_UnityEngine_Matrix4x4.h"
#include "UnityEngine_UnityEngine_Time.h"
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Random.h"
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
#include "UnityEngine_UnityEngine_YieldInstruction.h"
#include "UnityEngine_UnityEngine_YieldInstructionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefsException.h"
#include "UnityEngine_UnityEngine_PlayerPrefsExceptionMethodDeclarations.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefs.h"
#include "UnityEngine_UnityEngine_PlayerPrefsMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Particle.h"
#include "UnityEngine_UnityEngine_ParticleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_Physics.h"
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHit.h"
#include "UnityEngine_UnityEngine_Ray.h"
#include "UnityEngine_UnityEngine_RayMethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Collider.h"
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHitMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics2D.h"
#include "UnityEngine_UnityEngine_Physics2DMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_23MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_23.h"
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
#include "UnityEngine_UnityEngine_RaycastHit2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collider2D.h"
#include "UnityEngine_UnityEngine_Rigidbody2D.h"
#include "UnityEngine_UnityEngine_Collider2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_BoxCollider2D.h"
#include "UnityEngine_UnityEngine_BoxCollider2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigurationChan.h"
#include "UnityEngine_UnityEngine_AudioSettings_AudioConfigurationChanMethodDeclarations.h"
#include "mscorlib_System_AsyncCallback.h"
#include "UnityEngine_UnityEngine_AudioSettings.h"
#include "UnityEngine_UnityEngine_AudioSettingsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallback.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMReaderCallbackMethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallback.h"
#include "UnityEngine_UnityEngine_AudioClip_PCMSetPositionCallbackMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip.h"
#include "UnityEngine_UnityEngine_AudioClipMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WebCamDevice.h"
#include "UnityEngine_UnityEngine_WebCamDeviceMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationEventSource.h"
#include "UnityEngine_UnityEngine_AnimationEventSourceMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationEvent.h"
#include "UnityEngine_UnityEngine_AnimationEventMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationState.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
#include "mscorlib_System_SingleMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Keyframe.h"
#include "UnityEngine_UnityEngine_KeyframeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationCurve.h"
#include "UnityEngine_UnityEngine_AnimationCurveMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimationStateMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfo.h"
#include "UnityEngine_UnityEngine_AnimatorTransitionInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Animator.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorController.h"
#include "UnityEngine_UnityEngine_SkeletonBone.h"
#include "UnityEngine_UnityEngine_SkeletonBoneMethodDeclarations.h"
#include "UnityEngine_UnityEngine_HumanLimit.h"
#include "UnityEngine_UnityEngine_HumanLimitMethodDeclarations.h"
#include "UnityEngine_UnityEngine_HumanBone.h"
#include "UnityEngine_UnityEngine_HumanBoneMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RuntimeAnimatorControllerMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"
#include "UnityEngine_UnityEngine_TextAnchorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
#include "UnityEngine_UnityEngine_HorizontalWrapModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
#include "UnityEngine_UnityEngine_VerticalWrapModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CharacterInfo.h"
#include "UnityEngine_UnityEngine_CharacterInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_FontStyle.h"
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallback.h"
#include "UnityEngine_UnityEngine_Font_FontTextureRebuildCallbackMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Font.h"
#include "UnityEngine_UnityEngine_FontMethodDeclarations.h"
#include "mscorlib_System_Action_1_gen.h"
#include "mscorlib_System_DelegateMethodDeclarations.h"
#include "mscorlib_System_Delegate.h"
#include "UnityEngine_UnityEngine_Material.h"
#include "mscorlib_System_Char.h"
#include "mscorlib_System_Action_1_genMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"
#include "UnityEngine_UnityEngine_UICharInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"
#include "UnityEngine_UnityEngine_UILineInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextGenerator.h"
#include "UnityEngine_UnityEngine_TextGeneratorMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_9MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_24MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_25MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_9.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_24.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_25.h"
#include "UnityEngine_UnityEngine_UIVertex.h"
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
#include "UnityEngine_UnityEngine_TextGenerationSettingsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RenderMode.h"
#include "UnityEngine_UnityEngine_RenderModeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvases.h"
#include "UnityEngine_UnityEngine_Canvas_WillRenderCanvasesMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Canvas.h"
#include "UnityEngine_UnityEngine_CanvasMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera.h"
#include "UnityEngine_UnityEngine_CanvasGroup.h"
#include "UnityEngine_UnityEngine_CanvasGroupMethodDeclarations.h"
#include "UnityEngine_UnityEngine_UIVertexMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector4MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color32.h"
#include "mscorlib_System_Byte.h"
#include "UnityEngine_UnityEngine_Vector4.h"
#include "UnityEngine_UnityEngine_CanvasRenderer.h"
#include "UnityEngine_UnityEngine_CanvasRendererMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Texture.h"
#include "UnityEngine_UnityEngine_UnityStringMethodDeclarations.h"
#include "mscorlib_System_UInt16.h"
#include "UnityEngine_UnityEngine_RectTransformUtility.h"
#include "UnityEngine_UnityEngine_RectTransformUtilityMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlaneMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Plane.h"
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Match_Request.h"
#include "UnityEngine_UnityEngine_Networking_Match_RequestMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Types_SourceID.h"
#include "UnityEngine_UnityEngine_Networking_Types_AppID.h"
#include "mscorlib_System_EnumMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Match_ResponseBase.h"
#include "UnityEngine_UnityEngine_Networking_Match_ResponseBaseMethodDeclarations.h"
#include "mscorlib_System_FormatExceptionMethodDeclarations.h"
#include "mscorlib_System_FormatException.h"
#include "mscorlib_System_ConvertMethodDeclarations.h"
#include "mscorlib_System_UInt64.h"
#include "UnityEngine_UnityEngine_Networking_Match_Response.h"
#include "UnityEngine_UnityEngine_Networking_Match_ResponseMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Match_BasicResponse.h"
#include "UnityEngine_UnityEngine_Networking_Match_BasicResponseMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchRequest.h"
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchRequestMethodDeclarations.h"
#include "mscorlib_System_UInt32.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_7.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_7MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchResponse.h"
#include "UnityEngine_UnityEngine_Networking_Match_CreateMatchResponseMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"
#include "UnityEngine_UnityEngine_Networking_Types_NodeID.h"
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchRequest.h"
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchRequestMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchResponse.h"
#include "UnityEngine_UnityEngine_Networking_Match_JoinMatchResponseMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Match_DestroyMatchRequest.h"
#include "UnityEngine_UnityEngine_Networking_Match_DestroyMatchRequestMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Match_DropConnectionReque.h"
#include "UnityEngine_UnityEngine_Networking_Match_DropConnectionRequeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Match_ListMatchRequest.h"
#include "UnityEngine_UnityEngine_Networking_Match_ListMatchRequestMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Match_MatchDirectConnectI.h"
#include "UnityEngine_UnityEngine_Networking_Match_MatchDirectConnectIMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Match_MatchDesc.h"
#include "UnityEngine_UnityEngine_Networking_Match_MatchDescMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_26.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_26MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Match_ListMatchResponse.h"
#include "UnityEngine_UnityEngine_Networking_Match_ListMatchResponseMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_27.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_27MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Types_AppIDMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Types_SourceIDMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Types_NetworkIDMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Types_NodeIDMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Types_NetworkAccessToken.h"
#include "UnityEngine_UnityEngine_Networking_Types_NetworkAccessTokenMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Utility.h"
#include "UnityEngine_UnityEngine_Networking_UtilityMethodDeclarations.h"
#include "mscorlib_System_EnvironmentMethodDeclarations.h"
#include "mscorlib_System_RandomMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_8MethodDeclarations.h"
#include "mscorlib_System_Random.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_8.h"
#include "UnityEngine_UnityEngine_SystemInfoMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMatch.h"
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMatchMethodDeclarations.h"
#include "System_System_UriMethodDeclarations.h"
#include "mscorlib_System_UInt64MethodDeclarations.h"
#include "System_System_Uri.h"
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMatch_Respon.h"
#include "UnityEngine_UnityEngine_WWWFormMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
#include "mscorlib_System_UInt32MethodDeclarations.h"
#include "mscorlib_System_BooleanMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWForm.h"
#include "UnityEngine_UnityEngine_WWW.h"
#include "mscorlib_System_Enum.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_6.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_6MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMatch_Respon_0.h"
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMatch_Respon_1.h"
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMatch_Respon_2.h"
#include "UnityEngine_SimpleJson_JsonArray.h"
#include "UnityEngine_SimpleJson_JsonArrayMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_18MethodDeclarations.h"
#include "UnityEngine_SimpleJson_SimpleJsonMethodDeclarations.h"
#include "UnityEngine_SimpleJson_JsonObject.h"
#include "UnityEngine_SimpleJson_JsonObjectMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_9MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_9.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__1.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_3.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException.h"
#include "UnityEngine_SimpleJson_SimpleJson.h"
#include "mscorlib_System_Text_StringBuilderMethodDeclarations.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_18.h"
#include "mscorlib_System_Globalization_CultureInfoMethodDeclarations.h"
#include "mscorlib_System_Globalization_CultureInfo.h"
#include "mscorlib_System_Globalization_NumberStyles.h"
#include "mscorlib_System_ArgumentOutOfRangeExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentOutOfRangeException.h"
#include "mscorlib_System_DoubleMethodDeclarations.h"
#include "mscorlib_System_Int64MethodDeclarations.h"
#include "mscorlib_System_Double.h"
#include "mscorlib_System_Int64.h"
#include "mscorlib_System_StringComparison.h"
#include "mscorlib_System_Int32MethodDeclarations.h"
#include "mscorlib_System_DecimalMethodDeclarations.h"
#include "mscorlib_System_Decimal.h"
#include "mscorlib_System_SByte.h"
#include "mscorlib_System_Int16.h"
#include "UnityEngine_SimpleJson_PocoJsonSerializerStrategy.h"
#include "UnityEngine_SimpleJson_PocoJsonSerializerStrategyMethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafeMethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe_0MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe_1MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe_2MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe_3MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe_4MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_Constructo.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe_0.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe_1.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe_2.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe_3.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ThreadSafe_4.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtilsMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_10MethodDeclarations.h"
#include "mscorlib_System_Reflection_PropertyInfo.h"
#include "mscorlib_System_Reflection_MethodInfo.h"
#include "mscorlib_System_Reflection_FieldInfo.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_10.h"
#include "mscorlib_System_Reflection_PropertyInfoMethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBaseMethodDeclarations.h"
#include "mscorlib_System_Reflection_MethodBase.h"
#include "mscorlib_System_Reflection_MemberInfoMethodDeclarations.h"
#include "mscorlib_System_Reflection_MemberInfo.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_GetDelegat.h"
#include "mscorlib_System_Reflection_FieldInfoMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_11MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_11.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_SetDelegat.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_2.h"
#include "mscorlib_System_DateTimeMethodDeclarations.h"
#include "mscorlib_System_DateTimeOffsetMethodDeclarations.h"
#include "mscorlib_System_GuidMethodDeclarations.h"
#include "mscorlib_System_DateTime.h"
#include "mscorlib_System_DateTimeOffset.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_3MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_GetDelegatMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_3.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_SetDelegatMethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_ConstructoMethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetCons.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetConsMethodDeclarations.h"
#include "mscorlib_System_Reflection_ConstructorInfoMethodDeclarations.h"
#include "mscorlib_System_Reflection_ConstructorInfo.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetM.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetMMethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetM_0.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetGetM_0MethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetM.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetMMethodDeclarations.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetM_0.h"
#include "UnityEngine_SimpleJson_Reflection_ReflectionUtils_U3CGetSetM_0MethodDeclarations.h"
#include "mscorlib_System_Reflection_ParameterInfo.h"
#include "mscorlib_System_Reflection_ParameterInfoMethodDeclarations.h"
#include "mscorlib_System_Reflection_BindingFlags.h"
#include "UnityEngine_UnityEngine_WrapperlessIcall.h"
#include "UnityEngine_UnityEngine_WrapperlessIcallMethodDeclarations.h"
#include "mscorlib_System_AttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttribute.h"
#include "UnityEngine_UnityEngine_IL2CPPStructAlignmentAttributeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngine.h"
#include "UnityEngine_UnityEngine_AttributeHelperEngineMethodDeclarations.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponent.h"
#include "UnityEngine_UnityEngine_ExecuteInEditMode.h"
#include "UnityEngine_UnityEngine_RequireComponent.h"
#include "System_System_Collections_Generic_Stack_1_genMethodDeclarations.h"
#include "System_System_Collections_Generic_Stack_1_gen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_28MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_28.h"
#include "UnityEngine_UnityEngine_DisallowMultipleComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RequireComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AddComponentMenu.h"
#include "UnityEngine_UnityEngine_AddComponentMenuMethodDeclarations.h"

// System.Collections.Generic.List`1<!!0> UnityEngine.Networking.Match.ResponseBase::ParseJSONList<System.Object>(System.String,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern "C" List_1_t344 * ResponseBase_ParseJSONList_TisObject_t_m3365_gshared (ResponseBase_t484 * __this, String_t* p0, Object_t * p1, Object_t* p2, const MethodInfo* method);
#define ResponseBase_ParseJSONList_TisObject_t_m3365(__this, p0, p1, p2, method) (( List_1_t344 * (*) (ResponseBase_t484 *, String_t*, Object_t *, Object_t*, const MethodInfo*))ResponseBase_ParseJSONList_TisObject_t_m3365_gshared)(__this, p0, p1, p2, method)
// System.Collections.Generic.List`1<!!0> UnityEngine.Networking.Match.ResponseBase::ParseJSONList<UnityEngine.Networking.Match.MatchDirectConnectInfo>(System.String,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
#define ResponseBase_ParseJSONList_TisMatchDirectConnectInfo_t495_m3302(__this, p0, p1, p2, method) (( List_1_t497 * (*) (ResponseBase_t484 *, String_t*, Object_t *, Object_t*, const MethodInfo*))ResponseBase_ParseJSONList_TisObject_t_m3365_gshared)(__this, p0, p1, p2, method)
// System.Collections.Generic.List`1<!!0> UnityEngine.Networking.Match.ResponseBase::ParseJSONList<UnityEngine.Networking.Match.MatchDesc>(System.String,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
#define ResponseBase_ParseJSONList_TisMatchDesc_t496_m3303(__this, p0, p1, p2, method) (( List_1_t499 * (*) (ResponseBase_t484 *, String_t*, Object_t *, Object_t*, const MethodInfo*))ResponseBase_ParseJSONList_TisObject_t_m3365_gshared)(__this, p0, p1, p2, method)
// System.Collections.IEnumerator UnityEngine.Networking.Match.NetworkMatch::ProcessMatchResponse<System.Object>(UnityEngine.WWW,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<!!0>)
extern "C" Object_t * NetworkMatch_ProcessMatchResponse_TisObject_t_m3366_gshared (NetworkMatch_t508 * __this, WWW_t9 * p0, ResponseDelegate_1_t692 * p1, const MethodInfo* method);
#define NetworkMatch_ProcessMatchResponse_TisObject_t_m3366(__this, p0, p1, method) (( Object_t * (*) (NetworkMatch_t508 *, WWW_t9 *, ResponseDelegate_1_t692 *, const MethodInfo*))NetworkMatch_ProcessMatchResponse_TisObject_t_m3366_gshared)(__this, p0, p1, method)
// System.Collections.IEnumerator UnityEngine.Networking.Match.NetworkMatch::ProcessMatchResponse<UnityEngine.Networking.Match.CreateMatchResponse>(UnityEngine.WWW,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<!!0>)
#define NetworkMatch_ProcessMatchResponse_TisCreateMatchResponse_t489_m3313(__this, p0, p1, method) (( Object_t * (*) (NetworkMatch_t508 *, WWW_t9 *, ResponseDelegate_1_t614 *, const MethodInfo*))NetworkMatch_ProcessMatchResponse_TisObject_t_m3366_gshared)(__this, p0, p1, method)
// System.Collections.IEnumerator UnityEngine.Networking.Match.NetworkMatch::ProcessMatchResponse<UnityEngine.Networking.Match.JoinMatchResponse>(UnityEngine.WWW,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<!!0>)
#define NetworkMatch_ProcessMatchResponse_TisJoinMatchResponse_t491_m3314(__this, p0, p1, method) (( Object_t * (*) (NetworkMatch_t508 *, WWW_t9 *, ResponseDelegate_1_t615 *, const MethodInfo*))NetworkMatch_ProcessMatchResponse_TisObject_t_m3366_gshared)(__this, p0, p1, method)
// System.Collections.IEnumerator UnityEngine.Networking.Match.NetworkMatch::ProcessMatchResponse<UnityEngine.Networking.Match.BasicResponse>(UnityEngine.WWW,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<!!0>)
#define NetworkMatch_ProcessMatchResponse_TisBasicResponse_t486_m3315(__this, p0, p1, method) (( Object_t * (*) (NetworkMatch_t508 *, WWW_t9 *, ResponseDelegate_1_t616 *, const MethodInfo*))NetworkMatch_ProcessMatchResponse_TisObject_t_m3366_gshared)(__this, p0, p1, method)
// System.Collections.IEnumerator UnityEngine.Networking.Match.NetworkMatch::ProcessMatchResponse<UnityEngine.Networking.Match.ListMatchResponse>(UnityEngine.WWW,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<!!0>)
#define NetworkMatch_ProcessMatchResponse_TisListMatchResponse_t498_m3316(__this, p0, p1, method) (( Object_t * (*) (NetworkMatch_t508 *, WWW_t9 *, ResponseDelegate_1_t617 *, const MethodInfo*))NetworkMatch_ProcessMatchResponse_TisObject_t_m3366_gshared)(__this, p0, p1, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityEngine.MonoBehaviour::.ctor()
extern "C" void MonoBehaviour__ctor_m191 (MonoBehaviour_t2 * __this, const MethodInfo* method)
{
	{
		Behaviour__ctor_m2523(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)
extern "C" void MonoBehaviour_InvokeRepeating_m242 (MonoBehaviour_t2 * __this, String_t* ___methodName, float ___time, float ___repeatRate, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_InvokeRepeating_m242_ftn) (MonoBehaviour_t2 *, String_t*, float, float);
	static MonoBehaviour_InvokeRepeating_m242_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_InvokeRepeating_m242_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::InvokeRepeating(System.String,System.Single,System.Single)");
	_il2cpp_icall_func(__this, ___methodName, ___time, ___repeatRate);
}
// System.Void UnityEngine.MonoBehaviour::CancelInvoke(System.String)
extern "C" void MonoBehaviour_CancelInvoke_m253 (MonoBehaviour_t2 * __this, String_t* ___methodName, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_CancelInvoke_m253_ftn) (MonoBehaviour_t2 *, String_t*);
	static MonoBehaviour_CancelInvoke_m253_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_CancelInvoke_m253_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::CancelInvoke(System.String)");
	_il2cpp_icall_func(__this, ___methodName);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.Collections.IEnumerator)
extern "C" Coroutine_t188 * MonoBehaviour_StartCoroutine_m207 (MonoBehaviour_t2 * __this, Object_t * ___routine, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___routine;
		Coroutine_t188 * L_1 = MonoBehaviour_StartCoroutine_Auto_m2580(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)
extern "C" Coroutine_t188 * MonoBehaviour_StartCoroutine_Auto_m2580 (MonoBehaviour_t2 * __this, Object_t * ___routine, const MethodInfo* method)
{
	typedef Coroutine_t188 * (*MonoBehaviour_StartCoroutine_Auto_m2580_ftn) (MonoBehaviour_t2 *, Object_t *);
	static MonoBehaviour_StartCoroutine_Auto_m2580_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_Auto_m2580_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine_Auto(System.Collections.IEnumerator)");
	return _il2cpp_icall_func(__this, ___routine);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)
extern "C" Coroutine_t188 * MonoBehaviour_StartCoroutine_m2581 (MonoBehaviour_t2 * __this, String_t* ___methodName, Object_t * ___value, const MethodInfo* method)
{
	typedef Coroutine_t188 * (*MonoBehaviour_StartCoroutine_m2581_ftn) (MonoBehaviour_t2 *, String_t*, Object_t *);
	static MonoBehaviour_StartCoroutine_m2581_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StartCoroutine_m2581_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StartCoroutine(System.String,System.Object)");
	return _il2cpp_icall_func(__this, ___methodName, ___value);
}
// UnityEngine.Coroutine UnityEngine.MonoBehaviour::StartCoroutine(System.String)
extern "C" Coroutine_t188 * MonoBehaviour_StartCoroutine_m223 (MonoBehaviour_t2 * __this, String_t* ___methodName, const MethodInfo* method)
{
	Object_t * V_0 = {0};
	{
		V_0 = NULL;
		String_t* L_0 = ___methodName;
		Object_t * L_1 = V_0;
		Coroutine_t188 * L_2 = MonoBehaviour_StartCoroutine_m2581(__this, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(System.Collections.IEnumerator)
extern "C" void MonoBehaviour_StopCoroutine_m2582 (MonoBehaviour_t2 * __this, Object_t * ___routine, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___routine;
		MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2583(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine(UnityEngine.Coroutine)
extern "C" void MonoBehaviour_StopCoroutine_m1840 (MonoBehaviour_t2 * __this, Coroutine_t188 * ___routine, const MethodInfo* method)
{
	{
		Coroutine_t188 * L_0 = ___routine;
		MonoBehaviour_StopCoroutine_Auto_m2584(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)
extern "C" void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2583 (MonoBehaviour_t2 * __this, Object_t * ___routine, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2583_ftn) (MonoBehaviour_t2 *, Object_t *);
	static MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2583_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2583_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutineViaEnumerator_Auto(System.Collections.IEnumerator)");
	_il2cpp_icall_func(__this, ___routine);
}
// System.Void UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)
extern "C" void MonoBehaviour_StopCoroutine_Auto_m2584 (MonoBehaviour_t2 * __this, Coroutine_t188 * ___routine, const MethodInfo* method)
{
	typedef void (*MonoBehaviour_StopCoroutine_Auto_m2584_ftn) (MonoBehaviour_t2 *, Coroutine_t188 *);
	static MonoBehaviour_StopCoroutine_Auto_m2584_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (MonoBehaviour_StopCoroutine_Auto_m2584_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.MonoBehaviour::StopCoroutine_Auto(UnityEngine.Coroutine)");
	_il2cpp_icall_func(__this, ___routine);
}
// System.Int32 UnityEngine.Touch::get_fingerId()
extern "C" int32_t Touch_get_fingerId_m1523 (Touch_t282 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_FingerId_0);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.Touch::get_position()
extern "C" Vector2_t59  Touch_get_position_m1525 (Touch_t282 * __this, const MethodInfo* method)
{
	{
		Vector2_t59  L_0 = (__this->___m_Position_1);
		return L_0;
	}
}
// UnityEngine.TouchPhase UnityEngine.Touch::get_phase()
extern "C" int32_t Touch_get_phase_m1524 (Touch_t282 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Phase_6);
		return L_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.Touch
extern "C" void Touch_t282_marshal(const Touch_t282& unmarshaled, Touch_t282_marshaled& marshaled)
{
	marshaled.___m_FingerId_0 = unmarshaled.___m_FingerId_0;
	marshaled.___m_Position_1 = unmarshaled.___m_Position_1;
	marshaled.___m_RawPosition_2 = unmarshaled.___m_RawPosition_2;
	marshaled.___m_PositionDelta_3 = unmarshaled.___m_PositionDelta_3;
	marshaled.___m_TimeDelta_4 = unmarshaled.___m_TimeDelta_4;
	marshaled.___m_TapCount_5 = unmarshaled.___m_TapCount_5;
	marshaled.___m_Phase_6 = unmarshaled.___m_Phase_6;
}
extern "C" void Touch_t282_marshal_back(const Touch_t282_marshaled& marshaled, Touch_t282& unmarshaled)
{
	unmarshaled.___m_FingerId_0 = marshaled.___m_FingerId_0;
	unmarshaled.___m_Position_1 = marshaled.___m_Position_1;
	unmarshaled.___m_RawPosition_2 = marshaled.___m_RawPosition_2;
	unmarshaled.___m_PositionDelta_3 = marshaled.___m_PositionDelta_3;
	unmarshaled.___m_TimeDelta_4 = marshaled.___m_TimeDelta_4;
	unmarshaled.___m_TapCount_5 = marshaled.___m_TapCount_5;
	unmarshaled.___m_Phase_6 = marshaled.___m_Phase_6;
}
// Conversion method for clean up from marshalling of: UnityEngine.Touch
extern "C" void Touch_t282_marshal_cleanup(Touch_t282_marshaled& marshaled)
{
}
// System.Void UnityEngine.Input::.cctor()
extern "C" void Input__cctor_m2585 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Single UnityEngine.Input::GetAxisRaw(System.String)
extern "C" float Input_GetAxisRaw_m1548 (Object_t * __this /* static, unused */, String_t* ___axisName, const MethodInfo* method)
{
	typedef float (*Input_GetAxisRaw_m1548_ftn) (String_t*);
	static Input_GetAxisRaw_m1548_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetAxisRaw_m1548_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetAxisRaw(System.String)");
	return _il2cpp_icall_func(___axisName);
}
// System.Boolean UnityEngine.Input::GetButtonDown(System.String)
extern "C" bool Input_GetButtonDown_m1547 (Object_t * __this /* static, unused */, String_t* ___buttonName, const MethodInfo* method)
{
	typedef bool (*Input_GetButtonDown_m1547_ftn) (String_t*);
	static Input_GetButtonDown_m1547_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetButtonDown_m1547_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetButtonDown(System.String)");
	return _il2cpp_icall_func(___buttonName);
}
// System.Boolean UnityEngine.Input::GetMouseButton(System.Int32)
extern "C" bool Input_GetMouseButton_m1568 (Object_t * __this /* static, unused */, int32_t ___button, const MethodInfo* method)
{
	typedef bool (*Input_GetMouseButton_m1568_ftn) (int32_t);
	static Input_GetMouseButton_m1568_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButton_m1568_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButton(System.Int32)");
	return _il2cpp_icall_func(___button);
}
// System.Boolean UnityEngine.Input::GetMouseButtonDown(System.Int32)
extern "C" bool Input_GetMouseButtonDown_m1527 (Object_t * __this /* static, unused */, int32_t ___button, const MethodInfo* method)
{
	typedef bool (*Input_GetMouseButtonDown_m1527_ftn) (int32_t);
	static Input_GetMouseButtonDown_m1527_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonDown_m1527_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonDown(System.Int32)");
	return _il2cpp_icall_func(___button);
}
// System.Boolean UnityEngine.Input::GetMouseButtonUp(System.Int32)
extern "C" bool Input_GetMouseButtonUp_m1528 (Object_t * __this /* static, unused */, int32_t ___button, const MethodInfo* method)
{
	typedef bool (*Input_GetMouseButtonUp_m1528_ftn) (int32_t);
	static Input_GetMouseButtonUp_m1528_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetMouseButtonUp_m1528_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetMouseButtonUp(System.Int32)");
	return _il2cpp_icall_func(___button);
}
// UnityEngine.Vector3 UnityEngine.Input::get_mousePosition()
extern TypeInfo* Input_t299_il2cpp_TypeInfo_var;
extern "C" Vector3_t35  Input_get_mousePosition_m1529 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t299_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t35  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t299_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_mousePosition_m2586(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector3_t35  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)
extern "C" void Input_INTERNAL_get_mousePosition_m2586 (Object_t * __this /* static, unused */, Vector3_t35 * ___value, const MethodInfo* method)
{
	typedef void (*Input_INTERNAL_get_mousePosition_m2586_ftn) (Vector3_t35 *);
	static Input_INTERNAL_get_mousePosition_m2586_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_mousePosition_m2586_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_mousePosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(___value);
}
// UnityEngine.Vector2 UnityEngine.Input::get_mouseScrollDelta()
extern TypeInfo* Input_t299_il2cpp_TypeInfo_var;
extern "C" Vector2_t59  Input_get_mouseScrollDelta_m1530 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t299_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t59  V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t299_il2cpp_TypeInfo_var);
		Input_INTERNAL_get_mouseScrollDelta_m2587(NULL /*static, unused*/, (&V_0), /*hidden argument*/NULL);
		Vector2_t59  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)
extern "C" void Input_INTERNAL_get_mouseScrollDelta_m2587 (Object_t * __this /* static, unused */, Vector2_t59 * ___value, const MethodInfo* method)
{
	typedef void (*Input_INTERNAL_get_mouseScrollDelta_m2587_ftn) (Vector2_t59 *);
	static Input_INTERNAL_get_mouseScrollDelta_m2587_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_get_mouseScrollDelta_m2587_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_get_mouseScrollDelta(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value);
}
// System.Boolean UnityEngine.Input::get_mousePresent()
extern "C" bool Input_get_mousePresent_m1546 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef bool (*Input_get_mousePresent_m1546_ftn) ();
	static Input_get_mousePresent_m1546_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_mousePresent_m1546_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_mousePresent()");
	return _il2cpp_icall_func();
}
// UnityEngine.Touch UnityEngine.Input::GetTouch(System.Int32)
extern "C" Touch_t282  Input_GetTouch_m1566 (Object_t * __this /* static, unused */, int32_t ___index, const MethodInfo* method)
{
	typedef Touch_t282  (*Input_GetTouch_m1566_ftn) (int32_t);
	static Input_GetTouch_m1566_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_GetTouch_m1566_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::GetTouch(System.Int32)");
	return _il2cpp_icall_func(___index);
}
// System.Int32 UnityEngine.Input::get_touchCount()
extern "C" int32_t Input_get_touchCount_m1567 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef int32_t (*Input_get_touchCount_m1567_ftn) ();
	static Input_get_touchCount_m1567_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_touchCount_m1567_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_touchCount()");
	return _il2cpp_icall_func();
}
// System.Boolean UnityEngine.Input::get_touchSupported()
extern "C" bool Input_get_touchSupported_m1565 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		return 1;
	}
}
// System.Void UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)
extern "C" void Input_set_imeCompositionMode_m1832 (Object_t * __this /* static, unused */, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Input_set_imeCompositionMode_m1832_ftn) (int32_t);
	static Input_set_imeCompositionMode_m1832_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_set_imeCompositionMode_m1832_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::set_imeCompositionMode(UnityEngine.IMECompositionMode)");
	_il2cpp_icall_func(___value);
}
// System.String UnityEngine.Input::get_compositionString()
extern "C" String_t* Input_get_compositionString_m1751 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef String_t* (*Input_get_compositionString_m1751_ftn) ();
	static Input_get_compositionString_m1751_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_get_compositionString_m1751_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::get_compositionString()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Input::set_compositionCursorPos(UnityEngine.Vector2)
extern TypeInfo* Input_t299_il2cpp_TypeInfo_var;
extern "C" void Input_set_compositionCursorPos_m1819 (Object_t * __this /* static, unused */, Vector2_t59  ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t299_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(111);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t299_il2cpp_TypeInfo_var);
		Input_INTERNAL_set_compositionCursorPos_m2588(NULL /*static, unused*/, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)
extern "C" void Input_INTERNAL_set_compositionCursorPos_m2588 (Object_t * __this /* static, unused */, Vector2_t59 * ___value, const MethodInfo* method)
{
	typedef void (*Input_INTERNAL_set_compositionCursorPos_m2588_ftn) (Vector2_t59 *);
	static Input_INTERNAL_set_compositionCursorPos_m2588_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Input_INTERNAL_set_compositionCursorPos_m2588_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Input::INTERNAL_set_compositionCursorPos(UnityEngine.Vector2&)");
	_il2cpp_icall_func(___value);
}
// System.Void UnityEngine.Object::.ctor()
extern "C" void Object__ctor_m2589 (Object_t54 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Object UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)
extern "C" Object_t54 * Object_Internal_CloneSingle_m2590 (Object_t * __this /* static, unused */, Object_t54 * ___data, const MethodInfo* method)
{
	typedef Object_t54 * (*Object_Internal_CloneSingle_m2590_ftn) (Object_t54 *);
	static Object_Internal_CloneSingle_m2590_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Internal_CloneSingle_m2590_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Internal_CloneSingle(UnityEngine.Object)");
	return _il2cpp_icall_func(___data);
}
// UnityEngine.Object UnityEngine.Object::Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern "C" Object_t54 * Object_Internal_InstantiateSingle_m2591 (Object_t * __this /* static, unused */, Object_t54 * ___data, Vector3_t35  ___pos, Quaternion_t53  ___rot, const MethodInfo* method)
{
	{
		Object_t54 * L_0 = ___data;
		Object_t54 * L_1 = Object_INTERNAL_CALL_Internal_InstantiateSingle_m2592(NULL /*static, unused*/, L_0, (&___pos), (&___rot), /*hidden argument*/NULL);
		return L_1;
	}
}
// UnityEngine.Object UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C" Object_t54 * Object_INTERNAL_CALL_Internal_InstantiateSingle_m2592 (Object_t * __this /* static, unused */, Object_t54 * ___data, Vector3_t35 * ___pos, Quaternion_t53 * ___rot, const MethodInfo* method)
{
	typedef Object_t54 * (*Object_INTERNAL_CALL_Internal_InstantiateSingle_m2592_ftn) (Object_t54 *, Vector3_t35 *, Quaternion_t53 *);
	static Object_INTERNAL_CALL_Internal_InstantiateSingle_m2592_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_INTERNAL_CALL_Internal_InstantiateSingle_m2592_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::INTERNAL_CALL_Internal_InstantiateSingle(UnityEngine.Object,UnityEngine.Vector3&,UnityEngine.Quaternion&)");
	return _il2cpp_icall_func(___data, ___pos, ___rot);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)
extern "C" void Object_Destroy_m2593 (Object_t * __this /* static, unused */, Object_t54 * ___obj, float ___t, const MethodInfo* method)
{
	typedef void (*Object_Destroy_m2593_ftn) (Object_t54 *, float);
	static Object_Destroy_m2593_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_Destroy_m2593_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::Destroy(UnityEngine.Object,System.Single)");
	_il2cpp_icall_func(___obj, ___t);
}
// System.Void UnityEngine.Object::Destroy(UnityEngine.Object)
extern "C" void Object_Destroy_m227 (Object_t * __this /* static, unused */, Object_t54 * ___obj, const MethodInfo* method)
{
	float V_0 = 0.0f;
	{
		V_0 = (0.0f);
		Object_t54 * L_0 = ___obj;
		float L_1 = V_0;
		Object_Destroy_m2593(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)
extern "C" void Object_DestroyImmediate_m2594 (Object_t * __this /* static, unused */, Object_t54 * ___obj, bool ___allowDestroyingAssets, const MethodInfo* method)
{
	typedef void (*Object_DestroyImmediate_m2594_ftn) (Object_t54 *, bool);
	static Object_DestroyImmediate_m2594_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_DestroyImmediate_m2594_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::DestroyImmediate(UnityEngine.Object,System.Boolean)");
	_il2cpp_icall_func(___obj, ___allowDestroyingAssets);
}
// System.Void UnityEngine.Object::DestroyImmediate(UnityEngine.Object)
extern "C" void Object_DestroyImmediate_m1834 (Object_t * __this /* static, unused */, Object_t54 * ___obj, const MethodInfo* method)
{
	bool V_0 = false;
	{
		V_0 = 0;
		Object_t54 * L_0 = ___obj;
		bool L_1 = V_0;
		Object_DestroyImmediate_m2594(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Object::get_name()
extern "C" String_t* Object_get_name_m271 (Object_t54 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_get_name_m271_ftn) (Object_t54 *);
	static Object_get_name_m271_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_get_name_m271_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::get_name()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Object::set_name(System.String)
extern "C" void Object_set_name_m234 (Object_t54 * __this, String_t* ___value, const MethodInfo* method)
{
	typedef void (*Object_set_name_m234_ftn) (Object_t54 *, String_t*);
	static Object_set_name_m234_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_name_m234_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_name(System.String)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)
extern "C" void Object_set_hideFlags_m1795 (Object_t54 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*Object_set_hideFlags_m1795_ftn) (Object_t54 *, int32_t);
	static Object_set_hideFlags_m1795_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_set_hideFlags_m1795_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::set_hideFlags(UnityEngine.HideFlags)");
	_il2cpp_icall_func(__this, ___value);
}
// System.String UnityEngine.Object::ToString()
extern "C" String_t* Object_ToString_m2595 (Object_t54 * __this, const MethodInfo* method)
{
	typedef String_t* (*Object_ToString_m2595_ftn) (Object_t54 *);
	static Object_ToString_m2595_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Object_ToString_m2595_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Object::ToString()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Object::Equals(System.Object)
extern TypeInfo* Object_t54_il2cpp_TypeInfo_var;
extern "C" bool Object_Equals_m2596 (Object_t54 * __this, Object_t * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_t54_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(128);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___o;
		bool L_1 = Object_CompareBaseObjects_m2598(NULL /*static, unused*/, __this, ((Object_t54 *)IsInstClass(L_0, Object_t54_il2cpp_TypeInfo_var)), /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Int32 UnityEngine.Object::GetHashCode()
extern "C" int32_t Object_GetHashCode_m2597 (Object_t54 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = Object_GetInstanceID_m2600(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean UnityEngine.Object::CompareBaseObjects(UnityEngine.Object,UnityEngine.Object)
extern "C" bool Object_CompareBaseObjects_m2598 (Object_t * __this /* static, unused */, Object_t54 * ___lhs, Object_t54 * ___rhs, const MethodInfo* method)
{
	bool V_0 = false;
	bool V_1 = false;
	{
		Object_t54 * L_0 = ___lhs;
		V_0 = ((((Object_t*)(Object_t54 *)L_0) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
		Object_t54 * L_1 = ___rhs;
		V_1 = ((((Object_t*)(Object_t54 *)L_1) == ((Object_t*)(Object_t *)NULL))? 1 : 0);
		bool L_2 = V_1;
		if (!L_2)
		{
			goto IL_0018;
		}
	}
	{
		bool L_3 = V_0;
		if (!L_3)
		{
			goto IL_0018;
		}
	}
	{
		return 1;
	}

IL_0018:
	{
		bool L_4 = V_1;
		if (!L_4)
		{
			goto IL_0028;
		}
	}
	{
		Object_t54 * L_5 = ___lhs;
		bool L_6 = Object_IsNativeObjectAlive_m2599(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return ((((int32_t)L_6) == ((int32_t)0))? 1 : 0);
	}

IL_0028:
	{
		bool L_7 = V_0;
		if (!L_7)
		{
			goto IL_0038;
		}
	}
	{
		Object_t54 * L_8 = ___rhs;
		bool L_9 = Object_IsNativeObjectAlive_m2599(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		return ((((int32_t)L_9) == ((int32_t)0))? 1 : 0);
	}

IL_0038:
	{
		Object_t54 * L_10 = ___lhs;
		NullCheck(L_10);
		int32_t L_11 = (L_10->___m_InstanceID_0);
		Object_t54 * L_12 = ___rhs;
		NullCheck(L_12);
		int32_t L_13 = (L_12->___m_InstanceID_0);
		return ((((int32_t)L_11) == ((int32_t)L_13))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Object::IsNativeObjectAlive(UnityEngine.Object)
extern TypeInfo* IntPtr_t_il2cpp_TypeInfo_var;
extern "C" bool Object_IsNativeObjectAlive_m2599 (Object_t * __this /* static, unused */, Object_t54 * ___o, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IntPtr_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(286);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t54 * L_0 = ___o;
		NullCheck(L_0);
		IntPtr_t L_1 = Object_GetCachedPtr_m2601(L_0, /*hidden argument*/NULL);
		IntPtr_t L_2 = ((IntPtr_t_StaticFields*)IntPtr_t_il2cpp_TypeInfo_var->static_fields)->___Zero_1;
		bool L_3 = IntPtr_op_Inequality_m3288(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Int32 UnityEngine.Object::GetInstanceID()
extern "C" int32_t Object_GetInstanceID_m2600 (Object_t54 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_InstanceID_0);
		return L_0;
	}
}
// System.IntPtr UnityEngine.Object::GetCachedPtr()
extern "C" IntPtr_t Object_GetCachedPtr_m2601 (Object_t54 * __this, const MethodInfo* method)
{
	{
		IntPtr_t L_0 = (__this->___m_CachedPtr_1);
		return L_0;
	}
}
// UnityEngine.Object UnityEngine.Object::Instantiate(UnityEngine.Object,UnityEngine.Vector3,UnityEngine.Quaternion)
extern Il2CppCodeGenString* _stringLiteral265;
extern "C" Object_t54 * Object_Instantiate_m197 (Object_t * __this /* static, unused */, Object_t54 * ___original, Vector3_t35  ___position, Quaternion_t53  ___rotation, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral265 = il2cpp_codegen_string_literal_from_index(265);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t54 * L_0 = ___original;
		Object_CheckNullArgument_m2602(NULL /*static, unused*/, L_0, _stringLiteral265, /*hidden argument*/NULL);
		Object_t54 * L_1 = ___original;
		Vector3_t35  L_2 = ___position;
		Quaternion_t53  L_3 = ___rotation;
		Object_t54 * L_4 = Object_Internal_InstantiateSingle_m2591(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// System.Void UnityEngine.Object::CheckNullArgument(System.Object,System.String)
extern TypeInfo* ArgumentException_t343_il2cpp_TypeInfo_var;
extern "C" void Object_CheckNullArgument_m2602 (Object_t * __this /* static, unused */, Object_t * ___arg, String_t* ___message, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentException_t343_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(227);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___arg;
		if (L_0)
		{
			goto IL_000d;
		}
	}
	{
		String_t* L_1 = ___message;
		ArgumentException_t343 * L_2 = (ArgumentException_t343 *)il2cpp_codegen_object_new (ArgumentException_t343_il2cpp_TypeInfo_var);
		ArgumentException__ctor_m1919(L_2, L_1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_000d:
	{
		return;
	}
}
// System.Boolean UnityEngine.Object::op_Implicit(UnityEngine.Object)
extern "C" bool Object_op_Implicit_m1468 (Object_t * __this /* static, unused */, Object_t54 * ___exists, const MethodInfo* method)
{
	{
		Object_t54 * L_0 = ___exists;
		bool L_1 = Object_CompareBaseObjects_m2598(NULL /*static, unused*/, L_0, (Object_t54 *)NULL, /*hidden argument*/NULL);
		return ((((int32_t)L_1) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.Object::op_Equality(UnityEngine.Object,UnityEngine.Object)
extern "C" bool Object_op_Equality_m247 (Object_t * __this /* static, unused */, Object_t54 * ___x, Object_t54 * ___y, const MethodInfo* method)
{
	{
		Object_t54 * L_0 = ___x;
		Object_t54 * L_1 = ___y;
		bool L_2 = Object_CompareBaseObjects_m2598(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Object::op_Inequality(UnityEngine.Object,UnityEngine.Object)
extern "C" bool Object_op_Inequality_m1472 (Object_t * __this /* static, unused */, Object_t54 * ___x, Object_t54 * ___y, const MethodInfo* method)
{
	{
		Object_t54 * L_0 = ___x;
		Object_t54 * L_1 = ___y;
		bool L_2 = Object_CompareBaseObjects_m2598(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return ((((int32_t)L_2) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.Object
extern "C" void Object_t54_marshal(const Object_t54& unmarshaled, Object_t54_marshaled& marshaled)
{
	marshaled.___m_InstanceID_0 = unmarshaled.___m_InstanceID_0;
	marshaled.___m_CachedPtr_1 = reinterpret_cast<intptr_t>((unmarshaled.___m_CachedPtr_1).___m_value_0);
}
extern "C" void Object_t54_marshal_back(const Object_t54_marshaled& marshaled, Object_t54& unmarshaled)
{
	unmarshaled.___m_InstanceID_0 = marshaled.___m_InstanceID_0;
	(unmarshaled.___m_CachedPtr_1).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_CachedPtr_1);
}
// Conversion method for clean up from marshalling of: UnityEngine.Object
extern "C" void Object_t54_marshal_cleanup(Object_t54_marshaled& marshaled)
{
}
// System.Void UnityEngine.Component::.ctor()
extern "C" void Component__ctor_m2603 (Component_t64 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m2589(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Component::get_transform()
extern "C" Transform_t3 * Component_get_transform_m224 (Component_t64 * __this, const MethodInfo* method)
{
	typedef Transform_t3 * (*Component_get_transform_m224_ftn) (Component_t64 *);
	static Component_get_transform_m224_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_transform_m224_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_transform()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.GameObject UnityEngine.Component::get_gameObject()
extern "C" GameObject_t5 * Component_get_gameObject_m226 (Component_t64 * __this, const MethodInfo* method)
{
	typedef GameObject_t5 * (*Component_get_gameObject_m226_ftn) (Component_t64 *);
	static Component_get_gameObject_m226_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_get_gameObject_m226_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::get_gameObject()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Component UnityEngine.Component::GetComponent(System.Type)
extern "C" Component_t64 * Component_GetComponent_m1960 (Component_t64 * __this, Type_t * ___type, const MethodInfo* method)
{
	{
		GameObject_t5 * L_0 = Component_get_gameObject_m226(__this, /*hidden argument*/NULL);
		Type_t * L_1 = ___type;
		NullCheck(L_0);
		Component_t64 * L_2 = GameObject_GetComponent_m2606(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)
extern "C" void Component_GetComponentFastPath_m2604 (Component_t64 * __this, Type_t * ___type, IntPtr_t ___oneFurtherThanResultValue, const MethodInfo* method)
{
	typedef void (*Component_GetComponentFastPath_m2604_ftn) (Component_t64 *, Type_t *, IntPtr_t);
	static Component_GetComponentFastPath_m2604_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentFastPath_m2604_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type, ___oneFurtherThanResultValue);
}
// System.Void UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)
extern "C" void Component_GetComponentsForListInternal_m2605 (Component_t64 * __this, Type_t * ___searchType, Object_t * ___resultList, const MethodInfo* method)
{
	typedef void (*Component_GetComponentsForListInternal_m2605_ftn) (Component_t64 *, Type_t *, Object_t *);
	static Component_GetComponentsForListInternal_m2605_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Component_GetComponentsForListInternal_m2605_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Component::GetComponentsForListInternal(System.Type,System.Object)");
	_il2cpp_icall_func(__this, ___searchType, ___resultList);
}
// System.Void UnityEngine.Component::GetComponents(System.Type,System.Collections.Generic.List`1<UnityEngine.Component>)
extern "C" void Component_GetComponents_m1630 (Component_t64 * __this, Type_t * ___type, List_1_t292 * ___results, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type;
		List_1_t292 * L_1 = ___results;
		Component_GetComponentsForListInternal_m2605(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.GameObject::.ctor(System.String)
extern "C" void GameObject__ctor_m193 (GameObject_t5 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Object__ctor_m2589(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		GameObject_Internal_CreateGameObject_m2612(NULL /*static, unused*/, __this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Component UnityEngine.GameObject::GetComponent(System.Type)
extern "C" Component_t64 * GameObject_GetComponent_m2606 (GameObject_t5 * __this, Type_t * ___type, const MethodInfo* method)
{
	typedef Component_t64 * (*GameObject_GetComponent_m2606_ftn) (GameObject_t5 *, Type_t *);
	static GameObject_GetComponent_m2606_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponent_m2606_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponent(System.Type)");
	return _il2cpp_icall_func(__this, ___type);
}
// System.Void UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)
extern "C" void GameObject_GetComponentFastPath_m2607 (GameObject_t5 * __this, Type_t * ___type, IntPtr_t ___oneFurtherThanResultValue, const MethodInfo* method)
{
	typedef void (*GameObject_GetComponentFastPath_m2607_ftn) (GameObject_t5 *, Type_t *, IntPtr_t);
	static GameObject_GetComponentFastPath_m2607_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentFastPath_m2607_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentFastPath(System.Type,System.IntPtr)");
	_il2cpp_icall_func(__this, ___type, ___oneFurtherThanResultValue);
}
// System.Array UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)
extern "C" Array_t * GameObject_GetComponentsInternal_m2608 (GameObject_t5 * __this, Type_t * ___type, bool ___useSearchTypeAsArrayReturnType, bool ___recursive, bool ___includeInactive, bool ___reverse, Object_t * ___resultList, const MethodInfo* method)
{
	typedef Array_t * (*GameObject_GetComponentsInternal_m2608_ftn) (GameObject_t5 *, Type_t *, bool, bool, bool, bool, Object_t *);
	static GameObject_GetComponentsInternal_m2608_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_GetComponentsInternal_m2608_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::GetComponentsInternal(System.Type,System.Boolean,System.Boolean,System.Boolean,System.Boolean,System.Object)");
	return _il2cpp_icall_func(__this, ___type, ___useSearchTypeAsArrayReturnType, ___recursive, ___includeInactive, ___reverse, ___resultList);
}
// UnityEngine.Transform UnityEngine.GameObject::get_transform()
extern "C" Transform_t3 * GameObject_get_transform_m194 (GameObject_t5 * __this, const MethodInfo* method)
{
	typedef Transform_t3 * (*GameObject_get_transform_m194_ftn) (GameObject_t5 *);
	static GameObject_get_transform_m194_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_transform_m194_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_transform()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.GameObject::get_layer()
extern "C" int32_t GameObject_get_layer_m1797 (GameObject_t5 * __this, const MethodInfo* method)
{
	typedef int32_t (*GameObject_get_layer_m1797_ftn) (GameObject_t5 *);
	static GameObject_get_layer_m1797_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_layer_m1797_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_layer()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GameObject::set_layer(System.Int32)
extern "C" void GameObject_set_layer_m1798 (GameObject_t5 * __this, int32_t ___value, const MethodInfo* method)
{
	typedef void (*GameObject_set_layer_m1798_ftn) (GameObject_t5 *, int32_t);
	static GameObject_set_layer_m1798_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_set_layer_m1798_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::set_layer(System.Int32)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.GameObject::SetActive(System.Boolean)
extern "C" void GameObject_SetActive_m238 (GameObject_t5 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*GameObject_SetActive_m238_ftn) (GameObject_t5 *, bool);
	static GameObject_SetActive_m238_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SetActive_m238_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SetActive(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.GameObject::get_activeInHierarchy()
extern "C" bool GameObject_get_activeInHierarchy_m1514 (GameObject_t5 * __this, const MethodInfo* method)
{
	typedef bool (*GameObject_get_activeInHierarchy_m1514_ftn) (GameObject_t5 *);
	static GameObject_get_activeInHierarchy_m1514_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_get_activeInHierarchy_m1514_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::get_activeInHierarchy()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)
extern "C" void GameObject_SendMessage_m2609 (GameObject_t5 * __this, String_t* ___methodName, Object_t * ___value, int32_t ___options, const MethodInfo* method)
{
	typedef void (*GameObject_SendMessage_m2609_ftn) (GameObject_t5 *, String_t*, Object_t *, int32_t);
	static GameObject_SendMessage_m2609_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_SendMessage_m2609_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::SendMessage(System.String,System.Object,UnityEngine.SendMessageOptions)");
	_il2cpp_icall_func(__this, ___methodName, ___value, ___options);
}
// UnityEngine.Component UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)
extern "C" Component_t64 * GameObject_Internal_AddComponentWithType_m2610 (GameObject_t5 * __this, Type_t * ___componentType, const MethodInfo* method)
{
	typedef Component_t64 * (*GameObject_Internal_AddComponentWithType_m2610_ftn) (GameObject_t5 *, Type_t *);
	static GameObject_Internal_AddComponentWithType_m2610_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_AddComponentWithType_m2610_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_AddComponentWithType(System.Type)");
	return _il2cpp_icall_func(__this, ___componentType);
}
// UnityEngine.Component UnityEngine.GameObject::AddComponent(System.Type)
extern "C" Component_t64 * GameObject_AddComponent_m2611 (GameObject_t5 * __this, Type_t * ___componentType, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___componentType;
		Component_t64 * L_1 = GameObject_Internal_AddComponentWithType_m2610(__this, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)
extern "C" void GameObject_Internal_CreateGameObject_m2612 (Object_t * __this /* static, unused */, GameObject_t5 * ___mono, String_t* ___name, const MethodInfo* method)
{
	typedef void (*GameObject_Internal_CreateGameObject_m2612_ftn) (GameObject_t5 *, String_t*);
	static GameObject_Internal_CreateGameObject_m2612_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Internal_CreateGameObject_m2612_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Internal_CreateGameObject(UnityEngine.GameObject,System.String)");
	_il2cpp_icall_func(___mono, ___name);
}
// UnityEngine.GameObject UnityEngine.GameObject::Find(System.String)
extern "C" GameObject_t5 * GameObject_Find_m209 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef GameObject_t5 * (*GameObject_Find_m209_ftn) (String_t*);
	static GameObject_Find_m209_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (GameObject_Find_m209_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.GameObject::Find(System.String)");
	return _il2cpp_icall_func(___name);
}
// System.Void UnityEngine.Transform/Enumerator::.ctor(UnityEngine.Transform)
extern "C" void Enumerator__ctor_m2613 (Enumerator_t449 * __this, Transform_t3 * ___outer, const MethodInfo* method)
{
	{
		__this->___currentIndex_1 = (-1);
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		Transform_t3 * L_0 = ___outer;
		__this->___outer_0 = L_0;
		return;
	}
}
// System.Object UnityEngine.Transform/Enumerator::get_Current()
extern "C" Object_t * Enumerator_get_Current_m2614 (Enumerator_t449 * __this, const MethodInfo* method)
{
	{
		Transform_t3 * L_0 = (__this->___outer_0);
		int32_t L_1 = (__this->___currentIndex_1);
		NullCheck(L_0);
		Transform_t3 * L_2 = Transform_GetChild_m225(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.Transform/Enumerator::MoveNext()
extern "C" bool Enumerator_MoveNext_m2615 (Enumerator_t449 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	{
		Transform_t3 * L_0 = (__this->___outer_0);
		NullCheck(L_0);
		int32_t L_1 = Transform_get_childCount_m228(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = (__this->___currentIndex_1);
		int32_t L_3 = ((int32_t)((int32_t)L_2+(int32_t)1));
		V_1 = L_3;
		__this->___currentIndex_1 = L_3;
		int32_t L_4 = V_1;
		int32_t L_5 = V_0;
		return ((((int32_t)L_4) < ((int32_t)L_5))? 1 : 0);
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::get_position()
extern "C" Vector3_t35  Transform_get_position_m257 (Transform_t3 * __this, const MethodInfo* method)
{
	Vector3_t35  V_0 = {0};
	{
		Transform_INTERNAL_get_position_m2616(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t35  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_position(UnityEngine.Vector3)
extern "C" void Transform_set_position_m245 (Transform_t3 * __this, Vector3_t35  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_position_m2617(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_position_m2616 (Transform_t3 * __this, Vector3_t35 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_position_m2616_ftn) (Transform_t3 *, Vector3_t35 *);
	static Transform_INTERNAL_get_position_m2616_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_position_m2616_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_position_m2617 (Transform_t3 * __this, Vector3_t35 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_position_m2617_ftn) (Transform_t3 *, Vector3_t35 *);
	static Transform_INTERNAL_set_position_m2617_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_position_m2617_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_position(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localPosition()
extern "C" Vector3_t35  Transform_get_localPosition_m1803 (Transform_t3 * __this, const MethodInfo* method)
{
	Vector3_t35  V_0 = {0};
	{
		Transform_INTERNAL_get_localPosition_m2618(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t35  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localPosition(UnityEngine.Vector3)
extern "C" void Transform_set_localPosition_m1812 (Transform_t3 * __this, Vector3_t35  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localPosition_m2619(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_localPosition_m2618 (Transform_t3 * __this, Vector3_t35 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localPosition_m2618_ftn) (Transform_t3 *, Vector3_t35 *);
	static Transform_INTERNAL_get_localPosition_m2618_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localPosition_m2618_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_localPosition_m2619 (Transform_t3 * __this, Vector3_t35 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localPosition_m2619_ftn) (Transform_t3 *, Vector3_t35 *);
	static Transform_INTERNAL_set_localPosition_m2619_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localPosition_m2619_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localPosition(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_forward()
extern "C" Vector3_t35  Transform_get_forward_m1675 (Transform_t3 * __this, const MethodInfo* method)
{
	{
		Quaternion_t53  L_0 = Transform_get_rotation_m1671(__this, /*hidden argument*/NULL);
		Vector3_t35  L_1 = Vector3_get_forward_m1672(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t35  L_2 = Quaternion_op_Multiply_m1673(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.Quaternion UnityEngine.Transform::get_rotation()
extern "C" Quaternion_t53  Transform_get_rotation_m1671 (Transform_t3 * __this, const MethodInfo* method)
{
	Quaternion_t53  V_0 = {0};
	{
		Transform_INTERNAL_get_rotation_m2620(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t53  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_get_rotation_m2620 (Transform_t3 * __this, Quaternion_t53 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_rotation_m2620_ftn) (Transform_t3 *, Quaternion_t53 *);
	static Transform_INTERNAL_get_rotation_m2620_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_rotation_m2620_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_rotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Quaternion UnityEngine.Transform::get_localRotation()
extern "C" Quaternion_t53  Transform_get_localRotation_m1805 (Transform_t3 * __this, const MethodInfo* method)
{
	Quaternion_t53  V_0 = {0};
	{
		Transform_INTERNAL_get_localRotation_m2621(__this, (&V_0), /*hidden argument*/NULL);
		Quaternion_t53  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localRotation(UnityEngine.Quaternion)
extern "C" void Transform_set_localRotation_m1813 (Transform_t3 * __this, Quaternion_t53  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localRotation_m2622(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_get_localRotation_m2621 (Transform_t3 * __this, Quaternion_t53 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localRotation_m2621_ftn) (Transform_t3 *, Quaternion_t53 *);
	static Transform_INTERNAL_get_localRotation_m2621_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localRotation_m2621_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)
extern "C" void Transform_INTERNAL_set_localRotation_m2622 (Transform_t3 * __this, Quaternion_t53 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localRotation_m2622_ftn) (Transform_t3 *, Quaternion_t53 *);
	static Transform_INTERNAL_set_localRotation_m2622_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localRotation_m2622_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localRotation(UnityEngine.Quaternion&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::get_localScale()
extern "C" Vector3_t35  Transform_get_localScale_m1807 (Transform_t3 * __this, const MethodInfo* method)
{
	Vector3_t35  V_0 = {0};
	{
		Transform_INTERNAL_get_localScale_m2623(__this, (&V_0), /*hidden argument*/NULL);
		Vector3_t35  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_localScale(UnityEngine.Vector3)
extern "C" void Transform_set_localScale_m237 (Transform_t3 * __this, Vector3_t35  ___value, const MethodInfo* method)
{
	{
		Transform_INTERNAL_set_localScale_m2624(__this, (&___value), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_get_localScale_m2623 (Transform_t3 * __this, Vector3_t35 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_localScale_m2623_ftn) (Transform_t3 *, Vector3_t35 *);
	static Transform_INTERNAL_get_localScale_m2623_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_localScale_m2623_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)
extern "C" void Transform_INTERNAL_set_localScale_m2624 (Transform_t3 * __this, Vector3_t35 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_set_localScale_m2624_ftn) (Transform_t3 *, Vector3_t35 *);
	static Transform_INTERNAL_set_localScale_m2624_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_set_localScale_m2624_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_set_localScale(UnityEngine.Vector3&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Transform UnityEngine.Transform::get_parent()
extern "C" Transform_t3 * Transform_get_parent_m1506 (Transform_t3 * __this, const MethodInfo* method)
{
	{
		Transform_t3 * L_0 = Transform_get_parentInternal_m2625(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.Transform::set_parent(UnityEngine.Transform)
extern TypeInfo* RectTransform_t18_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral266;
extern "C" void Transform_set_parent_m235 (Transform_t3 * __this, Transform_t3 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(147);
		_stringLiteral266 = il2cpp_codegen_string_literal_from_index(266);
		s_Il2CppMethodIntialized = true;
	}
	{
		if (!((RectTransform_t18 *)IsInstSealed(__this, RectTransform_t18_il2cpp_TypeInfo_var)))
		{
			goto IL_0016;
		}
	}
	{
		Debug_LogWarning_m1900(NULL /*static, unused*/, _stringLiteral266, __this, /*hidden argument*/NULL);
	}

IL_0016:
	{
		Transform_t3 * L_0 = ___value;
		Transform_set_parentInternal_m2626(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Transform UnityEngine.Transform::get_parentInternal()
extern "C" Transform_t3 * Transform_get_parentInternal_m2625 (Transform_t3 * __this, const MethodInfo* method)
{
	typedef Transform_t3 * (*Transform_get_parentInternal_m2625_ftn) (Transform_t3 *);
	static Transform_get_parentInternal_m2625_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_parentInternal_m2625_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_parentInternal()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)
extern "C" void Transform_set_parentInternal_m2626 (Transform_t3 * __this, Transform_t3 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_set_parentInternal_m2626_ftn) (Transform_t3 *, Transform_t3 *);
	static Transform_set_parentInternal_m2626_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_set_parentInternal_m2626_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::set_parentInternal(UnityEngine.Transform)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform)
extern "C" void Transform_SetParent_m198 (Transform_t3 * __this, Transform_t3 * ___parent, const MethodInfo* method)
{
	{
		Transform_t3 * L_0 = ___parent;
		Transform_SetParent_m2627(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)
extern "C" void Transform_SetParent_m2627 (Transform_t3 * __this, Transform_t3 * ___parent, bool ___worldPositionStays, const MethodInfo* method)
{
	typedef void (*Transform_SetParent_m2627_ftn) (Transform_t3 *, Transform_t3 *, bool);
	static Transform_SetParent_m2627_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetParent_m2627_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetParent(UnityEngine.Transform,System.Boolean)");
	_il2cpp_icall_func(__this, ___parent, ___worldPositionStays);
}
// UnityEngine.Matrix4x4 UnityEngine.Transform::get_worldToLocalMatrix()
extern "C" Matrix4x4_t339  Transform_get_worldToLocalMatrix_m1867 (Transform_t3 * __this, const MethodInfo* method)
{
	Matrix4x4_t339  V_0 = {0};
	{
		Transform_INTERNAL_get_worldToLocalMatrix_m2628(__this, (&V_0), /*hidden argument*/NULL);
		Matrix4x4_t339  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)
extern "C" void Transform_INTERNAL_get_worldToLocalMatrix_m2628 (Transform_t3 * __this, Matrix4x4_t339 * ___value, const MethodInfo* method)
{
	typedef void (*Transform_INTERNAL_get_worldToLocalMatrix_m2628_ftn) (Transform_t3 *, Matrix4x4_t339 *);
	static Transform_INTERNAL_get_worldToLocalMatrix_m2628_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_get_worldToLocalMatrix_m2628_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_get_worldToLocalMatrix(UnityEngine.Matrix4x4&)");
	_il2cpp_icall_func(__this, ___value);
}
// UnityEngine.Vector3 UnityEngine.Transform::TransformPoint(UnityEngine.Vector3)
extern "C" Vector3_t35  Transform_TransformPoint_m1888 (Transform_t3 * __this, Vector3_t35  ___position, const MethodInfo* method)
{
	{
		Vector3_t35  L_0 = Transform_INTERNAL_CALL_TransformPoint_m2629(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)
extern "C" Vector3_t35  Transform_INTERNAL_CALL_TransformPoint_m2629 (Object_t * __this /* static, unused */, Transform_t3 * ___self, Vector3_t35 * ___position, const MethodInfo* method)
{
	typedef Vector3_t35  (*Transform_INTERNAL_CALL_TransformPoint_m2629_ftn) (Transform_t3 *, Vector3_t35 *);
	static Transform_INTERNAL_CALL_TransformPoint_m2629_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_TransformPoint_m2629_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_TransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Vector3 UnityEngine.Transform::InverseTransformPoint(UnityEngine.Vector3)
extern "C" Vector3_t35  Transform_InverseTransformPoint_m1766 (Transform_t3 * __this, Vector3_t35  ___position, const MethodInfo* method)
{
	{
		Vector3_t35  L_0 = Transform_INTERNAL_CALL_InverseTransformPoint_m2630(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)
extern "C" Vector3_t35  Transform_INTERNAL_CALL_InverseTransformPoint_m2630 (Object_t * __this /* static, unused */, Transform_t3 * ___self, Vector3_t35 * ___position, const MethodInfo* method)
{
	typedef Vector3_t35  (*Transform_INTERNAL_CALL_InverseTransformPoint_m2630_ftn) (Transform_t3 *, Vector3_t35 *);
	static Transform_INTERNAL_CALL_InverseTransformPoint_m2630_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_INTERNAL_CALL_InverseTransformPoint_m2630_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::INTERNAL_CALL_InverseTransformPoint(UnityEngine.Transform,UnityEngine.Vector3&)");
	return _il2cpp_icall_func(___self, ___position);
}
// System.Int32 UnityEngine.Transform::get_childCount()
extern "C" int32_t Transform_get_childCount_m228 (Transform_t3 * __this, const MethodInfo* method)
{
	typedef int32_t (*Transform_get_childCount_m228_ftn) (Transform_t3 *);
	static Transform_get_childCount_m228_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_get_childCount_m228_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::get_childCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Transform::SetAsFirstSibling()
extern "C" void Transform_SetAsFirstSibling_m1796 (Transform_t3 * __this, const MethodInfo* method)
{
	typedef void (*Transform_SetAsFirstSibling_m1796_ftn) (Transform_t3 *);
	static Transform_SetAsFirstSibling_m1796_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_SetAsFirstSibling_m1796_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::SetAsFirstSibling()");
	_il2cpp_icall_func(__this);
}
// System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator()
extern TypeInfo* Enumerator_t449_il2cpp_TypeInfo_var;
extern "C" Object_t * Transform_GetEnumerator_m2631 (Transform_t3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t449_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(336);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enumerator_t449 * L_0 = (Enumerator_t449 *)il2cpp_codegen_object_new (Enumerator_t449_il2cpp_TypeInfo_var);
		Enumerator__ctor_m2613(L_0, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// UnityEngine.Transform UnityEngine.Transform::GetChild(System.Int32)
extern "C" Transform_t3 * Transform_GetChild_m225 (Transform_t3 * __this, int32_t ___index, const MethodInfo* method)
{
	typedef Transform_t3 * (*Transform_GetChild_m225_ftn) (Transform_t3 *, int32_t);
	static Transform_GetChild_m225_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Transform_GetChild_m225_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Transform::GetChild(System.Int32)");
	return _il2cpp_icall_func(__this, ___index);
}
// System.Single UnityEngine.Time::get_deltaTime()
extern "C" float Time_get_deltaTime_m212 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_deltaTime_m212_ftn) ();
	static Time_get_deltaTime_m212_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_deltaTime_m212_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_deltaTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_unscaledTime()
extern "C" float Time_get_unscaledTime_m1552 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_unscaledTime_m1552_ftn) ();
	static Time_get_unscaledTime_m1552_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledTime_m1552_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledTime()");
	return _il2cpp_icall_func();
}
// System.Single UnityEngine.Time::get_unscaledDeltaTime()
extern "C" float Time_get_unscaledDeltaTime_m1600 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef float (*Time_get_unscaledDeltaTime_m1600_ftn) ();
	static Time_get_unscaledDeltaTime_m1600_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Time_get_unscaledDeltaTime_m1600_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Time::get_unscaledDeltaTime()");
	return _il2cpp_icall_func();
}
// System.Int32 UnityEngine.Random::Range(System.Int32,System.Int32)
extern "C" int32_t Random_Range_m195 (Object_t * __this /* static, unused */, int32_t ___min, int32_t ___max, const MethodInfo* method)
{
	{
		int32_t L_0 = ___min;
		int32_t L_1 = ___max;
		int32_t L_2 = Random_RandomRangeInt_m2632(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)
extern "C" int32_t Random_RandomRangeInt_m2632 (Object_t * __this /* static, unused */, int32_t ___min, int32_t ___max, const MethodInfo* method)
{
	typedef int32_t (*Random_RandomRangeInt_m2632_ftn) (int32_t, int32_t);
	static Random_RandomRangeInt_m2632_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Random_RandomRangeInt_m2632_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Random::RandomRangeInt(System.Int32,System.Int32)");
	return _il2cpp_icall_func(___min, ___max);
}
// System.Void UnityEngine.YieldInstruction::.ctor()
extern "C" void YieldInstruction__ctor_m2633 (YieldInstruction_t365 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t365_marshal(const YieldInstruction_t365& unmarshaled, YieldInstruction_t365_marshaled& marshaled)
{
}
extern "C" void YieldInstruction_t365_marshal_back(const YieldInstruction_t365_marshaled& marshaled, YieldInstruction_t365& unmarshaled)
{
}
// Conversion method for clean up from marshalling of: UnityEngine.YieldInstruction
extern "C" void YieldInstruction_t365_marshal_cleanup(YieldInstruction_t365_marshaled& marshaled)
{
}
// System.Void UnityEngine.PlayerPrefsException::.ctor(System.String)
extern "C" void PlayerPrefsException__ctor_m2634 (PlayerPrefsException_t452 * __this, String_t* ___error, const MethodInfo* method)
{
	{
		String_t* L_0 = ___error;
		Exception__ctor_m3289(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)
extern "C" bool PlayerPrefs_TrySetInt_m2635 (Object_t * __this /* static, unused */, String_t* ___key, int32_t ___value, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetInt_m2635_ftn) (String_t*, int32_t);
	static PlayerPrefs_TrySetInt_m2635_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetInt_m2635_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetInt(System.String,System.Int32)");
	return _il2cpp_icall_func(___key, ___value);
}
// System.Boolean UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)
extern "C" bool PlayerPrefs_TrySetSetString_m2636 (Object_t * __this /* static, unused */, String_t* ___key, String_t* ___value, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_TrySetSetString_m2636_ftn) (String_t*, String_t*);
	static PlayerPrefs_TrySetSetString_m2636_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_TrySetSetString_m2636_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::TrySetSetString(System.String,System.String)");
	return _il2cpp_icall_func(___key, ___value);
}
// System.Void UnityEngine.PlayerPrefs::SetInt(System.String,System.Int32)
extern TypeInfo* PlayerPrefsException_t452_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral267;
extern "C" void PlayerPrefs_SetInt_m246 (Object_t * __this /* static, unused */, String_t* ___key, int32_t ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayerPrefsException_t452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(337);
		_stringLiteral267 = il2cpp_codegen_string_literal_from_index(267);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key;
		int32_t L_1 = ___value;
		bool L_2 = PlayerPrefs_TrySetInt_m2635(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t452 * L_3 = (PlayerPrefsException_t452 *)il2cpp_codegen_object_new (PlayerPrefsException_t452_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m2634(L_3, _stringLiteral267, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.PlayerPrefs::SetString(System.String,System.String)
extern TypeInfo* PlayerPrefsException_t452_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral267;
extern "C" void PlayerPrefs_SetString_m220 (Object_t * __this /* static, unused */, String_t* ___key, String_t* ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PlayerPrefsException_t452_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(337);
		_stringLiteral267 = il2cpp_codegen_string_literal_from_index(267);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___key;
		String_t* L_1 = ___value;
		bool L_2 = PlayerPrefs_TrySetSetString_m2636(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		PlayerPrefsException_t452 * L_3 = (PlayerPrefsException_t452 *)il2cpp_codegen_object_new (PlayerPrefsException_t452_il2cpp_TypeInfo_var);
		PlayerPrefsException__ctor_m2634(L_3, _stringLiteral267, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_3);
	}

IL_0017:
	{
		return;
	}
}
// System.String UnityEngine.PlayerPrefs::GetString(System.String,System.String)
extern "C" String_t* PlayerPrefs_GetString_m2637 (Object_t * __this /* static, unused */, String_t* ___key, String_t* ___defaultValue, const MethodInfo* method)
{
	typedef String_t* (*PlayerPrefs_GetString_m2637_ftn) (String_t*, String_t*);
	static PlayerPrefs_GetString_m2637_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_GetString_m2637_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::GetString(System.String,System.String)");
	return _il2cpp_icall_func(___key, ___defaultValue);
}
// System.String UnityEngine.PlayerPrefs::GetString(System.String)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* PlayerPrefs_GetString_m201 (Object_t * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		V_0 = L_0;
		String_t* L_1 = ___key;
		String_t* L_2 = V_0;
		String_t* L_3 = PlayerPrefs_GetString_m2637(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.PlayerPrefs::HasKey(System.String)
extern "C" bool PlayerPrefs_HasKey_m254 (Object_t * __this /* static, unused */, String_t* ___key, const MethodInfo* method)
{
	typedef bool (*PlayerPrefs_HasKey_m254_ftn) (String_t*);
	static PlayerPrefs_HasKey_m254_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (PlayerPrefs_HasKey_m254_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.PlayerPrefs::HasKey(System.String)");
	return _il2cpp_icall_func(___key);
}
// UnityEngine.Vector3 UnityEngine.Particle::get_position()
extern "C" Vector3_t35  Particle_get_position_m2638 (Particle_t454 * __this, const MethodInfo* method)
{
	{
		Vector3_t35  L_0 = (__this->___m_Position_0);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_position(UnityEngine.Vector3)
extern "C" void Particle_set_position_m2639 (Particle_t454 * __this, Vector3_t35  ___value, const MethodInfo* method)
{
	{
		Vector3_t35  L_0 = ___value;
		__this->___m_Position_0 = L_0;
		return;
	}
}
// UnityEngine.Vector3 UnityEngine.Particle::get_velocity()
extern "C" Vector3_t35  Particle_get_velocity_m2640 (Particle_t454 * __this, const MethodInfo* method)
{
	{
		Vector3_t35  L_0 = (__this->___m_Velocity_1);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_velocity(UnityEngine.Vector3)
extern "C" void Particle_set_velocity_m2641 (Particle_t454 * __this, Vector3_t35  ___value, const MethodInfo* method)
{
	{
		Vector3_t35  L_0 = ___value;
		__this->___m_Velocity_1 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_energy()
extern "C" float Particle_get_energy_m2642 (Particle_t454 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Energy_5);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_energy(System.Single)
extern "C" void Particle_set_energy_m2643 (Particle_t454 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Energy_5 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_startEnergy()
extern "C" float Particle_get_startEnergy_m2644 (Particle_t454 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_StartEnergy_6);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_startEnergy(System.Single)
extern "C" void Particle_set_startEnergy_m2645 (Particle_t454 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_StartEnergy_6 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_size()
extern "C" float Particle_get_size_m2646 (Particle_t454 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Size_2);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_size(System.Single)
extern "C" void Particle_set_size_m2647 (Particle_t454 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Size_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_rotation()
extern "C" float Particle_get_rotation_m2648 (Particle_t454 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Rotation_3);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_rotation(System.Single)
extern "C" void Particle_set_rotation_m2649 (Particle_t454 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Rotation_3 = L_0;
		return;
	}
}
// System.Single UnityEngine.Particle::get_angularVelocity()
extern "C" float Particle_get_angularVelocity_m2650 (Particle_t454 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_AngularVelocity_4);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_angularVelocity(System.Single)
extern "C" void Particle_set_angularVelocity_m2651 (Particle_t454 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_AngularVelocity_4 = L_0;
		return;
	}
}
// UnityEngine.Color UnityEngine.Particle::get_color()
extern "C" Color_t128  Particle_get_color_m2652 (Particle_t454 * __this, const MethodInfo* method)
{
	{
		Color_t128  L_0 = (__this->___m_Color_7);
		return L_0;
	}
}
// System.Void UnityEngine.Particle::set_color(UnityEngine.Color)
extern "C" void Particle_set_color_m2653 (Particle_t454 * __this, Color_t128  ___value, const MethodInfo* method)
{
	{
		Color_t128  L_0 = ___value;
		__this->___m_Color_7 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.Physics::Internal_Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C" bool Physics_Internal_Raycast_m2654 (Object_t * __this /* static, unused */, Vector3_t35  ___origin, Vector3_t35  ___direction, RaycastHit_t283 * ___hitInfo, float ___maxDistance, int32_t ___layermask, const MethodInfo* method)
{
	{
		RaycastHit_t283 * L_0 = ___hitInfo;
		float L_1 = ___maxDistance;
		int32_t L_2 = ___layermask;
		bool L_3 = Physics_INTERNAL_CALL_Internal_Raycast_m2655(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C" bool Physics_INTERNAL_CALL_Internal_Raycast_m2655 (Object_t * __this /* static, unused */, Vector3_t35 * ___origin, Vector3_t35 * ___direction, RaycastHit_t283 * ___hitInfo, float ___maxDistance, int32_t ___layermask, const MethodInfo* method)
{
	typedef bool (*Physics_INTERNAL_CALL_Internal_Raycast_m2655_ftn) (Vector3_t35 *, Vector3_t35 *, RaycastHit_t283 *, float, int32_t);
	static Physics_INTERNAL_CALL_Internal_Raycast_m2655_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_Internal_Raycast_m2655_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector3&,UnityEngine.Vector3&,UnityEngine.RaycastHit&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___origin, ___direction, ___hitInfo, ___maxDistance, ___layermask);
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Vector3,UnityEngine.Vector3,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C" bool Physics_Raycast_m2656 (Object_t * __this /* static, unused */, Vector3_t35  ___origin, Vector3_t35  ___direction, RaycastHit_t283 * ___hitInfo, float ___maxDistance, int32_t ___layerMask, const MethodInfo* method)
{
	{
		Vector3_t35  L_0 = ___origin;
		Vector3_t35  L_1 = ___direction;
		RaycastHit_t283 * L_2 = ___hitInfo;
		float L_3 = ___maxDistance;
		int32_t L_4 = ___layerMask;
		bool L_5 = Physics_Internal_Raycast_m2654(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Boolean UnityEngine.Physics::Raycast(UnityEngine.Ray,UnityEngine.RaycastHit&,System.Single,System.Int32)
extern "C" bool Physics_Raycast_m1668 (Object_t * __this /* static, unused */, Ray_t306  ___ray, RaycastHit_t283 * ___hitInfo, float ___maxDistance, int32_t ___layerMask, const MethodInfo* method)
{
	{
		Vector3_t35  L_0 = Ray_get_origin_m1574((&___ray), /*hidden argument*/NULL);
		Vector3_t35  L_1 = Ray_get_direction_m1575((&___ray), /*hidden argument*/NULL);
		RaycastHit_t283 * L_2 = ___hitInfo;
		float L_3 = ___maxDistance;
		int32_t L_4 = ___layerMask;
		bool L_5 = Physics_Raycast_m2656(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Ray,System.Single,System.Int32)
extern "C" RaycastHitU5BU5D_t309* Physics_RaycastAll_m1588 (Object_t * __this /* static, unused */, Ray_t306  ___ray, float ___maxDistance, int32_t ___layerMask, const MethodInfo* method)
{
	{
		Vector3_t35  L_0 = Ray_get_origin_m1574((&___ray), /*hidden argument*/NULL);
		Vector3_t35  L_1 = Ray_get_direction_m1575((&___ray), /*hidden argument*/NULL);
		float L_2 = ___maxDistance;
		int32_t L_3 = ___layerMask;
		RaycastHitU5BU5D_t309* L_4 = Physics_RaycastAll_m2657(NULL /*static, unused*/, L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::RaycastAll(UnityEngine.Vector3,UnityEngine.Vector3,System.Single,System.Int32)
extern "C" RaycastHitU5BU5D_t309* Physics_RaycastAll_m2657 (Object_t * __this /* static, unused */, Vector3_t35  ___origin, Vector3_t35  ___direction, float ___maxDistance, int32_t ___layermask, const MethodInfo* method)
{
	{
		float L_0 = ___maxDistance;
		int32_t L_1 = ___layermask;
		RaycastHitU5BU5D_t309* L_2 = Physics_INTERNAL_CALL_RaycastAll_m2658(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// UnityEngine.RaycastHit[] UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32)
extern "C" RaycastHitU5BU5D_t309* Physics_INTERNAL_CALL_RaycastAll_m2658 (Object_t * __this /* static, unused */, Vector3_t35 * ___origin, Vector3_t35 * ___direction, float ___maxDistance, int32_t ___layermask, const MethodInfo* method)
{
	typedef RaycastHitU5BU5D_t309* (*Physics_INTERNAL_CALL_RaycastAll_m2658_ftn) (Vector3_t35 *, Vector3_t35 *, float, int32_t);
	static Physics_INTERNAL_CALL_RaycastAll_m2658_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics_INTERNAL_CALL_RaycastAll_m2658_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics::INTERNAL_CALL_RaycastAll(UnityEngine.Vector3&,UnityEngine.Vector3&,System.Single,System.Int32)");
	return _il2cpp_icall_func(___origin, ___direction, ___maxDistance, ___layermask);
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_point()
extern "C" Vector3_t35  RaycastHit_get_point_m1593 (RaycastHit_t283 * __this, const MethodInfo* method)
{
	{
		Vector3_t35  L_0 = (__this->___m_Point_0);
		return L_0;
	}
}
// UnityEngine.Vector3 UnityEngine.RaycastHit::get_normal()
extern "C" Vector3_t35  RaycastHit_get_normal_m1594 (RaycastHit_t283 * __this, const MethodInfo* method)
{
	{
		Vector3_t35  L_0 = (__this->___m_Normal_1);
		return L_0;
	}
}
// System.Single UnityEngine.RaycastHit::get_distance()
extern "C" float RaycastHit_get_distance_m1592 (RaycastHit_t283 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Distance_3);
		return L_0;
	}
}
// UnityEngine.Collider UnityEngine.RaycastHit::get_collider()
extern "C" Collider_t310 * RaycastHit_get_collider_m1591 (RaycastHit_t283 * __this, const MethodInfo* method)
{
	{
		Collider_t310 * L_0 = (__this->___m_Collider_5);
		return L_0;
	}
}
// System.Void UnityEngine.Physics2D::.cctor()
extern TypeInfo* List_1_t456_il2cpp_TypeInfo_var;
extern TypeInfo* Physics2D_t62_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3290_MethodInfo_var;
extern "C" void Physics2D__cctor_m2659 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t456_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(338);
		Physics2D_t62_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		List_1__ctor_m3290_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483917);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t456 * L_0 = (List_1_t456 *)il2cpp_codegen_object_new (List_1_t456_il2cpp_TypeInfo_var);
		List_1__ctor_m3290(L_0, /*hidden argument*/List_1__ctor_m3290_MethodInfo_var);
		((Physics2D_t62_StaticFields*)Physics2D_t62_il2cpp_TypeInfo_var->static_fields)->___m_LastDisabledRigidbody2D_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.Physics2D::Internal_Linecast(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern TypeInfo* Physics2D_t62_il2cpp_TypeInfo_var;
extern "C" void Physics2D_Internal_Linecast_m2660 (Object_t * __this /* static, unused */, Vector2_t59  ___start, Vector2_t59  ___end, int32_t ___layerMask, float ___minDepth, float ___maxDepth, RaycastHit2D_t52 * ___raycastHit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t62_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___layerMask;
		float L_1 = ___minDepth;
		float L_2 = ___maxDepth;
		RaycastHit2D_t52 * L_3 = ___raycastHit;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t62_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_CALL_Internal_Linecast_m2661(NULL /*static, unused*/, (&___start), (&___end), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Linecast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C" void Physics2D_INTERNAL_CALL_Internal_Linecast_m2661 (Object_t * __this /* static, unused */, Vector2_t59 * ___start, Vector2_t59 * ___end, int32_t ___layerMask, float ___minDepth, float ___maxDepth, RaycastHit2D_t52 * ___raycastHit, const MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_CALL_Internal_Linecast_m2661_ftn) (Vector2_t59 *, Vector2_t59 *, int32_t, float, float, RaycastHit2D_t52 *);
	static Physics2D_INTERNAL_CALL_Internal_Linecast_m2661_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_Linecast_m2661_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_Linecast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___start, ___end, ___layerMask, ___minDepth, ___maxDepth, ___raycastHit);
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Linecast(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32)
extern TypeInfo* Physics2D_t62_il2cpp_TypeInfo_var;
extern "C" RaycastHit2D_t52  Physics2D_Linecast_m268 (Object_t * __this /* static, unused */, Vector2_t59  ___start, Vector2_t59  ___end, int32_t ___layerMask, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t62_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		Vector2_t59  L_0 = ___start;
		Vector2_t59  L_1 = ___end;
		int32_t L_2 = ___layerMask;
		float L_3 = V_1;
		float L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t62_il2cpp_TypeInfo_var);
		RaycastHit2D_t52  L_5 = Physics2D_Linecast_m2662(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Linecast(UnityEngine.Vector2,UnityEngine.Vector2,System.Int32,System.Single,System.Single)
extern TypeInfo* Physics2D_t62_il2cpp_TypeInfo_var;
extern "C" RaycastHit2D_t52  Physics2D_Linecast_m2662 (Object_t * __this /* static, unused */, Vector2_t59  ___start, Vector2_t59  ___end, int32_t ___layerMask, float ___minDepth, float ___maxDepth, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t62_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t52  V_0 = {0};
	{
		Vector2_t59  L_0 = ___start;
		Vector2_t59  L_1 = ___end;
		int32_t L_2 = ___layerMask;
		float L_3 = ___minDepth;
		float L_4 = ___maxDepth;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t62_il2cpp_TypeInfo_var);
		Physics2D_Internal_Linecast_m2660(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		RaycastHit2D_t52  L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.Physics2D::Internal_Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern TypeInfo* Physics2D_t62_il2cpp_TypeInfo_var;
extern "C" void Physics2D_Internal_Raycast_m2663 (Object_t * __this /* static, unused */, Vector2_t59  ___origin, Vector2_t59  ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, RaycastHit2D_t52 * ___raycastHit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t62_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		float L_2 = ___minDepth;
		float L_3 = ___maxDepth;
		RaycastHit2D_t52 * L_4 = ___raycastHit;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t62_il2cpp_TypeInfo_var);
		Physics2D_INTERNAL_CALL_Internal_Raycast_m2664(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, L_3, L_4, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)
extern "C" void Physics2D_INTERNAL_CALL_Internal_Raycast_m2664 (Object_t * __this /* static, unused */, Vector2_t59 * ___origin, Vector2_t59 * ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, RaycastHit2D_t52 * ___raycastHit, const MethodInfo* method)
{
	typedef void (*Physics2D_INTERNAL_CALL_Internal_Raycast_m2664_ftn) (Vector2_t59 *, Vector2_t59 *, float, int32_t, float, float, RaycastHit2D_t52 *);
	static Physics2D_INTERNAL_CALL_Internal_Raycast_m2664_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_Internal_Raycast_m2664_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_Internal_Raycast(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single,UnityEngine.RaycastHit2D&)");
	_il2cpp_icall_func(___origin, ___direction, ___distance, ___layerMask, ___minDepth, ___maxDepth, ___raycastHit);
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern TypeInfo* Physics2D_t62_il2cpp_TypeInfo_var;
extern "C" RaycastHit2D_t52  Physics2D_Raycast_m1669 (Object_t * __this /* static, unused */, Vector2_t59  ___origin, Vector2_t59  ___direction, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t62_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		Vector2_t59  L_0 = ___origin;
		Vector2_t59  L_1 = ___direction;
		float L_2 = ___distance;
		int32_t L_3 = ___layerMask;
		float L_4 = V_1;
		float L_5 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t62_il2cpp_TypeInfo_var);
		RaycastHit2D_t52  L_6 = Physics2D_Raycast_m2665(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.RaycastHit2D UnityEngine.Physics2D::Raycast(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32,System.Single,System.Single)
extern TypeInfo* Physics2D_t62_il2cpp_TypeInfo_var;
extern "C" RaycastHit2D_t52  Physics2D_Raycast_m2665 (Object_t * __this /* static, unused */, Vector2_t59  ___origin, Vector2_t59  ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t62_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	RaycastHit2D_t52  V_0 = {0};
	{
		Vector2_t59  L_0 = ___origin;
		Vector2_t59  L_1 = ___direction;
		float L_2 = ___distance;
		int32_t L_3 = ___layerMask;
		float L_4 = ___minDepth;
		float L_5 = ___maxDepth;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t62_il2cpp_TypeInfo_var);
		Physics2D_Internal_Raycast_m2663(NULL /*static, unused*/, L_0, L_1, L_2, L_3, L_4, L_5, (&V_0), /*hidden argument*/NULL);
		RaycastHit2D_t52  L_6 = V_0;
		return L_6;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::RaycastAll(UnityEngine.Vector2,UnityEngine.Vector2,System.Single,System.Int32)
extern TypeInfo* Physics2D_t62_il2cpp_TypeInfo_var;
extern "C" RaycastHit2DU5BU5D_t307* Physics2D_RaycastAll_m1576 (Object_t * __this /* static, unused */, Vector2_t59  ___origin, Vector2_t59  ___direction, float ___distance, int32_t ___layerMask, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t62_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		V_0 = (std::numeric_limits<float>::infinity());
		V_1 = (-std::numeric_limits<float>::infinity());
		float L_0 = ___distance;
		int32_t L_1 = ___layerMask;
		float L_2 = V_1;
		float L_3 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t62_il2cpp_TypeInfo_var);
		RaycastHit2DU5BU5D_t307* L_4 = Physics2D_INTERNAL_CALL_RaycastAll_m2666(NULL /*static, unused*/, (&___origin), (&___direction), L_0, L_1, L_2, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.RaycastHit2D[] UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)
extern "C" RaycastHit2DU5BU5D_t307* Physics2D_INTERNAL_CALL_RaycastAll_m2666 (Object_t * __this /* static, unused */, Vector2_t59 * ___origin, Vector2_t59 * ___direction, float ___distance, int32_t ___layerMask, float ___minDepth, float ___maxDepth, const MethodInfo* method)
{
	typedef RaycastHit2DU5BU5D_t307* (*Physics2D_INTERNAL_CALL_RaycastAll_m2666_ftn) (Vector2_t59 *, Vector2_t59 *, float, int32_t, float, float);
	static Physics2D_INTERNAL_CALL_RaycastAll_m2666_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Physics2D_INTERNAL_CALL_RaycastAll_m2666_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Physics2D::INTERNAL_CALL_RaycastAll(UnityEngine.Vector2&,UnityEngine.Vector2&,System.Single,System.Int32,System.Single,System.Single)");
	return _il2cpp_icall_func(___origin, ___direction, ___distance, ___layerMask, ___minDepth, ___maxDepth);
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_point()
extern "C" Vector2_t59  RaycastHit2D_get_point_m1581 (RaycastHit2D_t52 * __this, const MethodInfo* method)
{
	{
		Vector2_t59  L_0 = (__this->___m_Point_1);
		return L_0;
	}
}
// UnityEngine.Vector2 UnityEngine.RaycastHit2D::get_normal()
extern "C" Vector2_t59  RaycastHit2D_get_normal_m1582 (RaycastHit2D_t52 * __this, const MethodInfo* method)
{
	{
		Vector2_t59  L_0 = (__this->___m_Normal_2);
		return L_0;
	}
}
// System.Single UnityEngine.RaycastHit2D::get_fraction()
extern "C" float RaycastHit2D_get_fraction_m1670 (RaycastHit2D_t52 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Fraction_4);
		return L_0;
	}
}
// UnityEngine.Collider2D UnityEngine.RaycastHit2D::get_collider()
extern "C" Collider2D_t308 * RaycastHit2D_get_collider_m1577 (RaycastHit2D_t52 * __this, const MethodInfo* method)
{
	{
		Collider2D_t308 * L_0 = (__this->___m_Collider_5);
		return L_0;
	}
}
// UnityEngine.Rigidbody2D UnityEngine.RaycastHit2D::get_rigidbody()
extern "C" Rigidbody2D_t39 * RaycastHit2D_get_rigidbody_m2667 (RaycastHit2D_t52 * __this, const MethodInfo* method)
{
	Rigidbody2D_t39 * G_B3_0 = {0};
	{
		Collider2D_t308 * L_0 = RaycastHit2D_get_collider_m1577(__this, /*hidden argument*/NULL);
		bool L_1 = Object_op_Inequality_m1472(NULL /*static, unused*/, L_0, (Object_t54 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0021;
		}
	}
	{
		Collider2D_t308 * L_2 = RaycastHit2D_get_collider_m1577(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody2D_t39 * L_3 = Collider2D_get_attachedRigidbody_m2670(L_2, /*hidden argument*/NULL);
		G_B3_0 = L_3;
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = ((Rigidbody2D_t39 *)(NULL));
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// UnityEngine.Transform UnityEngine.RaycastHit2D::get_transform()
extern "C" Transform_t3 * RaycastHit2D_get_transform_m1579 (RaycastHit2D_t52 * __this, const MethodInfo* method)
{
	Rigidbody2D_t39 * V_0 = {0};
	{
		Rigidbody2D_t39 * L_0 = RaycastHit2D_get_rigidbody_m2667(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Rigidbody2D_t39 * L_1 = V_0;
		bool L_2 = Object_op_Inequality_m1472(NULL /*static, unused*/, L_1, (Object_t54 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_001a;
		}
	}
	{
		Rigidbody2D_t39 * L_3 = V_0;
		NullCheck(L_3);
		Transform_t3 * L_4 = Component_get_transform_m224(L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_001a:
	{
		Collider2D_t308 * L_5 = RaycastHit2D_get_collider_m1577(__this, /*hidden argument*/NULL);
		bool L_6 = Object_op_Inequality_m1472(NULL /*static, unused*/, L_5, (Object_t54 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_0037;
		}
	}
	{
		Collider2D_t308 * L_7 = RaycastHit2D_get_collider_m1577(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		Transform_t3 * L_8 = Component_get_transform_m224(L_7, /*hidden argument*/NULL);
		return L_8;
	}

IL_0037:
	{
		return (Transform_t3 *)NULL;
	}
}
// UnityEngine.Vector2 UnityEngine.Rigidbody2D::get_position()
extern "C" Vector2_t59  Rigidbody2D_get_position_m259 (Rigidbody2D_t39 * __this, const MethodInfo* method)
{
	Vector2_t59  V_0 = {0};
	{
		Rigidbody2D_INTERNAL_get_position_m2668(__this, (&V_0), /*hidden argument*/NULL);
		Vector2_t59  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_get_position(UnityEngine.Vector2&)
extern "C" void Rigidbody2D_INTERNAL_get_position_m2668 (Rigidbody2D_t39 * __this, Vector2_t59 * ___value, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_get_position_m2668_ftn) (Rigidbody2D_t39 *, Vector2_t59 *);
	static Rigidbody2D_INTERNAL_get_position_m2668_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_get_position_m2668_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_get_position(UnityEngine.Vector2&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.Rigidbody2D::MovePosition(UnityEngine.Vector2)
extern "C" void Rigidbody2D_MovePosition_m263 (Rigidbody2D_t39 * __this, Vector2_t59  ___position, const MethodInfo* method)
{
	{
		Rigidbody2D_INTERNAL_CALL_MovePosition_m2669(NULL /*static, unused*/, __this, (&___position), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Rigidbody2D::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody2D,UnityEngine.Vector2&)
extern "C" void Rigidbody2D_INTERNAL_CALL_MovePosition_m2669 (Object_t * __this /* static, unused */, Rigidbody2D_t39 * ___self, Vector2_t59 * ___position, const MethodInfo* method)
{
	typedef void (*Rigidbody2D_INTERNAL_CALL_MovePosition_m2669_ftn) (Rigidbody2D_t39 *, Vector2_t59 *);
	static Rigidbody2D_INTERNAL_CALL_MovePosition_m2669_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Rigidbody2D_INTERNAL_CALL_MovePosition_m2669_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Rigidbody2D::INTERNAL_CALL_MovePosition(UnityEngine.Rigidbody2D,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___self, ___position);
}
// UnityEngine.Rigidbody2D UnityEngine.Collider2D::get_attachedRigidbody()
extern "C" Rigidbody2D_t39 * Collider2D_get_attachedRigidbody_m2670 (Collider2D_t308 * __this, const MethodInfo* method)
{
	typedef Rigidbody2D_t39 * (*Collider2D_get_attachedRigidbody_m2670_ftn) (Collider2D_t308 *);
	static Collider2D_get_attachedRigidbody_m2670_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Collider2D_get_attachedRigidbody_m2670_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Collider2D::get_attachedRigidbody()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::.ctor(System.Object,System.IntPtr)
extern "C" void AudioConfigurationChangeHandler__ctor_m2671 (AudioConfigurationChangeHandler_t457 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::Invoke(System.Boolean)
extern "C" void AudioConfigurationChangeHandler_Invoke_m2672 (AudioConfigurationChangeHandler_t457 * __this, bool ___deviceWasChanged, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		AudioConfigurationChangeHandler_Invoke_m2672((AudioConfigurationChangeHandler_t457 *)__this->___prev_9,___deviceWasChanged, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, bool ___deviceWasChanged, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___deviceWasChanged,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, bool ___deviceWasChanged, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___deviceWasChanged,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t457(Il2CppObject* delegate, bool ___deviceWasChanged)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___deviceWasChanged' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___deviceWasChanged);

	// Marshaling cleanup of parameter '___deviceWasChanged' native representation

}
// System.IAsyncResult UnityEngine.AudioSettings/AudioConfigurationChangeHandler::BeginInvoke(System.Boolean,System.AsyncCallback,System.Object)
extern TypeInfo* Boolean_t298_il2cpp_TypeInfo_var;
extern "C" Object_t * AudioConfigurationChangeHandler_BeginInvoke_m2673 (AudioConfigurationChangeHandler_t457 * __this, bool ___deviceWasChanged, AsyncCallback_t181 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t298_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(104);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Boolean_t298_il2cpp_TypeInfo_var, &___deviceWasChanged);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioSettings/AudioConfigurationChangeHandler::EndInvoke(System.IAsyncResult)
extern "C" void AudioConfigurationChangeHandler_EndInvoke_m2674 (AudioConfigurationChangeHandler_t457 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.AudioSettings::InvokeOnAudioConfigurationChanged(System.Boolean)
extern TypeInfo* AudioSettings_t458_il2cpp_TypeInfo_var;
extern "C" void AudioSettings_InvokeOnAudioConfigurationChanged_m2675 (AudioSettings_t458 * __this, bool ___deviceWasChanged, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AudioSettings_t458_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(339);
		s_Il2CppMethodIntialized = true;
	}
	{
		AudioConfigurationChangeHandler_t457 * L_0 = ((AudioSettings_t458_StaticFields*)AudioSettings_t458_il2cpp_TypeInfo_var->static_fields)->___OnAudioConfigurationChanged_0;
		if (!L_0)
		{
			goto IL_0015;
		}
	}
	{
		AudioConfigurationChangeHandler_t457 * L_1 = ((AudioSettings_t458_StaticFields*)AudioSettings_t458_il2cpp_TypeInfo_var->static_fields)->___OnAudioConfigurationChanged_0;
		bool L_2 = ___deviceWasChanged;
		NullCheck(L_1);
		AudioConfigurationChangeHandler_Invoke_m2672(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0015:
	{
		return;
	}
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::.ctor(System.Object,System.IntPtr)
extern "C" void PCMReaderCallback__ctor_m2676 (PCMReaderCallback_t459 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::Invoke(System.Single[])
extern "C" void PCMReaderCallback_Invoke_m2677 (PCMReaderCallback_t459 * __this, SingleU5BU5D_t460* ___data, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PCMReaderCallback_Invoke_m2677((PCMReaderCallback_t459 *)__this->___prev_9,___data, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, SingleU5BU5D_t460* ___data, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, SingleU5BU5D_t460* ___data, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___data,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_PCMReaderCallback_t459(Il2CppObject* delegate, SingleU5BU5D_t460* ___data)
{
	typedef void (STDCALL *native_function_ptr_type)(float*);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___data' to native representation
	float* ____data_marshaled = { 0 };
	____data_marshaled = il2cpp_codegen_marshal_array<float>((Il2CppCodeGenArray*)___data);

	// Native function invocation
	_il2cpp_pinvoke_func(____data_marshaled);

	// Marshaling cleanup of parameter '___data' native representation

}
// System.IAsyncResult UnityEngine.AudioClip/PCMReaderCallback::BeginInvoke(System.Single[],System.AsyncCallback,System.Object)
extern "C" Object_t * PCMReaderCallback_BeginInvoke_m2678 (PCMReaderCallback_t459 * __this, SingleU5BU5D_t460* ___data, AsyncCallback_t181 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___data;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioClip/PCMReaderCallback::EndInvoke(System.IAsyncResult)
extern "C" void PCMReaderCallback_EndInvoke_m2679 (PCMReaderCallback_t459 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::.ctor(System.Object,System.IntPtr)
extern "C" void PCMSetPositionCallback__ctor_m2680 (PCMSetPositionCallback_t461 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::Invoke(System.Int32)
extern "C" void PCMSetPositionCallback_Invoke_m2681 (PCMSetPositionCallback_t461 * __this, int32_t ___position, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		PCMSetPositionCallback_Invoke_m2681((PCMSetPositionCallback_t461 *)__this->___prev_9,___position, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, int32_t ___position, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___position,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, int32_t ___position, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___position,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_PCMSetPositionCallback_t461(Il2CppObject* delegate, int32_t ___position)
{
	typedef void (STDCALL *native_function_ptr_type)(int32_t);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___position' to native representation

	// Native function invocation
	_il2cpp_pinvoke_func(___position);

	// Marshaling cleanup of parameter '___position' native representation

}
// System.IAsyncResult UnityEngine.AudioClip/PCMSetPositionCallback::BeginInvoke(System.Int32,System.AsyncCallback,System.Object)
extern TypeInfo* Int32_t56_il2cpp_TypeInfo_var;
extern "C" Object_t * PCMSetPositionCallback_BeginInvoke_m2682 (PCMSetPositionCallback_t461 * __this, int32_t ___position, AsyncCallback_t181 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		s_Il2CppMethodIntialized = true;
	}
	void *__d_args[2] = {0};
	__d_args[0] = Box(Int32_t56_il2cpp_TypeInfo_var, &___position);
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.AudioClip/PCMSetPositionCallback::EndInvoke(System.IAsyncResult)
extern "C" void PCMSetPositionCallback_EndInvoke_m2683 (PCMSetPositionCallback_t461 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[])
extern "C" void AudioClip_InvokePCMReaderCallback_Internal_m2684 (AudioClip_t462 * __this, SingleU5BU5D_t460* ___data, const MethodInfo* method)
{
	{
		PCMReaderCallback_t459 * L_0 = (__this->___m_PCMReaderCallback_2);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		PCMReaderCallback_t459 * L_1 = (__this->___m_PCMReaderCallback_2);
		SingleU5BU5D_t460* L_2 = ___data;
		NullCheck(L_1);
		PCMReaderCallback_Invoke_m2677(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32)
extern "C" void AudioClip_InvokePCMSetPositionCallback_Internal_m2685 (AudioClip_t462 * __this, int32_t ___position, const MethodInfo* method)
{
	{
		PCMSetPositionCallback_t461 * L_0 = (__this->___m_PCMSetPositionCallback_3);
		if (!L_0)
		{
			goto IL_0017;
		}
	}
	{
		PCMSetPositionCallback_t461 * L_1 = (__this->___m_PCMSetPositionCallback_3);
		int32_t L_2 = ___position;
		NullCheck(L_1);
		PCMSetPositionCallback_Invoke_m2681(L_1, L_2, /*hidden argument*/NULL);
	}

IL_0017:
	{
		return;
	}
}
// System.String UnityEngine.WebCamDevice::get_name()
extern "C" String_t* WebCamDevice_get_name_m2686 (WebCamDevice_t463 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_Name_0);
		return L_0;
	}
}
// System.Boolean UnityEngine.WebCamDevice::get_isFrontFacing()
extern "C" bool WebCamDevice_get_isFrontFacing_m2687 (WebCamDevice_t463 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Flags_1);
		return ((((int32_t)((int32_t)((int32_t)L_0&(int32_t)1))) == ((int32_t)1))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.WebCamDevice
extern "C" void WebCamDevice_t463_marshal(const WebCamDevice_t463& unmarshaled, WebCamDevice_t463_marshaled& marshaled)
{
	marshaled.___m_Name_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_Name_0);
	marshaled.___m_Flags_1 = unmarshaled.___m_Flags_1;
}
extern "C" void WebCamDevice_t463_marshal_back(const WebCamDevice_t463_marshaled& marshaled, WebCamDevice_t463& unmarshaled)
{
	unmarshaled.___m_Name_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_Name_0);
	unmarshaled.___m_Flags_1 = marshaled.___m_Flags_1;
}
// Conversion method for clean up from marshalling of: UnityEngine.WebCamDevice
extern "C" void WebCamDevice_t463_marshal_cleanup(WebCamDevice_t463_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_Name_0);
	marshaled.___m_Name_0 = NULL;
}
// System.Void UnityEngine.AnimationEvent::.ctor()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void AnimationEvent__ctor_m2688 (AnimationEvent_t465 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		__this->___m_Time_0 = (0.0f);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_FunctionName_1 = L_0;
		String_t* L_1 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		__this->___m_StringParameter_2 = L_1;
		__this->___m_ObjectReferenceParameter_3 = (Object_t54 *)NULL;
		__this->___m_FloatParameter_4 = (0.0f);
		__this->___m_IntParameter_5 = 0;
		__this->___m_MessageOptions_6 = 0;
		__this->___m_Source_7 = 0;
		__this->___m_StateSender_8 = (AnimationState_t466 *)NULL;
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_data()
extern "C" String_t* AnimationEvent_get_data_m2689 (AnimationEvent_t465 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_StringParameter_2);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_data(System.String)
extern "C" void AnimationEvent_set_data_m2690 (AnimationEvent_t465 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_StringParameter_2 = L_0;
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_stringParameter()
extern "C" String_t* AnimationEvent_get_stringParameter_m2691 (AnimationEvent_t465 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_StringParameter_2);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_stringParameter(System.String)
extern "C" void AnimationEvent_set_stringParameter_m2692 (AnimationEvent_t465 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_StringParameter_2 = L_0;
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_floatParameter()
extern "C" float AnimationEvent_get_floatParameter_m2693 (AnimationEvent_t465 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_FloatParameter_4);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_floatParameter(System.Single)
extern "C" void AnimationEvent_set_floatParameter_m2694 (AnimationEvent_t465 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_FloatParameter_4 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.AnimationEvent::get_intParameter()
extern "C" int32_t AnimationEvent_get_intParameter_m2695 (AnimationEvent_t465 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_IntParameter_5);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_intParameter(System.Int32)
extern "C" void AnimationEvent_set_intParameter_m2696 (AnimationEvent_t465 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_IntParameter_5 = L_0;
		return;
	}
}
// UnityEngine.Object UnityEngine.AnimationEvent::get_objectReferenceParameter()
extern "C" Object_t54 * AnimationEvent_get_objectReferenceParameter_m2697 (AnimationEvent_t465 * __this, const MethodInfo* method)
{
	{
		Object_t54 * L_0 = (__this->___m_ObjectReferenceParameter_3);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_objectReferenceParameter(UnityEngine.Object)
extern "C" void AnimationEvent_set_objectReferenceParameter_m2698 (AnimationEvent_t465 * __this, Object_t54 * ___value, const MethodInfo* method)
{
	{
		Object_t54 * L_0 = ___value;
		__this->___m_ObjectReferenceParameter_3 = L_0;
		return;
	}
}
// System.String UnityEngine.AnimationEvent::get_functionName()
extern "C" String_t* AnimationEvent_get_functionName_m2699 (AnimationEvent_t465 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_FunctionName_1);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_functionName(System.String)
extern "C" void AnimationEvent_set_functionName_m2700 (AnimationEvent_t465 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_FunctionName_1 = L_0;
		return;
	}
}
// System.Single UnityEngine.AnimationEvent::get_time()
extern "C" float AnimationEvent_get_time_m2701 (AnimationEvent_t465 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Time_0);
		return L_0;
	}
}
// System.Void UnityEngine.AnimationEvent::set_time(System.Single)
extern "C" void AnimationEvent_set_time_m2702 (AnimationEvent_t465 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Time_0 = L_0;
		return;
	}
}
// UnityEngine.SendMessageOptions UnityEngine.AnimationEvent::get_messageOptions()
extern "C" int32_t AnimationEvent_get_messageOptions_m2703 (AnimationEvent_t465 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_MessageOptions_6);
		return (int32_t)(L_0);
	}
}
// System.Void UnityEngine.AnimationEvent::set_messageOptions(UnityEngine.SendMessageOptions)
extern "C" void AnimationEvent_set_messageOptions_m2704 (AnimationEvent_t465 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___m_MessageOptions_6 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByLegacy()
extern "C" bool AnimationEvent_get_isFiredByLegacy_m2705 (AnimationEvent_t465 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Source_7);
		return ((((int32_t)L_0) == ((int32_t)1))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimationEvent::get_isFiredByAnimator()
extern "C" bool AnimationEvent_get_isFiredByAnimator_m2706 (AnimationEvent_t465 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Source_7);
		return ((((int32_t)L_0) == ((int32_t)2))? 1 : 0);
	}
}
// UnityEngine.AnimationState UnityEngine.AnimationEvent::get_animationState()
extern Il2CppCodeGenString* _stringLiteral268;
extern "C" AnimationState_t466 * AnimationEvent_get_animationState_m2707 (AnimationEvent_t465 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral268 = il2cpp_codegen_string_literal_from_index(268);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByLegacy_m2705(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_LogError_m1469(NULL /*static, unused*/, _stringLiteral268, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimationState_t466 * L_1 = (__this->___m_StateSender_8);
		return L_1;
	}
}
// UnityEngine.AnimatorStateInfo UnityEngine.AnimationEvent::get_animatorStateInfo()
extern Il2CppCodeGenString* _stringLiteral269;
extern "C" AnimatorStateInfo_t467  AnimationEvent_get_animatorStateInfo_m2708 (AnimationEvent_t465 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral269 = il2cpp_codegen_string_literal_from_index(269);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m2706(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_LogError_m1469(NULL /*static, unused*/, _stringLiteral269, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimatorStateInfo_t467  L_1 = (__this->___m_AnimatorStateInfo_9);
		return L_1;
	}
}
// UnityEngine.AnimatorClipInfo UnityEngine.AnimationEvent::get_animatorClipInfo()
extern Il2CppCodeGenString* _stringLiteral270;
extern "C" AnimatorClipInfo_t468  AnimationEvent_get_animatorClipInfo_m2709 (AnimationEvent_t465 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral270 = il2cpp_codegen_string_literal_from_index(270);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = AnimationEvent_get_isFiredByAnimator_m2706(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_0015;
		}
	}
	{
		Debug_LogError_m1469(NULL /*static, unused*/, _stringLiteral270, /*hidden argument*/NULL);
	}

IL_0015:
	{
		AnimatorClipInfo_t468  L_1 = (__this->___m_AnimatorClipInfo_10);
		return L_1;
	}
}
// System.Int32 UnityEngine.AnimationEvent::GetHash()
extern "C" int32_t AnimationEvent_GetHash_m2710 (AnimationEvent_t465 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	float V_1 = 0.0f;
	{
		V_0 = 0;
		String_t* L_0 = AnimationEvent_get_functionName_m2699(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		int32_t L_1 = String_GetHashCode_m3247(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		float L_3 = AnimationEvent_get_time_m2701(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		int32_t L_4 = Single_GetHashCode_m3245((&V_1), /*hidden argument*/NULL);
		V_0 = ((int32_t)((int32_t)((int32_t)((int32_t)((int32_t)33)*(int32_t)L_2))+(int32_t)L_4));
		int32_t L_5 = V_0;
		return L_5;
	}
}
// System.Void UnityEngine.AnimationCurve::.ctor(UnityEngine.Keyframe[])
extern "C" void AnimationCurve__ctor_m2711 (AnimationCurve_t470 * __this, KeyframeU5BU5D_t610* ___keys, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		KeyframeU5BU5D_t610* L_0 = ___keys;
		AnimationCurve_Init_m2715(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::.ctor()
extern "C" void AnimationCurve__ctor_m2712 (AnimationCurve_t470 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		AnimationCurve_Init_m2715(__this, (KeyframeU5BU5D_t610*)(KeyframeU5BU5D_t610*)NULL, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Cleanup()
extern "C" void AnimationCurve_Cleanup_m2713 (AnimationCurve_t470 * __this, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Cleanup_m2713_ftn) (AnimationCurve_t470 *);
	static AnimationCurve_Cleanup_m2713_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Cleanup_m2713_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Cleanup()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.AnimationCurve::Finalize()
extern "C" void AnimationCurve_Finalize_m2714 (AnimationCurve_t470 * __this, const MethodInfo* method)
{
	Exception_t301 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t301 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		AnimationCurve_Cleanup_m2713(__this, /*hidden argument*/NULL);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t301 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3230(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t301 *)
	}

IL_0012:
	{
		return;
	}
}
// System.Void UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])
extern "C" void AnimationCurve_Init_m2715 (AnimationCurve_t470 * __this, KeyframeU5BU5D_t610* ___keys, const MethodInfo* method)
{
	typedef void (*AnimationCurve_Init_m2715_ftn) (AnimationCurve_t470 *, KeyframeU5BU5D_t610*);
	static AnimationCurve_Init_m2715_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (AnimationCurve_Init_m2715_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.AnimationCurve::Init(UnityEngine.Keyframe[])");
	_il2cpp_icall_func(__this, ___keys);
}
// Conversion methods for marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t470_marshal(const AnimationCurve_t470& unmarshaled, AnimationCurve_t470_marshaled& marshaled)
{
	marshaled.___m_Ptr_0 = reinterpret_cast<intptr_t>((unmarshaled.___m_Ptr_0).___m_value_0);
}
extern "C" void AnimationCurve_t470_marshal_back(const AnimationCurve_t470_marshaled& marshaled, AnimationCurve_t470& unmarshaled)
{
	(unmarshaled.___m_Ptr_0).___m_value_0 = reinterpret_cast<void*>(marshaled.___m_Ptr_0);
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimationCurve
extern "C" void AnimationCurve_t470_marshal_cleanup(AnimationCurve_t470_marshaled& marshaled)
{
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsName(System.String)
extern "C" bool AnimatorStateInfo_IsName_m2716 (AnimatorStateInfo_t467 * __this, String_t* ___name, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t G_B4_0 = 0;
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m2734(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		int32_t L_2 = V_0;
		int32_t L_3 = (__this->___m_FullPath_2);
		if ((((int32_t)L_2) == ((int32_t)L_3)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_4 = V_0;
		int32_t L_5 = (__this->___m_Name_0);
		if ((((int32_t)L_4) == ((int32_t)L_5)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_6 = V_0;
		int32_t L_7 = (__this->___m_Path_1);
		G_B4_0 = ((((int32_t)L_6) == ((int32_t)L_7))? 1 : 0);
		goto IL_002b;
	}

IL_002a:
	{
		G_B4_0 = 1;
	}

IL_002b:
	{
		return G_B4_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_fullPathHash()
extern "C" int32_t AnimatorStateInfo_get_fullPathHash_m2717 (AnimatorStateInfo_t467 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_FullPath_2);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_nameHash()
extern "C" int32_t AnimatorStateInfo_get_nameHash_m2718 (AnimatorStateInfo_t467 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Path_1);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_shortNameHash()
extern "C" int32_t AnimatorStateInfo_get_shortNameHash_m2719 (AnimatorStateInfo_t467 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Name_0);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_normalizedTime()
extern "C" float AnimatorStateInfo_get_normalizedTime_m2720 (AnimatorStateInfo_t467 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_NormalizedTime_3);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorStateInfo::get_length()
extern "C" float AnimatorStateInfo_get_length_m2721 (AnimatorStateInfo_t467 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Length_4);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorStateInfo::get_tagHash()
extern "C" int32_t AnimatorStateInfo_get_tagHash_m2722 (AnimatorStateInfo_t467 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Tag_5);
		return L_0;
	}
}
// System.Boolean UnityEngine.AnimatorStateInfo::IsTag(System.String)
extern "C" bool AnimatorStateInfo_IsTag_m2723 (AnimatorStateInfo_t467 * __this, String_t* ___tag, const MethodInfo* method)
{
	{
		String_t* L_0 = ___tag;
		int32_t L_1 = Animator_StringToHash_m2734(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_Tag_5);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimatorStateInfo::get_loop()
extern "C" bool AnimatorStateInfo_get_loop_m2724 (AnimatorStateInfo_t467 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Loop_6);
		return ((((int32_t)((((int32_t)L_0) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsName(System.String)
extern "C" bool AnimatorTransitionInfo_IsName_m2725 (AnimatorTransitionInfo_t472 * __this, String_t* ___name, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m2734(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_Name_2);
		if ((((int32_t)L_1) == ((int32_t)L_2)))
		{
			goto IL_0021;
		}
	}
	{
		String_t* L_3 = ___name;
		int32_t L_4 = Animator_StringToHash_m2734(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		int32_t L_5 = (__this->___m_FullPath_0);
		G_B3_0 = ((((int32_t)L_4) == ((int32_t)L_5))? 1 : 0);
		goto IL_0022;
	}

IL_0021:
	{
		G_B3_0 = 1;
	}

IL_0022:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::IsUserName(System.String)
extern "C" bool AnimatorTransitionInfo_IsUserName_m2726 (AnimatorTransitionInfo_t472 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		int32_t L_1 = Animator_StringToHash_m2734(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_UserName_1);
		return ((((int32_t)L_1) == ((int32_t)L_2))? 1 : 0);
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_fullPathHash()
extern "C" int32_t AnimatorTransitionInfo_get_fullPathHash_m2727 (AnimatorTransitionInfo_t472 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_FullPath_0);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_nameHash()
extern "C" int32_t AnimatorTransitionInfo_get_nameHash_m2728 (AnimatorTransitionInfo_t472 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_Name_2);
		return L_0;
	}
}
// System.Int32 UnityEngine.AnimatorTransitionInfo::get_userNameHash()
extern "C" int32_t AnimatorTransitionInfo_get_userNameHash_m2729 (AnimatorTransitionInfo_t472 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_UserName_1);
		return L_0;
	}
}
// System.Single UnityEngine.AnimatorTransitionInfo::get_normalizedTime()
extern "C" float AnimatorTransitionInfo_get_normalizedTime_m2730 (AnimatorTransitionInfo_t472 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_NormalizedTime_3);
		return L_0;
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_anyState()
extern "C" bool AnimatorTransitionInfo_get_anyState_m2731 (AnimatorTransitionInfo_t472 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_AnyState_4);
		return L_0;
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_entry()
extern "C" bool AnimatorTransitionInfo_get_entry_m2732 (AnimatorTransitionInfo_t472 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_TransitionType_5);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)2))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// System.Boolean UnityEngine.AnimatorTransitionInfo::get_exit()
extern "C" bool AnimatorTransitionInfo_get_exit_m2733 (AnimatorTransitionInfo_t472 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_TransitionType_5);
		return ((((int32_t)((((int32_t)((int32_t)((int32_t)L_0&(int32_t)4))) == ((int32_t)0))? 1 : 0)) == ((int32_t)0))? 1 : 0);
	}
}
// Conversion methods for marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t472_marshal(const AnimatorTransitionInfo_t472& unmarshaled, AnimatorTransitionInfo_t472_marshaled& marshaled)
{
	marshaled.___m_FullPath_0 = unmarshaled.___m_FullPath_0;
	marshaled.___m_UserName_1 = unmarshaled.___m_UserName_1;
	marshaled.___m_Name_2 = unmarshaled.___m_Name_2;
	marshaled.___m_NormalizedTime_3 = unmarshaled.___m_NormalizedTime_3;
	marshaled.___m_AnyState_4 = unmarshaled.___m_AnyState_4;
	marshaled.___m_TransitionType_5 = unmarshaled.___m_TransitionType_5;
}
extern "C" void AnimatorTransitionInfo_t472_marshal_back(const AnimatorTransitionInfo_t472_marshaled& marshaled, AnimatorTransitionInfo_t472& unmarshaled)
{
	unmarshaled.___m_FullPath_0 = marshaled.___m_FullPath_0;
	unmarshaled.___m_UserName_1 = marshaled.___m_UserName_1;
	unmarshaled.___m_Name_2 = marshaled.___m_Name_2;
	unmarshaled.___m_NormalizedTime_3 = marshaled.___m_NormalizedTime_3;
	unmarshaled.___m_AnyState_4 = marshaled.___m_AnyState_4;
	unmarshaled.___m_TransitionType_5 = marshaled.___m_TransitionType_5;
}
// Conversion method for clean up from marshalling of: UnityEngine.AnimatorTransitionInfo
extern "C" void AnimatorTransitionInfo_t472_marshal_cleanup(AnimatorTransitionInfo_t472_marshaled& marshaled)
{
}
// System.Void UnityEngine.Animator::SetTrigger(System.String)
extern "C" void Animator_SetTrigger_m1895 (Animator_t289 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Animator_SetTriggerString_m2735(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Animator::ResetTrigger(System.String)
extern "C" void Animator_ResetTrigger_m1894 (Animator_t289 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		Animator_ResetTriggerString_m2736(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.RuntimeAnimatorController UnityEngine.Animator::get_runtimeAnimatorController()
extern "C" RuntimeAnimatorController_t340 * Animator_get_runtimeAnimatorController_m1893 (Animator_t289 * __this, const MethodInfo* method)
{
	typedef RuntimeAnimatorController_t340 * (*Animator_get_runtimeAnimatorController_m1893_ftn) (Animator_t289 *);
	static Animator_get_runtimeAnimatorController_m1893_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_get_runtimeAnimatorController_m1893_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::get_runtimeAnimatorController()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Animator::StringToHash(System.String)
extern "C" int32_t Animator_StringToHash_m2734 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	typedef int32_t (*Animator_StringToHash_m2734_ftn) (String_t*);
	static Animator_StringToHash_m2734_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_StringToHash_m2734_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::StringToHash(System.String)");
	return _il2cpp_icall_func(___name);
}
// System.Void UnityEngine.Animator::SetTriggerString(System.String)
extern "C" void Animator_SetTriggerString_m2735 (Animator_t289 * __this, String_t* ___name, const MethodInfo* method)
{
	typedef void (*Animator_SetTriggerString_m2735_ftn) (Animator_t289 *, String_t*);
	static Animator_SetTriggerString_m2735_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_SetTriggerString_m2735_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::SetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name);
}
// System.Void UnityEngine.Animator::ResetTriggerString(System.String)
extern "C" void Animator_ResetTriggerString_m2736 (Animator_t289 * __this, String_t* ___name, const MethodInfo* method)
{
	typedef void (*Animator_ResetTriggerString_m2736_ftn) (Animator_t289 *, String_t*);
	static Animator_ResetTriggerString_m2736_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Animator_ResetTriggerString_m2736_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Animator::ResetTriggerString(System.String)");
	_il2cpp_icall_func(__this, ___name);
}
// Conversion methods for marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t473_marshal(const SkeletonBone_t473& unmarshaled, SkeletonBone_t473_marshaled& marshaled)
{
	marshaled.___name_0 = il2cpp_codegen_marshal_string(unmarshaled.___name_0);
	marshaled.___position_1 = unmarshaled.___position_1;
	marshaled.___rotation_2 = unmarshaled.___rotation_2;
	marshaled.___scale_3 = unmarshaled.___scale_3;
	marshaled.___transformModified_4 = unmarshaled.___transformModified_4;
}
extern "C" void SkeletonBone_t473_marshal_back(const SkeletonBone_t473_marshaled& marshaled, SkeletonBone_t473& unmarshaled)
{
	unmarshaled.___name_0 = il2cpp_codegen_marshal_string_result(marshaled.___name_0);
	unmarshaled.___position_1 = marshaled.___position_1;
	unmarshaled.___rotation_2 = marshaled.___rotation_2;
	unmarshaled.___scale_3 = marshaled.___scale_3;
	unmarshaled.___transformModified_4 = marshaled.___transformModified_4;
}
// Conversion method for clean up from marshalling of: UnityEngine.SkeletonBone
extern "C" void SkeletonBone_t473_marshal_cleanup(SkeletonBone_t473_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___name_0);
	marshaled.___name_0 = NULL;
}
// System.String UnityEngine.HumanBone::get_boneName()
extern "C" String_t* HumanBone_get_boneName_m2737 (HumanBone_t475 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_BoneName_0);
		return L_0;
	}
}
// System.Void UnityEngine.HumanBone::set_boneName(System.String)
extern "C" void HumanBone_set_boneName_m2738 (HumanBone_t475 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_BoneName_0 = L_0;
		return;
	}
}
// System.String UnityEngine.HumanBone::get_humanName()
extern "C" String_t* HumanBone_get_humanName_m2739 (HumanBone_t475 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___m_HumanName_1);
		return L_0;
	}
}
// System.Void UnityEngine.HumanBone::set_humanName(System.String)
extern "C" void HumanBone_set_humanName_m2740 (HumanBone_t475 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___m_HumanName_1 = L_0;
		return;
	}
}
// Conversion methods for marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t475_marshal(const HumanBone_t475& unmarshaled, HumanBone_t475_marshaled& marshaled)
{
	marshaled.___m_BoneName_0 = il2cpp_codegen_marshal_string(unmarshaled.___m_BoneName_0);
	marshaled.___m_HumanName_1 = il2cpp_codegen_marshal_string(unmarshaled.___m_HumanName_1);
	marshaled.___limit_2 = unmarshaled.___limit_2;
}
extern "C" void HumanBone_t475_marshal_back(const HumanBone_t475_marshaled& marshaled, HumanBone_t475& unmarshaled)
{
	unmarshaled.___m_BoneName_0 = il2cpp_codegen_marshal_string_result(marshaled.___m_BoneName_0);
	unmarshaled.___m_HumanName_1 = il2cpp_codegen_marshal_string_result(marshaled.___m_HumanName_1);
	unmarshaled.___limit_2 = marshaled.___limit_2;
}
// Conversion method for clean up from marshalling of: UnityEngine.HumanBone
extern "C" void HumanBone_t475_marshal_cleanup(HumanBone_t475_marshaled& marshaled)
{
	il2cpp_codegen_marshal_free(marshaled.___m_BoneName_0);
	marshaled.___m_BoneName_0 = NULL;
	il2cpp_codegen_marshal_free(marshaled.___m_HumanName_1);
	marshaled.___m_HumanName_1 = NULL;
}
// System.Int32 UnityEngine.CharacterInfo::get_advance()
extern "C" int32_t CharacterInfo_get_advance_m2741 (CharacterInfo_t478 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___width_3);
		return (((int32_t)L_0));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_glyphWidth()
extern "C" int32_t CharacterInfo_get_glyphWidth_m2742 (CharacterInfo_t478 * __this, const MethodInfo* method)
{
	{
		Rect_t184 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_width_m1635(L_0, /*hidden argument*/NULL);
		return (((int32_t)L_1));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_glyphHeight()
extern "C" int32_t CharacterInfo_get_glyphHeight_m2743 (CharacterInfo_t478 * __this, const MethodInfo* method)
{
	{
		Rect_t184 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_height_m1636(L_0, /*hidden argument*/NULL);
		return (((int32_t)((-L_1))));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_bearing()
extern "C" int32_t CharacterInfo_get_bearing_m2744 (CharacterInfo_t478 * __this, const MethodInfo* method)
{
	{
		Rect_t184 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m1640(L_0, /*hidden argument*/NULL);
		return (((int32_t)L_1));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_minY()
extern "C" int32_t CharacterInfo_get_minY_m2745 (CharacterInfo_t478 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___ascent_7);
		Rect_t184 * L_1 = &(__this->___vert_2);
		float L_2 = Rect_get_y_m1641(L_1, /*hidden argument*/NULL);
		Rect_t184 * L_3 = &(__this->___vert_2);
		float L_4 = Rect_get_height_m1636(L_3, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0+(int32_t)(((int32_t)((float)((float)L_2+(float)L_4))))));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_maxY()
extern "C" int32_t CharacterInfo_get_maxY_m2746 (CharacterInfo_t478 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___ascent_7);
		Rect_t184 * L_1 = &(__this->___vert_2);
		float L_2 = Rect_get_y_m1641(L_1, /*hidden argument*/NULL);
		return ((int32_t)((int32_t)L_0+(int32_t)(((int32_t)L_2))));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_minX()
extern "C" int32_t CharacterInfo_get_minX_m2747 (CharacterInfo_t478 * __this, const MethodInfo* method)
{
	{
		Rect_t184 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m1640(L_0, /*hidden argument*/NULL);
		return (((int32_t)L_1));
	}
}
// System.Int32 UnityEngine.CharacterInfo::get_maxX()
extern "C" int32_t CharacterInfo_get_maxX_m2748 (CharacterInfo_t478 * __this, const MethodInfo* method)
{
	{
		Rect_t184 * L_0 = &(__this->___vert_2);
		float L_1 = Rect_get_x_m1640(L_0, /*hidden argument*/NULL);
		Rect_t184 * L_2 = &(__this->___vert_2);
		float L_3 = Rect_get_width_m1635(L_2, /*hidden argument*/NULL);
		return (((int32_t)((float)((float)L_1+(float)L_3))));
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeftUnFlipped()
extern "C" Vector2_t59  CharacterInfo_get_uvBottomLeftUnFlipped_m2749 (CharacterInfo_t478 * __this, const MethodInfo* method)
{
	{
		Rect_t184 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m1640(L_0, /*hidden argument*/NULL);
		Rect_t184 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_y_m1641(L_2, /*hidden argument*/NULL);
		Vector2_t59  L_4 = {0};
		Vector2__ctor_m231(&L_4, L_1, L_3, /*hidden argument*/NULL);
		return L_4;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRightUnFlipped()
extern "C" Vector2_t59  CharacterInfo_get_uvBottomRightUnFlipped_m2750 (CharacterInfo_t478 * __this, const MethodInfo* method)
{
	{
		Rect_t184 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m1640(L_0, /*hidden argument*/NULL);
		Rect_t184 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_width_m1635(L_2, /*hidden argument*/NULL);
		Rect_t184 * L_4 = &(__this->___uv_1);
		float L_5 = Rect_get_y_m1641(L_4, /*hidden argument*/NULL);
		Vector2_t59  L_6 = {0};
		Vector2__ctor_m231(&L_6, ((float)((float)L_1+(float)L_3)), L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRightUnFlipped()
extern "C" Vector2_t59  CharacterInfo_get_uvTopRightUnFlipped_m2751 (CharacterInfo_t478 * __this, const MethodInfo* method)
{
	{
		Rect_t184 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m1640(L_0, /*hidden argument*/NULL);
		Rect_t184 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_width_m1635(L_2, /*hidden argument*/NULL);
		Rect_t184 * L_4 = &(__this->___uv_1);
		float L_5 = Rect_get_y_m1641(L_4, /*hidden argument*/NULL);
		Rect_t184 * L_6 = &(__this->___uv_1);
		float L_7 = Rect_get_height_m1636(L_6, /*hidden argument*/NULL);
		Vector2_t59  L_8 = {0};
		Vector2__ctor_m231(&L_8, ((float)((float)L_1+(float)L_3)), ((float)((float)L_5+(float)L_7)), /*hidden argument*/NULL);
		return L_8;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeftUnFlipped()
extern "C" Vector2_t59  CharacterInfo_get_uvTopLeftUnFlipped_m2752 (CharacterInfo_t478 * __this, const MethodInfo* method)
{
	{
		Rect_t184 * L_0 = &(__this->___uv_1);
		float L_1 = Rect_get_x_m1640(L_0, /*hidden argument*/NULL);
		Rect_t184 * L_2 = &(__this->___uv_1);
		float L_3 = Rect_get_y_m1641(L_2, /*hidden argument*/NULL);
		Rect_t184 * L_4 = &(__this->___uv_1);
		float L_5 = Rect_get_height_m1636(L_4, /*hidden argument*/NULL);
		Vector2_t59  L_6 = {0};
		Vector2__ctor_m231(&L_6, L_1, ((float)((float)L_3+(float)L_5)), /*hidden argument*/NULL);
		return L_6;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomLeft()
extern "C" Vector2_t59  CharacterInfo_get_uvBottomLeft_m2753 (CharacterInfo_t478 * __this, const MethodInfo* method)
{
	Vector2_t59  G_B3_0 = {0};
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t59  L_1 = CharacterInfo_get_uvBottomLeftUnFlipped_m2749(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Vector2_t59  L_2 = CharacterInfo_get_uvBottomLeftUnFlipped_m2749(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvBottomRight()
extern "C" Vector2_t59  CharacterInfo_get_uvBottomRight_m2754 (CharacterInfo_t478 * __this, const MethodInfo* method)
{
	Vector2_t59  G_B3_0 = {0};
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t59  L_1 = CharacterInfo_get_uvTopLeftUnFlipped_m2752(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Vector2_t59  L_2 = CharacterInfo_get_uvBottomRightUnFlipped_m2750(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopRight()
extern "C" Vector2_t59  CharacterInfo_get_uvTopRight_m2755 (CharacterInfo_t478 * __this, const MethodInfo* method)
{
	Vector2_t59  G_B3_0 = {0};
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t59  L_1 = CharacterInfo_get_uvTopRightUnFlipped_m2751(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Vector2_t59  L_2 = CharacterInfo_get_uvTopRightUnFlipped_m2751(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// UnityEngine.Vector2 UnityEngine.CharacterInfo::get_uvTopLeft()
extern "C" Vector2_t59  CharacterInfo_get_uvTopLeft_m2756 (CharacterInfo_t478 * __this, const MethodInfo* method)
{
	Vector2_t59  G_B3_0 = {0};
	{
		bool L_0 = (__this->___flipped_6);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		Vector2_t59  L_1 = CharacterInfo_get_uvBottomRightUnFlipped_m2750(__this, /*hidden argument*/NULL);
		G_B3_0 = L_1;
		goto IL_001c;
	}

IL_0016:
	{
		Vector2_t59  L_2 = CharacterInfo_get_uvTopLeftUnFlipped_m2752(__this, /*hidden argument*/NULL);
		G_B3_0 = L_2;
	}

IL_001c:
	{
		return G_B3_0;
	}
}
// Conversion methods for marshalling of: UnityEngine.CharacterInfo
extern "C" void CharacterInfo_t478_marshal(const CharacterInfo_t478& unmarshaled, CharacterInfo_t478_marshaled& marshaled)
{
	marshaled.___index_0 = unmarshaled.___index_0;
	marshaled.___uv_1 = unmarshaled.___uv_1;
	marshaled.___vert_2 = unmarshaled.___vert_2;
	marshaled.___width_3 = unmarshaled.___width_3;
	marshaled.___size_4 = unmarshaled.___size_4;
	marshaled.___style_5 = unmarshaled.___style_5;
	marshaled.___flipped_6 = unmarshaled.___flipped_6;
	marshaled.___ascent_7 = unmarshaled.___ascent_7;
}
extern "C" void CharacterInfo_t478_marshal_back(const CharacterInfo_t478_marshaled& marshaled, CharacterInfo_t478& unmarshaled)
{
	unmarshaled.___index_0 = marshaled.___index_0;
	unmarshaled.___uv_1 = marshaled.___uv_1;
	unmarshaled.___vert_2 = marshaled.___vert_2;
	unmarshaled.___width_3 = marshaled.___width_3;
	unmarshaled.___size_4 = marshaled.___size_4;
	unmarshaled.___style_5 = marshaled.___style_5;
	unmarshaled.___flipped_6 = marshaled.___flipped_6;
	unmarshaled.___ascent_7 = marshaled.___ascent_7;
}
// Conversion method for clean up from marshalling of: UnityEngine.CharacterInfo
extern "C" void CharacterInfo_t478_marshal_cleanup(CharacterInfo_t478_marshaled& marshaled)
{
}
// System.Void UnityEngine.Font/FontTextureRebuildCallback::.ctor(System.Object,System.IntPtr)
extern "C" void FontTextureRebuildCallback__ctor_m2757 (FontTextureRebuildCallback_t479 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Font/FontTextureRebuildCallback::Invoke()
extern "C" void FontTextureRebuildCallback_Invoke_m2758 (FontTextureRebuildCallback_t479 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		FontTextureRebuildCallback_Invoke_m2758((FontTextureRebuildCallback_t479 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_FontTextureRebuildCallback_t479(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Font/FontTextureRebuildCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * FontTextureRebuildCallback_BeginInvoke_m2759 (FontTextureRebuildCallback_t479 * __this, AsyncCallback_t181 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Font/FontTextureRebuildCallback::EndInvoke(System.IAsyncResult)
extern "C" void FontTextureRebuildCallback_EndInvoke_m2760 (FontTextureRebuildCallback_t479 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.Font::add_textureRebuilt(System.Action`1<UnityEngine.Font>)
extern TypeInfo* Font_t142_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t313_il2cpp_TypeInfo_var;
extern "C" void Font_add_textureRebuilt_m1617 (Object_t * __this /* static, unused */, Action_1_t313 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t142_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(133);
		Action_1_t313_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(137);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t313 * L_0 = ((Font_t142_StaticFields*)Font_t142_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2;
		Action_1_t313 * L_1 = ___value;
		Delegate_t320 * L_2 = Delegate_Combine_m1656(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Font_t142_StaticFields*)Font_t142_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2 = ((Action_1_t313 *)CastclassSealed(L_2, Action_1_t313_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Font::remove_textureRebuilt(System.Action`1<UnityEngine.Font>)
extern TypeInfo* Font_t142_il2cpp_TypeInfo_var;
extern TypeInfo* Action_1_t313_il2cpp_TypeInfo_var;
extern "C" void Font_remove_textureRebuilt_m2761 (Object_t * __this /* static, unused */, Action_1_t313 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t142_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(133);
		Action_1_t313_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(137);
		s_Il2CppMethodIntialized = true;
	}
	{
		Action_1_t313 * L_0 = ((Font_t142_StaticFields*)Font_t142_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2;
		Action_1_t313 * L_1 = ___value;
		Delegate_t320 * L_2 = Delegate_Remove_m1657(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Font_t142_StaticFields*)Font_t142_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2 = ((Action_1_t313 *)CastclassSealed(L_2, Action_1_t313_il2cpp_TypeInfo_var));
		return;
	}
}
// UnityEngine.Material UnityEngine.Font::get_material()
extern "C" Material_t146 * Font_get_material_m1905 (Font_t142 * __this, const MethodInfo* method)
{
	typedef Material_t146 * (*Font_get_material_m1905_ftn) (Font_t142 *);
	static Font_get_material_m1905_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_material_m1905_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_material()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Font::HasCharacter(System.Char)
extern "C" bool Font_HasCharacter_m1778 (Font_t142 * __this, uint16_t ___c, const MethodInfo* method)
{
	typedef bool (*Font_HasCharacter_m1778_ftn) (Font_t142 *, uint16_t);
	static Font_HasCharacter_m1778_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_HasCharacter_m1778_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::HasCharacter(System.Char)");
	return _il2cpp_icall_func(__this, ___c);
}
// System.Void UnityEngine.Font::InvokeTextureRebuilt_Internal(UnityEngine.Font)
extern TypeInfo* Font_t142_il2cpp_TypeInfo_var;
extern const MethodInfo* Action_1_Invoke_m3291_MethodInfo_var;
extern "C" void Font_InvokeTextureRebuilt_Internal_m2762 (Object_t * __this /* static, unused */, Font_t142 * ___font, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Font_t142_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(133);
		Action_1_Invoke_m3291_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483918);
		s_Il2CppMethodIntialized = true;
	}
	Action_1_t313 * V_0 = {0};
	{
		Action_1_t313 * L_0 = ((Font_t142_StaticFields*)Font_t142_il2cpp_TypeInfo_var->static_fields)->___textureRebuilt_2;
		V_0 = L_0;
		Action_1_t313 * L_1 = V_0;
		if (!L_1)
		{
			goto IL_0013;
		}
	}
	{
		Action_1_t313 * L_2 = V_0;
		Font_t142 * L_3 = ___font;
		NullCheck(L_2);
		Action_1_Invoke_m3291(L_2, L_3, /*hidden argument*/Action_1_Invoke_m3291_MethodInfo_var);
	}

IL_0013:
	{
		Font_t142 * L_4 = ___font;
		NullCheck(L_4);
		FontTextureRebuildCallback_t479 * L_5 = (L_4->___m_FontTextureRebuildCallback_3);
		if (!L_5)
		{
			goto IL_0029;
		}
	}
	{
		Font_t142 * L_6 = ___font;
		NullCheck(L_6);
		FontTextureRebuildCallback_t479 * L_7 = (L_6->___m_FontTextureRebuildCallback_3);
		NullCheck(L_7);
		FontTextureRebuildCallback_Invoke_m2758(L_7, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Boolean UnityEngine.Font::get_dynamic()
extern "C" bool Font_get_dynamic_m1908 (Font_t142 * __this, const MethodInfo* method)
{
	typedef bool (*Font_get_dynamic_m1908_ftn) (Font_t142 *);
	static Font_get_dynamic_m1908_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_dynamic_m1908_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_dynamic()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Font::get_fontSize()
extern "C" int32_t Font_get_fontSize_m1910 (Font_t142 * __this, const MethodInfo* method)
{
	typedef int32_t (*Font_get_fontSize_m1910_ftn) (Font_t142 *);
	static Font_get_fontSize_m1910_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Font_get_fontSize_m1910_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Font::get_fontSize()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::.ctor()
extern "C" void TextGenerator__ctor_m1734 (TextGenerator_t186 * __this, const MethodInfo* method)
{
	{
		TextGenerator__ctor_m1903(__this, ((int32_t)50), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::.ctor(System.Int32)
extern TypeInfo* List_1_t187_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t480_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t481_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3292_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3293_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m3294_MethodInfo_var;
extern "C" void TextGenerator__ctor_m1903 (TextGenerator_t186 * __this, int32_t ___initialCapacity, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t187_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(140);
		List_1_t480_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(340);
		List_1_t481_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(341);
		List_1__ctor_m3292_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483919);
		List_1__ctor_m3293_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483920);
		List_1__ctor_m3294_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483921);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		int32_t L_0 = ___initialCapacity;
		List_1_t187 * L_1 = (List_1_t187 *)il2cpp_codegen_object_new (List_1_t187_il2cpp_TypeInfo_var);
		List_1__ctor_m3292(L_1, ((int32_t)((int32_t)((int32_t)((int32_t)L_0+(int32_t)1))*(int32_t)4)), /*hidden argument*/List_1__ctor_m3292_MethodInfo_var);
		__this->___m_Verts_5 = L_1;
		int32_t L_2 = ___initialCapacity;
		List_1_t480 * L_3 = (List_1_t480 *)il2cpp_codegen_object_new (List_1_t480_il2cpp_TypeInfo_var);
		List_1__ctor_m3293(L_3, ((int32_t)((int32_t)L_2+(int32_t)1)), /*hidden argument*/List_1__ctor_m3293_MethodInfo_var);
		__this->___m_Characters_6 = L_3;
		List_1_t481 * L_4 = (List_1_t481 *)il2cpp_codegen_object_new (List_1_t481_il2cpp_TypeInfo_var);
		List_1__ctor_m3294(L_4, ((int32_t)20), /*hidden argument*/List_1__ctor_m3294_MethodInfo_var);
		__this->___m_Lines_7 = L_4;
		TextGenerator_Init_m2764(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::System.IDisposable.Dispose()
extern "C" void TextGenerator_System_IDisposable_Dispose_m2763 (TextGenerator_t186 * __this, const MethodInfo* method)
{
	{
		TextGenerator_Dispose_cpp_m2765(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::Init()
extern "C" void TextGenerator_Init_m2764 (TextGenerator_t186 * __this, const MethodInfo* method)
{
	typedef void (*TextGenerator_Init_m2764_ftn) (TextGenerator_t186 *);
	static TextGenerator_Init_m2764_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Init_m2764_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Init()");
	_il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::Dispose_cpp()
extern "C" void TextGenerator_Dispose_cpp_m2765 (TextGenerator_t186 * __this, const MethodInfo* method)
{
	typedef void (*TextGenerator_Dispose_cpp_m2765_ftn) (TextGenerator_t186 *);
	static TextGenerator_Dispose_cpp_m2765_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_Dispose_cpp_m2765_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::Dispose_cpp()");
	_il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,UnityEngine.VerticalWrapMode,UnityEngine.HorizontalWrapMode,System.Boolean,UnityEngine.TextAnchor,UnityEngine.Vector2,UnityEngine.Vector2,System.Boolean)
extern "C" bool TextGenerator_Populate_Internal_m2766 (TextGenerator_t186 * __this, String_t* ___str, Font_t142 * ___font, Color_t128  ___color, int32_t ___fontSize, float ___scaleFactor, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, Vector2_t59  ___extents, Vector2_t59  ___pivot, bool ___generateOutOfBounds, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		Font_t142 * L_1 = ___font;
		Color_t128  L_2 = ___color;
		int32_t L_3 = ___fontSize;
		float L_4 = ___scaleFactor;
		float L_5 = ___lineSpacing;
		int32_t L_6 = ___style;
		bool L_7 = ___richText;
		bool L_8 = ___resizeTextForBestFit;
		int32_t L_9 = ___resizeTextMinSize;
		int32_t L_10 = ___resizeTextMaxSize;
		int32_t L_11 = ___verticalOverFlow;
		int32_t L_12 = ___horizontalOverflow;
		bool L_13 = ___updateBounds;
		int32_t L_14 = ___anchor;
		float L_15 = ((&___extents)->___x_1);
		float L_16 = ((&___extents)->___y_2);
		float L_17 = ((&___pivot)->___x_1);
		float L_18 = ((&___pivot)->___y_2);
		bool L_19 = ___generateOutOfBounds;
		bool L_20 = TextGenerator_Populate_Internal_cpp_m2767(__this, L_0, L_1, L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, /*hidden argument*/NULL);
		return L_20;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate_Internal_cpp(System.String,UnityEngine.Font,UnityEngine.Color,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern "C" bool TextGenerator_Populate_Internal_cpp_m2767 (TextGenerator_t186 * __this, String_t* ___str, Font_t142 * ___font, Color_t128  ___color, int32_t ___fontSize, float ___scaleFactor, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, float ___extentsX, float ___extentsY, float ___pivotX, float ___pivotY, bool ___generateOutOfBounds, const MethodInfo* method)
{
	{
		String_t* L_0 = ___str;
		Font_t142 * L_1 = ___font;
		int32_t L_2 = ___fontSize;
		float L_3 = ___scaleFactor;
		float L_4 = ___lineSpacing;
		int32_t L_5 = ___style;
		bool L_6 = ___richText;
		bool L_7 = ___resizeTextForBestFit;
		int32_t L_8 = ___resizeTextMinSize;
		int32_t L_9 = ___resizeTextMaxSize;
		int32_t L_10 = ___verticalOverFlow;
		int32_t L_11 = ___horizontalOverflow;
		bool L_12 = ___updateBounds;
		int32_t L_13 = ___anchor;
		float L_14 = ___extentsX;
		float L_15 = ___extentsY;
		float L_16 = ___pivotX;
		float L_17 = ___pivotY;
		bool L_18 = ___generateOutOfBounds;
		bool L_19 = TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m2768(NULL /*static, unused*/, __this, L_0, L_1, (&___color), L_2, L_3, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, /*hidden argument*/NULL);
		return L_19;
	}
}
// System.Boolean UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)
extern "C" bool TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m2768 (Object_t * __this /* static, unused */, TextGenerator_t186 * ___self, String_t* ___str, Font_t142 * ___font, Color_t128 * ___color, int32_t ___fontSize, float ___scaleFactor, float ___lineSpacing, int32_t ___style, bool ___richText, bool ___resizeTextForBestFit, int32_t ___resizeTextMinSize, int32_t ___resizeTextMaxSize, int32_t ___verticalOverFlow, int32_t ___horizontalOverflow, bool ___updateBounds, int32_t ___anchor, float ___extentsX, float ___extentsY, float ___pivotX, float ___pivotY, bool ___generateOutOfBounds, const MethodInfo* method)
{
	typedef bool (*TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m2768_ftn) (TextGenerator_t186 *, String_t*, Font_t142 *, Color_t128 *, int32_t, float, float, int32_t, bool, bool, int32_t, int32_t, int32_t, int32_t, bool, int32_t, float, float, float, float, bool);
	static TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m2768_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m2768_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_CALL_Populate_Internal_cpp(UnityEngine.TextGenerator,System.String,UnityEngine.Font,UnityEngine.Color&,System.Int32,System.Single,System.Single,UnityEngine.FontStyle,System.Boolean,System.Boolean,System.Int32,System.Int32,System.Int32,System.Int32,System.Boolean,UnityEngine.TextAnchor,System.Single,System.Single,System.Single,System.Single,System.Boolean)");
	return _il2cpp_icall_func(___self, ___str, ___font, ___color, ___fontSize, ___scaleFactor, ___lineSpacing, ___style, ___richText, ___resizeTextForBestFit, ___resizeTextMinSize, ___resizeTextMaxSize, ___verticalOverFlow, ___horizontalOverflow, ___updateBounds, ___anchor, ___extentsX, ___extentsY, ___pivotX, ___pivotY, ___generateOutOfBounds);
}
// UnityEngine.Rect UnityEngine.TextGenerator::get_rectExtents()
extern "C" Rect_t184  TextGenerator_get_rectExtents_m1794 (TextGenerator_t186 * __this, const MethodInfo* method)
{
	Rect_t184  V_0 = {0};
	{
		TextGenerator_INTERNAL_get_rectExtents_m2769(__this, (&V_0), /*hidden argument*/NULL);
		Rect_t184  L_0 = V_0;
		return L_0;
	}
}
// System.Void UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)
extern "C" void TextGenerator_INTERNAL_get_rectExtents_m2769 (TextGenerator_t186 * __this, Rect_t184 * ___value, const MethodInfo* method)
{
	typedef void (*TextGenerator_INTERNAL_get_rectExtents_m2769_ftn) (TextGenerator_t186 *, Rect_t184 *);
	static TextGenerator_INTERNAL_get_rectExtents_m2769_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_INTERNAL_get_rectExtents_m2769_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::INTERNAL_get_rectExtents(UnityEngine.Rect&)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Int32 UnityEngine.TextGenerator::get_vertexCount()
extern "C" int32_t TextGenerator_get_vertexCount_m2770 (TextGenerator_t186 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_vertexCount_m2770_ftn) (TextGenerator_t186 *);
	static TextGenerator_get_vertexCount_m2770_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_vertexCount_m2770_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_vertexCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::GetVerticesInternal(System.Object)
extern "C" void TextGenerator_GetVerticesInternal_m2771 (TextGenerator_t186 * __this, Object_t * ___vertices, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetVerticesInternal_m2771_ftn) (TextGenerator_t186 *, Object_t *);
	static TextGenerator_GetVerticesInternal_m2771_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetVerticesInternal_m2771_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetVerticesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___vertices);
}
// UnityEngine.UIVertex[] UnityEngine.TextGenerator::GetVerticesArray()
extern "C" UIVertexU5BU5D_t185* TextGenerator_GetVerticesArray_m2772 (TextGenerator_t186 * __this, const MethodInfo* method)
{
	typedef UIVertexU5BU5D_t185* (*TextGenerator_GetVerticesArray_m2772_ftn) (TextGenerator_t186 *);
	static TextGenerator_GetVerticesArray_m2772_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetVerticesArray_m2772_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetVerticesArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_characterCount()
extern "C" int32_t TextGenerator_get_characterCount_m2773 (TextGenerator_t186 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_characterCount_m2773_ftn) (TextGenerator_t186 *);
	static TextGenerator_get_characterCount_m2773_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_characterCount_m2773_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_characterCount()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_characterCountVisible()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t57_il2cpp_TypeInfo_var;
extern "C" int32_t TextGenerator_get_characterCountVisible_m1772 (TextGenerator_t186 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Mathf_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		String_t* L_0 = (__this->___m_LastString_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_IsNullOrEmpty_m1792(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0016;
		}
	}
	{
		G_B3_0 = 0;
		goto IL_0036;
	}

IL_0016:
	{
		String_t* L_2 = (__this->___m_LastString_1);
		NullCheck(L_2);
		int32_t L_3 = String_get_Length_m1739(L_2, /*hidden argument*/NULL);
		int32_t L_4 = TextGenerator_get_vertexCount_m2770(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t57_il2cpp_TypeInfo_var);
		int32_t L_5 = Mathf_Max_m1783(NULL /*static, unused*/, 0, ((int32_t)((int32_t)((int32_t)((int32_t)L_4-(int32_t)4))/(int32_t)4)), /*hidden argument*/NULL);
		int32_t L_6 = Mathf_Min_m1785(NULL /*static, unused*/, L_3, L_5, /*hidden argument*/NULL);
		G_B3_0 = L_6;
	}

IL_0036:
	{
		return G_B3_0;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharactersInternal(System.Object)
extern "C" void TextGenerator_GetCharactersInternal_m2774 (TextGenerator_t186 * __this, Object_t * ___characters, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetCharactersInternal_m2774_ftn) (TextGenerator_t186 *, Object_t *);
	static TextGenerator_GetCharactersInternal_m2774_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetCharactersInternal_m2774_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetCharactersInternal(System.Object)");
	_il2cpp_icall_func(__this, ___characters);
}
// UnityEngine.UICharInfo[] UnityEngine.TextGenerator::GetCharactersArray()
extern "C" UICharInfoU5BU5D_t611* TextGenerator_GetCharactersArray_m2775 (TextGenerator_t186 * __this, const MethodInfo* method)
{
	typedef UICharInfoU5BU5D_t611* (*TextGenerator_GetCharactersArray_m2775_ftn) (TextGenerator_t186 *);
	static TextGenerator_GetCharactersArray_m2775_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetCharactersArray_m2775_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetCharactersArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_lineCount()
extern "C" int32_t TextGenerator_get_lineCount_m1771 (TextGenerator_t186 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_lineCount_m1771_ftn) (TextGenerator_t186 *);
	static TextGenerator_get_lineCount_m1771_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_lineCount_m1771_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_lineCount()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::GetLinesInternal(System.Object)
extern "C" void TextGenerator_GetLinesInternal_m2776 (TextGenerator_t186 * __this, Object_t * ___lines, const MethodInfo* method)
{
	typedef void (*TextGenerator_GetLinesInternal_m2776_ftn) (TextGenerator_t186 *, Object_t *);
	static TextGenerator_GetLinesInternal_m2776_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetLinesInternal_m2776_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetLinesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___lines);
}
// UnityEngine.UILineInfo[] UnityEngine.TextGenerator::GetLinesArray()
extern "C" UILineInfoU5BU5D_t612* TextGenerator_GetLinesArray_m2777 (TextGenerator_t186 * __this, const MethodInfo* method)
{
	typedef UILineInfoU5BU5D_t612* (*TextGenerator_GetLinesArray_m2777_ftn) (TextGenerator_t186 *);
	static TextGenerator_GetLinesArray_m2777_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_GetLinesArray_m2777_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::GetLinesArray()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.TextGenerator::get_fontSizeUsedForBestFit()
extern "C" int32_t TextGenerator_get_fontSizeUsedForBestFit_m1818 (TextGenerator_t186 * __this, const MethodInfo* method)
{
	typedef int32_t (*TextGenerator_get_fontSizeUsedForBestFit_m1818_ftn) (TextGenerator_t186 *);
	static TextGenerator_get_fontSizeUsedForBestFit_m1818_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (TextGenerator_get_fontSizeUsedForBestFit_m1818_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.TextGenerator::get_fontSizeUsedForBestFit()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.TextGenerator::Finalize()
extern TypeInfo* IDisposable_t303_il2cpp_TypeInfo_var;
extern "C" void TextGenerator_Finalize_m2778 (TextGenerator_t186 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDisposable_t303_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	Exception_t301 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t301 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);

IL_0000:
	try
	{ // begin try (depth: 1)
		InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t303_il2cpp_TypeInfo_var, __this);
		IL2CPP_LEAVE(0x12, FINALLY_000b);
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t301 *)e.ex;
		goto FINALLY_000b;
	}

FINALLY_000b:
	{ // begin finally (depth: 1)
		Object_Finalize_m3230(__this, /*hidden argument*/NULL);
		IL2CPP_END_FINALLY(11)
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(11)
	{
		IL2CPP_JUMP_TBL(0x12, IL_0012)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t301 *)
	}

IL_0012:
	{
		return;
	}
}
// UnityEngine.TextGenerationSettings UnityEngine.TextGenerator::ValidatedSettings(UnityEngine.TextGenerationSettings)
extern Il2CppCodeGenString* _stringLiteral271;
extern Il2CppCodeGenString* _stringLiteral272;
extern "C" TextGenerationSettings_t290  TextGenerator_ValidatedSettings_m2779 (TextGenerator_t186 * __this, TextGenerationSettings_t290  ___settings, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral271 = il2cpp_codegen_string_literal_from_index(271);
		_stringLiteral272 = il2cpp_codegen_string_literal_from_index(272);
		s_Il2CppMethodIntialized = true;
	}
	{
		Font_t142 * L_0 = ((&___settings)->___font_0);
		bool L_1 = Object_op_Inequality_m1472(NULL /*static, unused*/, L_0, (Object_t54 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0025;
		}
	}
	{
		Font_t142 * L_2 = ((&___settings)->___font_0);
		NullCheck(L_2);
		bool L_3 = Font_get_dynamic_m1908(L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0025;
		}
	}
	{
		TextGenerationSettings_t290  L_4 = ___settings;
		return L_4;
	}

IL_0025:
	{
		int32_t L_5 = ((&___settings)->___fontSize_2);
		if (L_5)
		{
			goto IL_003d;
		}
	}
	{
		int32_t L_6 = ((&___settings)->___fontStyle_6);
		if (!L_6)
		{
			goto IL_0057;
		}
	}

IL_003d:
	{
		Debug_LogWarning_m2547(NULL /*static, unused*/, _stringLiteral271, /*hidden argument*/NULL);
		(&___settings)->___fontSize_2 = 0;
		(&___settings)->___fontStyle_6 = 0;
	}

IL_0057:
	{
		bool L_7 = ((&___settings)->___resizeTextForBestFit_8);
		if (!L_7)
		{
			goto IL_0075;
		}
	}
	{
		Debug_LogWarning_m2547(NULL /*static, unused*/, _stringLiteral272, /*hidden argument*/NULL);
		(&___settings)->___resizeTextForBestFit_8 = 0;
	}

IL_0075:
	{
		TextGenerationSettings_t290  L_8 = ___settings;
		return L_8;
	}
}
// System.Void UnityEngine.TextGenerator::Invalidate()
extern "C" void TextGenerator_Invalidate_m1907 (TextGenerator_t186 * __this, const MethodInfo* method)
{
	{
		__this->___m_HasGenerated_3 = 0;
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetCharacters(System.Collections.Generic.List`1<UnityEngine.UICharInfo>)
extern "C" void TextGenerator_GetCharacters_m2780 (TextGenerator_t186 * __this, List_1_t480 * ___characters, const MethodInfo* method)
{
	{
		List_1_t480 * L_0 = ___characters;
		TextGenerator_GetCharactersInternal_m2774(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetLines(System.Collections.Generic.List`1<UnityEngine.UILineInfo>)
extern "C" void TextGenerator_GetLines_m2781 (TextGenerator_t186 * __this, List_1_t481 * ___lines, const MethodInfo* method)
{
	{
		List_1_t481 * L_0 = ___lines;
		TextGenerator_GetLinesInternal_m2776(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.TextGenerator::GetVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C" void TextGenerator_GetVertices_m2782 (TextGenerator_t186 * __this, List_1_t187 * ___vertices, const MethodInfo* method)
{
	{
		List_1_t187 * L_0 = ___vertices;
		TextGenerator_GetVerticesInternal_m2771(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredWidth(System.String,UnityEngine.TextGenerationSettings)
extern "C" float TextGenerator_GetPreferredWidth_m1913 (TextGenerator_t186 * __this, String_t* ___str, TextGenerationSettings_t290  ___settings, const MethodInfo* method)
{
	Rect_t184  V_0 = {0};
	{
		(&___settings)->___horizontalOverflow_13 = 1;
		(&___settings)->___verticalOverflow_12 = 1;
		(&___settings)->___updateBounds_11 = 1;
		String_t* L_0 = ___str;
		TextGenerationSettings_t290  L_1 = ___settings;
		TextGenerator_Populate_m1793(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t184  L_2 = TextGenerator_get_rectExtents_m1794(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_width_m1635((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Single UnityEngine.TextGenerator::GetPreferredHeight(System.String,UnityEngine.TextGenerationSettings)
extern "C" float TextGenerator_GetPreferredHeight_m1914 (TextGenerator_t186 * __this, String_t* ___str, TextGenerationSettings_t290  ___settings, const MethodInfo* method)
{
	Rect_t184  V_0 = {0};
	{
		(&___settings)->___verticalOverflow_12 = 1;
		(&___settings)->___updateBounds_11 = 1;
		String_t* L_0 = ___str;
		TextGenerationSettings_t290  L_1 = ___settings;
		TextGenerator_Populate_m1793(__this, L_0, L_1, /*hidden argument*/NULL);
		Rect_t184  L_2 = TextGenerator_get_rectExtents_m1794(__this, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = Rect_get_height_m1636((&V_0), /*hidden argument*/NULL);
		return L_3;
	}
}
// System.Boolean UnityEngine.TextGenerator::Populate(System.String,UnityEngine.TextGenerationSettings)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" bool TextGenerator_Populate_m1793 (TextGenerator_t186 * __this, String_t* ___str, TextGenerationSettings_t290  ___settings, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___m_HasGenerated_3);
		if (!L_0)
		{
			goto IL_0035;
		}
	}
	{
		String_t* L_1 = ___str;
		String_t* L_2 = (__this->___m_LastString_1);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m204(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		TextGenerationSettings_t290  L_4 = (__this->___m_LastSettings_2);
		bool L_5 = TextGenerationSettings_Equals_m3163((&___settings), L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0035;
		}
	}
	{
		bool L_6 = (__this->___m_LastValid_4);
		return L_6;
	}

IL_0035:
	{
		String_t* L_7 = ___str;
		TextGenerationSettings_t290  L_8 = ___settings;
		bool L_9 = TextGenerator_PopulateAlways_m2783(__this, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// System.Boolean UnityEngine.TextGenerator::PopulateAlways(System.String,UnityEngine.TextGenerationSettings)
extern "C" bool TextGenerator_PopulateAlways_m2783 (TextGenerator_t186 * __this, String_t* ___str, TextGenerationSettings_t290  ___settings, const MethodInfo* method)
{
	TextGenerationSettings_t290  V_0 = {0};
	{
		String_t* L_0 = ___str;
		__this->___m_LastString_1 = L_0;
		__this->___m_HasGenerated_3 = 1;
		__this->___m_CachedVerts_8 = 0;
		__this->___m_CachedCharacters_9 = 0;
		__this->___m_CachedLines_10 = 0;
		TextGenerationSettings_t290  L_1 = ___settings;
		__this->___m_LastSettings_2 = L_1;
		TextGenerationSettings_t290  L_2 = ___settings;
		TextGenerationSettings_t290  L_3 = TextGenerator_ValidatedSettings_m2779(__this, L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = ___str;
		Font_t142 * L_5 = ((&V_0)->___font_0);
		Color_t128  L_6 = ((&V_0)->___color_1);
		int32_t L_7 = ((&V_0)->___fontSize_2);
		float L_8 = ((&V_0)->___scaleFactor_5);
		float L_9 = ((&V_0)->___lineSpacing_3);
		int32_t L_10 = ((&V_0)->___fontStyle_6);
		bool L_11 = ((&V_0)->___richText_4);
		bool L_12 = ((&V_0)->___resizeTextForBestFit_8);
		int32_t L_13 = ((&V_0)->___resizeTextMinSize_9);
		int32_t L_14 = ((&V_0)->___resizeTextMaxSize_10);
		int32_t L_15 = ((&V_0)->___verticalOverflow_12);
		int32_t L_16 = ((&V_0)->___horizontalOverflow_13);
		bool L_17 = ((&V_0)->___updateBounds_11);
		int32_t L_18 = ((&V_0)->___textAnchor_7);
		Vector2_t59  L_19 = ((&V_0)->___generationExtents_14);
		Vector2_t59  L_20 = ((&V_0)->___pivot_15);
		bool L_21 = ((&V_0)->___generateOutOfBounds_16);
		bool L_22 = TextGenerator_Populate_Internal_m2766(__this, L_4, L_5, L_6, L_7, L_8, L_9, L_10, L_11, L_12, L_13, L_14, L_15, L_16, L_17, L_18, L_19, L_20, L_21, /*hidden argument*/NULL);
		__this->___m_LastValid_4 = L_22;
		bool L_23 = (__this->___m_LastValid_4);
		return L_23;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UIVertex> UnityEngine.TextGenerator::get_verts()
extern "C" Object_t* TextGenerator_get_verts_m1911 (TextGenerator_t186 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedVerts_8);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t187 * L_1 = (__this->___m_Verts_5);
		TextGenerator_GetVertices_m2782(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedVerts_8 = 1;
	}

IL_001e:
	{
		List_1_t187 * L_2 = (__this->___m_Verts_5);
		return L_2;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo> UnityEngine.TextGenerator::get_characters()
extern "C" Object_t* TextGenerator_get_characters_m1773 (TextGenerator_t186 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedCharacters_9);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t480 * L_1 = (__this->___m_Characters_6);
		TextGenerator_GetCharacters_m2780(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedCharacters_9 = 1;
	}

IL_001e:
	{
		List_1_t480 * L_2 = (__this->___m_Characters_6);
		return L_2;
	}
}
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo> UnityEngine.TextGenerator::get_lines()
extern "C" Object_t* TextGenerator_get_lines_m1770 (TextGenerator_t186 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CachedLines_10);
		if (L_0)
		{
			goto IL_001e;
		}
	}
	{
		List_1_t481 * L_1 = (__this->___m_Lines_7);
		TextGenerator_GetLines_m2781(__this, L_1, /*hidden argument*/NULL);
		__this->___m_CachedLines_10 = 1;
	}

IL_001e:
	{
		List_1_t481 * L_2 = (__this->___m_Lines_7);
		return L_2;
	}
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::.ctor(System.Object,System.IntPtr)
extern "C" void WillRenderCanvases__ctor_m1603 (WillRenderCanvases_t311 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::Invoke()
extern "C" void WillRenderCanvases_Invoke_m2784 (WillRenderCanvases_t311 * __this, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		WillRenderCanvases_Invoke_m2784((WillRenderCanvases_t311 *)__this->___prev_9, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if ((__this->___m_target_2 != NULL || MethodHasParameters((MethodInfo*)(__this->___method_3.___m_value_0))) && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_WillRenderCanvases_t311(Il2CppObject* delegate)
{
	typedef void (STDCALL *native_function_ptr_type)();
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Native function invocation
	_il2cpp_pinvoke_func();

}
// System.IAsyncResult UnityEngine.Canvas/WillRenderCanvases::BeginInvoke(System.AsyncCallback,System.Object)
extern "C" Object_t * WillRenderCanvases_BeginInvoke_m2785 (WillRenderCanvases_t311 * __this, AsyncCallback_t181 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[1] = {0};
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void UnityEngine.Canvas/WillRenderCanvases::EndInvoke(System.IAsyncResult)
extern "C" void WillRenderCanvases_EndInvoke_m2786 (WillRenderCanvases_t311 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void UnityEngine.Canvas::add_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern TypeInfo* Canvas_t148_il2cpp_TypeInfo_var;
extern TypeInfo* WillRenderCanvases_t311_il2cpp_TypeInfo_var;
extern "C" void Canvas_add_willRenderCanvases_m1604 (Object_t * __this /* static, unused */, WillRenderCanvases_t311 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t148_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(148);
		WillRenderCanvases_t311_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(125);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t311 * L_0 = ((Canvas_t148_StaticFields*)Canvas_t148_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		WillRenderCanvases_t311 * L_1 = ___value;
		Delegate_t320 * L_2 = Delegate_Combine_m1656(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Canvas_t148_StaticFields*)Canvas_t148_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2 = ((WillRenderCanvases_t311 *)CastclassSealed(L_2, WillRenderCanvases_t311_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void UnityEngine.Canvas::remove_willRenderCanvases(UnityEngine.Canvas/WillRenderCanvases)
extern TypeInfo* Canvas_t148_il2cpp_TypeInfo_var;
extern TypeInfo* WillRenderCanvases_t311_il2cpp_TypeInfo_var;
extern "C" void Canvas_remove_willRenderCanvases_m2787 (Object_t * __this /* static, unused */, WillRenderCanvases_t311 * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t148_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(148);
		WillRenderCanvases_t311_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(125);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t311 * L_0 = ((Canvas_t148_StaticFields*)Canvas_t148_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		WillRenderCanvases_t311 * L_1 = ___value;
		Delegate_t320 * L_2 = Delegate_Remove_m1657(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		((Canvas_t148_StaticFields*)Canvas_t148_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2 = ((WillRenderCanvases_t311 *)CastclassSealed(L_2, WillRenderCanvases_t311_il2cpp_TypeInfo_var));
		return;
	}
}
// UnityEngine.RenderMode UnityEngine.Canvas::get_renderMode()
extern "C" int32_t Canvas_get_renderMode_m1661 (Canvas_t148 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_renderMode_m1661_ftn) (Canvas_t148 *);
	static Canvas_get_renderMode_m1661_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderMode_m1661_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderMode()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.Canvas::get_isRootCanvas()
extern "C" bool Canvas_get_isRootCanvas_m1926 (Canvas_t148 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_isRootCanvas_m1926_ftn) (Canvas_t148 *);
	static Canvas_get_isRootCanvas_m1926_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_isRootCanvas_m1926_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_isRootCanvas()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Camera UnityEngine.Canvas::get_worldCamera()
extern "C" Camera_t122 * Canvas_get_worldCamera_m1677 (Canvas_t148 * __this, const MethodInfo* method)
{
	typedef Camera_t122 * (*Canvas_get_worldCamera_m1677_ftn) (Canvas_t148 *);
	static Canvas_get_worldCamera_m1677_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_worldCamera_m1677_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_worldCamera()");
	return _il2cpp_icall_func(__this);
}
// System.Single UnityEngine.Canvas::get_scaleFactor()
extern "C" float Canvas_get_scaleFactor_m1909 (Canvas_t148 * __this, const MethodInfo* method)
{
	typedef float (*Canvas_get_scaleFactor_m1909_ftn) (Canvas_t148 *);
	static Canvas_get_scaleFactor_m1909_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_scaleFactor_m1909_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_scaleFactor()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_scaleFactor(System.Single)
extern "C" void Canvas_set_scaleFactor_m1930 (Canvas_t148 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Canvas_set_scaleFactor_m1930_ftn) (Canvas_t148 *, float);
	static Canvas_set_scaleFactor_m1930_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_scaleFactor_m1930_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_scaleFactor(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Single UnityEngine.Canvas::get_referencePixelsPerUnit()
extern "C" float Canvas_get_referencePixelsPerUnit_m1694 (Canvas_t148 * __this, const MethodInfo* method)
{
	typedef float (*Canvas_get_referencePixelsPerUnit_m1694_ftn) (Canvas_t148 *);
	static Canvas_get_referencePixelsPerUnit_m1694_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_referencePixelsPerUnit_m1694_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_referencePixelsPerUnit()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)
extern "C" void Canvas_set_referencePixelsPerUnit_m1931 (Canvas_t148 * __this, float ___value, const MethodInfo* method)
{
	typedef void (*Canvas_set_referencePixelsPerUnit_m1931_ftn) (Canvas_t148 *, float);
	static Canvas_set_referencePixelsPerUnit_m1931_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_set_referencePixelsPerUnit_m1931_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::set_referencePixelsPerUnit(System.Single)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Boolean UnityEngine.Canvas::get_pixelPerfect()
extern "C" bool Canvas_get_pixelPerfect_m1647 (Canvas_t148 * __this, const MethodInfo* method)
{
	typedef bool (*Canvas_get_pixelPerfect_m1647_ftn) (Canvas_t148 *);
	static Canvas_get_pixelPerfect_m1647_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_pixelPerfect_m1647_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_pixelPerfect()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Canvas::get_renderOrder()
extern "C" int32_t Canvas_get_renderOrder_m1663 (Canvas_t148 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_renderOrder_m1663_ftn) (Canvas_t148 *);
	static Canvas_get_renderOrder_m1663_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_renderOrder_m1663_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_renderOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Canvas::get_sortingOrder()
extern "C" int32_t Canvas_get_sortingOrder_m1662 (Canvas_t148 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_sortingOrder_m1662_ftn) (Canvas_t148 *);
	static Canvas_get_sortingOrder_m1662_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_sortingOrder_m1662_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_sortingOrder()");
	return _il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.Canvas::get_cachedSortingLayerValue()
extern "C" int32_t Canvas_get_cachedSortingLayerValue_m1676 (Canvas_t148 * __this, const MethodInfo* method)
{
	typedef int32_t (*Canvas_get_cachedSortingLayerValue_m1676_ftn) (Canvas_t148 *);
	static Canvas_get_cachedSortingLayerValue_m1676_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_get_cachedSortingLayerValue_m1676_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::get_cachedSortingLayerValue()");
	return _il2cpp_icall_func(__this);
}
// UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasMaterial()
extern "C" Material_t146 * Canvas_GetDefaultCanvasMaterial_m1623 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t146 * (*Canvas_GetDefaultCanvasMaterial_m1623_ftn) ();
	static Canvas_GetDefaultCanvasMaterial_m1623_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_GetDefaultCanvasMaterial_m1623_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetDefaultCanvasMaterial()");
	return _il2cpp_icall_func();
}
// UnityEngine.Material UnityEngine.Canvas::GetDefaultCanvasTextMaterial()
extern "C" Material_t146 * Canvas_GetDefaultCanvasTextMaterial_m1904 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	typedef Material_t146 * (*Canvas_GetDefaultCanvasTextMaterial_m1904_ftn) ();
	static Canvas_GetDefaultCanvasTextMaterial_m1904_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (Canvas_GetDefaultCanvasTextMaterial_m1904_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.Canvas::GetDefaultCanvasTextMaterial()");
	return _il2cpp_icall_func();
}
// System.Void UnityEngine.Canvas::SendWillRenderCanvases()
extern TypeInfo* Canvas_t148_il2cpp_TypeInfo_var;
extern "C" void Canvas_SendWillRenderCanvases_m2788 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Canvas_t148_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(148);
		s_Il2CppMethodIntialized = true;
	}
	{
		WillRenderCanvases_t311 * L_0 = ((Canvas_t148_StaticFields*)Canvas_t148_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		if (!L_0)
		{
			goto IL_0014;
		}
	}
	{
		WillRenderCanvases_t311 * L_1 = ((Canvas_t148_StaticFields*)Canvas_t148_il2cpp_TypeInfo_var->static_fields)->___willRenderCanvases_2;
		NullCheck(L_1);
		WillRenderCanvases_Invoke_m2784(L_1, /*hidden argument*/NULL);
	}

IL_0014:
	{
		return;
	}
}
// System.Void UnityEngine.Canvas::ForceUpdateCanvases()
extern "C" void Canvas_ForceUpdateCanvases_m1853 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	{
		Canvas_SendWillRenderCanvases_m2788(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.CanvasGroup::get_interactable()
extern "C" bool CanvasGroup_get_interactable_m1884 (CanvasGroup_t317 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_interactable_m1884_ftn) (CanvasGroup_t317 *);
	static CanvasGroup_get_interactable_m1884_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_interactable_m1884_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_interactable()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::get_blocksRaycasts()
extern "C" bool CanvasGroup_get_blocksRaycasts_m2789 (CanvasGroup_t317 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_blocksRaycasts_m2789_ftn) (CanvasGroup_t317 *);
	static CanvasGroup_get_blocksRaycasts_m2789_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_blocksRaycasts_m2789_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_blocksRaycasts()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::get_ignoreParentGroups()
extern "C" bool CanvasGroup_get_ignoreParentGroups_m1646 (CanvasGroup_t317 * __this, const MethodInfo* method)
{
	typedef bool (*CanvasGroup_get_ignoreParentGroups_m1646_ftn) (CanvasGroup_t317 *);
	static CanvasGroup_get_ignoreParentGroups_m1646_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasGroup_get_ignoreParentGroups_m1646_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasGroup::get_ignoreParentGroups()");
	return _il2cpp_icall_func(__this);
}
// System.Boolean UnityEngine.CanvasGroup::IsRaycastLocationValid(UnityEngine.Vector2,UnityEngine.Camera)
extern "C" bool CanvasGroup_IsRaycastLocationValid_m2790 (CanvasGroup_t317 * __this, Vector2_t59  ___sp, Camera_t122 * ___eventCamera, const MethodInfo* method)
{
	{
		bool L_0 = CanvasGroup_get_blocksRaycasts_m2789(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityEngine.UIVertex::.cctor()
extern TypeInfo* UIVertex_t191_il2cpp_TypeInfo_var;
extern "C" void UIVertex__cctor_m2791 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		UIVertex_t191_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(141);
		s_Il2CppMethodIntialized = true;
	}
	UIVertex_t191  V_0 = {0};
	{
		Color32_t295  L_0 = {0};
		Color32__ctor_m1611(&L_0, ((int32_t)255), ((int32_t)255), ((int32_t)255), ((int32_t)255), /*hidden argument*/NULL);
		((UIVertex_t191_StaticFields*)UIVertex_t191_il2cpp_TypeInfo_var->static_fields)->___s_DefaultColor_6 = L_0;
		Vector4_t287  L_1 = {0};
		Vector4__ctor_m1642(&L_1, (1.0f), (0.0f), (0.0f), (-1.0f), /*hidden argument*/NULL);
		((UIVertex_t191_StaticFields*)UIVertex_t191_il2cpp_TypeInfo_var->static_fields)->___s_DefaultTangent_7 = L_1;
		Initobj (UIVertex_t191_il2cpp_TypeInfo_var, (&V_0));
		Vector3_t35  L_2 = Vector3_get_zero_m1509(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___position_0 = L_2;
		Vector3_t35  L_3 = Vector3_get_back_m2335(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___normal_1 = L_3;
		Vector4_t287  L_4 = ((UIVertex_t191_StaticFields*)UIVertex_t191_il2cpp_TypeInfo_var->static_fields)->___s_DefaultTangent_7;
		(&V_0)->___tangent_5 = L_4;
		Color32_t295  L_5 = ((UIVertex_t191_StaticFields*)UIVertex_t191_il2cpp_TypeInfo_var->static_fields)->___s_DefaultColor_6;
		(&V_0)->___color_2 = L_5;
		Vector2_t59  L_6 = Vector2_get_zero_m1510(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___uv0_3 = L_6;
		Vector2_t59  L_7 = Vector2_get_zero_m1510(NULL /*static, unused*/, /*hidden argument*/NULL);
		(&V_0)->___uv1_4 = L_7;
		UIVertex_t191  L_8 = V_0;
		((UIVertex_t191_StaticFields*)UIVertex_t191_il2cpp_TypeInfo_var->static_fields)->___simpleVert_8 = L_8;
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::SetColor(UnityEngine.Color)
extern "C" void CanvasRenderer_SetColor_m1652 (CanvasRenderer_t147 * __this, Color_t128  ___color, const MethodInfo* method)
{
	{
		CanvasRenderer_INTERNAL_CALL_SetColor_m2792(NULL /*static, unused*/, __this, (&___color), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)
extern "C" void CanvasRenderer_INTERNAL_CALL_SetColor_m2792 (Object_t * __this /* static, unused */, CanvasRenderer_t147 * ___self, Color_t128 * ___color, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_INTERNAL_CALL_SetColor_m2792_ftn) (CanvasRenderer_t147 *, Color_t128 *);
	static CanvasRenderer_INTERNAL_CALL_SetColor_m2792_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_INTERNAL_CALL_SetColor_m2792_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::INTERNAL_CALL_SetColor(UnityEngine.CanvasRenderer,UnityEngine.Color&)");
	_il2cpp_icall_func(___self, ___color);
}
// UnityEngine.Color UnityEngine.CanvasRenderer::GetColor()
extern "C" Color_t128  CanvasRenderer_GetColor_m1650 (CanvasRenderer_t147 * __this, const MethodInfo* method)
{
	typedef Color_t128  (*CanvasRenderer_GetColor_m1650_ftn) (CanvasRenderer_t147 *);
	static CanvasRenderer_GetColor_m1650_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_GetColor_m1650_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::GetColor()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.CanvasRenderer::set_isMask(System.Boolean)
extern "C" void CanvasRenderer_set_isMask_m1970 (CanvasRenderer_t147 * __this, bool ___value, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_set_isMask_m1970_ftn) (CanvasRenderer_t147 *, bool);
	static CanvasRenderer_set_isMask_m1970_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_set_isMask_m1970_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::set_isMask(System.Boolean)");
	_il2cpp_icall_func(__this, ___value);
}
// System.Void UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,UnityEngine.Texture)
extern "C" void CanvasRenderer_SetMaterial_m1639 (CanvasRenderer_t147 * __this, Material_t146 * ___material, Texture_t196 * ___texture, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetMaterial_m1639_ftn) (CanvasRenderer_t147 *, Material_t146 *, Texture_t196 *);
	static CanvasRenderer_SetMaterial_m1639_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetMaterial_m1639_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetMaterial(UnityEngine.Material,UnityEngine.Texture)");
	_il2cpp_icall_func(__this, ___material, ___texture);
}
// System.Void UnityEngine.CanvasRenderer::SetVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern TypeInfo* ObjectU5BU5D_t60_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t652_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral273;
extern "C" void CanvasRenderer_SetVertices_m1637 (CanvasRenderer_t147 * __this, List_1_t187 * ___vertices, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		UInt16_t652_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(342);
		_stringLiteral273 = il2cpp_codegen_string_literal_from_index(273);
		s_Il2CppMethodIntialized = true;
	}
	{
		List_1_t187 * L_0 = ___vertices;
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count() */, L_0);
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)65535))))
		{
			goto IL_0039;
		}
	}
	{
		ObjectU5BU5D_t60* L_2 = ((ObjectU5BU5D_t60*)SZArrayNew(ObjectU5BU5D_t60_il2cpp_TypeInfo_var, 1));
		uint16_t L_3 = ((int32_t)65535);
		Object_t * L_4 = Box(UInt16_t652_il2cpp_TypeInfo_var, &L_3);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_4);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 0, sizeof(Object_t *))) = (Object_t *)L_4;
		String_t* L_5 = UnityString_Format_m2512(NULL /*static, unused*/, _stringLiteral273, L_2, /*hidden argument*/NULL);
		Debug_LogWarning_m1900(NULL /*static, unused*/, L_5, __this, /*hidden argument*/NULL);
		List_1_t187 * L_6 = ___vertices;
		NullCheck(L_6);
		VirtActionInvoker0::Invoke(23 /* System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear() */, L_6);
	}

IL_0039:
	{
		List_1_t187 * L_7 = ___vertices;
		CanvasRenderer_SetVerticesInternal_m2793(__this, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::SetVerticesInternal(System.Object)
extern "C" void CanvasRenderer_SetVerticesInternal_m2793 (CanvasRenderer_t147 * __this, Object_t * ___vertices, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetVerticesInternal_m2793_ftn) (CanvasRenderer_t147 *, Object_t *);
	static CanvasRenderer_SetVerticesInternal_m2793_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetVerticesInternal_m2793_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetVerticesInternal(System.Object)");
	_il2cpp_icall_func(__this, ___vertices);
}
// System.Void UnityEngine.CanvasRenderer::SetVertices(UnityEngine.UIVertex[],System.Int32)
extern TypeInfo* ObjectU5BU5D_t60_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t652_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral273;
extern "C" void CanvasRenderer_SetVertices_m1753 (CanvasRenderer_t147 * __this, UIVertexU5BU5D_t185* ___vertices, int32_t ___size, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		UInt16_t652_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(342);
		_stringLiteral273 = il2cpp_codegen_string_literal_from_index(273);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___size;
		if ((((int32_t)L_0) <= ((int32_t)((int32_t)65535))))
		{
			goto IL_0031;
		}
	}
	{
		ObjectU5BU5D_t60* L_1 = ((ObjectU5BU5D_t60*)SZArrayNew(ObjectU5BU5D_t60_il2cpp_TypeInfo_var, 1));
		uint16_t L_2 = ((int32_t)65535);
		Object_t * L_3 = Box(UInt16_t652_il2cpp_TypeInfo_var, &L_2);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		ArrayElementTypeCheck (L_1, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_1, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		String_t* L_4 = UnityString_Format_m2512(NULL /*static, unused*/, _stringLiteral273, L_1, /*hidden argument*/NULL);
		Debug_LogWarning_m1900(NULL /*static, unused*/, L_4, __this, /*hidden argument*/NULL);
		___size = 0;
	}

IL_0031:
	{
		UIVertexU5BU5D_t185* L_5 = ___vertices;
		int32_t L_6 = ___size;
		CanvasRenderer_SetVerticesInternalArray_m2794(__this, L_5, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.CanvasRenderer::SetVerticesInternalArray(UnityEngine.UIVertex[],System.Int32)
extern "C" void CanvasRenderer_SetVerticesInternalArray_m2794 (CanvasRenderer_t147 * __this, UIVertexU5BU5D_t185* ___vertices, int32_t ___size, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_SetVerticesInternalArray_m2794_ftn) (CanvasRenderer_t147 *, UIVertexU5BU5D_t185*, int32_t);
	static CanvasRenderer_SetVerticesInternalArray_m2794_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_SetVerticesInternalArray_m2794_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::SetVerticesInternalArray(UnityEngine.UIVertex[],System.Int32)");
	_il2cpp_icall_func(__this, ___vertices, ___size);
}
// System.Void UnityEngine.CanvasRenderer::Clear()
extern "C" void CanvasRenderer_Clear_m1632 (CanvasRenderer_t147 * __this, const MethodInfo* method)
{
	typedef void (*CanvasRenderer_Clear_m1632_ftn) (CanvasRenderer_t147 *);
	static CanvasRenderer_Clear_m1632_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_Clear_m1632_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::Clear()");
	_il2cpp_icall_func(__this);
}
// System.Int32 UnityEngine.CanvasRenderer::get_absoluteDepth()
extern "C" int32_t CanvasRenderer_get_absoluteDepth_m1625 (CanvasRenderer_t147 * __this, const MethodInfo* method)
{
	typedef int32_t (*CanvasRenderer_get_absoluteDepth_m1625_ftn) (CanvasRenderer_t147 *);
	static CanvasRenderer_get_absoluteDepth_m1625_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (CanvasRenderer_get_absoluteDepth_m1625_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.CanvasRenderer::get_absoluteDepth()");
	return _il2cpp_icall_func(__this);
}
// System.Void UnityEngine.RectTransformUtility::.cctor()
extern TypeInfo* Vector3U5BU5D_t208_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransformUtility_t319_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility__cctor_m2795 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3U5BU5D_t208_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(207);
		RectTransformUtility_t319_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(159);
		s_Il2CppMethodIntialized = true;
	}
	{
		((RectTransformUtility_t319_StaticFields*)RectTransformUtility_t319_il2cpp_TypeInfo_var->static_fields)->___s_Corners_0 = ((Vector3U5BU5D_t208*)SZArrayNew(Vector3U5BU5D_t208_il2cpp_TypeInfo_var, 4));
		return;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera)
extern TypeInfo* RectTransformUtility_t319_il2cpp_TypeInfo_var;
extern "C" bool RectTransformUtility_RectangleContainsScreenPoint_m1678 (Object_t * __this /* static, unused */, RectTransform_t18 * ___rect, Vector2_t59  ___screenPoint, Camera_t122 * ___cam, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t319_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(159);
		s_Il2CppMethodIntialized = true;
	}
	{
		RectTransform_t18 * L_0 = ___rect;
		Camera_t122 * L_1 = ___cam;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t319_il2cpp_TypeInfo_var);
		bool L_2 = RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m2796(NULL /*static, unused*/, L_0, (&___screenPoint), L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)
extern "C" bool RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m2796 (Object_t * __this /* static, unused */, RectTransform_t18 * ___rect, Vector2_t59 * ___screenPoint, Camera_t122 * ___cam, const MethodInfo* method)
{
	typedef bool (*RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m2796_ftn) (RectTransform_t18 *, Vector2_t59 *, Camera_t122 *);
	static RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m2796_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m2796_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_RectangleContainsScreenPoint(UnityEngine.RectTransform,UnityEngine.Vector2&,UnityEngine.Camera)");
	return _il2cpp_icall_func(___rect, ___screenPoint, ___cam);
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas)
extern TypeInfo* RectTransformUtility_t319_il2cpp_TypeInfo_var;
extern "C" Vector2_t59  RectTransformUtility_PixelAdjustPoint_m1648 (Object_t * __this /* static, unused */, Vector2_t59  ___point, Transform_t3 * ___elementTransform, Canvas_t148 * ___canvas, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t319_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(159);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t59  V_0 = {0};
	{
		Vector2_t59  L_0 = ___point;
		Transform_t3 * L_1 = ___elementTransform;
		Canvas_t148 * L_2 = ___canvas;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t319_il2cpp_TypeInfo_var);
		RectTransformUtility_PixelAdjustPoint_m2797(NULL /*static, unused*/, L_0, L_1, L_2, (&V_0), /*hidden argument*/NULL);
		Vector2_t59  L_3 = V_0;
		return L_3;
	}
}
// System.Void UnityEngine.RectTransformUtility::PixelAdjustPoint(UnityEngine.Vector2,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern TypeInfo* RectTransformUtility_t319_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility_PixelAdjustPoint_m2797 (Object_t * __this /* static, unused */, Vector2_t59  ___point, Transform_t3 * ___elementTransform, Canvas_t148 * ___canvas, Vector2_t59 * ___output, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t319_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(159);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t3 * L_0 = ___elementTransform;
		Canvas_t148 * L_1 = ___canvas;
		Vector2_t59 * L_2 = ___output;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t319_il2cpp_TypeInfo_var);
		RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2798(NULL /*static, unused*/, (&___point), L_0, L_1, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)
extern "C" void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2798 (Object_t * __this /* static, unused */, Vector2_t59 * ___point, Transform_t3 * ___elementTransform, Canvas_t148 * ___canvas, Vector2_t59 * ___output, const MethodInfo* method)
{
	typedef void (*RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2798_ftn) (Vector2_t59 *, Transform_t3 *, Canvas_t148 *, Vector2_t59 *);
	static RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2798_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2798_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::INTERNAL_CALL_PixelAdjustPoint(UnityEngine.Vector2&,UnityEngine.Transform,UnityEngine.Canvas,UnityEngine.Vector2&)");
	_il2cpp_icall_func(___point, ___elementTransform, ___canvas, ___output);
}
// UnityEngine.Rect UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)
extern "C" Rect_t184  RectTransformUtility_PixelAdjustRect_m1649 (Object_t * __this /* static, unused */, RectTransform_t18 * ___rectTransform, Canvas_t148 * ___canvas, const MethodInfo* method)
{
	typedef Rect_t184  (*RectTransformUtility_PixelAdjustRect_m1649_ftn) (RectTransform_t18 *, Canvas_t148 *);
	static RectTransformUtility_PixelAdjustRect_m1649_ftn _il2cpp_icall_func;
	if (!_il2cpp_icall_func)
	_il2cpp_icall_func = (RectTransformUtility_PixelAdjustRect_m1649_ftn)il2cpp_codegen_resolve_icall ("UnityEngine.RectTransformUtility::PixelAdjustRect(UnityEngine.RectTransform,UnityEngine.Canvas)");
	return _il2cpp_icall_func(___rectTransform, ___canvas);
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToWorldPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector3&)
extern TypeInfo* RectTransformUtility_t319_il2cpp_TypeInfo_var;
extern "C" bool RectTransformUtility_ScreenPointToWorldPointInRectangle_m2799 (Object_t * __this /* static, unused */, RectTransform_t18 * ___rect, Vector2_t59  ___screenPoint, Camera_t122 * ___cam, Vector3_t35 * ___worldPoint, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t319_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(159);
		s_Il2CppMethodIntialized = true;
	}
	Ray_t306  V_0 = {0};
	Plane_t330  V_1 = {0};
	float V_2 = 0.0f;
	{
		Vector3_t35 * L_0 = ___worldPoint;
		Vector2_t59  L_1 = Vector2_get_zero_m1510(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t35  L_2 = Vector2_op_Implicit_m260(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		*L_0 = L_2;
		Camera_t122 * L_3 = ___cam;
		Vector2_t59  L_4 = ___screenPoint;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t319_il2cpp_TypeInfo_var);
		Ray_t306  L_5 = RectTransformUtility_ScreenPointToRay_m2800(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		RectTransform_t18 * L_6 = ___rect;
		NullCheck(L_6);
		Quaternion_t53  L_7 = Transform_get_rotation_m1671(L_6, /*hidden argument*/NULL);
		Vector3_t35  L_8 = Vector3_get_back_m2335(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t35  L_9 = Quaternion_op_Multiply_m1673(NULL /*static, unused*/, L_7, L_8, /*hidden argument*/NULL);
		RectTransform_t18 * L_10 = ___rect;
		NullCheck(L_10);
		Vector3_t35  L_11 = Transform_get_position_m257(L_10, /*hidden argument*/NULL);
		Plane__ctor_m1767((&V_1), L_9, L_11, /*hidden argument*/NULL);
		Ray_t306  L_12 = V_0;
		bool L_13 = Plane_Raycast_m1768((&V_1), L_12, (&V_2), /*hidden argument*/NULL);
		if (L_13)
		{
			goto IL_0046;
		}
	}
	{
		return 0;
	}

IL_0046:
	{
		Vector3_t35 * L_14 = ___worldPoint;
		float L_15 = V_2;
		Vector3_t35  L_16 = Ray_GetPoint_m1769((&V_0), L_15, /*hidden argument*/NULL);
		*L_14 = L_16;
		return 1;
	}
}
// System.Boolean UnityEngine.RectTransformUtility::ScreenPointToLocalPointInRectangle(UnityEngine.RectTransform,UnityEngine.Vector2,UnityEngine.Camera,UnityEngine.Vector2&)
extern TypeInfo* RectTransformUtility_t319_il2cpp_TypeInfo_var;
extern "C" bool RectTransformUtility_ScreenPointToLocalPointInRectangle_m1718 (Object_t * __this /* static, unused */, RectTransform_t18 * ___rect, Vector2_t59  ___screenPoint, Camera_t122 * ___cam, Vector2_t59 * ___localPoint, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransformUtility_t319_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(159);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t35  V_0 = {0};
	{
		Vector2_t59 * L_0 = ___localPoint;
		Vector2_t59  L_1 = Vector2_get_zero_m1510(NULL /*static, unused*/, /*hidden argument*/NULL);
		*L_0 = L_1;
		RectTransform_t18 * L_2 = ___rect;
		Vector2_t59  L_3 = ___screenPoint;
		Camera_t122 * L_4 = ___cam;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t319_il2cpp_TypeInfo_var);
		bool L_5 = RectTransformUtility_ScreenPointToWorldPointInRectangle_m2799(NULL /*static, unused*/, L_2, L_3, L_4, (&V_0), /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		Vector2_t59 * L_6 = ___localPoint;
		RectTransform_t18 * L_7 = ___rect;
		Vector3_t35  L_8 = V_0;
		NullCheck(L_7);
		Vector3_t35  L_9 = Transform_InverseTransformPoint_m1766(L_7, L_8, /*hidden argument*/NULL);
		Vector2_t59  L_10 = Vector2_op_Implicit_m262(NULL /*static, unused*/, L_9, /*hidden argument*/NULL);
		*L_6 = L_10;
		return 1;
	}

IL_002e:
	{
		return 0;
	}
}
// UnityEngine.Ray UnityEngine.RectTransformUtility::ScreenPointToRay(UnityEngine.Camera,UnityEngine.Vector2)
extern "C" Ray_t306  RectTransformUtility_ScreenPointToRay_m2800 (Object_t * __this /* static, unused */, Camera_t122 * ___cam, Vector2_t59  ___screenPos, const MethodInfo* method)
{
	Vector3_t35  V_0 = {0};
	{
		Camera_t122 * L_0 = ___cam;
		bool L_1 = Object_op_Inequality_m1472(NULL /*static, unused*/, L_0, (Object_t54 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0019;
		}
	}
	{
		Camera_t122 * L_2 = ___cam;
		Vector2_t59  L_3 = ___screenPos;
		Vector3_t35  L_4 = Vector2_op_Implicit_m260(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		NullCheck(L_2);
		Ray_t306  L_5 = Camera_ScreenPointToRay_m1571(L_2, L_4, /*hidden argument*/NULL);
		return L_5;
	}

IL_0019:
	{
		Vector2_t59  L_6 = ___screenPos;
		Vector3_t35  L_7 = Vector2_op_Implicit_m260(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		V_0 = L_7;
		Vector3_t35 * L_8 = (&V_0);
		float L_9 = (L_8->___z_3);
		L_8->___z_3 = ((float)((float)L_9-(float)(100.0f)));
		Vector3_t35  L_10 = V_0;
		Vector3_t35  L_11 = Vector3_get_forward_m1672(NULL /*static, unused*/, /*hidden argument*/NULL);
		Ray_t306  L_12 = {0};
		Ray__ctor_m2423(&L_12, L_10, L_11, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutOnAxis(UnityEngine.RectTransform,System.Int32,System.Boolean,System.Boolean)
extern TypeInfo* RectTransform_t18_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransformUtility_t319_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility_FlipLayoutOnAxis_m1848 (Object_t * __this /* static, unused */, RectTransform_t18 * ___rect, int32_t ___axis, bool ___keepPositioning, bool ___recursive, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(147);
		RectTransformUtility_t319_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(159);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t18 * V_1 = {0};
	Vector2_t59  V_2 = {0};
	Vector2_t59  V_3 = {0};
	Vector2_t59  V_4 = {0};
	Vector2_t59  V_5 = {0};
	float V_6 = 0.0f;
	{
		RectTransform_t18 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m247(NULL /*static, unused*/, L_0, (Object_t54 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive;
		if (!L_2)
		{
			goto IL_004c;
		}
	}
	{
		V_0 = 0;
		goto IL_0040;
	}

IL_001a:
	{
		RectTransform_t18 * L_3 = ___rect;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t3 * L_5 = Transform_GetChild_m225(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t18 *)IsInstSealed(L_5, RectTransform_t18_il2cpp_TypeInfo_var));
		RectTransform_t18 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m1472(NULL /*static, unused*/, L_6, (Object_t54 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003c;
		}
	}
	{
		RectTransform_t18 * L_8 = V_1;
		int32_t L_9 = ___axis;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t319_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutOnAxis_m1848(NULL /*static, unused*/, L_8, L_9, 0, 1, /*hidden argument*/NULL);
	}

IL_003c:
	{
		int32_t L_10 = V_0;
		V_0 = ((int32_t)((int32_t)L_10+(int32_t)1));
	}

IL_0040:
	{
		int32_t L_11 = V_0;
		RectTransform_t18 * L_12 = ___rect;
		NullCheck(L_12);
		int32_t L_13 = Transform_get_childCount_m228(L_12, /*hidden argument*/NULL);
		if ((((int32_t)L_11) < ((int32_t)L_13)))
		{
			goto IL_001a;
		}
	}

IL_004c:
	{
		RectTransform_t18 * L_14 = ___rect;
		NullCheck(L_14);
		Vector2_t59  L_15 = RectTransform_get_pivot_m1701(L_14, /*hidden argument*/NULL);
		V_2 = L_15;
		int32_t L_16 = ___axis;
		int32_t L_17 = ___axis;
		float L_18 = Vector2_get_Item_m1715((&V_2), L_17, /*hidden argument*/NULL);
		Vector2_set_Item_m1725((&V_2), L_16, ((float)((float)(1.0f)-(float)L_18)), /*hidden argument*/NULL);
		RectTransform_t18 * L_19 = ___rect;
		Vector2_t59  L_20 = V_2;
		NullCheck(L_19);
		RectTransform_set_pivot_m1816(L_19, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning;
		if (!L_21)
		{
			goto IL_0077;
		}
	}
	{
		return;
	}

IL_0077:
	{
		RectTransform_t18 * L_22 = ___rect;
		NullCheck(L_22);
		Vector2_t59  L_23 = RectTransform_get_anchoredPosition_m1810(L_22, /*hidden argument*/NULL);
		V_3 = L_23;
		int32_t L_24 = ___axis;
		int32_t L_25 = ___axis;
		float L_26 = Vector2_get_Item_m1715((&V_3), L_25, /*hidden argument*/NULL);
		Vector2_set_Item_m1725((&V_3), L_24, ((-L_26)), /*hidden argument*/NULL);
		RectTransform_t18 * L_27 = ___rect;
		Vector2_t59  L_28 = V_3;
		NullCheck(L_27);
		RectTransform_set_anchoredPosition_m1815(L_27, L_28, /*hidden argument*/NULL);
		RectTransform_t18 * L_29 = ___rect;
		NullCheck(L_29);
		Vector2_t59  L_30 = RectTransform_get_anchorMin_m1705(L_29, /*hidden argument*/NULL);
		V_4 = L_30;
		RectTransform_t18 * L_31 = ___rect;
		NullCheck(L_31);
		Vector2_t59  L_32 = RectTransform_get_anchorMax_m1809(L_31, /*hidden argument*/NULL);
		V_5 = L_32;
		int32_t L_33 = ___axis;
		float L_34 = Vector2_get_Item_m1715((&V_4), L_33, /*hidden argument*/NULL);
		V_6 = L_34;
		int32_t L_35 = ___axis;
		int32_t L_36 = ___axis;
		float L_37 = Vector2_get_Item_m1715((&V_5), L_36, /*hidden argument*/NULL);
		Vector2_set_Item_m1725((&V_4), L_35, ((float)((float)(1.0f)-(float)L_37)), /*hidden argument*/NULL);
		int32_t L_38 = ___axis;
		float L_39 = V_6;
		Vector2_set_Item_m1725((&V_5), L_38, ((float)((float)(1.0f)-(float)L_39)), /*hidden argument*/NULL);
		RectTransform_t18 * L_40 = ___rect;
		Vector2_t59  L_41 = V_4;
		NullCheck(L_40);
		RectTransform_set_anchorMin_m1814(L_40, L_41, /*hidden argument*/NULL);
		RectTransform_t18 * L_42 = ___rect;
		Vector2_t59  L_43 = V_5;
		NullCheck(L_42);
		RectTransform_set_anchorMax_m1706(L_42, L_43, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RectTransformUtility::FlipLayoutAxes(UnityEngine.RectTransform,System.Boolean,System.Boolean)
extern TypeInfo* RectTransform_t18_il2cpp_TypeInfo_var;
extern TypeInfo* RectTransformUtility_t319_il2cpp_TypeInfo_var;
extern "C" void RectTransformUtility_FlipLayoutAxes_m1847 (Object_t * __this /* static, unused */, RectTransform_t18 * ___rect, bool ___keepPositioning, bool ___recursive, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RectTransform_t18_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(147);
		RectTransformUtility_t319_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(159);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	RectTransform_t18 * V_1 = {0};
	{
		RectTransform_t18 * L_0 = ___rect;
		bool L_1 = Object_op_Equality_m247(NULL /*static, unused*/, L_0, (Object_t54 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_000d;
		}
	}
	{
		return;
	}

IL_000d:
	{
		bool L_2 = ___recursive;
		if (!L_2)
		{
			goto IL_004b;
		}
	}
	{
		V_0 = 0;
		goto IL_003f;
	}

IL_001a:
	{
		RectTransform_t18 * L_3 = ___rect;
		int32_t L_4 = V_0;
		NullCheck(L_3);
		Transform_t3 * L_5 = Transform_GetChild_m225(L_3, L_4, /*hidden argument*/NULL);
		V_1 = ((RectTransform_t18 *)IsInstSealed(L_5, RectTransform_t18_il2cpp_TypeInfo_var));
		RectTransform_t18 * L_6 = V_1;
		bool L_7 = Object_op_Inequality_m1472(NULL /*static, unused*/, L_6, (Object_t54 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_003b;
		}
	}
	{
		RectTransform_t18 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t319_il2cpp_TypeInfo_var);
		RectTransformUtility_FlipLayoutAxes_m1847(NULL /*static, unused*/, L_8, 0, 1, /*hidden argument*/NULL);
	}

IL_003b:
	{
		int32_t L_9 = V_0;
		V_0 = ((int32_t)((int32_t)L_9+(int32_t)1));
	}

IL_003f:
	{
		int32_t L_10 = V_0;
		RectTransform_t18 * L_11 = ___rect;
		NullCheck(L_11);
		int32_t L_12 = Transform_get_childCount_m228(L_11, /*hidden argument*/NULL);
		if ((((int32_t)L_10) < ((int32_t)L_12)))
		{
			goto IL_001a;
		}
	}

IL_004b:
	{
		RectTransform_t18 * L_13 = ___rect;
		RectTransform_t18 * L_14 = ___rect;
		NullCheck(L_14);
		Vector2_t59  L_15 = RectTransform_get_pivot_m1701(L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t319_il2cpp_TypeInfo_var);
		Vector2_t59  L_16 = RectTransformUtility_GetTransposed_m2801(NULL /*static, unused*/, L_15, /*hidden argument*/NULL);
		NullCheck(L_13);
		RectTransform_set_pivot_m1816(L_13, L_16, /*hidden argument*/NULL);
		RectTransform_t18 * L_17 = ___rect;
		RectTransform_t18 * L_18 = ___rect;
		NullCheck(L_18);
		Vector2_t59  L_19 = RectTransform_get_sizeDelta_m1811(L_18, /*hidden argument*/NULL);
		Vector2_t59  L_20 = RectTransformUtility_GetTransposed_m2801(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_17);
		RectTransform_set_sizeDelta_m232(L_17, L_20, /*hidden argument*/NULL);
		bool L_21 = ___keepPositioning;
		if (!L_21)
		{
			goto IL_0074;
		}
	}
	{
		return;
	}

IL_0074:
	{
		RectTransform_t18 * L_22 = ___rect;
		RectTransform_t18 * L_23 = ___rect;
		NullCheck(L_23);
		Vector2_t59  L_24 = RectTransform_get_anchoredPosition_m1810(L_23, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(RectTransformUtility_t319_il2cpp_TypeInfo_var);
		Vector2_t59  L_25 = RectTransformUtility_GetTransposed_m2801(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_22);
		RectTransform_set_anchoredPosition_m1815(L_22, L_25, /*hidden argument*/NULL);
		RectTransform_t18 * L_26 = ___rect;
		RectTransform_t18 * L_27 = ___rect;
		NullCheck(L_27);
		Vector2_t59  L_28 = RectTransform_get_anchorMin_m1705(L_27, /*hidden argument*/NULL);
		Vector2_t59  L_29 = RectTransformUtility_GetTransposed_m2801(NULL /*static, unused*/, L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		RectTransform_set_anchorMin_m1814(L_26, L_29, /*hidden argument*/NULL);
		RectTransform_t18 * L_30 = ___rect;
		RectTransform_t18 * L_31 = ___rect;
		NullCheck(L_31);
		Vector2_t59  L_32 = RectTransform_get_anchorMax_m1809(L_31, /*hidden argument*/NULL);
		Vector2_t59  L_33 = RectTransformUtility_GetTransposed_m2801(NULL /*static, unused*/, L_32, /*hidden argument*/NULL);
		NullCheck(L_30);
		RectTransform_set_anchorMax_m1706(L_30, L_33, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector2 UnityEngine.RectTransformUtility::GetTransposed(UnityEngine.Vector2)
extern "C" Vector2_t59  RectTransformUtility_GetTransposed_m2801 (Object_t * __this /* static, unused */, Vector2_t59  ___input, const MethodInfo* method)
{
	{
		float L_0 = ((&___input)->___y_2);
		float L_1 = ((&___input)->___x_1);
		Vector2_t59  L_2 = {0};
		Vector2__ctor_m231(&L_2, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityEngine.Networking.Match.Request::.ctor()
extern "C" void Request__ctor_m2802 (Request_t483 * __this, const MethodInfo* method)
{
	{
		__this->___version_0 = 2;
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Networking.Types.SourceID UnityEngine.Networking.Match.Request::get_sourceId()
extern "C" uint64_t Request_get_sourceId_m2803 (Request_t483 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = (__this->___U3CsourceIdU3Ek__BackingField_1);
		return L_0;
	}
}
// UnityEngine.Networking.Types.AppID UnityEngine.Networking.Match.Request::get_appId()
extern "C" uint64_t Request_get_appId_m2804 (Request_t483 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = (__this->___U3CappIdU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Int32 UnityEngine.Networking.Match.Request::get_domain()
extern "C" int32_t Request_get_domain_m2805 (Request_t483 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CdomainU3Ek__BackingField_3);
		return L_0;
	}
}
// System.String UnityEngine.Networking.Match.Request::ToString()
extern TypeInfo* ObjectU5BU5D_t60_il2cpp_TypeInfo_var;
extern TypeInfo* SourceID_t501_il2cpp_TypeInfo_var;
extern TypeInfo* AppID_t500_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t56_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral274;
extern Il2CppCodeGenString* _stringLiteral275;
extern "C" String_t* Request_ToString_m2806 (Request_t483 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		SourceID_t501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(343);
		AppID_t500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(344);
		Int32_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral274 = il2cpp_codegen_string_literal_from_index(274);
		_stringLiteral275 = il2cpp_codegen_string_literal_from_index(275);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t60* L_0 = ((ObjectU5BU5D_t60*)SZArrayNew(ObjectU5BU5D_t60_il2cpp_TypeInfo_var, 4));
		String_t* L_1 = Object_ToString_m3295(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t60* L_2 = L_0;
		uint64_t L_3 = Request_get_sourceId_m2803(__this, /*hidden argument*/NULL);
		uint64_t L_4 = L_3;
		Object_t * L_5 = Box(SourceID_t501_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_5);
		String_t* L_6 = Enum_ToString_m3296(L_5, _stringLiteral275, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_6;
		ObjectU5BU5D_t60* L_7 = L_2;
		uint64_t L_8 = Request_get_appId_m2804(__this, /*hidden argument*/NULL);
		uint64_t L_9 = L_8;
		Object_t * L_10 = Box(AppID_t500_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_10);
		String_t* L_11 = Enum_ToString_m3296(L_10, _stringLiteral275, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t60* L_12 = L_7;
		int32_t L_13 = Request_get_domain_m2805(__this, /*hidden argument*/NULL);
		int32_t L_14 = L_13;
		Object_t * L_15 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 3);
		ArrayElementTypeCheck (L_12, L_15);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_12, 3, sizeof(Object_t *))) = (Object_t *)L_15;
		String_t* L_16 = UnityString_Format_m2512(NULL /*static, unused*/, _stringLiteral274, L_12, /*hidden argument*/NULL);
		return L_16;
	}
}
// System.Void UnityEngine.Networking.Match.ResponseBase::.ctor()
extern "C" void ResponseBase__ctor_m2807 (ResponseBase_t484 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Networking.Match.ResponseBase::ParseJSONString(System.String,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern TypeInfo* IDictionary_2_t613_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t653_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral276;
extern "C" String_t* ResponseBase_ParseJSONString_m2808 (ResponseBase_t484 * __this, String_t* ___name, Object_t * ___obj, Object_t* ___dictJsonObj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(345);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		FormatException_t653_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(346);
		_stringLiteral276 = il2cpp_codegen_string_literal_from_index(276);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = ___dictJsonObj;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker2< bool, String_t*, Object_t ** >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::TryGetValue(!0,!1&) */, IDictionary_2_t613_il2cpp_TypeInfo_var, L_0, L_1, (&___obj));
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Object_t * L_3 = ___obj;
		return ((String_t*)IsInstSealed(L_3, String_t_il2cpp_TypeInfo_var));
	}

IL_0015:
	{
		String_t* L_4 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_5 = String_Concat_m217(NULL /*static, unused*/, L_4, _stringLiteral276, /*hidden argument*/NULL);
		FormatException_t653 * L_6 = (FormatException_t653 *)il2cpp_codegen_object_new (FormatException_t653_il2cpp_TypeInfo_var);
		FormatException__ctor_m3297(L_6, L_5, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_6);
	}
}
// System.Int32 UnityEngine.Networking.Match.ResponseBase::ParseJSONInt32(System.String,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern TypeInfo* IDictionary_2_t613_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t645_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t653_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral276;
extern "C" int32_t ResponseBase_ParseJSONInt32_m2809 (ResponseBase_t484 * __this, String_t* ___name, Object_t * ___obj, Object_t* ___dictJsonObj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(345);
		Convert_t645_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(311);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		FormatException_t653_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(346);
		_stringLiteral276 = il2cpp_codegen_string_literal_from_index(276);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = ___dictJsonObj;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker2< bool, String_t*, Object_t ** >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::TryGetValue(!0,!1&) */, IDictionary_2_t613_il2cpp_TypeInfo_var, L_0, L_1, (&___obj));
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Object_t * L_3 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t645_il2cpp_TypeInfo_var);
		int32_t L_4 = Convert_ToInt32_m3298(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0015:
	{
		String_t* L_5 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m217(NULL /*static, unused*/, L_5, _stringLiteral276, /*hidden argument*/NULL);
		FormatException_t653 * L_7 = (FormatException_t653 *)il2cpp_codegen_object_new (FormatException_t653_il2cpp_TypeInfo_var);
		FormatException__ctor_m3297(L_7, L_6, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}
}
// System.UInt16 UnityEngine.Networking.Match.ResponseBase::ParseJSONUInt16(System.String,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern TypeInfo* IDictionary_2_t613_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t645_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t653_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral276;
extern "C" uint16_t ResponseBase_ParseJSONUInt16_m2810 (ResponseBase_t484 * __this, String_t* ___name, Object_t * ___obj, Object_t* ___dictJsonObj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(345);
		Convert_t645_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(311);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		FormatException_t653_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(346);
		_stringLiteral276 = il2cpp_codegen_string_literal_from_index(276);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = ___dictJsonObj;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker2< bool, String_t*, Object_t ** >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::TryGetValue(!0,!1&) */, IDictionary_2_t613_il2cpp_TypeInfo_var, L_0, L_1, (&___obj));
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Object_t * L_3 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t645_il2cpp_TypeInfo_var);
		uint16_t L_4 = Convert_ToUInt16_m3299(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0015:
	{
		String_t* L_5 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m217(NULL /*static, unused*/, L_5, _stringLiteral276, /*hidden argument*/NULL);
		FormatException_t653 * L_7 = (FormatException_t653 *)il2cpp_codegen_object_new (FormatException_t653_il2cpp_TypeInfo_var);
		FormatException__ctor_m3297(L_7, L_6, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}
}
// System.UInt64 UnityEngine.Networking.Match.ResponseBase::ParseJSONUInt64(System.String,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern TypeInfo* IDictionary_2_t613_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t645_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t653_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral276;
extern "C" uint64_t ResponseBase_ParseJSONUInt64_m2811 (ResponseBase_t484 * __this, String_t* ___name, Object_t * ___obj, Object_t* ___dictJsonObj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(345);
		Convert_t645_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(311);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		FormatException_t653_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(346);
		_stringLiteral276 = il2cpp_codegen_string_literal_from_index(276);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = ___dictJsonObj;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker2< bool, String_t*, Object_t ** >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::TryGetValue(!0,!1&) */, IDictionary_2_t613_il2cpp_TypeInfo_var, L_0, L_1, (&___obj));
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Object_t * L_3 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t645_il2cpp_TypeInfo_var);
		uint64_t L_4 = Convert_ToUInt64_m3300(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0015:
	{
		String_t* L_5 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m217(NULL /*static, unused*/, L_5, _stringLiteral276, /*hidden argument*/NULL);
		FormatException_t653 * L_7 = (FormatException_t653 *)il2cpp_codegen_object_new (FormatException_t653_il2cpp_TypeInfo_var);
		FormatException__ctor_m3297(L_7, L_6, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}
}
// System.Boolean UnityEngine.Networking.Match.ResponseBase::ParseJSONBool(System.String,System.Object,System.Collections.Generic.IDictionary`2<System.String,System.Object>)
extern TypeInfo* IDictionary_2_t613_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t645_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t653_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral276;
extern "C" bool ResponseBase_ParseJSONBool_m2812 (ResponseBase_t484 * __this, String_t* ___name, Object_t * ___obj, Object_t* ___dictJsonObj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(345);
		Convert_t645_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(311);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		FormatException_t653_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(346);
		_stringLiteral276 = il2cpp_codegen_string_literal_from_index(276);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t* L_0 = ___dictJsonObj;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)InterfaceFuncInvoker2< bool, String_t*, Object_t ** >::Invoke(1 /* System.Boolean System.Collections.Generic.IDictionary`2<System.String,System.Object>::TryGetValue(!0,!1&) */, IDictionary_2_t613_il2cpp_TypeInfo_var, L_0, L_1, (&___obj));
		if (!L_2)
		{
			goto IL_0015;
		}
	}
	{
		Object_t * L_3 = ___obj;
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t645_il2cpp_TypeInfo_var);
		bool L_4 = Convert_ToBoolean_m3301(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		return L_4;
	}

IL_0015:
	{
		String_t* L_5 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m217(NULL /*static, unused*/, L_5, _stringLiteral276, /*hidden argument*/NULL);
		FormatException_t653 * L_7 = (FormatException_t653 *)il2cpp_codegen_object_new (FormatException_t653_il2cpp_TypeInfo_var);
		FormatException__ctor_m3297(L_7, L_6, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_7);
	}
}
// System.Void UnityEngine.Networking.Match.Response::.ctor()
extern "C" void Response__ctor_m2813 (Response_t485 * __this, const MethodInfo* method)
{
	{
		ResponseBase__ctor_m2807(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityEngine.Networking.Match.Response::get_success()
extern "C" bool Response_get_success_m2814 (Response_t485 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CsuccessU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.Response::set_success(System.Boolean)
extern "C" void Response_set_success_m2815 (Response_t485 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CsuccessU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.Response::get_extendedInfo()
extern "C" String_t* Response_get_extendedInfo_m2816 (Response_t485 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CextendedInfoU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.Response::set_extendedInfo(System.String)
extern "C" void Response_set_extendedInfo_m2817 (Response_t485 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CextendedInfoU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.Response::ToString()
extern TypeInfo* ObjectU5BU5D_t60_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t298_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral277;
extern "C" String_t* Response_ToString_m2818 (Response_t485 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Boolean_t298_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(104);
		_stringLiteral277 = il2cpp_codegen_string_literal_from_index(277);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t60* L_0 = ((ObjectU5BU5D_t60*)SZArrayNew(ObjectU5BU5D_t60_il2cpp_TypeInfo_var, 3));
		String_t* L_1 = Object_ToString_m3295(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t60* L_2 = L_0;
		bool L_3 = Response_get_success_m2814(__this, /*hidden argument*/NULL);
		bool L_4 = L_3;
		Object_t * L_5 = Box(Boolean_t298_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t60* L_6 = L_2;
		String_t* L_7 = Response_get_extendedInfo_m2816(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2, sizeof(Object_t *))) = (Object_t *)L_7;
		String_t* L_8 = UnityString_Format_m2512(NULL /*static, unused*/, _stringLiteral277, L_6, /*hidden argument*/NULL);
		return L_8;
	}
}
// System.Void UnityEngine.Networking.Match.Response::Parse(System.Object)
extern TypeInfo* IDictionary_2_t613_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t653_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral278;
extern Il2CppCodeGenString* _stringLiteral279;
extern Il2CppCodeGenString* _stringLiteral280;
extern "C" void Response_Parse_m2819 (Response_t485 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(345);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		FormatException_t653_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(346);
		_stringLiteral278 = il2cpp_codegen_string_literal_from_index(278);
		_stringLiteral279 = il2cpp_codegen_string_literal_from_index(279);
		_stringLiteral280 = il2cpp_codegen_string_literal_from_index(280);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	{
		Object_t * L_0 = ___obj;
		V_0 = ((Object_t*)IsInst(L_0, IDictionary_2_t613_il2cpp_TypeInfo_var));
		Object_t* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0054;
		}
	}
	{
		Object_t * L_2 = ___obj;
		Object_t* L_3 = V_0;
		bool L_4 = ResponseBase_ParseJSONBool_m2812(__this, _stringLiteral278, L_2, L_3, /*hidden argument*/NULL);
		Response_set_success_m2815(__this, L_4, /*hidden argument*/NULL);
		Object_t * L_5 = ___obj;
		Object_t* L_6 = V_0;
		String_t* L_7 = ResponseBase_ParseJSONString_m2808(__this, _stringLiteral279, L_5, L_6, /*hidden argument*/NULL);
		Response_set_extendedInfo_m2817(__this, L_7, /*hidden argument*/NULL);
		bool L_8 = Response_get_success_m2814(__this, /*hidden argument*/NULL);
		if (L_8)
		{
			goto IL_0054;
		}
	}
	{
		String_t* L_9 = Response_get_extendedInfo_m2816(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m217(NULL /*static, unused*/, _stringLiteral280, L_9, /*hidden argument*/NULL);
		FormatException_t653 * L_11 = (FormatException_t653 *)il2cpp_codegen_object_new (FormatException_t653_il2cpp_TypeInfo_var);
		FormatException__ctor_m3297(L_11, L_10, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_11);
	}

IL_0054:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.Match.BasicResponse::.ctor()
extern "C" void BasicResponse__ctor_m2820 (BasicResponse_t486 * __this, const MethodInfo* method)
{
	{
		Response__ctor_m2813(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::.ctor()
extern "C" void CreateMatchRequest__ctor_m2821 (CreateMatchRequest_t487 * __this, const MethodInfo* method)
{
	{
		Request__ctor_m2802(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Networking.Match.CreateMatchRequest::get_name()
extern "C" String_t* CreateMatchRequest_get_name_m2822 (CreateMatchRequest_t487 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CnameU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::set_name(System.String)
extern "C" void CreateMatchRequest_set_name_m2823 (CreateMatchRequest_t487 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CnameU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.UInt32 UnityEngine.Networking.Match.CreateMatchRequest::get_size()
extern "C" uint32_t CreateMatchRequest_get_size_m2824 (CreateMatchRequest_t487 * __this, const MethodInfo* method)
{
	{
		uint32_t L_0 = (__this->___U3CsizeU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::set_size(System.UInt32)
extern "C" void CreateMatchRequest_set_size_m2825 (CreateMatchRequest_t487 * __this, uint32_t ___value, const MethodInfo* method)
{
	{
		uint32_t L_0 = ___value;
		__this->___U3CsizeU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.Networking.Match.CreateMatchRequest::get_advertise()
extern "C" bool CreateMatchRequest_get_advertise_m2826 (CreateMatchRequest_t487 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CadvertiseU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::set_advertise(System.Boolean)
extern "C" void CreateMatchRequest_set_advertise_m2827 (CreateMatchRequest_t487 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CadvertiseU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.CreateMatchRequest::get_password()
extern "C" String_t* CreateMatchRequest_get_password_m2828 (CreateMatchRequest_t487 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CpasswordU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchRequest::set_password(System.String)
extern "C" void CreateMatchRequest_set_password_m2829 (CreateMatchRequest_t487 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CpasswordU3Ek__BackingField_7 = L_0;
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Int64> UnityEngine.Networking.Match.CreateMatchRequest::get_matchAttributes()
extern "C" Dictionary_2_t488 * CreateMatchRequest_get_matchAttributes_m2830 (CreateMatchRequest_t487 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t488 * L_0 = (__this->___U3CmatchAttributesU3Ek__BackingField_8);
		return L_0;
	}
}
// System.String UnityEngine.Networking.Match.CreateMatchRequest::ToString()
extern TypeInfo* ObjectU5BU5D_t60_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t654_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t298_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t56_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral281;
extern Il2CppCodeGenString* _stringLiteral282;
extern Il2CppCodeGenString* _stringLiteral283;
extern "C" String_t* CreateMatchRequest_ToString_m2831 (CreateMatchRequest_t487 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		UInt32_t654_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(347);
		Boolean_t298_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(104);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Int32_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral281 = il2cpp_codegen_string_literal_from_index(281);
		_stringLiteral282 = il2cpp_codegen_string_literal_from_index(282);
		_stringLiteral283 = il2cpp_codegen_string_literal_from_index(283);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t60* G_B2_1 = {0};
	ObjectU5BU5D_t60* G_B2_2 = {0};
	String_t* G_B2_3 = {0};
	int32_t G_B1_0 = 0;
	ObjectU5BU5D_t60* G_B1_1 = {0};
	ObjectU5BU5D_t60* G_B1_2 = {0};
	String_t* G_B1_3 = {0};
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	ObjectU5BU5D_t60* G_B3_2 = {0};
	ObjectU5BU5D_t60* G_B3_3 = {0};
	String_t* G_B3_4 = {0};
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t60* G_B5_1 = {0};
	ObjectU5BU5D_t60* G_B5_2 = {0};
	String_t* G_B5_3 = {0};
	int32_t G_B4_0 = 0;
	ObjectU5BU5D_t60* G_B4_1 = {0};
	ObjectU5BU5D_t60* G_B4_2 = {0};
	String_t* G_B4_3 = {0};
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	ObjectU5BU5D_t60* G_B6_2 = {0};
	ObjectU5BU5D_t60* G_B6_3 = {0};
	String_t* G_B6_4 = {0};
	{
		ObjectU5BU5D_t60* L_0 = ((ObjectU5BU5D_t60*)SZArrayNew(ObjectU5BU5D_t60_il2cpp_TypeInfo_var, 6));
		String_t* L_1 = Request_ToString_m2806(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t60* L_2 = L_0;
		String_t* L_3 = CreateMatchRequest_get_name_m2822(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t60* L_4 = L_2;
		uint32_t L_5 = CreateMatchRequest_get_size_m2824(__this, /*hidden argument*/NULL);
		uint32_t L_6 = L_5;
		Object_t * L_7 = Box(UInt32_t654_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t60* L_8 = L_4;
		bool L_9 = CreateMatchRequest_get_advertise_m2826(__this, /*hidden argument*/NULL);
		bool L_10 = L_9;
		Object_t * L_11 = Box(Boolean_t298_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t60* L_12 = L_8;
		String_t* L_13 = CreateMatchRequest_get_password_m2828(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		bool L_15 = String_op_Equality_m204(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		G_B1_0 = 4;
		G_B1_1 = L_12;
		G_B1_2 = L_12;
		G_B1_3 = _stringLiteral281;
		if (!L_15)
		{
			G_B2_0 = 4;
			G_B2_1 = L_12;
			G_B2_2 = L_12;
			G_B2_3 = _stringLiteral281;
			goto IL_005a;
		}
	}
	{
		G_B3_0 = _stringLiteral282;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_005f;
	}

IL_005a:
	{
		G_B3_0 = _stringLiteral283;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_005f:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B3_2, G_B3_1, sizeof(Object_t *))) = (Object_t *)G_B3_0;
		ObjectU5BU5D_t60* L_16 = G_B3_3;
		Dictionary_2_t488 * L_17 = CreateMatchRequest_get_matchAttributes_m2830(__this, /*hidden argument*/NULL);
		G_B4_0 = 5;
		G_B4_1 = L_16;
		G_B4_2 = L_16;
		G_B4_3 = G_B3_4;
		if (L_17)
		{
			G_B5_0 = 5;
			G_B5_1 = L_16;
			G_B5_2 = L_16;
			G_B5_3 = G_B3_4;
			goto IL_0073;
		}
	}
	{
		G_B6_0 = 0;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		G_B6_4 = G_B4_3;
		goto IL_007e;
	}

IL_0073:
	{
		Dictionary_2_t488 * L_18 = CreateMatchRequest_get_matchAttributes_m2830(__this, /*hidden argument*/NULL);
		NullCheck(L_18);
		int32_t L_19 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.Int64>::get_Count() */, L_18);
		G_B6_0 = L_19;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
		G_B6_4 = G_B5_3;
	}

IL_007e:
	{
		int32_t L_20 = G_B6_0;
		Object_t * L_21 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_20);
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, L_21);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B6_2, G_B6_1, sizeof(Object_t *))) = (Object_t *)L_21;
		String_t* L_22 = UnityString_Format_m2512(NULL /*static, unused*/, G_B6_4, G_B6_3, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::.ctor()
extern "C" void CreateMatchResponse__ctor_m2832 (CreateMatchResponse_t489 * __this, const MethodInfo* method)
{
	{
		BasicResponse__ctor_m2820(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Networking.Match.CreateMatchResponse::get_address()
extern "C" String_t* CreateMatchResponse_get_address_m2833 (CreateMatchResponse_t489 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CaddressU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_address(System.String)
extern "C" void CreateMatchResponse_set_address_m2834 (CreateMatchResponse_t489 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CaddressU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.Networking.Match.CreateMatchResponse::get_port()
extern "C" int32_t CreateMatchResponse_get_port_m2835 (CreateMatchResponse_t489 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CportU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_port(System.Int32)
extern "C" void CreateMatchResponse_set_port_m2836 (CreateMatchResponse_t489 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CportU3Ek__BackingField_3 = L_0;
		return;
	}
}
// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.CreateMatchResponse::get_networkId()
extern "C" uint64_t CreateMatchResponse_get_networkId_m2837 (CreateMatchResponse_t489 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = (__this->___U3CnetworkIdU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_networkId(UnityEngine.Networking.Types.NetworkID)
extern "C" void CreateMatchResponse_set_networkId_m2838 (CreateMatchResponse_t489 * __this, uint64_t ___value, const MethodInfo* method)
{
	{
		uint64_t L_0 = ___value;
		__this->___U3CnetworkIdU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.CreateMatchResponse::get_accessTokenString()
extern "C" String_t* CreateMatchResponse_get_accessTokenString_m2839 (CreateMatchResponse_t489 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CaccessTokenStringU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_accessTokenString(System.String)
extern "C" void CreateMatchResponse_set_accessTokenString_m2840 (CreateMatchResponse_t489 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CaccessTokenStringU3Ek__BackingField_5 = L_0;
		return;
	}
}
// UnityEngine.Networking.Types.NodeID UnityEngine.Networking.Match.CreateMatchResponse::get_nodeId()
extern "C" uint16_t CreateMatchResponse_get_nodeId_m2841 (CreateMatchResponse_t489 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (__this->___U3CnodeIdU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_nodeId(UnityEngine.Networking.Types.NodeID)
extern "C" void CreateMatchResponse_set_nodeId_m2842 (CreateMatchResponse_t489 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___U3CnodeIdU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.Networking.Match.CreateMatchResponse::get_usingRelay()
extern "C" bool CreateMatchResponse_get_usingRelay_m2843 (CreateMatchResponse_t489 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CusingRelayU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::set_usingRelay(System.Boolean)
extern "C" void CreateMatchResponse_set_usingRelay_m2844 (CreateMatchResponse_t489 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CusingRelayU3Ek__BackingField_7 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.CreateMatchResponse::ToString()
extern TypeInfo* ObjectU5BU5D_t60_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t56_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkID_t502_il2cpp_TypeInfo_var;
extern TypeInfo* NodeID_t503_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t298_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral284;
extern Il2CppCodeGenString* _stringLiteral275;
extern "C" String_t* CreateMatchResponse_ToString_m2845 (CreateMatchResponse_t489 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		NetworkID_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(348);
		NodeID_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(349);
		Boolean_t298_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(104);
		_stringLiteral284 = il2cpp_codegen_string_literal_from_index(284);
		_stringLiteral275 = il2cpp_codegen_string_literal_from_index(275);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t60* L_0 = ((ObjectU5BU5D_t60*)SZArrayNew(ObjectU5BU5D_t60_il2cpp_TypeInfo_var, 6));
		String_t* L_1 = Response_ToString_m2818(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t60* L_2 = L_0;
		String_t* L_3 = CreateMatchResponse_get_address_m2833(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t60* L_4 = L_2;
		int32_t L_5 = CreateMatchResponse_get_port_m2835(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t60* L_8 = L_4;
		uint64_t L_9 = CreateMatchResponse_get_networkId_m2837(__this, /*hidden argument*/NULL);
		uint64_t L_10 = L_9;
		Object_t * L_11 = Box(NetworkID_t502_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_11);
		String_t* L_12 = Enum_ToString_m3296(L_11, _stringLiteral275, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3, sizeof(Object_t *))) = (Object_t *)L_12;
		ObjectU5BU5D_t60* L_13 = L_8;
		uint16_t L_14 = CreateMatchResponse_get_nodeId_m2841(__this, /*hidden argument*/NULL);
		uint16_t L_15 = L_14;
		Object_t * L_16 = Box(NodeID_t503_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_16);
		String_t* L_17 = Enum_ToString_m3296(L_16, _stringLiteral275, /*hidden argument*/NULL);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, L_17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 4, sizeof(Object_t *))) = (Object_t *)L_17;
		ObjectU5BU5D_t60* L_18 = L_13;
		bool L_19 = CreateMatchResponse_get_usingRelay_m2843(__this, /*hidden argument*/NULL);
		bool L_20 = L_19;
		Object_t * L_21 = Box(Boolean_t298_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 5);
		ArrayElementTypeCheck (L_18, L_21);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 5, sizeof(Object_t *))) = (Object_t *)L_21;
		String_t* L_22 = UnityString_Format_m2512(NULL /*static, unused*/, _stringLiteral284, L_18, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.Void UnityEngine.Networking.Match.CreateMatchResponse::Parse(System.Object)
extern TypeInfo* IDictionary_2_t613_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t653_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral285;
extern Il2CppCodeGenString* _stringLiteral286;
extern Il2CppCodeGenString* _stringLiteral287;
extern Il2CppCodeGenString* _stringLiteral288;
extern Il2CppCodeGenString* _stringLiteral289;
extern Il2CppCodeGenString* _stringLiteral290;
extern Il2CppCodeGenString* _stringLiteral291;
extern "C" void CreateMatchResponse_Parse_m2846 (CreateMatchResponse_t489 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(345);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		FormatException_t653_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(346);
		_stringLiteral285 = il2cpp_codegen_string_literal_from_index(285);
		_stringLiteral286 = il2cpp_codegen_string_literal_from_index(286);
		_stringLiteral287 = il2cpp_codegen_string_literal_from_index(287);
		_stringLiteral288 = il2cpp_codegen_string_literal_from_index(288);
		_stringLiteral289 = il2cpp_codegen_string_literal_from_index(289);
		_stringLiteral290 = il2cpp_codegen_string_literal_from_index(290);
		_stringLiteral291 = il2cpp_codegen_string_literal_from_index(291);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	{
		Object_t * L_0 = ___obj;
		Response_Parse_m2819(__this, L_0, /*hidden argument*/NULL);
		Object_t * L_1 = ___obj;
		V_0 = ((Object_t*)IsInst(L_1, IDictionary_2_t613_il2cpp_TypeInfo_var));
		Object_t* L_2 = V_0;
		if (!L_2)
		{
			goto IL_008b;
		}
	}
	{
		Object_t * L_3 = ___obj;
		Object_t* L_4 = V_0;
		String_t* L_5 = ResponseBase_ParseJSONString_m2808(__this, _stringLiteral285, L_3, L_4, /*hidden argument*/NULL);
		CreateMatchResponse_set_address_m2834(__this, L_5, /*hidden argument*/NULL);
		Object_t * L_6 = ___obj;
		Object_t* L_7 = V_0;
		int32_t L_8 = ResponseBase_ParseJSONInt32_m2809(__this, _stringLiteral286, L_6, L_7, /*hidden argument*/NULL);
		CreateMatchResponse_set_port_m2836(__this, L_8, /*hidden argument*/NULL);
		Object_t * L_9 = ___obj;
		Object_t* L_10 = V_0;
		uint64_t L_11 = ResponseBase_ParseJSONUInt64_m2811(__this, _stringLiteral287, L_9, L_10, /*hidden argument*/NULL);
		CreateMatchResponse_set_networkId_m2838(__this, L_11, /*hidden argument*/NULL);
		Object_t * L_12 = ___obj;
		Object_t* L_13 = V_0;
		String_t* L_14 = ResponseBase_ParseJSONString_m2808(__this, _stringLiteral288, L_12, L_13, /*hidden argument*/NULL);
		CreateMatchResponse_set_accessTokenString_m2840(__this, L_14, /*hidden argument*/NULL);
		Object_t * L_15 = ___obj;
		Object_t* L_16 = V_0;
		uint16_t L_17 = ResponseBase_ParseJSONUInt16_m2810(__this, _stringLiteral289, L_15, L_16, /*hidden argument*/NULL);
		CreateMatchResponse_set_nodeId_m2842(__this, L_17, /*hidden argument*/NULL);
		Object_t * L_18 = ___obj;
		Object_t* L_19 = V_0;
		bool L_20 = ResponseBase_ParseJSONBool_m2812(__this, _stringLiteral290, L_18, L_19, /*hidden argument*/NULL);
		CreateMatchResponse_set_usingRelay_m2844(__this, L_20, /*hidden argument*/NULL);
		goto IL_00a1;
	}

IL_008b:
	{
		Object_t * L_21 = ___obj;
		NullCheck(L_21);
		String_t* L_22 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m217(NULL /*static, unused*/, _stringLiteral291, L_22, /*hidden argument*/NULL);
		FormatException_t653 * L_24 = (FormatException_t653 *)il2cpp_codegen_object_new (FormatException_t653_il2cpp_TypeInfo_var);
		FormatException__ctor_m3297(L_24, L_23, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_24);
	}

IL_00a1:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchRequest::.ctor()
extern "C" void JoinMatchRequest__ctor_m2847 (JoinMatchRequest_t490 * __this, const MethodInfo* method)
{
	{
		Request__ctor_m2802(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.JoinMatchRequest::get_networkId()
extern "C" uint64_t JoinMatchRequest_get_networkId_m2848 (JoinMatchRequest_t490 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = (__this->___U3CnetworkIdU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchRequest::set_networkId(UnityEngine.Networking.Types.NetworkID)
extern "C" void JoinMatchRequest_set_networkId_m2849 (JoinMatchRequest_t490 * __this, uint64_t ___value, const MethodInfo* method)
{
	{
		uint64_t L_0 = ___value;
		__this->___U3CnetworkIdU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.JoinMatchRequest::get_password()
extern "C" String_t* JoinMatchRequest_get_password_m2850 (JoinMatchRequest_t490 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CpasswordU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchRequest::set_password(System.String)
extern "C" void JoinMatchRequest_set_password_m2851 (JoinMatchRequest_t490 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CpasswordU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.JoinMatchRequest::ToString()
extern TypeInfo* ObjectU5BU5D_t60_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkID_t502_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral292;
extern Il2CppCodeGenString* _stringLiteral275;
extern Il2CppCodeGenString* _stringLiteral282;
extern Il2CppCodeGenString* _stringLiteral283;
extern "C" String_t* JoinMatchRequest_ToString_m2852 (JoinMatchRequest_t490 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		NetworkID_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(348);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral292 = il2cpp_codegen_string_literal_from_index(292);
		_stringLiteral275 = il2cpp_codegen_string_literal_from_index(275);
		_stringLiteral282 = il2cpp_codegen_string_literal_from_index(282);
		_stringLiteral283 = il2cpp_codegen_string_literal_from_index(283);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t60* G_B2_1 = {0};
	ObjectU5BU5D_t60* G_B2_2 = {0};
	String_t* G_B2_3 = {0};
	int32_t G_B1_0 = 0;
	ObjectU5BU5D_t60* G_B1_1 = {0};
	ObjectU5BU5D_t60* G_B1_2 = {0};
	String_t* G_B1_3 = {0};
	String_t* G_B3_0 = {0};
	int32_t G_B3_1 = 0;
	ObjectU5BU5D_t60* G_B3_2 = {0};
	ObjectU5BU5D_t60* G_B3_3 = {0};
	String_t* G_B3_4 = {0};
	{
		ObjectU5BU5D_t60* L_0 = ((ObjectU5BU5D_t60*)SZArrayNew(ObjectU5BU5D_t60_il2cpp_TypeInfo_var, 3));
		String_t* L_1 = Request_ToString_m2806(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t60* L_2 = L_0;
		uint64_t L_3 = JoinMatchRequest_get_networkId_m2848(__this, /*hidden argument*/NULL);
		uint64_t L_4 = L_3;
		Object_t * L_5 = Box(NetworkID_t502_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_5);
		String_t* L_6 = Enum_ToString_m3296(L_5, _stringLiteral275, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_6;
		ObjectU5BU5D_t60* L_7 = L_2;
		String_t* L_8 = JoinMatchRequest_get_password_m2850(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_9 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		bool L_10 = String_op_Equality_m204(NULL /*static, unused*/, L_8, L_9, /*hidden argument*/NULL);
		G_B1_0 = 2;
		G_B1_1 = L_7;
		G_B1_2 = L_7;
		G_B1_3 = _stringLiteral292;
		if (!L_10)
		{
			G_B2_0 = 2;
			G_B2_1 = L_7;
			G_B2_2 = L_7;
			G_B2_3 = _stringLiteral292;
			goto IL_004d;
		}
	}
	{
		G_B3_0 = _stringLiteral282;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_0052;
	}

IL_004d:
	{
		G_B3_0 = _stringLiteral283;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_0052:
	{
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, G_B3_0);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B3_2, G_B3_1, sizeof(Object_t *))) = (Object_t *)G_B3_0;
		String_t* L_11 = UnityString_Format_m2512(NULL /*static, unused*/, G_B3_4, G_B3_3, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::.ctor()
extern "C" void JoinMatchResponse__ctor_m2853 (JoinMatchResponse_t491 * __this, const MethodInfo* method)
{
	{
		BasicResponse__ctor_m2820(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityEngine.Networking.Match.JoinMatchResponse::get_address()
extern "C" String_t* JoinMatchResponse_get_address_m2854 (JoinMatchResponse_t491 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CaddressU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_address(System.String)
extern "C" void JoinMatchResponse_set_address_m2855 (JoinMatchResponse_t491 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CaddressU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.Networking.Match.JoinMatchResponse::get_port()
extern "C" int32_t JoinMatchResponse_get_port_m2856 (JoinMatchResponse_t491 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CportU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_port(System.Int32)
extern "C" void JoinMatchResponse_set_port_m2857 (JoinMatchResponse_t491 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CportU3Ek__BackingField_3 = L_0;
		return;
	}
}
// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.JoinMatchResponse::get_networkId()
extern "C" uint64_t JoinMatchResponse_get_networkId_m2858 (JoinMatchResponse_t491 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = (__this->___U3CnetworkIdU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_networkId(UnityEngine.Networking.Types.NetworkID)
extern "C" void JoinMatchResponse_set_networkId_m2859 (JoinMatchResponse_t491 * __this, uint64_t ___value, const MethodInfo* method)
{
	{
		uint64_t L_0 = ___value;
		__this->___U3CnetworkIdU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.JoinMatchResponse::get_accessTokenString()
extern "C" String_t* JoinMatchResponse_get_accessTokenString_m2860 (JoinMatchResponse_t491 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CaccessTokenStringU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_accessTokenString(System.String)
extern "C" void JoinMatchResponse_set_accessTokenString_m2861 (JoinMatchResponse_t491 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CaccessTokenStringU3Ek__BackingField_5 = L_0;
		return;
	}
}
// UnityEngine.Networking.Types.NodeID UnityEngine.Networking.Match.JoinMatchResponse::get_nodeId()
extern "C" uint16_t JoinMatchResponse_get_nodeId_m2862 (JoinMatchResponse_t491 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (__this->___U3CnodeIdU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_nodeId(UnityEngine.Networking.Types.NodeID)
extern "C" void JoinMatchResponse_set_nodeId_m2863 (JoinMatchResponse_t491 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___U3CnodeIdU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.Networking.Match.JoinMatchResponse::get_usingRelay()
extern "C" bool JoinMatchResponse_get_usingRelay_m2864 (JoinMatchResponse_t491 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CusingRelayU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::set_usingRelay(System.Boolean)
extern "C" void JoinMatchResponse_set_usingRelay_m2865 (JoinMatchResponse_t491 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CusingRelayU3Ek__BackingField_7 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.JoinMatchResponse::ToString()
extern TypeInfo* ObjectU5BU5D_t60_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t56_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkID_t502_il2cpp_TypeInfo_var;
extern TypeInfo* NodeID_t503_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t298_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral284;
extern Il2CppCodeGenString* _stringLiteral275;
extern "C" String_t* JoinMatchResponse_ToString_m2866 (JoinMatchResponse_t491 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		NetworkID_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(348);
		NodeID_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(349);
		Boolean_t298_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(104);
		_stringLiteral284 = il2cpp_codegen_string_literal_from_index(284);
		_stringLiteral275 = il2cpp_codegen_string_literal_from_index(275);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t60* L_0 = ((ObjectU5BU5D_t60*)SZArrayNew(ObjectU5BU5D_t60_il2cpp_TypeInfo_var, 6));
		String_t* L_1 = Response_ToString_m2818(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t60* L_2 = L_0;
		String_t* L_3 = JoinMatchResponse_get_address_m2854(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_3;
		ObjectU5BU5D_t60* L_4 = L_2;
		int32_t L_5 = JoinMatchResponse_get_port_m2856(__this, /*hidden argument*/NULL);
		int32_t L_6 = L_5;
		Object_t * L_7 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_6);
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_4, 2, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t60* L_8 = L_4;
		uint64_t L_9 = JoinMatchResponse_get_networkId_m2858(__this, /*hidden argument*/NULL);
		uint64_t L_10 = L_9;
		Object_t * L_11 = Box(NetworkID_t502_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_11);
		String_t* L_12 = Enum_ToString_m3296(L_11, _stringLiteral275, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3, sizeof(Object_t *))) = (Object_t *)L_12;
		ObjectU5BU5D_t60* L_13 = L_8;
		uint16_t L_14 = JoinMatchResponse_get_nodeId_m2862(__this, /*hidden argument*/NULL);
		uint16_t L_15 = L_14;
		Object_t * L_16 = Box(NodeID_t503_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_16);
		String_t* L_17 = Enum_ToString_m3296(L_16, _stringLiteral275, /*hidden argument*/NULL);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, L_17);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 4, sizeof(Object_t *))) = (Object_t *)L_17;
		ObjectU5BU5D_t60* L_18 = L_13;
		bool L_19 = JoinMatchResponse_get_usingRelay_m2864(__this, /*hidden argument*/NULL);
		bool L_20 = L_19;
		Object_t * L_21 = Box(Boolean_t298_il2cpp_TypeInfo_var, &L_20);
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 5);
		ArrayElementTypeCheck (L_18, L_21);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_18, 5, sizeof(Object_t *))) = (Object_t *)L_21;
		String_t* L_22 = UnityString_Format_m2512(NULL /*static, unused*/, _stringLiteral284, L_18, /*hidden argument*/NULL);
		return L_22;
	}
}
// System.Void UnityEngine.Networking.Match.JoinMatchResponse::Parse(System.Object)
extern TypeInfo* IDictionary_2_t613_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t653_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral285;
extern Il2CppCodeGenString* _stringLiteral286;
extern Il2CppCodeGenString* _stringLiteral287;
extern Il2CppCodeGenString* _stringLiteral288;
extern Il2CppCodeGenString* _stringLiteral289;
extern Il2CppCodeGenString* _stringLiteral290;
extern Il2CppCodeGenString* _stringLiteral291;
extern "C" void JoinMatchResponse_Parse_m2867 (JoinMatchResponse_t491 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(345);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		FormatException_t653_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(346);
		_stringLiteral285 = il2cpp_codegen_string_literal_from_index(285);
		_stringLiteral286 = il2cpp_codegen_string_literal_from_index(286);
		_stringLiteral287 = il2cpp_codegen_string_literal_from_index(287);
		_stringLiteral288 = il2cpp_codegen_string_literal_from_index(288);
		_stringLiteral289 = il2cpp_codegen_string_literal_from_index(289);
		_stringLiteral290 = il2cpp_codegen_string_literal_from_index(290);
		_stringLiteral291 = il2cpp_codegen_string_literal_from_index(291);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	{
		Object_t * L_0 = ___obj;
		Response_Parse_m2819(__this, L_0, /*hidden argument*/NULL);
		Object_t * L_1 = ___obj;
		V_0 = ((Object_t*)IsInst(L_1, IDictionary_2_t613_il2cpp_TypeInfo_var));
		Object_t* L_2 = V_0;
		if (!L_2)
		{
			goto IL_008b;
		}
	}
	{
		Object_t * L_3 = ___obj;
		Object_t* L_4 = V_0;
		String_t* L_5 = ResponseBase_ParseJSONString_m2808(__this, _stringLiteral285, L_3, L_4, /*hidden argument*/NULL);
		JoinMatchResponse_set_address_m2855(__this, L_5, /*hidden argument*/NULL);
		Object_t * L_6 = ___obj;
		Object_t* L_7 = V_0;
		int32_t L_8 = ResponseBase_ParseJSONInt32_m2809(__this, _stringLiteral286, L_6, L_7, /*hidden argument*/NULL);
		JoinMatchResponse_set_port_m2857(__this, L_8, /*hidden argument*/NULL);
		Object_t * L_9 = ___obj;
		Object_t* L_10 = V_0;
		uint64_t L_11 = ResponseBase_ParseJSONUInt64_m2811(__this, _stringLiteral287, L_9, L_10, /*hidden argument*/NULL);
		JoinMatchResponse_set_networkId_m2859(__this, L_11, /*hidden argument*/NULL);
		Object_t * L_12 = ___obj;
		Object_t* L_13 = V_0;
		String_t* L_14 = ResponseBase_ParseJSONString_m2808(__this, _stringLiteral288, L_12, L_13, /*hidden argument*/NULL);
		JoinMatchResponse_set_accessTokenString_m2861(__this, L_14, /*hidden argument*/NULL);
		Object_t * L_15 = ___obj;
		Object_t* L_16 = V_0;
		uint16_t L_17 = ResponseBase_ParseJSONUInt16_m2810(__this, _stringLiteral289, L_15, L_16, /*hidden argument*/NULL);
		JoinMatchResponse_set_nodeId_m2863(__this, L_17, /*hidden argument*/NULL);
		Object_t * L_18 = ___obj;
		Object_t* L_19 = V_0;
		bool L_20 = ResponseBase_ParseJSONBool_m2812(__this, _stringLiteral290, L_18, L_19, /*hidden argument*/NULL);
		JoinMatchResponse_set_usingRelay_m2865(__this, L_20, /*hidden argument*/NULL);
		goto IL_00a1;
	}

IL_008b:
	{
		Object_t * L_21 = ___obj;
		NullCheck(L_21);
		String_t* L_22 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_21);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_23 = String_Concat_m217(NULL /*static, unused*/, _stringLiteral291, L_22, /*hidden argument*/NULL);
		FormatException_t653 * L_24 = (FormatException_t653 *)il2cpp_codegen_object_new (FormatException_t653_il2cpp_TypeInfo_var);
		FormatException__ctor_m3297(L_24, L_23, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_24);
	}

IL_00a1:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.Match.DestroyMatchRequest::.ctor()
extern "C" void DestroyMatchRequest__ctor_m2868 (DestroyMatchRequest_t492 * __this, const MethodInfo* method)
{
	{
		Request__ctor_m2802(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.DestroyMatchRequest::get_networkId()
extern "C" uint64_t DestroyMatchRequest_get_networkId_m2869 (DestroyMatchRequest_t492 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = (__this->___U3CnetworkIdU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.DestroyMatchRequest::set_networkId(UnityEngine.Networking.Types.NetworkID)
extern "C" void DestroyMatchRequest_set_networkId_m2870 (DestroyMatchRequest_t492 * __this, uint64_t ___value, const MethodInfo* method)
{
	{
		uint64_t L_0 = ___value;
		__this->___U3CnetworkIdU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.DestroyMatchRequest::ToString()
extern TypeInfo* ObjectU5BU5D_t60_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkID_t502_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral293;
extern Il2CppCodeGenString* _stringLiteral275;
extern "C" String_t* DestroyMatchRequest_ToString_m2871 (DestroyMatchRequest_t492 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		NetworkID_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(348);
		_stringLiteral293 = il2cpp_codegen_string_literal_from_index(293);
		_stringLiteral275 = il2cpp_codegen_string_literal_from_index(275);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t60* L_0 = ((ObjectU5BU5D_t60*)SZArrayNew(ObjectU5BU5D_t60_il2cpp_TypeInfo_var, 2));
		String_t* L_1 = Request_ToString_m2806(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t60* L_2 = L_0;
		uint64_t L_3 = DestroyMatchRequest_get_networkId_m2869(__this, /*hidden argument*/NULL);
		uint64_t L_4 = L_3;
		Object_t * L_5 = Box(NetworkID_t502_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_5);
		String_t* L_6 = Enum_ToString_m3296(L_5, _stringLiteral275, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_6;
		String_t* L_7 = UnityString_Format_m2512(NULL /*static, unused*/, _stringLiteral293, L_2, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UnityEngine.Networking.Match.DropConnectionRequest::.ctor()
extern "C" void DropConnectionRequest__ctor_m2872 (DropConnectionRequest_t493 * __this, const MethodInfo* method)
{
	{
		Request__ctor_m2802(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.DropConnectionRequest::get_networkId()
extern "C" uint64_t DropConnectionRequest_get_networkId_m2873 (DropConnectionRequest_t493 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = (__this->___U3CnetworkIdU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.DropConnectionRequest::set_networkId(UnityEngine.Networking.Types.NetworkID)
extern "C" void DropConnectionRequest_set_networkId_m2874 (DropConnectionRequest_t493 * __this, uint64_t ___value, const MethodInfo* method)
{
	{
		uint64_t L_0 = ___value;
		__this->___U3CnetworkIdU3Ek__BackingField_4 = L_0;
		return;
	}
}
// UnityEngine.Networking.Types.NodeID UnityEngine.Networking.Match.DropConnectionRequest::get_nodeId()
extern "C" uint16_t DropConnectionRequest_get_nodeId_m2875 (DropConnectionRequest_t493 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (__this->___U3CnodeIdU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.DropConnectionRequest::set_nodeId(UnityEngine.Networking.Types.NodeID)
extern "C" void DropConnectionRequest_set_nodeId_m2876 (DropConnectionRequest_t493 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___U3CnodeIdU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.DropConnectionRequest::ToString()
extern TypeInfo* ObjectU5BU5D_t60_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkID_t502_il2cpp_TypeInfo_var;
extern TypeInfo* NodeID_t503_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral294;
extern Il2CppCodeGenString* _stringLiteral275;
extern "C" String_t* DropConnectionRequest_ToString_m2877 (DropConnectionRequest_t493 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		NetworkID_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(348);
		NodeID_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(349);
		_stringLiteral294 = il2cpp_codegen_string_literal_from_index(294);
		_stringLiteral275 = il2cpp_codegen_string_literal_from_index(275);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t60* L_0 = ((ObjectU5BU5D_t60*)SZArrayNew(ObjectU5BU5D_t60_il2cpp_TypeInfo_var, 3));
		String_t* L_1 = Request_ToString_m2806(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t60* L_2 = L_0;
		uint64_t L_3 = DropConnectionRequest_get_networkId_m2873(__this, /*hidden argument*/NULL);
		uint64_t L_4 = L_3;
		Object_t * L_5 = Box(NetworkID_t502_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_5);
		String_t* L_6 = Enum_ToString_m3296(L_5, _stringLiteral275, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_6;
		ObjectU5BU5D_t60* L_7 = L_2;
		uint16_t L_8 = DropConnectionRequest_get_nodeId_m2875(__this, /*hidden argument*/NULL);
		uint16_t L_9 = L_8;
		Object_t * L_10 = Box(NodeID_t503_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_10);
		String_t* L_11 = Enum_ToString_m3296(L_10, _stringLiteral275, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 2, sizeof(Object_t *))) = (Object_t *)L_11;
		String_t* L_12 = UnityString_Format_m2512(NULL /*static, unused*/, _stringLiteral294, L_7, /*hidden argument*/NULL);
		return L_12;
	}
}
// System.Void UnityEngine.Networking.Match.ListMatchRequest::.ctor()
extern "C" void ListMatchRequest__ctor_m2878 (ListMatchRequest_t494 * __this, const MethodInfo* method)
{
	{
		Request__ctor_m2802(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Int32 UnityEngine.Networking.Match.ListMatchRequest::get_pageSize()
extern "C" int32_t ListMatchRequest_get_pageSize_m2879 (ListMatchRequest_t494 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CpageSizeU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.ListMatchRequest::set_pageSize(System.Int32)
extern "C" void ListMatchRequest_set_pageSize_m2880 (ListMatchRequest_t494 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CpageSizeU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.Networking.Match.ListMatchRequest::get_pageNum()
extern "C" int32_t ListMatchRequest_get_pageNum_m2881 (ListMatchRequest_t494 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CpageNumU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.ListMatchRequest::set_pageNum(System.Int32)
extern "C" void ListMatchRequest_set_pageNum_m2882 (ListMatchRequest_t494 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CpageNumU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.ListMatchRequest::get_nameFilter()
extern "C" String_t* ListMatchRequest_get_nameFilter_m2883 (ListMatchRequest_t494 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CnameFilterU3Ek__BackingField_6);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.ListMatchRequest::set_nameFilter(System.String)
extern "C" void ListMatchRequest_set_nameFilter_m2884 (ListMatchRequest_t494 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CnameFilterU3Ek__BackingField_6 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.Networking.Match.ListMatchRequest::get_includePasswordMatches()
extern "C" bool ListMatchRequest_get_includePasswordMatches_m2885 (ListMatchRequest_t494 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CincludePasswordMatchesU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Int64> UnityEngine.Networking.Match.ListMatchRequest::get_matchAttributeFilterLessThan()
extern "C" Dictionary_2_t488 * ListMatchRequest_get_matchAttributeFilterLessThan_m2886 (ListMatchRequest_t494 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t488 * L_0 = (__this->___U3CmatchAttributeFilterLessThanU3Ek__BackingField_8);
		return L_0;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Int64> UnityEngine.Networking.Match.ListMatchRequest::get_matchAttributeFilterGreaterThan()
extern "C" Dictionary_2_t488 * ListMatchRequest_get_matchAttributeFilterGreaterThan_m2887 (ListMatchRequest_t494 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t488 * L_0 = (__this->___U3CmatchAttributeFilterGreaterThanU3Ek__BackingField_9);
		return L_0;
	}
}
// System.String UnityEngine.Networking.Match.ListMatchRequest::ToString()
extern TypeInfo* ObjectU5BU5D_t60_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t56_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral295;
extern "C" String_t* ListMatchRequest_ToString_m2888 (ListMatchRequest_t494 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral295 = il2cpp_codegen_string_literal_from_index(295);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t60* G_B2_1 = {0};
	ObjectU5BU5D_t60* G_B2_2 = {0};
	String_t* G_B2_3 = {0};
	int32_t G_B1_0 = 0;
	ObjectU5BU5D_t60* G_B1_1 = {0};
	ObjectU5BU5D_t60* G_B1_2 = {0};
	String_t* G_B1_3 = {0};
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	ObjectU5BU5D_t60* G_B3_2 = {0};
	ObjectU5BU5D_t60* G_B3_3 = {0};
	String_t* G_B3_4 = {0};
	int32_t G_B5_0 = 0;
	ObjectU5BU5D_t60* G_B5_1 = {0};
	ObjectU5BU5D_t60* G_B5_2 = {0};
	String_t* G_B5_3 = {0};
	int32_t G_B4_0 = 0;
	ObjectU5BU5D_t60* G_B4_1 = {0};
	ObjectU5BU5D_t60* G_B4_2 = {0};
	String_t* G_B4_3 = {0};
	int32_t G_B6_0 = 0;
	int32_t G_B6_1 = 0;
	ObjectU5BU5D_t60* G_B6_2 = {0};
	ObjectU5BU5D_t60* G_B6_3 = {0};
	String_t* G_B6_4 = {0};
	{
		ObjectU5BU5D_t60* L_0 = ((ObjectU5BU5D_t60*)SZArrayNew(ObjectU5BU5D_t60_il2cpp_TypeInfo_var, 6));
		String_t* L_1 = Request_ToString_m2806(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t60* L_2 = L_0;
		int32_t L_3 = ListMatchRequest_get_pageSize_m2879(__this, /*hidden argument*/NULL);
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t60* L_6 = L_2;
		int32_t L_7 = ListMatchRequest_get_pageNum_m2881(__this, /*hidden argument*/NULL);
		int32_t L_8 = L_7;
		Object_t * L_9 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_8);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2, sizeof(Object_t *))) = (Object_t *)L_9;
		ObjectU5BU5D_t60* L_10 = L_6;
		String_t* L_11 = ListMatchRequest_get_nameFilter_m2883(__this, /*hidden argument*/NULL);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 3);
		ArrayElementTypeCheck (L_10, L_11);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_10, 3, sizeof(Object_t *))) = (Object_t *)L_11;
		ObjectU5BU5D_t60* L_12 = L_10;
		Dictionary_2_t488 * L_13 = ListMatchRequest_get_matchAttributeFilterLessThan_m2886(__this, /*hidden argument*/NULL);
		G_B1_0 = 4;
		G_B1_1 = L_12;
		G_B1_2 = L_12;
		G_B1_3 = _stringLiteral295;
		if (L_13)
		{
			G_B2_0 = 4;
			G_B2_1 = L_12;
			G_B2_2 = L_12;
			G_B2_3 = _stringLiteral295;
			goto IL_004c;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_0057;
	}

IL_004c:
	{
		Dictionary_2_t488 * L_14 = ListMatchRequest_get_matchAttributeFilterLessThan_m2886(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.Int64>::get_Count() */, L_14);
		G_B3_0 = L_15;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_0057:
	{
		int32_t L_16 = G_B3_0;
		Object_t * L_17 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_16);
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, L_17);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B3_2, G_B3_1, sizeof(Object_t *))) = (Object_t *)L_17;
		ObjectU5BU5D_t60* L_18 = G_B3_3;
		Dictionary_2_t488 * L_19 = ListMatchRequest_get_matchAttributeFilterGreaterThan_m2887(__this, /*hidden argument*/NULL);
		G_B4_0 = 5;
		G_B4_1 = L_18;
		G_B4_2 = L_18;
		G_B4_3 = G_B3_4;
		if (L_19)
		{
			G_B5_0 = 5;
			G_B5_1 = L_18;
			G_B5_2 = L_18;
			G_B5_3 = G_B3_4;
			goto IL_0070;
		}
	}
	{
		G_B6_0 = 0;
		G_B6_1 = G_B4_0;
		G_B6_2 = G_B4_1;
		G_B6_3 = G_B4_2;
		G_B6_4 = G_B4_3;
		goto IL_007b;
	}

IL_0070:
	{
		Dictionary_2_t488 * L_20 = ListMatchRequest_get_matchAttributeFilterGreaterThan_m2887(__this, /*hidden argument*/NULL);
		NullCheck(L_20);
		int32_t L_21 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.Int64>::get_Count() */, L_20);
		G_B6_0 = L_21;
		G_B6_1 = G_B5_0;
		G_B6_2 = G_B5_1;
		G_B6_3 = G_B5_2;
		G_B6_4 = G_B5_3;
	}

IL_007b:
	{
		int32_t L_22 = G_B6_0;
		Object_t * L_23 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_22);
		NullCheck(G_B6_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B6_2, G_B6_1);
		ArrayElementTypeCheck (G_B6_2, L_23);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B6_2, G_B6_1, sizeof(Object_t *))) = (Object_t *)L_23;
		String_t* L_24 = UnityString_Format_m2512(NULL /*static, unused*/, G_B6_4, G_B6_3, /*hidden argument*/NULL);
		return L_24;
	}
}
// System.Void UnityEngine.Networking.Match.MatchDirectConnectInfo::.ctor()
extern "C" void MatchDirectConnectInfo__ctor_m2889 (MatchDirectConnectInfo_t495 * __this, const MethodInfo* method)
{
	{
		ResponseBase__ctor_m2807(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Networking.Types.NodeID UnityEngine.Networking.Match.MatchDirectConnectInfo::get_nodeId()
extern "C" uint16_t MatchDirectConnectInfo_get_nodeId_m2890 (MatchDirectConnectInfo_t495 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (__this->___U3CnodeIdU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.MatchDirectConnectInfo::set_nodeId(UnityEngine.Networking.Types.NodeID)
extern "C" void MatchDirectConnectInfo_set_nodeId_m2891 (MatchDirectConnectInfo_t495 * __this, uint16_t ___value, const MethodInfo* method)
{
	{
		uint16_t L_0 = ___value;
		__this->___U3CnodeIdU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.MatchDirectConnectInfo::get_publicAddress()
extern "C" String_t* MatchDirectConnectInfo_get_publicAddress_m2892 (MatchDirectConnectInfo_t495 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CpublicAddressU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.MatchDirectConnectInfo::set_publicAddress(System.String)
extern "C" void MatchDirectConnectInfo_set_publicAddress_m2893 (MatchDirectConnectInfo_t495 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CpublicAddressU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.MatchDirectConnectInfo::get_privateAddress()
extern "C" String_t* MatchDirectConnectInfo_get_privateAddress_m2894 (MatchDirectConnectInfo_t495 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CprivateAddressU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.MatchDirectConnectInfo::set_privateAddress(System.String)
extern "C" void MatchDirectConnectInfo_set_privateAddress_m2895 (MatchDirectConnectInfo_t495 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CprivateAddressU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.MatchDirectConnectInfo::ToString()
extern TypeInfo* ObjectU5BU5D_t60_il2cpp_TypeInfo_var;
extern TypeInfo* NodeID_t503_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral296;
extern "C" String_t* MatchDirectConnectInfo_ToString_m2896 (MatchDirectConnectInfo_t495 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		NodeID_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(349);
		_stringLiteral296 = il2cpp_codegen_string_literal_from_index(296);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t60* L_0 = ((ObjectU5BU5D_t60*)SZArrayNew(ObjectU5BU5D_t60_il2cpp_TypeInfo_var, 4));
		String_t* L_1 = Object_ToString_m3295(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t60* L_2 = L_0;
		uint16_t L_3 = MatchDirectConnectInfo_get_nodeId_m2890(__this, /*hidden argument*/NULL);
		uint16_t L_4 = L_3;
		Object_t * L_5 = Box(NodeID_t503_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_5);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_5;
		ObjectU5BU5D_t60* L_6 = L_2;
		String_t* L_7 = MatchDirectConnectInfo_get_publicAddress_m2892(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, 2);
		ArrayElementTypeCheck (L_6, L_7);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_6, 2, sizeof(Object_t *))) = (Object_t *)L_7;
		ObjectU5BU5D_t60* L_8 = L_6;
		String_t* L_9 = MatchDirectConnectInfo_get_privateAddress_m2894(__this, /*hidden argument*/NULL);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 3);
		ArrayElementTypeCheck (L_8, L_9);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_8, 3, sizeof(Object_t *))) = (Object_t *)L_9;
		String_t* L_10 = UnityString_Format_m2512(NULL /*static, unused*/, _stringLiteral296, L_8, /*hidden argument*/NULL);
		return L_10;
	}
}
// System.Void UnityEngine.Networking.Match.MatchDirectConnectInfo::Parse(System.Object)
extern TypeInfo* IDictionary_2_t613_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t653_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral289;
extern Il2CppCodeGenString* _stringLiteral297;
extern Il2CppCodeGenString* _stringLiteral298;
extern Il2CppCodeGenString* _stringLiteral291;
extern "C" void MatchDirectConnectInfo_Parse_m2897 (MatchDirectConnectInfo_t495 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(345);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		FormatException_t653_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(346);
		_stringLiteral289 = il2cpp_codegen_string_literal_from_index(289);
		_stringLiteral297 = il2cpp_codegen_string_literal_from_index(297);
		_stringLiteral298 = il2cpp_codegen_string_literal_from_index(298);
		_stringLiteral291 = il2cpp_codegen_string_literal_from_index(291);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	{
		Object_t * L_0 = ___obj;
		V_0 = ((Object_t*)IsInst(L_0, IDictionary_2_t613_il2cpp_TypeInfo_var));
		Object_t* L_1 = V_0;
		if (!L_1)
		{
			goto IL_004b;
		}
	}
	{
		Object_t * L_2 = ___obj;
		Object_t* L_3 = V_0;
		uint16_t L_4 = ResponseBase_ParseJSONUInt16_m2810(__this, _stringLiteral289, L_2, L_3, /*hidden argument*/NULL);
		MatchDirectConnectInfo_set_nodeId_m2891(__this, L_4, /*hidden argument*/NULL);
		Object_t * L_5 = ___obj;
		Object_t* L_6 = V_0;
		String_t* L_7 = ResponseBase_ParseJSONString_m2808(__this, _stringLiteral297, L_5, L_6, /*hidden argument*/NULL);
		MatchDirectConnectInfo_set_publicAddress_m2893(__this, L_7, /*hidden argument*/NULL);
		Object_t * L_8 = ___obj;
		Object_t* L_9 = V_0;
		String_t* L_10 = ResponseBase_ParseJSONString_m2808(__this, _stringLiteral298, L_8, L_9, /*hidden argument*/NULL);
		MatchDirectConnectInfo_set_privateAddress_m2895(__this, L_10, /*hidden argument*/NULL);
		goto IL_0061;
	}

IL_004b:
	{
		Object_t * L_11 = ___obj;
		NullCheck(L_11);
		String_t* L_12 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_11);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m217(NULL /*static, unused*/, _stringLiteral291, L_12, /*hidden argument*/NULL);
		FormatException_t653 * L_14 = (FormatException_t653 *)il2cpp_codegen_object_new (FormatException_t653_il2cpp_TypeInfo_var);
		FormatException__ctor_m3297(L_14, L_13, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_14);
	}

IL_0061:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.Match.MatchDesc::.ctor()
extern "C" void MatchDesc__ctor_m2898 (MatchDesc_t496 * __this, const MethodInfo* method)
{
	{
		ResponseBase__ctor_m2807(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Networking.Types.NetworkID UnityEngine.Networking.Match.MatchDesc::get_networkId()
extern "C" uint64_t MatchDesc_get_networkId_m2899 (MatchDesc_t496 * __this, const MethodInfo* method)
{
	{
		uint64_t L_0 = (__this->___U3CnetworkIdU3Ek__BackingField_0);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.MatchDesc::set_networkId(UnityEngine.Networking.Types.NetworkID)
extern "C" void MatchDesc_set_networkId_m2900 (MatchDesc_t496 * __this, uint64_t ___value, const MethodInfo* method)
{
	{
		uint64_t L_0 = ___value;
		__this->___U3CnetworkIdU3Ek__BackingField_0 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.MatchDesc::get_name()
extern "C" String_t* MatchDesc_get_name_m2901 (MatchDesc_t496 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CnameU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.MatchDesc::set_name(System.String)
extern "C" void MatchDesc_set_name_m2902 (MatchDesc_t496 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CnameU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.Networking.Match.MatchDesc::get_averageEloScore()
extern "C" int32_t MatchDesc_get_averageEloScore_m2903 (MatchDesc_t496 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CaverageEloScoreU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Int32 UnityEngine.Networking.Match.MatchDesc::get_maxSize()
extern "C" int32_t MatchDesc_get_maxSize_m2904 (MatchDesc_t496 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CmaxSizeU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.MatchDesc::set_maxSize(System.Int32)
extern "C" void MatchDesc_set_maxSize_m2905 (MatchDesc_t496 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CmaxSizeU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.Int32 UnityEngine.Networking.Match.MatchDesc::get_currentSize()
extern "C" int32_t MatchDesc_get_currentSize_m2906 (MatchDesc_t496 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___U3CcurrentSizeU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.MatchDesc::set_currentSize(System.Int32)
extern "C" void MatchDesc_set_currentSize_m2907 (MatchDesc_t496 * __this, int32_t ___value, const MethodInfo* method)
{
	{
		int32_t L_0 = ___value;
		__this->___U3CcurrentSizeU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Boolean UnityEngine.Networking.Match.MatchDesc::get_isPrivate()
extern "C" bool MatchDesc_get_isPrivate_m2908 (MatchDesc_t496 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CisPrivateU3Ek__BackingField_5);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.MatchDesc::set_isPrivate(System.Boolean)
extern "C" void MatchDesc_set_isPrivate_m2909 (MatchDesc_t496 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CisPrivateU3Ek__BackingField_5 = L_0;
		return;
	}
}
// System.Collections.Generic.Dictionary`2<System.String,System.Int64> UnityEngine.Networking.Match.MatchDesc::get_matchAttributes()
extern "C" Dictionary_2_t488 * MatchDesc_get_matchAttributes_m2910 (MatchDesc_t496 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t488 * L_0 = (__this->___U3CmatchAttributesU3Ek__BackingField_6);
		return L_0;
	}
}
// UnityEngine.Networking.Types.NodeID UnityEngine.Networking.Match.MatchDesc::get_hostNodeId()
extern "C" uint16_t MatchDesc_get_hostNodeId_m2911 (MatchDesc_t496 * __this, const MethodInfo* method)
{
	{
		uint16_t L_0 = (__this->___U3ChostNodeIdU3Ek__BackingField_7);
		return L_0;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo> UnityEngine.Networking.Match.MatchDesc::get_directConnectInfos()
extern "C" List_1_t497 * MatchDesc_get_directConnectInfos_m2912 (MatchDesc_t496 * __this, const MethodInfo* method)
{
	{
		List_1_t497 * L_0 = (__this->___U3CdirectConnectInfosU3Ek__BackingField_8);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.MatchDesc::set_directConnectInfos(System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>)
extern "C" void MatchDesc_set_directConnectInfos_m2913 (MatchDesc_t496 * __this, List_1_t497 * ___value, const MethodInfo* method)
{
	{
		List_1_t497 * L_0 = ___value;
		__this->___U3CdirectConnectInfosU3Ek__BackingField_8 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.MatchDesc::ToString()
extern TypeInfo* ObjectU5BU5D_t60_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkID_t502_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t56_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t298_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral299;
extern Il2CppCodeGenString* _stringLiteral275;
extern "C" String_t* MatchDesc_ToString_m2914 (MatchDesc_t496 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		NetworkID_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(348);
		Int32_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		Boolean_t298_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(104);
		_stringLiteral299 = il2cpp_codegen_string_literal_from_index(299);
		_stringLiteral275 = il2cpp_codegen_string_literal_from_index(275);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B2_0 = 0;
	ObjectU5BU5D_t60* G_B2_1 = {0};
	ObjectU5BU5D_t60* G_B2_2 = {0};
	String_t* G_B2_3 = {0};
	int32_t G_B1_0 = 0;
	ObjectU5BU5D_t60* G_B1_1 = {0};
	ObjectU5BU5D_t60* G_B1_2 = {0};
	String_t* G_B1_3 = {0};
	int32_t G_B3_0 = 0;
	int32_t G_B3_1 = 0;
	ObjectU5BU5D_t60* G_B3_2 = {0};
	ObjectU5BU5D_t60* G_B3_3 = {0};
	String_t* G_B3_4 = {0};
	{
		ObjectU5BU5D_t60* L_0 = ((ObjectU5BU5D_t60*)SZArrayNew(ObjectU5BU5D_t60_il2cpp_TypeInfo_var, ((int32_t)9)));
		String_t* L_1 = Object_ToString_m3295(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t60* L_2 = L_0;
		uint64_t L_3 = MatchDesc_get_networkId_m2899(__this, /*hidden argument*/NULL);
		uint64_t L_4 = L_3;
		Object_t * L_5 = Box(NetworkID_t502_il2cpp_TypeInfo_var, &L_4);
		NullCheck(L_5);
		String_t* L_6 = Enum_ToString_m3296(L_5, _stringLiteral275, /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_6;
		ObjectU5BU5D_t60* L_7 = L_2;
		String_t* L_8 = MatchDesc_get_name_m2901(__this, /*hidden argument*/NULL);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, 2);
		ArrayElementTypeCheck (L_7, L_8);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_7, 2, sizeof(Object_t *))) = (Object_t *)L_8;
		ObjectU5BU5D_t60* L_9 = L_7;
		int32_t L_10 = MatchDesc_get_averageEloScore_m2903(__this, /*hidden argument*/NULL);
		int32_t L_11 = L_10;
		Object_t * L_12 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_11);
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, 3);
		ArrayElementTypeCheck (L_9, L_12);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_9, 3, sizeof(Object_t *))) = (Object_t *)L_12;
		ObjectU5BU5D_t60* L_13 = L_9;
		int32_t L_14 = MatchDesc_get_maxSize_m2904(__this, /*hidden argument*/NULL);
		int32_t L_15 = L_14;
		Object_t * L_16 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 4);
		ArrayElementTypeCheck (L_13, L_16);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_13, 4, sizeof(Object_t *))) = (Object_t *)L_16;
		ObjectU5BU5D_t60* L_17 = L_13;
		int32_t L_18 = MatchDesc_get_currentSize_m2906(__this, /*hidden argument*/NULL);
		int32_t L_19 = L_18;
		Object_t * L_20 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_19);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 5);
		ArrayElementTypeCheck (L_17, L_20);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_17, 5, sizeof(Object_t *))) = (Object_t *)L_20;
		ObjectU5BU5D_t60* L_21 = L_17;
		bool L_22 = MatchDesc_get_isPrivate_m2908(__this, /*hidden argument*/NULL);
		bool L_23 = L_22;
		Object_t * L_24 = Box(Boolean_t298_il2cpp_TypeInfo_var, &L_23);
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 6);
		ArrayElementTypeCheck (L_21, L_24);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_21, 6, sizeof(Object_t *))) = (Object_t *)L_24;
		ObjectU5BU5D_t60* L_25 = L_21;
		Dictionary_2_t488 * L_26 = MatchDesc_get_matchAttributes_m2910(__this, /*hidden argument*/NULL);
		G_B1_0 = 7;
		G_B1_1 = L_25;
		G_B1_2 = L_25;
		G_B1_3 = _stringLiteral299;
		if (L_26)
		{
			G_B2_0 = 7;
			G_B2_1 = L_25;
			G_B2_2 = L_25;
			G_B2_3 = _stringLiteral299;
			goto IL_0081;
		}
	}
	{
		G_B3_0 = 0;
		G_B3_1 = G_B1_0;
		G_B3_2 = G_B1_1;
		G_B3_3 = G_B1_2;
		G_B3_4 = G_B1_3;
		goto IL_008c;
	}

IL_0081:
	{
		Dictionary_2_t488 * L_27 = MatchDesc_get_matchAttributes_m2910(__this, /*hidden argument*/NULL);
		NullCheck(L_27);
		int32_t L_28 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.Int64>::get_Count() */, L_27);
		G_B3_0 = L_28;
		G_B3_1 = G_B2_0;
		G_B3_2 = G_B2_1;
		G_B3_3 = G_B2_2;
		G_B3_4 = G_B2_3;
	}

IL_008c:
	{
		int32_t L_29 = G_B3_0;
		Object_t * L_30 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_29);
		NullCheck(G_B3_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(G_B3_2, G_B3_1);
		ArrayElementTypeCheck (G_B3_2, L_30);
		*((Object_t **)(Object_t **)SZArrayLdElema(G_B3_2, G_B3_1, sizeof(Object_t *))) = (Object_t *)L_30;
		ObjectU5BU5D_t60* L_31 = G_B3_3;
		List_1_t497 * L_32 = MatchDesc_get_directConnectInfos_m2912(__this, /*hidden argument*/NULL);
		NullCheck(L_32);
		int32_t L_33 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDirectConnectInfo>::get_Count() */, L_32);
		int32_t L_34 = L_33;
		Object_t * L_35 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_34);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 8);
		ArrayElementTypeCheck (L_31, L_35);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_31, 8, sizeof(Object_t *))) = (Object_t *)L_35;
		String_t* L_36 = UnityString_Format_m2512(NULL /*static, unused*/, G_B3_4, L_31, /*hidden argument*/NULL);
		return L_36;
	}
}
// System.Void UnityEngine.Networking.Match.MatchDesc::Parse(System.Object)
extern TypeInfo* IDictionary_2_t613_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t653_il2cpp_TypeInfo_var;
extern const MethodInfo* ResponseBase_ParseJSONList_TisMatchDirectConnectInfo_t495_m3302_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral287;
extern Il2CppCodeGenString* _stringLiteral300;
extern Il2CppCodeGenString* _stringLiteral301;
extern Il2CppCodeGenString* _stringLiteral302;
extern Il2CppCodeGenString* _stringLiteral303;
extern Il2CppCodeGenString* _stringLiteral304;
extern Il2CppCodeGenString* _stringLiteral291;
extern "C" void MatchDesc_Parse_m2915 (MatchDesc_t496 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(345);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		FormatException_t653_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(346);
		ResponseBase_ParseJSONList_TisMatchDirectConnectInfo_t495_m3302_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483922);
		_stringLiteral287 = il2cpp_codegen_string_literal_from_index(287);
		_stringLiteral300 = il2cpp_codegen_string_literal_from_index(300);
		_stringLiteral301 = il2cpp_codegen_string_literal_from_index(301);
		_stringLiteral302 = il2cpp_codegen_string_literal_from_index(302);
		_stringLiteral303 = il2cpp_codegen_string_literal_from_index(303);
		_stringLiteral304 = il2cpp_codegen_string_literal_from_index(304);
		_stringLiteral291 = il2cpp_codegen_string_literal_from_index(291);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	{
		Object_t * L_0 = ___obj;
		V_0 = ((Object_t*)IsInst(L_0, IDictionary_2_t613_il2cpp_TypeInfo_var));
		Object_t* L_1 = V_0;
		if (!L_1)
		{
			goto IL_0084;
		}
	}
	{
		Object_t * L_2 = ___obj;
		Object_t* L_3 = V_0;
		uint64_t L_4 = ResponseBase_ParseJSONUInt64_m2811(__this, _stringLiteral287, L_2, L_3, /*hidden argument*/NULL);
		MatchDesc_set_networkId_m2900(__this, L_4, /*hidden argument*/NULL);
		Object_t * L_5 = ___obj;
		Object_t* L_6 = V_0;
		String_t* L_7 = ResponseBase_ParseJSONString_m2808(__this, _stringLiteral300, L_5, L_6, /*hidden argument*/NULL);
		MatchDesc_set_name_m2902(__this, L_7, /*hidden argument*/NULL);
		Object_t * L_8 = ___obj;
		Object_t* L_9 = V_0;
		int32_t L_10 = ResponseBase_ParseJSONInt32_m2809(__this, _stringLiteral301, L_8, L_9, /*hidden argument*/NULL);
		MatchDesc_set_maxSize_m2905(__this, L_10, /*hidden argument*/NULL);
		Object_t * L_11 = ___obj;
		Object_t* L_12 = V_0;
		int32_t L_13 = ResponseBase_ParseJSONInt32_m2809(__this, _stringLiteral302, L_11, L_12, /*hidden argument*/NULL);
		MatchDesc_set_currentSize_m2907(__this, L_13, /*hidden argument*/NULL);
		Object_t * L_14 = ___obj;
		Object_t* L_15 = V_0;
		bool L_16 = ResponseBase_ParseJSONBool_m2812(__this, _stringLiteral303, L_14, L_15, /*hidden argument*/NULL);
		MatchDesc_set_isPrivate_m2909(__this, L_16, /*hidden argument*/NULL);
		Object_t * L_17 = ___obj;
		Object_t* L_18 = V_0;
		List_1_t497 * L_19 = ResponseBase_ParseJSONList_TisMatchDirectConnectInfo_t495_m3302(__this, _stringLiteral304, L_17, L_18, /*hidden argument*/ResponseBase_ParseJSONList_TisMatchDirectConnectInfo_t495_m3302_MethodInfo_var);
		MatchDesc_set_directConnectInfos_m2913(__this, L_19, /*hidden argument*/NULL);
		goto IL_009a;
	}

IL_0084:
	{
		Object_t * L_20 = ___obj;
		NullCheck(L_20);
		String_t* L_21 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_20);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_22 = String_Concat_m217(NULL /*static, unused*/, _stringLiteral291, L_21, /*hidden argument*/NULL);
		FormatException_t653 * L_23 = (FormatException_t653 *)il2cpp_codegen_object_new (FormatException_t653_il2cpp_TypeInfo_var);
		FormatException__ctor_m3297(L_23, L_22, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_23);
	}

IL_009a:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.Match.ListMatchResponse::.ctor()
extern "C" void ListMatchResponse__ctor_m2916 (ListMatchResponse_t498 * __this, const MethodInfo* method)
{
	{
		BasicResponse__ctor_m2820(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc> UnityEngine.Networking.Match.ListMatchResponse::get_matches()
extern "C" List_1_t499 * ListMatchResponse_get_matches_m2917 (ListMatchResponse_t498 * __this, const MethodInfo* method)
{
	{
		List_1_t499 * L_0 = (__this->___U3CmatchesU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.ListMatchResponse::set_matches(System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>)
extern "C" void ListMatchResponse_set_matches_m2918 (ListMatchResponse_t498 * __this, List_1_t499 * ___value, const MethodInfo* method)
{
	{
		List_1_t499 * L_0 = ___value;
		__this->___U3CmatchesU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.String UnityEngine.Networking.Match.ListMatchResponse::ToString()
extern TypeInfo* ObjectU5BU5D_t60_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t56_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral305;
extern "C" String_t* ListMatchResponse_ToString_m2919 (ListMatchResponse_t498 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Int32_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral305 = il2cpp_codegen_string_literal_from_index(305);
		s_Il2CppMethodIntialized = true;
	}
	{
		ObjectU5BU5D_t60* L_0 = ((ObjectU5BU5D_t60*)SZArrayNew(ObjectU5BU5D_t60_il2cpp_TypeInfo_var, 2));
		String_t* L_1 = Response_ToString_m2818(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_0, 0, sizeof(Object_t *))) = (Object_t *)L_1;
		ObjectU5BU5D_t60* L_2 = L_0;
		List_1_t499 * L_3 = ListMatchResponse_get_matches_m2917(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		int32_t L_4 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(20 /* System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::get_Count() */, L_3);
		int32_t L_5 = L_4;
		Object_t * L_6 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_5);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 1);
		ArrayElementTypeCheck (L_2, L_6);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 1, sizeof(Object_t *))) = (Object_t *)L_6;
		String_t* L_7 = UnityString_Format_m2512(NULL /*static, unused*/, _stringLiteral305, L_2, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UnityEngine.Networking.Match.ListMatchResponse::Parse(System.Object)
extern TypeInfo* IDictionary_2_t613_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* FormatException_t653_il2cpp_TypeInfo_var;
extern const MethodInfo* ResponseBase_ParseJSONList_TisMatchDesc_t496_m3303_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral306;
extern Il2CppCodeGenString* _stringLiteral291;
extern "C" void ListMatchResponse_Parse_m2920 (ListMatchResponse_t498 * __this, Object_t * ___obj, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IDictionary_2_t613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(345);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		FormatException_t653_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(346);
		ResponseBase_ParseJSONList_TisMatchDesc_t496_m3303_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483923);
		_stringLiteral306 = il2cpp_codegen_string_literal_from_index(306);
		_stringLiteral291 = il2cpp_codegen_string_literal_from_index(291);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	{
		Object_t * L_0 = ___obj;
		Response_Parse_m2819(__this, L_0, /*hidden argument*/NULL);
		Object_t * L_1 = ___obj;
		V_0 = ((Object_t*)IsInst(L_1, IDictionary_2_t613_il2cpp_TypeInfo_var));
		Object_t* L_2 = V_0;
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		Object_t * L_3 = ___obj;
		Object_t* L_4 = V_0;
		List_1_t499 * L_5 = ResponseBase_ParseJSONList_TisMatchDesc_t496_m3303(__this, _stringLiteral306, L_3, L_4, /*hidden argument*/ResponseBase_ParseJSONList_TisMatchDesc_t496_m3303_MethodInfo_var);
		ListMatchResponse_set_matches_m2918(__this, L_5, /*hidden argument*/NULL);
		goto IL_0042;
	}

IL_002c:
	{
		Object_t * L_6 = ___obj;
		NullCheck(L_6);
		String_t* L_7 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_6);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_8 = String_Concat_m217(NULL /*static, unused*/, _stringLiteral291, L_7, /*hidden argument*/NULL);
		FormatException_t653 * L_9 = (FormatException_t653 *)il2cpp_codegen_object_new (FormatException_t653_il2cpp_TypeInfo_var);
		FormatException__ctor_m3297(L_9, L_8, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_9);
	}

IL_0042:
	{
		return;
	}
}
// System.Void UnityEngine.Networking.Types.NetworkAccessToken::.ctor()
extern TypeInfo* ByteU5BU5D_t435_il2cpp_TypeInfo_var;
extern "C" void NetworkAccessToken__ctor_m2921 (NetworkAccessToken_t504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ByteU5BU5D_t435_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(326);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		__this->___array_0 = ((ByteU5BU5D_t435*)SZArrayNew(ByteU5BU5D_t435_il2cpp_TypeInfo_var, ((int32_t)64)));
		return;
	}
}
// System.String UnityEngine.Networking.Types.NetworkAccessToken::GetByteString()
extern TypeInfo* Convert_t645_il2cpp_TypeInfo_var;
extern "C" String_t* NetworkAccessToken_GetByteString_m2922 (NetworkAccessToken_t504 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Convert_t645_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(311);
		s_Il2CppMethodIntialized = true;
	}
	{
		ByteU5BU5D_t435* L_0 = (__this->___array_0);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t645_il2cpp_TypeInfo_var);
		String_t* L_1 = Convert_ToBase64String_m3304(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityEngine.Networking.Utility::.cctor()
extern TypeInfo* Random_t506_il2cpp_TypeInfo_var;
extern TypeInfo* Utility_t505_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t507_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3307_MethodInfo_var;
extern "C" void Utility__cctor_m2923 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Random_t506_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(353);
		Utility_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(354);
		Dictionary_2_t507_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(355);
		Dictionary_2__ctor_m3307_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483924);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = Environment_get_TickCount_m3305(NULL /*static, unused*/, /*hidden argument*/NULL);
		Random_t506 * L_1 = (Random_t506 *)il2cpp_codegen_object_new (Random_t506_il2cpp_TypeInfo_var);
		Random__ctor_m3306(L_1, L_0, /*hidden argument*/NULL);
		((Utility_t505_StaticFields*)Utility_t505_il2cpp_TypeInfo_var->static_fields)->___s_randomGenerator_0 = L_1;
		((Utility_t505_StaticFields*)Utility_t505_il2cpp_TypeInfo_var->static_fields)->___s_useRandomSourceID_1 = 0;
		((Utility_t505_StaticFields*)Utility_t505_il2cpp_TypeInfo_var->static_fields)->___s_randomSourceComponent_2 = 0;
		((Utility_t505_StaticFields*)Utility_t505_il2cpp_TypeInfo_var->static_fields)->___s_programAppID_3 = (((int64_t)(-1)));
		Dictionary_2_t507 * L_2 = (Dictionary_2_t507 *)il2cpp_codegen_object_new (Dictionary_2_t507_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3307(L_2, /*hidden argument*/Dictionary_2__ctor_m3307_MethodInfo_var);
		((Utility_t505_StaticFields*)Utility_t505_il2cpp_TypeInfo_var->static_fields)->___s_dictTokens_4 = L_2;
		return;
	}
}
// UnityEngine.Networking.Types.SourceID UnityEngine.Networking.Utility::GetSourceID()
extern TypeInfo* Utility_t505_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t56_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" uint64_t Utility_GetSourceID_m2924 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Utility_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(354);
		Int32_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = SystemInfo_get_deviceUniqueIdentifier_m1997(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Utility_t505_il2cpp_TypeInfo_var);
		int32_t L_1 = ((Utility_t505_StaticFields*)Utility_t505_il2cpp_TypeInfo_var->static_fields)->___s_randomSourceComponent_2;
		int32_t L_2 = L_1;
		Object_t * L_3 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m214(NULL /*static, unused*/, L_0, L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		int32_t L_5 = String_GetHashCode_m3247(L_4, /*hidden argument*/NULL);
		return (uint64_t)((((int64_t)L_5)));
	}
}
// System.Void UnityEngine.Networking.Utility::SetAppID(UnityEngine.Networking.Types.AppID)
extern TypeInfo* Utility_t505_il2cpp_TypeInfo_var;
extern "C" void Utility_SetAppID_m2925 (Object_t * __this /* static, unused */, uint64_t ___newAppID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Utility_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(354);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint64_t L_0 = ___newAppID;
		IL2CPP_RUNTIME_CLASS_INIT(Utility_t505_il2cpp_TypeInfo_var);
		((Utility_t505_StaticFields*)Utility_t505_il2cpp_TypeInfo_var->static_fields)->___s_programAppID_3 = L_0;
		return;
	}
}
// UnityEngine.Networking.Types.AppID UnityEngine.Networking.Utility::GetAppID()
extern TypeInfo* Utility_t505_il2cpp_TypeInfo_var;
extern "C" uint64_t Utility_GetAppID_m2926 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Utility_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(354);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Utility_t505_il2cpp_TypeInfo_var);
		uint64_t L_0 = ((Utility_t505_StaticFields*)Utility_t505_il2cpp_TypeInfo_var->static_fields)->___s_programAppID_3;
		return L_0;
	}
}
// UnityEngine.Networking.Types.NetworkAccessToken UnityEngine.Networking.Utility::GetAccessTokenForNetwork(UnityEngine.Networking.Types.NetworkID)
extern TypeInfo* Utility_t505_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkAccessToken_t504_il2cpp_TypeInfo_var;
extern "C" NetworkAccessToken_t504 * Utility_GetAccessTokenForNetwork_m2927 (Object_t * __this /* static, unused */, uint64_t ___netId, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Utility_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(354);
		NetworkAccessToken_t504_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(352);
		s_Il2CppMethodIntialized = true;
	}
	NetworkAccessToken_t504 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(Utility_t505_il2cpp_TypeInfo_var);
		Dictionary_2_t507 * L_0 = ((Utility_t505_StaticFields*)Utility_t505_il2cpp_TypeInfo_var->static_fields)->___s_dictTokens_4;
		uint64_t L_1 = ___netId;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker2< bool, uint64_t, NetworkAccessToken_t504 ** >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::TryGetValue(!0,!1&) */, L_0, L_1, (&V_0));
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		NetworkAccessToken_t504 * L_3 = (NetworkAccessToken_t504 *)il2cpp_codegen_object_new (NetworkAccessToken_t504_il2cpp_TypeInfo_var);
		NetworkAccessToken__ctor_m2921(L_3, /*hidden argument*/NULL);
		V_0 = L_3;
	}

IL_0018:
	{
		NetworkAccessToken_t504 * L_4 = V_0;
		return L_4;
	}
}
// System.Void UnityEngine.Networking.Match.NetworkMatch::.ctor()
extern TypeInfo* Uri_t509_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral307;
extern Il2CppCodeGenString* _stringLiteral308;
extern Il2CppCodeGenString* _stringLiteral309;
extern "C" void NetworkMatch__ctor_m2928 (NetworkMatch_t508 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t509_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(356);
		_stringLiteral307 = il2cpp_codegen_string_literal_from_index(307);
		_stringLiteral308 = il2cpp_codegen_string_literal_from_index(308);
		_stringLiteral309 = il2cpp_codegen_string_literal_from_index(309);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	uint64_t V_1 = 0;
	{
		Uri_t509 * L_0 = (Uri_t509 *)il2cpp_codegen_object_new (Uri_t509_il2cpp_TypeInfo_var);
		Uri__ctor_m3308(L_0, _stringLiteral307, /*hidden argument*/NULL);
		__this->___m_BaseUri_3 = L_0;
		MonoBehaviour__ctor_m191(__this, /*hidden argument*/NULL);
		String_t* L_1 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral308, /*hidden argument*/NULL);
		V_0 = L_1;
		String_t* L_2 = V_0;
		bool L_3 = UInt64_TryParse_m3309(NULL /*static, unused*/, L_2, (&V_1), /*hidden argument*/NULL);
		if (L_3)
		{
			goto IL_003d;
		}
	}
	{
		Debug_Log_m202(NULL /*static, unused*/, _stringLiteral309, /*hidden argument*/NULL);
		goto IL_0044;
	}

IL_003d:
	{
		uint64_t L_4 = V_1;
		NetworkMatch_SetProgramAppID_m2931(__this, L_4, /*hidden argument*/NULL);
	}

IL_0044:
	{
		return;
	}
}
// System.Uri UnityEngine.Networking.Match.NetworkMatch::get_baseUri()
extern "C" Uri_t509 * NetworkMatch_get_baseUri_m2929 (NetworkMatch_t508 * __this, const MethodInfo* method)
{
	{
		Uri_t509 * L_0 = (__this->___m_BaseUri_3);
		return L_0;
	}
}
// System.Void UnityEngine.Networking.Match.NetworkMatch::set_baseUri(System.Uri)
extern "C" void NetworkMatch_set_baseUri_m2930 (NetworkMatch_t508 * __this, Uri_t509 * ___value, const MethodInfo* method)
{
	{
		Uri_t509 * L_0 = ___value;
		__this->___m_BaseUri_3 = L_0;
		return;
	}
}
// System.Void UnityEngine.Networking.Match.NetworkMatch::SetProgramAppID(UnityEngine.Networking.Types.AppID)
extern TypeInfo* Utility_t505_il2cpp_TypeInfo_var;
extern "C" void NetworkMatch_SetProgramAppID_m2931 (NetworkMatch_t508 * __this, uint64_t ___programAppID, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Utility_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(354);
		s_Il2CppMethodIntialized = true;
	}
	{
		uint64_t L_0 = ___programAppID;
		IL2CPP_RUNTIME_CLASS_INIT(Utility_t505_il2cpp_TypeInfo_var);
		Utility_SetAppID_m2925(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::CreateMatch(System.String,System.UInt32,System.Boolean,System.String,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.CreateMatchResponse>)
extern TypeInfo* CreateMatchRequest_t487_il2cpp_TypeInfo_var;
extern "C" Coroutine_t188 * NetworkMatch_CreateMatch_m2932 (NetworkMatch_t508 * __this, String_t* ___matchName, uint32_t ___matchSize, bool ___matchAdvertise, String_t* ___matchPassword, ResponseDelegate_1_t614 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CreateMatchRequest_t487_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(357);
		s_Il2CppMethodIntialized = true;
	}
	CreateMatchRequest_t487 * V_0 = {0};
	{
		CreateMatchRequest_t487 * L_0 = (CreateMatchRequest_t487 *)il2cpp_codegen_object_new (CreateMatchRequest_t487_il2cpp_TypeInfo_var);
		CreateMatchRequest__ctor_m2821(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		CreateMatchRequest_t487 * L_1 = V_0;
		String_t* L_2 = ___matchName;
		NullCheck(L_1);
		CreateMatchRequest_set_name_m2823(L_1, L_2, /*hidden argument*/NULL);
		CreateMatchRequest_t487 * L_3 = V_0;
		uint32_t L_4 = ___matchSize;
		NullCheck(L_3);
		CreateMatchRequest_set_size_m2825(L_3, L_4, /*hidden argument*/NULL);
		CreateMatchRequest_t487 * L_5 = V_0;
		bool L_6 = ___matchAdvertise;
		NullCheck(L_5);
		CreateMatchRequest_set_advertise_m2827(L_5, L_6, /*hidden argument*/NULL);
		CreateMatchRequest_t487 * L_7 = V_0;
		String_t* L_8 = ___matchPassword;
		NullCheck(L_7);
		CreateMatchRequest_set_password_m2829(L_7, L_8, /*hidden argument*/NULL);
		CreateMatchRequest_t487 * L_9 = V_0;
		ResponseDelegate_1_t614 * L_10 = ___callback;
		Coroutine_t188 * L_11 = NetworkMatch_CreateMatch_m2933(__this, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::CreateMatch(UnityEngine.Networking.Match.CreateMatchRequest,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.CreateMatchResponse>)
extern TypeInfo* Uri_t509_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* Utility_t505_il2cpp_TypeInfo_var;
extern TypeInfo* SourceID_t501_il2cpp_TypeInfo_var;
extern TypeInfo* AppID_t500_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern const MethodInfo* NetworkMatch_ProcessMatchResponse_TisCreateMatchResponse_t489_m3313_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral310;
extern Il2CppCodeGenString* _stringLiteral311;
extern Il2CppCodeGenString* _stringLiteral312;
extern Il2CppCodeGenString* _stringLiteral313;
extern Il2CppCodeGenString* _stringLiteral314;
extern Il2CppCodeGenString* _stringLiteral288;
extern Il2CppCodeGenString* _stringLiteral315;
extern Il2CppCodeGenString* _stringLiteral300;
extern Il2CppCodeGenString* _stringLiteral316;
extern Il2CppCodeGenString* _stringLiteral317;
extern Il2CppCodeGenString* _stringLiteral318;
extern Il2CppCodeGenString* _stringLiteral319;
extern Il2CppCodeGenString* _stringLiteral320;
extern "C" Coroutine_t188 * NetworkMatch_CreateMatch_m2933 (NetworkMatch_t508 * __this, CreateMatchRequest_t487 * ___req, ResponseDelegate_1_t614 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t509_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(356);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		Utility_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(354);
		SourceID_t501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(343);
		AppID_t500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(344);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		NetworkMatch_ProcessMatchResponse_TisCreateMatchResponse_t489_m3313_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483925);
		_stringLiteral310 = il2cpp_codegen_string_literal_from_index(310);
		_stringLiteral311 = il2cpp_codegen_string_literal_from_index(311);
		_stringLiteral312 = il2cpp_codegen_string_literal_from_index(312);
		_stringLiteral313 = il2cpp_codegen_string_literal_from_index(313);
		_stringLiteral314 = il2cpp_codegen_string_literal_from_index(314);
		_stringLiteral288 = il2cpp_codegen_string_literal_from_index(288);
		_stringLiteral315 = il2cpp_codegen_string_literal_from_index(315);
		_stringLiteral300 = il2cpp_codegen_string_literal_from_index(300);
		_stringLiteral316 = il2cpp_codegen_string_literal_from_index(316);
		_stringLiteral317 = il2cpp_codegen_string_literal_from_index(317);
		_stringLiteral318 = il2cpp_codegen_string_literal_from_index(318);
		_stringLiteral319 = il2cpp_codegen_string_literal_from_index(319);
		_stringLiteral320 = il2cpp_codegen_string_literal_from_index(320);
		s_Il2CppMethodIntialized = true;
	}
	Uri_t509 * V_0 = {0};
	WWWForm_t8 * V_1 = {0};
	WWW_t9 * V_2 = {0};
	uint32_t V_3 = 0;
	bool V_4 = false;
	{
		Uri_t509 * L_0 = NetworkMatch_get_baseUri_m2929(__this, /*hidden argument*/NULL);
		Uri_t509 * L_1 = (Uri_t509 *)il2cpp_codegen_object_new (Uri_t509_il2cpp_TypeInfo_var);
		Uri__ctor_m3310(L_1, L_0, _stringLiteral310, /*hidden argument*/NULL);
		V_0 = L_1;
		Uri_t509 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m214(NULL /*static, unused*/, _stringLiteral311, L_2, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		WWWForm_t8 * L_4 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		WWWForm_t8 * L_5 = V_1;
		String_t* L_6 = Application_get_cloudProjectId_m2521(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		WWWForm_AddField_m203(L_5, _stringLiteral312, L_6, /*hidden argument*/NULL);
		WWWForm_t8 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Utility_t505_il2cpp_TypeInfo_var);
		uint64_t L_8 = Utility_GetSourceID_m2924(NULL /*static, unused*/, /*hidden argument*/NULL);
		uint64_t L_9 = L_8;
		Object_t * L_10 = Box(SourceID_t501_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_10);
		String_t* L_11 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_10);
		NullCheck(L_7);
		WWWForm_AddField_m203(L_7, _stringLiteral313, L_11, /*hidden argument*/NULL);
		WWWForm_t8 * L_12 = V_1;
		uint64_t L_13 = Utility_GetAppID_m2926(NULL /*static, unused*/, /*hidden argument*/NULL);
		uint64_t L_14 = L_13;
		Object_t * L_15 = Box(AppID_t500_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_15);
		String_t* L_16 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_15);
		NullCheck(L_12);
		WWWForm_AddField_m203(L_12, _stringLiteral314, L_16, /*hidden argument*/NULL);
		WWWForm_t8 * L_17 = V_1;
		NullCheck(L_17);
		WWWForm_AddField_m269(L_17, _stringLiteral288, 0, /*hidden argument*/NULL);
		WWWForm_t8 * L_18 = V_1;
		NullCheck(L_18);
		WWWForm_AddField_m269(L_18, _stringLiteral315, 0, /*hidden argument*/NULL);
		WWWForm_t8 * L_19 = V_1;
		CreateMatchRequest_t487 * L_20 = ___req;
		NullCheck(L_20);
		String_t* L_21 = CreateMatchRequest_get_name_m2822(L_20, /*hidden argument*/NULL);
		NullCheck(L_19);
		WWWForm_AddField_m203(L_19, _stringLiteral300, L_21, /*hidden argument*/NULL);
		WWWForm_t8 * L_22 = V_1;
		CreateMatchRequest_t487 * L_23 = ___req;
		NullCheck(L_23);
		uint32_t L_24 = CreateMatchRequest_get_size_m2824(L_23, /*hidden argument*/NULL);
		V_3 = L_24;
		String_t* L_25 = UInt32_ToString_m3311((&V_3), /*hidden argument*/NULL);
		NullCheck(L_22);
		WWWForm_AddField_m203(L_22, _stringLiteral316, L_25, /*hidden argument*/NULL);
		WWWForm_t8 * L_26 = V_1;
		CreateMatchRequest_t487 * L_27 = ___req;
		NullCheck(L_27);
		bool L_28 = CreateMatchRequest_get_advertise_m2826(L_27, /*hidden argument*/NULL);
		V_4 = L_28;
		String_t* L_29 = Boolean_ToString_m3312((&V_4), /*hidden argument*/NULL);
		NullCheck(L_26);
		WWWForm_AddField_m203(L_26, _stringLiteral317, L_29, /*hidden argument*/NULL);
		WWWForm_t8 * L_30 = V_1;
		CreateMatchRequest_t487 * L_31 = ___req;
		NullCheck(L_31);
		String_t* L_32 = CreateMatchRequest_get_password_m2828(L_31, /*hidden argument*/NULL);
		NullCheck(L_30);
		WWWForm_AddField_m203(L_30, _stringLiteral318, L_32, /*hidden argument*/NULL);
		WWWForm_t8 * L_33 = V_1;
		NullCheck(L_33);
		Dictionary_2_t607 * L_34 = WWWForm_get_headers_m2502(L_33, /*hidden argument*/NULL);
		NullCheck(L_34);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_34, _stringLiteral319, _stringLiteral320);
		Uri_t509 * L_35 = V_0;
		NullCheck(L_35);
		String_t* L_36 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Uri::ToString() */, L_35);
		WWWForm_t8 * L_37 = V_1;
		WWW_t9 * L_38 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_38, L_36, L_37, /*hidden argument*/NULL);
		V_2 = L_38;
		WWW_t9 * L_39 = V_2;
		ResponseDelegate_1_t614 * L_40 = ___callback;
		Object_t * L_41 = NetworkMatch_ProcessMatchResponse_TisCreateMatchResponse_t489_m3313(__this, L_39, L_40, /*hidden argument*/NetworkMatch_ProcessMatchResponse_TisCreateMatchResponse_t489_m3313_MethodInfo_var);
		Coroutine_t188 * L_42 = MonoBehaviour_StartCoroutine_m207(__this, L_41, /*hidden argument*/NULL);
		return L_42;
	}
}
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::JoinMatch(UnityEngine.Networking.Types.NetworkID,System.String,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.JoinMatchResponse>)
extern TypeInfo* JoinMatchRequest_t490_il2cpp_TypeInfo_var;
extern "C" Coroutine_t188 * NetworkMatch_JoinMatch_m2934 (NetworkMatch_t508 * __this, uint64_t ___netId, String_t* ___matchPassword, ResponseDelegate_1_t615 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		JoinMatchRequest_t490_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(359);
		s_Il2CppMethodIntialized = true;
	}
	JoinMatchRequest_t490 * V_0 = {0};
	{
		JoinMatchRequest_t490 * L_0 = (JoinMatchRequest_t490 *)il2cpp_codegen_object_new (JoinMatchRequest_t490_il2cpp_TypeInfo_var);
		JoinMatchRequest__ctor_m2847(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		JoinMatchRequest_t490 * L_1 = V_0;
		uint64_t L_2 = ___netId;
		NullCheck(L_1);
		JoinMatchRequest_set_networkId_m2849(L_1, L_2, /*hidden argument*/NULL);
		JoinMatchRequest_t490 * L_3 = V_0;
		String_t* L_4 = ___matchPassword;
		NullCheck(L_3);
		JoinMatchRequest_set_password_m2851(L_3, L_4, /*hidden argument*/NULL);
		JoinMatchRequest_t490 * L_5 = V_0;
		ResponseDelegate_1_t615 * L_6 = ___callback;
		Coroutine_t188 * L_7 = NetworkMatch_JoinMatch_m2935(__this, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::JoinMatch(UnityEngine.Networking.Match.JoinMatchRequest,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.JoinMatchResponse>)
extern TypeInfo* Uri_t509_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* Utility_t505_il2cpp_TypeInfo_var;
extern TypeInfo* SourceID_t501_il2cpp_TypeInfo_var;
extern TypeInfo* AppID_t500_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkID_t502_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern const MethodInfo* NetworkMatch_ProcessMatchResponse_TisJoinMatchResponse_t491_m3314_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral321;
extern Il2CppCodeGenString* _stringLiteral322;
extern Il2CppCodeGenString* _stringLiteral312;
extern Il2CppCodeGenString* _stringLiteral313;
extern Il2CppCodeGenString* _stringLiteral314;
extern Il2CppCodeGenString* _stringLiteral288;
extern Il2CppCodeGenString* _stringLiteral315;
extern Il2CppCodeGenString* _stringLiteral287;
extern Il2CppCodeGenString* _stringLiteral318;
extern Il2CppCodeGenString* _stringLiteral319;
extern Il2CppCodeGenString* _stringLiteral320;
extern "C" Coroutine_t188 * NetworkMatch_JoinMatch_m2935 (NetworkMatch_t508 * __this, JoinMatchRequest_t490 * ___req, ResponseDelegate_1_t615 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t509_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(356);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		Utility_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(354);
		SourceID_t501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(343);
		AppID_t500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(344);
		NetworkID_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(348);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		NetworkMatch_ProcessMatchResponse_TisJoinMatchResponse_t491_m3314_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483926);
		_stringLiteral321 = il2cpp_codegen_string_literal_from_index(321);
		_stringLiteral322 = il2cpp_codegen_string_literal_from_index(322);
		_stringLiteral312 = il2cpp_codegen_string_literal_from_index(312);
		_stringLiteral313 = il2cpp_codegen_string_literal_from_index(313);
		_stringLiteral314 = il2cpp_codegen_string_literal_from_index(314);
		_stringLiteral288 = il2cpp_codegen_string_literal_from_index(288);
		_stringLiteral315 = il2cpp_codegen_string_literal_from_index(315);
		_stringLiteral287 = il2cpp_codegen_string_literal_from_index(287);
		_stringLiteral318 = il2cpp_codegen_string_literal_from_index(318);
		_stringLiteral319 = il2cpp_codegen_string_literal_from_index(319);
		_stringLiteral320 = il2cpp_codegen_string_literal_from_index(320);
		s_Il2CppMethodIntialized = true;
	}
	Uri_t509 * V_0 = {0};
	WWWForm_t8 * V_1 = {0};
	WWW_t9 * V_2 = {0};
	{
		Uri_t509 * L_0 = NetworkMatch_get_baseUri_m2929(__this, /*hidden argument*/NULL);
		Uri_t509 * L_1 = (Uri_t509 *)il2cpp_codegen_object_new (Uri_t509_il2cpp_TypeInfo_var);
		Uri__ctor_m3310(L_1, L_0, _stringLiteral321, /*hidden argument*/NULL);
		V_0 = L_1;
		Uri_t509 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m214(NULL /*static, unused*/, _stringLiteral322, L_2, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		WWWForm_t8 * L_4 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		WWWForm_t8 * L_5 = V_1;
		String_t* L_6 = Application_get_cloudProjectId_m2521(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		WWWForm_AddField_m203(L_5, _stringLiteral312, L_6, /*hidden argument*/NULL);
		WWWForm_t8 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Utility_t505_il2cpp_TypeInfo_var);
		uint64_t L_8 = Utility_GetSourceID_m2924(NULL /*static, unused*/, /*hidden argument*/NULL);
		uint64_t L_9 = L_8;
		Object_t * L_10 = Box(SourceID_t501_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_10);
		String_t* L_11 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_10);
		NullCheck(L_7);
		WWWForm_AddField_m203(L_7, _stringLiteral313, L_11, /*hidden argument*/NULL);
		WWWForm_t8 * L_12 = V_1;
		uint64_t L_13 = Utility_GetAppID_m2926(NULL /*static, unused*/, /*hidden argument*/NULL);
		uint64_t L_14 = L_13;
		Object_t * L_15 = Box(AppID_t500_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_15);
		String_t* L_16 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_15);
		NullCheck(L_12);
		WWWForm_AddField_m203(L_12, _stringLiteral314, L_16, /*hidden argument*/NULL);
		WWWForm_t8 * L_17 = V_1;
		NullCheck(L_17);
		WWWForm_AddField_m269(L_17, _stringLiteral288, 0, /*hidden argument*/NULL);
		WWWForm_t8 * L_18 = V_1;
		NullCheck(L_18);
		WWWForm_AddField_m269(L_18, _stringLiteral315, 0, /*hidden argument*/NULL);
		WWWForm_t8 * L_19 = V_1;
		JoinMatchRequest_t490 * L_20 = ___req;
		NullCheck(L_20);
		uint64_t L_21 = JoinMatchRequest_get_networkId_m2848(L_20, /*hidden argument*/NULL);
		uint64_t L_22 = L_21;
		Object_t * L_23 = Box(NetworkID_t502_il2cpp_TypeInfo_var, &L_22);
		NullCheck(L_23);
		String_t* L_24 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_23);
		NullCheck(L_19);
		WWWForm_AddField_m203(L_19, _stringLiteral287, L_24, /*hidden argument*/NULL);
		WWWForm_t8 * L_25 = V_1;
		JoinMatchRequest_t490 * L_26 = ___req;
		NullCheck(L_26);
		String_t* L_27 = JoinMatchRequest_get_password_m2850(L_26, /*hidden argument*/NULL);
		NullCheck(L_25);
		WWWForm_AddField_m203(L_25, _stringLiteral318, L_27, /*hidden argument*/NULL);
		WWWForm_t8 * L_28 = V_1;
		NullCheck(L_28);
		Dictionary_2_t607 * L_29 = WWWForm_get_headers_m2502(L_28, /*hidden argument*/NULL);
		NullCheck(L_29);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_29, _stringLiteral319, _stringLiteral320);
		Uri_t509 * L_30 = V_0;
		NullCheck(L_30);
		String_t* L_31 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Uri::ToString() */, L_30);
		WWWForm_t8 * L_32 = V_1;
		WWW_t9 * L_33 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_33, L_31, L_32, /*hidden argument*/NULL);
		V_2 = L_33;
		WWW_t9 * L_34 = V_2;
		ResponseDelegate_1_t615 * L_35 = ___callback;
		Object_t * L_36 = NetworkMatch_ProcessMatchResponse_TisJoinMatchResponse_t491_m3314(__this, L_34, L_35, /*hidden argument*/NetworkMatch_ProcessMatchResponse_TisJoinMatchResponse_t491_m3314_MethodInfo_var);
		Coroutine_t188 * L_37 = MonoBehaviour_StartCoroutine_m207(__this, L_36, /*hidden argument*/NULL);
		return L_37;
	}
}
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::DestroyMatch(UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.BasicResponse>)
extern TypeInfo* DestroyMatchRequest_t492_il2cpp_TypeInfo_var;
extern "C" Coroutine_t188 * NetworkMatch_DestroyMatch_m2936 (NetworkMatch_t508 * __this, uint64_t ___netId, ResponseDelegate_1_t616 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DestroyMatchRequest_t492_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(361);
		s_Il2CppMethodIntialized = true;
	}
	DestroyMatchRequest_t492 * V_0 = {0};
	{
		DestroyMatchRequest_t492 * L_0 = (DestroyMatchRequest_t492 *)il2cpp_codegen_object_new (DestroyMatchRequest_t492_il2cpp_TypeInfo_var);
		DestroyMatchRequest__ctor_m2868(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		DestroyMatchRequest_t492 * L_1 = V_0;
		uint64_t L_2 = ___netId;
		NullCheck(L_1);
		DestroyMatchRequest_set_networkId_m2870(L_1, L_2, /*hidden argument*/NULL);
		DestroyMatchRequest_t492 * L_3 = V_0;
		ResponseDelegate_1_t616 * L_4 = ___callback;
		Coroutine_t188 * L_5 = NetworkMatch_DestroyMatch_m2937(__this, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::DestroyMatch(UnityEngine.Networking.Match.DestroyMatchRequest,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.BasicResponse>)
extern TypeInfo* Uri_t509_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* Utility_t505_il2cpp_TypeInfo_var;
extern TypeInfo* SourceID_t501_il2cpp_TypeInfo_var;
extern TypeInfo* AppID_t500_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkID_t502_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern const MethodInfo* NetworkMatch_ProcessMatchResponse_TisBasicResponse_t486_m3315_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral323;
extern Il2CppCodeGenString* _stringLiteral324;
extern Il2CppCodeGenString* _stringLiteral312;
extern Il2CppCodeGenString* _stringLiteral313;
extern Il2CppCodeGenString* _stringLiteral314;
extern Il2CppCodeGenString* _stringLiteral288;
extern Il2CppCodeGenString* _stringLiteral315;
extern Il2CppCodeGenString* _stringLiteral287;
extern Il2CppCodeGenString* _stringLiteral319;
extern Il2CppCodeGenString* _stringLiteral320;
extern "C" Coroutine_t188 * NetworkMatch_DestroyMatch_m2937 (NetworkMatch_t508 * __this, DestroyMatchRequest_t492 * ___req, ResponseDelegate_1_t616 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t509_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(356);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		Utility_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(354);
		SourceID_t501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(343);
		AppID_t500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(344);
		NetworkID_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(348);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		NetworkMatch_ProcessMatchResponse_TisBasicResponse_t486_m3315_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483927);
		_stringLiteral323 = il2cpp_codegen_string_literal_from_index(323);
		_stringLiteral324 = il2cpp_codegen_string_literal_from_index(324);
		_stringLiteral312 = il2cpp_codegen_string_literal_from_index(312);
		_stringLiteral313 = il2cpp_codegen_string_literal_from_index(313);
		_stringLiteral314 = il2cpp_codegen_string_literal_from_index(314);
		_stringLiteral288 = il2cpp_codegen_string_literal_from_index(288);
		_stringLiteral315 = il2cpp_codegen_string_literal_from_index(315);
		_stringLiteral287 = il2cpp_codegen_string_literal_from_index(287);
		_stringLiteral319 = il2cpp_codegen_string_literal_from_index(319);
		_stringLiteral320 = il2cpp_codegen_string_literal_from_index(320);
		s_Il2CppMethodIntialized = true;
	}
	Uri_t509 * V_0 = {0};
	WWWForm_t8 * V_1 = {0};
	WWW_t9 * V_2 = {0};
	{
		Uri_t509 * L_0 = NetworkMatch_get_baseUri_m2929(__this, /*hidden argument*/NULL);
		Uri_t509 * L_1 = (Uri_t509 *)il2cpp_codegen_object_new (Uri_t509_il2cpp_TypeInfo_var);
		Uri__ctor_m3310(L_1, L_0, _stringLiteral323, /*hidden argument*/NULL);
		V_0 = L_1;
		Uri_t509 * L_2 = V_0;
		NullCheck(L_2);
		String_t* L_3 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Uri::ToString() */, L_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_4 = String_Concat_m217(NULL /*static, unused*/, _stringLiteral324, L_3, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		WWWForm_t8 * L_5 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_5, /*hidden argument*/NULL);
		V_1 = L_5;
		WWWForm_t8 * L_6 = V_1;
		String_t* L_7 = Application_get_cloudProjectId_m2521(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_6);
		WWWForm_AddField_m203(L_6, _stringLiteral312, L_7, /*hidden argument*/NULL);
		WWWForm_t8 * L_8 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Utility_t505_il2cpp_TypeInfo_var);
		uint64_t L_9 = Utility_GetSourceID_m2924(NULL /*static, unused*/, /*hidden argument*/NULL);
		uint64_t L_10 = L_9;
		Object_t * L_11 = Box(SourceID_t501_il2cpp_TypeInfo_var, &L_10);
		NullCheck(L_11);
		String_t* L_12 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_11);
		NullCheck(L_8);
		WWWForm_AddField_m203(L_8, _stringLiteral313, L_12, /*hidden argument*/NULL);
		WWWForm_t8 * L_13 = V_1;
		uint64_t L_14 = Utility_GetAppID_m2926(NULL /*static, unused*/, /*hidden argument*/NULL);
		uint64_t L_15 = L_14;
		Object_t * L_16 = Box(AppID_t500_il2cpp_TypeInfo_var, &L_15);
		NullCheck(L_16);
		String_t* L_17 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_16);
		NullCheck(L_13);
		WWWForm_AddField_m203(L_13, _stringLiteral314, L_17, /*hidden argument*/NULL);
		WWWForm_t8 * L_18 = V_1;
		DestroyMatchRequest_t492 * L_19 = ___req;
		NullCheck(L_19);
		uint64_t L_20 = DestroyMatchRequest_get_networkId_m2869(L_19, /*hidden argument*/NULL);
		NetworkAccessToken_t504 * L_21 = Utility_GetAccessTokenForNetwork_m2927(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
		NullCheck(L_21);
		String_t* L_22 = NetworkAccessToken_GetByteString_m2922(L_21, /*hidden argument*/NULL);
		NullCheck(L_18);
		WWWForm_AddField_m203(L_18, _stringLiteral288, L_22, /*hidden argument*/NULL);
		WWWForm_t8 * L_23 = V_1;
		NullCheck(L_23);
		WWWForm_AddField_m269(L_23, _stringLiteral315, 0, /*hidden argument*/NULL);
		WWWForm_t8 * L_24 = V_1;
		DestroyMatchRequest_t492 * L_25 = ___req;
		NullCheck(L_25);
		uint64_t L_26 = DestroyMatchRequest_get_networkId_m2869(L_25, /*hidden argument*/NULL);
		uint64_t L_27 = L_26;
		Object_t * L_28 = Box(NetworkID_t502_il2cpp_TypeInfo_var, &L_27);
		NullCheck(L_28);
		String_t* L_29 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_28);
		NullCheck(L_24);
		WWWForm_AddField_m203(L_24, _stringLiteral287, L_29, /*hidden argument*/NULL);
		WWWForm_t8 * L_30 = V_1;
		NullCheck(L_30);
		Dictionary_2_t607 * L_31 = WWWForm_get_headers_m2502(L_30, /*hidden argument*/NULL);
		NullCheck(L_31);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_31, _stringLiteral319, _stringLiteral320);
		Uri_t509 * L_32 = V_0;
		NullCheck(L_32);
		String_t* L_33 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Uri::ToString() */, L_32);
		WWWForm_t8 * L_34 = V_1;
		WWW_t9 * L_35 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_35, L_33, L_34, /*hidden argument*/NULL);
		V_2 = L_35;
		WWW_t9 * L_36 = V_2;
		ResponseDelegate_1_t616 * L_37 = ___callback;
		Object_t * L_38 = NetworkMatch_ProcessMatchResponse_TisBasicResponse_t486_m3315(__this, L_36, L_37, /*hidden argument*/NetworkMatch_ProcessMatchResponse_TisBasicResponse_t486_m3315_MethodInfo_var);
		Coroutine_t188 * L_39 = MonoBehaviour_StartCoroutine_m207(__this, L_38, /*hidden argument*/NULL);
		return L_39;
	}
}
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::DropConnection(UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NodeID,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.BasicResponse>)
extern TypeInfo* DropConnectionRequest_t493_il2cpp_TypeInfo_var;
extern "C" Coroutine_t188 * NetworkMatch_DropConnection_m2938 (NetworkMatch_t508 * __this, uint64_t ___netId, uint16_t ___dropNodeId, ResponseDelegate_1_t616 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DropConnectionRequest_t493_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(363);
		s_Il2CppMethodIntialized = true;
	}
	DropConnectionRequest_t493 * V_0 = {0};
	{
		DropConnectionRequest_t493 * L_0 = (DropConnectionRequest_t493 *)il2cpp_codegen_object_new (DropConnectionRequest_t493_il2cpp_TypeInfo_var);
		DropConnectionRequest__ctor_m2872(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		DropConnectionRequest_t493 * L_1 = V_0;
		uint64_t L_2 = ___netId;
		NullCheck(L_1);
		DropConnectionRequest_set_networkId_m2874(L_1, L_2, /*hidden argument*/NULL);
		DropConnectionRequest_t493 * L_3 = V_0;
		uint16_t L_4 = ___dropNodeId;
		NullCheck(L_3);
		DropConnectionRequest_set_nodeId_m2876(L_3, L_4, /*hidden argument*/NULL);
		DropConnectionRequest_t493 * L_5 = V_0;
		ResponseDelegate_1_t616 * L_6 = ___callback;
		Coroutine_t188 * L_7 = NetworkMatch_DropConnection_m2939(__this, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::DropConnection(UnityEngine.Networking.Match.DropConnectionRequest,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.BasicResponse>)
extern TypeInfo* Uri_t509_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* Utility_t505_il2cpp_TypeInfo_var;
extern TypeInfo* SourceID_t501_il2cpp_TypeInfo_var;
extern TypeInfo* AppID_t500_il2cpp_TypeInfo_var;
extern TypeInfo* NetworkID_t502_il2cpp_TypeInfo_var;
extern TypeInfo* NodeID_t503_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern const MethodInfo* NetworkMatch_ProcessMatchResponse_TisBasicResponse_t486_m3315_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral325;
extern Il2CppCodeGenString* _stringLiteral326;
extern Il2CppCodeGenString* _stringLiteral312;
extern Il2CppCodeGenString* _stringLiteral313;
extern Il2CppCodeGenString* _stringLiteral314;
extern Il2CppCodeGenString* _stringLiteral288;
extern Il2CppCodeGenString* _stringLiteral315;
extern Il2CppCodeGenString* _stringLiteral287;
extern Il2CppCodeGenString* _stringLiteral289;
extern Il2CppCodeGenString* _stringLiteral319;
extern Il2CppCodeGenString* _stringLiteral320;
extern "C" Coroutine_t188 * NetworkMatch_DropConnection_m2939 (NetworkMatch_t508 * __this, DropConnectionRequest_t493 * ___req, ResponseDelegate_1_t616 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t509_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(356);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		Utility_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(354);
		SourceID_t501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(343);
		AppID_t500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(344);
		NetworkID_t502_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(348);
		NodeID_t503_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(349);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		NetworkMatch_ProcessMatchResponse_TisBasicResponse_t486_m3315_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483927);
		_stringLiteral325 = il2cpp_codegen_string_literal_from_index(325);
		_stringLiteral326 = il2cpp_codegen_string_literal_from_index(326);
		_stringLiteral312 = il2cpp_codegen_string_literal_from_index(312);
		_stringLiteral313 = il2cpp_codegen_string_literal_from_index(313);
		_stringLiteral314 = il2cpp_codegen_string_literal_from_index(314);
		_stringLiteral288 = il2cpp_codegen_string_literal_from_index(288);
		_stringLiteral315 = il2cpp_codegen_string_literal_from_index(315);
		_stringLiteral287 = il2cpp_codegen_string_literal_from_index(287);
		_stringLiteral289 = il2cpp_codegen_string_literal_from_index(289);
		_stringLiteral319 = il2cpp_codegen_string_literal_from_index(319);
		_stringLiteral320 = il2cpp_codegen_string_literal_from_index(320);
		s_Il2CppMethodIntialized = true;
	}
	Uri_t509 * V_0 = {0};
	WWWForm_t8 * V_1 = {0};
	WWW_t9 * V_2 = {0};
	{
		Uri_t509 * L_0 = NetworkMatch_get_baseUri_m2929(__this, /*hidden argument*/NULL);
		Uri_t509 * L_1 = (Uri_t509 *)il2cpp_codegen_object_new (Uri_t509_il2cpp_TypeInfo_var);
		Uri__ctor_m3310(L_1, L_0, _stringLiteral325, /*hidden argument*/NULL);
		V_0 = L_1;
		Uri_t509 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m214(NULL /*static, unused*/, _stringLiteral326, L_2, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		WWWForm_t8 * L_4 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		WWWForm_t8 * L_5 = V_1;
		String_t* L_6 = Application_get_cloudProjectId_m2521(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		WWWForm_AddField_m203(L_5, _stringLiteral312, L_6, /*hidden argument*/NULL);
		WWWForm_t8 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Utility_t505_il2cpp_TypeInfo_var);
		uint64_t L_8 = Utility_GetSourceID_m2924(NULL /*static, unused*/, /*hidden argument*/NULL);
		uint64_t L_9 = L_8;
		Object_t * L_10 = Box(SourceID_t501_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_10);
		String_t* L_11 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_10);
		NullCheck(L_7);
		WWWForm_AddField_m203(L_7, _stringLiteral313, L_11, /*hidden argument*/NULL);
		WWWForm_t8 * L_12 = V_1;
		uint64_t L_13 = Utility_GetAppID_m2926(NULL /*static, unused*/, /*hidden argument*/NULL);
		uint64_t L_14 = L_13;
		Object_t * L_15 = Box(AppID_t500_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_15);
		String_t* L_16 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_15);
		NullCheck(L_12);
		WWWForm_AddField_m203(L_12, _stringLiteral314, L_16, /*hidden argument*/NULL);
		WWWForm_t8 * L_17 = V_1;
		DropConnectionRequest_t493 * L_18 = ___req;
		NullCheck(L_18);
		uint64_t L_19 = DropConnectionRequest_get_networkId_m2873(L_18, /*hidden argument*/NULL);
		NetworkAccessToken_t504 * L_20 = Utility_GetAccessTokenForNetwork_m2927(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		String_t* L_21 = NetworkAccessToken_GetByteString_m2922(L_20, /*hidden argument*/NULL);
		NullCheck(L_17);
		WWWForm_AddField_m203(L_17, _stringLiteral288, L_21, /*hidden argument*/NULL);
		WWWForm_t8 * L_22 = V_1;
		NullCheck(L_22);
		WWWForm_AddField_m269(L_22, _stringLiteral315, 0, /*hidden argument*/NULL);
		WWWForm_t8 * L_23 = V_1;
		DropConnectionRequest_t493 * L_24 = ___req;
		NullCheck(L_24);
		uint64_t L_25 = DropConnectionRequest_get_networkId_m2873(L_24, /*hidden argument*/NULL);
		uint64_t L_26 = L_25;
		Object_t * L_27 = Box(NetworkID_t502_il2cpp_TypeInfo_var, &L_26);
		NullCheck(L_27);
		String_t* L_28 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_27);
		NullCheck(L_23);
		WWWForm_AddField_m203(L_23, _stringLiteral287, L_28, /*hidden argument*/NULL);
		WWWForm_t8 * L_29 = V_1;
		DropConnectionRequest_t493 * L_30 = ___req;
		NullCheck(L_30);
		uint16_t L_31 = DropConnectionRequest_get_nodeId_m2875(L_30, /*hidden argument*/NULL);
		uint16_t L_32 = L_31;
		Object_t * L_33 = Box(NodeID_t503_il2cpp_TypeInfo_var, &L_32);
		NullCheck(L_33);
		String_t* L_34 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_33);
		NullCheck(L_29);
		WWWForm_AddField_m203(L_29, _stringLiteral289, L_34, /*hidden argument*/NULL);
		WWWForm_t8 * L_35 = V_1;
		NullCheck(L_35);
		Dictionary_2_t607 * L_36 = WWWForm_get_headers_m2502(L_35, /*hidden argument*/NULL);
		NullCheck(L_36);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_36, _stringLiteral319, _stringLiteral320);
		Uri_t509 * L_37 = V_0;
		NullCheck(L_37);
		String_t* L_38 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Uri::ToString() */, L_37);
		WWWForm_t8 * L_39 = V_1;
		WWW_t9 * L_40 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_40, L_38, L_39, /*hidden argument*/NULL);
		V_2 = L_40;
		WWW_t9 * L_41 = V_2;
		ResponseDelegate_1_t616 * L_42 = ___callback;
		Object_t * L_43 = NetworkMatch_ProcessMatchResponse_TisBasicResponse_t486_m3315(__this, L_41, L_42, /*hidden argument*/NetworkMatch_ProcessMatchResponse_TisBasicResponse_t486_m3315_MethodInfo_var);
		Coroutine_t188 * L_44 = MonoBehaviour_StartCoroutine_m207(__this, L_43, /*hidden argument*/NULL);
		return L_44;
	}
}
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::ListMatches(System.Int32,System.Int32,System.String,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.ListMatchResponse>)
extern TypeInfo* ListMatchRequest_t494_il2cpp_TypeInfo_var;
extern "C" Coroutine_t188 * NetworkMatch_ListMatches_m2940 (NetworkMatch_t508 * __this, int32_t ___startPageNumber, int32_t ___resultPageSize, String_t* ___matchNameFilter, ResponseDelegate_1_t617 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ListMatchRequest_t494_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(364);
		s_Il2CppMethodIntialized = true;
	}
	ListMatchRequest_t494 * V_0 = {0};
	{
		ListMatchRequest_t494 * L_0 = (ListMatchRequest_t494 *)il2cpp_codegen_object_new (ListMatchRequest_t494_il2cpp_TypeInfo_var);
		ListMatchRequest__ctor_m2878(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		ListMatchRequest_t494 * L_1 = V_0;
		int32_t L_2 = ___startPageNumber;
		NullCheck(L_1);
		ListMatchRequest_set_pageNum_m2882(L_1, L_2, /*hidden argument*/NULL);
		ListMatchRequest_t494 * L_3 = V_0;
		int32_t L_4 = ___resultPageSize;
		NullCheck(L_3);
		ListMatchRequest_set_pageSize_m2880(L_3, L_4, /*hidden argument*/NULL);
		ListMatchRequest_t494 * L_5 = V_0;
		String_t* L_6 = ___matchNameFilter;
		NullCheck(L_5);
		ListMatchRequest_set_nameFilter_m2884(L_5, L_6, /*hidden argument*/NULL);
		ListMatchRequest_t494 * L_7 = V_0;
		ResponseDelegate_1_t617 * L_8 = ___callback;
		Coroutine_t188 * L_9 = NetworkMatch_ListMatches_m2941(__this, L_7, L_8, /*hidden argument*/NULL);
		return L_9;
	}
}
// UnityEngine.Coroutine UnityEngine.Networking.Match.NetworkMatch::ListMatches(UnityEngine.Networking.Match.ListMatchRequest,UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.ListMatchResponse>)
extern TypeInfo* Uri_t509_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* Utility_t505_il2cpp_TypeInfo_var;
extern TypeInfo* SourceID_t501_il2cpp_TypeInfo_var;
extern TypeInfo* AppID_t500_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern const MethodInfo* NetworkMatch_ProcessMatchResponse_TisListMatchResponse_t498_m3316_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral327;
extern Il2CppCodeGenString* _stringLiteral328;
extern Il2CppCodeGenString* _stringLiteral312;
extern Il2CppCodeGenString* _stringLiteral313;
extern Il2CppCodeGenString* _stringLiteral314;
extern Il2CppCodeGenString* _stringLiteral329;
extern Il2CppCodeGenString* _stringLiteral288;
extern Il2CppCodeGenString* _stringLiteral315;
extern Il2CppCodeGenString* _stringLiteral330;
extern Il2CppCodeGenString* _stringLiteral331;
extern Il2CppCodeGenString* _stringLiteral332;
extern Il2CppCodeGenString* _stringLiteral319;
extern Il2CppCodeGenString* _stringLiteral320;
extern "C" Coroutine_t188 * NetworkMatch_ListMatches_m2941 (NetworkMatch_t508 * __this, ListMatchRequest_t494 * ___req, ResponseDelegate_1_t617 * ___callback, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Uri_t509_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(356);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		Utility_t505_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(354);
		SourceID_t501_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(343);
		AppID_t500_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(344);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		NetworkMatch_ProcessMatchResponse_TisListMatchResponse_t498_m3316_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483928);
		_stringLiteral327 = il2cpp_codegen_string_literal_from_index(327);
		_stringLiteral328 = il2cpp_codegen_string_literal_from_index(328);
		_stringLiteral312 = il2cpp_codegen_string_literal_from_index(312);
		_stringLiteral313 = il2cpp_codegen_string_literal_from_index(313);
		_stringLiteral314 = il2cpp_codegen_string_literal_from_index(314);
		_stringLiteral329 = il2cpp_codegen_string_literal_from_index(329);
		_stringLiteral288 = il2cpp_codegen_string_literal_from_index(288);
		_stringLiteral315 = il2cpp_codegen_string_literal_from_index(315);
		_stringLiteral330 = il2cpp_codegen_string_literal_from_index(330);
		_stringLiteral331 = il2cpp_codegen_string_literal_from_index(331);
		_stringLiteral332 = il2cpp_codegen_string_literal_from_index(332);
		_stringLiteral319 = il2cpp_codegen_string_literal_from_index(319);
		_stringLiteral320 = il2cpp_codegen_string_literal_from_index(320);
		s_Il2CppMethodIntialized = true;
	}
	Uri_t509 * V_0 = {0};
	WWWForm_t8 * V_1 = {0};
	WWW_t9 * V_2 = {0};
	bool V_3 = false;
	{
		Uri_t509 * L_0 = NetworkMatch_get_baseUri_m2929(__this, /*hidden argument*/NULL);
		Uri_t509 * L_1 = (Uri_t509 *)il2cpp_codegen_object_new (Uri_t509_il2cpp_TypeInfo_var);
		Uri__ctor_m3310(L_1, L_0, _stringLiteral327, /*hidden argument*/NULL);
		V_0 = L_1;
		Uri_t509 * L_2 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m214(NULL /*static, unused*/, _stringLiteral328, L_2, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		WWWForm_t8 * L_4 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_4, /*hidden argument*/NULL);
		V_1 = L_4;
		WWWForm_t8 * L_5 = V_1;
		String_t* L_6 = Application_get_cloudProjectId_m2521(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_5);
		WWWForm_AddField_m203(L_5, _stringLiteral312, L_6, /*hidden argument*/NULL);
		WWWForm_t8 * L_7 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Utility_t505_il2cpp_TypeInfo_var);
		uint64_t L_8 = Utility_GetSourceID_m2924(NULL /*static, unused*/, /*hidden argument*/NULL);
		uint64_t L_9 = L_8;
		Object_t * L_10 = Box(SourceID_t501_il2cpp_TypeInfo_var, &L_9);
		NullCheck(L_10);
		String_t* L_11 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_10);
		NullCheck(L_7);
		WWWForm_AddField_m203(L_7, _stringLiteral313, L_11, /*hidden argument*/NULL);
		WWWForm_t8 * L_12 = V_1;
		uint64_t L_13 = Utility_GetAppID_m2926(NULL /*static, unused*/, /*hidden argument*/NULL);
		uint64_t L_14 = L_13;
		Object_t * L_15 = Box(AppID_t500_il2cpp_TypeInfo_var, &L_14);
		NullCheck(L_15);
		String_t* L_16 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Enum::ToString() */, L_15);
		NullCheck(L_12);
		WWWForm_AddField_m203(L_12, _stringLiteral314, L_16, /*hidden argument*/NULL);
		WWWForm_t8 * L_17 = V_1;
		ListMatchRequest_t494 * L_18 = ___req;
		NullCheck(L_18);
		bool L_19 = ListMatchRequest_get_includePasswordMatches_m2885(L_18, /*hidden argument*/NULL);
		V_3 = L_19;
		String_t* L_20 = Boolean_ToString_m3312((&V_3), /*hidden argument*/NULL);
		NullCheck(L_17);
		WWWForm_AddField_m203(L_17, _stringLiteral329, L_20, /*hidden argument*/NULL);
		WWWForm_t8 * L_21 = V_1;
		NullCheck(L_21);
		WWWForm_AddField_m269(L_21, _stringLiteral288, 0, /*hidden argument*/NULL);
		WWWForm_t8 * L_22 = V_1;
		NullCheck(L_22);
		WWWForm_AddField_m269(L_22, _stringLiteral315, 0, /*hidden argument*/NULL);
		WWWForm_t8 * L_23 = V_1;
		ListMatchRequest_t494 * L_24 = ___req;
		NullCheck(L_24);
		int32_t L_25 = ListMatchRequest_get_pageSize_m2879(L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		WWWForm_AddField_m269(L_23, _stringLiteral330, L_25, /*hidden argument*/NULL);
		WWWForm_t8 * L_26 = V_1;
		ListMatchRequest_t494 * L_27 = ___req;
		NullCheck(L_27);
		int32_t L_28 = ListMatchRequest_get_pageNum_m2881(L_27, /*hidden argument*/NULL);
		NullCheck(L_26);
		WWWForm_AddField_m269(L_26, _stringLiteral331, L_28, /*hidden argument*/NULL);
		WWWForm_t8 * L_29 = V_1;
		ListMatchRequest_t494 * L_30 = ___req;
		NullCheck(L_30);
		String_t* L_31 = ListMatchRequest_get_nameFilter_m2883(L_30, /*hidden argument*/NULL);
		NullCheck(L_29);
		WWWForm_AddField_m203(L_29, _stringLiteral332, L_31, /*hidden argument*/NULL);
		WWWForm_t8 * L_32 = V_1;
		NullCheck(L_32);
		Dictionary_2_t607 * L_33 = WWWForm_get_headers_m2502(L_32, /*hidden argument*/NULL);
		NullCheck(L_33);
		VirtActionInvoker2< String_t*, String_t* >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.String>::set_Item(!0,!1) */, L_33, _stringLiteral319, _stringLiteral320);
		Uri_t509 * L_34 = V_0;
		NullCheck(L_34);
		String_t* L_35 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Uri::ToString() */, L_34);
		WWWForm_t8 * L_36 = V_1;
		WWW_t9 * L_37 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_37, L_35, L_36, /*hidden argument*/NULL);
		V_2 = L_37;
		WWW_t9 * L_38 = V_2;
		ResponseDelegate_1_t617 * L_39 = ___callback;
		Object_t * L_40 = NetworkMatch_ProcessMatchResponse_TisListMatchResponse_t498_m3316(__this, L_38, L_39, /*hidden argument*/NetworkMatch_ProcessMatchResponse_TisListMatchResponse_t498_m3316_MethodInfo_var);
		Coroutine_t188 * L_41 = MonoBehaviour_StartCoroutine_m207(__this, L_40, /*hidden argument*/NULL);
		return L_41;
	}
}
// System.Void SimpleJson.JsonArray::.ctor()
extern TypeInfo* List_1_t344_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3317_MethodInfo_var;
extern "C" void JsonArray__ctor_m2942 (JsonArray_t510 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		List_1_t344_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(366);
		List_1__ctor_m3317_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483929);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(List_1_t344_il2cpp_TypeInfo_var);
		List_1__ctor_m3317(__this, /*hidden argument*/List_1__ctor_m3317_MethodInfo_var);
		return;
	}
}
// System.String SimpleJson.JsonArray::ToString()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" String_t* JsonArray_ToString_m2943 (JsonArray_t510 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	String_t* G_B2_0 = {0};
	String_t* G_B1_0 = {0};
	{
		String_t* L_0 = SimpleJson_SerializeObject_m2963(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		String_t* L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0012;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		G_B2_0 = L_2;
	}

IL_0012:
	{
		return G_B2_0;
	}
}
// System.Void SimpleJson.JsonObject::.ctor()
extern TypeInfo* Dictionary_2_t512_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3318_MethodInfo_var;
extern "C" void JsonObject__ctor_m2944 (JsonObject_t511 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t512_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(367);
		Dictionary_2__ctor_m3318_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483930);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		Dictionary_2_t512 * L_0 = (Dictionary_2_t512 *)il2cpp_codegen_object_new (Dictionary_2_t512_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3318(L_0, /*hidden argument*/Dictionary_2__ctor_m3318_MethodInfo_var);
		__this->____members_0 = L_0;
		return;
	}
}
// System.Collections.IEnumerator SimpleJson.JsonObject::System.Collections.IEnumerable.GetEnumerator()
extern TypeInfo* Enumerator_t655_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m3319_MethodInfo_var;
extern "C" Object_t * JsonObject_System_Collections_IEnumerable_GetEnumerator_m2945 (JsonObject_t511 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t655_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(368);
		Dictionary_2_GetEnumerator_m3319_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483931);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t512 * L_0 = (__this->____members_0);
		NullCheck(L_0);
		Enumerator_t655  L_1 = Dictionary_2_GetEnumerator_m3319(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m3319_MethodInfo_var);
		Enumerator_t655  L_2 = L_1;
		Object_t * L_3 = Box(Enumerator_t655_il2cpp_TypeInfo_var, &L_2);
		return (Object_t *)L_3;
	}
}
// System.Void SimpleJson.JsonObject::Add(System.String,System.Object)
extern "C" void JsonObject_Add_m2946 (JsonObject_t511 * __this, String_t* ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		Dictionary_2_t512 * L_0 = (__this->____members_0);
		String_t* L_1 = ___key;
		Object_t * L_2 = ___value;
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::Add(!0,!1) */, L_0, L_1, L_2);
		return;
	}
}
// System.Collections.Generic.ICollection`1<System.String> SimpleJson.JsonObject::get_Keys()
extern const MethodInfo* Dictionary_2_get_Keys_m3320_MethodInfo_var;
extern "C" Object_t* JsonObject_get_Keys_m2947 (JsonObject_t511 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_get_Keys_m3320_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483932);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t512 * L_0 = (__this->____members_0);
		NullCheck(L_0);
		KeyCollection_t656 * L_1 = Dictionary_2_get_Keys_m3320(L_0, /*hidden argument*/Dictionary_2_get_Keys_m3320_MethodInfo_var);
		return L_1;
	}
}
// System.Boolean SimpleJson.JsonObject::TryGetValue(System.String,System.Object&)
extern "C" bool JsonObject_TryGetValue_m2948 (JsonObject_t511 * __this, String_t* ___key, Object_t ** ___value, const MethodInfo* method)
{
	{
		Dictionary_2_t512 * L_0 = (__this->____members_0);
		String_t* L_1 = ___key;
		Object_t ** L_2 = ___value;
		NullCheck(L_0);
		bool L_3 = (bool)VirtFuncInvoker2< bool, String_t*, Object_t ** >::Invoke(19 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::TryGetValue(!0,!1&) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Collections.Generic.ICollection`1<System.Object> SimpleJson.JsonObject::get_Values()
extern const MethodInfo* Dictionary_2_get_Values_m3321_MethodInfo_var;
extern "C" Object_t* JsonObject_get_Values_m2949 (JsonObject_t511 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_get_Values_m3321_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483933);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t512 * L_0 = (__this->____members_0);
		NullCheck(L_0);
		ValueCollection_t657 * L_1 = Dictionary_2_get_Values_m3321(L_0, /*hidden argument*/Dictionary_2_get_Values_m3321_MethodInfo_var);
		return L_1;
	}
}
// System.Object SimpleJson.JsonObject::get_Item(System.String)
extern "C" Object_t * JsonObject_get_Item_m2950 (JsonObject_t511 * __this, String_t* ___key, const MethodInfo* method)
{
	{
		Dictionary_2_t512 * L_0 = (__this->____members_0);
		String_t* L_1 = ___key;
		NullCheck(L_0);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, String_t* >::Invoke(20 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Void SimpleJson.JsonObject::set_Item(System.String,System.Object)
extern "C" void JsonObject_set_Item_m2951 (JsonObject_t511 * __this, String_t* ___key, Object_t * ___value, const MethodInfo* method)
{
	{
		Dictionary_2_t512 * L_0 = (__this->____members_0);
		String_t* L_1 = ___key;
		Object_t * L_2 = ___value;
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(21 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::set_Item(!0,!1) */, L_0, L_1, L_2);
		return;
	}
}
// System.Void SimpleJson.JsonObject::Add(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern const MethodInfo* KeyValuePair_2_get_Key_m3322_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m3323_MethodInfo_var;
extern "C" void JsonObject_Add_m2952 (JsonObject_t511 * __this, KeyValuePair_2_t618  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		KeyValuePair_2_get_Key_m3322_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483934);
		KeyValuePair_2_get_Value_m3323_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483935);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t512 * L_0 = (__this->____members_0);
		String_t* L_1 = KeyValuePair_2_get_Key_m3322((&___item), /*hidden argument*/KeyValuePair_2_get_Key_m3322_MethodInfo_var);
		Object_t * L_2 = KeyValuePair_2_get_Value_m3323((&___item), /*hidden argument*/KeyValuePair_2_get_Value_m3323_MethodInfo_var);
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, Object_t * >::Invoke(18 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::Add(!0,!1) */, L_0, L_1, L_2);
		return;
	}
}
// System.Void SimpleJson.JsonObject::Clear()
extern "C" void JsonObject_Clear_m2953 (JsonObject_t511 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t512 * L_0 = (__this->____members_0);
		NullCheck(L_0);
		VirtActionInvoker0::Invoke(13 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Object>::Clear() */, L_0);
		return;
	}
}
// System.Boolean SimpleJson.JsonObject::Contains(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern const MethodInfo* KeyValuePair_2_get_Key_m3322_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m3323_MethodInfo_var;
extern "C" bool JsonObject_Contains_m2954 (JsonObject_t511 * __this, KeyValuePair_2_t618  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		KeyValuePair_2_get_Key_m3322_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483934);
		KeyValuePair_2_get_Value_m3323_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483935);
		s_Il2CppMethodIntialized = true;
	}
	int32_t G_B3_0 = 0;
	{
		Dictionary_2_t512 * L_0 = (__this->____members_0);
		String_t* L_1 = KeyValuePair_2_get_Key_m3322((&___item), /*hidden argument*/KeyValuePair_2_get_Key_m3322_MethodInfo_var);
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0034;
		}
	}
	{
		Dictionary_2_t512 * L_3 = (__this->____members_0);
		String_t* L_4 = KeyValuePair_2_get_Key_m3322((&___item), /*hidden argument*/KeyValuePair_2_get_Key_m3322_MethodInfo_var);
		NullCheck(L_3);
		Object_t * L_5 = (Object_t *)VirtFuncInvoker1< Object_t *, String_t* >::Invoke(20 /* !1 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Item(!0) */, L_3, L_4);
		Object_t * L_6 = KeyValuePair_2_get_Value_m3323((&___item), /*hidden argument*/KeyValuePair_2_get_Value_m3323_MethodInfo_var);
		G_B3_0 = ((((Object_t*)(Object_t *)L_5) == ((Object_t*)(Object_t *)L_6))? 1 : 0);
		goto IL_0035;
	}

IL_0034:
	{
		G_B3_0 = 0;
	}

IL_0035:
	{
		return G_B3_0;
	}
}
// System.Void SimpleJson.JsonObject::CopyTo(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>[],System.Int32)
extern TypeInfo* ArgumentNullException_t658_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t622_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t51_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t303_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral333;
extern "C" void JsonObject_CopyTo_m2955 (JsonObject_t511 * __this, KeyValuePair_2U5BU5D_t619* ___array, int32_t ___arrayIndex, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(369);
		IEnumerator_1_t622_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(370);
		IEnumerator_t51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		IDisposable_t303_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		_stringLiteral333 = il2cpp_codegen_string_literal_from_index(333);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	KeyValuePair_2_t618  V_1 = {0};
	Object_t* V_2 = {0};
	Exception_t301 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t301 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		KeyValuePair_2U5BU5D_t619* L_0 = ___array;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t658 * L_1 = (ArgumentNullException_t658 *)il2cpp_codegen_object_new (ArgumentNullException_t658_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3324(L_1, _stringLiteral333, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		int32_t L_2 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(11 /* System.Int32 SimpleJson.JsonObject::get_Count() */, __this);
		V_0 = L_2;
		Object_t* L_3 = (Object_t*)VirtFuncInvoker0< Object_t* >::Invoke(18 /* System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>> SimpleJson.JsonObject::GetEnumerator() */, __this);
		V_2 = L_3;
	}

IL_001f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_004d;
		}

IL_0024:
		{
			Object_t* L_4 = V_2;
			NullCheck(L_4);
			KeyValuePair_2_t618  L_5 = (KeyValuePair_2_t618 )InterfaceFuncInvoker0< KeyValuePair_2_t618  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>>::get_Current() */, IEnumerator_1_t622_il2cpp_TypeInfo_var, L_4);
			V_1 = L_5;
			KeyValuePair_2U5BU5D_t619* L_6 = ___array;
			int32_t L_7 = ___arrayIndex;
			int32_t L_8 = L_7;
			___arrayIndex = ((int32_t)((int32_t)L_8+(int32_t)1));
			NullCheck(L_6);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_8);
			KeyValuePair_2_t618  L_9 = V_1;
			*((KeyValuePair_2_t618 *)(KeyValuePair_2_t618 *)SZArrayLdElema(L_6, L_8, sizeof(KeyValuePair_2_t618 ))) = L_9;
			int32_t L_10 = V_0;
			int32_t L_11 = ((int32_t)((int32_t)L_10-(int32_t)1));
			V_0 = L_11;
			if ((((int32_t)L_11) > ((int32_t)0)))
			{
				goto IL_004d;
			}
		}

IL_0048:
		{
			IL2CPP_LEAVE(0x68, FINALLY_005d);
		}

IL_004d:
		{
			Object_t* L_12 = V_2;
			NullCheck(L_12);
			bool L_13 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t51_il2cpp_TypeInfo_var, L_12);
			if (L_13)
			{
				goto IL_0024;
			}
		}

IL_0058:
		{
			IL2CPP_LEAVE(0x68, FINALLY_005d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t301 *)e.ex;
		goto FINALLY_005d;
	}

FINALLY_005d:
	{ // begin finally (depth: 1)
		{
			Object_t* L_14 = V_2;
			if (L_14)
			{
				goto IL_0061;
			}
		}

IL_0060:
		{
			IL2CPP_END_FINALLY(93)
		}

IL_0061:
		{
			Object_t* L_15 = V_2;
			NullCheck(L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t303_il2cpp_TypeInfo_var, L_15);
			IL2CPP_END_FINALLY(93)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(93)
	{
		IL2CPP_JUMP_TBL(0x68, IL_0068)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t301 *)
	}

IL_0068:
	{
		return;
	}
}
// System.Int32 SimpleJson.JsonObject::get_Count()
extern "C" int32_t JsonObject_get_Count_m2956 (JsonObject_t511 * __this, const MethodInfo* method)
{
	{
		Dictionary_2_t512 * L_0 = (__this->____members_0);
		NullCheck(L_0);
		int32_t L_1 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(10 /* System.Int32 System.Collections.Generic.Dictionary`2<System.String,System.Object>::get_Count() */, L_0);
		return L_1;
	}
}
// System.Boolean SimpleJson.JsonObject::get_IsReadOnly()
extern "C" bool JsonObject_get_IsReadOnly_m2957 (JsonObject_t511 * __this, const MethodInfo* method)
{
	{
		return 0;
	}
}
// System.Boolean SimpleJson.JsonObject::Remove(System.Collections.Generic.KeyValuePair`2<System.String,System.Object>)
extern const MethodInfo* KeyValuePair_2_get_Key_m3322_MethodInfo_var;
extern "C" bool JsonObject_Remove_m2958 (JsonObject_t511 * __this, KeyValuePair_2_t618  ___item, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		KeyValuePair_2_get_Key_m3322_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483934);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t512 * L_0 = (__this->____members_0);
		String_t* L_1 = KeyValuePair_2_get_Key_m3322((&___item), /*hidden argument*/KeyValuePair_2_get_Key_m3322_MethodInfo_var);
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(34 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Object>::Remove(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,System.Object>> SimpleJson.JsonObject::GetEnumerator()
extern TypeInfo* Enumerator_t655_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2_GetEnumerator_m3319_MethodInfo_var;
extern "C" Object_t* JsonObject_GetEnumerator_m2959 (JsonObject_t511 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Enumerator_t655_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(368);
		Dictionary_2_GetEnumerator_m3319_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483931);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t512 * L_0 = (__this->____members_0);
		NullCheck(L_0);
		Enumerator_t655  L_1 = Dictionary_2_GetEnumerator_m3319(L_0, /*hidden argument*/Dictionary_2_GetEnumerator_m3319_MethodInfo_var);
		Enumerator_t655  L_2 = L_1;
		Object_t * L_3 = Box(Enumerator_t655_il2cpp_TypeInfo_var, &L_2);
		return (Object_t*)L_3;
	}
}
// System.String SimpleJson.JsonObject::ToString()
extern "C" String_t* JsonObject_ToString_m2960 (JsonObject_t511 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = SimpleJson_SerializeObject_m2963(NULL /*static, unused*/, __this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Boolean SimpleJson.SimpleJson::TryDeserializeObject(System.String,System.Object&)
extern "C" bool SimpleJson_TryDeserializeObject_m2961 (Object_t * __this /* static, unused */, String_t* ___json, Object_t ** ___obj, const MethodInfo* method)
{
	bool V_0 = false;
	CharU5BU5D_t58* V_1 = {0};
	int32_t V_2 = 0;
	{
		V_0 = 1;
		String_t* L_0 = ___json;
		if (!L_0)
		{
			goto IL_0022;
		}
	}
	{
		String_t* L_1 = ___json;
		NullCheck(L_1);
		CharU5BU5D_t58* L_2 = String_ToCharArray_m3325(L_1, /*hidden argument*/NULL);
		V_1 = L_2;
		V_2 = 0;
		Object_t ** L_3 = ___obj;
		CharU5BU5D_t58* L_4 = V_1;
		Object_t * L_5 = SimpleJson_ParseValue_m2966(NULL /*static, unused*/, L_4, (&V_2), (&V_0), /*hidden argument*/NULL);
		*((Object_t **)(L_3)) = (Object_t *)L_5;
		goto IL_0025;
	}

IL_0022:
	{
		Object_t ** L_6 = ___obj;
		*((Object_t **)(L_6)) = (Object_t *)NULL;
	}

IL_0025:
	{
		bool L_7 = V_0;
		return L_7;
	}
}
// System.String SimpleJson.SimpleJson::SerializeObject(System.Object,SimpleJson.IJsonSerializerStrategy)
extern TypeInfo* StringBuilder_t296_il2cpp_TypeInfo_var;
extern "C" String_t* SimpleJson_SerializeObject_m2962 (Object_t * __this /* static, unused */, Object_t * ___json, Object_t * ___jsonSerializerStrategy, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t296_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t296 * V_0 = {0};
	bool V_1 = false;
	String_t* G_B3_0 = {0};
	{
		StringBuilder_t296 * L_0 = (StringBuilder_t296 *)il2cpp_codegen_object_new (StringBuilder_t296_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3326(L_0, ((int32_t)2000), /*hidden argument*/NULL);
		V_0 = L_0;
		Object_t * L_1 = ___jsonSerializerStrategy;
		Object_t * L_2 = ___json;
		StringBuilder_t296 * L_3 = V_0;
		bool L_4 = SimpleJson_SerializeValue_m2974(NULL /*static, unused*/, L_1, L_2, L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		bool L_5 = V_1;
		if (!L_5)
		{
			goto IL_0025;
		}
	}
	{
		StringBuilder_t296 * L_6 = V_0;
		NullCheck(L_6);
		String_t* L_7 = StringBuilder_ToString_m1480(L_6, /*hidden argument*/NULL);
		G_B3_0 = L_7;
		goto IL_0026;
	}

IL_0025:
	{
		G_B3_0 = ((String_t*)(NULL));
	}

IL_0026:
	{
		return G_B3_0;
	}
}
// System.String SimpleJson.SimpleJson::SerializeObject(System.Object)
extern "C" String_t* SimpleJson_SerializeObject_m2963 (Object_t * __this /* static, unused */, Object_t * ___json, const MethodInfo* method)
{
	{
		Object_t * L_0 = ___json;
		Object_t * L_1 = SimpleJson_get_CurrentJsonSerializerStrategy_m2980(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_2 = SimpleJson_SerializeObject_m2962(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Collections.Generic.IDictionary`2<System.String,System.Object> SimpleJson.SimpleJson::ParseObject(System.Char[],System.Int32&,System.Boolean&)
extern TypeInfo* JsonObject_t511_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_2_t613_il2cpp_TypeInfo_var;
extern "C" Object_t* SimpleJson_ParseObject_m2964 (Object_t * __this /* static, unused */, CharU5BU5D_t58* ___json, int32_t* ___index, bool* ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		JsonObject_t511_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		IDictionary_2_t613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(345);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	int32_t V_1 = 0;
	bool V_2 = false;
	String_t* V_3 = {0};
	Object_t * V_4 = {0};
	{
		JsonObject_t511 * L_0 = (JsonObject_t511 *)il2cpp_codegen_object_new (JsonObject_t511_il2cpp_TypeInfo_var);
		JsonObject__ctor_m2944(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		CharU5BU5D_t58* L_1 = ___json;
		int32_t* L_2 = ___index;
		SimpleJson_NextToken_m2973(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_2 = 0;
		goto IL_0096;
	}

IL_0015:
	{
		CharU5BU5D_t58* L_3 = ___json;
		int32_t* L_4 = ___index;
		int32_t L_5 = SimpleJson_LookAhead_m2972(NULL /*static, unused*/, L_3, (*((int32_t*)L_4)), /*hidden argument*/NULL);
		V_1 = L_5;
		int32_t L_6 = V_1;
		if (L_6)
		{
			goto IL_0029;
		}
	}
	{
		bool* L_7 = ___success;
		*((int8_t*)(L_7)) = (int8_t)0;
		return (Object_t*)NULL;
	}

IL_0029:
	{
		int32_t L_8 = V_1;
		if ((!(((uint32_t)L_8) == ((uint32_t)6))))
		{
			goto IL_003d;
		}
	}
	{
		CharU5BU5D_t58* L_9 = ___json;
		int32_t* L_10 = ___index;
		SimpleJson_NextToken_m2973(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		goto IL_0096;
	}

IL_003d:
	{
		int32_t L_11 = V_1;
		if ((!(((uint32_t)L_11) == ((uint32_t)2))))
		{
			goto IL_004e;
		}
	}
	{
		CharU5BU5D_t58* L_12 = ___json;
		int32_t* L_13 = ___index;
		SimpleJson_NextToken_m2973(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		Object_t* L_14 = V_0;
		return L_14;
	}

IL_004e:
	{
		CharU5BU5D_t58* L_15 = ___json;
		int32_t* L_16 = ___index;
		bool* L_17 = ___success;
		String_t* L_18 = SimpleJson_ParseString_m2967(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		V_3 = L_18;
		bool* L_19 = ___success;
		if ((*((int8_t*)L_19)))
		{
			goto IL_0063;
		}
	}
	{
		bool* L_20 = ___success;
		*((int8_t*)(L_20)) = (int8_t)0;
		return (Object_t*)NULL;
	}

IL_0063:
	{
		CharU5BU5D_t58* L_21 = ___json;
		int32_t* L_22 = ___index;
		int32_t L_23 = SimpleJson_NextToken_m2973(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		int32_t L_24 = V_1;
		if ((((int32_t)L_24) == ((int32_t)5)))
		{
			goto IL_0077;
		}
	}
	{
		bool* L_25 = ___success;
		*((int8_t*)(L_25)) = (int8_t)0;
		return (Object_t*)NULL;
	}

IL_0077:
	{
		CharU5BU5D_t58* L_26 = ___json;
		int32_t* L_27 = ___index;
		bool* L_28 = ___success;
		Object_t * L_29 = SimpleJson_ParseValue_m2966(NULL /*static, unused*/, L_26, L_27, L_28, /*hidden argument*/NULL);
		V_4 = L_29;
		bool* L_30 = ___success;
		if ((*((int8_t*)L_30)))
		{
			goto IL_008d;
		}
	}
	{
		bool* L_31 = ___success;
		*((int8_t*)(L_31)) = (int8_t)0;
		return (Object_t*)NULL;
	}

IL_008d:
	{
		Object_t* L_32 = V_0;
		String_t* L_33 = V_3;
		Object_t * L_34 = V_4;
		NullCheck(L_32);
		InterfaceActionInvoker2< String_t*, Object_t * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.Object>::set_Item(!0,!1) */, IDictionary_2_t613_il2cpp_TypeInfo_var, L_32, L_33, L_34);
	}

IL_0096:
	{
		bool L_35 = V_2;
		if (!L_35)
		{
			goto IL_0015;
		}
	}
	{
		Object_t* L_36 = V_0;
		return L_36;
	}
}
// SimpleJson.JsonArray SimpleJson.SimpleJson::ParseArray(System.Char[],System.Int32&,System.Boolean&)
extern TypeInfo* JsonArray_t510_il2cpp_TypeInfo_var;
extern "C" JsonArray_t510 * SimpleJson_ParseArray_m2965 (Object_t * __this /* static, unused */, CharU5BU5D_t58* ___json, int32_t* ___index, bool* ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		JsonArray_t510_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(374);
		s_Il2CppMethodIntialized = true;
	}
	JsonArray_t510 * V_0 = {0};
	bool V_1 = false;
	int32_t V_2 = 0;
	Object_t * V_3 = {0};
	{
		JsonArray_t510 * L_0 = (JsonArray_t510 *)il2cpp_codegen_object_new (JsonArray_t510_il2cpp_TypeInfo_var);
		JsonArray__ctor_m2942(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		CharU5BU5D_t58* L_1 = ___json;
		int32_t* L_2 = ___index;
		SimpleJson_NextToken_m2973(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		V_1 = 0;
		goto IL_006a;
	}

IL_0015:
	{
		CharU5BU5D_t58* L_3 = ___json;
		int32_t* L_4 = ___index;
		int32_t L_5 = SimpleJson_LookAhead_m2972(NULL /*static, unused*/, L_3, (*((int32_t*)L_4)), /*hidden argument*/NULL);
		V_2 = L_5;
		int32_t L_6 = V_2;
		if (L_6)
		{
			goto IL_0029;
		}
	}
	{
		bool* L_7 = ___success;
		*((int8_t*)(L_7)) = (int8_t)0;
		return (JsonArray_t510 *)NULL;
	}

IL_0029:
	{
		int32_t L_8 = V_2;
		if ((!(((uint32_t)L_8) == ((uint32_t)6))))
		{
			goto IL_003d;
		}
	}
	{
		CharU5BU5D_t58* L_9 = ___json;
		int32_t* L_10 = ___index;
		SimpleJson_NextToken_m2973(NULL /*static, unused*/, L_9, L_10, /*hidden argument*/NULL);
		goto IL_006a;
	}

IL_003d:
	{
		int32_t L_11 = V_2;
		if ((!(((uint32_t)L_11) == ((uint32_t)4))))
		{
			goto IL_0051;
		}
	}
	{
		CharU5BU5D_t58* L_12 = ___json;
		int32_t* L_13 = ___index;
		SimpleJson_NextToken_m2973(NULL /*static, unused*/, L_12, L_13, /*hidden argument*/NULL);
		goto IL_0070;
	}

IL_0051:
	{
		CharU5BU5D_t58* L_14 = ___json;
		int32_t* L_15 = ___index;
		bool* L_16 = ___success;
		Object_t * L_17 = SimpleJson_ParseValue_m2966(NULL /*static, unused*/, L_14, L_15, L_16, /*hidden argument*/NULL);
		V_3 = L_17;
		bool* L_18 = ___success;
		if ((*((int8_t*)L_18)))
		{
			goto IL_0063;
		}
	}
	{
		return (JsonArray_t510 *)NULL;
	}

IL_0063:
	{
		JsonArray_t510 * L_19 = V_0;
		Object_t * L_20 = V_3;
		NullCheck(L_19);
		VirtActionInvoker1< Object_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Object>::Add(!0) */, L_19, L_20);
	}

IL_006a:
	{
		bool L_21 = V_1;
		if (!L_21)
		{
			goto IL_0015;
		}
	}

IL_0070:
	{
		JsonArray_t510 * L_22 = V_0;
		return L_22;
	}
}
// System.Object SimpleJson.SimpleJson::ParseValue(System.Char[],System.Int32&,System.Boolean&)
extern TypeInfo* Boolean_t298_il2cpp_TypeInfo_var;
extern "C" Object_t * SimpleJson_ParseValue_m2966 (Object_t * __this /* static, unused */, CharU5BU5D_t58* ___json, int32_t* ___index, bool* ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Boolean_t298_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(104);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		CharU5BU5D_t58* L_0 = ___json;
		int32_t* L_1 = ___index;
		int32_t L_2 = SimpleJson_LookAhead_m2972(NULL /*static, unused*/, L_0, (*((int32_t*)L_1)), /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = V_0;
		if (L_3 == 0)
		{
			goto IL_0090;
		}
		if (L_3 == 1)
		{
			goto IL_0056;
		}
		if (L_3 == 2)
		{
			goto IL_0095;
		}
		if (L_3 == 3)
		{
			goto IL_005f;
		}
		if (L_3 == 4)
		{
			goto IL_0095;
		}
		if (L_3 == 5)
		{
			goto IL_0095;
		}
		if (L_3 == 6)
		{
			goto IL_0095;
		}
		if (L_3 == 7)
		{
			goto IL_0044;
		}
		if (L_3 == 8)
		{
			goto IL_004d;
		}
		if (L_3 == 9)
		{
			goto IL_0068;
		}
		if (L_3 == 10)
		{
			goto IL_0077;
		}
		if (L_3 == 11)
		{
			goto IL_0086;
		}
	}
	{
		goto IL_0095;
	}

IL_0044:
	{
		CharU5BU5D_t58* L_4 = ___json;
		int32_t* L_5 = ___index;
		bool* L_6 = ___success;
		String_t* L_7 = SimpleJson_ParseString_m2967(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}

IL_004d:
	{
		CharU5BU5D_t58* L_8 = ___json;
		int32_t* L_9 = ___index;
		bool* L_10 = ___success;
		Object_t * L_11 = SimpleJson_ParseNumber_m2969(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
		return L_11;
	}

IL_0056:
	{
		CharU5BU5D_t58* L_12 = ___json;
		int32_t* L_13 = ___index;
		bool* L_14 = ___success;
		Object_t* L_15 = SimpleJson_ParseObject_m2964(NULL /*static, unused*/, L_12, L_13, L_14, /*hidden argument*/NULL);
		return L_15;
	}

IL_005f:
	{
		CharU5BU5D_t58* L_16 = ___json;
		int32_t* L_17 = ___index;
		bool* L_18 = ___success;
		JsonArray_t510 * L_19 = SimpleJson_ParseArray_m2965(NULL /*static, unused*/, L_16, L_17, L_18, /*hidden argument*/NULL);
		return L_19;
	}

IL_0068:
	{
		CharU5BU5D_t58* L_20 = ___json;
		int32_t* L_21 = ___index;
		SimpleJson_NextToken_m2973(NULL /*static, unused*/, L_20, L_21, /*hidden argument*/NULL);
		bool L_22 = 1;
		Object_t * L_23 = Box(Boolean_t298_il2cpp_TypeInfo_var, &L_22);
		return L_23;
	}

IL_0077:
	{
		CharU5BU5D_t58* L_24 = ___json;
		int32_t* L_25 = ___index;
		SimpleJson_NextToken_m2973(NULL /*static, unused*/, L_24, L_25, /*hidden argument*/NULL);
		bool L_26 = 0;
		Object_t * L_27 = Box(Boolean_t298_il2cpp_TypeInfo_var, &L_26);
		return L_27;
	}

IL_0086:
	{
		CharU5BU5D_t58* L_28 = ___json;
		int32_t* L_29 = ___index;
		SimpleJson_NextToken_m2973(NULL /*static, unused*/, L_28, L_29, /*hidden argument*/NULL);
		return NULL;
	}

IL_0090:
	{
		goto IL_0095;
	}

IL_0095:
	{
		bool* L_30 = ___success;
		*((int8_t*)(L_30)) = (int8_t)0;
		return NULL;
	}
}
// System.String SimpleJson.SimpleJson::ParseString(System.Char[],System.Int32&,System.Boolean&)
extern TypeInfo* StringBuilder_t296_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t659_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral334;
extern "C" String_t* SimpleJson_ParseString_m2967 (Object_t * __this /* static, unused */, CharU5BU5D_t58* ___json, int32_t* ___index, bool* ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		StringBuilder_t296_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(54);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		CultureInfo_t659_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(375);
		_stringLiteral334 = il2cpp_codegen_string_literal_from_index(334);
		s_Il2CppMethodIntialized = true;
	}
	StringBuilder_t296 * V_0 = {0};
	uint16_t V_1 = 0x0;
	bool V_2 = false;
	int32_t V_3 = 0;
	uint32_t V_4 = 0;
	uint32_t V_5 = 0;
	int32_t V_6 = 0;
	bool V_7 = false;
	{
		StringBuilder_t296 * L_0 = (StringBuilder_t296 *)il2cpp_codegen_object_new (StringBuilder_t296_il2cpp_TypeInfo_var);
		StringBuilder__ctor_m3326(L_0, ((int32_t)2000), /*hidden argument*/NULL);
		V_0 = L_0;
		CharU5BU5D_t58* L_1 = ___json;
		int32_t* L_2 = ___index;
		SimpleJson_EatWhitespace_m2971(NULL /*static, unused*/, L_1, L_2, /*hidden argument*/NULL);
		CharU5BU5D_t58* L_3 = ___json;
		int32_t* L_4 = ___index;
		int32_t* L_5 = ___index;
		int32_t L_6 = (*((int32_t*)L_5));
		V_6 = L_6;
		*((int32_t*)(L_4)) = (int32_t)((int32_t)((int32_t)L_6+(int32_t)1));
		int32_t L_7 = V_6;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_7);
		int32_t L_8 = L_7;
		V_1 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_3, L_8, sizeof(uint16_t)));
		V_2 = 0;
		goto IL_0239;
	}

IL_0027:
	{
		int32_t* L_9 = ___index;
		CharU5BU5D_t58* L_10 = ___json;
		NullCheck(L_10);
		if ((!(((uint32_t)(*((int32_t*)L_9))) == ((uint32_t)(((int32_t)(((Array_t *)L_10)->max_length)))))))
		{
			goto IL_0036;
		}
	}
	{
		goto IL_023f;
	}

IL_0036:
	{
		CharU5BU5D_t58* L_11 = ___json;
		int32_t* L_12 = ___index;
		int32_t* L_13 = ___index;
		int32_t L_14 = (*((int32_t*)L_13));
		V_6 = L_14;
		*((int32_t*)(L_12)) = (int32_t)((int32_t)((int32_t)L_14+(int32_t)1));
		int32_t L_15 = V_6;
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, L_15);
		int32_t L_16 = L_15;
		V_1 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_11, L_16, sizeof(uint16_t)));
		uint16_t L_17 = V_1;
		if ((!(((uint32_t)L_17) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0053;
		}
	}
	{
		V_2 = 1;
		goto IL_023f;
	}

IL_0053:
	{
		uint16_t L_18 = V_1;
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_0231;
		}
	}
	{
		int32_t* L_19 = ___index;
		CharU5BU5D_t58* L_20 = ___json;
		NullCheck(L_20);
		if ((!(((uint32_t)(*((int32_t*)L_19))) == ((uint32_t)(((int32_t)(((Array_t *)L_20)->max_length)))))))
		{
			goto IL_006a;
		}
	}
	{
		goto IL_023f;
	}

IL_006a:
	{
		CharU5BU5D_t58* L_21 = ___json;
		int32_t* L_22 = ___index;
		int32_t* L_23 = ___index;
		int32_t L_24 = (*((int32_t*)L_23));
		V_6 = L_24;
		*((int32_t*)(L_22)) = (int32_t)((int32_t)((int32_t)L_24+(int32_t)1));
		int32_t L_25 = V_6;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, L_25);
		int32_t L_26 = L_25;
		V_1 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_21, L_26, sizeof(uint16_t)));
		uint16_t L_27 = V_1;
		if ((!(((uint32_t)L_27) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_008e;
		}
	}
	{
		StringBuilder_t296 * L_28 = V_0;
		NullCheck(L_28);
		StringBuilder_Append_m1781(L_28, ((int32_t)34), /*hidden argument*/NULL);
		goto IL_022c;
	}

IL_008e:
	{
		uint16_t L_29 = V_1;
		if ((!(((uint32_t)L_29) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_00a4;
		}
	}
	{
		StringBuilder_t296 * L_30 = V_0;
		NullCheck(L_30);
		StringBuilder_Append_m1781(L_30, ((int32_t)92), /*hidden argument*/NULL);
		goto IL_022c;
	}

IL_00a4:
	{
		uint16_t L_31 = V_1;
		if ((!(((uint32_t)L_31) == ((uint32_t)((int32_t)47)))))
		{
			goto IL_00ba;
		}
	}
	{
		StringBuilder_t296 * L_32 = V_0;
		NullCheck(L_32);
		StringBuilder_Append_m1781(L_32, ((int32_t)47), /*hidden argument*/NULL);
		goto IL_022c;
	}

IL_00ba:
	{
		uint16_t L_33 = V_1;
		if ((!(((uint32_t)L_33) == ((uint32_t)((int32_t)98)))))
		{
			goto IL_00cf;
		}
	}
	{
		StringBuilder_t296 * L_34 = V_0;
		NullCheck(L_34);
		StringBuilder_Append_m1781(L_34, 8, /*hidden argument*/NULL);
		goto IL_022c;
	}

IL_00cf:
	{
		uint16_t L_35 = V_1;
		if ((!(((uint32_t)L_35) == ((uint32_t)((int32_t)102)))))
		{
			goto IL_00e5;
		}
	}
	{
		StringBuilder_t296 * L_36 = V_0;
		NullCheck(L_36);
		StringBuilder_Append_m1781(L_36, ((int32_t)12), /*hidden argument*/NULL);
		goto IL_022c;
	}

IL_00e5:
	{
		uint16_t L_37 = V_1;
		if ((!(((uint32_t)L_37) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_00fb;
		}
	}
	{
		StringBuilder_t296 * L_38 = V_0;
		NullCheck(L_38);
		StringBuilder_Append_m1781(L_38, ((int32_t)10), /*hidden argument*/NULL);
		goto IL_022c;
	}

IL_00fb:
	{
		uint16_t L_39 = V_1;
		if ((!(((uint32_t)L_39) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_0111;
		}
	}
	{
		StringBuilder_t296 * L_40 = V_0;
		NullCheck(L_40);
		StringBuilder_Append_m1781(L_40, ((int32_t)13), /*hidden argument*/NULL);
		goto IL_022c;
	}

IL_0111:
	{
		uint16_t L_41 = V_1;
		if ((!(((uint32_t)L_41) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_0127;
		}
	}
	{
		StringBuilder_t296 * L_42 = V_0;
		NullCheck(L_42);
		StringBuilder_Append_m1781(L_42, ((int32_t)9), /*hidden argument*/NULL);
		goto IL_022c;
	}

IL_0127:
	{
		uint16_t L_43 = V_1;
		if ((!(((uint32_t)L_43) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_022c;
		}
	}
	{
		CharU5BU5D_t58* L_44 = ___json;
		NullCheck(L_44);
		int32_t* L_45 = ___index;
		V_3 = ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_44)->max_length)))-(int32_t)(*((int32_t*)L_45))));
		int32_t L_46 = V_3;
		if ((((int32_t)L_46) < ((int32_t)4)))
		{
			goto IL_0227;
		}
	}
	{
		bool* L_47 = ___success;
		CharU5BU5D_t58* L_48 = ___json;
		int32_t* L_49 = ___index;
		String_t* L_50 = (String_t*)il2cpp_codegen_object_new (String_t_il2cpp_TypeInfo_var);
		L_50 = String_CreateString_m3327(L_50, L_48, (*((int32_t*)L_49)), 4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t659_il2cpp_TypeInfo_var);
		CultureInfo_t659 * L_51 = CultureInfo_get_InvariantCulture_m3328(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_52 = UInt32_TryParse_m3329(NULL /*static, unused*/, L_50, ((int32_t)515), L_51, (&V_4), /*hidden argument*/NULL);
		bool L_53 = L_52;
		V_7 = L_53;
		*((int8_t*)(L_47)) = (int8_t)L_53;
		bool L_54 = V_7;
		if (L_54)
		{
			goto IL_0169;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_55 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_55;
	}

IL_0169:
	{
		uint32_t L_56 = V_4;
		if ((!(((uint32_t)((int32_t)55296)) <= ((uint32_t)L_56))))
		{
			goto IL_020e;
		}
	}
	{
		uint32_t L_57 = V_4;
		if ((!(((uint32_t)L_57) <= ((uint32_t)((int32_t)56319)))))
		{
			goto IL_020e;
		}
	}
	{
		int32_t* L_58 = ___index;
		int32_t* L_59 = ___index;
		*((int32_t*)(L_58)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_59))+(int32_t)4));
		CharU5BU5D_t58* L_60 = ___json;
		NullCheck(L_60);
		int32_t* L_61 = ___index;
		V_3 = ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_60)->max_length)))-(int32_t)(*((int32_t*)L_61))));
		int32_t L_62 = V_3;
		if ((((int32_t)L_62) < ((int32_t)6)))
		{
			goto IL_0205;
		}
	}
	{
		CharU5BU5D_t58* L_63 = ___json;
		int32_t* L_64 = ___index;
		String_t* L_65 = (String_t*)il2cpp_codegen_object_new (String_t_il2cpp_TypeInfo_var);
		L_65 = String_CreateString_m3327(L_65, L_63, (*((int32_t*)L_64)), 2, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_66 = String_op_Equality_m204(NULL /*static, unused*/, L_65, _stringLiteral334, /*hidden argument*/NULL);
		if (!L_66)
		{
			goto IL_0205;
		}
	}
	{
		CharU5BU5D_t58* L_67 = ___json;
		int32_t* L_68 = ___index;
		String_t* L_69 = (String_t*)il2cpp_codegen_object_new (String_t_il2cpp_TypeInfo_var);
		L_69 = String_CreateString_m3327(L_69, L_67, ((int32_t)((int32_t)(*((int32_t*)L_68))+(int32_t)2)), 4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t659_il2cpp_TypeInfo_var);
		CultureInfo_t659 * L_70 = CultureInfo_get_InvariantCulture_m3328(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_71 = UInt32_TryParse_m3329(NULL /*static, unused*/, L_69, ((int32_t)515), L_70, (&V_5), /*hidden argument*/NULL);
		if (!L_71)
		{
			goto IL_0205;
		}
	}
	{
		uint32_t L_72 = V_5;
		if ((!(((uint32_t)((int32_t)56320)) <= ((uint32_t)L_72))))
		{
			goto IL_0205;
		}
	}
	{
		uint32_t L_73 = V_5;
		if ((!(((uint32_t)L_73) <= ((uint32_t)((int32_t)57343)))))
		{
			goto IL_0205;
		}
	}
	{
		StringBuilder_t296 * L_74 = V_0;
		uint32_t L_75 = V_4;
		NullCheck(L_74);
		StringBuilder_Append_m1781(L_74, (((uint16_t)L_75)), /*hidden argument*/NULL);
		StringBuilder_t296 * L_76 = V_0;
		uint32_t L_77 = V_5;
		NullCheck(L_76);
		StringBuilder_Append_m1781(L_76, (((uint16_t)L_77)), /*hidden argument*/NULL);
		int32_t* L_78 = ___index;
		int32_t* L_79 = ___index;
		*((int32_t*)(L_78)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_79))+(int32_t)6));
		goto IL_0239;
	}

IL_0205:
	{
		bool* L_80 = ___success;
		*((int8_t*)(L_80)) = (int8_t)0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_81 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		return L_81;
	}

IL_020e:
	{
		StringBuilder_t296 * L_82 = V_0;
		uint32_t L_83 = V_4;
		String_t* L_84 = SimpleJson_ConvertFromUtf32_m2968(NULL /*static, unused*/, L_83, /*hidden argument*/NULL);
		NullCheck(L_82);
		StringBuilder_Append_m3330(L_82, L_84, /*hidden argument*/NULL);
		int32_t* L_85 = ___index;
		int32_t* L_86 = ___index;
		*((int32_t*)(L_85)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_86))+(int32_t)4));
		goto IL_022c;
	}

IL_0227:
	{
		goto IL_023f;
	}

IL_022c:
	{
		goto IL_0239;
	}

IL_0231:
	{
		StringBuilder_t296 * L_87 = V_0;
		uint16_t L_88 = V_1;
		NullCheck(L_87);
		StringBuilder_Append_m1781(L_87, L_88, /*hidden argument*/NULL);
	}

IL_0239:
	{
		bool L_89 = V_2;
		if (!L_89)
		{
			goto IL_0027;
		}
	}

IL_023f:
	{
		bool L_90 = V_2;
		if (L_90)
		{
			goto IL_024a;
		}
	}
	{
		bool* L_91 = ___success;
		*((int8_t*)(L_91)) = (int8_t)0;
		return (String_t*)NULL;
	}

IL_024a:
	{
		StringBuilder_t296 * L_92 = V_0;
		NullCheck(L_92);
		String_t* L_93 = StringBuilder_ToString_m1480(L_92, /*hidden argument*/NULL);
		return L_93;
	}
}
// System.String SimpleJson.SimpleJson::ConvertFromUtf32(System.Int32)
extern TypeInfo* ArgumentOutOfRangeException_t660_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t58_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral335;
extern Il2CppCodeGenString* _stringLiteral336;
extern Il2CppCodeGenString* _stringLiteral337;
extern "C" String_t* SimpleJson_ConvertFromUtf32_m2968 (Object_t * __this /* static, unused */, int32_t ___utf32, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentOutOfRangeException_t660_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(376);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		CharU5BU5D_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral335 = il2cpp_codegen_string_literal_from_index(335);
		_stringLiteral336 = il2cpp_codegen_string_literal_from_index(336);
		_stringLiteral337 = il2cpp_codegen_string_literal_from_index(337);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___utf32;
		if ((((int32_t)L_0) < ((int32_t)0)))
		{
			goto IL_0012;
		}
	}
	{
		int32_t L_1 = ___utf32;
		if ((((int32_t)L_1) <= ((int32_t)((int32_t)1114111))))
		{
			goto IL_0022;
		}
	}

IL_0012:
	{
		ArgumentOutOfRangeException_t660 * L_2 = (ArgumentOutOfRangeException_t660 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t660_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3331(L_2, _stringLiteral335, _stringLiteral336, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_2);
	}

IL_0022:
	{
		int32_t L_3 = ___utf32;
		if ((((int32_t)((int32_t)55296)) > ((int32_t)L_3)))
		{
			goto IL_0048;
		}
	}
	{
		int32_t L_4 = ___utf32;
		if ((((int32_t)L_4) > ((int32_t)((int32_t)57343))))
		{
			goto IL_0048;
		}
	}
	{
		ArgumentOutOfRangeException_t660 * L_5 = (ArgumentOutOfRangeException_t660 *)il2cpp_codegen_object_new (ArgumentOutOfRangeException_t660_il2cpp_TypeInfo_var);
		ArgumentOutOfRangeException__ctor_m3331(L_5, _stringLiteral335, _stringLiteral337, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_5);
	}

IL_0048:
	{
		int32_t L_6 = ___utf32;
		if ((((int32_t)L_6) >= ((int32_t)((int32_t)65536))))
		{
			goto IL_005c;
		}
	}
	{
		int32_t L_7 = ___utf32;
		String_t* L_8 = (String_t*)il2cpp_codegen_object_new (String_t_il2cpp_TypeInfo_var);
		L_8 = String_CreateString_m1791(L_8, (((uint16_t)L_7)), 1, /*hidden argument*/NULL);
		return L_8;
	}

IL_005c:
	{
		int32_t L_9 = ___utf32;
		___utf32 = ((int32_t)((int32_t)L_9-(int32_t)((int32_t)65536)));
		CharU5BU5D_t58* L_10 = ((CharU5BU5D_t58*)SZArrayNew(CharU5BU5D_t58_il2cpp_TypeInfo_var, 2));
		int32_t L_11 = ___utf32;
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_10, 0, sizeof(uint16_t))) = (uint16_t)(((uint16_t)((int32_t)((int32_t)((int32_t)((int32_t)L_11>>(int32_t)((int32_t)10)))+(int32_t)((int32_t)55296)))));
		CharU5BU5D_t58* L_12 = L_10;
		int32_t L_13 = ___utf32;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 1);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_12, 1, sizeof(uint16_t))) = (uint16_t)(((uint16_t)((int32_t)((int32_t)((int32_t)((int32_t)L_13%(int32_t)((int32_t)1024)))+(int32_t)((int32_t)56320)))));
		String_t* L_14 = (String_t*)il2cpp_codegen_object_new (String_t_il2cpp_TypeInfo_var);
		L_14 = String_CreateString_m3332(L_14, L_12, /*hidden argument*/NULL);
		return L_14;
	}
}
// System.Object SimpleJson.SimpleJson::ParseNumber(System.Char[],System.Int32&,System.Boolean&)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t659_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t662_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral139;
extern Il2CppCodeGenString* _stringLiteral338;
extern "C" Object_t * SimpleJson_ParseNumber_m2969 (Object_t * __this /* static, unused */, CharU5BU5D_t58* ___json, int32_t* ___index, bool* ___success, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		CultureInfo_t659_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(375);
		Double_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(377);
		Int64_t662_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(378);
		_stringLiteral139 = il2cpp_codegen_string_literal_from_index(139);
		_stringLiteral338 = il2cpp_codegen_string_literal_from_index(338);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	Object_t * V_2 = {0};
	String_t* V_3 = {0};
	double V_4 = 0.0;
	int64_t V_5 = 0;
	{
		CharU5BU5D_t58* L_0 = ___json;
		int32_t* L_1 = ___index;
		SimpleJson_EatWhitespace_m2971(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		CharU5BU5D_t58* L_2 = ___json;
		int32_t* L_3 = ___index;
		int32_t L_4 = SimpleJson_GetLastIndexOfNumber_m2970(NULL /*static, unused*/, L_2, (*((int32_t*)L_3)), /*hidden argument*/NULL);
		V_0 = L_4;
		int32_t L_5 = V_0;
		int32_t* L_6 = ___index;
		V_1 = ((int32_t)((int32_t)((int32_t)((int32_t)L_5-(int32_t)(*((int32_t*)L_6))))+(int32_t)1));
		CharU5BU5D_t58* L_7 = ___json;
		int32_t* L_8 = ___index;
		int32_t L_9 = V_1;
		String_t* L_10 = (String_t*)il2cpp_codegen_object_new (String_t_il2cpp_TypeInfo_var);
		L_10 = String_CreateString_m3327(L_10, L_7, (*((int32_t*)L_8)), L_9, /*hidden argument*/NULL);
		V_3 = L_10;
		String_t* L_11 = V_3;
		NullCheck(L_11);
		int32_t L_12 = String_IndexOf_m3267(L_11, _stringLiteral139, 5, /*hidden argument*/NULL);
		if ((!(((uint32_t)L_12) == ((uint32_t)(-1)))))
		{
			goto IL_0045;
		}
	}
	{
		String_t* L_13 = V_3;
		NullCheck(L_13);
		int32_t L_14 = String_IndexOf_m3267(L_13, _stringLiteral338, 5, /*hidden argument*/NULL);
		if ((((int32_t)L_14) == ((int32_t)(-1))))
		{
			goto IL_006e;
		}
	}

IL_0045:
	{
		bool* L_15 = ___success;
		CharU5BU5D_t58* L_16 = ___json;
		int32_t* L_17 = ___index;
		int32_t L_18 = V_1;
		String_t* L_19 = (String_t*)il2cpp_codegen_object_new (String_t_il2cpp_TypeInfo_var);
		L_19 = String_CreateString_m3327(L_19, L_16, (*((int32_t*)L_17)), L_18, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t659_il2cpp_TypeInfo_var);
		CultureInfo_t659 * L_20 = CultureInfo_get_InvariantCulture_m3328(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_21 = Double_TryParse_m3333(NULL /*static, unused*/, L_19, ((int32_t)511), L_20, (&V_4), /*hidden argument*/NULL);
		*((int8_t*)(L_15)) = (int8_t)L_21;
		double L_22 = V_4;
		double L_23 = L_22;
		Object_t * L_24 = Box(Double_t661_il2cpp_TypeInfo_var, &L_23);
		V_2 = L_24;
		goto IL_0092;
	}

IL_006e:
	{
		bool* L_25 = ___success;
		CharU5BU5D_t58* L_26 = ___json;
		int32_t* L_27 = ___index;
		int32_t L_28 = V_1;
		String_t* L_29 = (String_t*)il2cpp_codegen_object_new (String_t_il2cpp_TypeInfo_var);
		L_29 = String_CreateString_m3327(L_29, L_26, (*((int32_t*)L_27)), L_28, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t659_il2cpp_TypeInfo_var);
		CultureInfo_t659 * L_30 = CultureInfo_get_InvariantCulture_m3328(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_31 = Int64_TryParse_m3334(NULL /*static, unused*/, L_29, ((int32_t)511), L_30, (&V_5), /*hidden argument*/NULL);
		*((int8_t*)(L_25)) = (int8_t)L_31;
		int64_t L_32 = V_5;
		int64_t L_33 = L_32;
		Object_t * L_34 = Box(Int64_t662_il2cpp_TypeInfo_var, &L_33);
		V_2 = L_34;
	}

IL_0092:
	{
		int32_t* L_35 = ___index;
		int32_t L_36 = V_0;
		*((int32_t*)(L_35)) = (int32_t)((int32_t)((int32_t)L_36+(int32_t)1));
		Object_t * L_37 = V_2;
		return L_37;
	}
}
// System.Int32 SimpleJson.SimpleJson::GetLastIndexOfNumber(System.Char[],System.Int32)
extern Il2CppCodeGenString* _stringLiteral339;
extern "C" int32_t SimpleJson_GetLastIndexOfNumber_m2970 (Object_t * __this /* static, unused */, CharU5BU5D_t58* ___json, int32_t ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral339 = il2cpp_codegen_string_literal_from_index(339);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		goto IL_0023;
	}

IL_0007:
	{
		CharU5BU5D_t58* L_1 = ___json;
		int32_t L_2 = V_0;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		NullCheck(_stringLiteral339);
		int32_t L_4 = String_IndexOf_m1827(_stringLiteral339, (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_1, L_3, sizeof(uint16_t))), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_4) == ((uint32_t)(-1)))))
		{
			goto IL_001f;
		}
	}
	{
		goto IL_002c;
	}

IL_001f:
	{
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0023:
	{
		int32_t L_6 = V_0;
		CharU5BU5D_t58* L_7 = ___json;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)(((Array_t *)L_7)->max_length))))))
		{
			goto IL_0007;
		}
	}

IL_002c:
	{
		int32_t L_8 = V_0;
		return ((int32_t)((int32_t)L_8-(int32_t)1));
	}
}
// System.Void SimpleJson.SimpleJson::EatWhitespace(System.Char[],System.Int32&)
extern Il2CppCodeGenString* _stringLiteral340;
extern "C" void SimpleJson_EatWhitespace_m2971 (Object_t * __this /* static, unused */, CharU5BU5D_t58* ___json, int32_t* ___index, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral340 = il2cpp_codegen_string_literal_from_index(340);
		s_Il2CppMethodIntialized = true;
	}
	{
		goto IL_0024;
	}

IL_0005:
	{
		CharU5BU5D_t58* L_0 = ___json;
		int32_t* L_1 = ___index;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, (*((int32_t*)L_1)));
		int32_t L_2 = (*((int32_t*)L_1));
		NullCheck(_stringLiteral340);
		int32_t L_3 = String_IndexOf_m1827(_stringLiteral340, (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_0, L_2, sizeof(uint16_t))), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_3) == ((uint32_t)(-1)))))
		{
			goto IL_001e;
		}
	}
	{
		goto IL_002e;
	}

IL_001e:
	{
		int32_t* L_4 = ___index;
		int32_t* L_5 = ___index;
		*((int32_t*)(L_4)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_5))+(int32_t)1));
	}

IL_0024:
	{
		int32_t* L_6 = ___index;
		CharU5BU5D_t58* L_7 = ___json;
		NullCheck(L_7);
		if ((((int32_t)(*((int32_t*)L_6))) < ((int32_t)(((int32_t)(((Array_t *)L_7)->max_length))))))
		{
			goto IL_0005;
		}
	}

IL_002e:
	{
		return;
	}
}
// System.Int32 SimpleJson.SimpleJson::LookAhead(System.Char[],System.Int32)
extern "C" int32_t SimpleJson_LookAhead_m2972 (Object_t * __this /* static, unused */, CharU5BU5D_t58* ___json, int32_t ___index, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		int32_t L_0 = ___index;
		V_0 = L_0;
		CharU5BU5D_t58* L_1 = ___json;
		int32_t L_2 = SimpleJson_NextToken_m2973(NULL /*static, unused*/, L_1, (&V_0), /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Int32 SimpleJson.SimpleJson::NextToken(System.Char[],System.Int32&)
extern "C" int32_t SimpleJson_NextToken_m2973 (Object_t * __this /* static, unused */, CharU5BU5D_t58* ___json, int32_t* ___index, const MethodInfo* method)
{
	uint16_t V_0 = 0x0;
	int32_t V_1 = 0;
	uint16_t V_2 = 0x0;
	{
		CharU5BU5D_t58* L_0 = ___json;
		int32_t* L_1 = ___index;
		SimpleJson_EatWhitespace_m2971(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		int32_t* L_2 = ___index;
		CharU5BU5D_t58* L_3 = ___json;
		NullCheck(L_3);
		if ((!(((uint32_t)(*((int32_t*)L_2))) == ((uint32_t)(((int32_t)(((Array_t *)L_3)->max_length)))))))
		{
			goto IL_0013;
		}
	}
	{
		return 0;
	}

IL_0013:
	{
		CharU5BU5D_t58* L_4 = ___json;
		int32_t* L_5 = ___index;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, (*((int32_t*)L_5)));
		int32_t L_6 = (*((int32_t*)L_5));
		V_0 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_4, L_6, sizeof(uint16_t)));
		int32_t* L_7 = ___index;
		int32_t* L_8 = ___index;
		*((int32_t*)(L_7)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_8))+(int32_t)1));
		uint16_t L_9 = V_0;
		V_2 = L_9;
		uint16_t L_10 = V_2;
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 0)
		{
			goto IL_00c6;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 1)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 2)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 3)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 4)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 5)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 6)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 7)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 8)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 9)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 10)
		{
			goto IL_00c4;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 11)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 12)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 13)
		{
			goto IL_008d;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 14)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 15)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 16)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 17)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 18)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 19)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 20)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 21)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 22)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 23)
		{
			goto IL_00c8;
		}
		if (((int32_t)((int32_t)L_10-(int32_t)((int32_t)34))) == 24)
		{
			goto IL_00ca;
		}
	}

IL_008d:
	{
		uint16_t L_11 = V_2;
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)91))) == 0)
		{
			goto IL_00c0;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)91))) == 1)
		{
			goto IL_00a2;
		}
		if (((int32_t)((int32_t)L_11-(int32_t)((int32_t)91))) == 2)
		{
			goto IL_00c2;
		}
	}

IL_00a2:
	{
		uint16_t L_12 = V_2;
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)123))) == 0)
		{
			goto IL_00bc;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)123))) == 1)
		{
			goto IL_00cc;
		}
		if (((int32_t)((int32_t)L_12-(int32_t)((int32_t)123))) == 2)
		{
			goto IL_00be;
		}
	}
	{
		goto IL_00cc;
	}

IL_00bc:
	{
		return 1;
	}

IL_00be:
	{
		return 2;
	}

IL_00c0:
	{
		return 3;
	}

IL_00c2:
	{
		return 4;
	}

IL_00c4:
	{
		return 6;
	}

IL_00c6:
	{
		return 7;
	}

IL_00c8:
	{
		return 8;
	}

IL_00ca:
	{
		return 5;
	}

IL_00cc:
	{
		int32_t* L_13 = ___index;
		int32_t* L_14 = ___index;
		*((int32_t*)(L_13)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_14))-(int32_t)1));
		CharU5BU5D_t58* L_15 = ___json;
		NullCheck(L_15);
		int32_t* L_16 = ___index;
		V_1 = ((int32_t)((int32_t)(((int32_t)(((Array_t *)L_15)->max_length)))-(int32_t)(*((int32_t*)L_16))));
		int32_t L_17 = V_1;
		if ((((int32_t)L_17) < ((int32_t)5)))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t58* L_18 = ___json;
		int32_t* L_19 = ___index;
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, (*((int32_t*)L_19)));
		int32_t L_20 = (*((int32_t*)L_19));
		if ((!(((uint32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_18, L_20, sizeof(uint16_t)))) == ((uint32_t)((int32_t)102)))))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t58* L_21 = ___json;
		int32_t* L_22 = ___index;
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, ((int32_t)((int32_t)(*((int32_t*)L_22))+(int32_t)1)));
		int32_t L_23 = ((int32_t)((int32_t)(*((int32_t*)L_22))+(int32_t)1));
		if ((!(((uint32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_21, L_23, sizeof(uint16_t)))) == ((uint32_t)((int32_t)97)))))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t58* L_24 = ___json;
		int32_t* L_25 = ___index;
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, ((int32_t)((int32_t)(*((int32_t*)L_25))+(int32_t)2)));
		int32_t L_26 = ((int32_t)((int32_t)(*((int32_t*)L_25))+(int32_t)2));
		if ((!(((uint32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_24, L_26, sizeof(uint16_t)))) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t58* L_27 = ___json;
		int32_t* L_28 = ___index;
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, ((int32_t)((int32_t)(*((int32_t*)L_28))+(int32_t)3)));
		int32_t L_29 = ((int32_t)((int32_t)(*((int32_t*)L_28))+(int32_t)3));
		if ((!(((uint32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_27, L_29, sizeof(uint16_t)))) == ((uint32_t)((int32_t)115)))))
		{
			goto IL_0128;
		}
	}
	{
		CharU5BU5D_t58* L_30 = ___json;
		int32_t* L_31 = ___index;
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, ((int32_t)((int32_t)(*((int32_t*)L_31))+(int32_t)4)));
		int32_t L_32 = ((int32_t)((int32_t)(*((int32_t*)L_31))+(int32_t)4));
		if ((!(((uint32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_30, L_32, sizeof(uint16_t)))) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_0128;
		}
	}
	{
		int32_t* L_33 = ___index;
		int32_t* L_34 = ___index;
		*((int32_t*)(L_33)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_34))+(int32_t)5));
		return ((int32_t)10);
	}

IL_0128:
	{
		int32_t L_35 = V_1;
		if ((((int32_t)L_35) < ((int32_t)4)))
		{
			goto IL_016a;
		}
	}
	{
		CharU5BU5D_t58* L_36 = ___json;
		int32_t* L_37 = ___index;
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, (*((int32_t*)L_37)));
		int32_t L_38 = (*((int32_t*)L_37));
		if ((!(((uint32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_36, L_38, sizeof(uint16_t)))) == ((uint32_t)((int32_t)116)))))
		{
			goto IL_016a;
		}
	}
	{
		CharU5BU5D_t58* L_39 = ___json;
		int32_t* L_40 = ___index;
		NullCheck(L_39);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_39, ((int32_t)((int32_t)(*((int32_t*)L_40))+(int32_t)1)));
		int32_t L_41 = ((int32_t)((int32_t)(*((int32_t*)L_40))+(int32_t)1));
		if ((!(((uint32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_39, L_41, sizeof(uint16_t)))) == ((uint32_t)((int32_t)114)))))
		{
			goto IL_016a;
		}
	}
	{
		CharU5BU5D_t58* L_42 = ___json;
		int32_t* L_43 = ___index;
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, ((int32_t)((int32_t)(*((int32_t*)L_43))+(int32_t)2)));
		int32_t L_44 = ((int32_t)((int32_t)(*((int32_t*)L_43))+(int32_t)2));
		if ((!(((uint32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_42, L_44, sizeof(uint16_t)))) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_016a;
		}
	}
	{
		CharU5BU5D_t58* L_45 = ___json;
		int32_t* L_46 = ___index;
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, ((int32_t)((int32_t)(*((int32_t*)L_46))+(int32_t)3)));
		int32_t L_47 = ((int32_t)((int32_t)(*((int32_t*)L_46))+(int32_t)3));
		if ((!(((uint32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_45, L_47, sizeof(uint16_t)))) == ((uint32_t)((int32_t)101)))))
		{
			goto IL_016a;
		}
	}
	{
		int32_t* L_48 = ___index;
		int32_t* L_49 = ___index;
		*((int32_t*)(L_48)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_49))+(int32_t)4));
		return ((int32_t)9);
	}

IL_016a:
	{
		int32_t L_50 = V_1;
		if ((((int32_t)L_50) < ((int32_t)4)))
		{
			goto IL_01ac;
		}
	}
	{
		CharU5BU5D_t58* L_51 = ___json;
		int32_t* L_52 = ___index;
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, (*((int32_t*)L_52)));
		int32_t L_53 = (*((int32_t*)L_52));
		if ((!(((uint32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_51, L_53, sizeof(uint16_t)))) == ((uint32_t)((int32_t)110)))))
		{
			goto IL_01ac;
		}
	}
	{
		CharU5BU5D_t58* L_54 = ___json;
		int32_t* L_55 = ___index;
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, ((int32_t)((int32_t)(*((int32_t*)L_55))+(int32_t)1)));
		int32_t L_56 = ((int32_t)((int32_t)(*((int32_t*)L_55))+(int32_t)1));
		if ((!(((uint32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_54, L_56, sizeof(uint16_t)))) == ((uint32_t)((int32_t)117)))))
		{
			goto IL_01ac;
		}
	}
	{
		CharU5BU5D_t58* L_57 = ___json;
		int32_t* L_58 = ___index;
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, ((int32_t)((int32_t)(*((int32_t*)L_58))+(int32_t)2)));
		int32_t L_59 = ((int32_t)((int32_t)(*((int32_t*)L_58))+(int32_t)2));
		if ((!(((uint32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_57, L_59, sizeof(uint16_t)))) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_01ac;
		}
	}
	{
		CharU5BU5D_t58* L_60 = ___json;
		int32_t* L_61 = ___index;
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, ((int32_t)((int32_t)(*((int32_t*)L_61))+(int32_t)3)));
		int32_t L_62 = ((int32_t)((int32_t)(*((int32_t*)L_61))+(int32_t)3));
		if ((!(((uint32_t)(*(uint16_t*)(uint16_t*)SZArrayLdElema(L_60, L_62, sizeof(uint16_t)))) == ((uint32_t)((int32_t)108)))))
		{
			goto IL_01ac;
		}
	}
	{
		int32_t* L_63 = ___index;
		int32_t* L_64 = ___index;
		*((int32_t*)(L_63)) = (int32_t)((int32_t)((int32_t)(*((int32_t*)L_64))+(int32_t)4));
		return ((int32_t)11);
	}

IL_01ac:
	{
		return 0;
	}
}
// System.Boolean SimpleJson.SimpleJson::SerializeValue(SimpleJson.IJsonSerializerStrategy,System.Object,System.Text.StringBuilder)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_2_t613_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_2_t663_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_t623_il2cpp_TypeInfo_var;
extern TypeInfo* Boolean_t298_il2cpp_TypeInfo_var;
extern TypeInfo* IJsonSerializerStrategy_t515_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral341;
extern Il2CppCodeGenString* _stringLiteral342;
extern Il2CppCodeGenString* _stringLiteral343;
extern "C" bool SimpleJson_SerializeValue_m2974 (Object_t * __this /* static, unused */, Object_t * ___jsonSerializerStrategy, Object_t * ___value, StringBuilder_t296 * ___builder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		IDictionary_2_t613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(345);
		IDictionary_2_t663_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(379);
		IEnumerable_t623_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(380);
		Boolean_t298_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(104);
		IJsonSerializerStrategy_t515_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(381);
		_stringLiteral341 = il2cpp_codegen_string_literal_from_index(341);
		_stringLiteral342 = il2cpp_codegen_string_literal_from_index(342);
		_stringLiteral343 = il2cpp_codegen_string_literal_from_index(343);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	String_t* V_1 = {0};
	Object_t* V_2 = {0};
	Object_t* V_3 = {0};
	Object_t * V_4 = {0};
	Object_t * V_5 = {0};
	StringBuilder_t296 * G_B13_0 = {0};
	StringBuilder_t296 * G_B12_0 = {0};
	String_t* G_B14_0 = {0};
	StringBuilder_t296 * G_B14_1 = {0};
	{
		V_0 = 1;
		Object_t * L_0 = ___value;
		V_1 = ((String_t*)IsInstSealed(L_0, String_t_il2cpp_TypeInfo_var));
		String_t* L_1 = V_1;
		if (!L_1)
		{
			goto IL_001c;
		}
	}
	{
		String_t* L_2 = V_1;
		StringBuilder_t296 * L_3 = ___builder;
		bool L_4 = SimpleJson_SerializeString_m2977(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		goto IL_0100;
	}

IL_001c:
	{
		Object_t * L_5 = ___value;
		V_2 = ((Object_t*)IsInst(L_5, IDictionary_2_t613_il2cpp_TypeInfo_var));
		Object_t* L_6 = V_2;
		if (!L_6)
		{
			goto IL_0042;
		}
	}
	{
		Object_t * L_7 = ___jsonSerializerStrategy;
		Object_t* L_8 = V_2;
		NullCheck(L_8);
		Object_t* L_9 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(4 /* System.Collections.Generic.ICollection`1<!0> System.Collections.Generic.IDictionary`2<System.String,System.Object>::get_Keys() */, IDictionary_2_t613_il2cpp_TypeInfo_var, L_8);
		Object_t* L_10 = V_2;
		NullCheck(L_10);
		Object_t* L_11 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(5 /* System.Collections.Generic.ICollection`1<!1> System.Collections.Generic.IDictionary`2<System.String,System.Object>::get_Values() */, IDictionary_2_t613_il2cpp_TypeInfo_var, L_10);
		StringBuilder_t296 * L_12 = ___builder;
		bool L_13 = SimpleJson_SerializeObject_m2975(NULL /*static, unused*/, L_7, L_9, L_11, L_12, /*hidden argument*/NULL);
		V_0 = L_13;
		goto IL_0100;
	}

IL_0042:
	{
		Object_t * L_14 = ___value;
		V_3 = ((Object_t*)IsInst(L_14, IDictionary_2_t663_il2cpp_TypeInfo_var));
		Object_t* L_15 = V_3;
		if (!L_15)
		{
			goto IL_0068;
		}
	}
	{
		Object_t * L_16 = ___jsonSerializerStrategy;
		Object_t* L_17 = V_3;
		NullCheck(L_17);
		Object_t* L_18 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(4 /* System.Collections.Generic.ICollection`1<!0> System.Collections.Generic.IDictionary`2<System.String,System.String>::get_Keys() */, IDictionary_2_t663_il2cpp_TypeInfo_var, L_17);
		Object_t* L_19 = V_3;
		NullCheck(L_19);
		Object_t* L_20 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(5 /* System.Collections.Generic.ICollection`1<!1> System.Collections.Generic.IDictionary`2<System.String,System.String>::get_Values() */, IDictionary_2_t663_il2cpp_TypeInfo_var, L_19);
		StringBuilder_t296 * L_21 = ___builder;
		bool L_22 = SimpleJson_SerializeObject_m2975(NULL /*static, unused*/, L_16, L_18, L_20, L_21, /*hidden argument*/NULL);
		V_0 = L_22;
		goto IL_0100;
	}

IL_0068:
	{
		Object_t * L_23 = ___value;
		V_4 = ((Object_t *)IsInst(L_23, IEnumerable_t623_il2cpp_TypeInfo_var));
		Object_t * L_24 = V_4;
		if (!L_24)
		{
			goto IL_0086;
		}
	}
	{
		Object_t * L_25 = ___jsonSerializerStrategy;
		Object_t * L_26 = V_4;
		StringBuilder_t296 * L_27 = ___builder;
		bool L_28 = SimpleJson_SerializeArray_m2976(NULL /*static, unused*/, L_25, L_26, L_27, /*hidden argument*/NULL);
		V_0 = L_28;
		goto IL_0100;
	}

IL_0086:
	{
		Object_t * L_29 = ___value;
		bool L_30 = SimpleJson_IsNumeric_m2979(NULL /*static, unused*/, L_29, /*hidden argument*/NULL);
		if (!L_30)
		{
			goto IL_009e;
		}
	}
	{
		Object_t * L_31 = ___value;
		StringBuilder_t296 * L_32 = ___builder;
		bool L_33 = SimpleJson_SerializeNumber_m2978(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		V_0 = L_33;
		goto IL_0100;
	}

IL_009e:
	{
		Object_t * L_34 = ___value;
		if (!((Object_t *)IsInstSealed(L_34, Boolean_t298_il2cpp_TypeInfo_var)))
		{
			goto IL_00cf;
		}
	}
	{
		StringBuilder_t296 * L_35 = ___builder;
		Object_t * L_36 = ___value;
		G_B12_0 = L_35;
		if (!((*(bool*)((bool*)UnBox (L_36, Boolean_t298_il2cpp_TypeInfo_var)))))
		{
			G_B13_0 = L_35;
			goto IL_00bf;
		}
	}
	{
		G_B14_0 = _stringLiteral341;
		G_B14_1 = G_B12_0;
		goto IL_00c4;
	}

IL_00bf:
	{
		G_B14_0 = _stringLiteral342;
		G_B14_1 = G_B13_0;
	}

IL_00c4:
	{
		NullCheck(G_B14_1);
		StringBuilder_Append_m3330(G_B14_1, G_B14_0, /*hidden argument*/NULL);
		goto IL_0100;
	}

IL_00cf:
	{
		Object_t * L_37 = ___value;
		if (L_37)
		{
			goto IL_00e6;
		}
	}
	{
		StringBuilder_t296 * L_38 = ___builder;
		NullCheck(L_38);
		StringBuilder_Append_m3330(L_38, _stringLiteral343, /*hidden argument*/NULL);
		goto IL_0100;
	}

IL_00e6:
	{
		Object_t * L_39 = ___jsonSerializerStrategy;
		Object_t * L_40 = ___value;
		NullCheck(L_39);
		bool L_41 = (bool)InterfaceFuncInvoker2< bool, Object_t *, Object_t ** >::Invoke(0 /* System.Boolean SimpleJson.IJsonSerializerStrategy::TrySerializeNonPrimitiveObject(System.Object,System.Object&) */, IJsonSerializerStrategy_t515_il2cpp_TypeInfo_var, L_39, L_40, (&V_5));
		V_0 = L_41;
		bool L_42 = V_0;
		if (!L_42)
		{
			goto IL_0100;
		}
	}
	{
		Object_t * L_43 = ___jsonSerializerStrategy;
		Object_t * L_44 = V_5;
		StringBuilder_t296 * L_45 = ___builder;
		SimpleJson_SerializeValue_m2974(NULL /*static, unused*/, L_43, L_44, L_45, /*hidden argument*/NULL);
	}

IL_0100:
	{
		bool L_46 = V_0;
		return L_46;
	}
}
// System.Boolean SimpleJson.SimpleJson::SerializeObject(SimpleJson.IJsonSerializerStrategy,System.Collections.IEnumerable,System.Collections.IEnumerable,System.Text.StringBuilder)
extern TypeInfo* IEnumerable_t623_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t51_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral344;
extern Il2CppCodeGenString* _stringLiteral345;
extern Il2CppCodeGenString* _stringLiteral346;
extern Il2CppCodeGenString* _stringLiteral181;
extern "C" bool SimpleJson_SerializeObject_m2975 (Object_t * __this /* static, unused */, Object_t * ___jsonSerializerStrategy, Object_t * ___keys, Object_t * ___values, StringBuilder_t296 * ___builder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerable_t623_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(380);
		IEnumerator_t51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral344 = il2cpp_codegen_string_literal_from_index(344);
		_stringLiteral345 = il2cpp_codegen_string_literal_from_index(345);
		_stringLiteral346 = il2cpp_codegen_string_literal_from_index(346);
		_stringLiteral181 = il2cpp_codegen_string_literal_from_index(181);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * V_0 = {0};
	Object_t * V_1 = {0};
	bool V_2 = false;
	Object_t * V_3 = {0};
	Object_t * V_4 = {0};
	String_t* V_5 = {0};
	{
		StringBuilder_t296 * L_0 = ___builder;
		NullCheck(L_0);
		StringBuilder_Append_m3330(L_0, _stringLiteral344, /*hidden argument*/NULL);
		Object_t * L_1 = ___keys;
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t623_il2cpp_TypeInfo_var, L_1);
		V_0 = L_2;
		Object_t * L_3 = ___values;
		NullCheck(L_3);
		Object_t * L_4 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t623_il2cpp_TypeInfo_var, L_3);
		V_1 = L_4;
		V_2 = 1;
		goto IL_008d;
	}

IL_0021:
	{
		Object_t * L_5 = V_0;
		NullCheck(L_5);
		Object_t * L_6 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t51_il2cpp_TypeInfo_var, L_5);
		V_3 = L_6;
		Object_t * L_7 = V_1;
		NullCheck(L_7);
		Object_t * L_8 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t51_il2cpp_TypeInfo_var, L_7);
		V_4 = L_8;
		bool L_9 = V_2;
		if (L_9)
		{
			goto IL_0042;
		}
	}
	{
		StringBuilder_t296 * L_10 = ___builder;
		NullCheck(L_10);
		StringBuilder_Append_m3330(L_10, _stringLiteral345, /*hidden argument*/NULL);
	}

IL_0042:
	{
		Object_t * L_11 = V_3;
		V_5 = ((String_t*)IsInstSealed(L_11, String_t_il2cpp_TypeInfo_var));
		String_t* L_12 = V_5;
		if (!L_12)
		{
			goto IL_005f;
		}
	}
	{
		String_t* L_13 = V_5;
		StringBuilder_t296 * L_14 = ___builder;
		SimpleJson_SerializeString_m2977(NULL /*static, unused*/, L_13, L_14, /*hidden argument*/NULL);
		goto IL_006f;
	}

IL_005f:
	{
		Object_t * L_15 = ___jsonSerializerStrategy;
		Object_t * L_16 = V_4;
		StringBuilder_t296 * L_17 = ___builder;
		bool L_18 = SimpleJson_SerializeValue_m2974(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		if (L_18)
		{
			goto IL_006f;
		}
	}
	{
		return 0;
	}

IL_006f:
	{
		StringBuilder_t296 * L_19 = ___builder;
		NullCheck(L_19);
		StringBuilder_Append_m3330(L_19, _stringLiteral346, /*hidden argument*/NULL);
		Object_t * L_20 = ___jsonSerializerStrategy;
		Object_t * L_21 = V_4;
		StringBuilder_t296 * L_22 = ___builder;
		bool L_23 = SimpleJson_SerializeValue_m2974(NULL /*static, unused*/, L_20, L_21, L_22, /*hidden argument*/NULL);
		if (L_23)
		{
			goto IL_008b;
		}
	}
	{
		return 0;
	}

IL_008b:
	{
		V_2 = 0;
	}

IL_008d:
	{
		Object_t * L_24 = V_0;
		NullCheck(L_24);
		bool L_25 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t51_il2cpp_TypeInfo_var, L_24);
		if (!L_25)
		{
			goto IL_00a3;
		}
	}
	{
		Object_t * L_26 = V_1;
		NullCheck(L_26);
		bool L_27 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t51_il2cpp_TypeInfo_var, L_26);
		if (L_27)
		{
			goto IL_0021;
		}
	}

IL_00a3:
	{
		StringBuilder_t296 * L_28 = ___builder;
		NullCheck(L_28);
		StringBuilder_Append_m3330(L_28, _stringLiteral181, /*hidden argument*/NULL);
		return 1;
	}
}
// System.Boolean SimpleJson.SimpleJson::SerializeArray(SimpleJson.IJsonSerializerStrategy,System.Collections.IEnumerable,System.Text.StringBuilder)
extern TypeInfo* IEnumerable_t623_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t51_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t303_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral347;
extern Il2CppCodeGenString* _stringLiteral345;
extern Il2CppCodeGenString* _stringLiteral348;
extern "C" bool SimpleJson_SerializeArray_m2976 (Object_t * __this /* static, unused */, Object_t * ___jsonSerializerStrategy, Object_t * ___anArray, StringBuilder_t296 * ___builder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerable_t623_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(380);
		IEnumerator_t51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		IDisposable_t303_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		_stringLiteral347 = il2cpp_codegen_string_literal_from_index(347);
		_stringLiteral345 = il2cpp_codegen_string_literal_from_index(345);
		_stringLiteral348 = il2cpp_codegen_string_literal_from_index(348);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	bool V_3 = false;
	Object_t * V_4 = {0};
	Exception_t301 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t301 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		StringBuilder_t296 * L_0 = ___builder;
		NullCheck(L_0);
		StringBuilder_Append_m3330(L_0, _stringLiteral347, /*hidden argument*/NULL);
		V_0 = 1;
		Object_t * L_1 = ___anArray;
		NullCheck(L_1);
		Object_t * L_2 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Collections.IEnumerator System.Collections.IEnumerable::GetEnumerator() */, IEnumerable_t623_il2cpp_TypeInfo_var, L_1);
		V_2 = L_2;
	}

IL_0015:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0049;
		}

IL_001a:
		{
			Object_t * L_3 = V_2;
			NullCheck(L_3);
			Object_t * L_4 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t51_il2cpp_TypeInfo_var, L_3);
			V_1 = L_4;
			bool L_5 = V_0;
			if (L_5)
			{
				goto IL_0033;
			}
		}

IL_0027:
		{
			StringBuilder_t296 * L_6 = ___builder;
			NullCheck(L_6);
			StringBuilder_Append_m3330(L_6, _stringLiteral345, /*hidden argument*/NULL);
		}

IL_0033:
		{
			Object_t * L_7 = ___jsonSerializerStrategy;
			Object_t * L_8 = V_1;
			StringBuilder_t296 * L_9 = ___builder;
			bool L_10 = SimpleJson_SerializeValue_m2974(NULL /*static, unused*/, L_7, L_8, L_9, /*hidden argument*/NULL);
			if (L_10)
			{
				goto IL_0047;
			}
		}

IL_0040:
		{
			V_3 = 0;
			IL2CPP_LEAVE(0x7C, FINALLY_0059);
		}

IL_0047:
		{
			V_0 = 0;
		}

IL_0049:
		{
			Object_t * L_11 = V_2;
			NullCheck(L_11);
			bool L_12 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t51_il2cpp_TypeInfo_var, L_11);
			if (L_12)
			{
				goto IL_001a;
			}
		}

IL_0054:
		{
			IL2CPP_LEAVE(0x6E, FINALLY_0059);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t301 *)e.ex;
		goto FINALLY_0059;
	}

FINALLY_0059:
	{ // begin finally (depth: 1)
		{
			Object_t * L_13 = V_2;
			V_4 = ((Object_t *)IsInst(L_13, IDisposable_t303_il2cpp_TypeInfo_var));
			Object_t * L_14 = V_4;
			if (L_14)
			{
				goto IL_0066;
			}
		}

IL_0065:
		{
			IL2CPP_END_FINALLY(89)
		}

IL_0066:
		{
			Object_t * L_15 = V_4;
			NullCheck(L_15);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t303_il2cpp_TypeInfo_var, L_15);
			IL2CPP_END_FINALLY(89)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(89)
	{
		IL2CPP_JUMP_TBL(0x7C, IL_007c)
		IL2CPP_JUMP_TBL(0x6E, IL_006e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t301 *)
	}

IL_006e:
	{
		StringBuilder_t296 * L_16 = ___builder;
		NullCheck(L_16);
		StringBuilder_Append_m3330(L_16, _stringLiteral348, /*hidden argument*/NULL);
		return 1;
	}

IL_007c:
	{
		bool L_17 = V_3;
		return L_17;
	}
}
// System.Boolean SimpleJson.SimpleJson::SerializeString(System.String,System.Text.StringBuilder)
extern Il2CppCodeGenString* _stringLiteral246;
extern Il2CppCodeGenString* _stringLiteral349;
extern Il2CppCodeGenString* _stringLiteral350;
extern Il2CppCodeGenString* _stringLiteral351;
extern Il2CppCodeGenString* _stringLiteral352;
extern Il2CppCodeGenString* _stringLiteral353;
extern Il2CppCodeGenString* _stringLiteral354;
extern Il2CppCodeGenString* _stringLiteral355;
extern "C" bool SimpleJson_SerializeString_m2977 (Object_t * __this /* static, unused */, String_t* ___aString, StringBuilder_t296 * ___builder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral246 = il2cpp_codegen_string_literal_from_index(246);
		_stringLiteral349 = il2cpp_codegen_string_literal_from_index(349);
		_stringLiteral350 = il2cpp_codegen_string_literal_from_index(350);
		_stringLiteral351 = il2cpp_codegen_string_literal_from_index(351);
		_stringLiteral352 = il2cpp_codegen_string_literal_from_index(352);
		_stringLiteral353 = il2cpp_codegen_string_literal_from_index(353);
		_stringLiteral354 = il2cpp_codegen_string_literal_from_index(354);
		_stringLiteral355 = il2cpp_codegen_string_literal_from_index(355);
		s_Il2CppMethodIntialized = true;
	}
	CharU5BU5D_t58* V_0 = {0};
	int32_t V_1 = 0;
	uint16_t V_2 = 0x0;
	{
		StringBuilder_t296 * L_0 = ___builder;
		NullCheck(L_0);
		StringBuilder_Append_m3330(L_0, _stringLiteral246, /*hidden argument*/NULL);
		String_t* L_1 = ___aString;
		NullCheck(L_1);
		CharU5BU5D_t58* L_2 = String_ToCharArray_m3325(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = 0;
		goto IL_00d8;
	}

IL_001a:
	{
		CharU5BU5D_t58* L_3 = V_0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_2 = (*(uint16_t*)(uint16_t*)SZArrayLdElema(L_3, L_5, sizeof(uint16_t)));
		uint16_t L_6 = V_2;
		if ((!(((uint32_t)L_6) == ((uint32_t)((int32_t)34)))))
		{
			goto IL_0037;
		}
	}
	{
		StringBuilder_t296 * L_7 = ___builder;
		NullCheck(L_7);
		StringBuilder_Append_m3330(L_7, _stringLiteral349, /*hidden argument*/NULL);
		goto IL_00d4;
	}

IL_0037:
	{
		uint16_t L_8 = V_2;
		if ((!(((uint32_t)L_8) == ((uint32_t)((int32_t)92)))))
		{
			goto IL_0050;
		}
	}
	{
		StringBuilder_t296 * L_9 = ___builder;
		NullCheck(L_9);
		StringBuilder_Append_m3330(L_9, _stringLiteral350, /*hidden argument*/NULL);
		goto IL_00d4;
	}

IL_0050:
	{
		uint16_t L_10 = V_2;
		if ((!(((uint32_t)L_10) == ((uint32_t)8))))
		{
			goto IL_0068;
		}
	}
	{
		StringBuilder_t296 * L_11 = ___builder;
		NullCheck(L_11);
		StringBuilder_Append_m3330(L_11, _stringLiteral351, /*hidden argument*/NULL);
		goto IL_00d4;
	}

IL_0068:
	{
		uint16_t L_12 = V_2;
		if ((!(((uint32_t)L_12) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0081;
		}
	}
	{
		StringBuilder_t296 * L_13 = ___builder;
		NullCheck(L_13);
		StringBuilder_Append_m3330(L_13, _stringLiteral352, /*hidden argument*/NULL);
		goto IL_00d4;
	}

IL_0081:
	{
		uint16_t L_14 = V_2;
		if ((!(((uint32_t)L_14) == ((uint32_t)((int32_t)10)))))
		{
			goto IL_009a;
		}
	}
	{
		StringBuilder_t296 * L_15 = ___builder;
		NullCheck(L_15);
		StringBuilder_Append_m3330(L_15, _stringLiteral353, /*hidden argument*/NULL);
		goto IL_00d4;
	}

IL_009a:
	{
		uint16_t L_16 = V_2;
		if ((!(((uint32_t)L_16) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_00b3;
		}
	}
	{
		StringBuilder_t296 * L_17 = ___builder;
		NullCheck(L_17);
		StringBuilder_Append_m3330(L_17, _stringLiteral354, /*hidden argument*/NULL);
		goto IL_00d4;
	}

IL_00b3:
	{
		uint16_t L_18 = V_2;
		if ((!(((uint32_t)L_18) == ((uint32_t)((int32_t)9)))))
		{
			goto IL_00cc;
		}
	}
	{
		StringBuilder_t296 * L_19 = ___builder;
		NullCheck(L_19);
		StringBuilder_Append_m3330(L_19, _stringLiteral355, /*hidden argument*/NULL);
		goto IL_00d4;
	}

IL_00cc:
	{
		StringBuilder_t296 * L_20 = ___builder;
		uint16_t L_21 = V_2;
		NullCheck(L_20);
		StringBuilder_Append_m1781(L_20, L_21, /*hidden argument*/NULL);
	}

IL_00d4:
	{
		int32_t L_22 = V_1;
		V_1 = ((int32_t)((int32_t)L_22+(int32_t)1));
	}

IL_00d8:
	{
		int32_t L_23 = V_1;
		CharU5BU5D_t58* L_24 = V_0;
		NullCheck(L_24);
		if ((((int32_t)L_23) < ((int32_t)(((int32_t)(((Array_t *)L_24)->max_length))))))
		{
			goto IL_001a;
		}
	}
	{
		StringBuilder_t296 * L_25 = ___builder;
		NullCheck(L_25);
		StringBuilder_Append_m3330(L_25, _stringLiteral246, /*hidden argument*/NULL);
		return 1;
	}
}
// System.Boolean SimpleJson.SimpleJson::SerializeNumber(System.Object,System.Text.StringBuilder)
extern TypeInfo* Int64_t662_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t659_il2cpp_TypeInfo_var;
extern TypeInfo* UInt64_t665_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t56_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t654_il2cpp_TypeInfo_var;
extern TypeInfo* Decimal_t664_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t61_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t645_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral356;
extern "C" bool SimpleJson_SerializeNumber_m2978 (Object_t * __this /* static, unused */, Object_t * ___number, StringBuilder_t296 * ___builder, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int64_t662_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(378);
		CultureInfo_t659_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(375);
		UInt64_t665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(382);
		Int32_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		UInt32_t654_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(347);
		Decimal_t664_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(383);
		Single_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		Convert_t645_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(311);
		_stringLiteral356 = il2cpp_codegen_string_literal_from_index(356);
		s_Il2CppMethodIntialized = true;
	}
	int64_t V_0 = 0;
	uint64_t V_1 = 0;
	int32_t V_2 = 0;
	uint32_t V_3 = 0;
	Decimal_t664  V_4 = {0};
	float V_5 = 0.0f;
	double V_6 = 0.0;
	{
		Object_t * L_0 = ___number;
		if (!((Object_t *)IsInstSealed(L_0, Int64_t662_il2cpp_TypeInfo_var)))
		{
			goto IL_002a;
		}
	}
	{
		StringBuilder_t296 * L_1 = ___builder;
		Object_t * L_2 = ___number;
		V_0 = ((*(int64_t*)((int64_t*)UnBox (L_2, Int64_t662_il2cpp_TypeInfo_var))));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t659_il2cpp_TypeInfo_var);
		CultureInfo_t659 * L_3 = CultureInfo_get_InvariantCulture_m3328(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_4 = Int64_ToString_m3335((&V_0), L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		StringBuilder_Append_m3330(L_1, L_4, /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_002a:
	{
		Object_t * L_5 = ___number;
		if (!((Object_t *)IsInstSealed(L_5, UInt64_t665_il2cpp_TypeInfo_var)))
		{
			goto IL_0054;
		}
	}
	{
		StringBuilder_t296 * L_6 = ___builder;
		Object_t * L_7 = ___number;
		V_1 = ((*(uint64_t*)((uint64_t*)UnBox (L_7, UInt64_t665_il2cpp_TypeInfo_var))));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t659_il2cpp_TypeInfo_var);
		CultureInfo_t659 * L_8 = CultureInfo_get_InvariantCulture_m3328(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_9 = UInt64_ToString_m3336((&V_1), L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		StringBuilder_Append_m3330(L_6, L_9, /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_0054:
	{
		Object_t * L_10 = ___number;
		if (!((Object_t *)IsInstSealed(L_10, Int32_t56_il2cpp_TypeInfo_var)))
		{
			goto IL_007e;
		}
	}
	{
		StringBuilder_t296 * L_11 = ___builder;
		Object_t * L_12 = ___number;
		V_2 = ((*(int32_t*)((int32_t*)UnBox (L_12, Int32_t56_il2cpp_TypeInfo_var))));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t659_il2cpp_TypeInfo_var);
		CultureInfo_t659 * L_13 = CultureInfo_get_InvariantCulture_m3328(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_14 = Int32_ToString_m3337((&V_2), L_13, /*hidden argument*/NULL);
		NullCheck(L_11);
		StringBuilder_Append_m3330(L_11, L_14, /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_007e:
	{
		Object_t * L_15 = ___number;
		if (!((Object_t *)IsInstSealed(L_15, UInt32_t654_il2cpp_TypeInfo_var)))
		{
			goto IL_00a8;
		}
	}
	{
		StringBuilder_t296 * L_16 = ___builder;
		Object_t * L_17 = ___number;
		V_3 = ((*(uint32_t*)((uint32_t*)UnBox (L_17, UInt32_t654_il2cpp_TypeInfo_var))));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t659_il2cpp_TypeInfo_var);
		CultureInfo_t659 * L_18 = CultureInfo_get_InvariantCulture_m3328(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_19 = UInt32_ToString_m3338((&V_3), L_18, /*hidden argument*/NULL);
		NullCheck(L_16);
		StringBuilder_Append_m3330(L_16, L_19, /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00a8:
	{
		Object_t * L_20 = ___number;
		if (!((Object_t *)IsInstSealed(L_20, Decimal_t664_il2cpp_TypeInfo_var)))
		{
			goto IL_00d3;
		}
	}
	{
		StringBuilder_t296 * L_21 = ___builder;
		Object_t * L_22 = ___number;
		V_4 = ((*(Decimal_t664 *)((Decimal_t664 *)UnBox (L_22, Decimal_t664_il2cpp_TypeInfo_var))));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t659_il2cpp_TypeInfo_var);
		CultureInfo_t659 * L_23 = CultureInfo_get_InvariantCulture_m3328(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_24 = Decimal_ToString_m3339((&V_4), L_23, /*hidden argument*/NULL);
		NullCheck(L_21);
		StringBuilder_Append_m3330(L_21, L_24, /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00d3:
	{
		Object_t * L_25 = ___number;
		if (!((Object_t *)IsInstSealed(L_25, Single_t61_il2cpp_TypeInfo_var)))
		{
			goto IL_00fe;
		}
	}
	{
		StringBuilder_t296 * L_26 = ___builder;
		Object_t * L_27 = ___number;
		V_5 = ((*(float*)((float*)UnBox (L_27, Single_t61_il2cpp_TypeInfo_var))));
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t659_il2cpp_TypeInfo_var);
		CultureInfo_t659 * L_28 = CultureInfo_get_InvariantCulture_m3328(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_29 = Single_ToString_m3340((&V_5), L_28, /*hidden argument*/NULL);
		NullCheck(L_26);
		StringBuilder_Append_m3330(L_26, L_29, /*hidden argument*/NULL);
		goto IL_0123;
	}

IL_00fe:
	{
		StringBuilder_t296 * L_30 = ___builder;
		Object_t * L_31 = ___number;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t659_il2cpp_TypeInfo_var);
		CultureInfo_t659 * L_32 = CultureInfo_get_InvariantCulture_m3328(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t645_il2cpp_TypeInfo_var);
		double L_33 = Convert_ToDouble_m3341(NULL /*static, unused*/, L_31, L_32, /*hidden argument*/NULL);
		V_6 = L_33;
		CultureInfo_t659 * L_34 = CultureInfo_get_InvariantCulture_m3328(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_35 = Double_ToString_m3342((&V_6), _stringLiteral356, L_34, /*hidden argument*/NULL);
		NullCheck(L_30);
		StringBuilder_Append_m3330(L_30, L_35, /*hidden argument*/NULL);
	}

IL_0123:
	{
		return 1;
	}
}
// System.Boolean SimpleJson.SimpleJson::IsNumeric(System.Object)
extern TypeInfo* SByte_t666_il2cpp_TypeInfo_var;
extern TypeInfo* Byte_t647_il2cpp_TypeInfo_var;
extern TypeInfo* Int16_t667_il2cpp_TypeInfo_var;
extern TypeInfo* UInt16_t652_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t56_il2cpp_TypeInfo_var;
extern TypeInfo* UInt32_t654_il2cpp_TypeInfo_var;
extern TypeInfo* Int64_t662_il2cpp_TypeInfo_var;
extern TypeInfo* UInt64_t665_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t61_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t661_il2cpp_TypeInfo_var;
extern TypeInfo* Decimal_t664_il2cpp_TypeInfo_var;
extern "C" bool SimpleJson_IsNumeric_m2979 (Object_t * __this /* static, unused */, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SByte_t666_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(384);
		Byte_t647_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(316);
		Int16_t667_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(385);
		UInt16_t652_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(342);
		Int32_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		UInt32_t654_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(347);
		Int64_t662_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(378);
		UInt64_t665_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(382);
		Single_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		Double_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(377);
		Decimal_t664_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(383);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t * L_0 = ___value;
		if (!((Object_t *)IsInstSealed(L_0, SByte_t666_il2cpp_TypeInfo_var)))
		{
			goto IL_000d;
		}
	}
	{
		return 1;
	}

IL_000d:
	{
		Object_t * L_1 = ___value;
		if (!((Object_t *)IsInstSealed(L_1, Byte_t647_il2cpp_TypeInfo_var)))
		{
			goto IL_001a;
		}
	}
	{
		return 1;
	}

IL_001a:
	{
		Object_t * L_2 = ___value;
		if (!((Object_t *)IsInstSealed(L_2, Int16_t667_il2cpp_TypeInfo_var)))
		{
			goto IL_0027;
		}
	}
	{
		return 1;
	}

IL_0027:
	{
		Object_t * L_3 = ___value;
		if (!((Object_t *)IsInstSealed(L_3, UInt16_t652_il2cpp_TypeInfo_var)))
		{
			goto IL_0034;
		}
	}
	{
		return 1;
	}

IL_0034:
	{
		Object_t * L_4 = ___value;
		if (!((Object_t *)IsInstSealed(L_4, Int32_t56_il2cpp_TypeInfo_var)))
		{
			goto IL_0041;
		}
	}
	{
		return 1;
	}

IL_0041:
	{
		Object_t * L_5 = ___value;
		if (!((Object_t *)IsInstSealed(L_5, UInt32_t654_il2cpp_TypeInfo_var)))
		{
			goto IL_004e;
		}
	}
	{
		return 1;
	}

IL_004e:
	{
		Object_t * L_6 = ___value;
		if (!((Object_t *)IsInstSealed(L_6, Int64_t662_il2cpp_TypeInfo_var)))
		{
			goto IL_005b;
		}
	}
	{
		return 1;
	}

IL_005b:
	{
		Object_t * L_7 = ___value;
		if (!((Object_t *)IsInstSealed(L_7, UInt64_t665_il2cpp_TypeInfo_var)))
		{
			goto IL_0068;
		}
	}
	{
		return 1;
	}

IL_0068:
	{
		Object_t * L_8 = ___value;
		if (!((Object_t *)IsInstSealed(L_8, Single_t61_il2cpp_TypeInfo_var)))
		{
			goto IL_0075;
		}
	}
	{
		return 1;
	}

IL_0075:
	{
		Object_t * L_9 = ___value;
		if (!((Object_t *)IsInstSealed(L_9, Double_t661_il2cpp_TypeInfo_var)))
		{
			goto IL_0082;
		}
	}
	{
		return 1;
	}

IL_0082:
	{
		Object_t * L_10 = ___value;
		if (!((Object_t *)IsInstSealed(L_10, Decimal_t664_il2cpp_TypeInfo_var)))
		{
			goto IL_008f;
		}
	}
	{
		return 1;
	}

IL_008f:
	{
		return 0;
	}
}
// SimpleJson.IJsonSerializerStrategy SimpleJson.SimpleJson::get_CurrentJsonSerializerStrategy()
extern TypeInfo* SimpleJson_t513_il2cpp_TypeInfo_var;
extern "C" Object_t * SimpleJson_get_CurrentJsonSerializerStrategy_m2980 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SimpleJson_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(386);
		s_Il2CppMethodIntialized = true;
	}
	Object_t * G_B2_0 = {0};
	Object_t * G_B1_0 = {0};
	{
		Object_t * L_0 = ((SimpleJson_t513_StaticFields*)SimpleJson_t513_il2cpp_TypeInfo_var->static_fields)->____currentJsonSerializerStrategy_0;
		Object_t * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0017;
		}
	}
	{
		PocoJsonSerializerStrategy_t514 * L_2 = SimpleJson_get_PocoJsonSerializerStrategy_m2981(NULL /*static, unused*/, /*hidden argument*/NULL);
		PocoJsonSerializerStrategy_t514 * L_3 = L_2;
		((SimpleJson_t513_StaticFields*)SimpleJson_t513_il2cpp_TypeInfo_var->static_fields)->____currentJsonSerializerStrategy_0 = L_3;
		G_B2_0 = ((Object_t *)(L_3));
	}

IL_0017:
	{
		return G_B2_0;
	}
}
// SimpleJson.PocoJsonSerializerStrategy SimpleJson.SimpleJson::get_PocoJsonSerializerStrategy()
extern TypeInfo* SimpleJson_t513_il2cpp_TypeInfo_var;
extern TypeInfo* PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var;
extern "C" PocoJsonSerializerStrategy_t514 * SimpleJson_get_PocoJsonSerializerStrategy_m2981 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		SimpleJson_t513_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(386);
		PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(387);
		s_Il2CppMethodIntialized = true;
	}
	PocoJsonSerializerStrategy_t514 * G_B2_0 = {0};
	PocoJsonSerializerStrategy_t514 * G_B1_0 = {0};
	{
		PocoJsonSerializerStrategy_t514 * L_0 = ((SimpleJson_t513_StaticFields*)SimpleJson_t513_il2cpp_TypeInfo_var->static_fields)->____pocoJsonSerializerStrategy_1;
		PocoJsonSerializerStrategy_t514 * L_1 = L_0;
		G_B1_0 = L_1;
		if (L_1)
		{
			G_B2_0 = L_1;
			goto IL_0017;
		}
	}
	{
		PocoJsonSerializerStrategy_t514 * L_2 = (PocoJsonSerializerStrategy_t514 *)il2cpp_codegen_object_new (PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var);
		PocoJsonSerializerStrategy__ctor_m2982(L_2, /*hidden argument*/NULL);
		PocoJsonSerializerStrategy_t514 * L_3 = L_2;
		((SimpleJson_t513_StaticFields*)SimpleJson_t513_il2cpp_TypeInfo_var->static_fields)->____pocoJsonSerializerStrategy_1 = L_3;
		G_B2_0 = L_3;
	}

IL_0017:
	{
		return G_B2_0;
	}
}
// System.Void SimpleJson.PocoJsonSerializerStrategy::.ctor()
extern TypeInfo* ThreadSafeDictionaryValueFactory_2_t668_il2cpp_TypeInfo_var;
extern TypeInfo* ThreadSafeDictionary_2_t669_il2cpp_TypeInfo_var;
extern TypeInfo* ThreadSafeDictionaryValueFactory_2_t670_il2cpp_TypeInfo_var;
extern TypeInfo* ThreadSafeDictionary_2_t671_il2cpp_TypeInfo_var;
extern TypeInfo* ThreadSafeDictionaryValueFactory_2_t672_il2cpp_TypeInfo_var;
extern TypeInfo* ThreadSafeDictionary_2_t673_il2cpp_TypeInfo_var;
extern const MethodInfo* ThreadSafeDictionaryValueFactory_2__ctor_m3343_MethodInfo_var;
extern const MethodInfo* ThreadSafeDictionary_2__ctor_m3344_MethodInfo_var;
extern const MethodInfo* ThreadSafeDictionaryValueFactory_2__ctor_m3345_MethodInfo_var;
extern const MethodInfo* ThreadSafeDictionary_2__ctor_m3346_MethodInfo_var;
extern const MethodInfo* ThreadSafeDictionaryValueFactory_2__ctor_m3347_MethodInfo_var;
extern const MethodInfo* ThreadSafeDictionary_2__ctor_m3348_MethodInfo_var;
extern "C" void PocoJsonSerializerStrategy__ctor_m2982 (PocoJsonSerializerStrategy_t514 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ThreadSafeDictionaryValueFactory_2_t668_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(394);
		ThreadSafeDictionary_2_t669_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(395);
		ThreadSafeDictionaryValueFactory_2_t670_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(396);
		ThreadSafeDictionary_2_t671_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(397);
		ThreadSafeDictionaryValueFactory_2_t672_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(398);
		ThreadSafeDictionary_2_t673_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(399);
		ThreadSafeDictionaryValueFactory_2__ctor_m3343_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483936);
		ThreadSafeDictionary_2__ctor_m3344_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483937);
		ThreadSafeDictionaryValueFactory_2__ctor_m3345_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483938);
		ThreadSafeDictionary_2__ctor_m3346_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483939);
		ThreadSafeDictionaryValueFactory_2__ctor_m3347_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483940);
		ThreadSafeDictionary_2__ctor_m3348_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483941);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		IntPtr_t L_0 = { (void*)GetVirtualMethodInfo(__this, 6) };
		ThreadSafeDictionaryValueFactory_2_t668 * L_1 = (ThreadSafeDictionaryValueFactory_2_t668 *)il2cpp_codegen_object_new (ThreadSafeDictionaryValueFactory_2_t668_il2cpp_TypeInfo_var);
		ThreadSafeDictionaryValueFactory_2__ctor_m3343(L_1, __this, L_0, /*hidden argument*/ThreadSafeDictionaryValueFactory_2__ctor_m3343_MethodInfo_var);
		ThreadSafeDictionary_2_t669 * L_2 = (ThreadSafeDictionary_2_t669 *)il2cpp_codegen_object_new (ThreadSafeDictionary_2_t669_il2cpp_TypeInfo_var);
		ThreadSafeDictionary_2__ctor_m3344(L_2, L_1, /*hidden argument*/ThreadSafeDictionary_2__ctor_m3344_MethodInfo_var);
		__this->___ConstructorCache_0 = L_2;
		IntPtr_t L_3 = { (void*)GetVirtualMethodInfo(__this, 7) };
		ThreadSafeDictionaryValueFactory_2_t670 * L_4 = (ThreadSafeDictionaryValueFactory_2_t670 *)il2cpp_codegen_object_new (ThreadSafeDictionaryValueFactory_2_t670_il2cpp_TypeInfo_var);
		ThreadSafeDictionaryValueFactory_2__ctor_m3345(L_4, __this, L_3, /*hidden argument*/ThreadSafeDictionaryValueFactory_2__ctor_m3345_MethodInfo_var);
		ThreadSafeDictionary_2_t671 * L_5 = (ThreadSafeDictionary_2_t671 *)il2cpp_codegen_object_new (ThreadSafeDictionary_2_t671_il2cpp_TypeInfo_var);
		ThreadSafeDictionary_2__ctor_m3346(L_5, L_4, /*hidden argument*/ThreadSafeDictionary_2__ctor_m3346_MethodInfo_var);
		__this->___GetCache_1 = L_5;
		IntPtr_t L_6 = { (void*)GetVirtualMethodInfo(__this, 8) };
		ThreadSafeDictionaryValueFactory_2_t672 * L_7 = (ThreadSafeDictionaryValueFactory_2_t672 *)il2cpp_codegen_object_new (ThreadSafeDictionaryValueFactory_2_t672_il2cpp_TypeInfo_var);
		ThreadSafeDictionaryValueFactory_2__ctor_m3347(L_7, __this, L_6, /*hidden argument*/ThreadSafeDictionaryValueFactory_2__ctor_m3347_MethodInfo_var);
		ThreadSafeDictionary_2_t673 * L_8 = (ThreadSafeDictionary_2_t673 *)il2cpp_codegen_object_new (ThreadSafeDictionary_2_t673_il2cpp_TypeInfo_var);
		ThreadSafeDictionary_2__ctor_m3348(L_8, L_7, /*hidden argument*/ThreadSafeDictionary_2__ctor_m3348_MethodInfo_var);
		__this->___SetCache_2 = L_8;
		return;
	}
}
// System.Void SimpleJson.PocoJsonSerializerStrategy::.cctor()
extern const Il2CppType* Int32_t56_0_0_0_var;
extern TypeInfo* TypeU5BU5D_t516_il2cpp_TypeInfo_var;
extern TypeInfo* PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t16_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral357;
extern Il2CppCodeGenString* _stringLiteral358;
extern Il2CppCodeGenString* _stringLiteral359;
extern "C" void PocoJsonSerializerStrategy__cctor_m2983 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t56_0_0_0_var = il2cpp_codegen_type_from_index(10);
		TypeU5BU5D_t516_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(400);
		PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(387);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(153);
		StringU5BU5D_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		_stringLiteral357 = il2cpp_codegen_string_literal_from_index(357);
		_stringLiteral358 = il2cpp_codegen_string_literal_from_index(358);
		_stringLiteral359 = il2cpp_codegen_string_literal_from_index(359);
		s_Il2CppMethodIntialized = true;
	}
	{
		((PocoJsonSerializerStrategy_t514_StaticFields*)PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var->static_fields)->___EmptyTypes_3 = ((TypeU5BU5D_t516*)SZArrayNew(TypeU5BU5D_t516_il2cpp_TypeInfo_var, 0));
		TypeU5BU5D_t516* L_0 = ((TypeU5BU5D_t516*)SZArrayNew(TypeU5BU5D_t516_il2cpp_TypeInfo_var, 1));
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1629(NULL /*static, unused*/, LoadTypeToken(Int32_t56_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		ArrayElementTypeCheck (L_0, L_1);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_0, 0, sizeof(Type_t *))) = (Type_t *)L_1;
		((PocoJsonSerializerStrategy_t514_StaticFields*)PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var->static_fields)->___ArrayConstructorParameterTypes_4 = L_0;
		StringU5BU5D_t16* L_2 = ((StringU5BU5D_t16*)SZArrayNew(StringU5BU5D_t16_il2cpp_TypeInfo_var, 3));
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, _stringLiteral357);
		*((String_t**)(String_t**)SZArrayLdElema(L_2, 0, sizeof(String_t*))) = (String_t*)_stringLiteral357;
		StringU5BU5D_t16* L_3 = L_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, 1);
		ArrayElementTypeCheck (L_3, _stringLiteral358);
		*((String_t**)(String_t**)SZArrayLdElema(L_3, 1, sizeof(String_t*))) = (String_t*)_stringLiteral358;
		StringU5BU5D_t16* L_4 = L_3;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 2);
		ArrayElementTypeCheck (L_4, _stringLiteral359);
		*((String_t**)(String_t**)SZArrayLdElema(L_4, 2, sizeof(String_t*))) = (String_t*)_stringLiteral359;
		((PocoJsonSerializerStrategy_t514_StaticFields*)PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var->static_fields)->___Iso8601Format_5 = L_4;
		return;
	}
}
// System.String SimpleJson.PocoJsonSerializerStrategy::MapClrMemberNameToJsonFieldName(System.String)
extern "C" String_t* PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m2984 (PocoJsonSerializerStrategy_t514 * __this, String_t* ___clrPropertyName, const MethodInfo* method)
{
	{
		String_t* L_0 = ___clrPropertyName;
		return L_0;
	}
}
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate SimpleJson.PocoJsonSerializerStrategy::ContructorDelegateFactory(System.Type)
extern TypeInfo* PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var;
extern TypeInfo* ReflectionUtils_t529_il2cpp_TypeInfo_var;
extern "C" ConstructorDelegate_t522 * PocoJsonSerializerStrategy_ContructorDelegateFactory_m2985 (PocoJsonSerializerStrategy_t514 * __this, Type_t * ___key, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(387);
		ReflectionUtils_t529_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(401);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * G_B2_0 = {0};
	Type_t * G_B1_0 = {0};
	TypeU5BU5D_t516* G_B3_0 = {0};
	Type_t * G_B3_1 = {0};
	{
		Type_t * L_0 = ___key;
		Type_t * L_1 = ___key;
		NullCheck(L_1);
		bool L_2 = (bool)VirtFuncInvoker0< bool >::Invoke(21 /* System.Boolean System.Type::get_IsArray() */, L_1);
		G_B1_0 = L_0;
		if (!L_2)
		{
			G_B2_0 = L_0;
			goto IL_0016;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var);
		TypeU5BU5D_t516* L_3 = ((PocoJsonSerializerStrategy_t514_StaticFields*)PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var->static_fields)->___ArrayConstructorParameterTypes_4;
		G_B3_0 = L_3;
		G_B3_1 = G_B1_0;
		goto IL_001b;
	}

IL_0016:
	{
		IL2CPP_RUNTIME_CLASS_INIT(PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var);
		TypeU5BU5D_t516* L_4 = ((PocoJsonSerializerStrategy_t514_StaticFields*)PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var->static_fields)->___EmptyTypes_3;
		G_B3_0 = L_4;
		G_B3_1 = G_B2_0;
	}

IL_001b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
		ConstructorDelegate_t522 * L_5 = ReflectionUtils_GetContructor_m3021(NULL /*static, unused*/, G_B3_1, G_B3_0, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate> SimpleJson.PocoJsonSerializerStrategy::GetterValueFactory(System.Type)
extern TypeInfo* Dictionary_2_t674_il2cpp_TypeInfo_var;
extern TypeInfo* ReflectionUtils_t529_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t628_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t675_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_2_t625_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t51_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t303_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t629_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t676_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3349_MethodInfo_var;
extern "C" Object_t* PocoJsonSerializerStrategy_GetterValueFactory_m2986 (PocoJsonSerializerStrategy_t514 * __this, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t674_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(402);
		ReflectionUtils_t529_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(401);
		IEnumerable_1_t628_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(403);
		IEnumerator_1_t675_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(405);
		IDictionary_2_t625_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(389);
		IEnumerator_t51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		IDisposable_t303_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		IEnumerable_1_t629_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(406);
		IEnumerator_1_t676_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(408);
		Dictionary_2__ctor_m3349_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483942);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	PropertyInfo_t * V_1 = {0};
	Object_t* V_2 = {0};
	MethodInfo_t * V_3 = {0};
	FieldInfo_t * V_4 = {0};
	Object_t* V_5 = {0};
	Exception_t301 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t301 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t674 * L_0 = (Dictionary_2_t674 *)il2cpp_codegen_object_new (Dictionary_2_t674_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3349(L_0, /*hidden argument*/Dictionary_2__ctor_m3349_MethodInfo_var);
		V_0 = L_0;
		Type_t * L_1 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
		Object_t* L_2 = ReflectionUtils_GetProperties_m3017(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Object_t* L_3 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>::GetEnumerator() */, IEnumerable_1_t628_il2cpp_TypeInfo_var, L_2);
		V_2 = L_3;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0063;
		}

IL_0017:
		{
			Object_t* L_4 = V_2;
			NullCheck(L_4);
			PropertyInfo_t * L_5 = (PropertyInfo_t *)InterfaceFuncInvoker0< PropertyInfo_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo>::get_Current() */, IEnumerator_1_t675_il2cpp_TypeInfo_var, L_4);
			V_1 = L_5;
			PropertyInfo_t * L_6 = V_1;
			NullCheck(L_6);
			bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(15 /* System.Boolean System.Reflection.PropertyInfo::get_CanRead() */, L_6);
			if (!L_7)
			{
				goto IL_0063;
			}
		}

IL_0029:
		{
			PropertyInfo_t * L_8 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
			MethodInfo_t * L_9 = ReflectionUtils_GetGetterMethodInfo_m3019(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			V_3 = L_9;
			MethodInfo_t * L_10 = V_3;
			NullCheck(L_10);
			bool L_11 = (bool)VirtFuncInvoker0< bool >::Invoke(22 /* System.Boolean System.Reflection.MethodBase::get_IsStatic() */, L_10);
			if (L_11)
			{
				goto IL_0046;
			}
		}

IL_003b:
		{
			MethodInfo_t * L_12 = V_3;
			NullCheck(L_12);
			bool L_13 = (bool)VirtFuncInvoker0< bool >::Invoke(21 /* System.Boolean System.Reflection.MethodBase::get_IsPublic() */, L_12);
			if (L_13)
			{
				goto IL_004b;
			}
		}

IL_0046:
		{
			goto IL_0063;
		}

IL_004b:
		{
			Object_t* L_14 = V_0;
			PropertyInfo_t * L_15 = V_1;
			NullCheck(L_15);
			String_t* L_16 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_15);
			String_t* L_17 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String SimpleJson.PocoJsonSerializerStrategy::MapClrMemberNameToJsonFieldName(System.String) */, __this, L_16);
			PropertyInfo_t * L_18 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
			GetDelegate_t520 * L_19 = ReflectionUtils_GetGetMethod_m3024(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
			NullCheck(L_14);
			InterfaceActionInvoker2< String_t*, GetDelegate_t520 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::set_Item(!0,!1) */, IDictionary_2_t625_il2cpp_TypeInfo_var, L_14, L_17, L_19);
		}

IL_0063:
		{
			Object_t* L_20 = V_2;
			NullCheck(L_20);
			bool L_21 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t51_il2cpp_TypeInfo_var, L_20);
			if (L_21)
			{
				goto IL_0017;
			}
		}

IL_006e:
		{
			IL2CPP_LEAVE(0x7E, FINALLY_0073);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t301 *)e.ex;
		goto FINALLY_0073;
	}

FINALLY_0073:
	{ // begin finally (depth: 1)
		{
			Object_t* L_22 = V_2;
			if (L_22)
			{
				goto IL_0077;
			}
		}

IL_0076:
		{
			IL2CPP_END_FINALLY(115)
		}

IL_0077:
		{
			Object_t* L_23 = V_2;
			NullCheck(L_23);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t303_il2cpp_TypeInfo_var, L_23);
			IL2CPP_END_FINALLY(115)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(115)
	{
		IL2CPP_JUMP_TBL(0x7E, IL_007e)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t301 *)
	}

IL_007e:
	{
		Type_t * L_24 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
		Object_t* L_25 = ReflectionUtils_GetFields_m3018(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
		NullCheck(L_25);
		Object_t* L_26 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo>::GetEnumerator() */, IEnumerable_1_t629_il2cpp_TypeInfo_var, L_25);
		V_5 = L_26;
	}

IL_008b:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00d0;
		}

IL_0090:
		{
			Object_t* L_27 = V_5;
			NullCheck(L_27);
			FieldInfo_t * L_28 = (FieldInfo_t *)InterfaceFuncInvoker0< FieldInfo_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo>::get_Current() */, IEnumerator_1_t676_il2cpp_TypeInfo_var, L_27);
			V_4 = L_28;
			FieldInfo_t * L_29 = V_4;
			NullCheck(L_29);
			bool L_30 = (bool)VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Reflection.FieldInfo::get_IsStatic() */, L_29);
			if (L_30)
			{
				goto IL_00b1;
			}
		}

IL_00a5:
		{
			FieldInfo_t * L_31 = V_4;
			NullCheck(L_31);
			bool L_32 = (bool)VirtFuncInvoker0< bool >::Invoke(21 /* System.Boolean System.Reflection.FieldInfo::get_IsPublic() */, L_31);
			if (L_32)
			{
				goto IL_00b6;
			}
		}

IL_00b1:
		{
			goto IL_00d0;
		}

IL_00b6:
		{
			Object_t* L_33 = V_0;
			FieldInfo_t * L_34 = V_4;
			NullCheck(L_34);
			String_t* L_35 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_34);
			String_t* L_36 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String SimpleJson.PocoJsonSerializerStrategy::MapClrMemberNameToJsonFieldName(System.String) */, __this, L_35);
			FieldInfo_t * L_37 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
			GetDelegate_t520 * L_38 = ReflectionUtils_GetGetMethod_m3025(NULL /*static, unused*/, L_37, /*hidden argument*/NULL);
			NullCheck(L_33);
			InterfaceActionInvoker2< String_t*, GetDelegate_t520 * >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>::set_Item(!0,!1) */, IDictionary_2_t625_il2cpp_TypeInfo_var, L_33, L_36, L_38);
		}

IL_00d0:
		{
			Object_t* L_39 = V_5;
			NullCheck(L_39);
			bool L_40 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t51_il2cpp_TypeInfo_var, L_39);
			if (L_40)
			{
				goto IL_0090;
			}
		}

IL_00dc:
		{
			IL2CPP_LEAVE(0xEE, FINALLY_00e1);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t301 *)e.ex;
		goto FINALLY_00e1;
	}

FINALLY_00e1:
	{ // begin finally (depth: 1)
		{
			Object_t* L_41 = V_5;
			if (L_41)
			{
				goto IL_00e6;
			}
		}

IL_00e5:
		{
			IL2CPP_END_FINALLY(225)
		}

IL_00e6:
		{
			Object_t* L_42 = V_5;
			NullCheck(L_42);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t303_il2cpp_TypeInfo_var, L_42);
			IL2CPP_END_FINALLY(225)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(225)
	{
		IL2CPP_JUMP_TBL(0xEE, IL_00ee)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t301 *)
	}

IL_00ee:
	{
		Object_t* L_43 = V_0;
		return L_43;
	}
}
// System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>> SimpleJson.PocoJsonSerializerStrategy::SetterValueFactory(System.Type)
extern TypeInfo* Dictionary_2_t677_il2cpp_TypeInfo_var;
extern TypeInfo* ReflectionUtils_t529_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t628_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t675_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_2_t626_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t51_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t303_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t629_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t676_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m3350_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2__ctor_m3351_MethodInfo_var;
extern "C" Object_t* PocoJsonSerializerStrategy_SetterValueFactory_m2987 (PocoJsonSerializerStrategy_t514 * __this, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t677_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(409);
		ReflectionUtils_t529_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(401);
		IEnumerable_1_t628_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(403);
		IEnumerator_1_t675_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(405);
		IDictionary_2_t626_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(391);
		IEnumerator_t51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		IDisposable_t303_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		IEnumerable_1_t629_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(406);
		IEnumerator_1_t676_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(408);
		Dictionary_2__ctor_m3350_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483943);
		KeyValuePair_2__ctor_m3351_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483944);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	PropertyInfo_t * V_1 = {0};
	Object_t* V_2 = {0};
	MethodInfo_t * V_3 = {0};
	FieldInfo_t * V_4 = {0};
	Object_t* V_5 = {0};
	Exception_t301 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t301 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Dictionary_2_t677 * L_0 = (Dictionary_2_t677 *)il2cpp_codegen_object_new (Dictionary_2_t677_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m3350(L_0, /*hidden argument*/Dictionary_2__ctor_m3350_MethodInfo_var);
		V_0 = L_0;
		Type_t * L_1 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
		Object_t* L_2 = ReflectionUtils_GetProperties_m3017(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Object_t* L_3 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo>::GetEnumerator() */, IEnumerable_1_t628_il2cpp_TypeInfo_var, L_2);
		V_2 = L_3;
	}

IL_0012:
	try
	{ // begin try (depth: 1)
		{
			goto IL_006e;
		}

IL_0017:
		{
			Object_t* L_4 = V_2;
			NullCheck(L_4);
			PropertyInfo_t * L_5 = (PropertyInfo_t *)InterfaceFuncInvoker0< PropertyInfo_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.PropertyInfo>::get_Current() */, IEnumerator_1_t675_il2cpp_TypeInfo_var, L_4);
			V_1 = L_5;
			PropertyInfo_t * L_6 = V_1;
			NullCheck(L_6);
			bool L_7 = (bool)VirtFuncInvoker0< bool >::Invoke(16 /* System.Boolean System.Reflection.PropertyInfo::get_CanWrite() */, L_6);
			if (!L_7)
			{
				goto IL_006e;
			}
		}

IL_0029:
		{
			PropertyInfo_t * L_8 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
			MethodInfo_t * L_9 = ReflectionUtils_GetSetterMethodInfo_m3020(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
			V_3 = L_9;
			MethodInfo_t * L_10 = V_3;
			NullCheck(L_10);
			bool L_11 = (bool)VirtFuncInvoker0< bool >::Invoke(22 /* System.Boolean System.Reflection.MethodBase::get_IsStatic() */, L_10);
			if (L_11)
			{
				goto IL_0046;
			}
		}

IL_003b:
		{
			MethodInfo_t * L_12 = V_3;
			NullCheck(L_12);
			bool L_13 = (bool)VirtFuncInvoker0< bool >::Invoke(21 /* System.Boolean System.Reflection.MethodBase::get_IsPublic() */, L_12);
			if (L_13)
			{
				goto IL_004b;
			}
		}

IL_0046:
		{
			goto IL_006e;
		}

IL_004b:
		{
			Object_t* L_14 = V_0;
			PropertyInfo_t * L_15 = V_1;
			NullCheck(L_15);
			String_t* L_16 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_15);
			String_t* L_17 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String SimpleJson.PocoJsonSerializerStrategy::MapClrMemberNameToJsonFieldName(System.String) */, __this, L_16);
			PropertyInfo_t * L_18 = V_1;
			NullCheck(L_18);
			Type_t * L_19 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Reflection.PropertyInfo::get_PropertyType() */, L_18);
			PropertyInfo_t * L_20 = V_1;
			IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
			SetDelegate_t521 * L_21 = ReflectionUtils_GetSetMethod_m3028(NULL /*static, unused*/, L_20, /*hidden argument*/NULL);
			KeyValuePair_2_t678  L_22 = {0};
			KeyValuePair_2__ctor_m3351(&L_22, L_19, L_21, /*hidden argument*/KeyValuePair_2__ctor_m3351_MethodInfo_var);
			NullCheck(L_14);
			InterfaceActionInvoker2< String_t*, KeyValuePair_2_t678  >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::set_Item(!0,!1) */, IDictionary_2_t626_il2cpp_TypeInfo_var, L_14, L_17, L_22);
		}

IL_006e:
		{
			Object_t* L_23 = V_2;
			NullCheck(L_23);
			bool L_24 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t51_il2cpp_TypeInfo_var, L_23);
			if (L_24)
			{
				goto IL_0017;
			}
		}

IL_0079:
		{
			IL2CPP_LEAVE(0x89, FINALLY_007e);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t301 *)e.ex;
		goto FINALLY_007e;
	}

FINALLY_007e:
	{ // begin finally (depth: 1)
		{
			Object_t* L_25 = V_2;
			if (L_25)
			{
				goto IL_0082;
			}
		}

IL_0081:
		{
			IL2CPP_END_FINALLY(126)
		}

IL_0082:
		{
			Object_t* L_26 = V_2;
			NullCheck(L_26);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t303_il2cpp_TypeInfo_var, L_26);
			IL2CPP_END_FINALLY(126)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(126)
	{
		IL2CPP_JUMP_TBL(0x89, IL_0089)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t301 *)
	}

IL_0089:
	{
		Type_t * L_27 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
		Object_t* L_28 = ReflectionUtils_GetFields_m3018(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		NullCheck(L_28);
		Object_t* L_29 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo>::GetEnumerator() */, IEnumerable_1_t629_il2cpp_TypeInfo_var, L_28);
		V_5 = L_29;
	}

IL_0096:
	try
	{ // begin try (depth: 1)
		{
			goto IL_00f3;
		}

IL_009b:
		{
			Object_t* L_30 = V_5;
			NullCheck(L_30);
			FieldInfo_t * L_31 = (FieldInfo_t *)InterfaceFuncInvoker0< FieldInfo_t * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.FieldInfo>::get_Current() */, IEnumerator_1_t676_il2cpp_TypeInfo_var, L_30);
			V_4 = L_31;
			FieldInfo_t * L_32 = V_4;
			NullCheck(L_32);
			bool L_33 = (bool)VirtFuncInvoker0< bool >::Invoke(20 /* System.Boolean System.Reflection.FieldInfo::get_IsInitOnly() */, L_32);
			if (L_33)
			{
				goto IL_00c8;
			}
		}

IL_00b0:
		{
			FieldInfo_t * L_34 = V_4;
			NullCheck(L_34);
			bool L_35 = (bool)VirtFuncInvoker0< bool >::Invoke(19 /* System.Boolean System.Reflection.FieldInfo::get_IsStatic() */, L_34);
			if (L_35)
			{
				goto IL_00c8;
			}
		}

IL_00bc:
		{
			FieldInfo_t * L_36 = V_4;
			NullCheck(L_36);
			bool L_37 = (bool)VirtFuncInvoker0< bool >::Invoke(21 /* System.Boolean System.Reflection.FieldInfo::get_IsPublic() */, L_36);
			if (L_37)
			{
				goto IL_00cd;
			}
		}

IL_00c8:
		{
			goto IL_00f3;
		}

IL_00cd:
		{
			Object_t* L_38 = V_0;
			FieldInfo_t * L_39 = V_4;
			NullCheck(L_39);
			String_t* L_40 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(8 /* System.String System.Reflection.MemberInfo::get_Name() */, L_39);
			String_t* L_41 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String SimpleJson.PocoJsonSerializerStrategy::MapClrMemberNameToJsonFieldName(System.String) */, __this, L_40);
			FieldInfo_t * L_42 = V_4;
			NullCheck(L_42);
			Type_t * L_43 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(16 /* System.Type System.Reflection.FieldInfo::get_FieldType() */, L_42);
			FieldInfo_t * L_44 = V_4;
			IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
			SetDelegate_t521 * L_45 = ReflectionUtils_GetSetMethod_m3029(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
			KeyValuePair_2_t678  L_46 = {0};
			KeyValuePair_2__ctor_m3351(&L_46, L_43, L_45, /*hidden argument*/KeyValuePair_2__ctor_m3351_MethodInfo_var);
			NullCheck(L_38);
			InterfaceActionInvoker2< String_t*, KeyValuePair_2_t678  >::Invoke(3 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::set_Item(!0,!1) */, IDictionary_2_t626_il2cpp_TypeInfo_var, L_38, L_41, L_46);
		}

IL_00f3:
		{
			Object_t* L_47 = V_5;
			NullCheck(L_47);
			bool L_48 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t51_il2cpp_TypeInfo_var, L_47);
			if (L_48)
			{
				goto IL_009b;
			}
		}

IL_00ff:
		{
			IL2CPP_LEAVE(0x111, FINALLY_0104);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t301 *)e.ex;
		goto FINALLY_0104;
	}

FINALLY_0104:
	{ // begin finally (depth: 1)
		{
			Object_t* L_49 = V_5;
			if (L_49)
			{
				goto IL_0109;
			}
		}

IL_0108:
		{
			IL2CPP_END_FINALLY(260)
		}

IL_0109:
		{
			Object_t* L_50 = V_5;
			NullCheck(L_50);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t303_il2cpp_TypeInfo_var, L_50);
			IL2CPP_END_FINALLY(260)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(260)
	{
		IL2CPP_JUMP_TBL(0x111, IL_0111)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t301 *)
	}

IL_0111:
	{
		Object_t* L_51 = V_0;
		return L_51;
	}
}
// System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeNonPrimitiveObject(System.Object,System.Object&)
extern "C" bool PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m2988 (PocoJsonSerializerStrategy_t514 * __this, Object_t * ___input, Object_t ** ___output, const MethodInfo* method)
{
	int32_t G_B3_0 = 0;
	{
		Object_t * L_0 = ___input;
		Object_t ** L_1 = ___output;
		bool L_2 = (bool)VirtFuncInvoker2< bool, Object_t *, Object_t ** >::Invoke(11 /* System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeKnownTypes(System.Object,System.Object&) */, __this, L_0, L_1);
		if (L_2)
		{
			goto IL_0017;
		}
	}
	{
		Object_t * L_3 = ___input;
		Object_t ** L_4 = ___output;
		bool L_5 = (bool)VirtFuncInvoker2< bool, Object_t *, Object_t ** >::Invoke(12 /* System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeUnknownTypes(System.Object,System.Object&) */, __this, L_3, L_4);
		G_B3_0 = ((int32_t)(L_5));
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 1;
	}

IL_0018:
	{
		return G_B3_0;
	}
}
// System.Object SimpleJson.PocoJsonSerializerStrategy::SerializeEnum(System.Enum)
extern TypeInfo* CultureInfo_t659_il2cpp_TypeInfo_var;
extern TypeInfo* Convert_t645_il2cpp_TypeInfo_var;
extern TypeInfo* Double_t661_il2cpp_TypeInfo_var;
extern "C" Object_t * PocoJsonSerializerStrategy_SerializeEnum_m2989 (PocoJsonSerializerStrategy_t514 * __this, Enum_t624 * ___p, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CultureInfo_t659_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(375);
		Convert_t645_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(311);
		Double_t661_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(377);
		s_Il2CppMethodIntialized = true;
	}
	{
		Enum_t624 * L_0 = ___p;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t659_il2cpp_TypeInfo_var);
		CultureInfo_t659 * L_1 = CultureInfo_get_InvariantCulture_m3328(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Convert_t645_il2cpp_TypeInfo_var);
		double L_2 = Convert_ToDouble_m3341(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		double L_3 = L_2;
		Object_t * L_4 = Box(Double_t661_il2cpp_TypeInfo_var, &L_3);
		return L_4;
	}
}
// System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeKnownTypes(System.Object,System.Object&)
extern TypeInfo* DateTime_t396_il2cpp_TypeInfo_var;
extern TypeInfo* PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var;
extern TypeInfo* CultureInfo_t659_il2cpp_TypeInfo_var;
extern TypeInfo* DateTimeOffset_t679_il2cpp_TypeInfo_var;
extern TypeInfo* Guid_t680_il2cpp_TypeInfo_var;
extern TypeInfo* Uri_t509_il2cpp_TypeInfo_var;
extern TypeInfo* Enum_t624_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral360;
extern "C" bool PocoJsonSerializerStrategy_TrySerializeKnownTypes_m2990 (PocoJsonSerializerStrategy_t514 * __this, Object_t * ___input, Object_t ** ___output, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DateTime_t396_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(289);
		PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(387);
		CultureInfo_t659_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(375);
		DateTimeOffset_t679_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(410);
		Guid_t680_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(411);
		Uri_t509_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(356);
		Enum_t624_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(412);
		_stringLiteral360 = il2cpp_codegen_string_literal_from_index(360);
		s_Il2CppMethodIntialized = true;
	}
	bool V_0 = false;
	Enum_t624 * V_1 = {0};
	DateTime_t396  V_2 = {0};
	DateTime_t396  V_3 = {0};
	DateTimeOffset_t679  V_4 = {0};
	DateTimeOffset_t679  V_5 = {0};
	Guid_t680  V_6 = {0};
	{
		V_0 = 1;
		Object_t * L_0 = ___input;
		if (!((Object_t *)IsInstSealed(L_0, DateTime_t396_il2cpp_TypeInfo_var)))
		{
			goto IL_0036;
		}
	}
	{
		Object_t ** L_1 = ___output;
		Object_t * L_2 = ___input;
		V_2 = ((*(DateTime_t396 *)((DateTime_t396 *)UnBox (L_2, DateTime_t396_il2cpp_TypeInfo_var))));
		DateTime_t396  L_3 = DateTime_ToUniversalTime_m3352((&V_2), /*hidden argument*/NULL);
		V_3 = L_3;
		IL2CPP_RUNTIME_CLASS_INIT(PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var);
		StringU5BU5D_t16* L_4 = ((PocoJsonSerializerStrategy_t514_StaticFields*)PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var->static_fields)->___Iso8601Format_5;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, 0);
		int32_t L_5 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t659_il2cpp_TypeInfo_var);
		CultureInfo_t659 * L_6 = CultureInfo_get_InvariantCulture_m3328(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_7 = DateTime_ToString_m3353((&V_3), (*(String_t**)(String_t**)SZArrayLdElema(L_4, L_5, sizeof(String_t*))), L_6, /*hidden argument*/NULL);
		*((Object_t **)(L_1)) = (Object_t *)L_7;
		goto IL_00ca;
	}

IL_0036:
	{
		Object_t * L_8 = ___input;
		if (!((Object_t *)IsInstSealed(L_8, DateTimeOffset_t679_il2cpp_TypeInfo_var)))
		{
			goto IL_006c;
		}
	}
	{
		Object_t ** L_9 = ___output;
		Object_t * L_10 = ___input;
		V_4 = ((*(DateTimeOffset_t679 *)((DateTimeOffset_t679 *)UnBox (L_10, DateTimeOffset_t679_il2cpp_TypeInfo_var))));
		DateTimeOffset_t679  L_11 = DateTimeOffset_ToUniversalTime_m3354((&V_4), /*hidden argument*/NULL);
		V_5 = L_11;
		IL2CPP_RUNTIME_CLASS_INIT(PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var);
		StringU5BU5D_t16* L_12 = ((PocoJsonSerializerStrategy_t514_StaticFields*)PocoJsonSerializerStrategy_t514_il2cpp_TypeInfo_var->static_fields)->___Iso8601Format_5;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		int32_t L_13 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(CultureInfo_t659_il2cpp_TypeInfo_var);
		CultureInfo_t659 * L_14 = CultureInfo_get_InvariantCulture_m3328(NULL /*static, unused*/, /*hidden argument*/NULL);
		String_t* L_15 = DateTimeOffset_ToString_m3355((&V_5), (*(String_t**)(String_t**)SZArrayLdElema(L_12, L_13, sizeof(String_t*))), L_14, /*hidden argument*/NULL);
		*((Object_t **)(L_9)) = (Object_t *)L_15;
		goto IL_00ca;
	}

IL_006c:
	{
		Object_t * L_16 = ___input;
		if (!((Object_t *)IsInstSealed(L_16, Guid_t680_il2cpp_TypeInfo_var)))
		{
			goto IL_0092;
		}
	}
	{
		Object_t ** L_17 = ___output;
		Object_t * L_18 = ___input;
		V_6 = ((*(Guid_t680 *)((Guid_t680 *)UnBox (L_18, Guid_t680_il2cpp_TypeInfo_var))));
		String_t* L_19 = Guid_ToString_m3356((&V_6), _stringLiteral360, /*hidden argument*/NULL);
		*((Object_t **)(L_17)) = (Object_t *)L_19;
		goto IL_00ca;
	}

IL_0092:
	{
		Object_t * L_20 = ___input;
		if (!((Uri_t509 *)IsInstClass(L_20, Uri_t509_il2cpp_TypeInfo_var)))
		{
			goto IL_00aa;
		}
	}
	{
		Object_t ** L_21 = ___output;
		Object_t * L_22 = ___input;
		NullCheck(L_22);
		String_t* L_23 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(3 /* System.String System.Object::ToString() */, L_22);
		*((Object_t **)(L_21)) = (Object_t *)L_23;
		goto IL_00ca;
	}

IL_00aa:
	{
		Object_t * L_24 = ___input;
		V_1 = ((Enum_t624 *)IsInstClass(L_24, Enum_t624_il2cpp_TypeInfo_var));
		Enum_t624 * L_25 = V_1;
		if (!L_25)
		{
			goto IL_00c5;
		}
	}
	{
		Object_t ** L_26 = ___output;
		Enum_t624 * L_27 = V_1;
		Object_t * L_28 = (Object_t *)VirtFuncInvoker1< Object_t *, Enum_t624 * >::Invoke(10 /* System.Object SimpleJson.PocoJsonSerializerStrategy::SerializeEnum(System.Enum) */, __this, L_27);
		*((Object_t **)(L_26)) = (Object_t *)L_28;
		goto IL_00ca;
	}

IL_00c5:
	{
		V_0 = 0;
		Object_t ** L_29 = ___output;
		*((Object_t **)(L_29)) = (Object_t *)NULL;
	}

IL_00ca:
	{
		bool L_30 = V_0;
		return L_30;
	}
}
// System.Boolean SimpleJson.PocoJsonSerializerStrategy::TrySerializeUnknownTypes(System.Object,System.Object&)
extern TypeInfo* ArgumentNullException_t658_il2cpp_TypeInfo_var;
extern TypeInfo* JsonObject_t511_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_2_t518_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t682_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t683_il2cpp_TypeInfo_var;
extern TypeInfo* IDictionary_2_t613_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t51_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t303_il2cpp_TypeInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Value_m3357_MethodInfo_var;
extern const MethodInfo* KeyValuePair_2_get_Key_m3358_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral361;
extern "C" bool PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m2991 (PocoJsonSerializerStrategy_t514 * __this, Object_t * ___input, Object_t ** ___output, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t658_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(369);
		JsonObject_t511_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(373);
		IDictionary_2_t518_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(413);
		IEnumerable_1_t682_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(414);
		IEnumerator_1_t683_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(416);
		IDictionary_2_t613_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(345);
		IEnumerator_t51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		IDisposable_t303_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		KeyValuePair_2_get_Value_m3357_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483945);
		KeyValuePair_2_get_Key_m3358_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483946);
		_stringLiteral361 = il2cpp_codegen_string_literal_from_index(361);
		s_Il2CppMethodIntialized = true;
	}
	Type_t * V_0 = {0};
	Object_t* V_1 = {0};
	Object_t* V_2 = {0};
	KeyValuePair_2_t681  V_3 = {0};
	Object_t* V_4 = {0};
	Exception_t301 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t301 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Object_t * L_0 = ___input;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t658 * L_1 = (ArgumentNullException_t658 *)il2cpp_codegen_object_new (ArgumentNullException_t658_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m3324(L_1, _stringLiteral361, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		Object_t ** L_2 = ___output;
		*((Object_t **)(L_2)) = (Object_t *)NULL;
		Object_t * L_3 = ___input;
		NullCheck(L_3);
		Type_t * L_4 = Object_GetType_m1538(L_3, /*hidden argument*/NULL);
		V_0 = L_4;
		Type_t * L_5 = V_0;
		NullCheck(L_5);
		String_t* L_6 = (String_t*)VirtFuncInvoker0< String_t* >::Invoke(18 /* System.String System.Type::get_FullName() */, L_5);
		if (L_6)
		{
			goto IL_0028;
		}
	}
	{
		return 0;
	}

IL_0028:
	{
		JsonObject_t511 * L_7 = (JsonObject_t511 *)il2cpp_codegen_object_new (JsonObject_t511_il2cpp_TypeInfo_var);
		JsonObject__ctor_m2944(L_7, /*hidden argument*/NULL);
		V_1 = L_7;
		Object_t* L_8 = (__this->___GetCache_1);
		Type_t * L_9 = V_0;
		NullCheck(L_8);
		Object_t* L_10 = (Object_t*)InterfaceFuncInvoker1< Object_t*, Type_t * >::Invoke(2 /* !1 System.Collections.Generic.IDictionary`2<System.Type,System.Collections.Generic.IDictionary`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::get_Item(!0) */, IDictionary_2_t518_il2cpp_TypeInfo_var, L_8, L_9);
		V_2 = L_10;
		Object_t* L_11 = V_2;
		NullCheck(L_11);
		Object_t* L_12 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::GetEnumerator() */, IEnumerable_1_t682_il2cpp_TypeInfo_var, L_11);
		V_4 = L_12;
	}

IL_0043:
	try
	{ // begin try (depth: 1)
		{
			goto IL_007c;
		}

IL_0048:
		{
			Object_t* L_13 = V_4;
			NullCheck(L_13);
			KeyValuePair_2_t681  L_14 = (KeyValuePair_2_t681 )InterfaceFuncInvoker0< KeyValuePair_2_t681  >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJson.Reflection.ReflectionUtils/GetDelegate>>::get_Current() */, IEnumerator_1_t683_il2cpp_TypeInfo_var, L_13);
			V_3 = L_14;
			GetDelegate_t520 * L_15 = KeyValuePair_2_get_Value_m3357((&V_3), /*hidden argument*/KeyValuePair_2_get_Value_m3357_MethodInfo_var);
			if (!L_15)
			{
				goto IL_007c;
			}
		}

IL_005c:
		{
			Object_t* L_16 = V_1;
			String_t* L_17 = KeyValuePair_2_get_Key_m3358((&V_3), /*hidden argument*/KeyValuePair_2_get_Key_m3358_MethodInfo_var);
			String_t* L_18 = (String_t*)VirtFuncInvoker1< String_t*, String_t* >::Invoke(5 /* System.String SimpleJson.PocoJsonSerializerStrategy::MapClrMemberNameToJsonFieldName(System.String) */, __this, L_17);
			GetDelegate_t520 * L_19 = KeyValuePair_2_get_Value_m3357((&V_3), /*hidden argument*/KeyValuePair_2_get_Value_m3357_MethodInfo_var);
			Object_t * L_20 = ___input;
			NullCheck(L_19);
			Object_t * L_21 = GetDelegate_Invoke_m2993(L_19, L_20, /*hidden argument*/NULL);
			NullCheck(L_16);
			InterfaceActionInvoker2< String_t*, Object_t * >::Invoke(0 /* System.Void System.Collections.Generic.IDictionary`2<System.String,System.Object>::Add(!0,!1) */, IDictionary_2_t613_il2cpp_TypeInfo_var, L_16, L_18, L_21);
		}

IL_007c:
		{
			Object_t* L_22 = V_4;
			NullCheck(L_22);
			bool L_23 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t51_il2cpp_TypeInfo_var, L_22);
			if (L_23)
			{
				goto IL_0048;
			}
		}

IL_0088:
		{
			IL2CPP_LEAVE(0x9A, FINALLY_008d);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t301 *)e.ex;
		goto FINALLY_008d;
	}

FINALLY_008d:
	{ // begin finally (depth: 1)
		{
			Object_t* L_24 = V_4;
			if (L_24)
			{
				goto IL_0092;
			}
		}

IL_0091:
		{
			IL2CPP_END_FINALLY(141)
		}

IL_0092:
		{
			Object_t* L_25 = V_4;
			NullCheck(L_25);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t303_il2cpp_TypeInfo_var, L_25);
			IL2CPP_END_FINALLY(141)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(141)
	{
		IL2CPP_JUMP_TBL(0x9A, IL_009a)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t301 *)
	}

IL_009a:
	{
		Object_t ** L_26 = ___output;
		Object_t* L_27 = V_1;
		*((Object_t **)(L_26)) = (Object_t *)L_27;
		return 1;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/GetDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void GetDelegate__ctor_m2992 (GetDelegate_t520 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Object SimpleJson.Reflection.ReflectionUtils/GetDelegate::Invoke(System.Object)
extern "C" Object_t * GetDelegate_Invoke_m2993 (GetDelegate_t520 * __this, Object_t * ___source, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		GetDelegate_Invoke_m2993((GetDelegate_t520 *)__this->___prev_9,___source, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___source, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___source,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, Object_t * ___source, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___source,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___source,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" Object_t * pinvoke_delegate_wrapper_GetDelegate_t520(Il2CppObject* delegate, Object_t * ___source)
{
	// Marshaling of parameter '___source' to native representation
	Object_t * ____source_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/GetDelegate::BeginInvoke(System.Object,System.AsyncCallback,System.Object)
extern "C" Object_t * GetDelegate_BeginInvoke_m2994 (GetDelegate_t520 * __this, Object_t * ___source, AsyncCallback_t181 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___source;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Object SimpleJson.Reflection.ReflectionUtils/GetDelegate::EndInvoke(System.IAsyncResult)
extern "C" Object_t * GetDelegate_EndInvoke_m2995 (GetDelegate_t520 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Object_t *)__result;
}
// System.Void SimpleJson.Reflection.ReflectionUtils/SetDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void SetDelegate__ctor_m2996 (SetDelegate_t521 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Void SimpleJson.Reflection.ReflectionUtils/SetDelegate::Invoke(System.Object,System.Object)
extern "C" void SetDelegate_Invoke_m2997 (SetDelegate_t521 * __this, Object_t * ___source, Object_t * ___value, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		SetDelegate_Invoke_m2997((SetDelegate_t521 *)__this->___prev_9,___source, ___value, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t *, Object_t * __this, Object_t * ___source, Object_t * ___value, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___source, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___source, Object_t * ___value, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___source, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef void (*FunctionPointerType) (Object_t * __this, Object_t * ___value, const MethodInfo* method);
		((FunctionPointerType)__this->___method_ptr_0)(___source, ___value,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" void pinvoke_delegate_wrapper_SetDelegate_t521(Il2CppObject* delegate, Object_t * ___source, Object_t * ___value)
{
	// Marshaling of parameter '___source' to native representation
	Object_t * ____source_marshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));
}
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/SetDelegate::BeginInvoke(System.Object,System.Object,System.AsyncCallback,System.Object)
extern "C" Object_t * SetDelegate_BeginInvoke_m2998 (SetDelegate_t521 * __this, Object_t * ___source, Object_t * ___value, AsyncCallback_t181 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[3] = {0};
	__d_args[0] = ___source;
	__d_args[1] = ___value;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Void SimpleJson.Reflection.ReflectionUtils/SetDelegate::EndInvoke(System.IAsyncResult)
extern "C" void SetDelegate_EndInvoke_m2999 (SetDelegate_t521 * __this, Object_t * ___result, const MethodInfo* method)
{
	il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
}
// System.Void SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::.ctor(System.Object,System.IntPtr)
extern "C" void ConstructorDelegate__ctor_m3000 (ConstructorDelegate_t522 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method)
{
	__this->___method_ptr_0 = (methodPointerType)((MethodInfo*)___method.___m_value_0)->method;
	__this->___method_3 = ___method;
	__this->___m_target_2 = ___object;
}
// System.Object SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::Invoke(System.Object[])
extern "C" Object_t * ConstructorDelegate_Invoke_m3001 (ConstructorDelegate_t522 * __this, ObjectU5BU5D_t60* ___args, const MethodInfo* method)
{
	if(__this->___prev_9 != NULL)
	{
		ConstructorDelegate_Invoke_m3001((ConstructorDelegate_t522 *)__this->___prev_9,___args, method);
	}
	il2cpp_codegen_raise_execution_engine_exception_if_method_is_not_found((MethodInfo*)(__this->___method_3.___m_value_0));
	bool ___methodIsStatic = MethodIsStatic((MethodInfo*)(__this->___method_3.___m_value_0));
	if (__this->___m_target_2 != NULL && ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t *, Object_t * __this, ObjectU5BU5D_t60* ___args, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(NULL,__this->___m_target_2,___args,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else if (__this->___m_target_2 != NULL || ___methodIsStatic)
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, ObjectU5BU5D_t60* ___args, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(__this->___m_target_2,___args,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
	else
	{
		typedef Object_t * (*FunctionPointerType) (Object_t * __this, const MethodInfo* method);
		return ((FunctionPointerType)__this->___method_ptr_0)(___args,(MethodInfo*)(__this->___method_3.___m_value_0));
	}
}
extern "C" Object_t * pinvoke_delegate_wrapper_ConstructorDelegate_t522(Il2CppObject* delegate, ObjectU5BU5D_t60* ___args)
{
	typedef Object_t * (STDCALL *native_function_ptr_type)(Object_t **);
	native_function_ptr_type _il2cpp_pinvoke_func = ((native_function_ptr_type)((Il2CppDelegate*)delegate)->method->method);
	// Marshaling of parameter '___args' to native representation
	Object_t ** ____args_marshaled = { 0 };
	____args_marshaled = il2cpp_codegen_marshal_array<Object_t *>((Il2CppCodeGenArray*)___args);

	// Native function invocation and marshaling of return value back from native representation
	Object_t * _return_value = _il2cpp_pinvoke_func(____args_marshaled);
	Object_t * __return_value_unmarshaled = { 0 };
	il2cpp_codegen_raise_exception((Il2CppCodeGenException*)il2cpp_codegen_get_not_supported_exception("Cannot marshal type 'System.Object'."));

	// Marshaling cleanup of parameter '___args' native representation

	return __return_value_unmarshaled;
}
// System.IAsyncResult SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::BeginInvoke(System.Object[],System.AsyncCallback,System.Object)
extern "C" Object_t * ConstructorDelegate_BeginInvoke_m3002 (ConstructorDelegate_t522 * __this, ObjectU5BU5D_t60* ___args, AsyncCallback_t181 * ___callback, Object_t * ___object, const MethodInfo* method)
{
	void *__d_args[2] = {0};
	__d_args[0] = ___args;
	return (Object_t *)il2cpp_delegate_begin_invoke((Il2CppDelegate*)__this, __d_args, (Il2CppDelegate*)___callback, (Il2CppObject*)___object);
}
// System.Object SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate::EndInvoke(System.IAsyncResult)
extern "C" Object_t * ConstructorDelegate_EndInvoke_m3003 (ConstructorDelegate_t522 * __this, Object_t * ___result, const MethodInfo* method)
{
	Il2CppObject *__result = il2cpp_delegate_end_invoke((Il2CppAsyncResult*) ___result, 0);
	return (Object_t *)__result;
}
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1::.ctor()
extern "C" void U3CGetConstructorByReflectionU3Ec__AnonStorey1__ctor_m3004 (U3CGetConstructorByReflectionU3Ec__AnonStorey1_t523 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SimpleJson.Reflection.ReflectionUtils/<GetConstructorByReflection>c__AnonStorey1::<>m__0(System.Object[])
extern "C" Object_t * U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m3005 (U3CGetConstructorByReflectionU3Ec__AnonStorey1_t523 * __this, ObjectU5BU5D_t60* ___args, const MethodInfo* method)
{
	{
		ConstructorInfo_t524 * L_0 = (__this->___constructorInfo_0);
		ObjectU5BU5D_t60* L_1 = ___args;
		NullCheck(L_0);
		Object_t * L_2 = ConstructorInfo_Invoke_m3359(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2::.ctor()
extern "C" void U3CGetGetMethodByReflectionU3Ec__AnonStorey2__ctor_m3006 (U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t525 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey2::<>m__1(System.Object)
extern TypeInfo* ReflectionUtils_t529_il2cpp_TypeInfo_var;
extern "C" Object_t * U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m3007 (U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t525 * __this, Object_t * ___source, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReflectionUtils_t529_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(401);
		s_Il2CppMethodIntialized = true;
	}
	{
		MethodInfo_t * L_0 = (__this->___methodInfo_0);
		Object_t * L_1 = ___source;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
		ObjectU5BU5D_t60* L_2 = ((ReflectionUtils_t529_StaticFields*)ReflectionUtils_t529_il2cpp_TypeInfo_var->static_fields)->___EmptyObjects_0;
		NullCheck(L_0);
		Object_t * L_3 = (Object_t *)VirtFuncInvoker2< Object_t *, Object_t *, ObjectU5BU5D_t60* >::Invoke(16 /* System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Object[]) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3::.ctor()
extern "C" void U3CGetGetMethodByReflectionU3Ec__AnonStorey3__ctor_m3008 (U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t526 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object SimpleJson.Reflection.ReflectionUtils/<GetGetMethodByReflection>c__AnonStorey3::<>m__2(System.Object)
extern "C" Object_t * U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m3009 (U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t526 * __this, Object_t * ___source, const MethodInfo* method)
{
	{
		FieldInfo_t * L_0 = (__this->___fieldInfo_0);
		Object_t * L_1 = ___source;
		NullCheck(L_0);
		Object_t * L_2 = (Object_t *)VirtFuncInvoker1< Object_t *, Object_t * >::Invoke(17 /* System.Object System.Reflection.FieldInfo::GetValue(System.Object) */, L_0, L_1);
		return L_2;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4::.ctor()
extern "C" void U3CGetSetMethodByReflectionU3Ec__AnonStorey4__ctor_m3010 (U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t527 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey4::<>m__3(System.Object,System.Object)
extern TypeInfo* ObjectU5BU5D_t60_il2cpp_TypeInfo_var;
extern "C" void U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m3011 (U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t527 * __this, Object_t * ___source, Object_t * ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		s_Il2CppMethodIntialized = true;
	}
	{
		MethodInfo_t * L_0 = (__this->___methodInfo_0);
		Object_t * L_1 = ___source;
		ObjectU5BU5D_t60* L_2 = ((ObjectU5BU5D_t60*)SZArrayNew(ObjectU5BU5D_t60_il2cpp_TypeInfo_var, 1));
		Object_t * L_3 = ___value;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, 0);
		ArrayElementTypeCheck (L_2, L_3);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_2, 0, sizeof(Object_t *))) = (Object_t *)L_3;
		NullCheck(L_0);
		VirtFuncInvoker2< Object_t *, Object_t *, ObjectU5BU5D_t60* >::Invoke(16 /* System.Object System.Reflection.MethodBase::Invoke(System.Object,System.Object[]) */, L_0, L_1, L_2);
		return;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5::.ctor()
extern "C" void U3CGetSetMethodByReflectionU3Ec__AnonStorey5__ctor_m3012 (U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t528 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils/<GetSetMethodByReflection>c__AnonStorey5::<>m__4(System.Object,System.Object)
extern "C" void U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m3013 (U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t528 * __this, Object_t * ___source, Object_t * ___value, const MethodInfo* method)
{
	{
		FieldInfo_t * L_0 = (__this->___fieldInfo_0);
		Object_t * L_1 = ___source;
		Object_t * L_2 = ___value;
		NullCheck(L_0);
		VirtActionInvoker2< Object_t *, Object_t * >::Invoke(24 /* System.Void System.Reflection.FieldInfo::SetValue(System.Object,System.Object) */, L_0, L_1, L_2);
		return;
	}
}
// System.Void SimpleJson.Reflection.ReflectionUtils::.cctor()
extern TypeInfo* ObjectU5BU5D_t60_il2cpp_TypeInfo_var;
extern TypeInfo* ReflectionUtils_t529_il2cpp_TypeInfo_var;
extern "C" void ReflectionUtils__cctor_m3014 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ObjectU5BU5D_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		ReflectionUtils_t529_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(401);
		s_Il2CppMethodIntialized = true;
	}
	{
		((ReflectionUtils_t529_StaticFields*)ReflectionUtils_t529_il2cpp_TypeInfo_var->static_fields)->___EmptyObjects_0 = ((ObjectU5BU5D_t60*)SZArrayNew(ObjectU5BU5D_t60_il2cpp_TypeInfo_var, 0));
		return;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Reflection.ConstructorInfo> SimpleJson.Reflection.ReflectionUtils::GetConstructors(System.Type)
extern "C" Object_t* ReflectionUtils_GetConstructors_m3015 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type;
		NullCheck(L_0);
		ConstructorInfoU5BU5D_t684* L_1 = (ConstructorInfoU5BU5D_t684*)VirtFuncInvoker0< ConstructorInfoU5BU5D_t684* >::Invoke(71 /* System.Reflection.ConstructorInfo[] System.Type::GetConstructors() */, L_0);
		return (Object_t*)L_1;
	}
}
// System.Reflection.ConstructorInfo SimpleJson.Reflection.ReflectionUtils::GetConstructorInfo(System.Type,System.Type[])
extern TypeInfo* ReflectionUtils_t529_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerable_1_t627_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_1_t687_il2cpp_TypeInfo_var;
extern TypeInfo* IEnumerator_t51_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t303_il2cpp_TypeInfo_var;
extern "C" ConstructorInfo_t524 * ReflectionUtils_GetConstructorInfo_m3016 (Object_t * __this /* static, unused */, Type_t * ___type, TypeU5BU5D_t516* ___argsType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReflectionUtils_t529_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(401);
		IEnumerable_1_t627_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(417);
		IEnumerator_1_t687_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(419);
		IEnumerator_t51_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(372);
		IDisposable_t303_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(113);
		s_Il2CppMethodIntialized = true;
	}
	Object_t* V_0 = {0};
	int32_t V_1 = 0;
	bool V_2 = false;
	ConstructorInfo_t524 * V_3 = {0};
	Object_t* V_4 = {0};
	ParameterInfoU5BU5D_t685* V_5 = {0};
	ParameterInfo_t686 * V_6 = {0};
	ParameterInfoU5BU5D_t685* V_7 = {0};
	int32_t V_8 = 0;
	ConstructorInfo_t524 * V_9 = {0};
	Exception_t301 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t301 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Type_t * L_0 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
		Object_t* L_1 = ReflectionUtils_GetConstructors_m3015(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		Object_t* L_2 = V_0;
		NullCheck(L_2);
		Object_t* L_3 = (Object_t*)InterfaceFuncInvoker0< Object_t* >::Invoke(0 /* System.Collections.Generic.IEnumerator`1<!0> System.Collections.Generic.IEnumerable`1<System.Reflection.ConstructorInfo>::GetEnumerator() */, IEnumerable_1_t627_il2cpp_TypeInfo_var, L_2);
		V_4 = L_3;
	}

IL_000f:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0085;
		}

IL_0014:
		{
			Object_t* L_4 = V_4;
			NullCheck(L_4);
			ConstructorInfo_t524 * L_5 = (ConstructorInfo_t524 *)InterfaceFuncInvoker0< ConstructorInfo_t524 * >::Invoke(0 /* !0 System.Collections.Generic.IEnumerator`1<System.Reflection.ConstructorInfo>::get_Current() */, IEnumerator_1_t687_il2cpp_TypeInfo_var, L_4);
			V_3 = L_5;
			ConstructorInfo_t524 * L_6 = V_3;
			NullCheck(L_6);
			ParameterInfoU5BU5D_t685* L_7 = (ParameterInfoU5BU5D_t685*)VirtFuncInvoker0< ParameterInfoU5BU5D_t685* >::Invoke(14 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_6);
			V_5 = L_7;
			TypeU5BU5D_t516* L_8 = ___argsType;
			NullCheck(L_8);
			ParameterInfoU5BU5D_t685* L_9 = V_5;
			NullCheck(L_9);
			if ((((int32_t)(((int32_t)(((Array_t *)L_8)->max_length)))) == ((int32_t)(((int32_t)(((Array_t *)L_9)->max_length))))))
			{
				goto IL_0035;
			}
		}

IL_0030:
		{
			goto IL_0085;
		}

IL_0035:
		{
			V_1 = 0;
			V_2 = 1;
			ConstructorInfo_t524 * L_10 = V_3;
			NullCheck(L_10);
			ParameterInfoU5BU5D_t685* L_11 = (ParameterInfoU5BU5D_t685*)VirtFuncInvoker0< ParameterInfoU5BU5D_t685* >::Invoke(14 /* System.Reflection.ParameterInfo[] System.Reflection.MethodBase::GetParameters() */, L_10);
			V_7 = L_11;
			V_8 = 0;
			goto IL_006c;
		}

IL_0049:
		{
			ParameterInfoU5BU5D_t685* L_12 = V_7;
			int32_t L_13 = V_8;
			NullCheck(L_12);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_13);
			int32_t L_14 = L_13;
			V_6 = (*(ParameterInfo_t686 **)(ParameterInfo_t686 **)SZArrayLdElema(L_12, L_14, sizeof(ParameterInfo_t686 *)));
			ParameterInfo_t686 * L_15 = V_6;
			NullCheck(L_15);
			Type_t * L_16 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(6 /* System.Type System.Reflection.ParameterInfo::get_ParameterType() */, L_15);
			TypeU5BU5D_t516* L_17 = ___argsType;
			int32_t L_18 = V_1;
			NullCheck(L_17);
			IL2CPP_ARRAY_BOUNDS_CHECK(L_17, L_18);
			int32_t L_19 = L_18;
			if ((((Object_t*)(Type_t *)L_16) == ((Object_t*)(Type_t *)(*(Type_t **)(Type_t **)SZArrayLdElema(L_17, L_19, sizeof(Type_t *))))))
			{
				goto IL_0066;
			}
		}

IL_005f:
		{
			V_2 = 0;
			goto IL_0077;
		}

IL_0066:
		{
			int32_t L_20 = V_8;
			V_8 = ((int32_t)((int32_t)L_20+(int32_t)1));
		}

IL_006c:
		{
			int32_t L_21 = V_8;
			ParameterInfoU5BU5D_t685* L_22 = V_7;
			NullCheck(L_22);
			if ((((int32_t)L_21) < ((int32_t)(((int32_t)(((Array_t *)L_22)->max_length))))))
			{
				goto IL_0049;
			}
		}

IL_0077:
		{
			bool L_23 = V_2;
			if (!L_23)
			{
				goto IL_0085;
			}
		}

IL_007d:
		{
			ConstructorInfo_t524 * L_24 = V_3;
			V_9 = L_24;
			IL2CPP_LEAVE(0xA5, FINALLY_0096);
		}

IL_0085:
		{
			Object_t* L_25 = V_4;
			NullCheck(L_25);
			bool L_26 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t51_il2cpp_TypeInfo_var, L_25);
			if (L_26)
			{
				goto IL_0014;
			}
		}

IL_0091:
		{
			IL2CPP_LEAVE(0xA3, FINALLY_0096);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t301 *)e.ex;
		goto FINALLY_0096;
	}

FINALLY_0096:
	{ // begin finally (depth: 1)
		{
			Object_t* L_27 = V_4;
			if (L_27)
			{
				goto IL_009b;
			}
		}

IL_009a:
		{
			IL2CPP_END_FINALLY(150)
		}

IL_009b:
		{
			Object_t* L_28 = V_4;
			NullCheck(L_28);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t303_il2cpp_TypeInfo_var, L_28);
			IL2CPP_END_FINALLY(150)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(150)
	{
		IL2CPP_JUMP_TBL(0xA5, IL_00a5)
		IL2CPP_JUMP_TBL(0xA3, IL_00a3)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t301 *)
	}

IL_00a3:
	{
		return (ConstructorInfo_t524 *)NULL;
	}

IL_00a5:
	{
		ConstructorInfo_t524 * L_29 = V_9;
		return L_29;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Reflection.PropertyInfo> SimpleJson.Reflection.ReflectionUtils::GetProperties(System.Type)
extern "C" Object_t* ReflectionUtils_GetProperties_m3017 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type;
		NullCheck(L_0);
		PropertyInfoU5BU5D_t688* L_1 = (PropertyInfoU5BU5D_t688*)VirtFuncInvoker1< PropertyInfoU5BU5D_t688*, int32_t >::Invoke(52 /* System.Reflection.PropertyInfo[] System.Type::GetProperties(System.Reflection.BindingFlags) */, L_0, ((int32_t)60));
		return (Object_t*)L_1;
	}
}
// System.Collections.Generic.IEnumerable`1<System.Reflection.FieldInfo> SimpleJson.Reflection.ReflectionUtils::GetFields(System.Type)
extern "C" Object_t* ReflectionUtils_GetFields_m3018 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	{
		Type_t * L_0 = ___type;
		NullCheck(L_0);
		FieldInfoU5BU5D_t689* L_1 = (FieldInfoU5BU5D_t689*)VirtFuncInvoker1< FieldInfoU5BU5D_t689*, int32_t >::Invoke(45 /* System.Reflection.FieldInfo[] System.Type::GetFields(System.Reflection.BindingFlags) */, L_0, ((int32_t)60));
		return (Object_t*)L_1;
	}
}
// System.Reflection.MethodInfo SimpleJson.Reflection.ReflectionUtils::GetGetterMethodInfo(System.Reflection.PropertyInfo)
extern "C" MethodInfo_t * ReflectionUtils_GetGetterMethodInfo_m3019 (Object_t * __this /* static, unused */, PropertyInfo_t * ___propertyInfo, const MethodInfo* method)
{
	{
		PropertyInfo_t * L_0 = ___propertyInfo;
		NullCheck(L_0);
		MethodInfo_t * L_1 = (MethodInfo_t *)VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(19 /* System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetGetMethod(System.Boolean) */, L_0, 1);
		return L_1;
	}
}
// System.Reflection.MethodInfo SimpleJson.Reflection.ReflectionUtils::GetSetterMethodInfo(System.Reflection.PropertyInfo)
extern "C" MethodInfo_t * ReflectionUtils_GetSetterMethodInfo_m3020 (Object_t * __this /* static, unused */, PropertyInfo_t * ___propertyInfo, const MethodInfo* method)
{
	{
		PropertyInfo_t * L_0 = ___propertyInfo;
		NullCheck(L_0);
		MethodInfo_t * L_1 = (MethodInfo_t *)VirtFuncInvoker1< MethodInfo_t *, bool >::Invoke(21 /* System.Reflection.MethodInfo System.Reflection.PropertyInfo::GetSetMethod(System.Boolean) */, L_0, 1);
		return L_1;
	}
}
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate SimpleJson.Reflection.ReflectionUtils::GetContructor(System.Type,System.Type[])
extern TypeInfo* ReflectionUtils_t529_il2cpp_TypeInfo_var;
extern "C" ConstructorDelegate_t522 * ReflectionUtils_GetContructor_m3021 (Object_t * __this /* static, unused */, Type_t * ___type, TypeU5BU5D_t516* ___argsType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReflectionUtils_t529_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(401);
		s_Il2CppMethodIntialized = true;
	}
	{
		Type_t * L_0 = ___type;
		TypeU5BU5D_t516* L_1 = ___argsType;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
		ConstructorDelegate_t522 * L_2 = ReflectionUtils_GetConstructorByReflection_m3023(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate SimpleJson.Reflection.ReflectionUtils::GetConstructorByReflection(System.Reflection.ConstructorInfo)
extern TypeInfo* U3CGetConstructorByReflectionU3Ec__AnonStorey1_t523_il2cpp_TypeInfo_var;
extern TypeInfo* ConstructorDelegate_t522_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m3005_MethodInfo_var;
extern "C" ConstructorDelegate_t522 * ReflectionUtils_GetConstructorByReflection_m3022 (Object_t * __this /* static, unused */, ConstructorInfo_t524 * ___constructorInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGetConstructorByReflectionU3Ec__AnonStorey1_t523_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(420);
		ConstructorDelegate_t522_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(388);
		U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m3005_MethodInfo_var = il2cpp_codegen_method_info_from_index(299);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetConstructorByReflectionU3Ec__AnonStorey1_t523 * V_0 = {0};
	{
		U3CGetConstructorByReflectionU3Ec__AnonStorey1_t523 * L_0 = (U3CGetConstructorByReflectionU3Ec__AnonStorey1_t523 *)il2cpp_codegen_object_new (U3CGetConstructorByReflectionU3Ec__AnonStorey1_t523_il2cpp_TypeInfo_var);
		U3CGetConstructorByReflectionU3Ec__AnonStorey1__ctor_m3004(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetConstructorByReflectionU3Ec__AnonStorey1_t523 * L_1 = V_0;
		ConstructorInfo_t524 * L_2 = ___constructorInfo;
		NullCheck(L_1);
		L_1->___constructorInfo_0 = L_2;
		U3CGetConstructorByReflectionU3Ec__AnonStorey1_t523 * L_3 = V_0;
		IntPtr_t L_4 = { (void*)U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m3005_MethodInfo_var };
		ConstructorDelegate_t522 * L_5 = (ConstructorDelegate_t522 *)il2cpp_codegen_object_new (ConstructorDelegate_t522_il2cpp_TypeInfo_var);
		ConstructorDelegate__ctor_m3000(L_5, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate SimpleJson.Reflection.ReflectionUtils::GetConstructorByReflection(System.Type,System.Type[])
extern TypeInfo* ReflectionUtils_t529_il2cpp_TypeInfo_var;
extern "C" ConstructorDelegate_t522 * ReflectionUtils_GetConstructorByReflection_m3023 (Object_t * __this /* static, unused */, Type_t * ___type, TypeU5BU5D_t516* ___argsType, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReflectionUtils_t529_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(401);
		s_Il2CppMethodIntialized = true;
	}
	ConstructorInfo_t524 * V_0 = {0};
	ConstructorDelegate_t522 * G_B3_0 = {0};
	{
		Type_t * L_0 = ___type;
		TypeU5BU5D_t516* L_1 = ___argsType;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
		ConstructorInfo_t524 * L_2 = ReflectionUtils_GetConstructorInfo_m3016(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		ConstructorInfo_t524 * L_3 = V_0;
		if (L_3)
		{
			goto IL_0014;
		}
	}
	{
		G_B3_0 = ((ConstructorDelegate_t522 *)(NULL));
		goto IL_001a;
	}

IL_0014:
	{
		ConstructorInfo_t524 * L_4 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
		ConstructorDelegate_t522 * L_5 = ReflectionUtils_GetConstructorByReflection_m3022(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		G_B3_0 = L_5;
	}

IL_001a:
	{
		return G_B3_0;
	}
}
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethod(System.Reflection.PropertyInfo)
extern TypeInfo* ReflectionUtils_t529_il2cpp_TypeInfo_var;
extern "C" GetDelegate_t520 * ReflectionUtils_GetGetMethod_m3024 (Object_t * __this /* static, unused */, PropertyInfo_t * ___propertyInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReflectionUtils_t529_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(401);
		s_Il2CppMethodIntialized = true;
	}
	{
		PropertyInfo_t * L_0 = ___propertyInfo;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
		GetDelegate_t520 * L_1 = ReflectionUtils_GetGetMethodByReflection_m3026(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethod(System.Reflection.FieldInfo)
extern TypeInfo* ReflectionUtils_t529_il2cpp_TypeInfo_var;
extern "C" GetDelegate_t520 * ReflectionUtils_GetGetMethod_m3025 (Object_t * __this /* static, unused */, FieldInfo_t * ___fieldInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReflectionUtils_t529_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(401);
		s_Il2CppMethodIntialized = true;
	}
	{
		FieldInfo_t * L_0 = ___fieldInfo;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
		GetDelegate_t520 * L_1 = ReflectionUtils_GetGetMethodByReflection_m3027(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethodByReflection(System.Reflection.PropertyInfo)
extern TypeInfo* U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t525_il2cpp_TypeInfo_var;
extern TypeInfo* ReflectionUtils_t529_il2cpp_TypeInfo_var;
extern TypeInfo* GetDelegate_t520_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m3007_MethodInfo_var;
extern "C" GetDelegate_t520 * ReflectionUtils_GetGetMethodByReflection_m3026 (Object_t * __this /* static, unused */, PropertyInfo_t * ___propertyInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t525_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(421);
		ReflectionUtils_t529_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(401);
		GetDelegate_t520_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(390);
		U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m3007_MethodInfo_var = il2cpp_codegen_method_info_from_index(300);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t525 * V_0 = {0};
	{
		U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t525 * L_0 = (U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t525 *)il2cpp_codegen_object_new (U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t525_il2cpp_TypeInfo_var);
		U3CGetGetMethodByReflectionU3Ec__AnonStorey2__ctor_m3006(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t525 * L_1 = V_0;
		PropertyInfo_t * L_2 = ___propertyInfo;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
		MethodInfo_t * L_3 = ReflectionUtils_GetGetterMethodInfo_m3019(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->___methodInfo_0 = L_3;
		U3CGetGetMethodByReflectionU3Ec__AnonStorey2_t525 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m3007_MethodInfo_var };
		GetDelegate_t520 * L_6 = (GetDelegate_t520 *)il2cpp_codegen_object_new (GetDelegate_t520_il2cpp_TypeInfo_var);
		GetDelegate__ctor_m2992(L_6, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// SimpleJson.Reflection.ReflectionUtils/GetDelegate SimpleJson.Reflection.ReflectionUtils::GetGetMethodByReflection(System.Reflection.FieldInfo)
extern TypeInfo* U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t526_il2cpp_TypeInfo_var;
extern TypeInfo* GetDelegate_t520_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m3009_MethodInfo_var;
extern "C" GetDelegate_t520 * ReflectionUtils_GetGetMethodByReflection_m3027 (Object_t * __this /* static, unused */, FieldInfo_t * ___fieldInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t526_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(422);
		GetDelegate_t520_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(390);
		U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m3009_MethodInfo_var = il2cpp_codegen_method_info_from_index(301);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t526 * V_0 = {0};
	{
		U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t526 * L_0 = (U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t526 *)il2cpp_codegen_object_new (U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t526_il2cpp_TypeInfo_var);
		U3CGetGetMethodByReflectionU3Ec__AnonStorey3__ctor_m3008(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t526 * L_1 = V_0;
		FieldInfo_t * L_2 = ___fieldInfo;
		NullCheck(L_1);
		L_1->___fieldInfo_0 = L_2;
		U3CGetGetMethodByReflectionU3Ec__AnonStorey3_t526 * L_3 = V_0;
		IntPtr_t L_4 = { (void*)U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m3009_MethodInfo_var };
		GetDelegate_t520 * L_5 = (GetDelegate_t520 *)il2cpp_codegen_object_new (GetDelegate_t520_il2cpp_TypeInfo_var);
		GetDelegate__ctor_m2992(L_5, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethod(System.Reflection.PropertyInfo)
extern TypeInfo* ReflectionUtils_t529_il2cpp_TypeInfo_var;
extern "C" SetDelegate_t521 * ReflectionUtils_GetSetMethod_m3028 (Object_t * __this /* static, unused */, PropertyInfo_t * ___propertyInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReflectionUtils_t529_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(401);
		s_Il2CppMethodIntialized = true;
	}
	{
		PropertyInfo_t * L_0 = ___propertyInfo;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
		SetDelegate_t521 * L_1 = ReflectionUtils_GetSetMethodByReflection_m3030(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethod(System.Reflection.FieldInfo)
extern TypeInfo* ReflectionUtils_t529_il2cpp_TypeInfo_var;
extern "C" SetDelegate_t521 * ReflectionUtils_GetSetMethod_m3029 (Object_t * __this /* static, unused */, FieldInfo_t * ___fieldInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ReflectionUtils_t529_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(401);
		s_Il2CppMethodIntialized = true;
	}
	{
		FieldInfo_t * L_0 = ___fieldInfo;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
		SetDelegate_t521 * L_1 = ReflectionUtils_GetSetMethodByReflection_m3031(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethodByReflection(System.Reflection.PropertyInfo)
extern TypeInfo* U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t527_il2cpp_TypeInfo_var;
extern TypeInfo* ReflectionUtils_t529_il2cpp_TypeInfo_var;
extern TypeInfo* SetDelegate_t521_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m3011_MethodInfo_var;
extern "C" SetDelegate_t521 * ReflectionUtils_GetSetMethodByReflection_m3030 (Object_t * __this /* static, unused */, PropertyInfo_t * ___propertyInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t527_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(423);
		ReflectionUtils_t529_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(401);
		SetDelegate_t521_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(393);
		U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m3011_MethodInfo_var = il2cpp_codegen_method_info_from_index(302);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t527 * V_0 = {0};
	{
		U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t527 * L_0 = (U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t527 *)il2cpp_codegen_object_new (U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t527_il2cpp_TypeInfo_var);
		U3CGetSetMethodByReflectionU3Ec__AnonStorey4__ctor_m3010(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t527 * L_1 = V_0;
		PropertyInfo_t * L_2 = ___propertyInfo;
		IL2CPP_RUNTIME_CLASS_INIT(ReflectionUtils_t529_il2cpp_TypeInfo_var);
		MethodInfo_t * L_3 = ReflectionUtils_GetSetterMethodInfo_m3020(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		NullCheck(L_1);
		L_1->___methodInfo_0 = L_3;
		U3CGetSetMethodByReflectionU3Ec__AnonStorey4_t527 * L_4 = V_0;
		IntPtr_t L_5 = { (void*)U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m3011_MethodInfo_var };
		SetDelegate_t521 * L_6 = (SetDelegate_t521 *)il2cpp_codegen_object_new (SetDelegate_t521_il2cpp_TypeInfo_var);
		SetDelegate__ctor_m2996(L_6, L_4, L_5, /*hidden argument*/NULL);
		return L_6;
	}
}
// SimpleJson.Reflection.ReflectionUtils/SetDelegate SimpleJson.Reflection.ReflectionUtils::GetSetMethodByReflection(System.Reflection.FieldInfo)
extern TypeInfo* U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t528_il2cpp_TypeInfo_var;
extern TypeInfo* SetDelegate_t521_il2cpp_TypeInfo_var;
extern const MethodInfo* U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m3013_MethodInfo_var;
extern "C" SetDelegate_t521 * ReflectionUtils_GetSetMethodByReflection_m3031 (Object_t * __this /* static, unused */, FieldInfo_t * ___fieldInfo, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t528_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(424);
		SetDelegate_t521_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(393);
		U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m3013_MethodInfo_var = il2cpp_codegen_method_info_from_index(303);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t528 * V_0 = {0};
	{
		U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t528 * L_0 = (U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t528 *)il2cpp_codegen_object_new (U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t528_il2cpp_TypeInfo_var);
		U3CGetSetMethodByReflectionU3Ec__AnonStorey5__ctor_m3012(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t528 * L_1 = V_0;
		FieldInfo_t * L_2 = ___fieldInfo;
		NullCheck(L_1);
		L_1->___fieldInfo_0 = L_2;
		U3CGetSetMethodByReflectionU3Ec__AnonStorey5_t528 * L_3 = V_0;
		IntPtr_t L_4 = { (void*)U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m3013_MethodInfo_var };
		SetDelegate_t521 * L_5 = (SetDelegate_t521 *)il2cpp_codegen_object_new (SetDelegate_t521_il2cpp_TypeInfo_var);
		SetDelegate__ctor_m2996(L_5, L_3, L_4, /*hidden argument*/NULL);
		return L_5;
	}
}
// System.Void UnityEngine.WrapperlessIcall::.ctor()
extern "C" void WrapperlessIcall__ctor_m3032 (WrapperlessIcall_t530 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m3265(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.IL2CPPStructAlignmentAttribute::.ctor()
extern "C" void IL2CPPStructAlignmentAttribute__ctor_m3033 (IL2CPPStructAlignmentAttribute_t531 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m3265(__this, /*hidden argument*/NULL);
		__this->___Align_0 = 1;
		return;
	}
}
// System.Void UnityEngine.AttributeHelperEngine::.cctor()
extern TypeInfo* DisallowMultipleComponentU5BU5D_t533_il2cpp_TypeInfo_var;
extern TypeInfo* AttributeHelperEngine_t532_il2cpp_TypeInfo_var;
extern TypeInfo* ExecuteInEditModeU5BU5D_t534_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponentU5BU5D_t535_il2cpp_TypeInfo_var;
extern "C" void AttributeHelperEngine__cctor_m3034 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DisallowMultipleComponentU5BU5D_t533_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(425);
		AttributeHelperEngine_t532_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(427);
		ExecuteInEditModeU5BU5D_t534_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(428);
		RequireComponentU5BU5D_t535_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(430);
		s_Il2CppMethodIntialized = true;
	}
	{
		((AttributeHelperEngine_t532_StaticFields*)AttributeHelperEngine_t532_il2cpp_TypeInfo_var->static_fields)->____disallowMultipleComponentArray_0 = ((DisallowMultipleComponentU5BU5D_t533*)SZArrayNew(DisallowMultipleComponentU5BU5D_t533_il2cpp_TypeInfo_var, 1));
		((AttributeHelperEngine_t532_StaticFields*)AttributeHelperEngine_t532_il2cpp_TypeInfo_var->static_fields)->____executeInEditModeArray_1 = ((ExecuteInEditModeU5BU5D_t534*)SZArrayNew(ExecuteInEditModeU5BU5D_t534_il2cpp_TypeInfo_var, 1));
		((AttributeHelperEngine_t532_StaticFields*)AttributeHelperEngine_t532_il2cpp_TypeInfo_var->static_fields)->____requireComponentArray_2 = ((RequireComponentU5BU5D_t535*)SZArrayNew(RequireComponentU5BU5D_t535_il2cpp_TypeInfo_var, 1));
		return;
	}
}
// System.Type UnityEngine.AttributeHelperEngine::GetParentTypeDisallowingMultipleInclusion(System.Type)
extern const Il2CppType* MonoBehaviour_t2_0_0_0_var;
extern const Il2CppType* DisallowMultipleComponent_t536_0_0_0_var;
extern TypeInfo* Stack_1_t690_il2cpp_TypeInfo_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern const MethodInfo* Stack_1__ctor_m3360_MethodInfo_var;
extern const MethodInfo* Stack_1_Push_m3361_MethodInfo_var;
extern const MethodInfo* Stack_1_Pop_m3362_MethodInfo_var;
extern "C" Type_t * AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m3035 (Object_t * __this /* static, unused */, Type_t * ___type, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MonoBehaviour_t2_0_0_0_var = il2cpp_codegen_type_from_index(432);
		DisallowMultipleComponent_t536_0_0_0_var = il2cpp_codegen_type_from_index(426);
		Stack_1_t690_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(433);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(153);
		Stack_1__ctor_m3360_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483952);
		Stack_1_Push_m3361_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483953);
		Stack_1_Pop_m3362_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483954);
		s_Il2CppMethodIntialized = true;
	}
	Stack_1_t690 * V_0 = {0};
	Type_t * V_1 = {0};
	ObjectU5BU5D_t60* V_2 = {0};
	{
		Stack_1_t690 * L_0 = (Stack_1_t690 *)il2cpp_codegen_object_new (Stack_1_t690_il2cpp_TypeInfo_var);
		Stack_1__ctor_m3360(L_0, /*hidden argument*/Stack_1__ctor_m3360_MethodInfo_var);
		V_0 = L_0;
		goto IL_001a;
	}

IL_000b:
	{
		Stack_1_t690 * L_1 = V_0;
		Type_t * L_2 = ___type;
		NullCheck(L_1);
		Stack_1_Push_m3361(L_1, L_2, /*hidden argument*/Stack_1_Push_m3361_MethodInfo_var);
		Type_t * L_3 = ___type;
		NullCheck(L_3);
		Type_t * L_4 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_3);
		___type = L_4;
	}

IL_001a:
	{
		Type_t * L_5 = ___type;
		if (!L_5)
		{
			goto IL_0030;
		}
	}
	{
		Type_t * L_6 = ___type;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_7 = Type_GetTypeFromHandle_m1629(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t2_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_6) == ((Object_t*)(Type_t *)L_7))))
		{
			goto IL_000b;
		}
	}

IL_0030:
	{
		V_1 = (Type_t *)NULL;
		goto IL_005a;
	}

IL_0037:
	{
		Stack_1_t690 * L_8 = V_0;
		NullCheck(L_8);
		Type_t * L_9 = Stack_1_Pop_m3362(L_8, /*hidden argument*/Stack_1_Pop_m3362_MethodInfo_var);
		V_1 = L_9;
		Type_t * L_10 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_11 = Type_GetTypeFromHandle_m1629(NULL /*static, unused*/, LoadTypeToken(DisallowMultipleComponent_t536_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_10);
		ObjectU5BU5D_t60* L_12 = (ObjectU5BU5D_t60*)VirtFuncInvoker2< ObjectU5BU5D_t60*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_10, L_11, 0);
		V_2 = L_12;
		ObjectU5BU5D_t60* L_13 = V_2;
		NullCheck(L_13);
		if (!(((int32_t)(((Array_t *)L_13)->max_length))))
		{
			goto IL_005a;
		}
	}
	{
		Type_t * L_14 = V_1;
		return L_14;
	}

IL_005a:
	{
		Stack_1_t690 * L_15 = V_0;
		NullCheck(L_15);
		int32_t L_16 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 System.Collections.Generic.Stack`1<System.Type>::get_Count() */, L_15);
		if ((((int32_t)L_16) > ((int32_t)0)))
		{
			goto IL_0037;
		}
	}
	{
		return (Type_t *)NULL;
	}
}
// System.Type[] UnityEngine.AttributeHelperEngine::GetRequiredComponents(System.Type)
extern const Il2CppType* RequireComponent_t537_0_0_0_var;
extern const Il2CppType* MonoBehaviour_t2_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* RequireComponent_t537_il2cpp_TypeInfo_var;
extern TypeInfo* TypeU5BU5D_t516_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t691_il2cpp_TypeInfo_var;
extern const MethodInfo* List_1__ctor_m3363_MethodInfo_var;
extern const MethodInfo* List_1_ToArray_m3364_MethodInfo_var;
extern "C" TypeU5BU5D_t516* AttributeHelperEngine_GetRequiredComponents_m3036 (Object_t * __this /* static, unused */, Type_t * ___klass, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		RequireComponent_t537_0_0_0_var = il2cpp_codegen_type_from_index(431);
		MonoBehaviour_t2_0_0_0_var = il2cpp_codegen_type_from_index(432);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(153);
		RequireComponent_t537_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(431);
		TypeU5BU5D_t516_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(400);
		List_1_t691_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(434);
		List_1__ctor_m3363_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483955);
		List_1_ToArray_m3364_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483956);
		s_Il2CppMethodIntialized = true;
	}
	List_1_t691 * V_0 = {0};
	ObjectU5BU5D_t60* V_1 = {0};
	int32_t V_2 = 0;
	RequireComponent_t537 * V_3 = {0};
	TypeU5BU5D_t516* V_4 = {0};
	{
		V_0 = (List_1_t691 *)NULL;
		goto IL_00d9;
	}

IL_0007:
	{
		Type_t * L_0 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1629(NULL /*static, unused*/, LoadTypeToken(RequireComponent_t537_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t60* L_2 = (ObjectU5BU5D_t60*)VirtFuncInvoker2< ObjectU5BU5D_t60*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, 0);
		V_1 = L_2;
		V_2 = 0;
		goto IL_00c8;
	}

IL_0020:
	{
		ObjectU5BU5D_t60* L_3 = V_1;
		int32_t L_4 = V_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		V_3 = ((RequireComponent_t537 *)CastclassSealed((*(Object_t **)(Object_t **)SZArrayLdElema(L_3, L_5, sizeof(Object_t *))), RequireComponent_t537_il2cpp_TypeInfo_var));
		List_1_t691 * L_6 = V_0;
		if (L_6)
		{
			goto IL_0073;
		}
	}
	{
		ObjectU5BU5D_t60* L_7 = V_1;
		NullCheck(L_7);
		if ((!(((uint32_t)(((int32_t)(((Array_t *)L_7)->max_length)))) == ((uint32_t)1))))
		{
			goto IL_0073;
		}
	}
	{
		Type_t * L_8 = ___klass;
		NullCheck(L_8);
		Type_t * L_9 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_8);
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_10 = Type_GetTypeFromHandle_m1629(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t2_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_9) == ((Object_t*)(Type_t *)L_10))))
		{
			goto IL_0073;
		}
	}
	{
		TypeU5BU5D_t516* L_11 = ((TypeU5BU5D_t516*)SZArrayNew(TypeU5BU5D_t516_il2cpp_TypeInfo_var, 3));
		RequireComponent_t537 * L_12 = V_3;
		NullCheck(L_12);
		Type_t * L_13 = (L_12->___m_Type0_0);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		ArrayElementTypeCheck (L_11, L_13);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_11, 0, sizeof(Type_t *))) = (Type_t *)L_13;
		TypeU5BU5D_t516* L_14 = L_11;
		RequireComponent_t537 * L_15 = V_3;
		NullCheck(L_15);
		Type_t * L_16 = (L_15->___m_Type1_1);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 1);
		ArrayElementTypeCheck (L_14, L_16);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_14, 1, sizeof(Type_t *))) = (Type_t *)L_16;
		TypeU5BU5D_t516* L_17 = L_14;
		RequireComponent_t537 * L_18 = V_3;
		NullCheck(L_18);
		Type_t * L_19 = (L_18->___m_Type2_2);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 2);
		ArrayElementTypeCheck (L_17, L_19);
		*((Type_t **)(Type_t **)SZArrayLdElema(L_17, 2, sizeof(Type_t *))) = (Type_t *)L_19;
		V_4 = L_17;
		TypeU5BU5D_t516* L_20 = V_4;
		return L_20;
	}

IL_0073:
	{
		List_1_t691 * L_21 = V_0;
		if (L_21)
		{
			goto IL_007f;
		}
	}
	{
		List_1_t691 * L_22 = (List_1_t691 *)il2cpp_codegen_object_new (List_1_t691_il2cpp_TypeInfo_var);
		List_1__ctor_m3363(L_22, /*hidden argument*/List_1__ctor_m3363_MethodInfo_var);
		V_0 = L_22;
	}

IL_007f:
	{
		RequireComponent_t537 * L_23 = V_3;
		NullCheck(L_23);
		Type_t * L_24 = (L_23->___m_Type0_0);
		if (!L_24)
		{
			goto IL_0096;
		}
	}
	{
		List_1_t691 * L_25 = V_0;
		RequireComponent_t537 * L_26 = V_3;
		NullCheck(L_26);
		Type_t * L_27 = (L_26->___m_Type0_0);
		NullCheck(L_25);
		VirtActionInvoker1< Type_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Type>::Add(!0) */, L_25, L_27);
	}

IL_0096:
	{
		RequireComponent_t537 * L_28 = V_3;
		NullCheck(L_28);
		Type_t * L_29 = (L_28->___m_Type1_1);
		if (!L_29)
		{
			goto IL_00ad;
		}
	}
	{
		List_1_t691 * L_30 = V_0;
		RequireComponent_t537 * L_31 = V_3;
		NullCheck(L_31);
		Type_t * L_32 = (L_31->___m_Type1_1);
		NullCheck(L_30);
		VirtActionInvoker1< Type_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Type>::Add(!0) */, L_30, L_32);
	}

IL_00ad:
	{
		RequireComponent_t537 * L_33 = V_3;
		NullCheck(L_33);
		Type_t * L_34 = (L_33->___m_Type2_2);
		if (!L_34)
		{
			goto IL_00c4;
		}
	}
	{
		List_1_t691 * L_35 = V_0;
		RequireComponent_t537 * L_36 = V_3;
		NullCheck(L_36);
		Type_t * L_37 = (L_36->___m_Type2_2);
		NullCheck(L_35);
		VirtActionInvoker1< Type_t * >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.Type>::Add(!0) */, L_35, L_37);
	}

IL_00c4:
	{
		int32_t L_38 = V_2;
		V_2 = ((int32_t)((int32_t)L_38+(int32_t)1));
	}

IL_00c8:
	{
		int32_t L_39 = V_2;
		ObjectU5BU5D_t60* L_40 = V_1;
		NullCheck(L_40);
		if ((((int32_t)L_39) < ((int32_t)(((int32_t)(((Array_t *)L_40)->max_length))))))
		{
			goto IL_0020;
		}
	}
	{
		Type_t * L_41 = ___klass;
		NullCheck(L_41);
		Type_t * L_42 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_41);
		___klass = L_42;
	}

IL_00d9:
	{
		Type_t * L_43 = ___klass;
		if (!L_43)
		{
			goto IL_00ef;
		}
	}
	{
		Type_t * L_44 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_45 = Type_GetTypeFromHandle_m1629(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t2_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_44) == ((Object_t*)(Type_t *)L_45))))
		{
			goto IL_0007;
		}
	}

IL_00ef:
	{
		List_1_t691 * L_46 = V_0;
		if (L_46)
		{
			goto IL_00f7;
		}
	}
	{
		return (TypeU5BU5D_t516*)NULL;
	}

IL_00f7:
	{
		List_1_t691 * L_47 = V_0;
		NullCheck(L_47);
		TypeU5BU5D_t516* L_48 = List_1_ToArray_m3364(L_47, /*hidden argument*/List_1_ToArray_m3364_MethodInfo_var);
		return L_48;
	}
}
// System.Boolean UnityEngine.AttributeHelperEngine::CheckIsEditorScript(System.Type)
extern const Il2CppType* ExecuteInEditMode_t539_0_0_0_var;
extern const Il2CppType* MonoBehaviour_t2_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern "C" bool AttributeHelperEngine_CheckIsEditorScript_m3037 (Object_t * __this /* static, unused */, Type_t * ___klass, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ExecuteInEditMode_t539_0_0_0_var = il2cpp_codegen_type_from_index(429);
		MonoBehaviour_t2_0_0_0_var = il2cpp_codegen_type_from_index(432);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(153);
		s_Il2CppMethodIntialized = true;
	}
	ObjectU5BU5D_t60* V_0 = {0};
	{
		goto IL_0029;
	}

IL_0005:
	{
		Type_t * L_0 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_1 = Type_GetTypeFromHandle_m1629(NULL /*static, unused*/, LoadTypeToken(ExecuteInEditMode_t539_0_0_0_var), /*hidden argument*/NULL);
		NullCheck(L_0);
		ObjectU5BU5D_t60* L_2 = (ObjectU5BU5D_t60*)VirtFuncInvoker2< ObjectU5BU5D_t60*, Type_t *, bool >::Invoke(13 /* System.Object[] System.Reflection.MemberInfo::GetCustomAttributes(System.Type,System.Boolean) */, L_0, L_1, 0);
		V_0 = L_2;
		ObjectU5BU5D_t60* L_3 = V_0;
		NullCheck(L_3);
		if (!(((int32_t)(((Array_t *)L_3)->max_length))))
		{
			goto IL_0021;
		}
	}
	{
		return 1;
	}

IL_0021:
	{
		Type_t * L_4 = ___klass;
		NullCheck(L_4);
		Type_t * L_5 = (Type_t *)VirtFuncInvoker0< Type_t * >::Invoke(17 /* System.Type System.Type::get_BaseType() */, L_4);
		___klass = L_5;
	}

IL_0029:
	{
		Type_t * L_6 = ___klass;
		if (!L_6)
		{
			goto IL_003f;
		}
	}
	{
		Type_t * L_7 = ___klass;
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_8 = Type_GetTypeFromHandle_m1629(NULL /*static, unused*/, LoadTypeToken(MonoBehaviour_t2_0_0_0_var), /*hidden argument*/NULL);
		if ((!(((Object_t*)(Type_t *)L_7) == ((Object_t*)(Type_t *)L_8))))
		{
			goto IL_0005;
		}
	}

IL_003f:
	{
		return 0;
	}
}
// System.Void UnityEngine.DisallowMultipleComponent::.ctor()
extern "C" void DisallowMultipleComponent__ctor_m3038 (DisallowMultipleComponent_t536 * __this, const MethodInfo* method)
{
	{
		Attribute__ctor_m3265(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
extern "C" void RequireComponent__ctor_m3039 (RequireComponent_t537 * __this, Type_t * ___requiredComponent, const MethodInfo* method)
{
	{
		Attribute__ctor_m3265(__this, /*hidden argument*/NULL);
		Type_t * L_0 = ___requiredComponent;
		__this->___m_Type0_0 = L_0;
		return;
	}
}
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String)
extern "C" void AddComponentMenu__ctor_m3040 (AddComponentMenu_t538 * __this, String_t* ___menuName, const MethodInfo* method)
{
	{
		Attribute__ctor_m3265(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName;
		__this->___m_AddComponentMenu_0 = L_0;
		__this->___m_Ordering_1 = 0;
		return;
	}
}
// System.Void UnityEngine.AddComponentMenu::.ctor(System.String,System.Int32)
extern "C" void AddComponentMenu__ctor_m3041 (AddComponentMenu_t538 * __this, String_t* ___menuName, int32_t ___order, const MethodInfo* method)
{
	{
		Attribute__ctor_m3265(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___menuName;
		__this->___m_AddComponentMenu_0 = L_0;
		int32_t L_1 = ___order;
		__this->___m_Ordering_1 = L_1;
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
