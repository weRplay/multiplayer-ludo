﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t5;
// UnityEngine.Camera
struct Camera_t122;

#include "mscorlib_System_ValueType.h"

// UnityEngine.SendMouseEvents/HitInfo
struct  HitInfo_t561 
{
	// UnityEngine.GameObject UnityEngine.SendMouseEvents/HitInfo::target
	GameObject_t5 * ___target_0;
	// UnityEngine.Camera UnityEngine.SendMouseEvents/HitInfo::camera
	Camera_t122 * ___camera_1;
};
