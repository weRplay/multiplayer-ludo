﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m12128_gshared (KeyValuePair_2_t1908 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m12128(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1908 *, Object_t *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m12128_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m12129_gshared (KeyValuePair_2_t1908 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m12129(__this, method) (( Object_t * (*) (KeyValuePair_2_t1908 *, const MethodInfo*))KeyValuePair_2_get_Key_m12129_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m12130_gshared (KeyValuePair_2_t1908 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m12130(__this, ___value, method) (( void (*) (KeyValuePair_2_t1908 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m12130_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::get_Value()
extern "C" int32_t KeyValuePair_2_get_Value_m12131_gshared (KeyValuePair_2_t1908 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m12131(__this, method) (( int32_t (*) (KeyValuePair_2_t1908 *, const MethodInfo*))KeyValuePair_2_get_Value_m12131_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m12132_gshared (KeyValuePair_2_t1908 * __this, int32_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m12132(__this, ___value, method) (( void (*) (KeyValuePair_2_t1908 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m12132_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m12133_gshared (KeyValuePair_2_t1908 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m12133(__this, method) (( String_t* (*) (KeyValuePair_2_t1908 *, const MethodInfo*))KeyValuePair_2_ToString_m12133_gshared)(__this, method)
