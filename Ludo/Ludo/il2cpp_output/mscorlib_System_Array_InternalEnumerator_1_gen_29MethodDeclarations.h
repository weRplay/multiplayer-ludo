﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUILayoutOption>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m13983(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2049 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m10443_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.GUILayoutOption>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13984(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2049 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m10445_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.GUILayoutOption>::Dispose()
#define InternalEnumerator_1_Dispose_m13985(__this, method) (( void (*) (InternalEnumerator_1_t2049 *, const MethodInfo*))InternalEnumerator_1_Dispose_m10447_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.GUILayoutOption>::MoveNext()
#define InternalEnumerator_1_MoveNext_m13986(__this, method) (( bool (*) (InternalEnumerator_1_t2049 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m10449_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.GUILayoutOption>::get_Current()
#define InternalEnumerator_1_get_Current_m13987(__this, method) (( GUILayoutOption_t407 * (*) (InternalEnumerator_1_t2049 *, const MethodInfo*))InternalEnumerator_1_get_Current_m10451_gshared)(__this, method)
