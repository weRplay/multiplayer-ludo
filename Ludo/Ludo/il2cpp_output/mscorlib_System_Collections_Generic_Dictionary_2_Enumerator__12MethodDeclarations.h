﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t2116;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__12.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15200_gshared (Enumerator_t2123 * __this, Dictionary_2_t2116 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m15200(__this, ___dictionary, method) (( void (*) (Enumerator_t2123 *, Dictionary_2_t2116 *, const MethodInfo*))Enumerator__ctor_m15200_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15201_gshared (Enumerator_t2123 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15201(__this, method) (( Object_t * (*) (Enumerator_t2123 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15201_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1068  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15202_gshared (Enumerator_t2123 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15202(__this, method) (( DictionaryEntry_t1068  (*) (Enumerator_t2123 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15202_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15203_gshared (Enumerator_t2123 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15203(__this, method) (( Object_t * (*) (Enumerator_t2123 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15203_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15204_gshared (Enumerator_t2123 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15204(__this, method) (( Object_t * (*) (Enumerator_t2123 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15205_gshared (Enumerator_t2123 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15205(__this, method) (( bool (*) (Enumerator_t2123 *, const MethodInfo*))Enumerator_MoveNext_m15205_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_Current()
extern "C" KeyValuePair_2_t2118  Enumerator_get_Current_m15206_gshared (Enumerator_t2123 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15206(__this, method) (( KeyValuePair_2_t2118  (*) (Enumerator_t2123 *, const MethodInfo*))Enumerator_get_Current_m15206_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m15207_gshared (Enumerator_t2123 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m15207(__this, method) (( Object_t * (*) (Enumerator_t2123 *, const MethodInfo*))Enumerator_get_CurrentKey_m15207_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::get_CurrentValue()
extern "C" int64_t Enumerator_get_CurrentValue_m15208_gshared (Enumerator_t2123 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m15208(__this, method) (( int64_t (*) (Enumerator_t2123 *, const MethodInfo*))Enumerator_get_CurrentValue_m15208_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::VerifyState()
extern "C" void Enumerator_VerifyState_m15209_gshared (Enumerator_t2123 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15209(__this, method) (( void (*) (Enumerator_t2123 *, const MethodInfo*))Enumerator_VerifyState_m15209_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m15210_gshared (Enumerator_t2123 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m15210(__this, method) (( void (*) (Enumerator_t2123 *, const MethodInfo*))Enumerator_VerifyCurrent_m15210_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Int64>::Dispose()
extern "C" void Enumerator_Dispose_m15211_gshared (Enumerator_t2123 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15211(__this, method) (( void (*) (Enumerator_t2123 *, const MethodInfo*))Enumerator_Dispose_m15211_gshared)(__this, method)
