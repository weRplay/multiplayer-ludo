﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>
struct Dictionary_2_t2153;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_21.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15664_gshared (Enumerator_t2163 * __this, Dictionary_2_t2153 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m15664(__this, ___host, method) (( void (*) (Enumerator_t2163 *, Dictionary_2_t2153 *, const MethodInfo*))Enumerator__ctor_m15664_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15665_gshared (Enumerator_t2163 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15665(__this, method) (( Object_t * (*) (Enumerator_t2163 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15665_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m15666_gshared (Enumerator_t2163 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15666(__this, method) (( void (*) (Enumerator_t2163 *, const MethodInfo*))Enumerator_Dispose_m15666_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15667_gshared (Enumerator_t2163 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15667(__this, method) (( bool (*) (Enumerator_t2163 *, const MethodInfo*))Enumerator_MoveNext_m15667_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m15668_gshared (Enumerator_t2163 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15668(__this, method) (( Object_t * (*) (Enumerator_t2163 *, const MethodInfo*))Enumerator_get_Current_m15668_gshared)(__this, method)
