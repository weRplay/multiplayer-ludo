﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct ShimEnumerator_t2210;
// System.Collections.Generic.Dictionary`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>
struct Dictionary_2_t2199;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m16392_gshared (ShimEnumerator_t2210 * __this, Dictionary_2_t2199 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m16392(__this, ___host, method) (( void (*) (ShimEnumerator_t2210 *, Dictionary_2_t2199 *, const MethodInfo*))ShimEnumerator__ctor_m16392_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m16393_gshared (ShimEnumerator_t2210 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m16393(__this, method) (( bool (*) (ShimEnumerator_t2210 *, const MethodInfo*))ShimEnumerator_MoveNext_m16393_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Entry()
extern "C" DictionaryEntry_t1068  ShimEnumerator_get_Entry_m16394_gshared (ShimEnumerator_t2210 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m16394(__this, method) (( DictionaryEntry_t1068  (*) (ShimEnumerator_t2210 *, const MethodInfo*))ShimEnumerator_get_Entry_m16394_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m16395_gshared (ShimEnumerator_t2210 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m16395(__this, method) (( Object_t * (*) (ShimEnumerator_t2210 *, const MethodInfo*))ShimEnumerator_get_Key_m16395_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m16396_gshared (ShimEnumerator_t2210 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m16396(__this, method) (( Object_t * (*) (ShimEnumerator_t2210 *, const MethodInfo*))ShimEnumerator_get_Value_m16396_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m16397_gshared (ShimEnumerator_t2210 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m16397(__this, method) (( Object_t * (*) (ShimEnumerator_t2210 *, const MethodInfo*))ShimEnumerator_get_Current_m16397_gshared)(__this, method)
