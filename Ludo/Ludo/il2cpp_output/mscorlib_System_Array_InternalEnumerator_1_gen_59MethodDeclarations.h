﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_59.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_25.h"

// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17210_gshared (InternalEnumerator_1_t2290 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17210(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2290 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17210_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17211_gshared (InternalEnumerator_1_t2290 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17211(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2290 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17211_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17212_gshared (InternalEnumerator_1_t2290 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17212(__this, method) (( void (*) (InternalEnumerator_1_t2290 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17212_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17213_gshared (InternalEnumerator_1_t2290 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17213(__this, method) (( bool (*) (InternalEnumerator_1_t2290 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17213_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Boolean>>::get_Current()
extern "C" KeyValuePair_2_t2289  InternalEnumerator_1_get_Current_m17214_gshared (InternalEnumerator_1_t2290 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17214(__this, method) (( KeyValuePair_2_t2289  (*) (InternalEnumerator_1_t2290 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17214_gshared)(__this, method)
