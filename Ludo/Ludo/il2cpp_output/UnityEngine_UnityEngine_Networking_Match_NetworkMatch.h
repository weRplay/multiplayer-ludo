﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.String
struct String_t;
// System.Uri
struct Uri_t509;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// UnityEngine.Networking.Match.NetworkMatch
struct  NetworkMatch_t508  : public MonoBehaviour_t2
{
	// System.Uri UnityEngine.Networking.Match.NetworkMatch::m_BaseUri
	Uri_t509 * ___m_BaseUri_3;
};
