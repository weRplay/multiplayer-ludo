﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t2116;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1907;
// System.Collections.Generic.IDictionary`2<System.Object,System.Int64>
struct IDictionary_2_t2448;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t631;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t621;
// System.Collections.Generic.ICollection`1<System.Int64>
struct ICollection_1_t2449;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>[]
struct KeyValuePair_2U5BU5D_t2447;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t51;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int64>>
struct IEnumerator_1_t2450;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t1067;
// System.Collections.Generic.Dictionary`2/KeyCollection<System.Object,System.Int64>
struct KeyCollection_t2121;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int64>
struct ValueCollection_t2125;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__12.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor()
extern "C" void Dictionary_2__ctor_m15066_gshared (Dictionary_2_t2116 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m15066(__this, method) (( void (*) (Dictionary_2_t2116 *, const MethodInfo*))Dictionary_2__ctor_m15066_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m15068_gshared (Dictionary_2_t2116 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m15068(__this, ___comparer, method) (( void (*) (Dictionary_2_t2116 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m15068_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>)
extern "C" void Dictionary_2__ctor_m15070_gshared (Dictionary_2_t2116 * __this, Object_t* ___dictionary, const MethodInfo* method);
#define Dictionary_2__ctor_m15070(__this, ___dictionary, method) (( void (*) (Dictionary_2_t2116 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m15070_gshared)(__this, ___dictionary, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m15072_gshared (Dictionary_2_t2116 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m15072(__this, ___capacity, method) (( void (*) (Dictionary_2_t2116 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m15072_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Collections.Generic.IDictionary`2<TKey,TValue>,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m15074_gshared (Dictionary_2_t2116 * __this, Object_t* ___dictionary, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m15074(__this, ___dictionary, ___comparer, method) (( void (*) (Dictionary_2_t2116 *, Object_t*, Object_t*, const MethodInfo*))Dictionary_2__ctor_m15074_gshared)(__this, ___dictionary, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m15076_gshared (Dictionary_2_t2116 * __this, SerializationInfo_t631 * ___info, StreamingContext_t632  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m15076(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2116 *, SerializationInfo_t631 *, StreamingContext_t632 , const MethodInfo*))Dictionary_2__ctor_m15076_gshared)(__this, ___info, ___context, method)
// System.Collections.Generic.ICollection`1<TKey> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Keys()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m15078_gshared (Dictionary_2_t2116 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m15078(__this, method) (( Object_t* (*) (Dictionary_2_t2116 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Keys_m15078_gshared)(__this, method)
// System.Collections.Generic.ICollection`1<TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.IDictionary<TKey,TValue>.get_Values()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m15080_gshared (Dictionary_2_t2116 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m15080(__this, method) (( Object_t* (*) (Dictionary_2_t2116 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IDictionaryU3CTKeyU2CTValueU3E_get_Values_m15080_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m15082_gshared (Dictionary_2_t2116 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m15082(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2116 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m15082_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m15084_gshared (Dictionary_2_t2116 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m15084(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2116 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m15084_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m15086_gshared (Dictionary_2_t2116 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m15086(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2116 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m15086_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m15088_gshared (Dictionary_2_t2116 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m15088(__this, ___key, method) (( bool (*) (Dictionary_2_t2116 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m15088_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m15090_gshared (Dictionary_2_t2116 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m15090(__this, ___key, method) (( void (*) (Dictionary_2_t2116 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m15090_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15092_gshared (Dictionary_2_t2116 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15092(__this, method) (( bool (*) (Dictionary_2_t2116 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m15092_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15094_gshared (Dictionary_2_t2116 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15094(__this, method) (( Object_t * (*) (Dictionary_2_t2116 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m15094_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15096_gshared (Dictionary_2_t2116 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15096(__this, method) (( bool (*) (Dictionary_2_t2116 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m15096_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15098_gshared (Dictionary_2_t2116 * __this, KeyValuePair_2_t2118  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15098(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t2116 *, KeyValuePair_2_t2118 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m15098_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15100_gshared (Dictionary_2_t2116 * __this, KeyValuePair_2_t2118  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15100(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2116 *, KeyValuePair_2_t2118 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m15100_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15102_gshared (Dictionary_2_t2116 * __this, KeyValuePair_2U5BU5D_t2447* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15102(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2116 *, KeyValuePair_2U5BU5D_t2447*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m15102_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15104_gshared (Dictionary_2_t2116 * __this, KeyValuePair_2_t2118  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15104(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t2116 *, KeyValuePair_2_t2118 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m15104_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m15106_gshared (Dictionary_2_t2116 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m15106(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2116 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m15106_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15108_gshared (Dictionary_2_t2116 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15108(__this, method) (( Object_t * (*) (Dictionary_2_t2116 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m15108_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15110_gshared (Dictionary_2_t2116 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15110(__this, method) (( Object_t* (*) (Dictionary_2_t2116 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m15110_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15112_gshared (Dictionary_2_t2116 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15112(__this, method) (( Object_t * (*) (Dictionary_2_t2116 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m15112_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m15114_gshared (Dictionary_2_t2116 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m15114(__this, method) (( int32_t (*) (Dictionary_2_t2116 *, const MethodInfo*))Dictionary_2_get_Count_m15114_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Item(TKey)
extern "C" int64_t Dictionary_2_get_Item_m15116_gshared (Dictionary_2_t2116 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m15116(__this, ___key, method) (( int64_t (*) (Dictionary_2_t2116 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m15116_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m15118_gshared (Dictionary_2_t2116 * __this, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m15118(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2116 *, Object_t *, int64_t, const MethodInfo*))Dictionary_2_set_Item_m15118_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m15120_gshared (Dictionary_2_t2116 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m15120(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t2116 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m15120_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m15122_gshared (Dictionary_2_t2116 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m15122(__this, ___size, method) (( void (*) (Dictionary_2_t2116 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m15122_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m15124_gshared (Dictionary_2_t2116 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m15124(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2116 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m15124_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t2118  Dictionary_2_make_pair_m15126_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m15126(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2118  (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_make_pair_m15126_gshared)(__this /* static, unused */, ___key, ___value, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::pick_key(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_key_m15128_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_key_m15128(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_pick_key_m15128_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::pick_value(TKey,TValue)
extern "C" int64_t Dictionary_2_pick_value_m15130_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m15130(__this /* static, unused */, ___key, ___value, method) (( int64_t (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_pick_value_m15130_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m15132_gshared (Dictionary_2_t2116 * __this, KeyValuePair_2U5BU5D_t2447* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m15132(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t2116 *, KeyValuePair_2U5BU5D_t2447*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m15132_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Resize()
extern "C" void Dictionary_2_Resize_m15134_gshared (Dictionary_2_t2116 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m15134(__this, method) (( void (*) (Dictionary_2_t2116 *, const MethodInfo*))Dictionary_2_Resize_m15134_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m15136_gshared (Dictionary_2_t2116 * __this, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m15136(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t2116 *, Object_t *, int64_t, const MethodInfo*))Dictionary_2_Add_m15136_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Clear()
extern "C" void Dictionary_2_Clear_m15138_gshared (Dictionary_2_t2116 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m15138(__this, method) (( void (*) (Dictionary_2_t2116 *, const MethodInfo*))Dictionary_2_Clear_m15138_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m15140_gshared (Dictionary_2_t2116 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m15140(__this, ___key, method) (( bool (*) (Dictionary_2_t2116 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m15140_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m15142_gshared (Dictionary_2_t2116 * __this, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m15142(__this, ___value, method) (( bool (*) (Dictionary_2_t2116 *, int64_t, const MethodInfo*))Dictionary_2_ContainsValue_m15142_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m15144_gshared (Dictionary_2_t2116 * __this, SerializationInfo_t631 * ___info, StreamingContext_t632  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m15144(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t2116 *, SerializationInfo_t631 *, StreamingContext_t632 , const MethodInfo*))Dictionary_2_GetObjectData_m15144_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m15146_gshared (Dictionary_2_t2116 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m15146(__this, ___sender, method) (( void (*) (Dictionary_2_t2116 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m15146_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m15148_gshared (Dictionary_2_t2116 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m15148(__this, ___key, method) (( bool (*) (Dictionary_2_t2116 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m15148_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m15150_gshared (Dictionary_2_t2116 * __this, Object_t * ___key, int64_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m15150(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t2116 *, Object_t *, int64_t*, const MethodInfo*))Dictionary_2_TryGetValue_m15150_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/KeyCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Keys()
extern "C" KeyCollection_t2121 * Dictionary_2_get_Keys_m15152_gshared (Dictionary_2_t2116 * __this, const MethodInfo* method);
#define Dictionary_2_get_Keys_m15152(__this, method) (( KeyCollection_t2121 * (*) (Dictionary_2_t2116 *, const MethodInfo*))Dictionary_2_get_Keys_m15152_gshared)(__this, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::get_Values()
extern "C" ValueCollection_t2125 * Dictionary_2_get_Values_m15154_gshared (Dictionary_2_t2116 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m15154(__this, method) (( ValueCollection_t2125 * (*) (Dictionary_2_t2116 *, const MethodInfo*))Dictionary_2_get_Values_m15154_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m15156_gshared (Dictionary_2_t2116 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m15156(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t2116 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m15156_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ToTValue(System.Object)
extern "C" int64_t Dictionary_2_ToTValue_m15158_gshared (Dictionary_2_t2116 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m15158(__this, ___value, method) (( int64_t (*) (Dictionary_2_t2116 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m15158_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m15160_gshared (Dictionary_2_t2116 * __this, KeyValuePair_2_t2118  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m15160(__this, ___pair, method) (( bool (*) (Dictionary_2_t2116 *, KeyValuePair_2_t2118 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m15160_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::GetEnumerator()
extern "C" Enumerator_t2123  Dictionary_2_GetEnumerator_m15162_gshared (Dictionary_2_t2116 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m15162(__this, method) (( Enumerator_t2123  (*) (Dictionary_2_t2116 *, const MethodInfo*))Dictionary_2_GetEnumerator_m15162_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Int64>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1068  Dictionary_2_U3CCopyToU3Em__0_m15164_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int64_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m15164(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1068  (*) (Object_t * /* static, unused */, Object_t *, int64_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m15164_gshared)(__this /* static, unused */, ___key, ___value, method)
