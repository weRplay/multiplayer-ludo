﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1905;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_7.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m12183_gshared (Enumerator_t1915 * __this, Dictionary_2_t1905 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m12183(__this, ___host, method) (( void (*) (Enumerator_t1915 *, Dictionary_2_t1905 *, const MethodInfo*))Enumerator__ctor_m12183_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m12184_gshared (Enumerator_t1915 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m12184(__this, method) (( Object_t * (*) (Enumerator_t1915 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12184_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m12185_gshared (Enumerator_t1915 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m12185(__this, method) (( void (*) (Enumerator_t1915 *, const MethodInfo*))Enumerator_Dispose_m12185_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m12186_gshared (Enumerator_t1915 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m12186(__this, method) (( bool (*) (Enumerator_t1915 *, const MethodInfo*))Enumerator_MoveNext_m12186_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m12187_gshared (Enumerator_t1915 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m12187(__this, method) (( int32_t (*) (Enumerator_t1915 *, const MethodInfo*))Enumerator_get_Current_m12187_gshared)(__this, method)
