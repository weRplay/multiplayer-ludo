﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// DynamicScrollView
struct DynamicScrollView_t14;
// System.String
struct String_t;
// System.Collections.IEnumerator
struct IEnumerator_t51;

#include "codegen/il2cpp-codegen.h"

// System.Void DynamicScrollView::.ctor()
extern "C" void DynamicScrollView__ctor_m30 (DynamicScrollView_t14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicScrollView::OnEnable()
extern "C" void DynamicScrollView_OnEnable_m31 (DynamicScrollView_t14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicScrollView::ClearOldElement()
extern "C" void DynamicScrollView_ClearOldElement_m32 (DynamicScrollView_t14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicScrollView::SetContentHeight()
extern "C" void DynamicScrollView_SetContentHeight_m33 (DynamicScrollView_t14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicScrollView::InitializeList()
extern "C" void DynamicScrollView_InitializeList_m34 (DynamicScrollView_t14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicScrollView::InitializeListForGames(System.String)
extern "C" void DynamicScrollView_InitializeListForGames_m35 (DynamicScrollView_t14 * __this, String_t* ___Game, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicScrollView::InitializeNewItem(System.String)
extern "C" void DynamicScrollView_InitializeNewItem_m36 (DynamicScrollView_t14 * __this, String_t* ___name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DynamicScrollView::MoveTowardsTarget(System.Single,System.Single,System.Single)
extern "C" Object_t * DynamicScrollView_MoveTowardsTarget_m37 (DynamicScrollView_t14 * __this, float ___time, float ___from, float ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator DynamicScrollView::GetActiveUsers()
extern "C" Object_t * DynamicScrollView_GetActiveUsers_m38 (DynamicScrollView_t14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void DynamicScrollView::AddNewElement()
extern "C" void DynamicScrollView_AddNewElement_m39 (DynamicScrollView_t14 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
