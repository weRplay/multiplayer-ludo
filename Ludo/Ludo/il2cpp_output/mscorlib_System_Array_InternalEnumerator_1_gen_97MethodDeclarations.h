﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_97.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Array/InternalEnumerator`1<System.DateTime>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17709_gshared (InternalEnumerator_1_t2366 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17709(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2366 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17709_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.DateTime>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17710_gshared (InternalEnumerator_1_t2366 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17710(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2366 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17710_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.DateTime>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17711_gshared (InternalEnumerator_1_t2366 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17711(__this, method) (( void (*) (InternalEnumerator_1_t2366 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17711_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.DateTime>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17712_gshared (InternalEnumerator_1_t2366 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17712(__this, method) (( bool (*) (InternalEnumerator_1_t2366 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17712_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.DateTime>::get_Current()
extern "C" DateTime_t396  InternalEnumerator_1_get_Current_m17713_gshared (InternalEnumerator_1_t2366 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17713(__this, method) (( DateTime_t396  (*) (InternalEnumerator_1_t2366 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17713_gshared)(__this, method)
