﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Capture>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m17364(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2309 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m10443_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Capture>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17365(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2309 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m10445_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Capture>::Dispose()
#define InternalEnumerator_1_Dispose_m17366(__this, method) (( void (*) (InternalEnumerator_1_t2309 *, const MethodInfo*))InternalEnumerator_1_Dispose_m10447_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Capture>::MoveNext()
#define InternalEnumerator_1_MoveNext_m17367(__this, method) (( bool (*) (InternalEnumerator_1_t2309 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m10449_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Capture>::get_Current()
#define InternalEnumerator_1_get_Current_m17368(__this, method) (( Capture_t994 * (*) (InternalEnumerator_1_t2309 *, const MethodInfo*))InternalEnumerator_1_get_Current_m10451_gshared)(__this, method)
