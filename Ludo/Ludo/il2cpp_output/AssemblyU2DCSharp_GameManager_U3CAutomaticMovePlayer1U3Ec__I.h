﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t8;
// UnityEngine.WWW
struct WWW_t9;
// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// GameManager/<AutomaticMovePlayer1>c__Iterator6
struct  U3CAutomaticMovePlayer1U3Ec__Iterator6_t24  : public Object_t
{
	// UnityEngine.WWWForm GameManager/<AutomaticMovePlayer1>c__Iterator6::<form>__0
	WWWForm_t8 * ___U3CformU3E__0_0;
	// UnityEngine.WWW GameManager/<AutomaticMovePlayer1>c__Iterator6::<w>__1
	WWW_t9 * ___U3CwU3E__1_1;
	// System.Int32 GameManager/<AutomaticMovePlayer1>c__Iterator6::$PC
	int32_t ___U24PC_2;
	// System.Object GameManager/<AutomaticMovePlayer1>c__Iterator6::$current
	Object_t * ___U24current_3;
};
