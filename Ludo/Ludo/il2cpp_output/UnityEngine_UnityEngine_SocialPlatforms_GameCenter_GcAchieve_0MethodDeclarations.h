﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SocialPlatforms.Impl.Achievement
struct Achievement_t555;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"

// UnityEngine.SocialPlatforms.Impl.Achievement UnityEngine.SocialPlatforms.GameCenter.GcAchievementData::ToAchievement()
extern "C" Achievement_t555 * GcAchievementData_ToAchievement_m3052 (GcAchievementData_t546 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void GcAchievementData_t546_marshal(const GcAchievementData_t546& unmarshaled, GcAchievementData_t546_marshaled& marshaled);
extern "C" void GcAchievementData_t546_marshal_back(const GcAchievementData_t546_marshaled& marshaled, GcAchievementData_t546& unmarshaled);
extern "C" void GcAchievementData_t546_marshal_cleanup(GcAchievementData_t546_marshaled& marshaled);
