﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m14336(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2068 *, String_t*, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m12128_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m14337(__this, method) (( String_t* (*) (KeyValuePair_2_t2068 *, const MethodInfo*))KeyValuePair_2_get_Key_m12129_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m14338(__this, ___value, method) (( void (*) (KeyValuePair_2_t2068 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m12130_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m14339(__this, method) (( int32_t (*) (KeyValuePair_2_t2068 *, const MethodInfo*))KeyValuePair_2_get_Value_m12131_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m14340(__this, ___value, method) (( void (*) (KeyValuePair_2_t2068 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m12132_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m14341(__this, method) (( String_t* (*) (KeyValuePair_2_t2068 *, const MethodInfo*))KeyValuePair_2_ToString_m12133_gshared)(__this, method)
