﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.WaitForSeconds
struct WaitForSeconds_t63;
struct WaitForSeconds_t63_marshaled;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.WaitForSeconds::.ctor(System.Single)
extern "C" void WaitForSeconds__ctor_m270 (WaitForSeconds_t63 * __this, float ___seconds, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void WaitForSeconds_t63_marshal(const WaitForSeconds_t63& unmarshaled, WaitForSeconds_t63_marshaled& marshaled);
extern "C" void WaitForSeconds_t63_marshal_back(const WaitForSeconds_t63_marshaled& marshaled, WaitForSeconds_t63& unmarshaled);
extern "C" void WaitForSeconds_t63_marshal_cleanup(WaitForSeconds_t63_marshaled& marshaled);
