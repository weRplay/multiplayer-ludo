﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t8;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t9;
// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// Menu/<CreateUniquePlayer>c__IteratorA
struct  U3CCreateUniquePlayerU3Ec__IteratorA_t31  : public Object_t
{
	// UnityEngine.WWWForm Menu/<CreateUniquePlayer>c__IteratorA::<form>__0
	WWWForm_t8 * ___U3CformU3E__0_0;
	// System.String Menu/<CreateUniquePlayer>c__IteratorA::username
	String_t* ___username_1;
	// UnityEngine.WWW Menu/<CreateUniquePlayer>c__IteratorA::<w>__1
	WWW_t9 * ___U3CwU3E__1_2;
	// System.String Menu/<CreateUniquePlayer>c__IteratorA::<newID>__2
	String_t* ___U3CnewIDU3E__2_3;
	// System.Int32 Menu/<CreateUniquePlayer>c__IteratorA::$PC
	int32_t ___U24PC_4;
	// System.Object Menu/<CreateUniquePlayer>c__IteratorA::$current
	Object_t * ___U24current_5;
	// System.String Menu/<CreateUniquePlayer>c__IteratorA::<$>username
	String_t* ___U3CU24U3Eusername_6;
};
