﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Sprite
struct Sprite_t49;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t50;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// Wall
struct  Wall_t48  : public MonoBehaviour_t2
{
	// UnityEngine.Sprite Wall::dmgSprite
	Sprite_t49 * ___dmgSprite_2;
	// System.Int32 Wall::hp
	int32_t ___hp_3;
	// UnityEngine.SpriteRenderer Wall::spriteRenderer
	SpriteRenderer_t50 * ___spriteRenderer_4;
};
