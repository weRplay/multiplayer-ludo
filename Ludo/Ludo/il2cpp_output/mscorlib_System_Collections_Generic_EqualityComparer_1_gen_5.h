﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>
struct EqualityComparer_1_t2109;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>
struct  EqualityComparer_1_t2109  : public Object_t
{
};
struct EqualityComparer_1_t2109_StaticFields{
	// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.UILineInfo>::_default
	EqualityComparer_1_t2109 * ____default_0;
};
