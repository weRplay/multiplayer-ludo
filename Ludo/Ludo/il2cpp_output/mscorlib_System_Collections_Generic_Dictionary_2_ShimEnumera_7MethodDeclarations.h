﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>
struct ShimEnumerator_t2321;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1000;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m17497_gshared (ShimEnumerator_t2321 * __this, Dictionary_2_t1000 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m17497(__this, ___host, method) (( void (*) (ShimEnumerator_t2321 *, Dictionary_2_t1000 *, const MethodInfo*))ShimEnumerator__ctor_m17497_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m17498_gshared (ShimEnumerator_t2321 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m17498(__this, method) (( bool (*) (ShimEnumerator_t2321 *, const MethodInfo*))ShimEnumerator_MoveNext_m17498_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Entry()
extern "C" DictionaryEntry_t1068  ShimEnumerator_get_Entry_m17499_gshared (ShimEnumerator_t2321 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m17499(__this, method) (( DictionaryEntry_t1068  (*) (ShimEnumerator_t2321 *, const MethodInfo*))ShimEnumerator_get_Entry_m17499_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m17500_gshared (ShimEnumerator_t2321 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m17500(__this, method) (( Object_t * (*) (ShimEnumerator_t2321 *, const MethodInfo*))ShimEnumerator_get_Key_m17500_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m17501_gshared (ShimEnumerator_t2321 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m17501(__this, method) (( Object_t * (*) (ShimEnumerator_t2321 *, const MethodInfo*))ShimEnumerator_get_Value_m17501_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Int32>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m17502_gshared (ShimEnumerator_t2321 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m17502(__this, method) (( Object_t * (*) (ShimEnumerator_t2321 *, const MethodInfo*))ShimEnumerator_get_Current_m17502_gshared)(__this, method)
