﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void U24ArrayTypeU244_t868_marshal(const U24ArrayTypeU244_t868& unmarshaled, U24ArrayTypeU244_t868_marshaled& marshaled);
extern "C" void U24ArrayTypeU244_t868_marshal_back(const U24ArrayTypeU244_t868_marshaled& marshaled, U24ArrayTypeU244_t868& unmarshaled);
extern "C" void U24ArrayTypeU244_t868_marshal_cleanup(U24ArrayTypeU244_t868_marshaled& marshaled);
