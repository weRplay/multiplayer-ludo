﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m13152(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1980 *, Graphic_t145 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m12128_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m13153(__this, method) (( Graphic_t145 * (*) (KeyValuePair_2_t1980 *, const MethodInfo*))KeyValuePair_2_get_Key_m12129_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m13154(__this, ___value, method) (( void (*) (KeyValuePair_2_t1980 *, Graphic_t145 *, const MethodInfo*))KeyValuePair_2_set_Key_m12130_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m13155(__this, method) (( int32_t (*) (KeyValuePair_2_t1980 *, const MethodInfo*))KeyValuePair_2_get_Value_m12131_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m13156(__this, ___value, method) (( void (*) (KeyValuePair_2_t1980 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m12132_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.UI.Graphic,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m13157(__this, method) (( String_t* (*) (KeyValuePair_2_t1980 *, const MethodInfo*))KeyValuePair_2_ToString_m12133_gshared)(__this, method)
