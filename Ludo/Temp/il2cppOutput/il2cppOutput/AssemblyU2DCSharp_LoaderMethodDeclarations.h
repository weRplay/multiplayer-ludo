﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Loader
struct Loader_t30;

#include "codegen/il2cpp-codegen.h"

// System.Void Loader::.ctor()
extern "C" void Loader__ctor_m108 (Loader_t30 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Loader::Awake()
extern "C" void Loader_Awake_m109 (Loader_t30 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
