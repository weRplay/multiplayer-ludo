﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.EventTrigger/Entry>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m11419(__this, ___l, method) (( void (*) (Enumerator_t1856 *, List_1_t77 *, const MethodInfo*))Enumerator__ctor_m10721_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.EventTrigger/Entry>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m11420(__this, method) (( Object_t * (*) (Enumerator_t1856 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m10722_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.EventTrigger/Entry>::Dispose()
#define Enumerator_Dispose_m11421(__this, method) (( void (*) (Enumerator_t1856 *, const MethodInfo*))Enumerator_Dispose_m10723_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.EventTrigger/Entry>::VerifyState()
#define Enumerator_VerifyState_m11422(__this, method) (( void (*) (Enumerator_t1856 *, const MethodInfo*))Enumerator_VerifyState_m10724_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.EventTrigger/Entry>::MoveNext()
#define Enumerator_MoveNext_m11423(__this, method) (( bool (*) (Enumerator_t1856 *, const MethodInfo*))Enumerator_MoveNext_m10725_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.EventTrigger/Entry>::get_Current()
#define Enumerator_get_Current_m11424(__this, method) (( Entry_t75 * (*) (Enumerator_t1856 *, const MethodInfo*))Enumerator_get_Current_m10726_gshared)(__this, method)
