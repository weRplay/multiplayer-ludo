﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t47;
// DynamicScrollView
struct DynamicScrollView_t14;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// ScrollItem
struct  ScrollItem_t46  : public MonoBehaviour_t2
{
	// UnityEngine.UI.Text ScrollItem::indexText
	Text_t47 * ___indexText_2;
	// DynamicScrollView ScrollItem::dynamicScrollView
	DynamicScrollView_t14 * ___dynamicScrollView_3;
};
