﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_18MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::.ctor()
#define List_1__ctor_m12206(__this, method) (( void (*) (List_1_t1903 *, const MethodInfo*))List_1__ctor_m3317_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::.ctor(System.Int32)
#define List_1__ctor_m12207(__this, ___capacity, method) (( void (*) (List_1_t1903 *, int32_t, const MethodInfo*))List_1__ctor_m10626_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::.cctor()
#define List_1__cctor_m12208(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m10628_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12209(__this, method) (( Object_t* (*) (List_1_t1903 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m10630_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m12210(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1903 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m10632_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m12211(__this, method) (( Object_t * (*) (List_1_t1903 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m10634_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m12212(__this, ___item, method) (( int32_t (*) (List_1_t1903 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m10636_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m12213(__this, ___item, method) (( bool (*) (List_1_t1903 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m10638_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m12214(__this, ___item, method) (( int32_t (*) (List_1_t1903 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m10640_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m12215(__this, ___index, ___item, method) (( void (*) (List_1_t1903 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m10642_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m12216(__this, ___item, method) (( void (*) (List_1_t1903 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m10644_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12217(__this, method) (( bool (*) (List_1_t1903 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m10646_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m12218(__this, method) (( bool (*) (List_1_t1903 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m10648_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m12219(__this, method) (( Object_t * (*) (List_1_t1903 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m10650_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m12220(__this, method) (( bool (*) (List_1_t1903 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m10652_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m12221(__this, method) (( bool (*) (List_1_t1903 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m10654_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m12222(__this, ___index, method) (( Object_t * (*) (List_1_t1903 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m10656_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m12223(__this, ___index, ___value, method) (( void (*) (List_1_t1903 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m10658_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Add(T)
#define List_1_Add_m12224(__this, ___item, method) (( void (*) (List_1_t1903 *, Object_t *, const MethodInfo*))List_1_Add_m10660_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m12225(__this, ___newCount, method) (( void (*) (List_1_t1903 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m10662_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m12226(__this, ___collection, method) (( void (*) (List_1_t1903 *, Object_t*, const MethodInfo*))List_1_AddCollection_m10664_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m12227(__this, ___enumerable, method) (( void (*) (List_1_t1903 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m10666_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m12228(__this, ___collection, method) (( void (*) (List_1_t1903 *, Object_t*, const MethodInfo*))List_1_AddRange_m10668_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::AsReadOnly()
#define List_1_AsReadOnly_m12229(__this, method) (( ReadOnlyCollection_1_t2409 * (*) (List_1_t1903 *, const MethodInfo*))List_1_AsReadOnly_m10670_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Clear()
#define List_1_Clear_m12230(__this, method) (( void (*) (List_1_t1903 *, const MethodInfo*))List_1_Clear_m10672_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Contains(T)
#define List_1_Contains_m12231(__this, ___item, method) (( bool (*) (List_1_t1903 *, Object_t *, const MethodInfo*))List_1_Contains_m10674_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m12232(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t1903 *, ICanvasElementU5BU5D_t1919*, int32_t, const MethodInfo*))List_1_CopyTo_m10676_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Find(System.Predicate`1<T>)
#define List_1_Find_m12233(__this, ___match, method) (( Object_t * (*) (List_1_t1903 *, Predicate_1_t139 *, const MethodInfo*))List_1_Find_m10678_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m12234(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t139 *, const MethodInfo*))List_1_CheckMatch_m10680_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m12235(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t1903 *, int32_t, int32_t, Predicate_1_t139 *, const MethodInfo*))List_1_GetIndex_m10682_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::GetEnumerator()
#define List_1_GetEnumerator_m12236(__this, method) (( Enumerator_t2410  (*) (List_1_t1903 *, const MethodInfo*))List_1_GetEnumerator_m10684_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::IndexOf(T)
#define List_1_IndexOf_m12237(__this, ___item, method) (( int32_t (*) (List_1_t1903 *, Object_t *, const MethodInfo*))List_1_IndexOf_m10686_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m12238(__this, ___start, ___delta, method) (( void (*) (List_1_t1903 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m10688_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m12239(__this, ___index, method) (( void (*) (List_1_t1903 *, int32_t, const MethodInfo*))List_1_CheckIndex_m10690_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Insert(System.Int32,T)
#define List_1_Insert_m12240(__this, ___index, ___item, method) (( void (*) (List_1_t1903 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m10692_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m12241(__this, ___collection, method) (( void (*) (List_1_t1903 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m10694_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Remove(T)
#define List_1_Remove_m12242(__this, ___item, method) (( bool (*) (List_1_t1903 *, Object_t *, const MethodInfo*))List_1_Remove_m10696_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m12243(__this, ___match, method) (( int32_t (*) (List_1_t1903 *, Predicate_1_t139 *, const MethodInfo*))List_1_RemoveAll_m10698_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m12244(__this, ___index, method) (( void (*) (List_1_t1903 *, int32_t, const MethodInfo*))List_1_RemoveAt_m10700_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Reverse()
#define List_1_Reverse_m12245(__this, method) (( void (*) (List_1_t1903 *, const MethodInfo*))List_1_Reverse_m10702_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Sort()
#define List_1_Sort_m12246(__this, method) (( void (*) (List_1_t1903 *, const MethodInfo*))List_1_Sort_m10704_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m12247(__this, ___comparison, method) (( void (*) (List_1_t1903 *, Comparison_1_t138 *, const MethodInfo*))List_1_Sort_m10706_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::ToArray()
#define List_1_ToArray_m12248(__this, method) (( ICanvasElementU5BU5D_t1919* (*) (List_1_t1903 *, const MethodInfo*))List_1_ToArray_m10708_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::TrimExcess()
#define List_1_TrimExcess_m12249(__this, method) (( void (*) (List_1_t1903 *, const MethodInfo*))List_1_TrimExcess_m10710_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::get_Capacity()
#define List_1_get_Capacity_m12250(__this, method) (( int32_t (*) (List_1_t1903 *, const MethodInfo*))List_1_get_Capacity_m10712_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m12251(__this, ___value, method) (( void (*) (List_1_t1903 *, int32_t, const MethodInfo*))List_1_set_Capacity_m10714_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::get_Count()
#define List_1_get_Count_m12252(__this, method) (( int32_t (*) (List_1_t1903 *, const MethodInfo*))List_1_get_Count_m10716_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::get_Item(System.Int32)
#define List_1_get_Item_m12253(__this, ___index, method) (( Object_t * (*) (List_1_t1903 *, int32_t, const MethodInfo*))List_1_get_Item_m10718_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UI.ICanvasElement>::set_Item(System.Int32,T)
#define List_1_set_Item_m12254(__this, ___index, ___value, method) (( void (*) (List_1_t1903 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m10720_gshared)(__this, ___index, ___value, method)
