﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// SimpleJson.JsonArray
struct JsonArray_t510;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void SimpleJson.JsonArray::.ctor()
extern "C" void JsonArray__ctor_m2942 (JsonArray_t510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String SimpleJson.JsonArray::ToString()
extern "C" String_t* JsonArray_ToString_m2943 (JsonArray_t510 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
