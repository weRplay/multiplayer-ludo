﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.TimeSpan>
struct GenericComparer_1_t1780;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.TimeSpan>::.ctor()
extern "C" void GenericComparer_1__ctor_m10440_gshared (GenericComparer_1_t1780 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m10440(__this, method) (( void (*) (GenericComparer_1_t1780 *, const MethodInfo*))GenericComparer_1__ctor_m10440_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.TimeSpan>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m17881_gshared (GenericComparer_1_t1780 * __this, TimeSpan_t977  ___x, TimeSpan_t977  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m17881(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t1780 *, TimeSpan_t977 , TimeSpan_t977 , const MethodInfo*))GenericComparer_1_Compare_m17881_gshared)(__this, ___x, ___y, method)
