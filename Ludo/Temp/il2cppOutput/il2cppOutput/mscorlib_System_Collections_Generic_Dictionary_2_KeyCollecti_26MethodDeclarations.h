﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2236;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_26.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m16735_gshared (Enumerator_t2242 * __this, Dictionary_2_t2236 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m16735(__this, ___host, method) (( void (*) (Enumerator_t2242 *, Dictionary_2_t2236 *, const MethodInfo*))Enumerator__ctor_m16735_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16736_gshared (Enumerator_t2242 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16736(__this, method) (( Object_t * (*) (Enumerator_t2242 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16736_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void Enumerator_Dispose_m16737_gshared (Enumerator_t2242 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16737(__this, method) (( void (*) (Enumerator_t2242 *, const MethodInfo*))Enumerator_Dispose_m16737_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16738_gshared (Enumerator_t2242 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16738(__this, method) (( bool (*) (Enumerator_t2242 *, const MethodInfo*))Enumerator_MoveNext_m16738_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m16739_gshared (Enumerator_t2242 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16739(__this, method) (( Object_t * (*) (Enumerator_t2242 *, const MethodInfo*))Enumerator_get_Current_m16739_gshared)(__this, method)
