﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t5;
// UnityEngine.UI.InputField
struct InputField_t33;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// Menu
struct  Menu_t32  : public MonoBehaviour_t2
{
	// UnityEngine.GameObject Menu::createUserButton
	GameObject_t5 * ___createUserButton_2;
	// UnityEngine.GameObject Menu::userNameTextEdit
	GameObject_t5 * ___userNameTextEdit_3;
	// UnityEngine.UI.InputField Menu::username
	InputField_t33 * ___username_4;
};
