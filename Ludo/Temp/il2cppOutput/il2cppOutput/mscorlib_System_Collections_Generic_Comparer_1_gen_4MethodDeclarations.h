﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>
struct Comparer_1_t2112;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void Comparer_1__ctor_m15055_gshared (Comparer_1_t2112 * __this, const MethodInfo* method);
#define Comparer_1__ctor_m15055(__this, method) (( void (*) (Comparer_1_t2112 *, const MethodInfo*))Comparer_1__ctor_m15055_gshared)(__this, method)
// System.Void System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::.cctor()
extern "C" void Comparer_1__cctor_m15056_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1__cctor_m15056(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1__cctor_m15056_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::System.Collections.IComparer.Compare(System.Object,System.Object)
extern "C" int32_t Comparer_1_System_Collections_IComparer_Compare_m15057_gshared (Comparer_1_t2112 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define Comparer_1_System_Collections_IComparer_Compare_m15057(__this, ___x, ___y, method) (( int32_t (*) (Comparer_1_t2112 *, Object_t *, Object_t *, const MethodInfo*))Comparer_1_System_Collections_IComparer_Compare_m15057_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.Comparer`1<T> System.Collections.Generic.Comparer`1<UnityEngine.UILineInfo>::get_Default()
extern "C" Comparer_1_t2112 * Comparer_1_get_Default_m15058_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Comparer_1_get_Default_m15058(__this /* static, unused */, method) (( Comparer_1_t2112 * (*) (Object_t * /* static, unused */, const MethodInfo*))Comparer_1_get_Default_m15058_gshared)(__this /* static, unused */, method)
