﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_25MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m17313(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2304 *, String_t*, bool, const MethodInfo*))KeyValuePair_2__ctor_m17215_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::get_Key()
#define KeyValuePair_2_get_Key_m17314(__this, method) (( String_t* (*) (KeyValuePair_2_t2304 *, const MethodInfo*))KeyValuePair_2_get_Key_m17216_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17315(__this, ___value, method) (( void (*) (KeyValuePair_2_t2304 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m17217_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::get_Value()
#define KeyValuePair_2_get_Value_m17316(__this, method) (( bool (*) (KeyValuePair_2_t2304 *, const MethodInfo*))KeyValuePair_2_get_Value_m17218_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17317(__this, ___value, method) (( void (*) (KeyValuePair_2_t2304 *, bool, const MethodInfo*))KeyValuePair_2_set_Value_m17219_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Boolean>::ToString()
#define KeyValuePair_2_ToString_m17318(__this, method) (( String_t* (*) (KeyValuePair_2_t2304 *, const MethodInfo*))KeyValuePair_2_ToString_m17220_gshared)(__this, method)
