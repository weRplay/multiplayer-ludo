﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrollItem/<CreateGame>c__Iterator11
struct U3CCreateGameU3Ec__Iterator11_t45;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ScrollItem/<CreateGame>c__Iterator11::.ctor()
extern "C" void U3CCreateGameU3Ec__Iterator11__ctor_m178 (U3CCreateGameU3Ec__Iterator11_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScrollItem/<CreateGame>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CCreateGameU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m179 (U3CCreateGameU3Ec__Iterator11_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object ScrollItem/<CreateGame>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCreateGameU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m180 (U3CCreateGameU3Ec__Iterator11_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean ScrollItem/<CreateGame>c__Iterator11::MoveNext()
extern "C" bool U3CCreateGameU3Ec__Iterator11_MoveNext_m181 (U3CCreateGameU3Ec__Iterator11_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollItem/<CreateGame>c__Iterator11::Dispose()
extern "C" void U3CCreateGameU3Ec__Iterator11_Dispose_m182 (U3CCreateGameU3Ec__Iterator11_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollItem/<CreateGame>c__Iterator11::Reset()
extern "C" void U3CCreateGameU3Ec__Iterator11_Reset_m183 (U3CCreateGameU3Ec__Iterator11_t45 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
