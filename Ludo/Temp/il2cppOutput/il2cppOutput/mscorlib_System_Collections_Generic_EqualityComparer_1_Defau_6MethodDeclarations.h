﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int64>
struct DefaultComparer_t2132;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int64>::.ctor()
extern "C" void DefaultComparer__ctor_m15261_gshared (DefaultComparer_t2132 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m15261(__this, method) (( void (*) (DefaultComparer_t2132 *, const MethodInfo*))DefaultComparer__ctor_m15261_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int64>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m15262_gshared (DefaultComparer_t2132 * __this, int64_t ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m15262(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2132 *, int64_t, const MethodInfo*))DefaultComparer_GetHashCode_m15262_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int64>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m15263_gshared (DefaultComparer_t2132 * __this, int64_t ___x, int64_t ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m15263(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2132 *, int64_t, int64_t, const MethodInfo*))DefaultComparer_Equals_m15263_gshared)(__this, ___x, ___y, method)
