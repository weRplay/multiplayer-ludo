﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// UnityEngine.GameObject[]
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t4  : public Array_t { };
// UnityEngine.Object[]
// UnityEngine.Object[]
struct ObjectU5BU5D_t599  : public Array_t { };
// UnityEngine.Vector3[]
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t208  : public Array_t { };
// UnityEngine.Component[]
// UnityEngine.Component[]
struct ComponentU5BU5D_t1830  : public Array_t { };
// UnityEngine.Transform[]
// UnityEngine.Transform[]
struct TransformU5BU5D_t1861  : public Array_t { };
// UnityEngine.RaycastHit2D[]
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t307  : public Array_t { };
// UnityEngine.RaycastHit[]
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t309  : public Array_t { };
// UnityEngine.Font[]
// UnityEngine.Font[]
struct FontU5BU5D_t1922  : public Array_t { };
// UnityEngine.UIVertex[]
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t185  : public Array_t { };
// UnityEngine.Canvas[]
// UnityEngine.Canvas[]
struct CanvasU5BU5D_t1961  : public Array_t { };
// UnityEngine.Vector2[]
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t169  : public Array_t { };
// UnityEngine.UILineInfo[]
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t612  : public Array_t { };
// UnityEngine.UICharInfo[]
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t611  : public Array_t { };
// UnityEngine.CanvasGroup[]
// UnityEngine.CanvasGroup[]
struct CanvasGroupU5BU5D_t2002  : public Array_t { };
// UnityEngine.RectTransform[]
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t2023  : public Array_t { };
// UnityEngine.SocialPlatforms.IAchievementDescription[]
// UnityEngine.SocialPlatforms.IAchievementDescription[]
struct IAchievementDescriptionU5BU5D_t636  : public Array_t { };
// UnityEngine.SocialPlatforms.IAchievement[]
// UnityEngine.SocialPlatforms.IAchievement[]
struct IAchievementU5BU5D_t638  : public Array_t { };
// UnityEngine.SocialPlatforms.IScore[]
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t558  : public Array_t { };
// UnityEngine.SocialPlatforms.IUserProfile[]
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t554  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
struct AchievementDescriptionU5BU5D_t375  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t376  : public Array_t { };
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
struct GcLeaderboardU5BU5D_t2040  : public Array_t { };
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct GcAchievementDataU5BU5D_t601  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.Achievement[]
// UnityEngine.SocialPlatforms.Impl.Achievement[]
struct AchievementU5BU5D_t637  : public Array_t { };
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct GcScoreDataU5BU5D_t602  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.Score[]
// UnityEngine.SocialPlatforms.Impl.Score[]
struct ScoreU5BU5D_t639  : public Array_t { };
// UnityEngine.GUILayoutOption[]
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t606  : public Array_t { };
// UnityEngine.GUILayoutUtility/LayoutCache[]
// UnityEngine.GUILayoutUtility/LayoutCache[]
struct LayoutCacheU5BU5D_t2050  : public Array_t { };
// UnityEngine.GUILayoutEntry[]
// UnityEngine.GUILayoutEntry[]
struct GUILayoutEntryU5BU5D_t2056  : public Array_t { };
// UnityEngine.GUIStyle[]
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t412  : public Array_t { };
// UnityEngine.Camera[]
// UnityEngine.Camera[]
struct CameraU5BU5D_t564  : public Array_t { };
// UnityEngine.Behaviour[]
// UnityEngine.Behaviour[]
struct BehaviourU5BU5D_t2505  : public Array_t { };
// UnityEngine.Display[]
// UnityEngine.Display[]
struct DisplayU5BU5D_t445  : public Array_t { };
// UnityEngine.Rigidbody2D[]
// UnityEngine.Rigidbody2D[]
struct Rigidbody2DU5BU5D_t2089  : public Array_t { };
// UnityEngine.Keyframe[]
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t610  : public Array_t { };
// UnityEngine.Networking.Match.MatchDirectConnectInfo[]
// UnityEngine.Networking.Match.MatchDirectConnectInfo[]
struct MatchDirectConnectInfoU5BU5D_t2137  : public Array_t { };
// UnityEngine.Networking.Match.MatchDesc[]
// UnityEngine.Networking.Match.MatchDesc[]
struct MatchDescU5BU5D_t2143  : public Array_t { };
// UnityEngine.Networking.Types.NetworkID[]
// UnityEngine.Networking.Types.NetworkID[]
struct NetworkIDU5BU5D_t2149  : public Array_t { };
// UnityEngine.Networking.Types.NetworkAccessToken[]
// UnityEngine.Networking.Types.NetworkAccessToken[]
struct NetworkAccessTokenU5BU5D_t2150  : public Array_t { };
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate[]
// SimpleJson.Reflection.ReflectionUtils/ConstructorDelegate[]
struct ConstructorDelegateU5BU5D_t2180  : public Array_t { };
// SimpleJson.Reflection.ReflectionUtils/GetDelegate[]
// SimpleJson.Reflection.ReflectionUtils/GetDelegate[]
struct GetDelegateU5BU5D_t2192  : public Array_t { };
// UnityEngine.DisallowMultipleComponent[]
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t533  : public Array_t { };
// UnityEngine.ExecuteInEditMode[]
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t534  : public Array_t { };
// UnityEngine.RequireComponent[]
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t535  : public Array_t { };
// UnityEngine.SendMouseEvents/HitInfo[]
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_t563  : public Array_t { };
// UnityEngine.TextEditor/TextEditOp[]
// UnityEngine.TextEditor/TextEditOp[]
struct TextEditOpU5BU5D_t2233  : public Array_t { };
// UnityEngine.Event[]
// UnityEngine.Event[]
struct EventU5BU5D_t2232  : public Array_t { };
// UnityEngine.Events.PersistentCall[]
// UnityEngine.Events.PersistentCall[]
struct PersistentCallU5BU5D_t2266  : public Array_t { };
// UnityEngine.Events.BaseInvokableCall[]
// UnityEngine.Events.BaseInvokableCall[]
struct BaseInvokableCallU5BU5D_t2271  : public Array_t { };
