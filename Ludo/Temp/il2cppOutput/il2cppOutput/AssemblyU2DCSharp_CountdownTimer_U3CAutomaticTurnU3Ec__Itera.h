﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t8;
// UnityEngine.WWW
struct WWW_t9;
// System.Object
struct Object_t;
// CountdownTimer
struct CountdownTimer_t10;

#include "mscorlib_System_Object.h"

// CountdownTimer/<AutomaticTurn>c__Iterator0
struct  U3CAutomaticTurnU3Ec__Iterator0_t7  : public Object_t
{
	// UnityEngine.WWWForm CountdownTimer/<AutomaticTurn>c__Iterator0::<form>__0
	WWWForm_t8 * ___U3CformU3E__0_0;
	// UnityEngine.WWW CountdownTimer/<AutomaticTurn>c__Iterator0::<w>__1
	WWW_t9 * ___U3CwU3E__1_1;
	// System.Int32 CountdownTimer/<AutomaticTurn>c__Iterator0::$PC
	int32_t ___U24PC_2;
	// System.Object CountdownTimer/<AutomaticTurn>c__Iterator0::$current
	Object_t * ___U24current_3;
	// CountdownTimer CountdownTimer/<AutomaticTurn>c__Iterator0::<>f__this
	CountdownTimer_t10 * ___U3CU3Ef__this_4;
};
