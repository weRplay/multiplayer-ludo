﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// MovingObject
struct MovingObject_t36;
// System.Collections.IEnumerator
struct IEnumerator_t51;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void MovingObject::.ctor()
extern "C" void MovingObject__ctor_m127 (MovingObject_t36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObject::Start()
extern "C" void MovingObject_Start_m128 (MovingObject_t36 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean MovingObject::Move(System.Int32,System.Int32,UnityEngine.RaycastHit2D&)
extern "C" bool MovingObject_Move_m129 (MovingObject_t36 * __this, int32_t ___xDir, int32_t ___yDir, RaycastHit2D_t52 * ___hit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator MovingObject::SmoothMovement(UnityEngine.Vector3)
extern "C" Object_t * MovingObject_SmoothMovement_m130 (MovingObject_t36 * __this, Vector3_t35  ___end, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void MovingObject::AttemptMove(System.Int32,System.Int32)
extern "C" void MovingObject_AttemptMove_m131 (MovingObject_t36 * __this, int32_t ___xDir, int32_t ___yDir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
