﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>
struct ShimEnumerator_t2166;
// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>
struct Dictionary_2_t2153;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m15681_gshared (ShimEnumerator_t2166 * __this, Dictionary_2_t2153 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m15681(__this, ___host, method) (( void (*) (ShimEnumerator_t2166 *, Dictionary_2_t2153 *, const MethodInfo*))ShimEnumerator__ctor_m15681_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m15682_gshared (ShimEnumerator_t2166 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m15682(__this, method) (( bool (*) (ShimEnumerator_t2166 *, const MethodInfo*))ShimEnumerator_MoveNext_m15682_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Entry()
extern "C" DictionaryEntry_t1068  ShimEnumerator_get_Entry_m15683_gshared (ShimEnumerator_t2166 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m15683(__this, method) (( DictionaryEntry_t1068  (*) (ShimEnumerator_t2166 *, const MethodInfo*))ShimEnumerator_get_Entry_m15683_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m15684_gshared (ShimEnumerator_t2166 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m15684(__this, method) (( Object_t * (*) (ShimEnumerator_t2166 *, const MethodInfo*))ShimEnumerator_get_Key_m15684_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m15685_gshared (ShimEnumerator_t2166 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m15685(__this, method) (( Object_t * (*) (ShimEnumerator_t2166 *, const MethodInfo*))ShimEnumerator_get_Value_m15685_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m15686_gshared (ShimEnumerator_t2166 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m15686(__this, method) (( Object_t * (*) (ShimEnumerator_t2166 *, const MethodInfo*))ShimEnumerator_get_Current_m15686_gshared)(__this, method)
