﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m15733(__this, ___dictionary, method) (( void (*) (Enumerator_t2172 *, Dictionary_2_t507 *, const MethodInfo*))Enumerator__ctor_m15634_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15734(__this, method) (( Object_t * (*) (Enumerator_t2172 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15635_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15735(__this, method) (( DictionaryEntry_t1068  (*) (Enumerator_t2172 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15636_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15736(__this, method) (( Object_t * (*) (Enumerator_t2172 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15637_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15737(__this, method) (( Object_t * (*) (Enumerator_t2172 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15638_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::MoveNext()
#define Enumerator_MoveNext_m15738(__this, method) (( bool (*) (Enumerator_t2172 *, const MethodInfo*))Enumerator_MoveNext_m15639_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_Current()
#define Enumerator_get_Current_m15739(__this, method) (( KeyValuePair_2_t2169  (*) (Enumerator_t2172 *, const MethodInfo*))Enumerator_get_Current_m15640_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m15740(__this, method) (( uint64_t (*) (Enumerator_t2172 *, const MethodInfo*))Enumerator_get_CurrentKey_m15641_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m15741(__this, method) (( NetworkAccessToken_t504 * (*) (Enumerator_t2172 *, const MethodInfo*))Enumerator_get_CurrentValue_m15642_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::VerifyState()
#define Enumerator_VerifyState_m15742(__this, method) (( void (*) (Enumerator_t2172 *, const MethodInfo*))Enumerator_VerifyState_m15643_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m15743(__this, method) (( void (*) (Enumerator_t2172 *, const MethodInfo*))Enumerator_VerifyCurrent_m15644_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::Dispose()
#define Enumerator_Dispose_m15744(__this, method) (( void (*) (Enumerator_t2172 *, const MethodInfo*))Enumerator_Dispose_m15645_gshared)(__this, method)
