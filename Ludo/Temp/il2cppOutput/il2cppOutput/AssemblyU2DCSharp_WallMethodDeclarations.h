﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Wall
struct Wall_t48;

#include "codegen/il2cpp-codegen.h"

// System.Void Wall::.ctor()
extern "C" void Wall__ctor_m188 (Wall_t48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Wall::Awake()
extern "C" void Wall_Awake_m189 (Wall_t48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Wall::DamageWall(System.Int32)
extern "C" void Wall_DamageWall_m190 (Wall_t48 * __this, int32_t ___loss, const MethodInfo* method) IL2CPP_METHOD_ATTR;
