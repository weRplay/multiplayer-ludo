﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Player/<GetPlayer1Position>c__IteratorD
struct U3CGetPlayer1PositionU3Ec__IteratorD_t41;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Player/<GetPlayer1Position>c__IteratorD::.ctor()
extern "C" void U3CGetPlayer1PositionU3Ec__IteratorD__ctor_m138 (U3CGetPlayer1PositionU3Ec__IteratorD_t41 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Player/<GetPlayer1Position>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CGetPlayer1PositionU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m139 (U3CGetPlayer1PositionU3Ec__IteratorD_t41 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Player/<GetPlayer1Position>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetPlayer1PositionU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m140 (U3CGetPlayer1PositionU3Ec__IteratorD_t41 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Player/<GetPlayer1Position>c__IteratorD::MoveNext()
extern "C" bool U3CGetPlayer1PositionU3Ec__IteratorD_MoveNext_m141 (U3CGetPlayer1PositionU3Ec__IteratorD_t41 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player/<GetPlayer1Position>c__IteratorD::Dispose()
extern "C" void U3CGetPlayer1PositionU3Ec__IteratorD_Dispose_m142 (U3CGetPlayer1PositionU3Ec__IteratorD_t41 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player/<GetPlayer1Position>c__IteratorD::Reset()
extern "C" void U3CGetPlayer1PositionU3Ec__IteratorD_Reset_m143 (U3CGetPlayer1PositionU3Ec__IteratorD_t41 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
