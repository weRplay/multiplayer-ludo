﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t8;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t9;
// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// GameMenu/<GetActiveUsers>c__Iterator9
struct  U3CGetActiveUsersU3Ec__Iterator9_t28  : public Object_t
{
	// UnityEngine.WWWForm GameMenu/<GetActiveUsers>c__Iterator9::<form>__0
	WWWForm_t8 * ___U3CformU3E__0_0;
	// System.String GameMenu/<GetActiveUsers>c__Iterator9::<username>__1
	String_t* ___U3CusernameU3E__1_1;
	// UnityEngine.WWW GameMenu/<GetActiveUsers>c__Iterator9::<w>__2
	WWW_t9 * ___U3CwU3E__2_2;
	// System.String GameMenu/<GetActiveUsers>c__Iterator9::<Players>__3
	String_t* ___U3CPlayersU3E__3_3;
	// System.Int32 GameMenu/<GetActiveUsers>c__Iterator9::$PC
	int32_t ___U24PC_4;
	// System.Object GameMenu/<GetActiveUsers>c__Iterator9::$current
	Object_t * ___U24current_5;
};
