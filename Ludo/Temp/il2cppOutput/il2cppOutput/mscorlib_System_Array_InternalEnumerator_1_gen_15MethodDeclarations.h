﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_15.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m13248_gshared (InternalEnumerator_1_t1988 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m13248(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1988 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m13248_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13249_gshared (InternalEnumerator_1_t1988 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13249(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1988 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m13249_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Vector2>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m13250_gshared (InternalEnumerator_1_t1988 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m13250(__this, method) (( void (*) (InternalEnumerator_1_t1988 *, const MethodInfo*))InternalEnumerator_1_Dispose_m13250_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Vector2>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m13251_gshared (InternalEnumerator_1_t1988 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m13251(__this, method) (( bool (*) (InternalEnumerator_1_t1988 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m13251_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Vector2>::get_Current()
extern "C" Vector2_t59  InternalEnumerator_1_get_Current_m13252_gshared (InternalEnumerator_1_t1988 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m13252(__this, method) (( Vector2_t59  (*) (InternalEnumerator_1_t1988 *, const MethodInfo*))InternalEnumerator_1_get_Current_m13252_gshared)(__this, method)
