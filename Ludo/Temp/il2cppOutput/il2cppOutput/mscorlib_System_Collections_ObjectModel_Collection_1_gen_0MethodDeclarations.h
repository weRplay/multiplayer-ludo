﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Object>
struct Collection_1_t1805;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t51;
// System.Object
struct Object_t;
// System.Object[]
struct ObjectU5BU5D_t60;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2022;
// System.Collections.Generic.IList`1<System.Object>
struct IList_1_t1804;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::.ctor()
extern "C" void Collection_1__ctor_m10757_gshared (Collection_1_t1805 * __this, const MethodInfo* method);
#define Collection_1__ctor_m10757(__this, method) (( void (*) (Collection_1_t1805 *, const MethodInfo*))Collection_1__ctor_m10757_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m10758_gshared (Collection_1_t1805 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m10758(__this, method) (( bool (*) (Collection_1_t1805 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m10758_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m10759_gshared (Collection_1_t1805 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m10759(__this, ___array, ___index, method) (( void (*) (Collection_1_t1805 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m10759_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m10760_gshared (Collection_1_t1805 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m10760(__this, method) (( Object_t * (*) (Collection_1_t1805 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m10760_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m10761_gshared (Collection_1_t1805 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m10761(__this, ___value, method) (( int32_t (*) (Collection_1_t1805 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m10761_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m10762_gshared (Collection_1_t1805 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m10762(__this, ___value, method) (( bool (*) (Collection_1_t1805 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m10762_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m10763_gshared (Collection_1_t1805 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m10763(__this, ___value, method) (( int32_t (*) (Collection_1_t1805 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m10763_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m10764_gshared (Collection_1_t1805 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m10764(__this, ___index, ___value, method) (( void (*) (Collection_1_t1805 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m10764_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m10765_gshared (Collection_1_t1805 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m10765(__this, ___value, method) (( void (*) (Collection_1_t1805 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m10765_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m10766_gshared (Collection_1_t1805 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m10766(__this, method) (( bool (*) (Collection_1_t1805 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m10766_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m10767_gshared (Collection_1_t1805 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m10767(__this, method) (( Object_t * (*) (Collection_1_t1805 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m10767_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m10768_gshared (Collection_1_t1805 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m10768(__this, method) (( bool (*) (Collection_1_t1805 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m10768_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m10769_gshared (Collection_1_t1805 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m10769(__this, method) (( bool (*) (Collection_1_t1805 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m10769_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m10770_gshared (Collection_1_t1805 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m10770(__this, ___index, method) (( Object_t * (*) (Collection_1_t1805 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m10770_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m10771_gshared (Collection_1_t1805 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m10771(__this, ___index, ___value, method) (( void (*) (Collection_1_t1805 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m10771_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Add(T)
extern "C" void Collection_1_Add_m10772_gshared (Collection_1_t1805 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Add_m10772(__this, ___item, method) (( void (*) (Collection_1_t1805 *, Object_t *, const MethodInfo*))Collection_1_Add_m10772_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Clear()
extern "C" void Collection_1_Clear_m10773_gshared (Collection_1_t1805 * __this, const MethodInfo* method);
#define Collection_1_Clear_m10773(__this, method) (( void (*) (Collection_1_t1805 *, const MethodInfo*))Collection_1_Clear_m10773_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::ClearItems()
extern "C" void Collection_1_ClearItems_m10774_gshared (Collection_1_t1805 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m10774(__this, method) (( void (*) (Collection_1_t1805 *, const MethodInfo*))Collection_1_ClearItems_m10774_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Contains(T)
extern "C" bool Collection_1_Contains_m10775_gshared (Collection_1_t1805 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Contains_m10775(__this, ___item, method) (( bool (*) (Collection_1_t1805 *, Object_t *, const MethodInfo*))Collection_1_Contains_m10775_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m10776_gshared (Collection_1_t1805 * __this, ObjectU5BU5D_t60* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m10776(__this, ___array, ___index, method) (( void (*) (Collection_1_t1805 *, ObjectU5BU5D_t60*, int32_t, const MethodInfo*))Collection_1_CopyTo_m10776_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Object>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m10777_gshared (Collection_1_t1805 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m10777(__this, method) (( Object_t* (*) (Collection_1_t1805 *, const MethodInfo*))Collection_1_GetEnumerator_m10777_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m10778_gshared (Collection_1_t1805 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m10778(__this, ___item, method) (( int32_t (*) (Collection_1_t1805 *, Object_t *, const MethodInfo*))Collection_1_IndexOf_m10778_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m10779_gshared (Collection_1_t1805 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Insert_m10779(__this, ___index, ___item, method) (( void (*) (Collection_1_t1805 *, int32_t, Object_t *, const MethodInfo*))Collection_1_Insert_m10779_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m10780_gshared (Collection_1_t1805 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m10780(__this, ___index, ___item, method) (( void (*) (Collection_1_t1805 *, int32_t, Object_t *, const MethodInfo*))Collection_1_InsertItem_m10780_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::Remove(T)
extern "C" bool Collection_1_Remove_m10781_gshared (Collection_1_t1805 * __this, Object_t * ___item, const MethodInfo* method);
#define Collection_1_Remove_m10781(__this, ___item, method) (( bool (*) (Collection_1_t1805 *, Object_t *, const MethodInfo*))Collection_1_Remove_m10781_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m10782_gshared (Collection_1_t1805 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m10782(__this, ___index, method) (( void (*) (Collection_1_t1805 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m10782_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m10783_gshared (Collection_1_t1805 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m10783(__this, ___index, method) (( void (*) (Collection_1_t1805 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m10783_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Object>::get_Count()
extern "C" int32_t Collection_1_get_Count_m10784_gshared (Collection_1_t1805 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m10784(__this, method) (( int32_t (*) (Collection_1_t1805 *, const MethodInfo*))Collection_1_get_Count_m10784_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * Collection_1_get_Item_m10785_gshared (Collection_1_t1805 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m10785(__this, ___index, method) (( Object_t * (*) (Collection_1_t1805 *, int32_t, const MethodInfo*))Collection_1_get_Item_m10785_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m10786_gshared (Collection_1_t1805 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_set_Item_m10786(__this, ___index, ___value, method) (( void (*) (Collection_1_t1805 *, int32_t, Object_t *, const MethodInfo*))Collection_1_set_Item_m10786_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m10787_gshared (Collection_1_t1805 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define Collection_1_SetItem_m10787(__this, ___index, ___item, method) (( void (*) (Collection_1_t1805 *, int32_t, Object_t *, const MethodInfo*))Collection_1_SetItem_m10787_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m10788_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m10788(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m10788_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Object>::ConvertItem(System.Object)
extern "C" Object_t * Collection_1_ConvertItem_m10789_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m10789(__this /* static, unused */, ___item, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m10789_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Object>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m10790_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m10790(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m10790_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m10791_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m10791(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m10791_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Object>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m10792_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m10792(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m10792_gshared)(__this /* static, unused */, ___list, method)
