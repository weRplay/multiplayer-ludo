﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_InvokableCall_1_genMethodDeclarations.h"

// System.Void UnityEngine.Events.InvokableCall`1<System.String>::.ctor(System.Object,System.Reflection.MethodInfo)
#define InvokableCall_1__ctor_m16885(__this, ___target, ___theFunction, method) (( void (*) (InvokableCall_1_t2265 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1__ctor_m11441_gshared)(__this, ___target, ___theFunction, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.String>::.ctor(UnityEngine.Events.UnityAction`1<T1>)
#define InvokableCall_1__ctor_m16886(__this, ___callback, method) (( void (*) (InvokableCall_1_t2265 *, UnityAction_1_t1992 *, const MethodInfo*))InvokableCall_1__ctor_m11442_gshared)(__this, ___callback, method)
// System.Void UnityEngine.Events.InvokableCall`1<System.String>::Invoke(System.Object[])
#define InvokableCall_1_Invoke_m16887(__this, ___args, method) (( void (*) (InvokableCall_1_t2265 *, ObjectU5BU5D_t60*, const MethodInfo*))InvokableCall_1_Invoke_m11443_gshared)(__this, ___args, method)
// System.Boolean UnityEngine.Events.InvokableCall`1<System.String>::Find(System.Object,System.Reflection.MethodInfo)
#define InvokableCall_1_Find_m16888(__this, ___targetObj, ___method, method) (( bool (*) (InvokableCall_1_t2265 *, Object_t *, MethodInfo_t *, const MethodInfo*))InvokableCall_1_Find_m11444_gshared)(__this, ___targetObj, ___method, method)
