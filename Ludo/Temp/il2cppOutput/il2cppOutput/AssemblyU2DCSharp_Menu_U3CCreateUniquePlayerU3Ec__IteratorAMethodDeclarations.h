﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Menu/<CreateUniquePlayer>c__IteratorA
struct U3CCreateUniquePlayerU3Ec__IteratorA_t31;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Menu/<CreateUniquePlayer>c__IteratorA::.ctor()
extern "C" void U3CCreateUniquePlayerU3Ec__IteratorA__ctor_m110 (U3CCreateUniquePlayerU3Ec__IteratorA_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Menu/<CreateUniquePlayer>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CCreateUniquePlayerU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m111 (U3CCreateUniquePlayerU3Ec__IteratorA_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Menu/<CreateUniquePlayer>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCreateUniquePlayerU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m112 (U3CCreateUniquePlayerU3Ec__IteratorA_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Menu/<CreateUniquePlayer>c__IteratorA::MoveNext()
extern "C" bool U3CCreateUniquePlayerU3Ec__IteratorA_MoveNext_m113 (U3CCreateUniquePlayerU3Ec__IteratorA_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Menu/<CreateUniquePlayer>c__IteratorA::Dispose()
extern "C" void U3CCreateUniquePlayerU3Ec__IteratorA_Dispose_m114 (U3CCreateUniquePlayerU3Ec__IteratorA_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Menu/<CreateUniquePlayer>c__IteratorA::Reset()
extern "C" void U3CCreateUniquePlayerU3Ec__IteratorA_Reset_m115 (U3CCreateUniquePlayerU3Ec__IteratorA_t31 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
