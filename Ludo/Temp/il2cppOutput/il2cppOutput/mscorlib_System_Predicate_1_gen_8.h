﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.EventSystems.BaseRaycaster
struct BaseRaycaster_t104;
// System.IAsyncResult
struct IAsyncResult_t180;
// System.AsyncCallback
struct AsyncCallback_t181;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"

// System.Predicate`1<UnityEngine.EventSystems.BaseRaycaster>
struct  Predicate_1_t1849  : public MulticastDelegate_t179
{
};
