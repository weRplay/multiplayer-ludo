﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t1932;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_6.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m12479_gshared (Enumerator_t1937 * __this, Dictionary_2_t1932 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m12479(__this, ___host, method) (( void (*) (Enumerator_t1937 *, Dictionary_2_t1932 *, const MethodInfo*))Enumerator__ctor_m12479_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m12480_gshared (Enumerator_t1937 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m12480(__this, method) (( Object_t * (*) (Enumerator_t1937 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12480_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m12481_gshared (Enumerator_t1937 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m12481(__this, method) (( void (*) (Enumerator_t1937 *, const MethodInfo*))Enumerator_Dispose_m12481_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m12482_gshared (Enumerator_t1937 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m12482(__this, method) (( bool (*) (Enumerator_t1937 *, const MethodInfo*))Enumerator_MoveNext_m12482_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Object,System.Object>::get_Current()
extern "C" Object_t * Enumerator_get_Current_m12483_gshared (Enumerator_t1937 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m12483(__this, method) (( Object_t * (*) (Enumerator_t1937 *, const MethodInfo*))Enumerator_get_Current_m12483_gshared)(__this, method)
