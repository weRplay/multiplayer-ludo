﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m15604_gshared (KeyValuePair_2_t2155 * __this, uint64_t ___key, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m15604(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2155 *, uint64_t, Object_t *, const MethodInfo*))KeyValuePair_2__ctor_m15604_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Key()
extern "C" uint64_t KeyValuePair_2_get_Key_m15605_gshared (KeyValuePair_2_t2155 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m15605(__this, method) (( uint64_t (*) (KeyValuePair_2_t2155 *, const MethodInfo*))KeyValuePair_2_get_Key_m15605_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m15606_gshared (KeyValuePair_2_t2155 * __this, uint64_t ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m15606(__this, ___value, method) (( void (*) (KeyValuePair_2_t2155 *, uint64_t, const MethodInfo*))KeyValuePair_2_set_Key_m15606_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Value()
extern "C" Object_t * KeyValuePair_2_get_Value_m15607_gshared (KeyValuePair_2_t2155 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m15607(__this, method) (( Object_t * (*) (KeyValuePair_2_t2155 *, const MethodInfo*))KeyValuePair_2_get_Value_m15607_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m15608_gshared (KeyValuePair_2_t2155 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m15608(__this, ___value, method) (( void (*) (KeyValuePair_2_t2155 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Value_m15608_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,System.Object>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m15609_gshared (KeyValuePair_2_t2155 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m15609(__this, method) (( String_t* (*) (KeyValuePair_2_t2155 *, const MethodInfo*))KeyValuePair_2_ToString_m15609_gshared)(__this, method)
