﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_1MethodDeclarations.h"

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::.ctor()
#define IndexedSet_1__ctor_m1682(__this, method) (( void (*) (IndexedSet_1_t324 *, const MethodInfo*))IndexedSet_1__ctor_m12042_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m13086(__this, method) (( Object_t * (*) (IndexedSet_1_t324 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m12044_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Add(T)
#define IndexedSet_1_Add_m13087(__this, ___item, method) (( void (*) (IndexedSet_1_t324 *, Graphic_t145 *, const MethodInfo*))IndexedSet_1_Add_m12046_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Remove(T)
#define IndexedSet_1_Remove_m13088(__this, ___item, method) (( bool (*) (IndexedSet_1_t324 *, Graphic_t145 *, const MethodInfo*))IndexedSet_1_Remove_m12048_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m13089(__this, method) (( Object_t* (*) (IndexedSet_1_t324 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m12050_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Clear()
#define IndexedSet_1_Clear_m13090(__this, method) (( void (*) (IndexedSet_1_t324 *, const MethodInfo*))IndexedSet_1_Clear_m12052_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Contains(T)
#define IndexedSet_1_Contains_m13091(__this, ___item, method) (( bool (*) (IndexedSet_1_t324 *, Graphic_t145 *, const MethodInfo*))IndexedSet_1_Contains_m12054_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m13092(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t324 *, GraphicU5BU5D_t1967*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m12056_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Count()
#define IndexedSet_1_get_Count_m13093(__this, method) (( int32_t (*) (IndexedSet_1_t324 *, const MethodInfo*))IndexedSet_1_get_Count_m12058_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m13094(__this, method) (( bool (*) (IndexedSet_1_t324 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m12060_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::IndexOf(T)
#define IndexedSet_1_IndexOf_m13095(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t324 *, Graphic_t145 *, const MethodInfo*))IndexedSet_1_IndexOf_m12062_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m13096(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t324 *, int32_t, Graphic_t145 *, const MethodInfo*))IndexedSet_1_Insert_m12064_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m13097(__this, ___index, method) (( void (*) (IndexedSet_1_t324 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m12066_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m13098(__this, ___index, method) (( Graphic_t145 * (*) (IndexedSet_1_t324 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m12068_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m13099(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t324 *, int32_t, Graphic_t145 *, const MethodInfo*))IndexedSet_1_set_Item_m12070_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m13100(__this, ___match, method) (( void (*) (IndexedSet_1_t324 *, Predicate_1_t1969 *, const MethodInfo*))IndexedSet_1_RemoveAll_m12071_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m13101(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t324 *, Comparison_1_t157 *, const MethodInfo*))IndexedSet_1_Sort_m12072_gshared)(__this, ___sortLayoutFunction, method)
