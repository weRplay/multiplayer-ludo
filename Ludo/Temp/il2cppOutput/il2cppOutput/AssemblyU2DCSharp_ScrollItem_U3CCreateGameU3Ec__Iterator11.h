﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t8;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t9;
// System.String[]
struct StringU5BU5D_t16;
// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// ScrollItem/<CreateGame>c__Iterator11
struct  U3CCreateGameU3Ec__Iterator11_t45  : public Object_t
{
	// UnityEngine.WWWForm ScrollItem/<CreateGame>c__Iterator11::<form>__0
	WWWForm_t8 * ___U3CformU3E__0_0;
	// System.String ScrollItem/<CreateGame>c__Iterator11::player2Name
	String_t* ___player2Name_1;
	// UnityEngine.WWW ScrollItem/<CreateGame>c__Iterator11::<w>__1
	WWW_t9 * ___U3CwU3E__1_2;
	// System.String ScrollItem/<CreateGame>c__Iterator11::<JsonString>__2
	String_t* ___U3CJsonStringU3E__2_3;
	// System.String[] ScrollItem/<CreateGame>c__Iterator11::<array>__3
	StringU5BU5D_t16* ___U3CarrayU3E__3_4;
	// System.Int32 ScrollItem/<CreateGame>c__Iterator11::$PC
	int32_t ___U24PC_5;
	// System.Object ScrollItem/<CreateGame>c__Iterator11::$current
	Object_t * ___U24current_6;
	// System.String ScrollItem/<CreateGame>c__Iterator11::<$>player2Name
	String_t* ___U3CU24U3Eplayer2Name_7;
};
