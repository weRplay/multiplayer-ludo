﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// BoardManager
struct BoardManager_t1;
// Player
struct Player_t11;
// PlayerTwo
struct PlayerTwo_t12;
// CountdownTimer
struct CountdownTimer_t10;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t4;
// UnityEngine.GameObject
struct GameObject_t5;
// UnityEngine.UI.Slider
struct Slider_t26;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// GameManager
struct  GameManager_t22  : public MonoBehaviour_t2
{
	// System.Single GameManager::turnDelay
	float ___turnDelay_12;
	// System.Boolean GameManager::delayBool
	bool ___delayBool_13;
	// UnityEngine.GameObject[] GameManager::dices
	GameObjectU5BU5D_t4* ___dices_14;
	// UnityEngine.GameObject GameManager::rollDiceButton
	GameObject_t5 * ___rollDiceButton_18;
	// UnityEngine.GameObject GameManager::playerMoveButton
	GameObject_t5 * ___playerMoveButton_20;
	// UnityEngine.UI.Slider GameManager::testslider
	Slider_t26 * ___testslider_23;
};
struct GameManager_t22_StaticFields{
	// BoardManager GameManager::boardScript
	BoardManager_t1 * ___boardScript_2;
	// Player GameManager::playerScript
	Player_t11 * ___playerScript_3;
	// PlayerTwo GameManager::playerTwoScript
	PlayerTwo_t12 * ___playerTwoScript_4;
	// CountdownTimer GameManager::countdownTimerScript
	CountdownTimer_t10 * ___countdownTimerScript_5;
	// System.Boolean GameManager::playersTurn
	bool ___playersTurn_6;
	// System.Boolean GameManager::callCoroutineBool
	bool ___callCoroutineBool_7;
	// System.Boolean GameManager::gameOverBool
	bool ___gameOverBool_8;
	// System.Boolean GameManager::ShowButtonBool
	bool ___ShowButtonBool_9;
	// System.Boolean GameManager::MoveForward
	bool ___MoveForward_10;
	// System.Boolean GameManager::MoveDice
	bool ___MoveDice_11;
	// UnityEngine.GameObject GameManager::diceInstance
	GameObject_t5 * ___diceInstance_15;
	// UnityEngine.GameObject GameManager::diceInstanceB
	GameObject_t5 * ___diceInstanceB_16;
	// UnityEngine.GameObject GameManager::resetButton
	GameObject_t5 * ___resetButton_17;
	// UnityEngine.GameObject GameManager::turnText
	GameObject_t5 * ___turnText_19;
	// System.Int32 GameManager::diceNumber
	int32_t ___diceNumber_21;
	// System.Int32 GameManager::playerPosition
	int32_t ___playerPosition_22;
};
