﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1875;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m11810_gshared (Enumerator_t1882 * __this, Dictionary_2_t1875 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m11810(__this, ___dictionary, method) (( void (*) (Enumerator_t1882 *, Dictionary_2_t1875 *, const MethodInfo*))Enumerator__ctor_m11810_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m11811_gshared (Enumerator_t1882 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m11811(__this, method) (( Object_t * (*) (Enumerator_t1882 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11811_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1068  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11812_gshared (Enumerator_t1882 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11812(__this, method) (( DictionaryEntry_t1068  (*) (Enumerator_t1882 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11812_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11813_gshared (Enumerator_t1882 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11813(__this, method) (( Object_t * (*) (Enumerator_t1882 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11813_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11814_gshared (Enumerator_t1882 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11814(__this, method) (( Object_t * (*) (Enumerator_t1882 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11814_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m11815_gshared (Enumerator_t1882 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m11815(__this, method) (( bool (*) (Enumerator_t1882 *, const MethodInfo*))Enumerator_MoveNext_m11815_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" KeyValuePair_2_t1877  Enumerator_get_Current_m11816_gshared (Enumerator_t1882 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m11816(__this, method) (( KeyValuePair_2_t1877  (*) (Enumerator_t1882 *, const MethodInfo*))Enumerator_get_Current_m11816_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentKey()
extern "C" int32_t Enumerator_get_CurrentKey_m11817_gshared (Enumerator_t1882 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m11817(__this, method) (( int32_t (*) (Enumerator_t1882 *, const MethodInfo*))Enumerator_get_CurrentKey_m11817_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m11818_gshared (Enumerator_t1882 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m11818(__this, method) (( Object_t * (*) (Enumerator_t1882 *, const MethodInfo*))Enumerator_get_CurrentValue_m11818_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m11819_gshared (Enumerator_t1882 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m11819(__this, method) (( void (*) (Enumerator_t1882 *, const MethodInfo*))Enumerator_VerifyState_m11819_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m11820_gshared (Enumerator_t1882 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m11820(__this, method) (( void (*) (Enumerator_t1882 *, const MethodInfo*))Enumerator_VerifyCurrent_m11820_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m11821_gshared (Enumerator_t1882 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m11821(__this, method) (( void (*) (Enumerator_t1882 *, const MethodInfo*))Enumerator_Dispose_m11821_gshared)(__this, method)
