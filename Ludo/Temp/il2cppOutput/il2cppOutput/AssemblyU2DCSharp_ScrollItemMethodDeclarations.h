﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// ScrollItem
struct ScrollItem_t46;
// System.Collections.IEnumerator
struct IEnumerator_t51;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void ScrollItem::.ctor()
extern "C" void ScrollItem__ctor_m184 (ScrollItem_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollItem::OnEnable()
extern "C" void ScrollItem_OnEnable_m185 (ScrollItem_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator ScrollItem::CreateGame(System.String)
extern "C" Object_t * ScrollItem_CreateGame_m186 (ScrollItem_t46 * __this, String_t* ___player2Name, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void ScrollItem::OnRemoveMe()
extern "C" void ScrollItem_OnRemoveMe_m187 (ScrollItem_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
