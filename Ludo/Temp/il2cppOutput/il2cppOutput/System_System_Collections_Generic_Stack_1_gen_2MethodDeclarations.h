﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor()
#define Stack_1__ctor_m12685(__this, method) (( void (*) (Stack_1_t1949 *, const MethodInfo*))Stack_1__ctor_m10975_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m12686(__this, method) (( bool (*) (Stack_1_t1949 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m10976_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m12687(__this, method) (( Object_t * (*) (Stack_1_t1949 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m10977_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m12688(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t1949 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m10978_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12689(__this, method) (( Object_t* (*) (Stack_1_t1949 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m10979_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m12690(__this, method) (( Object_t * (*) (Stack_1_t1949 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m10980_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Peek()
#define Stack_1_Peek_m12691(__this, method) (( List_1_t187 * (*) (Stack_1_t1949 *, const MethodInfo*))Stack_1_Peek_m10981_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Pop()
#define Stack_1_Pop_m12692(__this, method) (( List_1_t187 * (*) (Stack_1_t1949 *, const MethodInfo*))Stack_1_Pop_m10982_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Push(T)
#define Stack_1_Push_m12693(__this, ___t, method) (( void (*) (Stack_1_t1949 *, List_1_t187 *, const MethodInfo*))Stack_1_Push_m10983_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::get_Count()
#define Stack_1_get_Count_m12694(__this, method) (( int32_t (*) (Stack_1_t1949 *, const MethodInfo*))Stack_1_get_Count_m10984_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::GetEnumerator()
#define Stack_1_GetEnumerator_m12695(__this, method) (( Enumerator_t2418  (*) (Stack_1_t1949 *, const MethodInfo*))Stack_1_GetEnumerator_m10985_gshared)(__this, method)
