﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t5;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// Loader
struct  Loader_t30  : public MonoBehaviour_t2
{
	// UnityEngine.GameObject Loader::gameManager
	GameObject_t5 * ___gameManager_2;
};
