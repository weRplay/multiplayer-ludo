﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Selectable
struct Selectable_t134;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode.h"

// UnityEngine.UI.Navigation/Mode UnityEngine.UI.Navigation::get_mode()
extern "C" int32_t Navigation_get_mode_m923 (Navigation_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_mode(UnityEngine.UI.Navigation/Mode)
extern "C" void Navigation_set_mode_m924 (Navigation_t194 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnUp()
extern "C" Selectable_t134 * Navigation_get_selectOnUp_m925 (Navigation_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnUp(UnityEngine.UI.Selectable)
extern "C" void Navigation_set_selectOnUp_m926 (Navigation_t194 * __this, Selectable_t134 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnDown()
extern "C" Selectable_t134 * Navigation_get_selectOnDown_m927 (Navigation_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnDown(UnityEngine.UI.Selectable)
extern "C" void Navigation_set_selectOnDown_m928 (Navigation_t194 * __this, Selectable_t134 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnLeft()
extern "C" Selectable_t134 * Navigation_get_selectOnLeft_m929 (Navigation_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnLeft(UnityEngine.UI.Selectable)
extern "C" void Navigation_set_selectOnLeft_m930 (Navigation_t194 * __this, Selectable_t134 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Selectable UnityEngine.UI.Navigation::get_selectOnRight()
extern "C" Selectable_t134 * Navigation_get_selectOnRight_m931 (Navigation_t194 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Navigation::set_selectOnRight(UnityEngine.UI.Selectable)
extern "C" void Navigation_set_selectOnRight_m932 (Navigation_t194 * __this, Selectable_t134 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Navigation UnityEngine.UI.Navigation::get_defaultNavigation()
extern "C" Navigation_t194  Navigation_get_defaultNavigation_m933 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
