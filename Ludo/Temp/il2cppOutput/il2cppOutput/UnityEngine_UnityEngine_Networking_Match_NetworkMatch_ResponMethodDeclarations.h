﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Networking_Match_NetworkMatch_Respon_3MethodDeclarations.h"

// System.Void UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.CreateMatchResponse>::.ctor(System.Object,System.IntPtr)
#define ResponseDelegate_1__ctor_m15745(__this, ___object, ___method, method) (( void (*) (ResponseDelegate_1_t614 *, Object_t *, IntPtr_t, const MethodInfo*))ResponseDelegate_1__ctor_m15746_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.CreateMatchResponse>::Invoke(T)
#define ResponseDelegate_1_Invoke_m15747(__this, ___response, method) (( void (*) (ResponseDelegate_1_t614 *, CreateMatchResponse_t489 *, const MethodInfo*))ResponseDelegate_1_Invoke_m15748_gshared)(__this, ___response, method)
// System.IAsyncResult UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.CreateMatchResponse>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define ResponseDelegate_1_BeginInvoke_m15749(__this, ___response, ___callback, ___object, method) (( Object_t * (*) (ResponseDelegate_1_t614 *, CreateMatchResponse_t489 *, AsyncCallback_t181 *, Object_t *, const MethodInfo*))ResponseDelegate_1_BeginInvoke_m15750_gshared)(__this, ___response, ___callback, ___object, method)
// System.Void UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.CreateMatchResponse>::EndInvoke(System.IAsyncResult)
#define ResponseDelegate_1_EndInvoke_m15751(__this, ___result, method) (( void (*) (ResponseDelegate_1_t614 *, Object_t *, const MethodInfo*))ResponseDelegate_1_EndInvoke_m15752_gshared)(__this, ___result, method)
