﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m14266(__this, ___dictionary, method) (( void (*) (Enumerator_t2066 *, Dictionary_2_t413 *, const MethodInfo*))Enumerator__ctor_m12484_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14267(__this, method) (( Object_t * (*) (Enumerator_t2066 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12485_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14268(__this, method) (( DictionaryEntry_t1068  (*) (Enumerator_t2066 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m12486_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14269(__this, method) (( Object_t * (*) (Enumerator_t2066 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m12487_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14270(__this, method) (( Object_t * (*) (Enumerator_t2066 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m12488_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m14271(__this, method) (( bool (*) (Enumerator_t2066 *, const MethodInfo*))Enumerator_MoveNext_m12489_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m14272(__this, method) (( KeyValuePair_2_t2064  (*) (Enumerator_t2066 *, const MethodInfo*))Enumerator_get_Current_m12490_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m14273(__this, method) (( String_t* (*) (Enumerator_t2066 *, const MethodInfo*))Enumerator_get_CurrentKey_m12491_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m14274(__this, method) (( GUIStyle_t402 * (*) (Enumerator_t2066 *, const MethodInfo*))Enumerator_get_CurrentValue_m12492_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyState()
#define Enumerator_VerifyState_m14275(__this, method) (( void (*) (Enumerator_t2066 *, const MethodInfo*))Enumerator_VerifyState_m12493_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m14276(__this, method) (( void (*) (Enumerator_t2066 *, const MethodInfo*))Enumerator_VerifyCurrent_m12494_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m14277(__this, method) (( void (*) (Enumerator_t2066 *, const MethodInfo*))Enumerator_Dispose_m12495_gshared)(__this, method)
