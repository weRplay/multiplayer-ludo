﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void U24ArrayTypeU241024_t1720_marshal(const U24ArrayTypeU241024_t1720& unmarshaled, U24ArrayTypeU241024_t1720_marshaled& marshaled);
extern "C" void U24ArrayTypeU241024_t1720_marshal_back(const U24ArrayTypeU241024_t1720_marshaled& marshaled, U24ArrayTypeU241024_t1720& unmarshaled);
extern "C" void U24ArrayTypeU241024_t1720_marshal_cleanup(U24ArrayTypeU241024_t1720_marshaled& marshaled);
