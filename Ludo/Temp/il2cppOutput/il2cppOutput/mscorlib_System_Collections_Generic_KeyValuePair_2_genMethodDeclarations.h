﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m11883(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t304 *, int32_t, PointerEventData_t108 *, const MethodInfo*))KeyValuePair_2__ctor_m11780_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Key()
#define KeyValuePair_2_get_Key_m1543(__this, method) (( int32_t (*) (KeyValuePair_2_t304 *, const MethodInfo*))KeyValuePair_2_get_Key_m11781_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m11884(__this, ___value, method) (( void (*) (KeyValuePair_2_t304 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m11782_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Value()
#define KeyValuePair_2_get_Value_m1542(__this, method) (( PointerEventData_t108 * (*) (KeyValuePair_2_t304 *, const MethodInfo*))KeyValuePair_2_get_Value_m11783_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m11885(__this, ___value, method) (( void (*) (KeyValuePair_2_t304 *, PointerEventData_t108 *, const MethodInfo*))KeyValuePair_2_set_Value_m11784_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.EventSystems.PointerEventData>::ToString()
#define KeyValuePair_2_ToString_m1570(__this, method) (( String_t* (*) (KeyValuePair_2_t304 *, const MethodInfo*))KeyValuePair_2_ToString_m11785_gshared)(__this, method)
