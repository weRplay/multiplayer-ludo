﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// System.EventArgs
struct EventArgs_t759;
// System.IAsyncResult
struct IAsyncResult_t180;
// System.AsyncCallback
struct AsyncCallback_t181;

#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Void.h"

// System.EventHandler
struct  EventHandler_t1629  : public MulticastDelegate_t179
{
};
