﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_35.h"

// System.Void System.Array/InternalEnumerator`1<System.Single>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m14771_gshared (InternalEnumerator_1_t2095 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m14771(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2095 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14771_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Single>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14772_gshared (InternalEnumerator_1_t2095 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14772(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2095 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14772_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Single>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m14773_gshared (InternalEnumerator_1_t2095 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m14773(__this, method) (( void (*) (InternalEnumerator_1_t2095 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14773_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Single>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m14774_gshared (InternalEnumerator_1_t2095 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m14774(__this, method) (( bool (*) (InternalEnumerator_1_t2095 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14774_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Single>::get_Current()
extern "C" float InternalEnumerator_1_get_Current_m14775_gshared (InternalEnumerator_1_t2095 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m14775(__this, method) (( float (*) (InternalEnumerator_1_t2095 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14775_gshared)(__this, method)
