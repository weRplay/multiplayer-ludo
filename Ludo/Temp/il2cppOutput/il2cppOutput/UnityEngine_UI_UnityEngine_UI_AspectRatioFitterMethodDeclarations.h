﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.AspectRatioFitter
struct AspectRatioFitter_t230;
// UnityEngine.RectTransform
struct RectTransform_t18;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void UnityEngine.UI.AspectRatioFitter::.ctor()
extern "C" void AspectRatioFitter__ctor_m1240 (AspectRatioFitter_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.AspectRatioFitter/AspectMode UnityEngine.UI.AspectRatioFitter::get_aspectMode()
extern "C" int32_t AspectRatioFitter_get_aspectMode_m1241 (AspectRatioFitter_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectMode(UnityEngine.UI.AspectRatioFitter/AspectMode)
extern "C" void AspectRatioFitter_set_aspectMode_m1242 (AspectRatioFitter_t230 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.AspectRatioFitter::get_aspectRatio()
extern "C" float AspectRatioFitter_get_aspectRatio_m1243 (AspectRatioFitter_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::set_aspectRatio(System.Single)
extern "C" void AspectRatioFitter_set_aspectRatio_m1244 (AspectRatioFitter_t230 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UnityEngine.UI.AspectRatioFitter::get_rectTransform()
extern "C" RectTransform_t18 * AspectRatioFitter_get_rectTransform_m1245 (AspectRatioFitter_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::OnEnable()
extern "C" void AspectRatioFitter_OnEnable_m1246 (AspectRatioFitter_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::OnDisable()
extern "C" void AspectRatioFitter_OnDisable_m1247 (AspectRatioFitter_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::OnRectTransformDimensionsChange()
extern "C" void AspectRatioFitter_OnRectTransformDimensionsChange_m1248 (AspectRatioFitter_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::UpdateRect()
extern "C" void AspectRatioFitter_UpdateRect_m1249 (AspectRatioFitter_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.AspectRatioFitter::GetSizeDeltaToProduceSize(System.Single,System.Int32)
extern "C" float AspectRatioFitter_GetSizeDeltaToProduceSize_m1250 (AspectRatioFitter_t230 * __this, float ___size, int32_t ___axis, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Vector2 UnityEngine.UI.AspectRatioFitter::GetParentSize()
extern "C" Vector2_t59  AspectRatioFitter_GetParentSize_m1251 (AspectRatioFitter_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutHorizontal()
extern "C" void AspectRatioFitter_SetLayoutHorizontal_m1252 (AspectRatioFitter_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::SetLayoutVertical()
extern "C" void AspectRatioFitter_SetLayoutVertical_m1253 (AspectRatioFitter_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.AspectRatioFitter::SetDirty()
extern "C" void AspectRatioFitter_SetDirty_m1254 (AspectRatioFitter_t230 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
