﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m14233(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2064 *, String_t*, GUIStyle_t402 *, const MethodInfo*))KeyValuePair_2__ctor_m12459_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Key()
#define KeyValuePair_2_get_Key_m14234(__this, method) (( String_t* (*) (KeyValuePair_2_t2064 *, const MethodInfo*))KeyValuePair_2_get_Key_m12460_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m14235(__this, ___value, method) (( void (*) (KeyValuePair_2_t2064 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m12461_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Value()
#define KeyValuePair_2_get_Value_m14236(__this, method) (( GUIStyle_t402 * (*) (KeyValuePair_2_t2064 *, const MethodInfo*))KeyValuePair_2_get_Value_m12462_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m14237(__this, ___value, method) (( void (*) (KeyValuePair_2_t2064 *, GUIStyle_t402 *, const MethodInfo*))KeyValuePair_2_set_Value_m12463_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::ToString()
#define KeyValuePair_2_ToString_m14238(__this, method) (( String_t* (*) (KeyValuePair_2_t2064 *, const MethodInfo*))KeyValuePair_2_ToString_m12464_gshared)(__this, method)
