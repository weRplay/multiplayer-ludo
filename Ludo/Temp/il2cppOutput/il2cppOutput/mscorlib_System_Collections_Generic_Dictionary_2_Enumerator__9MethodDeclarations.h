﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m14075(__this, ___dictionary, method) (( void (*) (Enumerator_t2055 *, Dictionary_2_t401 *, const MethodInfo*))Enumerator__ctor_m11810_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14076(__this, method) (( Object_t * (*) (Enumerator_t2055 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11811_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14077(__this, method) (( DictionaryEntry_t1068  (*) (Enumerator_t2055 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11812_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14078(__this, method) (( Object_t * (*) (Enumerator_t2055 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11813_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14079(__this, method) (( Object_t * (*) (Enumerator_t2055 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11814_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::MoveNext()
#define Enumerator_MoveNext_m14080(__this, method) (( bool (*) (Enumerator_t2055 *, const MethodInfo*))Enumerator_MoveNext_m11815_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Current()
#define Enumerator_get_Current_m14081(__this, method) (( KeyValuePair_2_t2052  (*) (Enumerator_t2055 *, const MethodInfo*))Enumerator_get_Current_m11816_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m14082(__this, method) (( int32_t (*) (Enumerator_t2055 *, const MethodInfo*))Enumerator_get_CurrentKey_m11817_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m14083(__this, method) (( LayoutCache_t398 * (*) (Enumerator_t2055 *, const MethodInfo*))Enumerator_get_CurrentValue_m11818_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::VerifyState()
#define Enumerator_VerifyState_m14084(__this, method) (( void (*) (Enumerator_t2055 *, const MethodInfo*))Enumerator_VerifyState_m11819_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m14085(__this, method) (( void (*) (Enumerator_t2055 *, const MethodInfo*))Enumerator_VerifyCurrent_m11820_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::Dispose()
#define Enumerator_Dispose_m14086(__this, method) (( void (*) (Enumerator_t2055 *, const MethodInfo*))Enumerator_Dispose_m11821_gshared)(__this, method)
