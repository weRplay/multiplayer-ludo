﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_36MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m16406(__this, ___object, ___method, method) (( void (*) (Transform_1_t2198 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m16384_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m16407(__this, ___key, ___value, method) (( DictionaryEntry_t1068  (*) (Transform_1_t2198 *, String_t*, KeyValuePair_2_t678 , const MethodInfo*))Transform_1_Invoke_m16385_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m16408(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2198 *, String_t*, KeyValuePair_2_t678 , AsyncCallback_t181 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m16386_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m16409(__this, ___result, method) (( DictionaryEntry_t1068  (*) (Transform_1_t2198 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m16387_gshared)(__this, ___result, method)
