﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t6;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t2392;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t51;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.Vector3>
struct ICollection_1_t2393;
// System.Collections.Generic.IEnumerable`1<UnityEngine.Vector3>
struct IEnumerable_1_t2394;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.Vector3>
struct ReadOnlyCollection_1_t1786;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t208;
// System.Predicate`1<UnityEngine.Vector3>
struct Predicate_1_t1792;
// System.Comparison`1<UnityEngine.Vector3>
struct Comparison_1_t1798;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor()
extern "C" void List_1__ctor_m10452_gshared (List_1_t6 * __this, const MethodInfo* method);
#define List_1__ctor_m10452(__this, method) (( void (*) (List_1_t6 *, const MethodInfo*))List_1__ctor_m10452_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.ctor(System.Int32)
extern "C" void List_1__ctor_m10453_gshared (List_1_t6 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m10453(__this, ___capacity, method) (( void (*) (List_1_t6 *, int32_t, const MethodInfo*))List_1__ctor_m10453_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::.cctor()
extern "C" void List_1__cctor_m10454_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m10454(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m10454_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m10455_gshared (List_1_t6 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m10455(__this, method) (( Object_t* (*) (List_1_t6 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m10455_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m10456_gshared (List_1_t6 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m10456(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t6 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m10456_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m10457_gshared (List_1_t6 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m10457(__this, method) (( Object_t * (*) (List_1_t6 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m10457_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m10458_gshared (List_1_t6 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m10458(__this, ___item, method) (( int32_t (*) (List_1_t6 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m10458_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m10459_gshared (List_1_t6 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m10459(__this, ___item, method) (( bool (*) (List_1_t6 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m10459_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m10460_gshared (List_1_t6 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m10460(__this, ___item, method) (( int32_t (*) (List_1_t6 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m10460_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m10461_gshared (List_1_t6 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m10461(__this, ___index, ___item, method) (( void (*) (List_1_t6 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m10461_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m10462_gshared (List_1_t6 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m10462(__this, ___item, method) (( void (*) (List_1_t6 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m10462_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m10463_gshared (List_1_t6 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m10463(__this, method) (( bool (*) (List_1_t6 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m10463_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m10464_gshared (List_1_t6 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m10464(__this, method) (( bool (*) (List_1_t6 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m10464_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m10465_gshared (List_1_t6 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m10465(__this, method) (( Object_t * (*) (List_1_t6 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m10465_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m10466_gshared (List_1_t6 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m10466(__this, method) (( bool (*) (List_1_t6 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m10466_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m10467_gshared (List_1_t6 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m10467(__this, method) (( bool (*) (List_1_t6 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m10467_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m10468_gshared (List_1_t6 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m10468(__this, ___index, method) (( Object_t * (*) (List_1_t6 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m10468_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m10469_gshared (List_1_t6 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m10469(__this, ___index, ___value, method) (( void (*) (List_1_t6 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m10469_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(T)
extern "C" void List_1_Add_m10470_gshared (List_1_t6 * __this, Vector3_t35  ___item, const MethodInfo* method);
#define List_1_Add_m10470(__this, ___item, method) (( void (*) (List_1_t6 *, Vector3_t35 , const MethodInfo*))List_1_Add_m10470_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m10471_gshared (List_1_t6 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m10471(__this, ___newCount, method) (( void (*) (List_1_t6 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m10471_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m10472_gshared (List_1_t6 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m10472(__this, ___collection, method) (( void (*) (List_1_t6 *, Object_t*, const MethodInfo*))List_1_AddCollection_m10472_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m10473_gshared (List_1_t6 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m10473(__this, ___enumerable, method) (( void (*) (List_1_t6 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m10473_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m10474_gshared (List_1_t6 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m10474(__this, ___collection, method) (( void (*) (List_1_t6 *, Object_t*, const MethodInfo*))List_1_AddRange_m10474_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1786 * List_1_AsReadOnly_m10475_gshared (List_1_t6 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m10475(__this, method) (( ReadOnlyCollection_1_t1786 * (*) (List_1_t6 *, const MethodInfo*))List_1_AsReadOnly_m10475_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Clear()
extern "C" void List_1_Clear_m10476_gshared (List_1_t6 * __this, const MethodInfo* method);
#define List_1_Clear_m10476(__this, method) (( void (*) (List_1_t6 *, const MethodInfo*))List_1_Clear_m10476_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::Contains(T)
extern "C" bool List_1_Contains_m10477_gshared (List_1_t6 * __this, Vector3_t35  ___item, const MethodInfo* method);
#define List_1_Contains_m10477(__this, ___item, method) (( bool (*) (List_1_t6 *, Vector3_t35 , const MethodInfo*))List_1_Contains_m10477_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m10478_gshared (List_1_t6 * __this, Vector3U5BU5D_t208* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m10478(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t6 *, Vector3U5BU5D_t208*, int32_t, const MethodInfo*))List_1_CopyTo_m10478_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector3>::Find(System.Predicate`1<T>)
extern "C" Vector3_t35  List_1_Find_m10479_gshared (List_1_t6 * __this, Predicate_1_t1792 * ___match, const MethodInfo* method);
#define List_1_Find_m10479(__this, ___match, method) (( Vector3_t35  (*) (List_1_t6 *, Predicate_1_t1792 *, const MethodInfo*))List_1_Find_m10479_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m10480_gshared (Object_t * __this /* static, unused */, Predicate_1_t1792 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m10480(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1792 *, const MethodInfo*))List_1_CheckMatch_m10480_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m10481_gshared (List_1_t6 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1792 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m10481(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t6 *, int32_t, int32_t, Predicate_1_t1792 *, const MethodInfo*))List_1_GetIndex_m10481_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Vector3>::GetEnumerator()
extern "C" Enumerator_t1785  List_1_GetEnumerator_m10482_gshared (List_1_t6 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m10482(__this, method) (( Enumerator_t1785  (*) (List_1_t6 *, const MethodInfo*))List_1_GetEnumerator_m10482_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m10483_gshared (List_1_t6 * __this, Vector3_t35  ___item, const MethodInfo* method);
#define List_1_IndexOf_m10483(__this, ___item, method) (( int32_t (*) (List_1_t6 *, Vector3_t35 , const MethodInfo*))List_1_IndexOf_m10483_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m10484_gshared (List_1_t6 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m10484(__this, ___start, ___delta, method) (( void (*) (List_1_t6 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m10484_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m10485_gshared (List_1_t6 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m10485(__this, ___index, method) (( void (*) (List_1_t6 *, int32_t, const MethodInfo*))List_1_CheckIndex_m10485_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m10486_gshared (List_1_t6 * __this, int32_t ___index, Vector3_t35  ___item, const MethodInfo* method);
#define List_1_Insert_m10486(__this, ___index, ___item, method) (( void (*) (List_1_t6 *, int32_t, Vector3_t35 , const MethodInfo*))List_1_Insert_m10486_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m10487_gshared (List_1_t6 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m10487(__this, ___collection, method) (( void (*) (List_1_t6 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m10487_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Vector3>::Remove(T)
extern "C" bool List_1_Remove_m10488_gshared (List_1_t6 * __this, Vector3_t35  ___item, const MethodInfo* method);
#define List_1_Remove_m10488(__this, ___item, method) (( bool (*) (List_1_t6 *, Vector3_t35 , const MethodInfo*))List_1_Remove_m10488_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m10489_gshared (List_1_t6 * __this, Predicate_1_t1792 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m10489(__this, ___match, method) (( int32_t (*) (List_1_t6 *, Predicate_1_t1792 *, const MethodInfo*))List_1_RemoveAll_m10489_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m10490_gshared (List_1_t6 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m10490(__this, ___index, method) (( void (*) (List_1_t6 *, int32_t, const MethodInfo*))List_1_RemoveAt_m10490_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Reverse()
extern "C" void List_1_Reverse_m10491_gshared (List_1_t6 * __this, const MethodInfo* method);
#define List_1_Reverse_m10491(__this, method) (( void (*) (List_1_t6 *, const MethodInfo*))List_1_Reverse_m10491_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Sort()
extern "C" void List_1_Sort_m10492_gshared (List_1_t6 * __this, const MethodInfo* method);
#define List_1_Sort_m10492(__this, method) (( void (*) (List_1_t6 *, const MethodInfo*))List_1_Sort_m10492_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m10493_gshared (List_1_t6 * __this, Comparison_1_t1798 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m10493(__this, ___comparison, method) (( void (*) (List_1_t6 *, Comparison_1_t1798 *, const MethodInfo*))List_1_Sort_m10493_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Vector3>::ToArray()
extern "C" Vector3U5BU5D_t208* List_1_ToArray_m10494_gshared (List_1_t6 * __this, const MethodInfo* method);
#define List_1_ToArray_m10494(__this, method) (( Vector3U5BU5D_t208* (*) (List_1_t6 *, const MethodInfo*))List_1_ToArray_m10494_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::TrimExcess()
extern "C" void List_1_TrimExcess_m10495_gshared (List_1_t6 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m10495(__this, method) (( void (*) (List_1_t6 *, const MethodInfo*))List_1_TrimExcess_m10495_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m10496_gshared (List_1_t6 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m10496(__this, method) (( int32_t (*) (List_1_t6 *, const MethodInfo*))List_1_get_Capacity_m10496_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m10497_gshared (List_1_t6 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m10497(__this, ___value, method) (( void (*) (List_1_t6 *, int32_t, const MethodInfo*))List_1_set_Capacity_m10497_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Count()
extern "C" int32_t List_1_get_Count_m10498_gshared (List_1_t6 * __this, const MethodInfo* method);
#define List_1_get_Count_m10498(__this, method) (( int32_t (*) (List_1_t6 *, const MethodInfo*))List_1_get_Count_m10498_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C" Vector3_t35  List_1_get_Item_m10499_gshared (List_1_t6 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m10499(__this, ___index, method) (( Vector3_t35  (*) (List_1_t6 *, int32_t, const MethodInfo*))List_1_get_Item_m10499_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m10500_gshared (List_1_t6 * __this, int32_t ___index, Vector3_t35  ___value, const MethodInfo* method);
#define List_1_set_Item_m10500(__this, ___index, ___value, method) (( void (*) (List_1_t6 *, int32_t, Vector3_t35 , const MethodInfo*))List_1_set_Item_m10500_gshared)(__this, ___index, ___value, method)
