﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2236;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__19.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_23.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m16740_gshared (Enumerator_t2243 * __this, Dictionary_2_t2236 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m16740(__this, ___dictionary, method) (( void (*) (Enumerator_t2243 *, Dictionary_2_t2236 *, const MethodInfo*))Enumerator__ctor_m16740_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16741_gshared (Enumerator_t2243 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16741(__this, method) (( Object_t * (*) (Enumerator_t2243 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16741_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1068  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16742_gshared (Enumerator_t2243 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16742(__this, method) (( DictionaryEntry_t1068  (*) (Enumerator_t2243 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16742_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16743_gshared (Enumerator_t2243 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16743(__this, method) (( Object_t * (*) (Enumerator_t2243 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16743_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16744_gshared (Enumerator_t2243 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16744(__this, method) (( Object_t * (*) (Enumerator_t2243 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16744_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16745_gshared (Enumerator_t2243 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16745(__this, method) (( bool (*) (Enumerator_t2243 *, const MethodInfo*))Enumerator_MoveNext_m16745_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" KeyValuePair_2_t2238  Enumerator_get_Current_m16746_gshared (Enumerator_t2243 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16746(__this, method) (( KeyValuePair_2_t2238  (*) (Enumerator_t2243 *, const MethodInfo*))Enumerator_get_Current_m16746_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m16747_gshared (Enumerator_t2243 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m16747(__this, method) (( Object_t * (*) (Enumerator_t2243 *, const MethodInfo*))Enumerator_get_CurrentKey_m16747_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m16748_gshared (Enumerator_t2243 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m16748(__this, method) (( int32_t (*) (Enumerator_t2243 *, const MethodInfo*))Enumerator_get_CurrentValue_m16748_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::VerifyState()
extern "C" void Enumerator_VerifyState_m16749_gshared (Enumerator_t2243 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m16749(__this, method) (( void (*) (Enumerator_t2243 *, const MethodInfo*))Enumerator_VerifyState_m16749_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m16750_gshared (Enumerator_t2243 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m16750(__this, method) (( void (*) (Enumerator_t2243 *, const MethodInfo*))Enumerator_VerifyCurrent_m16750_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void Enumerator_Dispose_m16751_gshared (Enumerator_t2243 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16751(__this, method) (( void (*) (Enumerator_t2243 *, const MethodInfo*))Enumerator_Dispose_m16751_gshared)(__this, method)
