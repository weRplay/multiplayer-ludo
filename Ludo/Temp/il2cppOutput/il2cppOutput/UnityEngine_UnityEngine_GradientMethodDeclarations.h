﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Gradient
struct Gradient_t390;
struct Gradient_t390_marshaled;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Gradient::.ctor()
extern "C" void Gradient__ctor_m2119 (Gradient_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Init()
extern "C" void Gradient_Init_m2120 (Gradient_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Cleanup()
extern "C" void Gradient_Cleanup_m2121 (Gradient_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Finalize()
extern "C" void Gradient_Finalize_m2122 (Gradient_t390 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void Gradient_t390_marshal(const Gradient_t390& unmarshaled, Gradient_t390_marshaled& marshaled);
extern "C" void Gradient_t390_marshal_back(const Gradient_t390_marshaled& marshaled, Gradient_t390& unmarshaled);
extern "C" void Gradient_t390_marshal_cleanup(Gradient_t390_marshaled& marshaled);
