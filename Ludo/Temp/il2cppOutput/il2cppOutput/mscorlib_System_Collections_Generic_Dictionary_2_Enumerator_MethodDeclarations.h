﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m11913(__this, ___dictionary, method) (( void (*) (Enumerator_t305 *, Dictionary_2_t116 *, const MethodInfo*))Enumerator__ctor_m11810_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m11914(__this, method) (( Object_t * (*) (Enumerator_t305 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11811_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11915(__this, method) (( DictionaryEntry_t1068  (*) (Enumerator_t305 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11812_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11916(__this, method) (( Object_t * (*) (Enumerator_t305 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11813_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11917(__this, method) (( Object_t * (*) (Enumerator_t305 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11814_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::MoveNext()
#define Enumerator_MoveNext_m1544(__this, method) (( bool (*) (Enumerator_t305 *, const MethodInfo*))Enumerator_MoveNext_m11815_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Current()
#define Enumerator_get_Current_m1541(__this, method) (( KeyValuePair_2_t304  (*) (Enumerator_t305 *, const MethodInfo*))Enumerator_get_Current_m11816_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m11918(__this, method) (( int32_t (*) (Enumerator_t305 *, const MethodInfo*))Enumerator_get_CurrentKey_m11817_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m11919(__this, method) (( PointerEventData_t108 * (*) (Enumerator_t305 *, const MethodInfo*))Enumerator_get_CurrentValue_m11818_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::VerifyState()
#define Enumerator_VerifyState_m11920(__this, method) (( void (*) (Enumerator_t305 *, const MethodInfo*))Enumerator_VerifyState_m11819_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m11921(__this, method) (( void (*) (Enumerator_t305 *, const MethodInfo*))Enumerator_VerifyCurrent_m11820_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::Dispose()
#define Enumerator_Dispose_m11922(__this, method) (( void (*) (Enumerator_t305 *, const MethodInfo*))Enumerator_Dispose_m11821_gshared)(__this, method)
