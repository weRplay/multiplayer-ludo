﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<UnityEngine.Networking.Types.NetworkID,System.Object>
struct Dictionary_2_t2153;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_19.h"
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m15629_gshared (Enumerator_t2159 * __this, Dictionary_2_t2153 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m15629(__this, ___host, method) (( void (*) (Enumerator_t2159 *, Dictionary_2_t2153 *, const MethodInfo*))Enumerator__ctor_m15629_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15630_gshared (Enumerator_t2159 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15630(__this, method) (( Object_t * (*) (Enumerator_t2159 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15630_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m15631_gshared (Enumerator_t2159 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15631(__this, method) (( void (*) (Enumerator_t2159 *, const MethodInfo*))Enumerator_Dispose_m15631_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15632_gshared (Enumerator_t2159 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15632(__this, method) (( bool (*) (Enumerator_t2159 *, const MethodInfo*))Enumerator_MoveNext_m15632_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<UnityEngine.Networking.Types.NetworkID,System.Object>::get_Current()
extern "C" uint64_t Enumerator_get_Current_m15633_gshared (Enumerator_t2159 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15633(__this, method) (( uint64_t (*) (Enumerator_t2159 *, const MethodInfo*))Enumerator_get_Current_m15633_gshared)(__this, method)
