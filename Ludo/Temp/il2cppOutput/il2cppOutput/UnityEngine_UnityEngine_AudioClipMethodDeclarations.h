﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AudioClip
struct AudioClip_t462;
// System.Single[]
struct SingleU5BU5D_t460;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.AudioClip::InvokePCMReaderCallback_Internal(System.Single[])
extern "C" void AudioClip_InvokePCMReaderCallback_Internal_m2684 (AudioClip_t462 * __this, SingleU5BU5D_t460* ___data, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.AudioClip::InvokePCMSetPositionCallback_Internal(System.Int32)
extern "C" void AudioClip_InvokePCMSetPositionCallback_Internal_m2685 (AudioClip_t462 * __this, int32_t ___position, const MethodInfo* method) IL2CPP_METHOD_ATTR;
