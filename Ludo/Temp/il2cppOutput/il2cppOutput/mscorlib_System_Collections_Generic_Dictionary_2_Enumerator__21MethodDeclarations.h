﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t2287;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__21.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_25.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m17245_gshared (Enumerator_t2294 * __this, Dictionary_2_t2287 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m17245(__this, ___dictionary, method) (( void (*) (Enumerator_t2294 *, Dictionary_2_t2287 *, const MethodInfo*))Enumerator__ctor_m17245_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17246_gshared (Enumerator_t2294 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17246(__this, method) (( Object_t * (*) (Enumerator_t2294 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17246_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1068  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17247_gshared (Enumerator_t2294 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17247(__this, method) (( DictionaryEntry_t1068  (*) (Enumerator_t2294 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17247_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17248_gshared (Enumerator_t2294 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17248(__this, method) (( Object_t * (*) (Enumerator_t2294 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17248_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17249_gshared (Enumerator_t2294 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17249(__this, method) (( Object_t * (*) (Enumerator_t2294 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17249_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17250_gshared (Enumerator_t2294 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17250(__this, method) (( bool (*) (Enumerator_t2294 *, const MethodInfo*))Enumerator_MoveNext_m17250_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C" KeyValuePair_2_t2289  Enumerator_get_Current_m17251_gshared (Enumerator_t2294 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17251(__this, method) (( KeyValuePair_2_t2289  (*) (Enumerator_t2294 *, const MethodInfo*))Enumerator_get_Current_m17251_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m17252_gshared (Enumerator_t2294 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m17252(__this, method) (( Object_t * (*) (Enumerator_t2294 *, const MethodInfo*))Enumerator_get_CurrentKey_m17252_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentValue()
extern "C" bool Enumerator_get_CurrentValue_m17253_gshared (Enumerator_t2294 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m17253(__this, method) (( bool (*) (Enumerator_t2294 *, const MethodInfo*))Enumerator_get_CurrentValue_m17253_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyState()
extern "C" void Enumerator_VerifyState_m17254_gshared (Enumerator_t2294 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m17254(__this, method) (( void (*) (Enumerator_t2294 *, const MethodInfo*))Enumerator_VerifyState_m17254_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m17255_gshared (Enumerator_t2294 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m17255(__this, method) (( void (*) (Enumerator_t2294 *, const MethodInfo*))Enumerator_VerifyCurrent_m17255_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C" void Enumerator_Dispose_m17256_gshared (Enumerator_t2294 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17256(__this, method) (( void (*) (Enumerator_t2294 *, const MethodInfo*))Enumerator_Dispose_m17256_gshared)(__this, method)
