﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_65.h"
#include "System_System_Text_RegularExpressions_Mark.h"

// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17511_gshared (InternalEnumerator_1_t2325 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17511(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2325 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17511_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17512_gshared (InternalEnumerator_1_t2325 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17512(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2325 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17512_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17513_gshared (InternalEnumerator_1_t2325 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17513(__this, method) (( void (*) (InternalEnumerator_1_t2325 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17513_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17514_gshared (InternalEnumerator_1_t2325 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17514(__this, method) (( bool (*) (InternalEnumerator_1_t2325 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17514_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::get_Current()
extern "C" Mark_t1019  InternalEnumerator_1_get_Current_m17515_gshared (InternalEnumerator_1_t2325 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17515(__this, method) (( Mark_t1019  (*) (InternalEnumerator_1_t2325 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17515_gshared)(__this, method)
