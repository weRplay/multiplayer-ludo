﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m13236(__this, ___dictionary, method) (( void (*) (Enumerator_t1987 *, Dictionary_2_t323 *, const MethodInfo*))Enumerator__ctor_m12153_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m13237(__this, method) (( Object_t * (*) (Enumerator_t1987 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12154_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13238(__this, method) (( DictionaryEntry_t1068  (*) (Enumerator_t1987 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m12155_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13239(__this, method) (( Object_t * (*) (Enumerator_t1987 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m12156_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13240(__this, method) (( Object_t * (*) (Enumerator_t1987 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m12157_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m13241(__this, method) (( bool (*) (Enumerator_t1987 *, const MethodInfo*))Enumerator_MoveNext_m12158_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::get_Current()
#define Enumerator_get_Current_m13242(__this, method) (( KeyValuePair_2_t1984  (*) (Enumerator_t1987 *, const MethodInfo*))Enumerator_get_Current_m12159_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m13243(__this, method) (( Object_t * (*) (Enumerator_t1987 *, const MethodInfo*))Enumerator_get_CurrentKey_m12160_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m13244(__this, method) (( int32_t (*) (Enumerator_t1987 *, const MethodInfo*))Enumerator_get_CurrentValue_m12161_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m13245(__this, method) (( void (*) (Enumerator_t1987 *, const MethodInfo*))Enumerator_VerifyState_m12162_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m13246(__this, method) (( void (*) (Enumerator_t1987 *, const MethodInfo*))Enumerator_VerifyCurrent_m12163_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::Dispose()
#define Enumerator_Dispose_m13247(__this, method) (( void (*) (Enumerator_t1987 *, const MethodInfo*))Enumerator_Dispose_m12164_gshared)(__this, method)
