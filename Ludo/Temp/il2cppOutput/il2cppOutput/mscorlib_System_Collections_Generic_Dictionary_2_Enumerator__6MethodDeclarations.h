﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m13136(__this, ___dictionary, method) (( void (*) (Enumerator_t1979 *, Dictionary_2_t159 *, const MethodInfo*))Enumerator__ctor_m12484_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m13137(__this, method) (( Object_t * (*) (Enumerator_t1979 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12485_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13138(__this, method) (( DictionaryEntry_t1068  (*) (Enumerator_t1979 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m12486_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13139(__this, method) (( Object_t * (*) (Enumerator_t1979 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m12487_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13140(__this, method) (( Object_t * (*) (Enumerator_t1979 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m12488_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::MoveNext()
#define Enumerator_MoveNext_m13141(__this, method) (( bool (*) (Enumerator_t1979 *, const MethodInfo*))Enumerator_MoveNext_m12489_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Current()
#define Enumerator_get_Current_m13142(__this, method) (( KeyValuePair_2_t1976  (*) (Enumerator_t1979 *, const MethodInfo*))Enumerator_get_Current_m12490_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m13143(__this, method) (( Canvas_t148 * (*) (Enumerator_t1979 *, const MethodInfo*))Enumerator_get_CurrentKey_m12491_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m13144(__this, method) (( IndexedSet_1_t324 * (*) (Enumerator_t1979 *, const MethodInfo*))Enumerator_get_CurrentValue_m12492_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::VerifyState()
#define Enumerator_VerifyState_m13145(__this, method) (( void (*) (Enumerator_t1979 *, const MethodInfo*))Enumerator_VerifyState_m12493_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m13146(__this, method) (( void (*) (Enumerator_t1979 *, const MethodInfo*))Enumerator_VerifyCurrent_m12494_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::Dispose()
#define Enumerator_Dispose_m13147(__this, method) (( void (*) (Enumerator_t1979 *, const MethodInfo*))Enumerator_Dispose_m12495_gshared)(__this, method)
