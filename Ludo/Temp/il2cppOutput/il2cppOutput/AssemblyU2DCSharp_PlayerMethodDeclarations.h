﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Player
struct Player_t11;
// System.Collections.IEnumerator
struct IEnumerator_t51;

#include "codegen/il2cpp-codegen.h"

// System.Void Player::.ctor()
extern "C" void Player__ctor_m150 (Player_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::Start()
extern "C" void Player_Start_m151 (Player_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::MoveThePlayer(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void Player_MoveThePlayer_m152 (Player_t11 * __this, int32_t ___x, int32_t ___y, int32_t ___a, int32_t ___b, int32_t ___playerPosition, int32_t ___diceNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::AttemptMove(System.Int32,System.Int32)
extern "C" void Player_AttemptMove_m153 (Player_t11 * __this, int32_t ___xDir, int32_t ___yDir, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Player::SendPlayer1Position(System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" Object_t * Player_SendPlayer1Position_m154 (Player_t11 * __this, int32_t ___a, int32_t ___b, int32_t ___playerPosition, int32_t ___diceNumber, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::ChangeTransform(System.Single,System.Single)
extern "C" void Player_ChangeTransform_m155 (Player_t11 * __this, float ___x, float ___y, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Player::GetPlayer1Position()
extern "C" Object_t * Player_GetPlayer1Position_m156 (Player_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player::CallExecuteAfterTime()
extern "C" void Player_CallExecuteAfterTime_m157 (Player_t11 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Player::ExecuteAfterTime(System.Single)
extern "C" Object_t * Player_ExecuteAfterTime_m158 (Player_t11 * __this, float ___time, const MethodInfo* method) IL2CPP_METHOD_ATTR;
