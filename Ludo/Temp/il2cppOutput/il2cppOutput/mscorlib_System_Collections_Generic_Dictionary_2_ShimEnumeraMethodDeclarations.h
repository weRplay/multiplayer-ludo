﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>
struct ShimEnumerator_t1889;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1875;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m11862_gshared (ShimEnumerator_t1889 * __this, Dictionary_2_t1875 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m11862(__this, ___host, method) (( void (*) (ShimEnumerator_t1889 *, Dictionary_2_t1875 *, const MethodInfo*))ShimEnumerator__ctor_m11862_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m11863_gshared (ShimEnumerator_t1889 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m11863(__this, method) (( bool (*) (ShimEnumerator_t1889 *, const MethodInfo*))ShimEnumerator_MoveNext_m11863_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Entry()
extern "C" DictionaryEntry_t1068  ShimEnumerator_get_Entry_m11864_gshared (ShimEnumerator_t1889 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m11864(__this, method) (( DictionaryEntry_t1068  (*) (ShimEnumerator_t1889 *, const MethodInfo*))ShimEnumerator_get_Entry_m11864_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m11865_gshared (ShimEnumerator_t1889 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m11865(__this, method) (( Object_t * (*) (ShimEnumerator_t1889 *, const MethodInfo*))ShimEnumerator_get_Key_m11865_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m11866_gshared (ShimEnumerator_t1889 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m11866(__this, method) (( Object_t * (*) (ShimEnumerator_t1889 *, const MethodInfo*))ShimEnumerator_get_Value_m11866_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m11867_gshared (ShimEnumerator_t1889 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m11867(__this, method) (( Object_t * (*) (ShimEnumerator_t1889 *, const MethodInfo*))ShimEnumerator_get_Current_m11867_gshared)(__this, method)
