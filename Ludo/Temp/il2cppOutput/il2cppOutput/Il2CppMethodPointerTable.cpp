﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void BoardManager__ctor_m0 ();
extern "C" void BoardManager_Start_m1 ();
extern "C" void BoardManager_InitializeRedPath_m2 ();
extern "C" void BoardManager_InitializeBluePath_m3 ();
extern "C" void BoardManager_BoardSetup_m4 ();
extern "C" void BoardManager_SetupScene_m5 ();
extern "C" void U3CAutomaticTurnU3Ec__Iterator0__ctor_m6 ();
extern "C" void U3CAutomaticTurnU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m7 ();
extern "C" void U3CAutomaticTurnU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m8 ();
extern "C" void U3CAutomaticTurnU3Ec__Iterator0_MoveNext_m9 ();
extern "C" void U3CAutomaticTurnU3Ec__Iterator0_Dispose_m10 ();
extern "C" void U3CAutomaticTurnU3Ec__Iterator0_Reset_m11 ();
extern "C" void CountdownTimer__ctor_m12 ();
extern "C" void CountdownTimer__cctor_m13 ();
extern "C" void CountdownTimer_Start_m14 ();
extern "C" void CountdownTimer_Update_m15 ();
extern "C" void CountdownTimer_ShiftTimer_m16 ();
extern "C" void CountdownTimer_AutomaticTurn_m17 ();
extern "C" void U3CMoveTowardsTargetU3Ec__Iterator1__ctor_m18 ();
extern "C" void U3CMoveTowardsTargetU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m19 ();
extern "C" void U3CMoveTowardsTargetU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m20 ();
extern "C" void U3CMoveTowardsTargetU3Ec__Iterator1_MoveNext_m21 ();
extern "C" void U3CMoveTowardsTargetU3Ec__Iterator1_Dispose_m22 ();
extern "C" void U3CMoveTowardsTargetU3Ec__Iterator1_Reset_m23 ();
extern "C" void U3CGetActiveUsersU3Ec__Iterator2__ctor_m24 ();
extern "C" void U3CGetActiveUsersU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m25 ();
extern "C" void U3CGetActiveUsersU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m26 ();
extern "C" void U3CGetActiveUsersU3Ec__Iterator2_MoveNext_m27 ();
extern "C" void U3CGetActiveUsersU3Ec__Iterator2_Dispose_m28 ();
extern "C" void U3CGetActiveUsersU3Ec__Iterator2_Reset_m29 ();
extern "C" void DynamicScrollView__ctor_m30 ();
extern "C" void DynamicScrollView_OnEnable_m31 ();
extern "C" void DynamicScrollView_ClearOldElement_m32 ();
extern "C" void DynamicScrollView_SetContentHeight_m33 ();
extern "C" void DynamicScrollView_InitializeList_m34 ();
extern "C" void DynamicScrollView_InitializeListForGames_m35 ();
extern "C" void DynamicScrollView_InitializeNewItem_m36 ();
extern "C" void DynamicScrollView_MoveTowardsTarget_m37 ();
extern "C" void DynamicScrollView_GetActiveUsers_m38 ();
extern "C" void DynamicScrollView_AddNewElement_m39 ();
extern "C" void U3CResetBoardU3Ec__Iterator3__ctor_m40 ();
extern "C" void U3CResetBoardU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m41 ();
extern "C" void U3CResetBoardU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m42 ();
extern "C" void U3CResetBoardU3Ec__Iterator3_MoveNext_m43 ();
extern "C" void U3CResetBoardU3Ec__Iterator3_Dispose_m44 ();
extern "C" void U3CResetBoardU3Ec__Iterator3_Reset_m45 ();
extern "C" void U3CGetPlayer1TurnU3Ec__Iterator4__ctor_m46 ();
extern "C" void U3CGetPlayer1TurnU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m47 ();
extern "C" void U3CGetPlayer1TurnU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m48 ();
extern "C" void U3CGetPlayer1TurnU3Ec__Iterator4_MoveNext_m49 ();
extern "C" void U3CGetPlayer1TurnU3Ec__Iterator4_Dispose_m50 ();
extern "C" void U3CGetPlayer1TurnU3Ec__Iterator4_Reset_m51 ();
extern "C" void U3CGetPlayer2TurnU3Ec__Iterator5__ctor_m52 ();
extern "C" void U3CGetPlayer2TurnU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m53 ();
extern "C" void U3CGetPlayer2TurnU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m54 ();
extern "C" void U3CGetPlayer2TurnU3Ec__Iterator5_MoveNext_m55 ();
extern "C" void U3CGetPlayer2TurnU3Ec__Iterator5_Dispose_m56 ();
extern "C" void U3CGetPlayer2TurnU3Ec__Iterator5_Reset_m57 ();
extern "C" void U3CAutomaticMovePlayer1U3Ec__Iterator6__ctor_m58 ();
extern "C" void U3CAutomaticMovePlayer1U3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m59 ();
extern "C" void U3CAutomaticMovePlayer1U3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m60 ();
extern "C" void U3CAutomaticMovePlayer1U3Ec__Iterator6_MoveNext_m61 ();
extern "C" void U3CAutomaticMovePlayer1U3Ec__Iterator6_Dispose_m62 ();
extern "C" void U3CAutomaticMovePlayer1U3Ec__Iterator6_Reset_m63 ();
extern "C" void U3CAutomaticMovePlayer2U3Ec__Iterator7__ctor_m64 ();
extern "C" void U3CAutomaticMovePlayer2U3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m65 ();
extern "C" void U3CAutomaticMovePlayer2U3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m66 ();
extern "C" void U3CAutomaticMovePlayer2U3Ec__Iterator7_MoveNext_m67 ();
extern "C" void U3CAutomaticMovePlayer2U3Ec__Iterator7_Dispose_m68 ();
extern "C" void U3CAutomaticMovePlayer2U3Ec__Iterator7_Reset_m69 ();
extern "C" void GameManager__ctor_m70 ();
extern "C" void GameManager__cctor_m71 ();
extern "C" void GameManager_Awake_m72 ();
extern "C" void GameManager_Start_m73 ();
extern "C" void GameManager_InitGame_m74 ();
extern "C" void GameManager_GameOver_m75 ();
extern "C" void GameManager_GetNewPosition_m76 ();
extern "C" void GameManager_RollDice_m77 ();
extern "C" void GameManager_Move_Dice_m78 ();
extern "C" void GameManager_Move_OpponentsDice_m79 ();
extern "C" void GameManager_Stop_Dice_m80 ();
extern "C" void GameManager_Stop_OpponentsDice_m81 ();
extern "C" void GameManager_Update_m82 ();
extern "C" void GameManager_ResetButtonPressed_m83 ();
extern "C" void GameManager_ResetBoard_m84 ();
extern "C" void GameManager_CallGetPlayerTurn_m85 ();
extern "C" void GameManager_GetPlayer1Turn_m86 ();
extern "C" void GameManager_GetPlayer2Turn_m87 ();
extern "C" void GameManager_AutomaticMovePlayer1_m88 ();
extern "C" void GameManager_AutomaticMovePlayer2_m89 ();
extern "C" void U3CSetUserOnlineU3Ec__Iterator8__ctor_m90 ();
extern "C" void U3CSetUserOnlineU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m91 ();
extern "C" void U3CSetUserOnlineU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m92 ();
extern "C" void U3CSetUserOnlineU3Ec__Iterator8_MoveNext_m93 ();
extern "C" void U3CSetUserOnlineU3Ec__Iterator8_Dispose_m94 ();
extern "C" void U3CSetUserOnlineU3Ec__Iterator8_Reset_m95 ();
extern "C" void U3CGetActiveUsersU3Ec__Iterator9__ctor_m96 ();
extern "C" void U3CGetActiveUsersU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m97 ();
extern "C" void U3CGetActiveUsersU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m98 ();
extern "C" void U3CGetActiveUsersU3Ec__Iterator9_MoveNext_m99 ();
extern "C" void U3CGetActiveUsersU3Ec__Iterator9_Dispose_m100 ();
extern "C" void U3CGetActiveUsersU3Ec__Iterator9_Reset_m101 ();
extern "C" void GameMenu__ctor_m102 ();
extern "C" void GameMenu_Start_m103 ();
extern "C" void GameMenu_CallSetActiveUsers_m104 ();
extern "C" void GameMenu_SetUserOnline_m105 ();
extern "C" void GameMenu_GetActiveUsers_m106 ();
extern "C" void GameMenu_Refresh_m107 ();
extern "C" void Loader__ctor_m108 ();
extern "C" void Loader_Awake_m109 ();
extern "C" void U3CCreateUniquePlayerU3Ec__IteratorA__ctor_m110 ();
extern "C" void U3CCreateUniquePlayerU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m111 ();
extern "C" void U3CCreateUniquePlayerU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m112 ();
extern "C" void U3CCreateUniquePlayerU3Ec__IteratorA_MoveNext_m113 ();
extern "C" void U3CCreateUniquePlayerU3Ec__IteratorA_Dispose_m114 ();
extern "C" void U3CCreateUniquePlayerU3Ec__IteratorA_Reset_m115 ();
extern "C" void Menu__ctor_m116 ();
extern "C" void Menu_Start_m117 ();
extern "C" void Menu_CreateUser_m118 ();
extern "C" void Menu_PlayGame_m119 ();
extern "C" void Menu_CreateUniquePlayer_m120 ();
extern "C" void U3CSmoothMovementU3Ec__IteratorB__ctor_m121 ();
extern "C" void U3CSmoothMovementU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m122 ();
extern "C" void U3CSmoothMovementU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m123 ();
extern "C" void U3CSmoothMovementU3Ec__IteratorB_MoveNext_m124 ();
extern "C" void U3CSmoothMovementU3Ec__IteratorB_Dispose_m125 ();
extern "C" void U3CSmoothMovementU3Ec__IteratorB_Reset_m126 ();
extern "C" void MovingObject__ctor_m127 ();
extern "C" void MovingObject_Start_m128 ();
extern "C" void MovingObject_Move_m129 ();
extern "C" void MovingObject_SmoothMovement_m130 ();
extern "C" void MovingObject_AttemptMove_m131 ();
extern "C" void U3CSendPlayer1PositionU3Ec__IteratorC__ctor_m132 ();
extern "C" void U3CSendPlayer1PositionU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m133 ();
extern "C" void U3CSendPlayer1PositionU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m134 ();
extern "C" void U3CSendPlayer1PositionU3Ec__IteratorC_MoveNext_m135 ();
extern "C" void U3CSendPlayer1PositionU3Ec__IteratorC_Dispose_m136 ();
extern "C" void U3CSendPlayer1PositionU3Ec__IteratorC_Reset_m137 ();
extern "C" void U3CGetPlayer1PositionU3Ec__IteratorD__ctor_m138 ();
extern "C" void U3CGetPlayer1PositionU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m139 ();
extern "C" void U3CGetPlayer1PositionU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m140 ();
extern "C" void U3CGetPlayer1PositionU3Ec__IteratorD_MoveNext_m141 ();
extern "C" void U3CGetPlayer1PositionU3Ec__IteratorD_Dispose_m142 ();
extern "C" void U3CGetPlayer1PositionU3Ec__IteratorD_Reset_m143 ();
extern "C" void U3CExecuteAfterTimeU3Ec__IteratorE__ctor_m144 ();
extern "C" void U3CExecuteAfterTimeU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m145 ();
extern "C" void U3CExecuteAfterTimeU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m146 ();
extern "C" void U3CExecuteAfterTimeU3Ec__IteratorE_MoveNext_m147 ();
extern "C" void U3CExecuteAfterTimeU3Ec__IteratorE_Dispose_m148 ();
extern "C" void U3CExecuteAfterTimeU3Ec__IteratorE_Reset_m149 ();
extern "C" void Player__ctor_m150 ();
extern "C" void Player_Start_m151 ();
extern "C" void Player_MoveThePlayer_m152 ();
extern "C" void Player_AttemptMove_m153 ();
extern "C" void Player_SendPlayer1Position_m154 ();
extern "C" void Player_ChangeTransform_m155 ();
extern "C" void Player_GetPlayer1Position_m156 ();
extern "C" void Player_CallExecuteAfterTime_m157 ();
extern "C" void Player_ExecuteAfterTime_m158 ();
extern "C" void U3CSendPlayer2PositionU3Ec__IteratorF__ctor_m159 ();
extern "C" void U3CSendPlayer2PositionU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m160 ();
extern "C" void U3CSendPlayer2PositionU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m161 ();
extern "C" void U3CSendPlayer2PositionU3Ec__IteratorF_MoveNext_m162 ();
extern "C" void U3CSendPlayer2PositionU3Ec__IteratorF_Dispose_m163 ();
extern "C" void U3CSendPlayer2PositionU3Ec__IteratorF_Reset_m164 ();
extern "C" void U3CGetPlayer2PositionU3Ec__Iterator10__ctor_m165 ();
extern "C" void U3CGetPlayer2PositionU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m166 ();
extern "C" void U3CGetPlayer2PositionU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m167 ();
extern "C" void U3CGetPlayer2PositionU3Ec__Iterator10_MoveNext_m168 ();
extern "C" void U3CGetPlayer2PositionU3Ec__Iterator10_Dispose_m169 ();
extern "C" void U3CGetPlayer2PositionU3Ec__Iterator10_Reset_m170 ();
extern "C" void PlayerTwo__ctor_m171 ();
extern "C" void PlayerTwo_Start_m172 ();
extern "C" void PlayerTwo_MoveThePlayer_m173 ();
extern "C" void PlayerTwo_AttemptMove_m174 ();
extern "C" void PlayerTwo_SendPlayer2Position_m175 ();
extern "C" void PlayerTwo_ChangeTransform_m176 ();
extern "C" void PlayerTwo_GetPlayer2Position_m177 ();
extern "C" void U3CCreateGameU3Ec__Iterator11__ctor_m178 ();
extern "C" void U3CCreateGameU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m179 ();
extern "C" void U3CCreateGameU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m180 ();
extern "C" void U3CCreateGameU3Ec__Iterator11_MoveNext_m181 ();
extern "C" void U3CCreateGameU3Ec__Iterator11_Dispose_m182 ();
extern "C" void U3CCreateGameU3Ec__Iterator11_Reset_m183 ();
extern "C" void ScrollItem__ctor_m184 ();
extern "C" void ScrollItem_OnEnable_m185 ();
extern "C" void ScrollItem_CreateGame_m186 ();
extern "C" void ScrollItem_OnRemoveMe_m187 ();
extern "C" void Wall__ctor_m188 ();
extern "C" void Wall_Awake_m189 ();
extern "C" void Wall_DamageWall_m190 ();
extern "C" void EventSystem__ctor_m277 ();
extern "C" void EventSystem__cctor_m278 ();
extern "C" void EventSystem_get_current_m279 ();
extern "C" void EventSystem_set_current_m280 ();
extern "C" void EventSystem_get_sendNavigationEvents_m281 ();
extern "C" void EventSystem_set_sendNavigationEvents_m282 ();
extern "C" void EventSystem_get_pixelDragThreshold_m283 ();
extern "C" void EventSystem_set_pixelDragThreshold_m284 ();
extern "C" void EventSystem_get_currentInputModule_m285 ();
extern "C" void EventSystem_get_firstSelectedGameObject_m286 ();
extern "C" void EventSystem_set_firstSelectedGameObject_m287 ();
extern "C" void EventSystem_get_currentSelectedGameObject_m288 ();
extern "C" void EventSystem_get_lastSelectedGameObject_m289 ();
extern "C" void EventSystem_UpdateModules_m290 ();
extern "C" void EventSystem_get_alreadySelecting_m291 ();
extern "C" void EventSystem_SetSelectedGameObject_m292 ();
extern "C" void EventSystem_get_baseEventDataCache_m293 ();
extern "C" void EventSystem_SetSelectedGameObject_m294 ();
extern "C" void EventSystem_RaycastComparer_m295 ();
extern "C" void EventSystem_RaycastAll_m296 ();
extern "C" void EventSystem_IsPointerOverGameObject_m297 ();
extern "C" void EventSystem_IsPointerOverGameObject_m298 ();
extern "C" void EventSystem_OnEnable_m299 ();
extern "C" void EventSystem_OnDisable_m300 ();
extern "C" void EventSystem_TickModules_m301 ();
extern "C" void EventSystem_Update_m302 ();
extern "C" void EventSystem_ChangeEventModule_m303 ();
extern "C" void EventSystem_ToString_m304 ();
extern "C" void TriggerEvent__ctor_m305 ();
extern "C" void Entry__ctor_m306 ();
extern "C" void EventTrigger__ctor_m307 ();
extern "C" void EventTrigger_get_triggers_m308 ();
extern "C" void EventTrigger_set_triggers_m309 ();
extern "C" void EventTrigger_Execute_m310 ();
extern "C" void EventTrigger_OnPointerEnter_m311 ();
extern "C" void EventTrigger_OnPointerExit_m312 ();
extern "C" void EventTrigger_OnDrag_m313 ();
extern "C" void EventTrigger_OnDrop_m314 ();
extern "C" void EventTrigger_OnPointerDown_m315 ();
extern "C" void EventTrigger_OnPointerUp_m316 ();
extern "C" void EventTrigger_OnPointerClick_m317 ();
extern "C" void EventTrigger_OnSelect_m318 ();
extern "C" void EventTrigger_OnDeselect_m319 ();
extern "C" void EventTrigger_OnScroll_m320 ();
extern "C" void EventTrigger_OnMove_m321 ();
extern "C" void EventTrigger_OnUpdateSelected_m322 ();
extern "C" void EventTrigger_OnInitializePotentialDrag_m323 ();
extern "C" void EventTrigger_OnBeginDrag_m324 ();
extern "C" void EventTrigger_OnEndDrag_m325 ();
extern "C" void EventTrigger_OnSubmit_m326 ();
extern "C" void EventTrigger_OnCancel_m327 ();
extern "C" void ExecuteEvents__cctor_m328 ();
extern "C" void ExecuteEvents_Execute_m329 ();
extern "C" void ExecuteEvents_Execute_m330 ();
extern "C" void ExecuteEvents_Execute_m331 ();
extern "C" void ExecuteEvents_Execute_m332 ();
extern "C" void ExecuteEvents_Execute_m333 ();
extern "C" void ExecuteEvents_Execute_m334 ();
extern "C" void ExecuteEvents_Execute_m335 ();
extern "C" void ExecuteEvents_Execute_m336 ();
extern "C" void ExecuteEvents_Execute_m337 ();
extern "C" void ExecuteEvents_Execute_m338 ();
extern "C" void ExecuteEvents_Execute_m339 ();
extern "C" void ExecuteEvents_Execute_m340 ();
extern "C" void ExecuteEvents_Execute_m341 ();
extern "C" void ExecuteEvents_Execute_m342 ();
extern "C" void ExecuteEvents_Execute_m343 ();
extern "C" void ExecuteEvents_Execute_m344 ();
extern "C" void ExecuteEvents_Execute_m345 ();
extern "C" void ExecuteEvents_get_pointerEnterHandler_m346 ();
extern "C" void ExecuteEvents_get_pointerExitHandler_m347 ();
extern "C" void ExecuteEvents_get_pointerDownHandler_m348 ();
extern "C" void ExecuteEvents_get_pointerUpHandler_m349 ();
extern "C" void ExecuteEvents_get_pointerClickHandler_m350 ();
extern "C" void ExecuteEvents_get_initializePotentialDrag_m351 ();
extern "C" void ExecuteEvents_get_beginDragHandler_m352 ();
extern "C" void ExecuteEvents_get_dragHandler_m353 ();
extern "C" void ExecuteEvents_get_endDragHandler_m354 ();
extern "C" void ExecuteEvents_get_dropHandler_m355 ();
extern "C" void ExecuteEvents_get_scrollHandler_m356 ();
extern "C" void ExecuteEvents_get_updateSelectedHandler_m357 ();
extern "C" void ExecuteEvents_get_selectHandler_m358 ();
extern "C" void ExecuteEvents_get_deselectHandler_m359 ();
extern "C" void ExecuteEvents_get_moveHandler_m360 ();
extern "C" void ExecuteEvents_get_submitHandler_m361 ();
extern "C" void ExecuteEvents_get_cancelHandler_m362 ();
extern "C" void ExecuteEvents_GetEventChain_m363 ();
extern "C" void ExecuteEvents_U3Cs_HandlerListPoolU3Em__0_m364 ();
extern "C" void RaycasterManager__cctor_m365 ();
extern "C" void RaycasterManager_AddRaycaster_m366 ();
extern "C" void RaycasterManager_GetRaycasters_m367 ();
extern "C" void RaycasterManager_RemoveRaycasters_m368 ();
extern "C" void RaycastResult_get_gameObject_m369 ();
extern "C" void RaycastResult_set_gameObject_m370 ();
extern "C" void RaycastResult_get_isValid_m371 ();
extern "C" void RaycastResult_Clear_m372 ();
extern "C" void RaycastResult_ToString_m373 ();
extern "C" void UIBehaviour__ctor_m374 ();
extern "C" void UIBehaviour_Awake_m375 ();
extern "C" void UIBehaviour_OnEnable_m376 ();
extern "C" void UIBehaviour_Start_m377 ();
extern "C" void UIBehaviour_OnDisable_m378 ();
extern "C" void UIBehaviour_OnDestroy_m379 ();
extern "C" void UIBehaviour_IsActive_m380 ();
extern "C" void UIBehaviour_OnRectTransformDimensionsChange_m381 ();
extern "C" void UIBehaviour_OnBeforeTransformParentChanged_m382 ();
extern "C" void UIBehaviour_OnTransformParentChanged_m383 ();
extern "C" void UIBehaviour_OnDidApplyAnimationProperties_m384 ();
extern "C" void UIBehaviour_OnCanvasGroupChanged_m385 ();
extern "C" void UIBehaviour_OnCanvasHierarchyChanged_m386 ();
extern "C" void UIBehaviour_IsDestroyed_m387 ();
extern "C" void AxisEventData__ctor_m388 ();
extern "C" void AxisEventData_get_moveVector_m389 ();
extern "C" void AxisEventData_set_moveVector_m390 ();
extern "C" void AxisEventData_get_moveDir_m391 ();
extern "C" void AxisEventData_set_moveDir_m392 ();
extern "C" void BaseEventData__ctor_m393 ();
extern "C" void BaseEventData_Reset_m394 ();
extern "C" void BaseEventData_Use_m395 ();
extern "C" void BaseEventData_get_used_m396 ();
extern "C" void BaseEventData_get_currentInputModule_m397 ();
extern "C" void BaseEventData_get_selectedObject_m398 ();
extern "C" void BaseEventData_set_selectedObject_m399 ();
extern "C" void PointerEventData__ctor_m400 ();
extern "C" void PointerEventData_get_pointerEnter_m401 ();
extern "C" void PointerEventData_set_pointerEnter_m402 ();
extern "C" void PointerEventData_get_lastPress_m403 ();
extern "C" void PointerEventData_set_lastPress_m404 ();
extern "C" void PointerEventData_get_rawPointerPress_m405 ();
extern "C" void PointerEventData_set_rawPointerPress_m406 ();
extern "C" void PointerEventData_get_pointerDrag_m407 ();
extern "C" void PointerEventData_set_pointerDrag_m408 ();
extern "C" void PointerEventData_get_pointerCurrentRaycast_m409 ();
extern "C" void PointerEventData_set_pointerCurrentRaycast_m410 ();
extern "C" void PointerEventData_get_pointerPressRaycast_m411 ();
extern "C" void PointerEventData_set_pointerPressRaycast_m412 ();
extern "C" void PointerEventData_get_eligibleForClick_m413 ();
extern "C" void PointerEventData_set_eligibleForClick_m414 ();
extern "C" void PointerEventData_get_pointerId_m415 ();
extern "C" void PointerEventData_set_pointerId_m416 ();
extern "C" void PointerEventData_get_position_m417 ();
extern "C" void PointerEventData_set_position_m418 ();
extern "C" void PointerEventData_get_delta_m419 ();
extern "C" void PointerEventData_set_delta_m420 ();
extern "C" void PointerEventData_get_pressPosition_m421 ();
extern "C" void PointerEventData_set_pressPosition_m422 ();
extern "C" void PointerEventData_get_worldPosition_m423 ();
extern "C" void PointerEventData_set_worldPosition_m424 ();
extern "C" void PointerEventData_get_worldNormal_m425 ();
extern "C" void PointerEventData_set_worldNormal_m426 ();
extern "C" void PointerEventData_get_clickTime_m427 ();
extern "C" void PointerEventData_set_clickTime_m428 ();
extern "C" void PointerEventData_get_clickCount_m429 ();
extern "C" void PointerEventData_set_clickCount_m430 ();
extern "C" void PointerEventData_get_scrollDelta_m431 ();
extern "C" void PointerEventData_set_scrollDelta_m432 ();
extern "C" void PointerEventData_get_useDragThreshold_m433 ();
extern "C" void PointerEventData_set_useDragThreshold_m434 ();
extern "C" void PointerEventData_get_dragging_m435 ();
extern "C" void PointerEventData_set_dragging_m436 ();
extern "C" void PointerEventData_get_button_m437 ();
extern "C" void PointerEventData_set_button_m438 ();
extern "C" void PointerEventData_IsPointerMoving_m439 ();
extern "C" void PointerEventData_IsScrolling_m440 ();
extern "C" void PointerEventData_get_enterEventCamera_m441 ();
extern "C" void PointerEventData_get_pressEventCamera_m442 ();
extern "C" void PointerEventData_get_pointerPress_m443 ();
extern "C" void PointerEventData_set_pointerPress_m444 ();
extern "C" void PointerEventData_ToString_m445 ();
extern "C" void BaseInputModule__ctor_m446 ();
extern "C" void BaseInputModule_get_eventSystem_m447 ();
extern "C" void BaseInputModule_OnEnable_m448 ();
extern "C" void BaseInputModule_OnDisable_m449 ();
extern "C" void BaseInputModule_FindFirstRaycast_m450 ();
extern "C" void BaseInputModule_DetermineMoveDirection_m451 ();
extern "C" void BaseInputModule_DetermineMoveDirection_m452 ();
extern "C" void BaseInputModule_FindCommonRoot_m453 ();
extern "C" void BaseInputModule_HandlePointerExitAndEnter_m454 ();
extern "C" void BaseInputModule_GetAxisEventData_m455 ();
extern "C" void BaseInputModule_GetBaseEventData_m456 ();
extern "C" void BaseInputModule_IsPointerOverGameObject_m457 ();
extern "C" void BaseInputModule_ShouldActivateModule_m458 ();
extern "C" void BaseInputModule_DeactivateModule_m459 ();
extern "C" void BaseInputModule_ActivateModule_m460 ();
extern "C" void BaseInputModule_UpdateModule_m461 ();
extern "C" void BaseInputModule_IsModuleSupported_m462 ();
extern "C" void ButtonState__ctor_m463 ();
extern "C" void ButtonState_get_eventData_m464 ();
extern "C" void ButtonState_set_eventData_m465 ();
extern "C" void ButtonState_get_button_m466 ();
extern "C" void ButtonState_set_button_m467 ();
extern "C" void MouseState__ctor_m468 ();
extern "C" void MouseState_AnyPressesThisFrame_m469 ();
extern "C" void MouseState_AnyReleasesThisFrame_m470 ();
extern "C" void MouseState_GetButtonState_m471 ();
extern "C" void MouseState_SetButtonState_m472 ();
extern "C" void MouseButtonEventData__ctor_m473 ();
extern "C" void MouseButtonEventData_PressedThisFrame_m474 ();
extern "C" void MouseButtonEventData_ReleasedThisFrame_m475 ();
extern "C" void PointerInputModule__ctor_m476 ();
extern "C" void PointerInputModule_GetPointerData_m477 ();
extern "C" void PointerInputModule_RemovePointerData_m478 ();
extern "C" void PointerInputModule_GetTouchPointerEventData_m479 ();
extern "C" void PointerInputModule_CopyFromTo_m480 ();
extern "C" void PointerInputModule_StateForMouseButton_m481 ();
extern "C" void PointerInputModule_GetMousePointerEventData_m482 ();
extern "C" void PointerInputModule_GetLastPointerEventData_m483 ();
extern "C" void PointerInputModule_ShouldStartDrag_m484 ();
extern "C" void PointerInputModule_ProcessMove_m485 ();
extern "C" void PointerInputModule_ProcessDrag_m486 ();
extern "C" void PointerInputModule_IsPointerOverGameObject_m487 ();
extern "C" void PointerInputModule_ClearSelection_m488 ();
extern "C" void PointerInputModule_ToString_m489 ();
extern "C" void PointerInputModule_DeselectIfSelectionChanged_m490 ();
extern "C" void StandaloneInputModule__ctor_m491 ();
extern "C" void StandaloneInputModule_get_inputMode_m492 ();
extern "C" void StandaloneInputModule_get_allowActivationOnMobileDevice_m493 ();
extern "C" void StandaloneInputModule_set_allowActivationOnMobileDevice_m494 ();
extern "C" void StandaloneInputModule_get_inputActionsPerSecond_m495 ();
extern "C" void StandaloneInputModule_set_inputActionsPerSecond_m496 ();
extern "C" void StandaloneInputModule_get_repeatDelay_m497 ();
extern "C" void StandaloneInputModule_set_repeatDelay_m498 ();
extern "C" void StandaloneInputModule_get_horizontalAxis_m499 ();
extern "C" void StandaloneInputModule_set_horizontalAxis_m500 ();
extern "C" void StandaloneInputModule_get_verticalAxis_m501 ();
extern "C" void StandaloneInputModule_set_verticalAxis_m502 ();
extern "C" void StandaloneInputModule_get_submitButton_m503 ();
extern "C" void StandaloneInputModule_set_submitButton_m504 ();
extern "C" void StandaloneInputModule_get_cancelButton_m505 ();
extern "C" void StandaloneInputModule_set_cancelButton_m506 ();
extern "C" void StandaloneInputModule_UpdateModule_m507 ();
extern "C" void StandaloneInputModule_IsModuleSupported_m508 ();
extern "C" void StandaloneInputModule_ShouldActivateModule_m509 ();
extern "C" void StandaloneInputModule_ActivateModule_m510 ();
extern "C" void StandaloneInputModule_DeactivateModule_m511 ();
extern "C" void StandaloneInputModule_Process_m512 ();
extern "C" void StandaloneInputModule_SendSubmitEventToSelectedObject_m513 ();
extern "C" void StandaloneInputModule_GetRawMoveVector_m514 ();
extern "C" void StandaloneInputModule_SendMoveEventToSelectedObject_m515 ();
extern "C" void StandaloneInputModule_ProcessMouseEvent_m516 ();
extern "C" void StandaloneInputModule_ProcessMouseEvent_m517 ();
extern "C" void StandaloneInputModule_SendUpdateEventToSelectedObject_m518 ();
extern "C" void StandaloneInputModule_ProcessMousePress_m519 ();
extern "C" void TouchInputModule__ctor_m520 ();
extern "C" void TouchInputModule_get_allowActivationOnStandalone_m521 ();
extern "C" void TouchInputModule_set_allowActivationOnStandalone_m522 ();
extern "C" void TouchInputModule_UpdateModule_m523 ();
extern "C" void TouchInputModule_IsModuleSupported_m524 ();
extern "C" void TouchInputModule_ShouldActivateModule_m525 ();
extern "C" void TouchInputModule_UseFakeInput_m526 ();
extern "C" void TouchInputModule_Process_m527 ();
extern "C" void TouchInputModule_FakeTouches_m528 ();
extern "C" void TouchInputModule_ProcessTouchEvents_m529 ();
extern "C" void TouchInputModule_ProcessTouchPress_m530 ();
extern "C" void TouchInputModule_DeactivateModule_m531 ();
extern "C" void TouchInputModule_ToString_m532 ();
extern "C" void BaseRaycaster__ctor_m533 ();
extern "C" void BaseRaycaster_get_priority_m534 ();
extern "C" void BaseRaycaster_get_sortOrderPriority_m535 ();
extern "C" void BaseRaycaster_get_renderOrderPriority_m536 ();
extern "C" void BaseRaycaster_OnEnable_m537 ();
extern "C" void BaseRaycaster_OnDisable_m538 ();
extern "C" void Physics2DRaycaster__ctor_m539 ();
extern "C" void Physics2DRaycaster_Raycast_m540 ();
extern "C" void PhysicsRaycaster__ctor_m541 ();
extern "C" void PhysicsRaycaster_get_eventCamera_m542 ();
extern "C" void PhysicsRaycaster_get_depth_m543 ();
extern "C" void PhysicsRaycaster_get_finalEventMask_m544 ();
extern "C" void PhysicsRaycaster_get_eventMask_m545 ();
extern "C" void PhysicsRaycaster_set_eventMask_m546 ();
extern "C" void PhysicsRaycaster_Raycast_m547 ();
extern "C" void PhysicsRaycaster_U3CRaycastU3Em__1_m548 ();
extern "C" void ColorTweenCallback__ctor_m549 ();
extern "C" void ColorTween_get_startColor_m550 ();
extern "C" void ColorTween_set_startColor_m551 ();
extern "C" void ColorTween_get_targetColor_m552 ();
extern "C" void ColorTween_set_targetColor_m553 ();
extern "C" void ColorTween_get_tweenMode_m554 ();
extern "C" void ColorTween_set_tweenMode_m555 ();
extern "C" void ColorTween_get_duration_m556 ();
extern "C" void ColorTween_set_duration_m557 ();
extern "C" void ColorTween_get_ignoreTimeScale_m558 ();
extern "C" void ColorTween_set_ignoreTimeScale_m559 ();
extern "C" void ColorTween_TweenValue_m560 ();
extern "C" void ColorTween_AddOnChangedCallback_m561 ();
extern "C" void ColorTween_GetIgnoreTimescale_m562 ();
extern "C" void ColorTween_GetDuration_m563 ();
extern "C" void ColorTween_ValidTarget_m564 ();
extern "C" void AnimationTriggers__ctor_m565 ();
extern "C" void AnimationTriggers_get_normalTrigger_m566 ();
extern "C" void AnimationTriggers_set_normalTrigger_m567 ();
extern "C" void AnimationTriggers_get_highlightedTrigger_m568 ();
extern "C" void AnimationTriggers_set_highlightedTrigger_m569 ();
extern "C" void AnimationTriggers_get_pressedTrigger_m570 ();
extern "C" void AnimationTriggers_set_pressedTrigger_m571 ();
extern "C" void AnimationTriggers_get_disabledTrigger_m572 ();
extern "C" void AnimationTriggers_set_disabledTrigger_m573 ();
extern "C" void ButtonClickedEvent__ctor_m574 ();
extern "C" void U3COnFinishSubmitU3Ec__Iterator1__ctor_m575 ();
extern "C" void U3COnFinishSubmitU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m576 ();
extern "C" void U3COnFinishSubmitU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m577 ();
extern "C" void U3COnFinishSubmitU3Ec__Iterator1_MoveNext_m578 ();
extern "C" void U3COnFinishSubmitU3Ec__Iterator1_Dispose_m579 ();
extern "C" void U3COnFinishSubmitU3Ec__Iterator1_Reset_m580 ();
extern "C" void Button__ctor_m581 ();
extern "C" void Button_get_onClick_m582 ();
extern "C" void Button_set_onClick_m583 ();
extern "C" void Button_Press_m584 ();
extern "C" void Button_OnPointerClick_m585 ();
extern "C" void Button_OnSubmit_m586 ();
extern "C" void Button_OnFinishSubmit_m587 ();
extern "C" void CanvasUpdateRegistry__ctor_m588 ();
extern "C" void CanvasUpdateRegistry__cctor_m589 ();
extern "C" void CanvasUpdateRegistry_get_instance_m590 ();
extern "C" void CanvasUpdateRegistry_ObjectValidForUpdate_m591 ();
extern "C" void CanvasUpdateRegistry_PerformUpdate_m592 ();
extern "C" void CanvasUpdateRegistry_ParentCount_m593 ();
extern "C" void CanvasUpdateRegistry_SortLayoutList_m594 ();
extern "C" void CanvasUpdateRegistry_RegisterCanvasElementForLayoutRebuild_m595 ();
extern "C" void CanvasUpdateRegistry_InternalRegisterCanvasElementForLayoutRebuild_m596 ();
extern "C" void CanvasUpdateRegistry_RegisterCanvasElementForGraphicRebuild_m597 ();
extern "C" void CanvasUpdateRegistry_InternalRegisterCanvasElementForGraphicRebuild_m598 ();
extern "C" void CanvasUpdateRegistry_UnRegisterCanvasElementForRebuild_m599 ();
extern "C" void CanvasUpdateRegistry_InternalUnRegisterCanvasElementForLayoutRebuild_m600 ();
extern "C" void CanvasUpdateRegistry_InternalUnRegisterCanvasElementForGraphicRebuild_m601 ();
extern "C" void CanvasUpdateRegistry_IsRebuildingLayout_m602 ();
extern "C" void CanvasUpdateRegistry_IsRebuildingGraphics_m603 ();
extern "C" void CanvasUpdateRegistry_U3CPerformUpdateU3Em__2_m604 ();
extern "C" void CanvasUpdateRegistry_U3CPerformUpdateU3Em__3_m605 ();
extern "C" void ColorBlock_get_normalColor_m606 ();
extern "C" void ColorBlock_set_normalColor_m607 ();
extern "C" void ColorBlock_get_highlightedColor_m608 ();
extern "C" void ColorBlock_set_highlightedColor_m609 ();
extern "C" void ColorBlock_get_pressedColor_m610 ();
extern "C" void ColorBlock_set_pressedColor_m611 ();
extern "C" void ColorBlock_get_disabledColor_m612 ();
extern "C" void ColorBlock_set_disabledColor_m613 ();
extern "C" void ColorBlock_get_colorMultiplier_m614 ();
extern "C" void ColorBlock_set_colorMultiplier_m615 ();
extern "C" void ColorBlock_get_fadeDuration_m616 ();
extern "C" void ColorBlock_set_fadeDuration_m617 ();
extern "C" void ColorBlock_get_defaultColorBlock_m618 ();
extern "C" void FontData__ctor_m619 ();
extern "C" void FontData_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m620 ();
extern "C" void FontData_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m621 ();
extern "C" void FontData_get_defaultFontData_m622 ();
extern "C" void FontData_get_font_m623 ();
extern "C" void FontData_set_font_m624 ();
extern "C" void FontData_get_fontSize_m625 ();
extern "C" void FontData_set_fontSize_m626 ();
extern "C" void FontData_get_fontStyle_m627 ();
extern "C" void FontData_set_fontStyle_m628 ();
extern "C" void FontData_get_bestFit_m629 ();
extern "C" void FontData_set_bestFit_m630 ();
extern "C" void FontData_get_minSize_m631 ();
extern "C" void FontData_set_minSize_m632 ();
extern "C" void FontData_get_maxSize_m633 ();
extern "C" void FontData_set_maxSize_m634 ();
extern "C" void FontData_get_alignment_m635 ();
extern "C" void FontData_set_alignment_m636 ();
extern "C" void FontData_get_richText_m637 ();
extern "C" void FontData_set_richText_m638 ();
extern "C" void FontData_get_horizontalOverflow_m639 ();
extern "C" void FontData_set_horizontalOverflow_m640 ();
extern "C" void FontData_get_verticalOverflow_m641 ();
extern "C" void FontData_set_verticalOverflow_m642 ();
extern "C" void FontData_get_lineSpacing_m643 ();
extern "C" void FontData_set_lineSpacing_m644 ();
extern "C" void FontUpdateTracker__cctor_m645 ();
extern "C" void FontUpdateTracker_TrackText_m646 ();
extern "C" void FontUpdateTracker_RebuildForFont_m647 ();
extern "C" void FontUpdateTracker_UntrackText_m648 ();
extern "C" void Graphic__ctor_m649 ();
extern "C" void Graphic__cctor_m650 ();
extern "C" void Graphic_get_defaultGraphicMaterial_m651 ();
extern "C" void Graphic_get_color_m652 ();
extern "C" void Graphic_set_color_m653 ();
extern "C" void Graphic_SetAllDirty_m654 ();
extern "C" void Graphic_SetLayoutDirty_m655 ();
extern "C" void Graphic_SetVerticesDirty_m656 ();
extern "C" void Graphic_SetMaterialDirty_m657 ();
extern "C" void Graphic_OnRectTransformDimensionsChange_m658 ();
extern "C" void Graphic_OnBeforeTransformParentChanged_m659 ();
extern "C" void Graphic_OnTransformParentChanged_m660 ();
extern "C" void Graphic_get_depth_m661 ();
extern "C" void Graphic_get_rectTransform_m662 ();
extern "C" void Graphic_get_canvas_m663 ();
extern "C" void Graphic_CacheCanvas_m664 ();
extern "C" void Graphic_get_canvasRenderer_m665 ();
extern "C" void Graphic_get_defaultMaterial_m666 ();
extern "C" void Graphic_get_material_m667 ();
extern "C" void Graphic_set_material_m668 ();
extern "C" void Graphic_get_materialForRendering_m669 ();
extern "C" void Graphic_get_mainTexture_m670 ();
extern "C" void Graphic_OnEnable_m671 ();
extern "C" void Graphic_OnDisable_m672 ();
extern "C" void Graphic_SendGraphicEnabledDisabled_m673 ();
extern "C" void Graphic_OnCanvasHierarchyChanged_m674 ();
extern "C" void Graphic_Rebuild_m675 ();
extern "C" void Graphic_UpdateGeometry_m676 ();
extern "C" void Graphic_UpdateMaterial_m677 ();
extern "C" void Graphic_OnFillVBO_m678 ();
extern "C" void Graphic_OnDidApplyAnimationProperties_m679 ();
extern "C" void Graphic_SetNativeSize_m680 ();
extern "C" void Graphic_Raycast_m681 ();
extern "C" void Graphic_PixelAdjustPoint_m682 ();
extern "C" void Graphic_GetPixelAdjustedRect_m683 ();
extern "C" void Graphic_CrossFadeColor_m684 ();
extern "C" void Graphic_CrossFadeColor_m685 ();
extern "C" void Graphic_CreateColorFromAlpha_m686 ();
extern "C" void Graphic_CrossFadeAlpha_m687 ();
extern "C" void Graphic_RegisterDirtyLayoutCallback_m688 ();
extern "C" void Graphic_UnregisterDirtyLayoutCallback_m689 ();
extern "C" void Graphic_RegisterDirtyVerticesCallback_m690 ();
extern "C" void Graphic_UnregisterDirtyVerticesCallback_m691 ();
extern "C" void Graphic_RegisterDirtyMaterialCallback_m692 ();
extern "C" void Graphic_UnregisterDirtyMaterialCallback_m693 ();
extern "C" void Graphic_U3Cs_VboPoolU3Em__4_m694 ();
extern "C" void Graphic_U3Cs_VboPoolU3Em__5_m695 ();
extern "C" void Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m696 ();
extern "C" void Graphic_UnityEngine_UI_ICanvasElement_get_transform_m697 ();
extern "C" void GraphicRaycaster__ctor_m698 ();
extern "C" void GraphicRaycaster__cctor_m699 ();
extern "C" void GraphicRaycaster_get_sortOrderPriority_m700 ();
extern "C" void GraphicRaycaster_get_renderOrderPriority_m701 ();
extern "C" void GraphicRaycaster_get_ignoreReversedGraphics_m702 ();
extern "C" void GraphicRaycaster_set_ignoreReversedGraphics_m703 ();
extern "C" void GraphicRaycaster_get_blockingObjects_m704 ();
extern "C" void GraphicRaycaster_set_blockingObjects_m705 ();
extern "C" void GraphicRaycaster_get_canvas_m706 ();
extern "C" void GraphicRaycaster_Raycast_m707 ();
extern "C" void GraphicRaycaster_get_eventCamera_m708 ();
extern "C" void GraphicRaycaster_Raycast_m709 ();
extern "C" void GraphicRaycaster_U3CRaycastU3Em__6_m710 ();
extern "C" void GraphicRegistry__ctor_m711 ();
extern "C" void GraphicRegistry__cctor_m712 ();
extern "C" void GraphicRegistry_get_instance_m713 ();
extern "C" void GraphicRegistry_RegisterGraphicForCanvas_m714 ();
extern "C" void GraphicRegistry_UnregisterGraphicForCanvas_m715 ();
extern "C" void GraphicRegistry_GetGraphicsForCanvas_m716 ();
extern "C" void Image__ctor_m717 ();
extern "C" void Image__cctor_m718 ();
extern "C" void Image_get_sprite_m719 ();
extern "C" void Image_set_sprite_m720 ();
extern "C" void Image_get_overrideSprite_m721 ();
extern "C" void Image_set_overrideSprite_m722 ();
extern "C" void Image_get_type_m723 ();
extern "C" void Image_set_type_m724 ();
extern "C" void Image_get_preserveAspect_m725 ();
extern "C" void Image_set_preserveAspect_m726 ();
extern "C" void Image_get_fillCenter_m727 ();
extern "C" void Image_set_fillCenter_m728 ();
extern "C" void Image_get_fillMethod_m729 ();
extern "C" void Image_set_fillMethod_m730 ();
extern "C" void Image_get_fillAmount_m731 ();
extern "C" void Image_set_fillAmount_m732 ();
extern "C" void Image_get_fillClockwise_m733 ();
extern "C" void Image_set_fillClockwise_m734 ();
extern "C" void Image_get_fillOrigin_m735 ();
extern "C" void Image_set_fillOrigin_m736 ();
extern "C" void Image_get_eventAlphaThreshold_m737 ();
extern "C" void Image_set_eventAlphaThreshold_m738 ();
extern "C" void Image_get_mainTexture_m739 ();
extern "C" void Image_get_hasBorder_m740 ();
extern "C" void Image_get_pixelsPerUnit_m741 ();
extern "C" void Image_OnBeforeSerialize_m742 ();
extern "C" void Image_OnAfterDeserialize_m743 ();
extern "C" void Image_GetDrawingDimensions_m744 ();
extern "C" void Image_SetNativeSize_m745 ();
extern "C" void Image_OnFillVBO_m746 ();
extern "C" void Image_GenerateSimpleSprite_m747 ();
extern "C" void Image_GenerateSlicedSprite_m748 ();
extern "C" void Image_GenerateTiledSprite_m749 ();
extern "C" void Image_AddQuad_m750 ();
extern "C" void Image_GetAdjustedBorders_m751 ();
extern "C" void Image_GenerateFilledSprite_m752 ();
extern "C" void Image_RadialCut_m753 ();
extern "C" void Image_RadialCut_m754 ();
extern "C" void Image_CalculateLayoutInputHorizontal_m755 ();
extern "C" void Image_CalculateLayoutInputVertical_m756 ();
extern "C" void Image_get_minWidth_m757 ();
extern "C" void Image_get_preferredWidth_m758 ();
extern "C" void Image_get_flexibleWidth_m759 ();
extern "C" void Image_get_minHeight_m760 ();
extern "C" void Image_get_preferredHeight_m761 ();
extern "C" void Image_get_flexibleHeight_m762 ();
extern "C" void Image_get_layoutPriority_m763 ();
extern "C" void Image_IsRaycastLocationValid_m764 ();
extern "C" void Image_MapCoordinate_m765 ();
extern "C" void SubmitEvent__ctor_m766 ();
extern "C" void OnChangeEvent__ctor_m767 ();
extern "C" void OnValidateInput__ctor_m768 ();
extern "C" void OnValidateInput_Invoke_m769 ();
extern "C" void OnValidateInput_BeginInvoke_m770 ();
extern "C" void OnValidateInput_EndInvoke_m771 ();
extern "C" void U3CCaretBlinkU3Ec__Iterator2__ctor_m772 ();
extern "C" void U3CCaretBlinkU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m773 ();
extern "C" void U3CCaretBlinkU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m774 ();
extern "C" void U3CCaretBlinkU3Ec__Iterator2_MoveNext_m775 ();
extern "C" void U3CCaretBlinkU3Ec__Iterator2_Dispose_m776 ();
extern "C" void U3CCaretBlinkU3Ec__Iterator2_Reset_m777 ();
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator3__ctor_m778 ();
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m779 ();
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m780 ();
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator3_MoveNext_m781 ();
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator3_Dispose_m782 ();
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator3_Reset_m783 ();
extern "C" void InputField__ctor_m784 ();
extern "C" void InputField__cctor_m785 ();
extern "C" void InputField_get_cachedInputTextGenerator_m786 ();
extern "C" void InputField_set_shouldHideMobileInput_m787 ();
extern "C" void InputField_get_shouldHideMobileInput_m788 ();
extern "C" void InputField_get_text_m255 ();
extern "C" void InputField_set_text_m789 ();
extern "C" void InputField_get_isFocused_m790 ();
extern "C" void InputField_get_caretBlinkRate_m791 ();
extern "C" void InputField_set_caretBlinkRate_m792 ();
extern "C" void InputField_get_textComponent_m793 ();
extern "C" void InputField_set_textComponent_m794 ();
extern "C" void InputField_get_placeholder_m795 ();
extern "C" void InputField_set_placeholder_m796 ();
extern "C" void InputField_get_selectionColor_m797 ();
extern "C" void InputField_set_selectionColor_m798 ();
extern "C" void InputField_get_onEndEdit_m799 ();
extern "C" void InputField_set_onEndEdit_m800 ();
extern "C" void InputField_get_onValueChange_m801 ();
extern "C" void InputField_set_onValueChange_m802 ();
extern "C" void InputField_get_onValidateInput_m803 ();
extern "C" void InputField_set_onValidateInput_m804 ();
extern "C" void InputField_get_characterLimit_m805 ();
extern "C" void InputField_set_characterLimit_m806 ();
extern "C" void InputField_get_contentType_m807 ();
extern "C" void InputField_set_contentType_m808 ();
extern "C" void InputField_get_lineType_m809 ();
extern "C" void InputField_set_lineType_m810 ();
extern "C" void InputField_get_inputType_m811 ();
extern "C" void InputField_set_inputType_m812 ();
extern "C" void InputField_get_keyboardType_m813 ();
extern "C" void InputField_set_keyboardType_m814 ();
extern "C" void InputField_get_characterValidation_m815 ();
extern "C" void InputField_set_characterValidation_m816 ();
extern "C" void InputField_get_multiLine_m817 ();
extern "C" void InputField_get_asteriskChar_m818 ();
extern "C" void InputField_set_asteriskChar_m819 ();
extern "C" void InputField_get_wasCanceled_m820 ();
extern "C" void InputField_ClampPos_m821 ();
extern "C" void InputField_get_caretPositionInternal_m822 ();
extern "C" void InputField_set_caretPositionInternal_m823 ();
extern "C" void InputField_get_caretSelectPositionInternal_m824 ();
extern "C" void InputField_set_caretSelectPositionInternal_m825 ();
extern "C" void InputField_get_hasSelection_m826 ();
extern "C" void InputField_get_caretPosition_m827 ();
extern "C" void InputField_set_caretPosition_m828 ();
extern "C" void InputField_get_selectionAnchorPosition_m829 ();
extern "C" void InputField_set_selectionAnchorPosition_m830 ();
extern "C" void InputField_get_selectionFocusPosition_m831 ();
extern "C" void InputField_set_selectionFocusPosition_m832 ();
extern "C" void InputField_OnEnable_m833 ();
extern "C" void InputField_OnDisable_m834 ();
extern "C" void InputField_CaretBlink_m835 ();
extern "C" void InputField_SetCaretVisible_m836 ();
extern "C" void InputField_SetCaretActive_m837 ();
extern "C" void InputField_OnFocus_m838 ();
extern "C" void InputField_SelectAll_m839 ();
extern "C" void InputField_MoveTextEnd_m840 ();
extern "C" void InputField_MoveTextStart_m841 ();
extern "C" void InputField_get_clipboard_m842 ();
extern "C" void InputField_set_clipboard_m843 ();
extern "C" void InputField_InPlaceEditing_m844 ();
extern "C" void InputField_LateUpdate_m845 ();
extern "C" void InputField_ScreenToLocal_m846 ();
extern "C" void InputField_GetUnclampedCharacterLineFromPosition_m847 ();
extern "C" void InputField_GetCharacterIndexFromPosition_m848 ();
extern "C" void InputField_MayDrag_m849 ();
extern "C" void InputField_OnBeginDrag_m850 ();
extern "C" void InputField_OnDrag_m851 ();
extern "C" void InputField_MouseDragOutsideRect_m852 ();
extern "C" void InputField_OnEndDrag_m853 ();
extern "C" void InputField_OnPointerDown_m854 ();
extern "C" void InputField_KeyPressed_m855 ();
extern "C" void InputField_IsValidChar_m856 ();
extern "C" void InputField_ProcessEvent_m857 ();
extern "C" void InputField_OnUpdateSelected_m858 ();
extern "C" void InputField_GetSelectedString_m859 ();
extern "C" void InputField_FindtNextWordBegin_m860 ();
extern "C" void InputField_MoveRight_m861 ();
extern "C" void InputField_FindtPrevWordBegin_m862 ();
extern "C" void InputField_MoveLeft_m863 ();
extern "C" void InputField_DetermineCharacterLine_m864 ();
extern "C" void InputField_LineUpCharacterPosition_m865 ();
extern "C" void InputField_LineDownCharacterPosition_m866 ();
extern "C" void InputField_MoveDown_m867 ();
extern "C" void InputField_MoveDown_m868 ();
extern "C" void InputField_MoveUp_m869 ();
extern "C" void InputField_MoveUp_m870 ();
extern "C" void InputField_Delete_m871 ();
extern "C" void InputField_ForwardSpace_m872 ();
extern "C" void InputField_Backspace_m873 ();
extern "C" void InputField_Insert_m874 ();
extern "C" void InputField_SendOnValueChangedAndUpdateLabel_m875 ();
extern "C" void InputField_SendOnValueChanged_m876 ();
extern "C" void InputField_SendOnSubmit_m877 ();
extern "C" void InputField_Append_m878 ();
extern "C" void InputField_Append_m879 ();
extern "C" void InputField_UpdateLabel_m880 ();
extern "C" void InputField_IsSelectionVisible_m881 ();
extern "C" void InputField_GetLineStartPosition_m882 ();
extern "C" void InputField_GetLineEndPosition_m883 ();
extern "C" void InputField_SetDrawRangeToContainCaretPosition_m884 ();
extern "C" void InputField_MarkGeometryAsDirty_m885 ();
extern "C" void InputField_Rebuild_m886 ();
extern "C" void InputField_UpdateGeometry_m887 ();
extern "C" void InputField_AssignPositioningIfNeeded_m888 ();
extern "C" void InputField_OnFillVBO_m889 ();
extern "C" void InputField_GenerateCursor_m890 ();
extern "C" void InputField_CreateCursorVerts_m891 ();
extern "C" void InputField_SumLineHeights_m892 ();
extern "C" void InputField_GenerateHightlight_m893 ();
extern "C" void InputField_Validate_m894 ();
extern "C" void InputField_ActivateInputField_m895 ();
extern "C" void InputField_ActivateInputFieldInternal_m896 ();
extern "C" void InputField_OnSelect_m897 ();
extern "C" void InputField_OnPointerClick_m898 ();
extern "C" void InputField_DeactivateInputField_m899 ();
extern "C" void InputField_OnDeselect_m900 ();
extern "C" void InputField_OnSubmit_m901 ();
extern "C" void InputField_EnforceContentType_m902 ();
extern "C" void InputField_SetToCustomIfContentTypeIsNot_m903 ();
extern "C" void InputField_SetToCustom_m904 ();
extern "C" void InputField_DoStateTransition_m905 ();
extern "C" void InputField_UnityEngine_UI_ICanvasElement_IsDestroyed_m906 ();
extern "C" void InputField_UnityEngine_UI_ICanvasElement_get_transform_m907 ();
extern "C" void MaskableGraphic__ctor_m908 ();
extern "C" void MaskableGraphic_get_maskable_m909 ();
extern "C" void MaskableGraphic_set_maskable_m910 ();
extern "C" void MaskableGraphic_get_material_m911 ();
extern "C" void MaskableGraphic_set_material_m912 ();
extern "C" void MaskableGraphic_UpdateInternalState_m913 ();
extern "C" void MaskableGraphic_OnEnable_m914 ();
extern "C" void MaskableGraphic_OnDisable_m915 ();
extern "C" void MaskableGraphic_OnTransformParentChanged_m916 ();
extern "C" void MaskableGraphic_ParentMaskStateChanged_m917 ();
extern "C" void MaskableGraphic_ClearMaskMaterial_m918 ();
extern "C" void MaskableGraphic_SetMaterialDirty_m919 ();
extern "C" void MaskableGraphic_GetStencilForGraphic_m920 ();
extern "C" void Misc_Destroy_m921 ();
extern "C" void Misc_DestroyImmediate_m922 ();
extern "C" void Navigation_get_mode_m923 ();
extern "C" void Navigation_set_mode_m924 ();
extern "C" void Navigation_get_selectOnUp_m925 ();
extern "C" void Navigation_set_selectOnUp_m926 ();
extern "C" void Navigation_get_selectOnDown_m927 ();
extern "C" void Navigation_set_selectOnDown_m928 ();
extern "C" void Navigation_get_selectOnLeft_m929 ();
extern "C" void Navigation_set_selectOnLeft_m930 ();
extern "C" void Navigation_get_selectOnRight_m931 ();
extern "C" void Navigation_set_selectOnRight_m932 ();
extern "C" void Navigation_get_defaultNavigation_m933 ();
extern "C" void RawImage__ctor_m934 ();
extern "C" void RawImage_get_mainTexture_m935 ();
extern "C" void RawImage_get_texture_m936 ();
extern "C" void RawImage_set_texture_m937 ();
extern "C" void RawImage_get_uvRect_m938 ();
extern "C" void RawImage_set_uvRect_m939 ();
extern "C" void RawImage_SetNativeSize_m940 ();
extern "C" void RawImage_OnFillVBO_m941 ();
extern "C" void ScrollEvent__ctor_m942 ();
extern "C" void U3CClickRepeatU3Ec__Iterator4__ctor_m943 ();
extern "C" void U3CClickRepeatU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m944 ();
extern "C" void U3CClickRepeatU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m945 ();
extern "C" void U3CClickRepeatU3Ec__Iterator4_MoveNext_m946 ();
extern "C" void U3CClickRepeatU3Ec__Iterator4_Dispose_m947 ();
extern "C" void U3CClickRepeatU3Ec__Iterator4_Reset_m948 ();
extern "C" void Scrollbar__ctor_m949 ();
extern "C" void Scrollbar_get_handleRect_m950 ();
extern "C" void Scrollbar_set_handleRect_m951 ();
extern "C" void Scrollbar_get_direction_m952 ();
extern "C" void Scrollbar_set_direction_m953 ();
extern "C" void Scrollbar_get_value_m954 ();
extern "C" void Scrollbar_set_value_m955 ();
extern "C" void Scrollbar_get_size_m956 ();
extern "C" void Scrollbar_set_size_m957 ();
extern "C" void Scrollbar_get_numberOfSteps_m958 ();
extern "C" void Scrollbar_set_numberOfSteps_m959 ();
extern "C" void Scrollbar_get_onValueChanged_m960 ();
extern "C" void Scrollbar_set_onValueChanged_m961 ();
extern "C" void Scrollbar_get_stepSize_m962 ();
extern "C" void Scrollbar_Rebuild_m963 ();
extern "C" void Scrollbar_OnEnable_m964 ();
extern "C" void Scrollbar_OnDisable_m965 ();
extern "C" void Scrollbar_UpdateCachedReferences_m966 ();
extern "C" void Scrollbar_Set_m967 ();
extern "C" void Scrollbar_Set_m968 ();
extern "C" void Scrollbar_OnRectTransformDimensionsChange_m969 ();
extern "C" void Scrollbar_get_axis_m970 ();
extern "C" void Scrollbar_get_reverseValue_m971 ();
extern "C" void Scrollbar_UpdateVisuals_m972 ();
extern "C" void Scrollbar_UpdateDrag_m973 ();
extern "C" void Scrollbar_MayDrag_m974 ();
extern "C" void Scrollbar_OnBeginDrag_m975 ();
extern "C" void Scrollbar_OnDrag_m976 ();
extern "C" void Scrollbar_OnPointerDown_m977 ();
extern "C" void Scrollbar_ClickRepeat_m978 ();
extern "C" void Scrollbar_OnPointerUp_m979 ();
extern "C" void Scrollbar_OnMove_m980 ();
extern "C" void Scrollbar_FindSelectableOnLeft_m981 ();
extern "C" void Scrollbar_FindSelectableOnRight_m982 ();
extern "C" void Scrollbar_FindSelectableOnUp_m983 ();
extern "C" void Scrollbar_FindSelectableOnDown_m984 ();
extern "C" void Scrollbar_OnInitializePotentialDrag_m985 ();
extern "C" void Scrollbar_SetDirection_m986 ();
extern "C" void Scrollbar_UnityEngine_UI_ICanvasElement_IsDestroyed_m987 ();
extern "C" void Scrollbar_UnityEngine_UI_ICanvasElement_get_transform_m988 ();
extern "C" void ScrollRectEvent__ctor_m989 ();
extern "C" void ScrollRect__ctor_m990 ();
extern "C" void ScrollRect_get_content_m991 ();
extern "C" void ScrollRect_set_content_m992 ();
extern "C" void ScrollRect_get_horizontal_m993 ();
extern "C" void ScrollRect_set_horizontal_m994 ();
extern "C" void ScrollRect_get_vertical_m995 ();
extern "C" void ScrollRect_set_vertical_m996 ();
extern "C" void ScrollRect_get_movementType_m997 ();
extern "C" void ScrollRect_set_movementType_m998 ();
extern "C" void ScrollRect_get_elasticity_m999 ();
extern "C" void ScrollRect_set_elasticity_m1000 ();
extern "C" void ScrollRect_get_inertia_m1001 ();
extern "C" void ScrollRect_set_inertia_m1002 ();
extern "C" void ScrollRect_get_decelerationRate_m1003 ();
extern "C" void ScrollRect_set_decelerationRate_m1004 ();
extern "C" void ScrollRect_get_scrollSensitivity_m1005 ();
extern "C" void ScrollRect_set_scrollSensitivity_m1006 ();
extern "C" void ScrollRect_get_horizontalScrollbar_m1007 ();
extern "C" void ScrollRect_set_horizontalScrollbar_m1008 ();
extern "C" void ScrollRect_get_verticalScrollbar_m1009 ();
extern "C" void ScrollRect_set_verticalScrollbar_m1010 ();
extern "C" void ScrollRect_get_onValueChanged_m1011 ();
extern "C" void ScrollRect_set_onValueChanged_m1012 ();
extern "C" void ScrollRect_get_viewRect_m1013 ();
extern "C" void ScrollRect_get_velocity_m1014 ();
extern "C" void ScrollRect_set_velocity_m1015 ();
extern "C" void ScrollRect_Rebuild_m1016 ();
extern "C" void ScrollRect_OnEnable_m1017 ();
extern "C" void ScrollRect_OnDisable_m1018 ();
extern "C" void ScrollRect_IsActive_m1019 ();
extern "C" void ScrollRect_EnsureLayoutHasRebuilt_m1020 ();
extern "C" void ScrollRect_StopMovement_m1021 ();
extern "C" void ScrollRect_OnScroll_m1022 ();
extern "C" void ScrollRect_OnInitializePotentialDrag_m1023 ();
extern "C" void ScrollRect_OnBeginDrag_m1024 ();
extern "C" void ScrollRect_OnEndDrag_m1025 ();
extern "C" void ScrollRect_OnDrag_m1026 ();
extern "C" void ScrollRect_SetContentAnchoredPosition_m1027 ();
extern "C" void ScrollRect_LateUpdate_m1028 ();
extern "C" void ScrollRect_UpdatePrevData_m1029 ();
extern "C" void ScrollRect_UpdateScrollbars_m1030 ();
extern "C" void ScrollRect_get_normalizedPosition_m1031 ();
extern "C" void ScrollRect_set_normalizedPosition_m1032 ();
extern "C" void ScrollRect_get_horizontalNormalizedPosition_m1033 ();
extern "C" void ScrollRect_set_horizontalNormalizedPosition_m1034 ();
extern "C" void ScrollRect_get_verticalNormalizedPosition_m240 ();
extern "C" void ScrollRect_set_verticalNormalizedPosition_m216 ();
extern "C" void ScrollRect_SetHorizontalNormalizedPosition_m1035 ();
extern "C" void ScrollRect_SetVerticalNormalizedPosition_m1036 ();
extern "C" void ScrollRect_SetNormalizedPosition_m1037 ();
extern "C" void ScrollRect_RubberDelta_m1038 ();
extern "C" void ScrollRect_UpdateBounds_m1039 ();
extern "C" void ScrollRect_GetBounds_m1040 ();
extern "C" void ScrollRect_CalculateOffset_m1041 ();
extern "C" void ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1042 ();
extern "C" void ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1043 ();
extern "C" void Selectable__ctor_m1044 ();
extern "C" void Selectable__cctor_m1045 ();
extern "C" void Selectable_get_allSelectables_m1046 ();
extern "C" void Selectable_get_navigation_m1047 ();
extern "C" void Selectable_set_navigation_m1048 ();
extern "C" void Selectable_get_transition_m1049 ();
extern "C" void Selectable_set_transition_m1050 ();
extern "C" void Selectable_get_colors_m1051 ();
extern "C" void Selectable_set_colors_m1052 ();
extern "C" void Selectable_get_spriteState_m1053 ();
extern "C" void Selectable_set_spriteState_m1054 ();
extern "C" void Selectable_get_animationTriggers_m1055 ();
extern "C" void Selectable_set_animationTriggers_m1056 ();
extern "C" void Selectable_get_targetGraphic_m1057 ();
extern "C" void Selectable_set_targetGraphic_m1058 ();
extern "C" void Selectable_get_interactable_m1059 ();
extern "C" void Selectable_set_interactable_m1060 ();
extern "C" void Selectable_get_isPointerInside_m1061 ();
extern "C" void Selectable_set_isPointerInside_m1062 ();
extern "C" void Selectable_get_isPointerDown_m1063 ();
extern "C" void Selectable_set_isPointerDown_m1064 ();
extern "C" void Selectable_get_hasSelection_m1065 ();
extern "C" void Selectable_set_hasSelection_m1066 ();
extern "C" void Selectable_get_image_m1067 ();
extern "C" void Selectable_set_image_m1068 ();
extern "C" void Selectable_get_animator_m1069 ();
extern "C" void Selectable_Awake_m1070 ();
extern "C" void Selectable_OnCanvasGroupChanged_m1071 ();
extern "C" void Selectable_IsInteractable_m1072 ();
extern "C" void Selectable_OnDidApplyAnimationProperties_m1073 ();
extern "C" void Selectable_OnEnable_m1074 ();
extern "C" void Selectable_OnSetProperty_m1075 ();
extern "C" void Selectable_OnDisable_m1076 ();
extern "C" void Selectable_get_currentSelectionState_m1077 ();
extern "C" void Selectable_InstantClearState_m1078 ();
extern "C" void Selectable_DoStateTransition_m1079 ();
extern "C" void Selectable_FindSelectable_m1080 ();
extern "C" void Selectable_GetPointOnRectEdge_m1081 ();
extern "C" void Selectable_Navigate_m1082 ();
extern "C" void Selectable_FindSelectableOnLeft_m1083 ();
extern "C" void Selectable_FindSelectableOnRight_m1084 ();
extern "C" void Selectable_FindSelectableOnUp_m1085 ();
extern "C" void Selectable_FindSelectableOnDown_m1086 ();
extern "C" void Selectable_OnMove_m1087 ();
extern "C" void Selectable_StartColorTween_m1088 ();
extern "C" void Selectable_DoSpriteSwap_m1089 ();
extern "C" void Selectable_TriggerAnimation_m1090 ();
extern "C" void Selectable_IsHighlighted_m1091 ();
extern "C" void Selectable_IsPressed_m1092 ();
extern "C" void Selectable_IsPressed_m1093 ();
extern "C" void Selectable_UpdateSelectionState_m1094 ();
extern "C" void Selectable_EvaluateAndTransitionToSelectionState_m1095 ();
extern "C" void Selectable_InternalEvaluateAndTransitionToSelectionState_m1096 ();
extern "C" void Selectable_OnPointerDown_m1097 ();
extern "C" void Selectable_OnPointerUp_m1098 ();
extern "C" void Selectable_OnPointerEnter_m1099 ();
extern "C" void Selectable_OnPointerExit_m1100 ();
extern "C" void Selectable_OnSelect_m1101 ();
extern "C" void Selectable_OnDeselect_m1102 ();
extern "C" void Selectable_Select_m1103 ();
extern "C" void SetPropertyUtility_SetColor_m1104 ();
extern "C" void SliderEvent__ctor_m1105 ();
extern "C" void Slider__ctor_m1106 ();
extern "C" void Slider_get_fillRect_m1107 ();
extern "C" void Slider_set_fillRect_m1108 ();
extern "C" void Slider_get_handleRect_m1109 ();
extern "C" void Slider_set_handleRect_m1110 ();
extern "C" void Slider_get_direction_m1111 ();
extern "C" void Slider_set_direction_m1112 ();
extern "C" void Slider_get_minValue_m1113 ();
extern "C" void Slider_set_minValue_m1114 ();
extern "C" void Slider_get_maxValue_m1115 ();
extern "C" void Slider_set_maxValue_m1116 ();
extern "C" void Slider_get_wholeNumbers_m1117 ();
extern "C" void Slider_set_wholeNumbers_m1118 ();
extern "C" void Slider_get_value_m1119 ();
extern "C" void Slider_set_value_m1120 ();
extern "C" void Slider_get_normalizedValue_m1121 ();
extern "C" void Slider_set_normalizedValue_m1122 ();
extern "C" void Slider_get_onValueChanged_m1123 ();
extern "C" void Slider_set_onValueChanged_m1124 ();
extern "C" void Slider_get_stepSize_m1125 ();
extern "C" void Slider_Rebuild_m1126 ();
extern "C" void Slider_OnEnable_m1127 ();
extern "C" void Slider_OnDisable_m1128 ();
extern "C" void Slider_OnDidApplyAnimationProperties_m1129 ();
extern "C" void Slider_UpdateCachedReferences_m1130 ();
extern "C" void Slider_ClampValue_m1131 ();
extern "C" void Slider_Set_m1132 ();
extern "C" void Slider_Set_m1133 ();
extern "C" void Slider_OnRectTransformDimensionsChange_m1134 ();
extern "C" void Slider_get_axis_m1135 ();
extern "C" void Slider_get_reverseValue_m1136 ();
extern "C" void Slider_UpdateVisuals_m1137 ();
extern "C" void Slider_UpdateDrag_m1138 ();
extern "C" void Slider_MayDrag_m1139 ();
extern "C" void Slider_OnPointerDown_m1140 ();
extern "C" void Slider_OnDrag_m1141 ();
extern "C" void Slider_OnMove_m1142 ();
extern "C" void Slider_FindSelectableOnLeft_m1143 ();
extern "C" void Slider_FindSelectableOnRight_m1144 ();
extern "C" void Slider_FindSelectableOnUp_m1145 ();
extern "C" void Slider_FindSelectableOnDown_m1146 ();
extern "C" void Slider_OnInitializePotentialDrag_m1147 ();
extern "C" void Slider_SetDirection_m1148 ();
extern "C" void Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1149 ();
extern "C" void Slider_UnityEngine_UI_ICanvasElement_get_transform_m1150 ();
extern "C" void SpriteState_get_highlightedSprite_m1151 ();
extern "C" void SpriteState_set_highlightedSprite_m1152 ();
extern "C" void SpriteState_get_pressedSprite_m1153 ();
extern "C" void SpriteState_set_pressedSprite_m1154 ();
extern "C" void SpriteState_get_disabledSprite_m1155 ();
extern "C" void SpriteState_set_disabledSprite_m1156 ();
extern "C" void MatEntry__ctor_m1157 ();
extern "C" void StencilMaterial__cctor_m1158 ();
extern "C" void StencilMaterial_Add_m1159 ();
extern "C" void StencilMaterial_Remove_m1160 ();
extern "C" void Text__ctor_m1161 ();
extern "C" void Text__cctor_m1162 ();
extern "C" void Text_get_cachedTextGenerator_m1163 ();
extern "C" void Text_get_cachedTextGeneratorForLayout_m1164 ();
extern "C" void Text_get_defaultMaterial_m1165 ();
extern "C" void Text_get_mainTexture_m1166 ();
extern "C" void Text_FontTextureChanged_m1167 ();
extern "C" void Text_get_font_m1168 ();
extern "C" void Text_set_font_m1169 ();
extern "C" void Text_get_text_m1170 ();
extern "C" void Text_set_text_m1171 ();
extern "C" void Text_get_supportRichText_m1172 ();
extern "C" void Text_set_supportRichText_m1173 ();
extern "C" void Text_get_resizeTextForBestFit_m1174 ();
extern "C" void Text_set_resizeTextForBestFit_m1175 ();
extern "C" void Text_get_resizeTextMinSize_m1176 ();
extern "C" void Text_set_resizeTextMinSize_m1177 ();
extern "C" void Text_get_resizeTextMaxSize_m1178 ();
extern "C" void Text_set_resizeTextMaxSize_m1179 ();
extern "C" void Text_get_alignment_m1180 ();
extern "C" void Text_set_alignment_m1181 ();
extern "C" void Text_get_fontSize_m1182 ();
extern "C" void Text_set_fontSize_m1183 ();
extern "C" void Text_get_horizontalOverflow_m1184 ();
extern "C" void Text_set_horizontalOverflow_m1185 ();
extern "C" void Text_get_verticalOverflow_m1186 ();
extern "C" void Text_set_verticalOverflow_m1187 ();
extern "C" void Text_get_lineSpacing_m1188 ();
extern "C" void Text_set_lineSpacing_m1189 ();
extern "C" void Text_get_fontStyle_m1190 ();
extern "C" void Text_set_fontStyle_m1191 ();
extern "C" void Text_get_pixelsPerUnit_m1192 ();
extern "C" void Text_OnEnable_m1193 ();
extern "C" void Text_OnDisable_m1194 ();
extern "C" void Text_UpdateGeometry_m1195 ();
extern "C" void Text_GetGenerationSettings_m1196 ();
extern "C" void Text_GetTextAnchorPivot_m1197 ();
extern "C" void Text_OnFillVBO_m1198 ();
extern "C" void Text_CalculateLayoutInputHorizontal_m1199 ();
extern "C" void Text_CalculateLayoutInputVertical_m1200 ();
extern "C" void Text_get_minWidth_m1201 ();
extern "C" void Text_get_preferredWidth_m1202 ();
extern "C" void Text_get_flexibleWidth_m1203 ();
extern "C" void Text_get_minHeight_m1204 ();
extern "C" void Text_get_preferredHeight_m1205 ();
extern "C" void Text_get_flexibleHeight_m1206 ();
extern "C" void Text_get_layoutPriority_m1207 ();
extern "C" void ToggleEvent__ctor_m1208 ();
extern "C" void Toggle__ctor_m1209 ();
extern "C" void Toggle_get_group_m1210 ();
extern "C" void Toggle_set_group_m1211 ();
extern "C" void Toggle_Rebuild_m1212 ();
extern "C" void Toggle_OnEnable_m1213 ();
extern "C" void Toggle_OnDisable_m1214 ();
extern "C" void Toggle_OnDidApplyAnimationProperties_m1215 ();
extern "C" void Toggle_SetToggleGroup_m1216 ();
extern "C" void Toggle_get_isOn_m1217 ();
extern "C" void Toggle_set_isOn_m1218 ();
extern "C" void Toggle_Set_m1219 ();
extern "C" void Toggle_Set_m1220 ();
extern "C" void Toggle_PlayEffect_m1221 ();
extern "C" void Toggle_Start_m1222 ();
extern "C" void Toggle_InternalToggle_m1223 ();
extern "C" void Toggle_OnPointerClick_m1224 ();
extern "C" void Toggle_OnSubmit_m1225 ();
extern "C" void Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1226 ();
extern "C" void Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1227 ();
extern "C" void ToggleGroup__ctor_m1228 ();
extern "C" void ToggleGroup_get_allowSwitchOff_m1229 ();
extern "C" void ToggleGroup_set_allowSwitchOff_m1230 ();
extern "C" void ToggleGroup_ValidateToggleIsInGroup_m1231 ();
extern "C" void ToggleGroup_NotifyToggleOn_m1232 ();
extern "C" void ToggleGroup_UnregisterToggle_m1233 ();
extern "C" void ToggleGroup_RegisterToggle_m1234 ();
extern "C" void ToggleGroup_AnyTogglesOn_m1235 ();
extern "C" void ToggleGroup_ActiveToggles_m1236 ();
extern "C" void ToggleGroup_SetAllTogglesOff_m1237 ();
extern "C" void ToggleGroup_U3CAnyTogglesOnU3Em__7_m1238 ();
extern "C" void ToggleGroup_U3CActiveTogglesU3Em__8_m1239 ();
extern "C" void AspectRatioFitter__ctor_m1240 ();
extern "C" void AspectRatioFitter_get_aspectMode_m1241 ();
extern "C" void AspectRatioFitter_set_aspectMode_m1242 ();
extern "C" void AspectRatioFitter_get_aspectRatio_m1243 ();
extern "C" void AspectRatioFitter_set_aspectRatio_m1244 ();
extern "C" void AspectRatioFitter_get_rectTransform_m1245 ();
extern "C" void AspectRatioFitter_OnEnable_m1246 ();
extern "C" void AspectRatioFitter_OnDisable_m1247 ();
extern "C" void AspectRatioFitter_OnRectTransformDimensionsChange_m1248 ();
extern "C" void AspectRatioFitter_UpdateRect_m1249 ();
extern "C" void AspectRatioFitter_GetSizeDeltaToProduceSize_m1250 ();
extern "C" void AspectRatioFitter_GetParentSize_m1251 ();
extern "C" void AspectRatioFitter_SetLayoutHorizontal_m1252 ();
extern "C" void AspectRatioFitter_SetLayoutVertical_m1253 ();
extern "C" void AspectRatioFitter_SetDirty_m1254 ();
extern "C" void CanvasScaler__ctor_m1255 ();
extern "C" void CanvasScaler_get_uiScaleMode_m1256 ();
extern "C" void CanvasScaler_set_uiScaleMode_m1257 ();
extern "C" void CanvasScaler_get_referencePixelsPerUnit_m1258 ();
extern "C" void CanvasScaler_set_referencePixelsPerUnit_m1259 ();
extern "C" void CanvasScaler_get_scaleFactor_m1260 ();
extern "C" void CanvasScaler_set_scaleFactor_m1261 ();
extern "C" void CanvasScaler_get_referenceResolution_m1262 ();
extern "C" void CanvasScaler_set_referenceResolution_m1263 ();
extern "C" void CanvasScaler_get_screenMatchMode_m1264 ();
extern "C" void CanvasScaler_set_screenMatchMode_m1265 ();
extern "C" void CanvasScaler_get_matchWidthOrHeight_m1266 ();
extern "C" void CanvasScaler_set_matchWidthOrHeight_m1267 ();
extern "C" void CanvasScaler_get_physicalUnit_m1268 ();
extern "C" void CanvasScaler_set_physicalUnit_m1269 ();
extern "C" void CanvasScaler_get_fallbackScreenDPI_m1270 ();
extern "C" void CanvasScaler_set_fallbackScreenDPI_m1271 ();
extern "C" void CanvasScaler_get_defaultSpriteDPI_m1272 ();
extern "C" void CanvasScaler_set_defaultSpriteDPI_m1273 ();
extern "C" void CanvasScaler_get_dynamicPixelsPerUnit_m1274 ();
extern "C" void CanvasScaler_set_dynamicPixelsPerUnit_m1275 ();
extern "C" void CanvasScaler_OnEnable_m1276 ();
extern "C" void CanvasScaler_OnDisable_m1277 ();
extern "C" void CanvasScaler_Update_m1278 ();
extern "C" void CanvasScaler_Handle_m1279 ();
extern "C" void CanvasScaler_HandleWorldCanvas_m1280 ();
extern "C" void CanvasScaler_HandleConstantPixelSize_m1281 ();
extern "C" void CanvasScaler_HandleScaleWithScreenSize_m1282 ();
extern "C" void CanvasScaler_HandleConstantPhysicalSize_m1283 ();
extern "C" void CanvasScaler_SetScaleFactor_m1284 ();
extern "C" void CanvasScaler_SetReferencePixelsPerUnit_m1285 ();
extern "C" void ContentSizeFitter__ctor_m1286 ();
extern "C" void ContentSizeFitter_get_horizontalFit_m1287 ();
extern "C" void ContentSizeFitter_set_horizontalFit_m1288 ();
extern "C" void ContentSizeFitter_get_verticalFit_m1289 ();
extern "C" void ContentSizeFitter_set_verticalFit_m1290 ();
extern "C" void ContentSizeFitter_get_rectTransform_m1291 ();
extern "C" void ContentSizeFitter_OnEnable_m1292 ();
extern "C" void ContentSizeFitter_OnDisable_m1293 ();
extern "C" void ContentSizeFitter_OnRectTransformDimensionsChange_m1294 ();
extern "C" void ContentSizeFitter_HandleSelfFittingAlongAxis_m1295 ();
extern "C" void ContentSizeFitter_SetLayoutHorizontal_m1296 ();
extern "C" void ContentSizeFitter_SetLayoutVertical_m1297 ();
extern "C" void ContentSizeFitter_SetDirty_m1298 ();
extern "C" void GridLayoutGroup__ctor_m1299 ();
extern "C" void GridLayoutGroup_get_startCorner_m1300 ();
extern "C" void GridLayoutGroup_set_startCorner_m1301 ();
extern "C" void GridLayoutGroup_get_startAxis_m1302 ();
extern "C" void GridLayoutGroup_set_startAxis_m1303 ();
extern "C" void GridLayoutGroup_get_cellSize_m229 ();
extern "C" void GridLayoutGroup_set_cellSize_m1304 ();
extern "C" void GridLayoutGroup_get_spacing_m230 ();
extern "C" void GridLayoutGroup_set_spacing_m1305 ();
extern "C" void GridLayoutGroup_get_constraint_m1306 ();
extern "C" void GridLayoutGroup_set_constraint_m1307 ();
extern "C" void GridLayoutGroup_get_constraintCount_m1308 ();
extern "C" void GridLayoutGroup_set_constraintCount_m1309 ();
extern "C" void GridLayoutGroup_CalculateLayoutInputHorizontal_m1310 ();
extern "C" void GridLayoutGroup_CalculateLayoutInputVertical_m1311 ();
extern "C" void GridLayoutGroup_SetLayoutHorizontal_m1312 ();
extern "C" void GridLayoutGroup_SetLayoutVertical_m1313 ();
extern "C" void GridLayoutGroup_SetCellsAlongAxis_m1314 ();
extern "C" void HorizontalLayoutGroup__ctor_m1315 ();
extern "C" void HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1316 ();
extern "C" void HorizontalLayoutGroup_CalculateLayoutInputVertical_m1317 ();
extern "C" void HorizontalLayoutGroup_SetLayoutHorizontal_m1318 ();
extern "C" void HorizontalLayoutGroup_SetLayoutVertical_m1319 ();
extern "C" void HorizontalOrVerticalLayoutGroup__ctor_m1320 ();
extern "C" void HorizontalOrVerticalLayoutGroup_get_spacing_m1321 ();
extern "C" void HorizontalOrVerticalLayoutGroup_set_spacing_m1322 ();
extern "C" void HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1323 ();
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1324 ();
extern "C" void HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1325 ();
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1326 ();
extern "C" void HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1327 ();
extern "C" void HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1328 ();
extern "C" void LayoutElement__ctor_m1329 ();
extern "C" void LayoutElement_get_ignoreLayout_m1330 ();
extern "C" void LayoutElement_set_ignoreLayout_m1331 ();
extern "C" void LayoutElement_CalculateLayoutInputHorizontal_m1332 ();
extern "C" void LayoutElement_CalculateLayoutInputVertical_m1333 ();
extern "C" void LayoutElement_get_minWidth_m1334 ();
extern "C" void LayoutElement_set_minWidth_m1335 ();
extern "C" void LayoutElement_get_minHeight_m1336 ();
extern "C" void LayoutElement_set_minHeight_m1337 ();
extern "C" void LayoutElement_get_preferredWidth_m1338 ();
extern "C" void LayoutElement_set_preferredWidth_m1339 ();
extern "C" void LayoutElement_get_preferredHeight_m1340 ();
extern "C" void LayoutElement_set_preferredHeight_m1341 ();
extern "C" void LayoutElement_get_flexibleWidth_m1342 ();
extern "C" void LayoutElement_set_flexibleWidth_m1343 ();
extern "C" void LayoutElement_get_flexibleHeight_m1344 ();
extern "C" void LayoutElement_set_flexibleHeight_m1345 ();
extern "C" void LayoutElement_get_layoutPriority_m1346 ();
extern "C" void LayoutElement_OnEnable_m1347 ();
extern "C" void LayoutElement_OnTransformParentChanged_m1348 ();
extern "C" void LayoutElement_OnDisable_m1349 ();
extern "C" void LayoutElement_OnDidApplyAnimationProperties_m1350 ();
extern "C" void LayoutElement_OnBeforeTransformParentChanged_m1351 ();
extern "C" void LayoutElement_SetDirty_m1352 ();
extern "C" void LayoutGroup__ctor_m1353 ();
extern "C" void LayoutGroup_get_padding_m1354 ();
extern "C" void LayoutGroup_set_padding_m1355 ();
extern "C" void LayoutGroup_get_childAlignment_m1356 ();
extern "C" void LayoutGroup_set_childAlignment_m1357 ();
extern "C" void LayoutGroup_get_rectTransform_m1358 ();
extern "C" void LayoutGroup_get_rectChildren_m1359 ();
extern "C" void LayoutGroup_CalculateLayoutInputHorizontal_m1360 ();
extern "C" void LayoutGroup_get_minWidth_m1361 ();
extern "C" void LayoutGroup_get_preferredWidth_m1362 ();
extern "C" void LayoutGroup_get_flexibleWidth_m1363 ();
extern "C" void LayoutGroup_get_minHeight_m1364 ();
extern "C" void LayoutGroup_get_preferredHeight_m1365 ();
extern "C" void LayoutGroup_get_flexibleHeight_m1366 ();
extern "C" void LayoutGroup_get_layoutPriority_m1367 ();
extern "C" void LayoutGroup_OnEnable_m1368 ();
extern "C" void LayoutGroup_OnDisable_m1369 ();
extern "C" void LayoutGroup_OnDidApplyAnimationProperties_m1370 ();
extern "C" void LayoutGroup_GetTotalMinSize_m1371 ();
extern "C" void LayoutGroup_GetTotalPreferredSize_m1372 ();
extern "C" void LayoutGroup_GetTotalFlexibleSize_m1373 ();
extern "C" void LayoutGroup_GetStartOffset_m1374 ();
extern "C" void LayoutGroup_SetLayoutInputForAxis_m1375 ();
extern "C" void LayoutGroup_SetChildAlongAxis_m1376 ();
extern "C" void LayoutGroup_get_isRootLayoutGroup_m1377 ();
extern "C" void LayoutGroup_OnRectTransformDimensionsChange_m1378 ();
extern "C" void LayoutGroup_OnTransformChildrenChanged_m1379 ();
extern "C" void LayoutGroup_SetDirty_m1380 ();
extern "C" void LayoutRebuilder__ctor_m1381 ();
extern "C" void LayoutRebuilder__cctor_m1382 ();
extern "C" void LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1383 ();
extern "C" void LayoutRebuilder_ReapplyDrivenProperties_m1384 ();
extern "C" void LayoutRebuilder_get_transform_m1385 ();
extern "C" void LayoutRebuilder_IsDestroyed_m1386 ();
extern "C" void LayoutRebuilder_StripDisabledBehavioursFromList_m1387 ();
extern "C" void LayoutRebuilder_PerformLayoutControl_m1388 ();
extern "C" void LayoutRebuilder_PerformLayoutCalculation_m1389 ();
extern "C" void LayoutRebuilder_MarkLayoutForRebuild_m1390 ();
extern "C" void LayoutRebuilder_ValidLayoutGroup_m1391 ();
extern "C" void LayoutRebuilder_ValidController_m1392 ();
extern "C" void LayoutRebuilder_MarkLayoutRootForRebuild_m1393 ();
extern "C" void LayoutRebuilder_Equals_m1394 ();
extern "C" void LayoutRebuilder_GetHashCode_m1395 ();
extern "C" void LayoutRebuilder_ToString_m1396 ();
extern "C" void LayoutRebuilder_U3CRebuildU3Em__9_m1397 ();
extern "C" void LayoutRebuilder_U3CRebuildU3Em__A_m1398 ();
extern "C" void LayoutRebuilder_U3CRebuildU3Em__B_m1399 ();
extern "C" void LayoutRebuilder_U3CRebuildU3Em__C_m1400 ();
extern "C" void LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1401 ();
extern "C" void LayoutUtility_GetMinSize_m1402 ();
extern "C" void LayoutUtility_GetPreferredSize_m1403 ();
extern "C" void LayoutUtility_GetFlexibleSize_m1404 ();
extern "C" void LayoutUtility_GetMinWidth_m1405 ();
extern "C" void LayoutUtility_GetPreferredWidth_m1406 ();
extern "C" void LayoutUtility_GetFlexibleWidth_m1407 ();
extern "C" void LayoutUtility_GetMinHeight_m1408 ();
extern "C" void LayoutUtility_GetPreferredHeight_m1409 ();
extern "C" void LayoutUtility_GetFlexibleHeight_m1410 ();
extern "C" void LayoutUtility_GetLayoutProperty_m1411 ();
extern "C" void LayoutUtility_GetLayoutProperty_m1412 ();
extern "C" void LayoutUtility_U3CGetMinWidthU3Em__E_m1413 ();
extern "C" void LayoutUtility_U3CGetPreferredWidthU3Em__F_m1414 ();
extern "C" void LayoutUtility_U3CGetPreferredWidthU3Em__10_m1415 ();
extern "C" void LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1416 ();
extern "C" void LayoutUtility_U3CGetMinHeightU3Em__12_m1417 ();
extern "C" void LayoutUtility_U3CGetPreferredHeightU3Em__13_m1418 ();
extern "C" void LayoutUtility_U3CGetPreferredHeightU3Em__14_m1419 ();
extern "C" void LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1420 ();
extern "C" void VerticalLayoutGroup__ctor_m1421 ();
extern "C" void VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1422 ();
extern "C" void VerticalLayoutGroup_CalculateLayoutInputVertical_m1423 ();
extern "C" void VerticalLayoutGroup_SetLayoutHorizontal_m1424 ();
extern "C" void VerticalLayoutGroup_SetLayoutVertical_m1425 ();
extern "C" void Mask__ctor_m1426 ();
extern "C" void Mask_get_graphic_m1427 ();
extern "C" void Mask_get_showMaskGraphic_m1428 ();
extern "C" void Mask_set_showMaskGraphic_m1429 ();
extern "C" void Mask_get_rectTransform_m1430 ();
extern "C" void Mask_MaskEnabled_m1431 ();
extern "C" void Mask_OnSiblingGraphicEnabledDisabled_m1432 ();
extern "C" void Mask_NotifyMaskStateChanged_m1433 ();
extern "C" void Mask_ClearCachedMaterial_m1434 ();
extern "C" void Mask_OnEnable_m1435 ();
extern "C" void Mask_OnDisable_m1436 ();
extern "C" void Mask_IsRaycastLocationValid_m1437 ();
extern "C" void Mask_GetModifiedMaterial_m1438 ();
extern "C" void CanvasListPool__cctor_m1439 ();
extern "C" void CanvasListPool_Get_m1440 ();
extern "C" void CanvasListPool_Release_m1441 ();
extern "C" void CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1442 ();
extern "C" void ComponentListPool__cctor_m1443 ();
extern "C" void ComponentListPool_Get_m1444 ();
extern "C" void ComponentListPool_Release_m1445 ();
extern "C" void ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1446 ();
extern "C" void BaseVertexEffect__ctor_m1447 ();
extern "C" void BaseVertexEffect_get_graphic_m1448 ();
extern "C" void BaseVertexEffect_OnEnable_m1449 ();
extern "C" void BaseVertexEffect_OnDisable_m1450 ();
extern "C" void BaseVertexEffect_OnDidApplyAnimationProperties_m1451 ();
extern "C" void Outline__ctor_m1452 ();
extern "C" void Outline_ModifyVertices_m1453 ();
extern "C" void PositionAsUV1__ctor_m1454 ();
extern "C" void PositionAsUV1_ModifyVertices_m1455 ();
extern "C" void Shadow__ctor_m1456 ();
extern "C" void Shadow_get_effectColor_m1457 ();
extern "C" void Shadow_set_effectColor_m1458 ();
extern "C" void Shadow_get_effectDistance_m1459 ();
extern "C" void Shadow_set_effectDistance_m1460 ();
extern "C" void Shadow_get_useGraphicAlpha_m1461 ();
extern "C" void Shadow_set_useGraphicAlpha_m1462 ();
extern "C" void Shadow_ApplyShadow_m1463 ();
extern "C" void Shadow_ModifyVertices_m1464 ();
extern "C" void AssetBundleCreateRequest__ctor_m1983 ();
extern "C" void AssetBundleCreateRequest_get_assetBundle_m1984 ();
extern "C" void AssetBundleCreateRequest_DisableCompatibilityChecks_m1985 ();
extern "C" void AssetBundleRequest__ctor_m1986 ();
extern "C" void AssetBundleRequest_get_asset_m1987 ();
extern "C" void AssetBundleRequest_get_allAssets_m1988 ();
extern "C" void AssetBundle_LoadAsset_m1989 ();
extern "C" void AssetBundle_LoadAsset_Internal_m1990 ();
extern "C" void AssetBundle_LoadAssetWithSubAssets_Internal_m1991 ();
extern "C" void LayerMask_get_value_m1992 ();
extern "C" void LayerMask_set_value_m1993 ();
extern "C" void LayerMask_LayerToName_m1994 ();
extern "C" void LayerMask_NameToLayer_m1995 ();
extern "C" void LayerMask_GetMask_m1996 ();
extern "C" void LayerMask_op_Implicit_m267 ();
extern "C" void LayerMask_op_Implicit_m1585 ();
extern "C" void SystemInfo_get_deviceUniqueIdentifier_m1997 ();
extern "C" void WaitForSeconds__ctor_m270 ();
extern "C" void WaitForFixedUpdate__ctor_m1998 ();
extern "C" void WaitForEndOfFrame__ctor_m1839 ();
extern "C" void Coroutine__ctor_m1999 ();
extern "C" void Coroutine_ReleaseCoroutine_m2000 ();
extern "C" void Coroutine_Finalize_m2001 ();
extern "C" void ScriptableObject__ctor_m2002 ();
extern "C" void ScriptableObject_Internal_CreateScriptableObject_m2003 ();
extern "C" void ScriptableObject_CreateInstance_m2004 ();
extern "C" void ScriptableObject_CreateInstance_m2005 ();
extern "C" void ScriptableObject_CreateInstanceFromType_m2006 ();
extern "C" void UnhandledExceptionHandler__ctor_m2007 ();
extern "C" void UnhandledExceptionHandler_RegisterUECatcher_m2008 ();
extern "C" void UnhandledExceptionHandler_HandleUnhandledException_m2009 ();
extern "C" void UnhandledExceptionHandler_PrintException_m2010 ();
extern "C" void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m2011 ();
extern "C" void GameCenterPlatform__ctor_m2012 ();
extern "C" void GameCenterPlatform__cctor_m2013 ();
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m2014 ();
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m2015 ();
extern "C" void GameCenterPlatform_Internal_Authenticate_m2016 ();
extern "C" void GameCenterPlatform_Internal_Authenticated_m2017 ();
extern "C" void GameCenterPlatform_Internal_UserName_m2018 ();
extern "C" void GameCenterPlatform_Internal_UserID_m2019 ();
extern "C" void GameCenterPlatform_Internal_Underage_m2020 ();
extern "C" void GameCenterPlatform_Internal_UserImage_m2021 ();
extern "C" void GameCenterPlatform_Internal_LoadFriends_m2022 ();
extern "C" void GameCenterPlatform_Internal_LoadAchievementDescriptions_m2023 ();
extern "C" void GameCenterPlatform_Internal_LoadAchievements_m2024 ();
extern "C" void GameCenterPlatform_Internal_ReportProgress_m2025 ();
extern "C" void GameCenterPlatform_Internal_ReportScore_m2026 ();
extern "C" void GameCenterPlatform_Internal_LoadScores_m2027 ();
extern "C" void GameCenterPlatform_Internal_ShowAchievementsUI_m2028 ();
extern "C" void GameCenterPlatform_Internal_ShowLeaderboardUI_m2029 ();
extern "C" void GameCenterPlatform_Internal_LoadUsers_m2030 ();
extern "C" void GameCenterPlatform_Internal_ResetAllAchievements_m2031 ();
extern "C" void GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m2032 ();
extern "C" void GameCenterPlatform_ResetAllAchievements_m2033 ();
extern "C" void GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m2034 ();
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m2035 ();
extern "C" void GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m2036 ();
extern "C" void GameCenterPlatform_ClearAchievementDescriptions_m2037 ();
extern "C" void GameCenterPlatform_SetAchievementDescription_m2038 ();
extern "C" void GameCenterPlatform_SetAchievementDescriptionImage_m2039 ();
extern "C" void GameCenterPlatform_TriggerAchievementDescriptionCallback_m2040 ();
extern "C" void GameCenterPlatform_AuthenticateCallbackWrapper_m2041 ();
extern "C" void GameCenterPlatform_ClearFriends_m2042 ();
extern "C" void GameCenterPlatform_SetFriends_m2043 ();
extern "C" void GameCenterPlatform_SetFriendImage_m2044 ();
extern "C" void GameCenterPlatform_TriggerFriendsCallbackWrapper_m2045 ();
extern "C" void GameCenterPlatform_AchievementCallbackWrapper_m2046 ();
extern "C" void GameCenterPlatform_ProgressCallbackWrapper_m2047 ();
extern "C" void GameCenterPlatform_ScoreCallbackWrapper_m2048 ();
extern "C" void GameCenterPlatform_ScoreLoaderCallbackWrapper_m2049 ();
extern "C" void GameCenterPlatform_get_localUser_m2050 ();
extern "C" void GameCenterPlatform_PopulateLocalUser_m2051 ();
extern "C" void GameCenterPlatform_LoadAchievementDescriptions_m2052 ();
extern "C" void GameCenterPlatform_ReportProgress_m2053 ();
extern "C" void GameCenterPlatform_LoadAchievements_m2054 ();
extern "C" void GameCenterPlatform_ReportScore_m2055 ();
extern "C" void GameCenterPlatform_LoadScores_m2056 ();
extern "C" void GameCenterPlatform_LoadScores_m2057 ();
extern "C" void GameCenterPlatform_LeaderboardCallbackWrapper_m2058 ();
extern "C" void GameCenterPlatform_GetLoading_m2059 ();
extern "C" void GameCenterPlatform_VerifyAuthentication_m2060 ();
extern "C" void GameCenterPlatform_ShowAchievementsUI_m2061 ();
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m2062 ();
extern "C" void GameCenterPlatform_ClearUsers_m2063 ();
extern "C" void GameCenterPlatform_SetUser_m2064 ();
extern "C" void GameCenterPlatform_SetUserImage_m2065 ();
extern "C" void GameCenterPlatform_TriggerUsersCallbackWrapper_m2066 ();
extern "C" void GameCenterPlatform_LoadUsers_m2067 ();
extern "C" void GameCenterPlatform_SafeSetUserImage_m2068 ();
extern "C" void GameCenterPlatform_SafeClearArray_m2069 ();
extern "C" void GameCenterPlatform_CreateLeaderboard_m2070 ();
extern "C" void GameCenterPlatform_CreateAchievement_m2071 ();
extern "C" void GameCenterPlatform_TriggerResetAchievementCallback_m2072 ();
extern "C" void GcLeaderboard__ctor_m2073 ();
extern "C" void GcLeaderboard_Finalize_m2074 ();
extern "C" void GcLeaderboard_Contains_m2075 ();
extern "C" void GcLeaderboard_SetScores_m2076 ();
extern "C" void GcLeaderboard_SetLocalScore_m2077 ();
extern "C" void GcLeaderboard_SetMaxRange_m2078 ();
extern "C" void GcLeaderboard_SetTitle_m2079 ();
extern "C" void GcLeaderboard_Internal_LoadScores_m2080 ();
extern "C" void GcLeaderboard_Internal_LoadScoresWithUsers_m2081 ();
extern "C" void GcLeaderboard_Loading_m2082 ();
extern "C" void GcLeaderboard_Dispose_m2083 ();
extern "C" void BoneWeight_get_weight0_m2084 ();
extern "C" void BoneWeight_set_weight0_m2085 ();
extern "C" void BoneWeight_get_weight1_m2086 ();
extern "C" void BoneWeight_set_weight1_m2087 ();
extern "C" void BoneWeight_get_weight2_m2088 ();
extern "C" void BoneWeight_set_weight2_m2089 ();
extern "C" void BoneWeight_get_weight3_m2090 ();
extern "C" void BoneWeight_set_weight3_m2091 ();
extern "C" void BoneWeight_get_boneIndex0_m2092 ();
extern "C" void BoneWeight_set_boneIndex0_m2093 ();
extern "C" void BoneWeight_get_boneIndex1_m2094 ();
extern "C" void BoneWeight_set_boneIndex1_m2095 ();
extern "C" void BoneWeight_get_boneIndex2_m2096 ();
extern "C" void BoneWeight_set_boneIndex2_m2097 ();
extern "C" void BoneWeight_get_boneIndex3_m2098 ();
extern "C" void BoneWeight_set_boneIndex3_m2099 ();
extern "C" void BoneWeight_GetHashCode_m2100 ();
extern "C" void BoneWeight_Equals_m2101 ();
extern "C" void BoneWeight_op_Equality_m2102 ();
extern "C" void BoneWeight_op_Inequality_m2103 ();
extern "C" void Renderer_get_sortingLayerID_m1583 ();
extern "C" void Renderer_get_sortingOrder_m1584 ();
extern "C" void Screen_get_width_m1665 ();
extern "C" void Screen_get_height_m1666 ();
extern "C" void Screen_get_dpi_m1929 ();
extern "C" void Texture__ctor_m2104 ();
extern "C" void Texture_Internal_GetWidth_m2105 ();
extern "C" void Texture_Internal_GetHeight_m2106 ();
extern "C" void Texture_get_width_m2107 ();
extern "C" void Texture_get_height_m2108 ();
extern "C" void Texture2D__ctor_m2109 ();
extern "C" void Texture2D_Internal_Create_m2110 ();
extern "C" void Texture2D_get_whiteTexture_m1631 ();
extern "C" void Texture2D_GetPixelBilinear_m1722 ();
extern "C" void RenderTexture_Internal_GetWidth_m2111 ();
extern "C" void RenderTexture_Internal_GetHeight_m2112 ();
extern "C" void RenderTexture_get_width_m2113 ();
extern "C" void RenderTexture_get_height_m2114 ();
extern "C" void GUILayer_HitTest_m2115 ();
extern "C" void GUILayer_INTERNAL_CALL_HitTest_m2116 ();
extern "C" void GradientColorKey__ctor_m2117 ();
extern "C" void GradientAlphaKey__ctor_m2118 ();
extern "C" void Gradient__ctor_m2119 ();
extern "C" void Gradient_Init_m2120 ();
extern "C" void Gradient_Cleanup_m2121 ();
extern "C" void Gradient_Finalize_m2122 ();
extern "C" void ScrollViewState__ctor_m2123 ();
extern "C" void WindowFunction__ctor_m2124 ();
extern "C" void WindowFunction_Invoke_m2125 ();
extern "C" void WindowFunction_BeginInvoke_m2126 ();
extern "C" void WindowFunction_EndInvoke_m2127 ();
extern "C" void GUI__cctor_m2128 ();
extern "C" void GUI_set_nextScrollStepTime_m2129 ();
extern "C" void GUI_set_skin_m2130 ();
extern "C" void GUI_get_skin_m2131 ();
extern "C" void GUI_set_changed_m2132 ();
extern "C" void GUI_CallWindowDelegate_m2133 ();
extern "C" void GUILayout_Width_m2134 ();
extern "C" void GUILayout_Height_m2135 ();
extern "C" void LayoutCache__ctor_m2136 ();
extern "C" void GUILayoutUtility__cctor_m2137 ();
extern "C" void GUILayoutUtility_SelectIDList_m2138 ();
extern "C" void GUILayoutUtility_Begin_m2139 ();
extern "C" void GUILayoutUtility_BeginWindow_m2140 ();
extern "C" void GUILayoutUtility_Layout_m2141 ();
extern "C" void GUILayoutUtility_LayoutFromEditorWindow_m2142 ();
extern "C" void GUILayoutUtility_LayoutFreeGroup_m2143 ();
extern "C" void GUILayoutUtility_LayoutSingleGroup_m2144 ();
extern "C" void GUILayoutUtility_Internal_GetWindowRect_m2145 ();
extern "C" void GUILayoutUtility_Internal_MoveWindow_m2146 ();
extern "C" void GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m2147 ();
extern "C" void GUILayoutUtility_get_spaceStyle_m2148 ();
extern "C" void GUILayoutEntry__ctor_m2149 ();
extern "C" void GUILayoutEntry__cctor_m2150 ();
extern "C" void GUILayoutEntry_get_style_m2151 ();
extern "C" void GUILayoutEntry_set_style_m2152 ();
extern "C" void GUILayoutEntry_get_margin_m2153 ();
extern "C" void GUILayoutEntry_CalcWidth_m2154 ();
extern "C" void GUILayoutEntry_CalcHeight_m2155 ();
extern "C" void GUILayoutEntry_SetHorizontal_m2156 ();
extern "C" void GUILayoutEntry_SetVertical_m2157 ();
extern "C" void GUILayoutEntry_ApplyStyleSettings_m2158 ();
extern "C" void GUILayoutEntry_ApplyOptions_m2159 ();
extern "C" void GUILayoutEntry_ToString_m2160 ();
extern "C" void GUILayoutGroup__ctor_m2161 ();
extern "C" void GUILayoutGroup_get_margin_m2162 ();
extern "C" void GUILayoutGroup_ApplyOptions_m2163 ();
extern "C" void GUILayoutGroup_ApplyStyleSettings_m2164 ();
extern "C" void GUILayoutGroup_ResetCursor_m2165 ();
extern "C" void GUILayoutGroup_CalcWidth_m2166 ();
extern "C" void GUILayoutGroup_SetHorizontal_m2167 ();
extern "C" void GUILayoutGroup_CalcHeight_m2168 ();
extern "C" void GUILayoutGroup_SetVertical_m2169 ();
extern "C" void GUILayoutGroup_ToString_m2170 ();
extern "C" void GUIScrollGroup__ctor_m2171 ();
extern "C" void GUIScrollGroup_CalcWidth_m2172 ();
extern "C" void GUIScrollGroup_SetHorizontal_m2173 ();
extern "C" void GUIScrollGroup_CalcHeight_m2174 ();
extern "C" void GUIScrollGroup_SetVertical_m2175 ();
extern "C" void GUILayoutOption__ctor_m2176 ();
extern "C" void GUIUtility__cctor_m2177 ();
extern "C" void GUIUtility_get_systemCopyBuffer_m2178 ();
extern "C" void GUIUtility_set_systemCopyBuffer_m2179 ();
extern "C" void GUIUtility_GetDefaultSkin_m2180 ();
extern "C" void GUIUtility_Internal_GetDefaultSkin_m2181 ();
extern "C" void GUIUtility_BeginGUI_m2182 ();
extern "C" void GUIUtility_Internal_ExitGUI_m2183 ();
extern "C" void GUIUtility_EndGUI_m2184 ();
extern "C" void GUIUtility_EndGUIFromException_m2185 ();
extern "C" void GUIUtility_CheckOnGUI_m2186 ();
extern "C" void GUIUtility_Internal_GetGUIDepth_m2187 ();
extern "C" void GUISettings__ctor_m2188 ();
extern "C" void SkinChangedDelegate__ctor_m2189 ();
extern "C" void SkinChangedDelegate_Invoke_m2190 ();
extern "C" void SkinChangedDelegate_BeginInvoke_m2191 ();
extern "C" void SkinChangedDelegate_EndInvoke_m2192 ();
extern "C" void GUISkin__ctor_m2193 ();
extern "C" void GUISkin_OnEnable_m2194 ();
extern "C" void GUISkin_get_font_m2195 ();
extern "C" void GUISkin_set_font_m2196 ();
extern "C" void GUISkin_get_box_m2197 ();
extern "C" void GUISkin_set_box_m2198 ();
extern "C" void GUISkin_get_label_m2199 ();
extern "C" void GUISkin_set_label_m2200 ();
extern "C" void GUISkin_get_textField_m2201 ();
extern "C" void GUISkin_set_textField_m2202 ();
extern "C" void GUISkin_get_textArea_m2203 ();
extern "C" void GUISkin_set_textArea_m2204 ();
extern "C" void GUISkin_get_button_m2205 ();
extern "C" void GUISkin_set_button_m2206 ();
extern "C" void GUISkin_get_toggle_m2207 ();
extern "C" void GUISkin_set_toggle_m2208 ();
extern "C" void GUISkin_get_window_m2209 ();
extern "C" void GUISkin_set_window_m2210 ();
extern "C" void GUISkin_get_horizontalSlider_m2211 ();
extern "C" void GUISkin_set_horizontalSlider_m2212 ();
extern "C" void GUISkin_get_horizontalSliderThumb_m2213 ();
extern "C" void GUISkin_set_horizontalSliderThumb_m2214 ();
extern "C" void GUISkin_get_verticalSlider_m2215 ();
extern "C" void GUISkin_set_verticalSlider_m2216 ();
extern "C" void GUISkin_get_verticalSliderThumb_m2217 ();
extern "C" void GUISkin_set_verticalSliderThumb_m2218 ();
extern "C" void GUISkin_get_horizontalScrollbar_m2219 ();
extern "C" void GUISkin_set_horizontalScrollbar_m2220 ();
extern "C" void GUISkin_get_horizontalScrollbarThumb_m2221 ();
extern "C" void GUISkin_set_horizontalScrollbarThumb_m2222 ();
extern "C" void GUISkin_get_horizontalScrollbarLeftButton_m2223 ();
extern "C" void GUISkin_set_horizontalScrollbarLeftButton_m2224 ();
extern "C" void GUISkin_get_horizontalScrollbarRightButton_m2225 ();
extern "C" void GUISkin_set_horizontalScrollbarRightButton_m2226 ();
extern "C" void GUISkin_get_verticalScrollbar_m2227 ();
extern "C" void GUISkin_set_verticalScrollbar_m2228 ();
extern "C" void GUISkin_get_verticalScrollbarThumb_m2229 ();
extern "C" void GUISkin_set_verticalScrollbarThumb_m2230 ();
extern "C" void GUISkin_get_verticalScrollbarUpButton_m2231 ();
extern "C" void GUISkin_set_verticalScrollbarUpButton_m2232 ();
extern "C" void GUISkin_get_verticalScrollbarDownButton_m2233 ();
extern "C" void GUISkin_set_verticalScrollbarDownButton_m2234 ();
extern "C" void GUISkin_get_scrollView_m2235 ();
extern "C" void GUISkin_set_scrollView_m2236 ();
extern "C" void GUISkin_get_customStyles_m2237 ();
extern "C" void GUISkin_set_customStyles_m2238 ();
extern "C" void GUISkin_get_settings_m2239 ();
extern "C" void GUISkin_get_error_m2240 ();
extern "C" void GUISkin_Apply_m2241 ();
extern "C" void GUISkin_BuildStyleCache_m2242 ();
extern "C" void GUISkin_GetStyle_m2243 ();
extern "C" void GUISkin_FindStyle_m2244 ();
extern "C" void GUISkin_MakeCurrent_m2245 ();
extern "C" void GUISkin_GetEnumerator_m2246 ();
extern "C" void GUIContent__ctor_m2247 ();
extern "C" void GUIContent__ctor_m1757 ();
extern "C" void GUIContent__cctor_m2248 ();
extern "C" void GUIContent_get_text_m1756 ();
extern "C" void GUIContent_set_text_m2249 ();
extern "C" void GUIContent_ClearStaticCache_m2250 ();
extern "C" void GUIStyleState__ctor_m2251 ();
extern "C" void GUIStyleState__ctor_m2252 ();
extern "C" void GUIStyleState_Finalize_m2253 ();
extern "C" void GUIStyleState_Init_m2254 ();
extern "C" void GUIStyleState_Cleanup_m2255 ();
extern "C" void GUIStyleState_GetBackgroundInternal_m2256 ();
extern "C" void GUIStyleState_set_textColor_m2257 ();
extern "C" void GUIStyleState_INTERNAL_set_textColor_m2258 ();
extern "C" void RectOffset__ctor_m1956 ();
extern "C" void RectOffset__ctor_m2259 ();
extern "C" void RectOffset__ctor_m2260 ();
extern "C" void RectOffset_Finalize_m2261 ();
extern "C" void RectOffset_Init_m2262 ();
extern "C" void RectOffset_Cleanup_m2263 ();
extern "C" void RectOffset_get_left_m1954 ();
extern "C" void RectOffset_set_left_m2264 ();
extern "C" void RectOffset_get_right_m2265 ();
extern "C" void RectOffset_set_right_m2266 ();
extern "C" void RectOffset_get_top_m1955 ();
extern "C" void RectOffset_set_top_m2267 ();
extern "C" void RectOffset_get_bottom_m2268 ();
extern "C" void RectOffset_set_bottom_m2269 ();
extern "C" void RectOffset_get_horizontal_m1948 ();
extern "C" void RectOffset_get_vertical_m1950 ();
extern "C" void RectOffset_Remove_m2270 ();
extern "C" void RectOffset_INTERNAL_CALL_Remove_m2271 ();
extern "C" void RectOffset_ToString_m2272 ();
extern "C" void GUIStyle__ctor_m2273 ();
extern "C" void GUIStyle__cctor_m2274 ();
extern "C" void GUIStyle_Finalize_m2275 ();
extern "C" void GUIStyle_Init_m2276 ();
extern "C" void GUIStyle_Cleanup_m2277 ();
extern "C" void GUIStyle_get_name_m2278 ();
extern "C" void GUIStyle_set_name_m2279 ();
extern "C" void GUIStyle_get_normal_m2280 ();
extern "C" void GUIStyle_GetStyleStatePtr_m2281 ();
extern "C" void GUIStyle_get_margin_m2282 ();
extern "C" void GUIStyle_get_padding_m2283 ();
extern "C" void GUIStyle_GetRectOffsetPtr_m2284 ();
extern "C" void GUIStyle_get_fixedWidth_m2285 ();
extern "C" void GUIStyle_get_fixedHeight_m2286 ();
extern "C" void GUIStyle_get_stretchWidth_m2287 ();
extern "C" void GUIStyle_set_stretchWidth_m2288 ();
extern "C" void GUIStyle_get_stretchHeight_m2289 ();
extern "C" void GUIStyle_set_stretchHeight_m2290 ();
extern "C" void GUIStyle_Internal_GetLineHeight_m2291 ();
extern "C" void GUIStyle_get_lineHeight_m2292 ();
extern "C" void GUIStyle_SetDefaultFont_m2293 ();
extern "C" void GUIStyle_get_none_m2294 ();
extern "C" void GUIStyle_GetCursorPixelPosition_m2295 ();
extern "C" void GUIStyle_Internal_GetCursorPixelPosition_m2296 ();
extern "C" void GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m2297 ();
extern "C" void GUIStyle_CalcSize_m2298 ();
extern "C" void GUIStyle_Internal_CalcSize_m2299 ();
extern "C" void GUIStyle_CalcHeight_m2300 ();
extern "C" void GUIStyle_Internal_CalcHeight_m2301 ();
extern "C" void GUIStyle_ToString_m2302 ();
extern "C" void TouchScreenKeyboard__ctor_m2303 ();
extern "C" void TouchScreenKeyboard_Destroy_m2304 ();
extern "C" void TouchScreenKeyboard_Finalize_m2305 ();
extern "C" void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m2306 ();
extern "C" void TouchScreenKeyboard_get_isSupported_m1760 ();
extern "C" void TouchScreenKeyboard_Open_m1830 ();
extern "C" void TouchScreenKeyboard_Open_m1831 ();
extern "C" void TouchScreenKeyboard_Open_m2307 ();
extern "C" void TouchScreenKeyboard_get_text_m1737 ();
extern "C" void TouchScreenKeyboard_set_text_m1738 ();
extern "C" void TouchScreenKeyboard_set_hideInput_m1829 ();
extern "C" void TouchScreenKeyboard_get_active_m1736 ();
extern "C" void TouchScreenKeyboard_set_active_m1828 ();
extern "C" void TouchScreenKeyboard_get_done_m1765 ();
extern "C" void TouchScreenKeyboard_get_wasCanceled_m1761 ();
extern "C" void Event__ctor_m1733 ();
extern "C" void Event_Init_m2308 ();
extern "C" void Event_Finalize_m2309 ();
extern "C" void Event_Cleanup_m2310 ();
extern "C" void Event_get_rawType_m1779 ();
extern "C" void Event_get_type_m2311 ();
extern "C" void Event_get_mousePosition_m2312 ();
extern "C" void Event_Internal_GetMousePosition_m2313 ();
extern "C" void Event_get_modifiers_m1775 ();
extern "C" void Event_get_character_m1777 ();
extern "C" void Event_get_commandName_m2314 ();
extern "C" void Event_get_keyCode_m1776 ();
extern "C" void Event_get_current_m2315 ();
extern "C" void Event_Internal_SetNativeEvent_m2316 ();
extern "C" void Event_Internal_MakeMasterEventCurrent_m2317 ();
extern "C" void Event_PopEvent_m1780 ();
extern "C" void Event_get_isKey_m2318 ();
extern "C" void Event_get_isMouse_m2319 ();
extern "C" void Event_GetHashCode_m2320 ();
extern "C" void Event_Equals_m2321 ();
extern "C" void Event_ToString_m2322 ();
extern "C" void Vector2__ctor_m231 ();
extern "C" void Vector2_get_Item_m1715 ();
extern "C" void Vector2_set_Item_m1725 ();
extern "C" void Vector2_Scale_m1817 ();
extern "C" void Vector2_ToString_m2323 ();
extern "C" void Vector2_GetHashCode_m2324 ();
extern "C" void Vector2_Equals_m2325 ();
extern "C" void Vector2_Dot_m1553 ();
extern "C" void Vector2_get_sqrMagnitude_m1516 ();
extern "C" void Vector2_SqrMagnitude_m2326 ();
extern "C" void Vector2_get_zero_m1510 ();
extern "C" void Vector2_get_one_m1711 ();
extern "C" void Vector2_get_up_m1951 ();
extern "C" void Vector2_op_Addition_m266 ();
extern "C" void Vector2_op_Subtraction_m1526 ();
extern "C" void Vector2_op_Multiply_m1712 ();
extern "C" void Vector2_op_Division_m1774 ();
extern "C" void Vector2_op_Equality_m1980 ();
extern "C" void Vector2_op_Inequality_m1808 ();
extern "C" void Vector2_op_Implicit_m262 ();
extern "C" void Vector2_op_Implicit_m260 ();
extern "C" void Vector3__ctor_m192 ();
extern "C" void Vector3__ctor_m1644 ();
extern "C" void Vector3_Lerp_m1856 ();
extern "C" void Vector3_MoveTowards_m261 ();
extern "C" void Vector3_get_Item_m1860 ();
extern "C" void Vector3_set_Item_m1861 ();
extern "C" void Vector3_GetHashCode_m2327 ();
extern "C" void Vector3_Equals_m2328 ();
extern "C" void Vector3_Normalize_m2329 ();
extern "C" void Vector3_get_normalized_m1886 ();
extern "C" void Vector3_ToString_m2330 ();
extern "C" void Vector3_ToString_m2331 ();
extern "C" void Vector3_Dot_m1674 ();
extern "C" void Vector3_Distance_m1580 ();
extern "C" void Vector3_Magnitude_m2332 ();
extern "C" void Vector3_get_magnitude_m2333 ();
extern "C" void Vector3_SqrMagnitude_m2334 ();
extern "C" void Vector3_get_sqrMagnitude_m258 ();
extern "C" void Vector3_Min_m1870 ();
extern "C" void Vector3_Max_m1871 ();
extern "C" void Vector3_get_zero_m1509 ();
extern "C" void Vector3_get_one_m236 ();
extern "C" void Vector3_get_forward_m1672 ();
extern "C" void Vector3_get_back_m2335 ();
extern "C" void Vector3_get_up_m1508 ();
extern "C" void Vector3_get_down_m1892 ();
extern "C" void Vector3_get_left_m1890 ();
extern "C" void Vector3_get_right_m1891 ();
extern "C" void Vector3_op_Addition_m1820 ();
extern "C" void Vector3_op_Subtraction_m251 ();
extern "C" void Vector3_op_Multiply_m1912 ();
extern "C" void Vector3_op_Division_m2336 ();
extern "C" void Vector3_op_Equality_m2337 ();
extern "C" void Vector3_op_Inequality_m1804 ();
extern "C" void Color__ctor_m1731 ();
extern "C" void Color__ctor_m2338 ();
extern "C" void Color_ToString_m2339 ();
extern "C" void Color_GetHashCode_m2340 ();
extern "C" void Color_Equals_m1651 ();
extern "C" void Color_Lerp_m1596 ();
extern "C" void Color_get_red_m2341 ();
extern "C" void Color_get_white_m1618 ();
extern "C" void Color_get_black_m1655 ();
extern "C" void Color_op_Multiply_m1885 ();
extern "C" void Color_op_Implicit_m2342 ();
extern "C" void Color32__ctor_m1611 ();
extern "C" void Color32_ToString_m2343 ();
extern "C" void Color32_op_Implicit_m1643 ();
extern "C" void Color32_op_Implicit_m1612 ();
extern "C" void Quaternion__ctor_m2344 ();
extern "C" void Quaternion_get_identity_m196 ();
extern "C" void Quaternion_Dot_m2345 ();
extern "C" void Quaternion_Inverse_m1887 ();
extern "C" void Quaternion_INTERNAL_CALL_Inverse_m2346 ();
extern "C" void Quaternion_ToString_m2347 ();
extern "C" void Quaternion_GetHashCode_m2348 ();
extern "C" void Quaternion_Equals_m2349 ();
extern "C" void Quaternion_op_Multiply_m1673 ();
extern "C" void Quaternion_op_Inequality_m1806 ();
extern "C" void Rect__ctor_m1836 ();
extern "C" void Rect_get_x_m1640 ();
extern "C" void Rect_set_x_m1704 ();
extern "C" void Rect_get_y_m1641 ();
extern "C" void Rect_set_y_m1702 ();
extern "C" void Rect_get_position_m1713 ();
extern "C" void Rect_get_center_m1846 ();
extern "C" void Rect_get_width_m1635 ();
extern "C" void Rect_set_width_m1703 ();
extern "C" void Rect_get_height_m1636 ();
extern "C" void Rect_set_height_m1700 ();
extern "C" void Rect_get_size_m1710 ();
extern "C" void Rect_get_xMin_m1730 ();
extern "C" void Rect_get_yMin_m1729 ();
extern "C" void Rect_get_xMax_m1720 ();
extern "C" void Rect_get_yMax_m1721 ();
extern "C" void Rect_ToString_m2350 ();
extern "C" void Rect_Contains_m2351 ();
extern "C" void Rect_GetHashCode_m2352 ();
extern "C" void Rect_Equals_m2353 ();
extern "C" void Rect_op_Equality_m1837 ();
extern "C" void Matrix4x4_get_Item_m2354 ();
extern "C" void Matrix4x4_set_Item_m2355 ();
extern "C" void Matrix4x4_get_Item_m2356 ();
extern "C" void Matrix4x4_set_Item_m2357 ();
extern "C" void Matrix4x4_GetHashCode_m2358 ();
extern "C" void Matrix4x4_Equals_m2359 ();
extern "C" void Matrix4x4_Inverse_m2360 ();
extern "C" void Matrix4x4_INTERNAL_CALL_Inverse_m2361 ();
extern "C" void Matrix4x4_Transpose_m2362 ();
extern "C" void Matrix4x4_INTERNAL_CALL_Transpose_m2363 ();
extern "C" void Matrix4x4_Invert_m2364 ();
extern "C" void Matrix4x4_INTERNAL_CALL_Invert_m2365 ();
extern "C" void Matrix4x4_get_inverse_m2366 ();
extern "C" void Matrix4x4_get_transpose_m2367 ();
extern "C" void Matrix4x4_get_isIdentity_m2368 ();
extern "C" void Matrix4x4_GetColumn_m2369 ();
extern "C" void Matrix4x4_GetRow_m2370 ();
extern "C" void Matrix4x4_SetColumn_m2371 ();
extern "C" void Matrix4x4_SetRow_m2372 ();
extern "C" void Matrix4x4_MultiplyPoint_m2373 ();
extern "C" void Matrix4x4_MultiplyPoint3x4_m1869 ();
extern "C" void Matrix4x4_MultiplyVector_m2374 ();
extern "C" void Matrix4x4_Scale_m2375 ();
extern "C" void Matrix4x4_get_zero_m2376 ();
extern "C" void Matrix4x4_get_identity_m2377 ();
extern "C" void Matrix4x4_SetTRS_m2378 ();
extern "C" void Matrix4x4_TRS_m2379 ();
extern "C" void Matrix4x4_INTERNAL_CALL_TRS_m2380 ();
extern "C" void Matrix4x4_ToString_m2381 ();
extern "C" void Matrix4x4_ToString_m2382 ();
extern "C" void Matrix4x4_Ortho_m2383 ();
extern "C" void Matrix4x4_Perspective_m2384 ();
extern "C" void Matrix4x4_op_Multiply_m2385 ();
extern "C" void Matrix4x4_op_Multiply_m2386 ();
extern "C" void Matrix4x4_op_Equality_m2387 ();
extern "C" void Matrix4x4_op_Inequality_m2388 ();
extern "C" void Bounds__ctor_m1863 ();
extern "C" void Bounds_GetHashCode_m2389 ();
extern "C" void Bounds_Equals_m2390 ();
extern "C" void Bounds_get_center_m1864 ();
extern "C" void Bounds_set_center_m1866 ();
extern "C" void Bounds_get_size_m1854 ();
extern "C" void Bounds_set_size_m1865 ();
extern "C" void Bounds_get_extents_m2391 ();
extern "C" void Bounds_set_extents_m2392 ();
extern "C" void Bounds_get_min_m1859 ();
extern "C" void Bounds_set_min_m2393 ();
extern "C" void Bounds_get_max_m1873 ();
extern "C" void Bounds_set_max_m2394 ();
extern "C" void Bounds_SetMinMax_m2395 ();
extern "C" void Bounds_Encapsulate_m1872 ();
extern "C" void Bounds_Encapsulate_m2396 ();
extern "C" void Bounds_Expand_m2397 ();
extern "C" void Bounds_Expand_m2398 ();
extern "C" void Bounds_Intersects_m2399 ();
extern "C" void Bounds_Internal_Contains_m2400 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_Contains_m2401 ();
extern "C" void Bounds_Contains_m2402 ();
extern "C" void Bounds_Internal_SqrDistance_m2403 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_SqrDistance_m2404 ();
extern "C" void Bounds_SqrDistance_m2405 ();
extern "C" void Bounds_Internal_IntersectRay_m2406 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_IntersectRay_m2407 ();
extern "C" void Bounds_IntersectRay_m2408 ();
extern "C" void Bounds_IntersectRay_m2409 ();
extern "C" void Bounds_Internal_GetClosestPoint_m2410 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m2411 ();
extern "C" void Bounds_ClosestPoint_m2412 ();
extern "C" void Bounds_ToString_m2413 ();
extern "C" void Bounds_ToString_m2414 ();
extern "C" void Bounds_op_Equality_m2415 ();
extern "C" void Bounds_op_Inequality_m1857 ();
extern "C" void Vector4__ctor_m1642 ();
extern "C" void Vector4_get_Item_m1714 ();
extern "C" void Vector4_set_Item_m1716 ();
extern "C" void Vector4_GetHashCode_m2416 ();
extern "C" void Vector4_Equals_m2417 ();
extern "C" void Vector4_ToString_m2418 ();
extern "C" void Vector4_Dot_m2419 ();
extern "C" void Vector4_SqrMagnitude_m2420 ();
extern "C" void Vector4_get_sqrMagnitude_m1692 ();
extern "C" void Vector4_get_zero_m1696 ();
extern "C" void Vector4_op_Subtraction_m2421 ();
extern "C" void Vector4_op_Division_m1709 ();
extern "C" void Vector4_op_Equality_m2422 ();
extern "C" void Ray__ctor_m2423 ();
extern "C" void Ray_get_origin_m1574 ();
extern "C" void Ray_get_direction_m1575 ();
extern "C" void Ray_GetPoint_m1769 ();
extern "C" void Ray_ToString_m2424 ();
extern "C" void Plane__ctor_m1767 ();
extern "C" void Plane_get_normal_m2425 ();
extern "C" void Plane_get_distance_m2426 ();
extern "C" void Plane_Raycast_m1768 ();
extern "C" void MathfInternal__cctor_m2427 ();
extern "C" void Mathf__cctor_m2428 ();
extern "C" void Mathf_Sin_m2429 ();
extern "C" void Mathf_Cos_m2430 ();
extern "C" void Mathf_Sqrt_m2431 ();
extern "C" void Mathf_Abs_m2432 ();
extern "C" void Mathf_Min_m1928 ();
extern "C" void Mathf_Min_m1785 ();
extern "C" void Mathf_Max_m1889 ();
extern "C" void Mathf_Max_m1783 ();
extern "C" void Mathf_Pow_m2433 ();
extern "C" void Mathf_Log_m1927 ();
extern "C" void Mathf_Floor_m2434 ();
extern "C" void Mathf_Round_m2435 ();
extern "C" void Mathf_CeilToInt_m1947 ();
extern "C" void Mathf_FloorToInt_m1949 ();
extern "C" void Mathf_RoundToInt_m1699 ();
extern "C" void Mathf_Sign_m1862 ();
extern "C" void Mathf_Clamp_m1695 ();
extern "C" void Mathf_Clamp_m1613 ();
extern "C" void Mathf_Clamp01_m1687 ();
extern "C" void Mathf_Lerp_m215 ();
extern "C" void Mathf_Approximately_m1549 ();
extern "C" void Mathf_SmoothDamp_m1855 ();
extern "C" void Mathf_Repeat_m1727 ();
extern "C" void Mathf_InverseLerp_m1726 ();
extern "C" void DrivenRectTransformTracker_Add_m1845 ();
extern "C" void DrivenRectTransformTracker_Clear_m1843 ();
extern "C" void ReapplyDrivenProperties__ctor_m1962 ();
extern "C" void ReapplyDrivenProperties_Invoke_m2436 ();
extern "C" void ReapplyDrivenProperties_BeginInvoke_m2437 ();
extern "C" void ReapplyDrivenProperties_EndInvoke_m2438 ();
extern "C" void RectTransform_add_reapplyDrivenProperties_m1963 ();
extern "C" void RectTransform_remove_reapplyDrivenProperties_m2439 ();
extern "C" void RectTransform_get_rect_m1634 ();
extern "C" void RectTransform_INTERNAL_get_rect_m2440 ();
extern "C" void RectTransform_get_anchorMin_m1705 ();
extern "C" void RectTransform_set_anchorMin_m1814 ();
extern "C" void RectTransform_INTERNAL_get_anchorMin_m2441 ();
extern "C" void RectTransform_INTERNAL_set_anchorMin_m2442 ();
extern "C" void RectTransform_get_anchorMax_m1809 ();
extern "C" void RectTransform_set_anchorMax_m1706 ();
extern "C" void RectTransform_INTERNAL_get_anchorMax_m2443 ();
extern "C" void RectTransform_INTERNAL_set_anchorMax_m2444 ();
extern "C" void RectTransform_get_anchoredPosition_m1810 ();
extern "C" void RectTransform_set_anchoredPosition_m1815 ();
extern "C" void RectTransform_INTERNAL_get_anchoredPosition_m2445 ();
extern "C" void RectTransform_INTERNAL_set_anchoredPosition_m2446 ();
extern "C" void RectTransform_get_sizeDelta_m1811 ();
extern "C" void RectTransform_set_sizeDelta_m232 ();
extern "C" void RectTransform_INTERNAL_get_sizeDelta_m2447 ();
extern "C" void RectTransform_INTERNAL_set_sizeDelta_m2448 ();
extern "C" void RectTransform_get_pivot_m1701 ();
extern "C" void RectTransform_set_pivot_m1816 ();
extern "C" void RectTransform_INTERNAL_get_pivot_m2449 ();
extern "C" void RectTransform_INTERNAL_set_pivot_m2450 ();
extern "C" void RectTransform_SendReapplyDrivenProperties_m2451 ();
extern "C" void RectTransform_GetLocalCorners_m2452 ();
extern "C" void RectTransform_GetWorldCorners_m1868 ();
extern "C" void RectTransform_SetInsetAndSizeFromParentEdge_m1961 ();
extern "C" void RectTransform_SetSizeWithCurrentAnchors_m1925 ();
extern "C" void RectTransform_GetParentSize_m2453 ();
extern "C" void ResourceRequest__ctor_m2454 ();
extern "C" void ResourceRequest_get_asset_m2455 ();
extern "C" void Resources_Load_m2456 ();
extern "C" void SerializePrivateVariables__ctor_m2457 ();
extern "C" void SerializeField__ctor_m2458 ();
extern "C" void Shader_PropertyToID_m2459 ();
extern "C" void Material__ctor_m1901 ();
extern "C" void Material_get_mainTexture_m1906 ();
extern "C" void Material_GetTexture_m2460 ();
extern "C" void Material_GetTexture_m2461 ();
extern "C" void Material_SetFloat_m2462 ();
extern "C" void Material_SetFloat_m2463 ();
extern "C" void Material_SetInt_m1902 ();
extern "C" void Material_HasProperty_m1899 ();
extern "C" void Material_HasProperty_m2464 ();
extern "C" void Material_Internal_CreateWithMaterial_m2465 ();
extern "C" void SphericalHarmonicsL2_Clear_m2466 ();
extern "C" void SphericalHarmonicsL2_ClearInternal_m2467 ();
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m2468 ();
extern "C" void SphericalHarmonicsL2_AddAmbientLight_m2469 ();
extern "C" void SphericalHarmonicsL2_AddAmbientLightInternal_m2470 ();
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m2471 ();
extern "C" void SphericalHarmonicsL2_AddDirectionalLight_m2472 ();
extern "C" void SphericalHarmonicsL2_AddDirectionalLightInternal_m2473 ();
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m2474 ();
extern "C" void SphericalHarmonicsL2_get_Item_m2475 ();
extern "C" void SphericalHarmonicsL2_set_Item_m2476 ();
extern "C" void SphericalHarmonicsL2_GetHashCode_m2477 ();
extern "C" void SphericalHarmonicsL2_Equals_m2478 ();
extern "C" void SphericalHarmonicsL2_op_Multiply_m2479 ();
extern "C" void SphericalHarmonicsL2_op_Multiply_m2480 ();
extern "C" void SphericalHarmonicsL2_op_Addition_m2481 ();
extern "C" void SphericalHarmonicsL2_op_Equality_m2482 ();
extern "C" void SphericalHarmonicsL2_op_Inequality_m2483 ();
extern "C" void Sprite_get_rect_m1698 ();
extern "C" void Sprite_INTERNAL_get_rect_m2484 ();
extern "C" void Sprite_get_pixelsPerUnit_m1693 ();
extern "C" void Sprite_get_texture_m1690 ();
extern "C" void Sprite_get_textureRect_m1719 ();
extern "C" void Sprite_INTERNAL_get_textureRect_m2485 ();
extern "C" void Sprite_get_border_m1691 ();
extern "C" void Sprite_INTERNAL_get_border_m2486 ();
extern "C" void SpriteRenderer_set_sprite_m273 ();
extern "C" void SpriteRenderer_SetSprite_INTERNAL_m2487 ();
extern "C" void DataUtility_GetInnerUV_m1708 ();
extern "C" void DataUtility_GetOuterUV_m1707 ();
extern "C" void DataUtility_GetPadding_m1697 ();
extern "C" void DataUtility_GetMinSize_m1717 ();
extern "C" void DataUtility_Internal_GetMinSize_m2488 ();
extern "C" void WWW__ctor_m205 ();
extern "C" void WWW_Dispose_m2489 ();
extern "C" void WWW_Finalize_m2490 ();
extern "C" void WWW_DestroyWWW_m2491 ();
extern "C" void WWW_InitWWW_m2492 ();
extern "C" void WWW_get_responseHeaders_m2493 ();
extern "C" void WWW_get_responseHeadersString_m2494 ();
extern "C" void WWW_get_text_m206 ();
extern "C" void WWW_get_DefaultEncoding_m2495 ();
extern "C" void WWW_GetTextEncoder_m2496 ();
extern "C" void WWW_get_bytes_m2497 ();
extern "C" void WWW_get_error_m218 ();
extern "C" void WWW_get_isDone_m2498 ();
extern "C" void WWW_FlattenedHeadersFrom_m2499 ();
extern "C" void WWW_ParseHTTPHeaderString_m2500 ();
extern "C" void WWWForm__ctor_m200 ();
extern "C" void WWWForm_AddField_m203 ();
extern "C" void WWWForm_AddField_m2501 ();
extern "C" void WWWForm_AddField_m269 ();
extern "C" void WWWForm_get_headers_m2502 ();
extern "C" void WWWForm_get_data_m2503 ();
extern "C" void WWWTranscoder__cctor_m2504 ();
extern "C" void WWWTranscoder_Byte2Hex_m2505 ();
extern "C" void WWWTranscoder_URLEncode_m2506 ();
extern "C" void WWWTranscoder_QPEncode_m2507 ();
extern "C" void WWWTranscoder_Encode_m2508 ();
extern "C" void WWWTranscoder_ByteArrayContains_m2509 ();
extern "C" void WWWTranscoder_SevenBitClean_m2510 ();
extern "C" void WWWTranscoder_SevenBitClean_m2511 ();
extern "C" void UnityString_Format_m2512 ();
extern "C" void AsyncOperation__ctor_m2513 ();
extern "C" void AsyncOperation_InternalDestroy_m2514 ();
extern "C" void AsyncOperation_Finalize_m2515 ();
extern "C" void LogCallback__ctor_m2516 ();
extern "C" void LogCallback_Invoke_m2517 ();
extern "C" void LogCallback_BeginInvoke_m2518 ();
extern "C" void LogCallback_EndInvoke_m2519 ();
extern "C" void Application_LoadLevel_m241 ();
extern "C" void Application_LoadLevelAsync_m2520 ();
extern "C" void Application_get_isPlaying_m1833 ();
extern "C" void Application_get_isEditor_m1835 ();
extern "C" void Application_get_platform_m1735 ();
extern "C" void Application_get_cloudProjectId_m2521 ();
extern "C" void Application_CallLogCallback_m2522 ();
extern "C" void Behaviour__ctor_m2523 ();
extern "C" void Behaviour_get_enabled_m1512 ();
extern "C" void Behaviour_set_enabled_m250 ();
extern "C" void Behaviour_get_isActiveAndEnabled_m1513 ();
extern "C" void CameraCallback__ctor_m2524 ();
extern "C" void CameraCallback_Invoke_m2525 ();
extern "C" void CameraCallback_BeginInvoke_m2526 ();
extern "C" void CameraCallback_EndInvoke_m2527 ();
extern "C" void Camera_get_nearClipPlane_m1573 ();
extern "C" void Camera_get_farClipPlane_m1572 ();
extern "C" void Camera_get_depth_m1473 ();
extern "C" void Camera_get_cullingMask_m1587 ();
extern "C" void Camera_get_eventMask_m2528 ();
extern "C" void Camera_get_pixelRect_m2529 ();
extern "C" void Camera_INTERNAL_get_pixelRect_m2530 ();
extern "C" void Camera_get_targetTexture_m2531 ();
extern "C" void Camera_get_clearFlags_m2532 ();
extern "C" void Camera_ScreenToViewportPoint_m1667 ();
extern "C" void Camera_INTERNAL_CALL_ScreenToViewportPoint_m2533 ();
extern "C" void Camera_ScreenPointToRay_m1571 ();
extern "C" void Camera_INTERNAL_CALL_ScreenPointToRay_m2534 ();
extern "C" void Camera_get_main_m1586 ();
extern "C" void Camera_get_allCamerasCount_m2535 ();
extern "C" void Camera_GetAllCameras_m2536 ();
extern "C" void Camera_FireOnPreCull_m2537 ();
extern "C" void Camera_FireOnPreRender_m2538 ();
extern "C" void Camera_FireOnPostRender_m2539 ();
extern "C" void Camera_RaycastTry_m2540 ();
extern "C" void Camera_INTERNAL_CALL_RaycastTry_m2541 ();
extern "C" void Camera_RaycastTry2D_m2542 ();
extern "C" void Camera_INTERNAL_CALL_RaycastTry2D_m2543 ();
extern "C" void Debug_Internal_Log_m2544 ();
extern "C" void Debug_Internal_LogException_m2545 ();
extern "C" void Debug_Log_m202 ();
extern "C" void Debug_LogError_m1469 ();
extern "C" void Debug_LogError_m1724 ();
extern "C" void Debug_LogException_m2546 ();
extern "C" void Debug_LogException_m1609 ();
extern "C" void Debug_LogWarning_m2547 ();
extern "C" void Debug_LogWarning_m1900 ();
extern "C" void DisplaysUpdatedDelegate__ctor_m2548 ();
extern "C" void DisplaysUpdatedDelegate_Invoke_m2549 ();
extern "C" void DisplaysUpdatedDelegate_BeginInvoke_m2550 ();
extern "C" void DisplaysUpdatedDelegate_EndInvoke_m2551 ();
extern "C" void Display__ctor_m2552 ();
extern "C" void Display__ctor_m2553 ();
extern "C" void Display__cctor_m2554 ();
extern "C" void Display_add_onDisplaysUpdated_m2555 ();
extern "C" void Display_remove_onDisplaysUpdated_m2556 ();
extern "C" void Display_get_renderingWidth_m2557 ();
extern "C" void Display_get_renderingHeight_m2558 ();
extern "C" void Display_get_systemWidth_m2559 ();
extern "C" void Display_get_systemHeight_m2560 ();
extern "C" void Display_get_colorBuffer_m2561 ();
extern "C" void Display_get_depthBuffer_m2562 ();
extern "C" void Display_Activate_m2563 ();
extern "C" void Display_Activate_m2564 ();
extern "C" void Display_SetParams_m2565 ();
extern "C" void Display_SetRenderingResolution_m2566 ();
extern "C" void Display_MultiDisplayLicense_m2567 ();
extern "C" void Display_RelativeMouseAt_m2568 ();
extern "C" void Display_get_main_m2569 ();
extern "C" void Display_RecreateDisplayList_m2570 ();
extern "C" void Display_FireDisplaysUpdated_m2571 ();
extern "C" void Display_GetSystemExtImpl_m2572 ();
extern "C" void Display_GetRenderingExtImpl_m2573 ();
extern "C" void Display_GetRenderingBuffersImpl_m2574 ();
extern "C" void Display_SetRenderingResolutionImpl_m2575 ();
extern "C" void Display_ActivateDisplayImpl_m2576 ();
extern "C" void Display_SetParamsImpl_m2577 ();
extern "C" void Display_MultiDisplayLicenseImpl_m2578 ();
extern "C" void Display_RelativeMouseAtImpl_m2579 ();
extern "C" void MonoBehaviour__ctor_m191 ();
extern "C" void MonoBehaviour_InvokeRepeating_m242 ();
extern "C" void MonoBehaviour_CancelInvoke_m253 ();
extern "C" void MonoBehaviour_StartCoroutine_m207 ();
extern "C" void MonoBehaviour_StartCoroutine_Auto_m2580 ();
extern "C" void MonoBehaviour_StartCoroutine_m2581 ();
extern "C" void MonoBehaviour_StartCoroutine_m223 ();
extern "C" void MonoBehaviour_StopCoroutine_m2582 ();
extern "C" void MonoBehaviour_StopCoroutine_m1840 ();
extern "C" void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2583 ();
extern "C" void MonoBehaviour_StopCoroutine_Auto_m2584 ();
extern "C" void Touch_get_fingerId_m1523 ();
extern "C" void Touch_get_position_m1525 ();
extern "C" void Touch_get_phase_m1524 ();
extern "C" void Input__cctor_m2585 ();
extern "C" void Input_GetAxisRaw_m1548 ();
extern "C" void Input_GetButtonDown_m1547 ();
extern "C" void Input_GetMouseButton_m1568 ();
extern "C" void Input_GetMouseButtonDown_m1527 ();
extern "C" void Input_GetMouseButtonUp_m1528 ();
extern "C" void Input_get_mousePosition_m1529 ();
extern "C" void Input_INTERNAL_get_mousePosition_m2586 ();
extern "C" void Input_get_mouseScrollDelta_m1530 ();
extern "C" void Input_INTERNAL_get_mouseScrollDelta_m2587 ();
extern "C" void Input_get_mousePresent_m1546 ();
extern "C" void Input_GetTouch_m1566 ();
extern "C" void Input_get_touchCount_m1567 ();
extern "C" void Input_get_touchSupported_m1565 ();
extern "C" void Input_set_imeCompositionMode_m1832 ();
extern "C" void Input_get_compositionString_m1751 ();
extern "C" void Input_set_compositionCursorPos_m1819 ();
extern "C" void Input_INTERNAL_set_compositionCursorPos_m2588 ();
extern "C" void Object__ctor_m2589 ();
extern "C" void Object_Internal_CloneSingle_m2590 ();
extern "C" void Object_Internal_InstantiateSingle_m2591 ();
extern "C" void Object_INTERNAL_CALL_Internal_InstantiateSingle_m2592 ();
extern "C" void Object_Destroy_m2593 ();
extern "C" void Object_Destroy_m227 ();
extern "C" void Object_DestroyImmediate_m2594 ();
extern "C" void Object_DestroyImmediate_m1834 ();
extern "C" void Object_get_name_m271 ();
extern "C" void Object_set_name_m234 ();
extern "C" void Object_set_hideFlags_m1795 ();
extern "C" void Object_ToString_m2595 ();
extern "C" void Object_Equals_m2596 ();
extern "C" void Object_GetHashCode_m2597 ();
extern "C" void Object_CompareBaseObjects_m2598 ();
extern "C" void Object_IsNativeObjectAlive_m2599 ();
extern "C" void Object_GetInstanceID_m2600 ();
extern "C" void Object_GetCachedPtr_m2601 ();
extern "C" void Object_Instantiate_m197 ();
extern "C" void Object_CheckNullArgument_m2602 ();
extern "C" void Object_op_Implicit_m1468 ();
extern "C" void Object_op_Equality_m247 ();
extern "C" void Object_op_Inequality_m1472 ();
extern "C" void Component__ctor_m2603 ();
extern "C" void Component_get_transform_m224 ();
extern "C" void Component_get_gameObject_m226 ();
extern "C" void Component_GetComponent_m1960 ();
extern "C" void Component_GetComponentFastPath_m2604 ();
extern "C" void Component_GetComponentsForListInternal_m2605 ();
extern "C" void Component_GetComponents_m1630 ();
extern "C" void GameObject__ctor_m193 ();
extern "C" void GameObject_GetComponent_m2606 ();
extern "C" void GameObject_GetComponentFastPath_m2607 ();
extern "C" void GameObject_GetComponentsInternal_m2608 ();
extern "C" void GameObject_get_transform_m194 ();
extern "C" void GameObject_get_layer_m1797 ();
extern "C" void GameObject_set_layer_m1798 ();
extern "C" void GameObject_SetActive_m238 ();
extern "C" void GameObject_get_activeInHierarchy_m1514 ();
extern "C" void GameObject_SendMessage_m2609 ();
extern "C" void GameObject_Internal_AddComponentWithType_m2610 ();
extern "C" void GameObject_AddComponent_m2611 ();
extern "C" void GameObject_Internal_CreateGameObject_m2612 ();
extern "C" void GameObject_Find_m209 ();
extern "C" void Enumerator__ctor_m2613 ();
extern "C" void Enumerator_get_Current_m2614 ();
extern "C" void Enumerator_MoveNext_m2615 ();
extern "C" void Transform_get_position_m257 ();
extern "C" void Transform_set_position_m245 ();
extern "C" void Transform_INTERNAL_get_position_m2616 ();
extern "C" void Transform_INTERNAL_set_position_m2617 ();
extern "C" void Transform_get_localPosition_m1803 ();
extern "C" void Transform_set_localPosition_m1812 ();
extern "C" void Transform_INTERNAL_get_localPosition_m2618 ();
extern "C" void Transform_INTERNAL_set_localPosition_m2619 ();
extern "C" void Transform_get_forward_m1675 ();
extern "C" void Transform_get_rotation_m1671 ();
extern "C" void Transform_INTERNAL_get_rotation_m2620 ();
extern "C" void Transform_get_localRotation_m1805 ();
extern "C" void Transform_set_localRotation_m1813 ();
extern "C" void Transform_INTERNAL_get_localRotation_m2621 ();
extern "C" void Transform_INTERNAL_set_localRotation_m2622 ();
extern "C" void Transform_get_localScale_m1807 ();
extern "C" void Transform_set_localScale_m237 ();
extern "C" void Transform_INTERNAL_get_localScale_m2623 ();
extern "C" void Transform_INTERNAL_set_localScale_m2624 ();
extern "C" void Transform_get_parent_m1506 ();
extern "C" void Transform_set_parent_m235 ();
extern "C" void Transform_get_parentInternal_m2625 ();
extern "C" void Transform_set_parentInternal_m2626 ();
extern "C" void Transform_SetParent_m198 ();
extern "C" void Transform_SetParent_m2627 ();
extern "C" void Transform_get_worldToLocalMatrix_m1867 ();
extern "C" void Transform_INTERNAL_get_worldToLocalMatrix_m2628 ();
extern "C" void Transform_TransformPoint_m1888 ();
extern "C" void Transform_INTERNAL_CALL_TransformPoint_m2629 ();
extern "C" void Transform_InverseTransformPoint_m1766 ();
extern "C" void Transform_INTERNAL_CALL_InverseTransformPoint_m2630 ();
extern "C" void Transform_get_childCount_m228 ();
extern "C" void Transform_SetAsFirstSibling_m1796 ();
extern "C" void Transform_GetEnumerator_m2631 ();
extern "C" void Transform_GetChild_m225 ();
extern "C" void Time_get_deltaTime_m212 ();
extern "C" void Time_get_unscaledTime_m1552 ();
extern "C" void Time_get_unscaledDeltaTime_m1600 ();
extern "C" void Random_Range_m195 ();
extern "C" void Random_RandomRangeInt_m2632 ();
extern "C" void YieldInstruction__ctor_m2633 ();
extern "C" void PlayerPrefsException__ctor_m2634 ();
extern "C" void PlayerPrefs_TrySetInt_m2635 ();
extern "C" void PlayerPrefs_TrySetSetString_m2636 ();
extern "C" void PlayerPrefs_SetInt_m246 ();
extern "C" void PlayerPrefs_SetString_m220 ();
extern "C" void PlayerPrefs_GetString_m2637 ();
extern "C" void PlayerPrefs_GetString_m201 ();
extern "C" void PlayerPrefs_HasKey_m254 ();
extern "C" void Particle_get_position_m2638 ();
extern "C" void Particle_set_position_m2639 ();
extern "C" void Particle_get_velocity_m2640 ();
extern "C" void Particle_set_velocity_m2641 ();
extern "C" void Particle_get_energy_m2642 ();
extern "C" void Particle_set_energy_m2643 ();
extern "C" void Particle_get_startEnergy_m2644 ();
extern "C" void Particle_set_startEnergy_m2645 ();
extern "C" void Particle_get_size_m2646 ();
extern "C" void Particle_set_size_m2647 ();
extern "C" void Particle_get_rotation_m2648 ();
extern "C" void Particle_set_rotation_m2649 ();
extern "C" void Particle_get_angularVelocity_m2650 ();
extern "C" void Particle_set_angularVelocity_m2651 ();
extern "C" void Particle_get_color_m2652 ();
extern "C" void Particle_set_color_m2653 ();
extern "C" void Physics_Internal_Raycast_m2654 ();
extern "C" void Physics_INTERNAL_CALL_Internal_Raycast_m2655 ();
extern "C" void Physics_Raycast_m2656 ();
extern "C" void Physics_Raycast_m1668 ();
extern "C" void Physics_RaycastAll_m1588 ();
extern "C" void Physics_RaycastAll_m2657 ();
extern "C" void Physics_INTERNAL_CALL_RaycastAll_m2658 ();
extern "C" void RaycastHit_get_point_m1593 ();
extern "C" void RaycastHit_get_normal_m1594 ();
extern "C" void RaycastHit_get_distance_m1592 ();
extern "C" void RaycastHit_get_collider_m1591 ();
extern "C" void Physics2D__cctor_m2659 ();
extern "C" void Physics2D_Internal_Linecast_m2660 ();
extern "C" void Physics2D_INTERNAL_CALL_Internal_Linecast_m2661 ();
extern "C" void Physics2D_Linecast_m268 ();
extern "C" void Physics2D_Linecast_m2662 ();
extern "C" void Physics2D_Internal_Raycast_m2663 ();
extern "C" void Physics2D_INTERNAL_CALL_Internal_Raycast_m2664 ();
extern "C" void Physics2D_Raycast_m1669 ();
extern "C" void Physics2D_Raycast_m2665 ();
extern "C" void Physics2D_RaycastAll_m1576 ();
extern "C" void Physics2D_INTERNAL_CALL_RaycastAll_m2666 ();
extern "C" void RaycastHit2D_get_point_m1581 ();
extern "C" void RaycastHit2D_get_normal_m1582 ();
extern "C" void RaycastHit2D_get_fraction_m1670 ();
extern "C" void RaycastHit2D_get_collider_m1577 ();
extern "C" void RaycastHit2D_get_rigidbody_m2667 ();
extern "C" void RaycastHit2D_get_transform_m1579 ();
extern "C" void Rigidbody2D_get_position_m259 ();
extern "C" void Rigidbody2D_INTERNAL_get_position_m2668 ();
extern "C" void Rigidbody2D_MovePosition_m263 ();
extern "C" void Rigidbody2D_INTERNAL_CALL_MovePosition_m2669 ();
extern "C" void Collider2D_get_attachedRigidbody_m2670 ();
extern "C" void AudioConfigurationChangeHandler__ctor_m2671 ();
extern "C" void AudioConfigurationChangeHandler_Invoke_m2672 ();
extern "C" void AudioConfigurationChangeHandler_BeginInvoke_m2673 ();
extern "C" void AudioConfigurationChangeHandler_EndInvoke_m2674 ();
extern "C" void AudioSettings_InvokeOnAudioConfigurationChanged_m2675 ();
extern "C" void PCMReaderCallback__ctor_m2676 ();
extern "C" void PCMReaderCallback_Invoke_m2677 ();
extern "C" void PCMReaderCallback_BeginInvoke_m2678 ();
extern "C" void PCMReaderCallback_EndInvoke_m2679 ();
extern "C" void PCMSetPositionCallback__ctor_m2680 ();
extern "C" void PCMSetPositionCallback_Invoke_m2681 ();
extern "C" void PCMSetPositionCallback_BeginInvoke_m2682 ();
extern "C" void PCMSetPositionCallback_EndInvoke_m2683 ();
extern "C" void AudioClip_InvokePCMReaderCallback_Internal_m2684 ();
extern "C" void AudioClip_InvokePCMSetPositionCallback_Internal_m2685 ();
extern "C" void WebCamDevice_get_name_m2686 ();
extern "C" void WebCamDevice_get_isFrontFacing_m2687 ();
extern "C" void AnimationEvent__ctor_m2688 ();
extern "C" void AnimationEvent_get_data_m2689 ();
extern "C" void AnimationEvent_set_data_m2690 ();
extern "C" void AnimationEvent_get_stringParameter_m2691 ();
extern "C" void AnimationEvent_set_stringParameter_m2692 ();
extern "C" void AnimationEvent_get_floatParameter_m2693 ();
extern "C" void AnimationEvent_set_floatParameter_m2694 ();
extern "C" void AnimationEvent_get_intParameter_m2695 ();
extern "C" void AnimationEvent_set_intParameter_m2696 ();
extern "C" void AnimationEvent_get_objectReferenceParameter_m2697 ();
extern "C" void AnimationEvent_set_objectReferenceParameter_m2698 ();
extern "C" void AnimationEvent_get_functionName_m2699 ();
extern "C" void AnimationEvent_set_functionName_m2700 ();
extern "C" void AnimationEvent_get_time_m2701 ();
extern "C" void AnimationEvent_set_time_m2702 ();
extern "C" void AnimationEvent_get_messageOptions_m2703 ();
extern "C" void AnimationEvent_set_messageOptions_m2704 ();
extern "C" void AnimationEvent_get_isFiredByLegacy_m2705 ();
extern "C" void AnimationEvent_get_isFiredByAnimator_m2706 ();
extern "C" void AnimationEvent_get_animationState_m2707 ();
extern "C" void AnimationEvent_get_animatorStateInfo_m2708 ();
extern "C" void AnimationEvent_get_animatorClipInfo_m2709 ();
extern "C" void AnimationEvent_GetHash_m2710 ();
extern "C" void AnimationCurve__ctor_m2711 ();
extern "C" void AnimationCurve__ctor_m2712 ();
extern "C" void AnimationCurve_Cleanup_m2713 ();
extern "C" void AnimationCurve_Finalize_m2714 ();
extern "C" void AnimationCurve_Init_m2715 ();
extern "C" void AnimatorStateInfo_IsName_m2716 ();
extern "C" void AnimatorStateInfo_get_fullPathHash_m2717 ();
extern "C" void AnimatorStateInfo_get_nameHash_m2718 ();
extern "C" void AnimatorStateInfo_get_shortNameHash_m2719 ();
extern "C" void AnimatorStateInfo_get_normalizedTime_m2720 ();
extern "C" void AnimatorStateInfo_get_length_m2721 ();
extern "C" void AnimatorStateInfo_get_tagHash_m2722 ();
extern "C" void AnimatorStateInfo_IsTag_m2723 ();
extern "C" void AnimatorStateInfo_get_loop_m2724 ();
extern "C" void AnimatorTransitionInfo_IsName_m2725 ();
extern "C" void AnimatorTransitionInfo_IsUserName_m2726 ();
extern "C" void AnimatorTransitionInfo_get_fullPathHash_m2727 ();
extern "C" void AnimatorTransitionInfo_get_nameHash_m2728 ();
extern "C" void AnimatorTransitionInfo_get_userNameHash_m2729 ();
extern "C" void AnimatorTransitionInfo_get_normalizedTime_m2730 ();
extern "C" void AnimatorTransitionInfo_get_anyState_m2731 ();
extern "C" void AnimatorTransitionInfo_get_entry_m2732 ();
extern "C" void AnimatorTransitionInfo_get_exit_m2733 ();
extern "C" void Animator_SetTrigger_m1895 ();
extern "C" void Animator_ResetTrigger_m1894 ();
extern "C" void Animator_get_runtimeAnimatorController_m1893 ();
extern "C" void Animator_StringToHash_m2734 ();
extern "C" void Animator_SetTriggerString_m2735 ();
extern "C" void Animator_ResetTriggerString_m2736 ();
extern "C" void HumanBone_get_boneName_m2737 ();
extern "C" void HumanBone_set_boneName_m2738 ();
extern "C" void HumanBone_get_humanName_m2739 ();
extern "C" void HumanBone_set_humanName_m2740 ();
extern "C" void CharacterInfo_get_advance_m2741 ();
extern "C" void CharacterInfo_get_glyphWidth_m2742 ();
extern "C" void CharacterInfo_get_glyphHeight_m2743 ();
extern "C" void CharacterInfo_get_bearing_m2744 ();
extern "C" void CharacterInfo_get_minY_m2745 ();
extern "C" void CharacterInfo_get_maxY_m2746 ();
extern "C" void CharacterInfo_get_minX_m2747 ();
extern "C" void CharacterInfo_get_maxX_m2748 ();
extern "C" void CharacterInfo_get_uvBottomLeftUnFlipped_m2749 ();
extern "C" void CharacterInfo_get_uvBottomRightUnFlipped_m2750 ();
extern "C" void CharacterInfo_get_uvTopRightUnFlipped_m2751 ();
extern "C" void CharacterInfo_get_uvTopLeftUnFlipped_m2752 ();
extern "C" void CharacterInfo_get_uvBottomLeft_m2753 ();
extern "C" void CharacterInfo_get_uvBottomRight_m2754 ();
extern "C" void CharacterInfo_get_uvTopRight_m2755 ();
extern "C" void CharacterInfo_get_uvTopLeft_m2756 ();
extern "C" void FontTextureRebuildCallback__ctor_m2757 ();
extern "C" void FontTextureRebuildCallback_Invoke_m2758 ();
extern "C" void FontTextureRebuildCallback_BeginInvoke_m2759 ();
extern "C" void FontTextureRebuildCallback_EndInvoke_m2760 ();
extern "C" void Font_add_textureRebuilt_m1617 ();
extern "C" void Font_remove_textureRebuilt_m2761 ();
extern "C" void Font_get_material_m1905 ();
extern "C" void Font_HasCharacter_m1778 ();
extern "C" void Font_InvokeTextureRebuilt_Internal_m2762 ();
extern "C" void Font_get_dynamic_m1908 ();
extern "C" void Font_get_fontSize_m1910 ();
extern "C" void TextGenerator__ctor_m1734 ();
extern "C" void TextGenerator__ctor_m1903 ();
extern "C" void TextGenerator_System_IDisposable_Dispose_m2763 ();
extern "C" void TextGenerator_Init_m2764 ();
extern "C" void TextGenerator_Dispose_cpp_m2765 ();
extern "C" void TextGenerator_Populate_Internal_m2766 ();
extern "C" void TextGenerator_Populate_Internal_cpp_m2767 ();
extern "C" void TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m2768 ();
extern "C" void TextGenerator_get_rectExtents_m1794 ();
extern "C" void TextGenerator_INTERNAL_get_rectExtents_m2769 ();
extern "C" void TextGenerator_get_vertexCount_m2770 ();
extern "C" void TextGenerator_GetVerticesInternal_m2771 ();
extern "C" void TextGenerator_GetVerticesArray_m2772 ();
extern "C" void TextGenerator_get_characterCount_m2773 ();
extern "C" void TextGenerator_get_characterCountVisible_m1772 ();
extern "C" void TextGenerator_GetCharactersInternal_m2774 ();
extern "C" void TextGenerator_GetCharactersArray_m2775 ();
extern "C" void TextGenerator_get_lineCount_m1771 ();
extern "C" void TextGenerator_GetLinesInternal_m2776 ();
extern "C" void TextGenerator_GetLinesArray_m2777 ();
extern "C" void TextGenerator_get_fontSizeUsedForBestFit_m1818 ();
extern "C" void TextGenerator_Finalize_m2778 ();
extern "C" void TextGenerator_ValidatedSettings_m2779 ();
extern "C" void TextGenerator_Invalidate_m1907 ();
extern "C" void TextGenerator_GetCharacters_m2780 ();
extern "C" void TextGenerator_GetLines_m2781 ();
extern "C" void TextGenerator_GetVertices_m2782 ();
extern "C" void TextGenerator_GetPreferredWidth_m1913 ();
extern "C" void TextGenerator_GetPreferredHeight_m1914 ();
extern "C" void TextGenerator_Populate_m1793 ();
extern "C" void TextGenerator_PopulateAlways_m2783 ();
extern "C" void TextGenerator_get_verts_m1911 ();
extern "C" void TextGenerator_get_characters_m1773 ();
extern "C" void TextGenerator_get_lines_m1770 ();
extern "C" void WillRenderCanvases__ctor_m1603 ();
extern "C" void WillRenderCanvases_Invoke_m2784 ();
extern "C" void WillRenderCanvases_BeginInvoke_m2785 ();
extern "C" void WillRenderCanvases_EndInvoke_m2786 ();
extern "C" void Canvas_add_willRenderCanvases_m1604 ();
extern "C" void Canvas_remove_willRenderCanvases_m2787 ();
extern "C" void Canvas_get_renderMode_m1661 ();
extern "C" void Canvas_get_isRootCanvas_m1926 ();
extern "C" void Canvas_get_worldCamera_m1677 ();
extern "C" void Canvas_get_scaleFactor_m1909 ();
extern "C" void Canvas_set_scaleFactor_m1930 ();
extern "C" void Canvas_get_referencePixelsPerUnit_m1694 ();
extern "C" void Canvas_set_referencePixelsPerUnit_m1931 ();
extern "C" void Canvas_get_pixelPerfect_m1647 ();
extern "C" void Canvas_get_renderOrder_m1663 ();
extern "C" void Canvas_get_sortingOrder_m1662 ();
extern "C" void Canvas_get_cachedSortingLayerValue_m1676 ();
extern "C" void Canvas_GetDefaultCanvasMaterial_m1623 ();
extern "C" void Canvas_GetDefaultCanvasTextMaterial_m1904 ();
extern "C" void Canvas_SendWillRenderCanvases_m2788 ();
extern "C" void Canvas_ForceUpdateCanvases_m1853 ();
extern "C" void CanvasGroup_get_interactable_m1884 ();
extern "C" void CanvasGroup_get_blocksRaycasts_m2789 ();
extern "C" void CanvasGroup_get_ignoreParentGroups_m1646 ();
extern "C" void CanvasGroup_IsRaycastLocationValid_m2790 ();
extern "C" void UIVertex__cctor_m2791 ();
extern "C" void CanvasRenderer_SetColor_m1652 ();
extern "C" void CanvasRenderer_INTERNAL_CALL_SetColor_m2792 ();
extern "C" void CanvasRenderer_GetColor_m1650 ();
extern "C" void CanvasRenderer_set_isMask_m1970 ();
extern "C" void CanvasRenderer_SetMaterial_m1639 ();
extern "C" void CanvasRenderer_SetVertices_m1637 ();
extern "C" void CanvasRenderer_SetVerticesInternal_m2793 ();
extern "C" void CanvasRenderer_SetVertices_m1753 ();
extern "C" void CanvasRenderer_SetVerticesInternalArray_m2794 ();
extern "C" void CanvasRenderer_Clear_m1632 ();
extern "C" void CanvasRenderer_get_absoluteDepth_m1625 ();
extern "C" void RectTransformUtility__cctor_m2795 ();
extern "C" void RectTransformUtility_RectangleContainsScreenPoint_m1678 ();
extern "C" void RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m2796 ();
extern "C" void RectTransformUtility_PixelAdjustPoint_m1648 ();
extern "C" void RectTransformUtility_PixelAdjustPoint_m2797 ();
extern "C" void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2798 ();
extern "C" void RectTransformUtility_PixelAdjustRect_m1649 ();
extern "C" void RectTransformUtility_ScreenPointToWorldPointInRectangle_m2799 ();
extern "C" void RectTransformUtility_ScreenPointToLocalPointInRectangle_m1718 ();
extern "C" void RectTransformUtility_ScreenPointToRay_m2800 ();
extern "C" void RectTransformUtility_FlipLayoutOnAxis_m1848 ();
extern "C" void RectTransformUtility_FlipLayoutAxes_m1847 ();
extern "C" void RectTransformUtility_GetTransposed_m2801 ();
extern "C" void Request__ctor_m2802 ();
extern "C" void Request_get_sourceId_m2803 ();
extern "C" void Request_get_appId_m2804 ();
extern "C" void Request_get_domain_m2805 ();
extern "C" void Request_ToString_m2806 ();
extern "C" void ResponseBase__ctor_m2807 ();
extern "C" void ResponseBase_ParseJSONString_m2808 ();
extern "C" void ResponseBase_ParseJSONInt32_m2809 ();
extern "C" void ResponseBase_ParseJSONUInt16_m2810 ();
extern "C" void ResponseBase_ParseJSONUInt64_m2811 ();
extern "C" void ResponseBase_ParseJSONBool_m2812 ();
extern "C" void Response__ctor_m2813 ();
extern "C" void Response_get_success_m2814 ();
extern "C" void Response_set_success_m2815 ();
extern "C" void Response_get_extendedInfo_m2816 ();
extern "C" void Response_set_extendedInfo_m2817 ();
extern "C" void Response_ToString_m2818 ();
extern "C" void Response_Parse_m2819 ();
extern "C" void BasicResponse__ctor_m2820 ();
extern "C" void CreateMatchRequest__ctor_m2821 ();
extern "C" void CreateMatchRequest_get_name_m2822 ();
extern "C" void CreateMatchRequest_set_name_m2823 ();
extern "C" void CreateMatchRequest_get_size_m2824 ();
extern "C" void CreateMatchRequest_set_size_m2825 ();
extern "C" void CreateMatchRequest_get_advertise_m2826 ();
extern "C" void CreateMatchRequest_set_advertise_m2827 ();
extern "C" void CreateMatchRequest_get_password_m2828 ();
extern "C" void CreateMatchRequest_set_password_m2829 ();
extern "C" void CreateMatchRequest_get_matchAttributes_m2830 ();
extern "C" void CreateMatchRequest_ToString_m2831 ();
extern "C" void CreateMatchResponse__ctor_m2832 ();
extern "C" void CreateMatchResponse_get_address_m2833 ();
extern "C" void CreateMatchResponse_set_address_m2834 ();
extern "C" void CreateMatchResponse_get_port_m2835 ();
extern "C" void CreateMatchResponse_set_port_m2836 ();
extern "C" void CreateMatchResponse_get_networkId_m2837 ();
extern "C" void CreateMatchResponse_set_networkId_m2838 ();
extern "C" void CreateMatchResponse_get_accessTokenString_m2839 ();
extern "C" void CreateMatchResponse_set_accessTokenString_m2840 ();
extern "C" void CreateMatchResponse_get_nodeId_m2841 ();
extern "C" void CreateMatchResponse_set_nodeId_m2842 ();
extern "C" void CreateMatchResponse_get_usingRelay_m2843 ();
extern "C" void CreateMatchResponse_set_usingRelay_m2844 ();
extern "C" void CreateMatchResponse_ToString_m2845 ();
extern "C" void CreateMatchResponse_Parse_m2846 ();
extern "C" void JoinMatchRequest__ctor_m2847 ();
extern "C" void JoinMatchRequest_get_networkId_m2848 ();
extern "C" void JoinMatchRequest_set_networkId_m2849 ();
extern "C" void JoinMatchRequest_get_password_m2850 ();
extern "C" void JoinMatchRequest_set_password_m2851 ();
extern "C" void JoinMatchRequest_ToString_m2852 ();
extern "C" void JoinMatchResponse__ctor_m2853 ();
extern "C" void JoinMatchResponse_get_address_m2854 ();
extern "C" void JoinMatchResponse_set_address_m2855 ();
extern "C" void JoinMatchResponse_get_port_m2856 ();
extern "C" void JoinMatchResponse_set_port_m2857 ();
extern "C" void JoinMatchResponse_get_networkId_m2858 ();
extern "C" void JoinMatchResponse_set_networkId_m2859 ();
extern "C" void JoinMatchResponse_get_accessTokenString_m2860 ();
extern "C" void JoinMatchResponse_set_accessTokenString_m2861 ();
extern "C" void JoinMatchResponse_get_nodeId_m2862 ();
extern "C" void JoinMatchResponse_set_nodeId_m2863 ();
extern "C" void JoinMatchResponse_get_usingRelay_m2864 ();
extern "C" void JoinMatchResponse_set_usingRelay_m2865 ();
extern "C" void JoinMatchResponse_ToString_m2866 ();
extern "C" void JoinMatchResponse_Parse_m2867 ();
extern "C" void DestroyMatchRequest__ctor_m2868 ();
extern "C" void DestroyMatchRequest_get_networkId_m2869 ();
extern "C" void DestroyMatchRequest_set_networkId_m2870 ();
extern "C" void DestroyMatchRequest_ToString_m2871 ();
extern "C" void DropConnectionRequest__ctor_m2872 ();
extern "C" void DropConnectionRequest_get_networkId_m2873 ();
extern "C" void DropConnectionRequest_set_networkId_m2874 ();
extern "C" void DropConnectionRequest_get_nodeId_m2875 ();
extern "C" void DropConnectionRequest_set_nodeId_m2876 ();
extern "C" void DropConnectionRequest_ToString_m2877 ();
extern "C" void ListMatchRequest__ctor_m2878 ();
extern "C" void ListMatchRequest_get_pageSize_m2879 ();
extern "C" void ListMatchRequest_set_pageSize_m2880 ();
extern "C" void ListMatchRequest_get_pageNum_m2881 ();
extern "C" void ListMatchRequest_set_pageNum_m2882 ();
extern "C" void ListMatchRequest_get_nameFilter_m2883 ();
extern "C" void ListMatchRequest_set_nameFilter_m2884 ();
extern "C" void ListMatchRequest_get_includePasswordMatches_m2885 ();
extern "C" void ListMatchRequest_get_matchAttributeFilterLessThan_m2886 ();
extern "C" void ListMatchRequest_get_matchAttributeFilterGreaterThan_m2887 ();
extern "C" void ListMatchRequest_ToString_m2888 ();
extern "C" void MatchDirectConnectInfo__ctor_m2889 ();
extern "C" void MatchDirectConnectInfo_get_nodeId_m2890 ();
extern "C" void MatchDirectConnectInfo_set_nodeId_m2891 ();
extern "C" void MatchDirectConnectInfo_get_publicAddress_m2892 ();
extern "C" void MatchDirectConnectInfo_set_publicAddress_m2893 ();
extern "C" void MatchDirectConnectInfo_get_privateAddress_m2894 ();
extern "C" void MatchDirectConnectInfo_set_privateAddress_m2895 ();
extern "C" void MatchDirectConnectInfo_ToString_m2896 ();
extern "C" void MatchDirectConnectInfo_Parse_m2897 ();
extern "C" void MatchDesc__ctor_m2898 ();
extern "C" void MatchDesc_get_networkId_m2899 ();
extern "C" void MatchDesc_set_networkId_m2900 ();
extern "C" void MatchDesc_get_name_m2901 ();
extern "C" void MatchDesc_set_name_m2902 ();
extern "C" void MatchDesc_get_averageEloScore_m2903 ();
extern "C" void MatchDesc_get_maxSize_m2904 ();
extern "C" void MatchDesc_set_maxSize_m2905 ();
extern "C" void MatchDesc_get_currentSize_m2906 ();
extern "C" void MatchDesc_set_currentSize_m2907 ();
extern "C" void MatchDesc_get_isPrivate_m2908 ();
extern "C" void MatchDesc_set_isPrivate_m2909 ();
extern "C" void MatchDesc_get_matchAttributes_m2910 ();
extern "C" void MatchDesc_get_hostNodeId_m2911 ();
extern "C" void MatchDesc_get_directConnectInfos_m2912 ();
extern "C" void MatchDesc_set_directConnectInfos_m2913 ();
extern "C" void MatchDesc_ToString_m2914 ();
extern "C" void MatchDesc_Parse_m2915 ();
extern "C" void ListMatchResponse__ctor_m2916 ();
extern "C" void ListMatchResponse_get_matches_m2917 ();
extern "C" void ListMatchResponse_set_matches_m2918 ();
extern "C" void ListMatchResponse_ToString_m2919 ();
extern "C" void ListMatchResponse_Parse_m2920 ();
extern "C" void NetworkAccessToken__ctor_m2921 ();
extern "C" void NetworkAccessToken_GetByteString_m2922 ();
extern "C" void Utility__cctor_m2923 ();
extern "C" void Utility_GetSourceID_m2924 ();
extern "C" void Utility_SetAppID_m2925 ();
extern "C" void Utility_GetAppID_m2926 ();
extern "C" void Utility_GetAccessTokenForNetwork_m2927 ();
extern "C" void NetworkMatch__ctor_m2928 ();
extern "C" void NetworkMatch_get_baseUri_m2929 ();
extern "C" void NetworkMatch_set_baseUri_m2930 ();
extern "C" void NetworkMatch_SetProgramAppID_m2931 ();
extern "C" void NetworkMatch_CreateMatch_m2932 ();
extern "C" void NetworkMatch_CreateMatch_m2933 ();
extern "C" void NetworkMatch_JoinMatch_m2934 ();
extern "C" void NetworkMatch_JoinMatch_m2935 ();
extern "C" void NetworkMatch_DestroyMatch_m2936 ();
extern "C" void NetworkMatch_DestroyMatch_m2937 ();
extern "C" void NetworkMatch_DropConnection_m2938 ();
extern "C" void NetworkMatch_DropConnection_m2939 ();
extern "C" void NetworkMatch_ListMatches_m2940 ();
extern "C" void NetworkMatch_ListMatches_m2941 ();
extern "C" void JsonArray__ctor_m2942 ();
extern "C" void JsonArray_ToString_m2943 ();
extern "C" void JsonObject__ctor_m2944 ();
extern "C" void JsonObject_System_Collections_IEnumerable_GetEnumerator_m2945 ();
extern "C" void JsonObject_Add_m2946 ();
extern "C" void JsonObject_get_Keys_m2947 ();
extern "C" void JsonObject_TryGetValue_m2948 ();
extern "C" void JsonObject_get_Values_m2949 ();
extern "C" void JsonObject_get_Item_m2950 ();
extern "C" void JsonObject_set_Item_m2951 ();
extern "C" void JsonObject_Add_m2952 ();
extern "C" void JsonObject_Clear_m2953 ();
extern "C" void JsonObject_Contains_m2954 ();
extern "C" void JsonObject_CopyTo_m2955 ();
extern "C" void JsonObject_get_Count_m2956 ();
extern "C" void JsonObject_get_IsReadOnly_m2957 ();
extern "C" void JsonObject_Remove_m2958 ();
extern "C" void JsonObject_GetEnumerator_m2959 ();
extern "C" void JsonObject_ToString_m2960 ();
extern "C" void SimpleJson_TryDeserializeObject_m2961 ();
extern "C" void SimpleJson_SerializeObject_m2962 ();
extern "C" void SimpleJson_SerializeObject_m2963 ();
extern "C" void SimpleJson_ParseObject_m2964 ();
extern "C" void SimpleJson_ParseArray_m2965 ();
extern "C" void SimpleJson_ParseValue_m2966 ();
extern "C" void SimpleJson_ParseString_m2967 ();
extern "C" void SimpleJson_ConvertFromUtf32_m2968 ();
extern "C" void SimpleJson_ParseNumber_m2969 ();
extern "C" void SimpleJson_GetLastIndexOfNumber_m2970 ();
extern "C" void SimpleJson_EatWhitespace_m2971 ();
extern "C" void SimpleJson_LookAhead_m2972 ();
extern "C" void SimpleJson_NextToken_m2973 ();
extern "C" void SimpleJson_SerializeValue_m2974 ();
extern "C" void SimpleJson_SerializeObject_m2975 ();
extern "C" void SimpleJson_SerializeArray_m2976 ();
extern "C" void SimpleJson_SerializeString_m2977 ();
extern "C" void SimpleJson_SerializeNumber_m2978 ();
extern "C" void SimpleJson_IsNumeric_m2979 ();
extern "C" void SimpleJson_get_CurrentJsonSerializerStrategy_m2980 ();
extern "C" void SimpleJson_get_PocoJsonSerializerStrategy_m2981 ();
extern "C" void PocoJsonSerializerStrategy__ctor_m2982 ();
extern "C" void PocoJsonSerializerStrategy__cctor_m2983 ();
extern "C" void PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m2984 ();
extern "C" void PocoJsonSerializerStrategy_ContructorDelegateFactory_m2985 ();
extern "C" void PocoJsonSerializerStrategy_GetterValueFactory_m2986 ();
extern "C" void PocoJsonSerializerStrategy_SetterValueFactory_m2987 ();
extern "C" void PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m2988 ();
extern "C" void PocoJsonSerializerStrategy_SerializeEnum_m2989 ();
extern "C" void PocoJsonSerializerStrategy_TrySerializeKnownTypes_m2990 ();
extern "C" void PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m2991 ();
extern "C" void GetDelegate__ctor_m2992 ();
extern "C" void GetDelegate_Invoke_m2993 ();
extern "C" void GetDelegate_BeginInvoke_m2994 ();
extern "C" void GetDelegate_EndInvoke_m2995 ();
extern "C" void SetDelegate__ctor_m2996 ();
extern "C" void SetDelegate_Invoke_m2997 ();
extern "C" void SetDelegate_BeginInvoke_m2998 ();
extern "C" void SetDelegate_EndInvoke_m2999 ();
extern "C" void ConstructorDelegate__ctor_m3000 ();
extern "C" void ConstructorDelegate_Invoke_m3001 ();
extern "C" void ConstructorDelegate_BeginInvoke_m3002 ();
extern "C" void ConstructorDelegate_EndInvoke_m3003 ();
extern "C" void U3CGetConstructorByReflectionU3Ec__AnonStorey1__ctor_m3004 ();
extern "C" void U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m3005 ();
extern "C" void U3CGetGetMethodByReflectionU3Ec__AnonStorey2__ctor_m3006 ();
extern "C" void U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m3007 ();
extern "C" void U3CGetGetMethodByReflectionU3Ec__AnonStorey3__ctor_m3008 ();
extern "C" void U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m3009 ();
extern "C" void U3CGetSetMethodByReflectionU3Ec__AnonStorey4__ctor_m3010 ();
extern "C" void U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m3011 ();
extern "C" void U3CGetSetMethodByReflectionU3Ec__AnonStorey5__ctor_m3012 ();
extern "C" void U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m3013 ();
extern "C" void ReflectionUtils__cctor_m3014 ();
extern "C" void ReflectionUtils_GetConstructors_m3015 ();
extern "C" void ReflectionUtils_GetConstructorInfo_m3016 ();
extern "C" void ReflectionUtils_GetProperties_m3017 ();
extern "C" void ReflectionUtils_GetFields_m3018 ();
extern "C" void ReflectionUtils_GetGetterMethodInfo_m3019 ();
extern "C" void ReflectionUtils_GetSetterMethodInfo_m3020 ();
extern "C" void ReflectionUtils_GetContructor_m3021 ();
extern "C" void ReflectionUtils_GetConstructorByReflection_m3022 ();
extern "C" void ReflectionUtils_GetConstructorByReflection_m3023 ();
extern "C" void ReflectionUtils_GetGetMethod_m3024 ();
extern "C" void ReflectionUtils_GetGetMethod_m3025 ();
extern "C" void ReflectionUtils_GetGetMethodByReflection_m3026 ();
extern "C" void ReflectionUtils_GetGetMethodByReflection_m3027 ();
extern "C" void ReflectionUtils_GetSetMethod_m3028 ();
extern "C" void ReflectionUtils_GetSetMethod_m3029 ();
extern "C" void ReflectionUtils_GetSetMethodByReflection_m3030 ();
extern "C" void ReflectionUtils_GetSetMethodByReflection_m3031 ();
extern "C" void WrapperlessIcall__ctor_m3032 ();
extern "C" void IL2CPPStructAlignmentAttribute__ctor_m3033 ();
extern "C" void AttributeHelperEngine__cctor_m3034 ();
extern "C" void AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m3035 ();
extern "C" void AttributeHelperEngine_GetRequiredComponents_m3036 ();
extern "C" void AttributeHelperEngine_CheckIsEditorScript_m3037 ();
extern "C" void DisallowMultipleComponent__ctor_m3038 ();
extern "C" void RequireComponent__ctor_m3039 ();
extern "C" void AddComponentMenu__ctor_m3040 ();
extern "C" void AddComponentMenu__ctor_m3041 ();
extern "C" void ExecuteInEditMode__ctor_m3042 ();
extern "C" void HideInInspector__ctor_m3043 ();
extern "C" void SetupCoroutine__ctor_m3044 ();
extern "C" void SetupCoroutine_InvokeMember_m3045 ();
extern "C" void SetupCoroutine_InvokeStatic_m3046 ();
extern "C" void WritableAttribute__ctor_m3047 ();
extern "C" void AssemblyIsEditorAssembly__ctor_m3048 ();
extern "C" void GcUserProfileData_ToUserProfile_m3049 ();
extern "C" void GcUserProfileData_AddToArray_m3050 ();
extern "C" void GcAchievementDescriptionData_ToAchievementDescription_m3051 ();
extern "C" void GcAchievementData_ToAchievement_m3052 ();
extern "C" void GcScoreData_ToScore_m3053 ();
extern "C" void Resolution_get_width_m3054 ();
extern "C" void Resolution_set_width_m3055 ();
extern "C" void Resolution_get_height_m3056 ();
extern "C" void Resolution_set_height_m3057 ();
extern "C" void Resolution_get_refreshRate_m3058 ();
extern "C" void Resolution_set_refreshRate_m3059 ();
extern "C" void Resolution_ToString_m3060 ();
extern "C" void LocalUser__ctor_m3061 ();
extern "C" void LocalUser_SetFriends_m3062 ();
extern "C" void LocalUser_SetAuthenticated_m3063 ();
extern "C" void LocalUser_SetUnderage_m3064 ();
extern "C" void LocalUser_get_authenticated_m3065 ();
extern "C" void UserProfile__ctor_m3066 ();
extern "C" void UserProfile__ctor_m3067 ();
extern "C" void UserProfile_ToString_m3068 ();
extern "C" void UserProfile_SetUserName_m3069 ();
extern "C" void UserProfile_SetUserID_m3070 ();
extern "C" void UserProfile_SetImage_m3071 ();
extern "C" void UserProfile_get_userName_m3072 ();
extern "C" void UserProfile_get_id_m3073 ();
extern "C" void UserProfile_get_isFriend_m3074 ();
extern "C" void UserProfile_get_state_m3075 ();
extern "C" void Achievement__ctor_m3076 ();
extern "C" void Achievement__ctor_m3077 ();
extern "C" void Achievement__ctor_m3078 ();
extern "C" void Achievement_ToString_m3079 ();
extern "C" void Achievement_get_id_m3080 ();
extern "C" void Achievement_set_id_m3081 ();
extern "C" void Achievement_get_percentCompleted_m3082 ();
extern "C" void Achievement_set_percentCompleted_m3083 ();
extern "C" void Achievement_get_completed_m3084 ();
extern "C" void Achievement_get_hidden_m3085 ();
extern "C" void Achievement_get_lastReportedDate_m3086 ();
extern "C" void AchievementDescription__ctor_m3087 ();
extern "C" void AchievementDescription_ToString_m3088 ();
extern "C" void AchievementDescription_SetImage_m3089 ();
extern "C" void AchievementDescription_get_id_m3090 ();
extern "C" void AchievementDescription_set_id_m3091 ();
extern "C" void AchievementDescription_get_title_m3092 ();
extern "C" void AchievementDescription_get_achievedDescription_m3093 ();
extern "C" void AchievementDescription_get_unachievedDescription_m3094 ();
extern "C" void AchievementDescription_get_hidden_m3095 ();
extern "C" void AchievementDescription_get_points_m3096 ();
extern "C" void Score__ctor_m3097 ();
extern "C" void Score__ctor_m3098 ();
extern "C" void Score_ToString_m3099 ();
extern "C" void Score_get_leaderboardID_m3100 ();
extern "C" void Score_set_leaderboardID_m3101 ();
extern "C" void Score_get_value_m3102 ();
extern "C" void Score_set_value_m3103 ();
extern "C" void Leaderboard__ctor_m3104 ();
extern "C" void Leaderboard_ToString_m3105 ();
extern "C" void Leaderboard_SetLocalUserScore_m3106 ();
extern "C" void Leaderboard_SetMaxRange_m3107 ();
extern "C" void Leaderboard_SetScores_m3108 ();
extern "C" void Leaderboard_SetTitle_m3109 ();
extern "C" void Leaderboard_GetUserFilter_m3110 ();
extern "C" void Leaderboard_get_id_m3111 ();
extern "C" void Leaderboard_set_id_m3112 ();
extern "C" void Leaderboard_get_userScope_m3113 ();
extern "C" void Leaderboard_set_userScope_m3114 ();
extern "C" void Leaderboard_get_range_m3115 ();
extern "C" void Leaderboard_set_range_m3116 ();
extern "C" void Leaderboard_get_timeScope_m3117 ();
extern "C" void Leaderboard_set_timeScope_m3118 ();
extern "C" void HitInfo_SendMessage_m3119 ();
extern "C" void HitInfo_Compare_m3120 ();
extern "C" void HitInfo_op_Implicit_m3121 ();
extern "C" void SendMouseEvents__cctor_m3122 ();
extern "C" void SendMouseEvents_DoSendMouseEvents_m3123 ();
extern "C" void SendMouseEvents_SendEvents_m3124 ();
extern "C" void Range__ctor_m3125 ();
extern "C" void PropertyAttribute__ctor_m3126 ();
extern "C" void TooltipAttribute__ctor_m3127 ();
extern "C" void SpaceAttribute__ctor_m3128 ();
extern "C" void RangeAttribute__ctor_m3129 ();
extern "C" void TextAreaAttribute__ctor_m3130 ();
extern "C" void SelectionBaseAttribute__ctor_m3131 ();
extern "C" void SliderState__ctor_m3132 ();
extern "C" void StackTraceUtility__ctor_m3133 ();
extern "C" void StackTraceUtility__cctor_m3134 ();
extern "C" void StackTraceUtility_SetProjectFolder_m3135 ();
extern "C" void StackTraceUtility_ExtractStackTrace_m3136 ();
extern "C" void StackTraceUtility_IsSystemStacktraceType_m3137 ();
extern "C" void StackTraceUtility_ExtractStringFromException_m3138 ();
extern "C" void StackTraceUtility_ExtractStringFromExceptionInternal_m3139 ();
extern "C" void StackTraceUtility_PostprocessStacktrace_m3140 ();
extern "C" void StackTraceUtility_ExtractFormattedStackTrace_m3141 ();
extern "C" void UnityException__ctor_m3142 ();
extern "C" void UnityException__ctor_m3143 ();
extern "C" void UnityException__ctor_m3144 ();
extern "C" void UnityException__ctor_m3145 ();
extern "C" void SharedBetweenAnimatorsAttribute__ctor_m3146 ();
extern "C" void StateMachineBehaviour__ctor_m3147 ();
extern "C" void StateMachineBehaviour_OnStateEnter_m3148 ();
extern "C" void StateMachineBehaviour_OnStateUpdate_m3149 ();
extern "C" void StateMachineBehaviour_OnStateExit_m3150 ();
extern "C" void StateMachineBehaviour_OnStateMove_m3151 ();
extern "C" void StateMachineBehaviour_OnStateIK_m3152 ();
extern "C" void StateMachineBehaviour_OnStateMachineEnter_m3153 ();
extern "C" void StateMachineBehaviour_OnStateMachineExit_m3154 ();
extern "C" void TextEditor__ctor_m1754 ();
extern "C" void TextEditor_ClearCursorPos_m3155 ();
extern "C" void TextEditor_OnFocus_m1758 ();
extern "C" void TextEditor_SelectAll_m3156 ();
extern "C" void TextEditor_DeleteSelection_m3157 ();
extern "C" void TextEditor_ReplaceSelection_m3158 ();
extern "C" void TextEditor_UpdateScrollOffset_m3159 ();
extern "C" void TextEditor_Copy_m1759 ();
extern "C" void TextEditor_ReplaceNewlinesWithSpaces_m3160 ();
extern "C" void TextEditor_Paste_m1755 ();
extern "C" void TextGenerationSettings_CompareColors_m3161 ();
extern "C" void TextGenerationSettings_CompareVector2_m3162 ();
extern "C" void TextGenerationSettings_Equals_m3163 ();
extern "C" void TrackedReference_Equals_m3164 ();
extern "C" void TrackedReference_GetHashCode_m3165 ();
extern "C" void TrackedReference_op_Equality_m3166 ();
extern "C" void ArgumentCache__ctor_m3167 ();
extern "C" void ArgumentCache_get_unityObjectArgument_m3168 ();
extern "C" void ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3169 ();
extern "C" void ArgumentCache_get_intArgument_m3170 ();
extern "C" void ArgumentCache_get_floatArgument_m3171 ();
extern "C" void ArgumentCache_get_stringArgument_m3172 ();
extern "C" void ArgumentCache_get_boolArgument_m3173 ();
extern "C" void ArgumentCache_TidyAssemblyTypeName_m3174 ();
extern "C" void ArgumentCache_OnBeforeSerialize_m3175 ();
extern "C" void ArgumentCache_OnAfterDeserialize_m3176 ();
extern "C" void BaseInvokableCall__ctor_m3177 ();
extern "C" void BaseInvokableCall__ctor_m3178 ();
extern "C" void BaseInvokableCall_AllowInvoke_m3179 ();
extern "C" void InvokableCall__ctor_m3180 ();
extern "C" void InvokableCall_Invoke_m3181 ();
extern "C" void InvokableCall_Find_m3182 ();
extern "C" void PersistentCall__ctor_m3183 ();
extern "C" void PersistentCall_get_target_m3184 ();
extern "C" void PersistentCall_get_methodName_m3185 ();
extern "C" void PersistentCall_get_mode_m3186 ();
extern "C" void PersistentCall_get_arguments_m3187 ();
extern "C" void PersistentCall_IsValid_m3188 ();
extern "C" void PersistentCall_GetRuntimeCall_m3189 ();
extern "C" void PersistentCall_GetObjectCall_m3190 ();
extern "C" void PersistentCallGroup__ctor_m3191 ();
extern "C" void PersistentCallGroup_Initialize_m3192 ();
extern "C" void InvokableCallList__ctor_m3193 ();
extern "C" void InvokableCallList_AddPersistentInvokableCall_m3194 ();
extern "C" void InvokableCallList_AddListener_m3195 ();
extern "C" void InvokableCallList_RemoveListener_m3196 ();
extern "C" void InvokableCallList_ClearPersistent_m3197 ();
extern "C" void InvokableCallList_Invoke_m3198 ();
extern "C" void UnityEventBase__ctor_m3199 ();
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3200 ();
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3201 ();
extern "C" void UnityEventBase_FindMethod_m3202 ();
extern "C" void UnityEventBase_FindMethod_m3203 ();
extern "C" void UnityEventBase_DirtyPersistentCalls_m3204 ();
extern "C" void UnityEventBase_RebuildPersistentCallsIfNeeded_m3205 ();
extern "C" void UnityEventBase_AddCall_m3206 ();
extern "C" void UnityEventBase_RemoveListener_m3207 ();
extern "C" void UnityEventBase_Invoke_m3208 ();
extern "C" void UnityEventBase_ToString_m3209 ();
extern "C" void UnityEventBase_GetValidMethodInfo_m3210 ();
extern "C" void UnityEvent__ctor_m1599 ();
extern "C" void UnityEvent_FindMethod_Impl_m3211 ();
extern "C" void UnityEvent_GetDelegate_m3212 ();
extern "C" void UnityEvent_Invoke_m1601 ();
extern "C" void UserAuthorizationDialog__ctor_m3213 ();
extern "C" void UserAuthorizationDialog_Start_m3214 ();
extern "C" void UserAuthorizationDialog_OnGUI_m3215 ();
extern "C" void UserAuthorizationDialog_DoUserAuthorizationDialog_m3216 ();
extern "C" void DefaultValueAttribute__ctor_m3217 ();
extern "C" void DefaultValueAttribute_get_Value_m3218 ();
extern "C" void DefaultValueAttribute_Equals_m3219 ();
extern "C" void DefaultValueAttribute_GetHashCode_m3220 ();
extern "C" void ExcludeFromDocsAttribute__ctor_m3221 ();
extern "C" void FormerlySerializedAsAttribute__ctor_m3222 ();
extern "C" void TypeInferenceRuleAttribute__ctor_m3223 ();
extern "C" void TypeInferenceRuleAttribute__ctor_m3224 ();
extern "C" void TypeInferenceRuleAttribute_ToString_m3225 ();
extern "C" void GenericStack__ctor_m3226 ();
extern "C" void UnityAction__ctor_m1752 ();
extern "C" void UnityAction_Invoke_m1624 ();
extern "C" void UnityAction_BeginInvoke_m3227 ();
extern "C" void UnityAction_EndInvoke_m3228 ();
extern "C" void ExtensionAttribute__ctor_m3400 ();
extern "C" void Locale_GetText_m3401 ();
extern "C" void Locale_GetText_m3402 ();
extern "C" void KeyBuilder_get_Rng_m3403 ();
extern "C" void KeyBuilder_Key_m3404 ();
extern "C" void KeyBuilder_IV_m3405 ();
extern "C" void SymmetricTransform__ctor_m3406 ();
extern "C" void SymmetricTransform_System_IDisposable_Dispose_m3407 ();
extern "C" void SymmetricTransform_Finalize_m3408 ();
extern "C" void SymmetricTransform_Dispose_m3409 ();
extern "C" void SymmetricTransform_get_CanReuseTransform_m3410 ();
extern "C" void SymmetricTransform_Transform_m3411 ();
extern "C" void SymmetricTransform_CBC_m3412 ();
extern "C" void SymmetricTransform_CFB_m3413 ();
extern "C" void SymmetricTransform_OFB_m3414 ();
extern "C" void SymmetricTransform_CTS_m3415 ();
extern "C" void SymmetricTransform_CheckInput_m3416 ();
extern "C" void SymmetricTransform_TransformBlock_m3417 ();
extern "C" void SymmetricTransform_get_KeepLastBlock_m3418 ();
extern "C" void SymmetricTransform_InternalTransformBlock_m3419 ();
extern "C" void SymmetricTransform_Random_m3420 ();
extern "C" void SymmetricTransform_ThrowBadPaddingException_m3421 ();
extern "C" void SymmetricTransform_FinalEncrypt_m3422 ();
extern "C" void SymmetricTransform_FinalDecrypt_m3423 ();
extern "C" void SymmetricTransform_TransformFinalBlock_m3424 ();
extern "C" void Check_SourceAndPredicate_m3425 ();
extern "C" void Aes__ctor_m3426 ();
extern "C" void AesManaged__ctor_m3427 ();
extern "C" void AesManaged_GenerateIV_m3428 ();
extern "C" void AesManaged_GenerateKey_m3429 ();
extern "C" void AesManaged_CreateDecryptor_m3430 ();
extern "C" void AesManaged_CreateEncryptor_m3431 ();
extern "C" void AesManaged_get_IV_m3432 ();
extern "C" void AesManaged_set_IV_m3433 ();
extern "C" void AesManaged_get_Key_m3434 ();
extern "C" void AesManaged_set_Key_m3435 ();
extern "C" void AesManaged_get_KeySize_m3436 ();
extern "C" void AesManaged_set_KeySize_m3437 ();
extern "C" void AesManaged_CreateDecryptor_m3438 ();
extern "C" void AesManaged_CreateEncryptor_m3439 ();
extern "C" void AesManaged_Dispose_m3440 ();
extern "C" void AesTransform__ctor_m3441 ();
extern "C" void AesTransform__cctor_m3442 ();
extern "C" void AesTransform_ECB_m3443 ();
extern "C" void AesTransform_SubByte_m3444 ();
extern "C" void AesTransform_Encrypt128_m3445 ();
extern "C" void AesTransform_Decrypt128_m3446 ();
extern "C" void Locale_GetText_m3467 ();
extern "C" void ModulusRing__ctor_m3468 ();
extern "C" void ModulusRing_BarrettReduction_m3469 ();
extern "C" void ModulusRing_Multiply_m3470 ();
extern "C" void ModulusRing_Difference_m3471 ();
extern "C" void ModulusRing_Pow_m3472 ();
extern "C" void ModulusRing_Pow_m3473 ();
extern "C" void Kernel_AddSameSign_m3474 ();
extern "C" void Kernel_Subtract_m3475 ();
extern "C" void Kernel_MinusEq_m3476 ();
extern "C" void Kernel_PlusEq_m3477 ();
extern "C" void Kernel_Compare_m3478 ();
extern "C" void Kernel_SingleByteDivideInPlace_m3479 ();
extern "C" void Kernel_DwordMod_m3480 ();
extern "C" void Kernel_DwordDivMod_m3481 ();
extern "C" void Kernel_multiByteDivide_m3482 ();
extern "C" void Kernel_LeftShift_m3483 ();
extern "C" void Kernel_RightShift_m3484 ();
extern "C" void Kernel_Multiply_m3485 ();
extern "C" void Kernel_MultiplyMod2p32pmod_m3486 ();
extern "C" void Kernel_modInverse_m3487 ();
extern "C" void Kernel_modInverse_m3488 ();
extern "C" void BigInteger__ctor_m3489 ();
extern "C" void BigInteger__ctor_m3490 ();
extern "C" void BigInteger__ctor_m3491 ();
extern "C" void BigInteger__ctor_m3492 ();
extern "C" void BigInteger__ctor_m3493 ();
extern "C" void BigInteger__cctor_m3494 ();
extern "C" void BigInteger_get_Rng_m3495 ();
extern "C" void BigInteger_GenerateRandom_m3496 ();
extern "C" void BigInteger_GenerateRandom_m3497 ();
extern "C" void BigInteger_BitCount_m3498 ();
extern "C" void BigInteger_TestBit_m3499 ();
extern "C" void BigInteger_SetBit_m3500 ();
extern "C" void BigInteger_SetBit_m3501 ();
extern "C" void BigInteger_LowestSetBit_m3502 ();
extern "C" void BigInteger_GetBytes_m3503 ();
extern "C" void BigInteger_ToString_m3504 ();
extern "C" void BigInteger_ToString_m3505 ();
extern "C" void BigInteger_Normalize_m3506 ();
extern "C" void BigInteger_Clear_m3507 ();
extern "C" void BigInteger_GetHashCode_m3508 ();
extern "C" void BigInteger_ToString_m3509 ();
extern "C" void BigInteger_Equals_m3510 ();
extern "C" void BigInteger_ModInverse_m3511 ();
extern "C" void BigInteger_ModPow_m3512 ();
extern "C" void BigInteger_GeneratePseudoPrime_m3513 ();
extern "C" void BigInteger_Incr2_m3514 ();
extern "C" void BigInteger_op_Implicit_m3515 ();
extern "C" void BigInteger_op_Implicit_m3516 ();
extern "C" void BigInteger_op_Addition_m3517 ();
extern "C" void BigInteger_op_Subtraction_m3518 ();
extern "C" void BigInteger_op_Modulus_m3519 ();
extern "C" void BigInteger_op_Modulus_m3520 ();
extern "C" void BigInteger_op_Division_m3521 ();
extern "C" void BigInteger_op_Multiply_m3522 ();
extern "C" void BigInteger_op_LeftShift_m3523 ();
extern "C" void BigInteger_op_RightShift_m3524 ();
extern "C" void BigInteger_op_Equality_m3525 ();
extern "C" void BigInteger_op_Inequality_m3526 ();
extern "C" void BigInteger_op_Equality_m3527 ();
extern "C" void BigInteger_op_Inequality_m3528 ();
extern "C" void BigInteger_op_GreaterThan_m3529 ();
extern "C" void BigInteger_op_LessThan_m3530 ();
extern "C" void BigInteger_op_GreaterThanOrEqual_m3531 ();
extern "C" void BigInteger_op_LessThanOrEqual_m3532 ();
extern "C" void PrimalityTests_GetSPPRounds_m3533 ();
extern "C" void PrimalityTests_RabinMillerTest_m3534 ();
extern "C" void PrimeGeneratorBase__ctor_m3535 ();
extern "C" void PrimeGeneratorBase_get_Confidence_m3536 ();
extern "C" void PrimeGeneratorBase_get_PrimalityTest_m3537 ();
extern "C" void PrimeGeneratorBase_get_TrialDivisionBounds_m3538 ();
extern "C" void SequentialSearchPrimeGeneratorBase__ctor_m3539 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m3540 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m3541 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m3542 ();
extern "C" void SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m3543 ();
extern "C" void ASN1__ctor_m3544 ();
extern "C" void ASN1__ctor_m3545 ();
extern "C" void ASN1__ctor_m3546 ();
extern "C" void ASN1_get_Count_m3547 ();
extern "C" void ASN1_get_Tag_m3548 ();
extern "C" void ASN1_get_Length_m3549 ();
extern "C" void ASN1_get_Value_m3550 ();
extern "C" void ASN1_set_Value_m3551 ();
extern "C" void ASN1_CompareArray_m3552 ();
extern "C" void ASN1_CompareValue_m3553 ();
extern "C" void ASN1_Add_m3554 ();
extern "C" void ASN1_GetBytes_m3555 ();
extern "C" void ASN1_Decode_m3556 ();
extern "C" void ASN1_DecodeTLV_m3557 ();
extern "C" void ASN1_get_Item_m3558 ();
extern "C" void ASN1_Element_m3559 ();
extern "C" void ASN1_ToString_m3560 ();
extern "C" void ASN1Convert_FromInt32_m3561 ();
extern "C" void ASN1Convert_FromOid_m3562 ();
extern "C" void ASN1Convert_ToInt32_m3563 ();
extern "C" void ASN1Convert_ToOid_m3564 ();
extern "C" void ASN1Convert_ToDateTime_m3565 ();
extern "C" void BitConverterLE_GetUIntBytes_m3566 ();
extern "C" void BitConverterLE_GetBytes_m3567 ();
extern "C" void ContentInfo__ctor_m3568 ();
extern "C" void ContentInfo__ctor_m3569 ();
extern "C" void ContentInfo__ctor_m3570 ();
extern "C" void ContentInfo__ctor_m3571 ();
extern "C" void ContentInfo_get_Content_m3572 ();
extern "C" void ContentInfo_set_Content_m3573 ();
extern "C" void ContentInfo_get_ContentType_m3574 ();
extern "C" void EncryptedData__ctor_m3575 ();
extern "C" void EncryptedData__ctor_m3576 ();
extern "C" void EncryptedData_get_EncryptionAlgorithm_m3577 ();
extern "C" void EncryptedData_get_EncryptedContent_m3578 ();
extern "C" void ARC4Managed__ctor_m3579 ();
extern "C" void ARC4Managed_Finalize_m3580 ();
extern "C" void ARC4Managed_Dispose_m3581 ();
extern "C" void ARC4Managed_get_Key_m3582 ();
extern "C" void ARC4Managed_set_Key_m3583 ();
extern "C" void ARC4Managed_get_CanReuseTransform_m3584 ();
extern "C" void ARC4Managed_CreateEncryptor_m3585 ();
extern "C" void ARC4Managed_CreateDecryptor_m3586 ();
extern "C" void ARC4Managed_GenerateIV_m3587 ();
extern "C" void ARC4Managed_GenerateKey_m3588 ();
extern "C" void ARC4Managed_KeySetup_m3589 ();
extern "C" void ARC4Managed_CheckInput_m3590 ();
extern "C" void ARC4Managed_TransformBlock_m3591 ();
extern "C" void ARC4Managed_InternalTransformBlock_m3592 ();
extern "C" void ARC4Managed_TransformFinalBlock_m3593 ();
extern "C" void CryptoConvert_ToHex_m3594 ();
extern "C" void KeyBuilder_get_Rng_m3595 ();
extern "C" void KeyBuilder_Key_m3596 ();
extern "C" void MD2__ctor_m3597 ();
extern "C" void MD2_Create_m3598 ();
extern "C" void MD2_Create_m3599 ();
extern "C" void MD2Managed__ctor_m3600 ();
extern "C" void MD2Managed__cctor_m3601 ();
extern "C" void MD2Managed_Padding_m3602 ();
extern "C" void MD2Managed_Initialize_m3603 ();
extern "C" void MD2Managed_HashCore_m3604 ();
extern "C" void MD2Managed_HashFinal_m3605 ();
extern "C" void MD2Managed_MD2Transform_m3606 ();
extern "C" void PKCS1__cctor_m3607 ();
extern "C" void PKCS1_Compare_m3608 ();
extern "C" void PKCS1_I2OSP_m3609 ();
extern "C" void PKCS1_OS2IP_m3610 ();
extern "C" void PKCS1_RSASP1_m3611 ();
extern "C" void PKCS1_RSAVP1_m3612 ();
extern "C" void PKCS1_Sign_v15_m3613 ();
extern "C" void PKCS1_Verify_v15_m3614 ();
extern "C" void PKCS1_Verify_v15_m3615 ();
extern "C" void PKCS1_Encode_v15_m3616 ();
extern "C" void PrivateKeyInfo__ctor_m3617 ();
extern "C" void PrivateKeyInfo__ctor_m3618 ();
extern "C" void PrivateKeyInfo_get_PrivateKey_m3619 ();
extern "C" void PrivateKeyInfo_Decode_m3620 ();
extern "C" void PrivateKeyInfo_RemoveLeadingZero_m3621 ();
extern "C" void PrivateKeyInfo_Normalize_m3622 ();
extern "C" void PrivateKeyInfo_DecodeRSA_m3623 ();
extern "C" void PrivateKeyInfo_DecodeDSA_m3624 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m3625 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m3626 ();
extern "C" void EncryptedPrivateKeyInfo_get_Algorithm_m3627 ();
extern "C" void EncryptedPrivateKeyInfo_get_EncryptedData_m3628 ();
extern "C" void EncryptedPrivateKeyInfo_get_Salt_m3629 ();
extern "C" void EncryptedPrivateKeyInfo_get_IterationCount_m3630 ();
extern "C" void EncryptedPrivateKeyInfo_Decode_m3631 ();
extern "C" void RC4__ctor_m3632 ();
extern "C" void RC4__cctor_m3633 ();
extern "C" void RC4_get_IV_m3634 ();
extern "C" void RC4_set_IV_m3635 ();
extern "C" void KeyGeneratedEventHandler__ctor_m3636 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m3637 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m3638 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m3639 ();
extern "C" void RSAManaged__ctor_m3640 ();
extern "C" void RSAManaged__ctor_m3641 ();
extern "C" void RSAManaged_Finalize_m3642 ();
extern "C" void RSAManaged_GenerateKeyPair_m3643 ();
extern "C" void RSAManaged_get_KeySize_m3644 ();
extern "C" void RSAManaged_get_PublicOnly_m3645 ();
extern "C" void RSAManaged_DecryptValue_m3646 ();
extern "C" void RSAManaged_EncryptValue_m3647 ();
extern "C" void RSAManaged_ExportParameters_m3648 ();
extern "C" void RSAManaged_ImportParameters_m3649 ();
extern "C" void RSAManaged_Dispose_m3650 ();
extern "C" void RSAManaged_ToXmlString_m3651 ();
extern "C" void RSAManaged_GetPaddedValue_m3652 ();
extern "C" void SafeBag__ctor_m3653 ();
extern "C" void SafeBag_get_BagOID_m3654 ();
extern "C" void SafeBag_get_ASN1_m3655 ();
extern "C" void DeriveBytes__ctor_m3656 ();
extern "C" void DeriveBytes__cctor_m3657 ();
extern "C" void DeriveBytes_set_HashName_m3658 ();
extern "C" void DeriveBytes_set_IterationCount_m3659 ();
extern "C" void DeriveBytes_set_Password_m3660 ();
extern "C" void DeriveBytes_set_Salt_m3661 ();
extern "C" void DeriveBytes_Adjust_m3662 ();
extern "C" void DeriveBytes_Derive_m3663 ();
extern "C" void DeriveBytes_DeriveKey_m3664 ();
extern "C" void DeriveBytes_DeriveIV_m3665 ();
extern "C" void DeriveBytes_DeriveMAC_m3666 ();
extern "C" void PKCS12__ctor_m3667 ();
extern "C" void PKCS12__ctor_m3668 ();
extern "C" void PKCS12__ctor_m3669 ();
extern "C" void PKCS12__cctor_m3670 ();
extern "C" void PKCS12_Decode_m3671 ();
extern "C" void PKCS12_Finalize_m3672 ();
extern "C" void PKCS12_set_Password_m3673 ();
extern "C" void PKCS12_get_Keys_m3674 ();
extern "C" void PKCS12_get_Certificates_m3675 ();
extern "C" void PKCS12_Compare_m3676 ();
extern "C" void PKCS12_GetSymmetricAlgorithm_m3677 ();
extern "C" void PKCS12_Decrypt_m3678 ();
extern "C" void PKCS12_Decrypt_m3679 ();
extern "C" void PKCS12_GetExistingParameters_m3680 ();
extern "C" void PKCS12_AddPrivateKey_m3681 ();
extern "C" void PKCS12_ReadSafeBag_m3682 ();
extern "C" void PKCS12_MAC_m3683 ();
extern "C" void PKCS12_get_MaximumPasswordLength_m3684 ();
extern "C" void X501__cctor_m3685 ();
extern "C" void X501_ToString_m3686 ();
extern "C" void X501_ToString_m3687 ();
extern "C" void X501_AppendEntry_m3688 ();
extern "C" void X509Certificate__ctor_m3689 ();
extern "C" void X509Certificate__cctor_m3690 ();
extern "C" void X509Certificate_Parse_m3691 ();
extern "C" void X509Certificate_GetUnsignedBigInteger_m3692 ();
extern "C" void X509Certificate_get_DSA_m3693 ();
extern "C" void X509Certificate_set_DSA_m3694 ();
extern "C" void X509Certificate_get_Extensions_m3695 ();
extern "C" void X509Certificate_get_Hash_m3696 ();
extern "C" void X509Certificate_get_IssuerName_m3697 ();
extern "C" void X509Certificate_get_KeyAlgorithm_m3698 ();
extern "C" void X509Certificate_get_KeyAlgorithmParameters_m3699 ();
extern "C" void X509Certificate_set_KeyAlgorithmParameters_m3700 ();
extern "C" void X509Certificate_get_PublicKey_m3701 ();
extern "C" void X509Certificate_get_RSA_m3702 ();
extern "C" void X509Certificate_set_RSA_m3703 ();
extern "C" void X509Certificate_get_RawData_m3704 ();
extern "C" void X509Certificate_get_SerialNumber_m3705 ();
extern "C" void X509Certificate_get_Signature_m3706 ();
extern "C" void X509Certificate_get_SignatureAlgorithm_m3707 ();
extern "C" void X509Certificate_get_SubjectName_m3708 ();
extern "C" void X509Certificate_get_ValidFrom_m3709 ();
extern "C" void X509Certificate_get_ValidUntil_m3710 ();
extern "C" void X509Certificate_get_Version_m3711 ();
extern "C" void X509Certificate_get_IsCurrent_m3712 ();
extern "C" void X509Certificate_WasCurrent_m3713 ();
extern "C" void X509Certificate_VerifySignature_m3714 ();
extern "C" void X509Certificate_VerifySignature_m3715 ();
extern "C" void X509Certificate_VerifySignature_m3716 ();
extern "C" void X509Certificate_get_IsSelfSigned_m3717 ();
extern "C" void X509Certificate_GetIssuerName_m3718 ();
extern "C" void X509Certificate_GetSubjectName_m3719 ();
extern "C" void X509Certificate_GetObjectData_m3720 ();
extern "C" void X509Certificate_PEM_m3721 ();
extern "C" void X509CertificateEnumerator__ctor_m3722 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m3723 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m3724 ();
extern "C" void X509CertificateEnumerator_get_Current_m3725 ();
extern "C" void X509CertificateEnumerator_MoveNext_m3726 ();
extern "C" void X509CertificateCollection__ctor_m3727 ();
extern "C" void X509CertificateCollection__ctor_m3728 ();
extern "C" void X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m3729 ();
extern "C" void X509CertificateCollection_get_Item_m3730 ();
extern "C" void X509CertificateCollection_Add_m3731 ();
extern "C" void X509CertificateCollection_AddRange_m3732 ();
extern "C" void X509CertificateCollection_Contains_m3733 ();
extern "C" void X509CertificateCollection_GetEnumerator_m3734 ();
extern "C" void X509CertificateCollection_GetHashCode_m3735 ();
extern "C" void X509CertificateCollection_IndexOf_m3736 ();
extern "C" void X509CertificateCollection_Remove_m3737 ();
extern "C" void X509CertificateCollection_Compare_m3738 ();
extern "C" void X509Chain__ctor_m3739 ();
extern "C" void X509Chain__ctor_m3740 ();
extern "C" void X509Chain_get_Status_m3741 ();
extern "C" void X509Chain_get_TrustAnchors_m3742 ();
extern "C" void X509Chain_Build_m3743 ();
extern "C" void X509Chain_IsValid_m3744 ();
extern "C" void X509Chain_FindCertificateParent_m3745 ();
extern "C" void X509Chain_FindCertificateRoot_m3746 ();
extern "C" void X509Chain_IsTrusted_m3747 ();
extern "C" void X509Chain_IsParent_m3748 ();
extern "C" void X509CrlEntry__ctor_m3749 ();
extern "C" void X509CrlEntry_get_SerialNumber_m3750 ();
extern "C" void X509CrlEntry_get_RevocationDate_m3751 ();
extern "C" void X509CrlEntry_get_Extensions_m3752 ();
extern "C" void X509Crl__ctor_m3753 ();
extern "C" void X509Crl_Parse_m3754 ();
extern "C" void X509Crl_get_Extensions_m3755 ();
extern "C" void X509Crl_get_Hash_m3756 ();
extern "C" void X509Crl_get_IssuerName_m3757 ();
extern "C" void X509Crl_get_NextUpdate_m3758 ();
extern "C" void X509Crl_Compare_m3759 ();
extern "C" void X509Crl_GetCrlEntry_m3760 ();
extern "C" void X509Crl_GetCrlEntry_m3761 ();
extern "C" void X509Crl_GetHashName_m3762 ();
extern "C" void X509Crl_VerifySignature_m3763 ();
extern "C" void X509Crl_VerifySignature_m3764 ();
extern "C" void X509Crl_VerifySignature_m3765 ();
extern "C" void X509Extension__ctor_m3766 ();
extern "C" void X509Extension__ctor_m3767 ();
extern "C" void X509Extension_Decode_m3768 ();
extern "C" void X509Extension_Encode_m3769 ();
extern "C" void X509Extension_get_Oid_m3770 ();
extern "C" void X509Extension_get_Critical_m3771 ();
extern "C" void X509Extension_get_Value_m3772 ();
extern "C" void X509Extension_Equals_m3773 ();
extern "C" void X509Extension_GetHashCode_m3774 ();
extern "C" void X509Extension_WriteLine_m3775 ();
extern "C" void X509Extension_ToString_m3776 ();
extern "C" void X509ExtensionCollection__ctor_m3777 ();
extern "C" void X509ExtensionCollection__ctor_m3778 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m3779 ();
extern "C" void X509ExtensionCollection_IndexOf_m3780 ();
extern "C" void X509ExtensionCollection_get_Item_m3781 ();
extern "C" void X509Store__ctor_m3782 ();
extern "C" void X509Store_get_Certificates_m3783 ();
extern "C" void X509Store_get_Crls_m3784 ();
extern "C" void X509Store_Load_m3785 ();
extern "C" void X509Store_LoadCertificate_m3786 ();
extern "C" void X509Store_LoadCrl_m3787 ();
extern "C" void X509Store_CheckStore_m3788 ();
extern "C" void X509Store_BuildCertificatesCollection_m3789 ();
extern "C" void X509Store_BuildCrlsCollection_m3790 ();
extern "C" void X509StoreManager_get_CurrentUser_m3791 ();
extern "C" void X509StoreManager_get_LocalMachine_m3792 ();
extern "C" void X509StoreManager_get_TrustedRootCertificates_m3793 ();
extern "C" void X509Stores__ctor_m3794 ();
extern "C" void X509Stores_get_TrustedRoot_m3795 ();
extern "C" void X509Stores_Open_m3796 ();
extern "C" void AuthorityKeyIdentifierExtension__ctor_m3797 ();
extern "C" void AuthorityKeyIdentifierExtension_Decode_m3798 ();
extern "C" void AuthorityKeyIdentifierExtension_get_Identifier_m3799 ();
extern "C" void AuthorityKeyIdentifierExtension_ToString_m3800 ();
extern "C" void BasicConstraintsExtension__ctor_m3801 ();
extern "C" void BasicConstraintsExtension_Decode_m3802 ();
extern "C" void BasicConstraintsExtension_Encode_m3803 ();
extern "C" void BasicConstraintsExtension_get_CertificateAuthority_m3804 ();
extern "C" void BasicConstraintsExtension_ToString_m3805 ();
extern "C" void ExtendedKeyUsageExtension__ctor_m3806 ();
extern "C" void ExtendedKeyUsageExtension_Decode_m3807 ();
extern "C" void ExtendedKeyUsageExtension_Encode_m3808 ();
extern "C" void ExtendedKeyUsageExtension_get_KeyPurpose_m3809 ();
extern "C" void ExtendedKeyUsageExtension_ToString_m3810 ();
extern "C" void GeneralNames__ctor_m3811 ();
extern "C" void GeneralNames_get_DNSNames_m3812 ();
extern "C" void GeneralNames_get_IPAddresses_m3813 ();
extern "C" void GeneralNames_ToString_m3814 ();
extern "C" void KeyUsageExtension__ctor_m3815 ();
extern "C" void KeyUsageExtension_Decode_m3816 ();
extern "C" void KeyUsageExtension_Encode_m3817 ();
extern "C" void KeyUsageExtension_Support_m3818 ();
extern "C" void KeyUsageExtension_ToString_m3819 ();
extern "C" void NetscapeCertTypeExtension__ctor_m3820 ();
extern "C" void NetscapeCertTypeExtension_Decode_m3821 ();
extern "C" void NetscapeCertTypeExtension_Support_m3822 ();
extern "C" void NetscapeCertTypeExtension_ToString_m3823 ();
extern "C" void SubjectAltNameExtension__ctor_m3824 ();
extern "C" void SubjectAltNameExtension_Decode_m3825 ();
extern "C" void SubjectAltNameExtension_get_DNSNames_m3826 ();
extern "C" void SubjectAltNameExtension_get_IPAddresses_m3827 ();
extern "C" void SubjectAltNameExtension_ToString_m3828 ();
extern "C" void HMAC__ctor_m3829 ();
extern "C" void HMAC_get_Key_m3830 ();
extern "C" void HMAC_set_Key_m3831 ();
extern "C" void HMAC_Initialize_m3832 ();
extern "C" void HMAC_HashFinal_m3833 ();
extern "C" void HMAC_HashCore_m3834 ();
extern "C" void HMAC_initializePad_m3835 ();
extern "C" void MD5SHA1__ctor_m3836 ();
extern "C" void MD5SHA1_Initialize_m3837 ();
extern "C" void MD5SHA1_HashFinal_m3838 ();
extern "C" void MD5SHA1_HashCore_m3839 ();
extern "C" void MD5SHA1_CreateSignature_m3840 ();
extern "C" void MD5SHA1_VerifySignature_m3841 ();
extern "C" void Alert__ctor_m3842 ();
extern "C" void Alert__ctor_m3843 ();
extern "C" void Alert_get_Level_m3844 ();
extern "C" void Alert_get_Description_m3845 ();
extern "C" void Alert_get_IsWarning_m3846 ();
extern "C" void Alert_get_IsCloseNotify_m3847 ();
extern "C" void Alert_inferAlertLevel_m3848 ();
extern "C" void Alert_GetAlertMessage_m3849 ();
extern "C" void CipherSuite__ctor_m3850 ();
extern "C" void CipherSuite__cctor_m3851 ();
extern "C" void CipherSuite_get_EncryptionCipher_m3852 ();
extern "C" void CipherSuite_get_DecryptionCipher_m3853 ();
extern "C" void CipherSuite_get_ClientHMAC_m3854 ();
extern "C" void CipherSuite_get_ServerHMAC_m3855 ();
extern "C" void CipherSuite_get_CipherAlgorithmType_m3856 ();
extern "C" void CipherSuite_get_HashAlgorithmName_m3857 ();
extern "C" void CipherSuite_get_HashAlgorithmType_m3858 ();
extern "C" void CipherSuite_get_HashSize_m3859 ();
extern "C" void CipherSuite_get_ExchangeAlgorithmType_m3860 ();
extern "C" void CipherSuite_get_CipherMode_m3861 ();
extern "C" void CipherSuite_get_Code_m3862 ();
extern "C" void CipherSuite_get_Name_m3863 ();
extern "C" void CipherSuite_get_IsExportable_m3864 ();
extern "C" void CipherSuite_get_KeyMaterialSize_m3865 ();
extern "C" void CipherSuite_get_KeyBlockSize_m3866 ();
extern "C" void CipherSuite_get_ExpandedKeyMaterialSize_m3867 ();
extern "C" void CipherSuite_get_EffectiveKeyBits_m3868 ();
extern "C" void CipherSuite_get_IvSize_m3869 ();
extern "C" void CipherSuite_get_Context_m3870 ();
extern "C" void CipherSuite_set_Context_m3871 ();
extern "C" void CipherSuite_Write_m3872 ();
extern "C" void CipherSuite_Write_m3873 ();
extern "C" void CipherSuite_InitializeCipher_m3874 ();
extern "C" void CipherSuite_EncryptRecord_m3875 ();
extern "C" void CipherSuite_DecryptRecord_m3876 ();
extern "C" void CipherSuite_CreatePremasterSecret_m3877 ();
extern "C" void CipherSuite_PRF_m3878 ();
extern "C" void CipherSuite_Expand_m3879 ();
extern "C" void CipherSuite_createEncryptionCipher_m3880 ();
extern "C" void CipherSuite_createDecryptionCipher_m3881 ();
extern "C" void CipherSuiteCollection__ctor_m3882 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_get_Item_m3883 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_set_Item_m3884 ();
extern "C" void CipherSuiteCollection_System_Collections_ICollection_get_IsSynchronized_m3885 ();
extern "C" void CipherSuiteCollection_System_Collections_ICollection_get_SyncRoot_m3886 ();
extern "C" void CipherSuiteCollection_System_Collections_IEnumerable_GetEnumerator_m3887 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Contains_m3888 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_IndexOf_m3889 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Insert_m3890 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Remove_m3891 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_RemoveAt_m3892 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Add_m3893 ();
extern "C" void CipherSuiteCollection_get_Item_m3894 ();
extern "C" void CipherSuiteCollection_get_Item_m3895 ();
extern "C" void CipherSuiteCollection_set_Item_m3896 ();
extern "C" void CipherSuiteCollection_get_Item_m3897 ();
extern "C" void CipherSuiteCollection_get_Count_m3898 ();
extern "C" void CipherSuiteCollection_get_IsFixedSize_m3899 ();
extern "C" void CipherSuiteCollection_get_IsReadOnly_m3900 ();
extern "C" void CipherSuiteCollection_CopyTo_m3901 ();
extern "C" void CipherSuiteCollection_Clear_m3902 ();
extern "C" void CipherSuiteCollection_IndexOf_m3903 ();
extern "C" void CipherSuiteCollection_IndexOf_m3904 ();
extern "C" void CipherSuiteCollection_Add_m3905 ();
extern "C" void CipherSuiteCollection_add_m3906 ();
extern "C" void CipherSuiteCollection_add_m3907 ();
extern "C" void CipherSuiteCollection_cultureAwareCompare_m3908 ();
extern "C" void CipherSuiteFactory_GetSupportedCiphers_m3909 ();
extern "C" void CipherSuiteFactory_GetTls1SupportedCiphers_m3910 ();
extern "C" void CipherSuiteFactory_GetSsl3SupportedCiphers_m3911 ();
extern "C" void ClientContext__ctor_m3912 ();
extern "C" void ClientContext_get_SslStream_m3913 ();
extern "C" void ClientContext_get_ClientHelloProtocol_m3914 ();
extern "C" void ClientContext_set_ClientHelloProtocol_m3915 ();
extern "C" void ClientContext_Clear_m3916 ();
extern "C" void ClientRecordProtocol__ctor_m3917 ();
extern "C" void ClientRecordProtocol_GetMessage_m3918 ();
extern "C" void ClientRecordProtocol_ProcessHandshakeMessage_m3919 ();
extern "C" void ClientRecordProtocol_createClientHandshakeMessage_m3920 ();
extern "C" void ClientRecordProtocol_createServerHandshakeMessage_m3921 ();
extern "C" void ClientSessionInfo__ctor_m3922 ();
extern "C" void ClientSessionInfo__cctor_m3923 ();
extern "C" void ClientSessionInfo_Finalize_m3924 ();
extern "C" void ClientSessionInfo_get_HostName_m3925 ();
extern "C" void ClientSessionInfo_get_Id_m3926 ();
extern "C" void ClientSessionInfo_get_Valid_m3927 ();
extern "C" void ClientSessionInfo_GetContext_m3928 ();
extern "C" void ClientSessionInfo_SetContext_m3929 ();
extern "C" void ClientSessionInfo_KeepAlive_m3930 ();
extern "C" void ClientSessionInfo_Dispose_m3931 ();
extern "C" void ClientSessionInfo_Dispose_m3932 ();
extern "C" void ClientSessionInfo_CheckDisposed_m3933 ();
extern "C" void ClientSessionCache__cctor_m3934 ();
extern "C" void ClientSessionCache_Add_m3935 ();
extern "C" void ClientSessionCache_FromHost_m3936 ();
extern "C" void ClientSessionCache_FromContext_m3937 ();
extern "C" void ClientSessionCache_SetContextInCache_m3938 ();
extern "C" void ClientSessionCache_SetContextFromCache_m3939 ();
extern "C" void Context__ctor_m3940 ();
extern "C" void Context_get_AbbreviatedHandshake_m3941 ();
extern "C" void Context_set_AbbreviatedHandshake_m3942 ();
extern "C" void Context_get_ProtocolNegotiated_m3943 ();
extern "C" void Context_set_ProtocolNegotiated_m3944 ();
extern "C" void Context_get_SecurityProtocol_m3945 ();
extern "C" void Context_set_SecurityProtocol_m3946 ();
extern "C" void Context_get_SecurityProtocolFlags_m3947 ();
extern "C" void Context_get_Protocol_m3948 ();
extern "C" void Context_get_SessionId_m3949 ();
extern "C" void Context_set_SessionId_m3950 ();
extern "C" void Context_get_CompressionMethod_m3951 ();
extern "C" void Context_set_CompressionMethod_m3952 ();
extern "C" void Context_get_ServerSettings_m3953 ();
extern "C" void Context_get_ClientSettings_m3954 ();
extern "C" void Context_get_LastHandshakeMsg_m3955 ();
extern "C" void Context_set_LastHandshakeMsg_m3956 ();
extern "C" void Context_get_HandshakeState_m3957 ();
extern "C" void Context_set_HandshakeState_m3958 ();
extern "C" void Context_get_ReceivedConnectionEnd_m3959 ();
extern "C" void Context_set_ReceivedConnectionEnd_m3960 ();
extern "C" void Context_get_SentConnectionEnd_m3961 ();
extern "C" void Context_set_SentConnectionEnd_m3962 ();
extern "C" void Context_get_SupportedCiphers_m3963 ();
extern "C" void Context_set_SupportedCiphers_m3964 ();
extern "C" void Context_get_HandshakeMessages_m3965 ();
extern "C" void Context_get_WriteSequenceNumber_m3966 ();
extern "C" void Context_set_WriteSequenceNumber_m3967 ();
extern "C" void Context_get_ReadSequenceNumber_m3968 ();
extern "C" void Context_set_ReadSequenceNumber_m3969 ();
extern "C" void Context_get_ClientRandom_m3970 ();
extern "C" void Context_set_ClientRandom_m3971 ();
extern "C" void Context_get_ServerRandom_m3972 ();
extern "C" void Context_set_ServerRandom_m3973 ();
extern "C" void Context_get_RandomCS_m3974 ();
extern "C" void Context_set_RandomCS_m3975 ();
extern "C" void Context_get_RandomSC_m3976 ();
extern "C" void Context_set_RandomSC_m3977 ();
extern "C" void Context_get_MasterSecret_m3978 ();
extern "C" void Context_set_MasterSecret_m3979 ();
extern "C" void Context_get_ClientWriteKey_m3980 ();
extern "C" void Context_set_ClientWriteKey_m3981 ();
extern "C" void Context_get_ServerWriteKey_m3982 ();
extern "C" void Context_set_ServerWriteKey_m3983 ();
extern "C" void Context_get_ClientWriteIV_m3984 ();
extern "C" void Context_set_ClientWriteIV_m3985 ();
extern "C" void Context_get_ServerWriteIV_m3986 ();
extern "C" void Context_set_ServerWriteIV_m3987 ();
extern "C" void Context_get_RecordProtocol_m3988 ();
extern "C" void Context_set_RecordProtocol_m3989 ();
extern "C" void Context_GetUnixTime_m3990 ();
extern "C" void Context_GetSecureRandomBytes_m3991 ();
extern "C" void Context_Clear_m3992 ();
extern "C" void Context_ClearKeyInfo_m3993 ();
extern "C" void Context_DecodeProtocolCode_m3994 ();
extern "C" void Context_ChangeProtocol_m3995 ();
extern "C" void Context_get_Current_m3996 ();
extern "C" void Context_get_Negotiating_m3997 ();
extern "C" void Context_get_Read_m3998 ();
extern "C" void Context_get_Write_m3999 ();
extern "C" void Context_StartSwitchingSecurityParameters_m4000 ();
extern "C" void Context_EndSwitchingSecurityParameters_m4001 ();
extern "C" void HttpsClientStream__ctor_m4002 ();
extern "C" void HttpsClientStream_get_TrustFailure_m4003 ();
extern "C" void HttpsClientStream_RaiseServerCertificateValidation_m4004 ();
extern "C" void HttpsClientStream_U3CHttpsClientStreamU3Em__0_m4005 ();
extern "C" void HttpsClientStream_U3CHttpsClientStreamU3Em__1_m4006 ();
extern "C" void ReceiveRecordAsyncResult__ctor_m4007 ();
extern "C" void ReceiveRecordAsyncResult_get_Record_m4008 ();
extern "C" void ReceiveRecordAsyncResult_get_ResultingBuffer_m4009 ();
extern "C" void ReceiveRecordAsyncResult_get_InitialBuffer_m4010 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncState_m4011 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncException_m4012 ();
extern "C" void ReceiveRecordAsyncResult_get_CompletedWithError_m4013 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncWaitHandle_m4014 ();
extern "C" void ReceiveRecordAsyncResult_get_IsCompleted_m4015 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m4016 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m4017 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m4018 ();
extern "C" void SendRecordAsyncResult__ctor_m4019 ();
extern "C" void SendRecordAsyncResult_get_Message_m4020 ();
extern "C" void SendRecordAsyncResult_get_AsyncState_m4021 ();
extern "C" void SendRecordAsyncResult_get_AsyncException_m4022 ();
extern "C" void SendRecordAsyncResult_get_CompletedWithError_m4023 ();
extern "C" void SendRecordAsyncResult_get_AsyncWaitHandle_m4024 ();
extern "C" void SendRecordAsyncResult_get_IsCompleted_m4025 ();
extern "C" void SendRecordAsyncResult_SetComplete_m4026 ();
extern "C" void SendRecordAsyncResult_SetComplete_m4027 ();
extern "C" void RecordProtocol__ctor_m4028 ();
extern "C" void RecordProtocol__cctor_m4029 ();
extern "C" void RecordProtocol_get_Context_m4030 ();
extern "C" void RecordProtocol_SendRecord_m4031 ();
extern "C" void RecordProtocol_ProcessChangeCipherSpec_m4032 ();
extern "C" void RecordProtocol_GetMessage_m4033 ();
extern "C" void RecordProtocol_BeginReceiveRecord_m4034 ();
extern "C" void RecordProtocol_InternalReceiveRecordCallback_m4035 ();
extern "C" void RecordProtocol_EndReceiveRecord_m4036 ();
extern "C" void RecordProtocol_ReceiveRecord_m4037 ();
extern "C" void RecordProtocol_ReadRecordBuffer_m4038 ();
extern "C" void RecordProtocol_ReadClientHelloV2_m4039 ();
extern "C" void RecordProtocol_ReadStandardRecordBuffer_m4040 ();
extern "C" void RecordProtocol_ProcessAlert_m4041 ();
extern "C" void RecordProtocol_SendAlert_m4042 ();
extern "C" void RecordProtocol_SendAlert_m4043 ();
extern "C" void RecordProtocol_SendAlert_m4044 ();
extern "C" void RecordProtocol_SendChangeCipherSpec_m4045 ();
extern "C" void RecordProtocol_BeginSendRecord_m4046 ();
extern "C" void RecordProtocol_InternalSendRecordCallback_m4047 ();
extern "C" void RecordProtocol_BeginSendRecord_m4048 ();
extern "C" void RecordProtocol_EndSendRecord_m4049 ();
extern "C" void RecordProtocol_SendRecord_m4050 ();
extern "C" void RecordProtocol_EncodeRecord_m4051 ();
extern "C" void RecordProtocol_EncodeRecord_m4052 ();
extern "C" void RecordProtocol_encryptRecordFragment_m4053 ();
extern "C" void RecordProtocol_decryptRecordFragment_m4054 ();
extern "C" void RecordProtocol_Compare_m4055 ();
extern "C" void RecordProtocol_ProcessCipherSpecV2Buffer_m4056 ();
extern "C" void RecordProtocol_MapV2CipherCode_m4057 ();
extern "C" void RSASslSignatureDeformatter__ctor_m4058 ();
extern "C" void RSASslSignatureDeformatter_VerifySignature_m4059 ();
extern "C" void RSASslSignatureDeformatter_SetHashAlgorithm_m4060 ();
extern "C" void RSASslSignatureDeformatter_SetKey_m4061 ();
extern "C" void RSASslSignatureFormatter__ctor_m4062 ();
extern "C" void RSASslSignatureFormatter_CreateSignature_m4063 ();
extern "C" void RSASslSignatureFormatter_SetHashAlgorithm_m4064 ();
extern "C" void RSASslSignatureFormatter_SetKey_m4065 ();
extern "C" void SecurityParameters__ctor_m4066 ();
extern "C" void SecurityParameters_get_Cipher_m4067 ();
extern "C" void SecurityParameters_set_Cipher_m4068 ();
extern "C" void SecurityParameters_get_ClientWriteMAC_m4069 ();
extern "C" void SecurityParameters_set_ClientWriteMAC_m4070 ();
extern "C" void SecurityParameters_get_ServerWriteMAC_m4071 ();
extern "C" void SecurityParameters_set_ServerWriteMAC_m4072 ();
extern "C" void SecurityParameters_Clear_m4073 ();
extern "C" void ValidationResult_get_Trusted_m4074 ();
extern "C" void ValidationResult_get_ErrorCode_m4075 ();
extern "C" void SslClientStream__ctor_m4076 ();
extern "C" void SslClientStream__ctor_m4077 ();
extern "C" void SslClientStream__ctor_m4078 ();
extern "C" void SslClientStream__ctor_m4079 ();
extern "C" void SslClientStream__ctor_m4080 ();
extern "C" void SslClientStream_add_ServerCertValidation_m4081 ();
extern "C" void SslClientStream_remove_ServerCertValidation_m4082 ();
extern "C" void SslClientStream_add_ClientCertSelection_m4083 ();
extern "C" void SslClientStream_remove_ClientCertSelection_m4084 ();
extern "C" void SslClientStream_add_PrivateKeySelection_m4085 ();
extern "C" void SslClientStream_remove_PrivateKeySelection_m4086 ();
extern "C" void SslClientStream_add_ServerCertValidation2_m4087 ();
extern "C" void SslClientStream_remove_ServerCertValidation2_m4088 ();
extern "C" void SslClientStream_get_InputBuffer_m4089 ();
extern "C" void SslClientStream_get_ClientCertificates_m4090 ();
extern "C" void SslClientStream_get_SelectedClientCertificate_m4091 ();
extern "C" void SslClientStream_get_ServerCertValidationDelegate_m4092 ();
extern "C" void SslClientStream_set_ServerCertValidationDelegate_m4093 ();
extern "C" void SslClientStream_get_ClientCertSelectionDelegate_m4094 ();
extern "C" void SslClientStream_set_ClientCertSelectionDelegate_m4095 ();
extern "C" void SslClientStream_get_PrivateKeyCertSelectionDelegate_m4096 ();
extern "C" void SslClientStream_set_PrivateKeyCertSelectionDelegate_m4097 ();
extern "C" void SslClientStream_Finalize_m4098 ();
extern "C" void SslClientStream_Dispose_m4099 ();
extern "C" void SslClientStream_OnBeginNegotiateHandshake_m4100 ();
extern "C" void SslClientStream_SafeReceiveRecord_m4101 ();
extern "C" void SslClientStream_OnNegotiateHandshakeCallback_m4102 ();
extern "C" void SslClientStream_OnLocalCertificateSelection_m4103 ();
extern "C" void SslClientStream_get_HaveRemoteValidation2Callback_m4104 ();
extern "C" void SslClientStream_OnRemoteCertificateValidation2_m4105 ();
extern "C" void SslClientStream_OnRemoteCertificateValidation_m4106 ();
extern "C" void SslClientStream_RaiseServerCertificateValidation_m4107 ();
extern "C" void SslClientStream_RaiseServerCertificateValidation2_m4108 ();
extern "C" void SslClientStream_RaiseClientCertificateSelection_m4109 ();
extern "C" void SslClientStream_OnLocalPrivateKeySelection_m4110 ();
extern "C" void SslClientStream_RaisePrivateKeySelection_m4111 ();
extern "C" void SslCipherSuite__ctor_m4112 ();
extern "C" void SslCipherSuite_ComputeServerRecordMAC_m4113 ();
extern "C" void SslCipherSuite_ComputeClientRecordMAC_m4114 ();
extern "C" void SslCipherSuite_ComputeMasterSecret_m4115 ();
extern "C" void SslCipherSuite_ComputeKeys_m4116 ();
extern "C" void SslCipherSuite_prf_m4117 ();
extern "C" void SslHandshakeHash__ctor_m4118 ();
extern "C" void SslHandshakeHash_Initialize_m4119 ();
extern "C" void SslHandshakeHash_HashFinal_m4120 ();
extern "C" void SslHandshakeHash_HashCore_m4121 ();
extern "C" void SslHandshakeHash_CreateSignature_m4122 ();
extern "C" void SslHandshakeHash_initializePad_m4123 ();
extern "C" void InternalAsyncResult__ctor_m4124 ();
extern "C" void InternalAsyncResult_get_ProceedAfterHandshake_m4125 ();
extern "C" void InternalAsyncResult_get_FromWrite_m4126 ();
extern "C" void InternalAsyncResult_get_Buffer_m4127 ();
extern "C" void InternalAsyncResult_get_Offset_m4128 ();
extern "C" void InternalAsyncResult_get_Count_m4129 ();
extern "C" void InternalAsyncResult_get_BytesRead_m4130 ();
extern "C" void InternalAsyncResult_get_AsyncState_m4131 ();
extern "C" void InternalAsyncResult_get_AsyncException_m4132 ();
extern "C" void InternalAsyncResult_get_CompletedWithError_m4133 ();
extern "C" void InternalAsyncResult_get_AsyncWaitHandle_m4134 ();
extern "C" void InternalAsyncResult_get_IsCompleted_m4135 ();
extern "C" void InternalAsyncResult_SetComplete_m4136 ();
extern "C" void InternalAsyncResult_SetComplete_m4137 ();
extern "C" void InternalAsyncResult_SetComplete_m4138 ();
extern "C" void InternalAsyncResult_SetComplete_m4139 ();
extern "C" void SslStreamBase__ctor_m4140 ();
extern "C" void SslStreamBase__cctor_m4141 ();
extern "C" void SslStreamBase_AsyncHandshakeCallback_m4142 ();
extern "C" void SslStreamBase_get_MightNeedHandshake_m4143 ();
extern "C" void SslStreamBase_NegotiateHandshake_m4144 ();
extern "C" void SslStreamBase_RaiseLocalCertificateSelection_m4145 ();
extern "C" void SslStreamBase_RaiseRemoteCertificateValidation_m4146 ();
extern "C" void SslStreamBase_RaiseRemoteCertificateValidation2_m4147 ();
extern "C" void SslStreamBase_RaiseLocalPrivateKeySelection_m4148 ();
extern "C" void SslStreamBase_get_CheckCertRevocationStatus_m4149 ();
extern "C" void SslStreamBase_set_CheckCertRevocationStatus_m4150 ();
extern "C" void SslStreamBase_get_CipherAlgorithm_m4151 ();
extern "C" void SslStreamBase_get_CipherStrength_m4152 ();
extern "C" void SslStreamBase_get_HashAlgorithm_m4153 ();
extern "C" void SslStreamBase_get_HashStrength_m4154 ();
extern "C" void SslStreamBase_get_KeyExchangeStrength_m4155 ();
extern "C" void SslStreamBase_get_KeyExchangeAlgorithm_m4156 ();
extern "C" void SslStreamBase_get_SecurityProtocol_m4157 ();
extern "C" void SslStreamBase_get_ServerCertificate_m4158 ();
extern "C" void SslStreamBase_get_ServerCertificates_m4159 ();
extern "C" void SslStreamBase_BeginNegotiateHandshake_m4160 ();
extern "C" void SslStreamBase_EndNegotiateHandshake_m4161 ();
extern "C" void SslStreamBase_BeginRead_m4162 ();
extern "C" void SslStreamBase_InternalBeginRead_m4163 ();
extern "C" void SslStreamBase_InternalReadCallback_m4164 ();
extern "C" void SslStreamBase_InternalBeginWrite_m4165 ();
extern "C" void SslStreamBase_InternalWriteCallback_m4166 ();
extern "C" void SslStreamBase_BeginWrite_m4167 ();
extern "C" void SslStreamBase_EndRead_m4168 ();
extern "C" void SslStreamBase_EndWrite_m4169 ();
extern "C" void SslStreamBase_Close_m4170 ();
extern "C" void SslStreamBase_Flush_m4171 ();
extern "C" void SslStreamBase_Read_m4172 ();
extern "C" void SslStreamBase_Read_m4173 ();
extern "C" void SslStreamBase_Seek_m4174 ();
extern "C" void SslStreamBase_SetLength_m4175 ();
extern "C" void SslStreamBase_Write_m4176 ();
extern "C" void SslStreamBase_Write_m4177 ();
extern "C" void SslStreamBase_get_CanRead_m4178 ();
extern "C" void SslStreamBase_get_CanSeek_m4179 ();
extern "C" void SslStreamBase_get_CanWrite_m4180 ();
extern "C" void SslStreamBase_get_Length_m4181 ();
extern "C" void SslStreamBase_get_Position_m4182 ();
extern "C" void SslStreamBase_set_Position_m4183 ();
extern "C" void SslStreamBase_Finalize_m4184 ();
extern "C" void SslStreamBase_Dispose_m4185 ();
extern "C" void SslStreamBase_resetBuffer_m4186 ();
extern "C" void SslStreamBase_checkDisposed_m4187 ();
extern "C" void TlsCipherSuite__ctor_m4188 ();
extern "C" void TlsCipherSuite_ComputeServerRecordMAC_m4189 ();
extern "C" void TlsCipherSuite_ComputeClientRecordMAC_m4190 ();
extern "C" void TlsCipherSuite_ComputeMasterSecret_m4191 ();
extern "C" void TlsCipherSuite_ComputeKeys_m4192 ();
extern "C" void TlsClientSettings__ctor_m4193 ();
extern "C" void TlsClientSettings_get_TargetHost_m4194 ();
extern "C" void TlsClientSettings_set_TargetHost_m4195 ();
extern "C" void TlsClientSettings_get_Certificates_m4196 ();
extern "C" void TlsClientSettings_set_Certificates_m4197 ();
extern "C" void TlsClientSettings_get_ClientCertificate_m4198 ();
extern "C" void TlsClientSettings_set_ClientCertificate_m4199 ();
extern "C" void TlsClientSettings_UpdateCertificateRSA_m4200 ();
extern "C" void TlsException__ctor_m4201 ();
extern "C" void TlsException__ctor_m4202 ();
extern "C" void TlsException__ctor_m4203 ();
extern "C" void TlsException__ctor_m4204 ();
extern "C" void TlsException__ctor_m4205 ();
extern "C" void TlsException__ctor_m4206 ();
extern "C" void TlsException_get_Alert_m4207 ();
extern "C" void TlsServerSettings__ctor_m4208 ();
extern "C" void TlsServerSettings_get_ServerKeyExchange_m4209 ();
extern "C" void TlsServerSettings_set_ServerKeyExchange_m4210 ();
extern "C" void TlsServerSettings_get_Certificates_m4211 ();
extern "C" void TlsServerSettings_set_Certificates_m4212 ();
extern "C" void TlsServerSettings_get_CertificateRSA_m4213 ();
extern "C" void TlsServerSettings_get_RsaParameters_m4214 ();
extern "C" void TlsServerSettings_set_RsaParameters_m4215 ();
extern "C" void TlsServerSettings_set_SignedParams_m4216 ();
extern "C" void TlsServerSettings_get_CertificateRequest_m4217 ();
extern "C" void TlsServerSettings_set_CertificateRequest_m4218 ();
extern "C" void TlsServerSettings_set_CertificateTypes_m4219 ();
extern "C" void TlsServerSettings_set_DistinguisedNames_m4220 ();
extern "C" void TlsServerSettings_UpdateCertificateRSA_m4221 ();
extern "C" void TlsStream__ctor_m4222 ();
extern "C" void TlsStream__ctor_m4223 ();
extern "C" void TlsStream_get_EOF_m4224 ();
extern "C" void TlsStream_get_CanWrite_m4225 ();
extern "C" void TlsStream_get_CanRead_m4226 ();
extern "C" void TlsStream_get_CanSeek_m4227 ();
extern "C" void TlsStream_get_Position_m4228 ();
extern "C" void TlsStream_set_Position_m4229 ();
extern "C" void TlsStream_get_Length_m4230 ();
extern "C" void TlsStream_ReadSmallValue_m4231 ();
extern "C" void TlsStream_ReadByte_m4232 ();
extern "C" void TlsStream_ReadInt16_m4233 ();
extern "C" void TlsStream_ReadInt24_m4234 ();
extern "C" void TlsStream_ReadBytes_m4235 ();
extern "C" void TlsStream_Write_m4236 ();
extern "C" void TlsStream_Write_m4237 ();
extern "C" void TlsStream_WriteInt24_m4238 ();
extern "C" void TlsStream_Write_m4239 ();
extern "C" void TlsStream_Write_m4240 ();
extern "C" void TlsStream_Reset_m4241 ();
extern "C" void TlsStream_ToArray_m4242 ();
extern "C" void TlsStream_Flush_m4243 ();
extern "C" void TlsStream_SetLength_m4244 ();
extern "C" void TlsStream_Seek_m4245 ();
extern "C" void TlsStream_Read_m4246 ();
extern "C" void TlsStream_Write_m4247 ();
extern "C" void HandshakeMessage__ctor_m4248 ();
extern "C" void HandshakeMessage__ctor_m4249 ();
extern "C" void HandshakeMessage__ctor_m4250 ();
extern "C" void HandshakeMessage_get_Context_m4251 ();
extern "C" void HandshakeMessage_get_HandshakeType_m4252 ();
extern "C" void HandshakeMessage_get_ContentType_m4253 ();
extern "C" void HandshakeMessage_Process_m4254 ();
extern "C" void HandshakeMessage_Update_m4255 ();
extern "C" void HandshakeMessage_EncodeMessage_m4256 ();
extern "C" void HandshakeMessage_Compare_m4257 ();
extern "C" void TlsClientCertificate__ctor_m4258 ();
extern "C" void TlsClientCertificate_get_ClientCertificate_m4259 ();
extern "C" void TlsClientCertificate_Update_m4260 ();
extern "C" void TlsClientCertificate_GetClientCertificate_m4261 ();
extern "C" void TlsClientCertificate_SendCertificates_m4262 ();
extern "C" void TlsClientCertificate_ProcessAsSsl3_m4263 ();
extern "C" void TlsClientCertificate_ProcessAsTls1_m4264 ();
extern "C" void TlsClientCertificate_FindParentCertificate_m4265 ();
extern "C" void TlsClientCertificateVerify__ctor_m4266 ();
extern "C" void TlsClientCertificateVerify_Update_m4267 ();
extern "C" void TlsClientCertificateVerify_ProcessAsSsl3_m4268 ();
extern "C" void TlsClientCertificateVerify_ProcessAsTls1_m4269 ();
extern "C" void TlsClientCertificateVerify_getClientCertRSA_m4270 ();
extern "C" void TlsClientCertificateVerify_getUnsignedBigInteger_m4271 ();
extern "C" void TlsClientFinished__ctor_m4272 ();
extern "C" void TlsClientFinished__cctor_m4273 ();
extern "C" void TlsClientFinished_Update_m4274 ();
extern "C" void TlsClientFinished_ProcessAsSsl3_m4275 ();
extern "C" void TlsClientFinished_ProcessAsTls1_m4276 ();
extern "C" void TlsClientHello__ctor_m4277 ();
extern "C" void TlsClientHello_Update_m4278 ();
extern "C" void TlsClientHello_ProcessAsSsl3_m4279 ();
extern "C" void TlsClientHello_ProcessAsTls1_m4280 ();
extern "C" void TlsClientKeyExchange__ctor_m4281 ();
extern "C" void TlsClientKeyExchange_ProcessAsSsl3_m4282 ();
extern "C" void TlsClientKeyExchange_ProcessAsTls1_m4283 ();
extern "C" void TlsClientKeyExchange_ProcessCommon_m4284 ();
extern "C" void TlsServerCertificate__ctor_m4285 ();
extern "C" void TlsServerCertificate_Update_m4286 ();
extern "C" void TlsServerCertificate_ProcessAsSsl3_m4287 ();
extern "C" void TlsServerCertificate_ProcessAsTls1_m4288 ();
extern "C" void TlsServerCertificate_checkCertificateUsage_m4289 ();
extern "C" void TlsServerCertificate_validateCertificates_m4290 ();
extern "C" void TlsServerCertificate_checkServerIdentity_m4291 ();
extern "C" void TlsServerCertificate_checkDomainName_m4292 ();
extern "C" void TlsServerCertificate_Match_m4293 ();
extern "C" void TlsServerCertificateRequest__ctor_m4294 ();
extern "C" void TlsServerCertificateRequest_Update_m4295 ();
extern "C" void TlsServerCertificateRequest_ProcessAsSsl3_m4296 ();
extern "C" void TlsServerCertificateRequest_ProcessAsTls1_m4297 ();
extern "C" void TlsServerFinished__ctor_m4298 ();
extern "C" void TlsServerFinished__cctor_m4299 ();
extern "C" void TlsServerFinished_Update_m4300 ();
extern "C" void TlsServerFinished_ProcessAsSsl3_m4301 ();
extern "C" void TlsServerFinished_ProcessAsTls1_m4302 ();
extern "C" void TlsServerHello__ctor_m4303 ();
extern "C" void TlsServerHello_Update_m4304 ();
extern "C" void TlsServerHello_ProcessAsSsl3_m4305 ();
extern "C" void TlsServerHello_ProcessAsTls1_m4306 ();
extern "C" void TlsServerHello_processProtocol_m4307 ();
extern "C" void TlsServerHelloDone__ctor_m4308 ();
extern "C" void TlsServerHelloDone_ProcessAsSsl3_m4309 ();
extern "C" void TlsServerHelloDone_ProcessAsTls1_m4310 ();
extern "C" void TlsServerKeyExchange__ctor_m4311 ();
extern "C" void TlsServerKeyExchange_Update_m4312 ();
extern "C" void TlsServerKeyExchange_ProcessAsSsl3_m4313 ();
extern "C" void TlsServerKeyExchange_ProcessAsTls1_m4314 ();
extern "C" void TlsServerKeyExchange_verifySignature_m4315 ();
extern "C" void PrimalityTest__ctor_m4316 ();
extern "C" void PrimalityTest_Invoke_m4317 ();
extern "C" void PrimalityTest_BeginInvoke_m4318 ();
extern "C" void PrimalityTest_EndInvoke_m4319 ();
extern "C" void CertificateValidationCallback__ctor_m4320 ();
extern "C" void CertificateValidationCallback_Invoke_m4321 ();
extern "C" void CertificateValidationCallback_BeginInvoke_m4322 ();
extern "C" void CertificateValidationCallback_EndInvoke_m4323 ();
extern "C" void CertificateValidationCallback2__ctor_m4324 ();
extern "C" void CertificateValidationCallback2_Invoke_m4325 ();
extern "C" void CertificateValidationCallback2_BeginInvoke_m4326 ();
extern "C" void CertificateValidationCallback2_EndInvoke_m4327 ();
extern "C" void CertificateSelectionCallback__ctor_m4328 ();
extern "C" void CertificateSelectionCallback_Invoke_m4329 ();
extern "C" void CertificateSelectionCallback_BeginInvoke_m4330 ();
extern "C" void CertificateSelectionCallback_EndInvoke_m4331 ();
extern "C" void PrivateKeySelectionCallback__ctor_m4332 ();
extern "C" void PrivateKeySelectionCallback_Invoke_m4333 ();
extern "C" void PrivateKeySelectionCallback_BeginInvoke_m4334 ();
extern "C" void PrivateKeySelectionCallback_EndInvoke_m4335 ();
extern "C" void Locale_GetText_m4461 ();
extern "C" void Locale_GetText_m4462 ();
extern "C" void MonoTODOAttribute__ctor_m4463 ();
extern "C" void MonoTODOAttribute__ctor_m4464 ();
extern "C" void GeneratedCodeAttribute__ctor_m4465 ();
extern "C" void HybridDictionary__ctor_m4466 ();
extern "C" void HybridDictionary__ctor_m4467 ();
extern "C" void HybridDictionary_System_Collections_IEnumerable_GetEnumerator_m4468 ();
extern "C" void HybridDictionary_get_inner_m4469 ();
extern "C" void HybridDictionary_get_Count_m4470 ();
extern "C" void HybridDictionary_get_IsSynchronized_m4471 ();
extern "C" void HybridDictionary_get_Item_m4472 ();
extern "C" void HybridDictionary_set_Item_m4473 ();
extern "C" void HybridDictionary_get_SyncRoot_m4474 ();
extern "C" void HybridDictionary_Add_m4475 ();
extern "C" void HybridDictionary_Contains_m4476 ();
extern "C" void HybridDictionary_CopyTo_m4477 ();
extern "C" void HybridDictionary_GetEnumerator_m4478 ();
extern "C" void HybridDictionary_Remove_m4479 ();
extern "C" void HybridDictionary_Switch_m4480 ();
extern "C" void DictionaryNode__ctor_m4481 ();
extern "C" void DictionaryNodeEnumerator__ctor_m4482 ();
extern "C" void DictionaryNodeEnumerator_FailFast_m4483 ();
extern "C" void DictionaryNodeEnumerator_MoveNext_m4484 ();
extern "C" void DictionaryNodeEnumerator_Reset_m4485 ();
extern "C" void DictionaryNodeEnumerator_get_Current_m4486 ();
extern "C" void DictionaryNodeEnumerator_get_DictionaryNode_m4487 ();
extern "C" void DictionaryNodeEnumerator_get_Entry_m4488 ();
extern "C" void DictionaryNodeEnumerator_get_Key_m4489 ();
extern "C" void DictionaryNodeEnumerator_get_Value_m4490 ();
extern "C" void ListDictionary__ctor_m4491 ();
extern "C" void ListDictionary__ctor_m4492 ();
extern "C" void ListDictionary_System_Collections_IEnumerable_GetEnumerator_m4493 ();
extern "C" void ListDictionary_FindEntry_m4494 ();
extern "C" void ListDictionary_FindEntry_m4495 ();
extern "C" void ListDictionary_AddImpl_m4496 ();
extern "C" void ListDictionary_get_Count_m4497 ();
extern "C" void ListDictionary_get_IsSynchronized_m4498 ();
extern "C" void ListDictionary_get_SyncRoot_m4499 ();
extern "C" void ListDictionary_CopyTo_m4500 ();
extern "C" void ListDictionary_get_Item_m4501 ();
extern "C" void ListDictionary_set_Item_m4502 ();
extern "C" void ListDictionary_Add_m4503 ();
extern "C" void ListDictionary_Clear_m4504 ();
extern "C" void ListDictionary_Contains_m4505 ();
extern "C" void ListDictionary_GetEnumerator_m4506 ();
extern "C" void ListDictionary_Remove_m4507 ();
extern "C" void _Item__ctor_m4508 ();
extern "C" void _KeysEnumerator__ctor_m4509 ();
extern "C" void _KeysEnumerator_get_Current_m4510 ();
extern "C" void _KeysEnumerator_MoveNext_m4511 ();
extern "C" void _KeysEnumerator_Reset_m4512 ();
extern "C" void KeysCollection__ctor_m4513 ();
extern "C" void KeysCollection_System_Collections_ICollection_CopyTo_m4514 ();
extern "C" void KeysCollection_System_Collections_ICollection_get_IsSynchronized_m4515 ();
extern "C" void KeysCollection_System_Collections_ICollection_get_SyncRoot_m4516 ();
extern "C" void KeysCollection_get_Count_m4517 ();
extern "C" void KeysCollection_GetEnumerator_m4518 ();
extern "C" void NameObjectCollectionBase__ctor_m4519 ();
extern "C" void NameObjectCollectionBase__ctor_m4520 ();
extern "C" void NameObjectCollectionBase_System_Collections_ICollection_get_IsSynchronized_m4521 ();
extern "C" void NameObjectCollectionBase_System_Collections_ICollection_get_SyncRoot_m4522 ();
extern "C" void NameObjectCollectionBase_System_Collections_ICollection_CopyTo_m4523 ();
extern "C" void NameObjectCollectionBase_Init_m4524 ();
extern "C" void NameObjectCollectionBase_get_Keys_m4525 ();
extern "C" void NameObjectCollectionBase_GetEnumerator_m4526 ();
extern "C" void NameObjectCollectionBase_GetObjectData_m4527 ();
extern "C" void NameObjectCollectionBase_get_Count_m4528 ();
extern "C" void NameObjectCollectionBase_OnDeserialization_m4529 ();
extern "C" void NameObjectCollectionBase_get_IsReadOnly_m4530 ();
extern "C" void NameObjectCollectionBase_BaseAdd_m4531 ();
extern "C" void NameObjectCollectionBase_BaseGet_m4532 ();
extern "C" void NameObjectCollectionBase_BaseGet_m4533 ();
extern "C" void NameObjectCollectionBase_BaseGetKey_m4534 ();
extern "C" void NameObjectCollectionBase_FindFirstMatchedItem_m4535 ();
extern "C" void NameValueCollection__ctor_m4536 ();
extern "C" void NameValueCollection__ctor_m4537 ();
extern "C" void NameValueCollection_Add_m4538 ();
extern "C" void NameValueCollection_Get_m4539 ();
extern "C" void NameValueCollection_AsSingleString_m4540 ();
extern "C" void NameValueCollection_GetKey_m4541 ();
extern "C" void NameValueCollection_InvalidateCachedArrays_m4542 ();
extern "C" void DefaultValueAttribute__ctor_m4543 ();
extern "C" void DefaultValueAttribute_get_Value_m4544 ();
extern "C" void DefaultValueAttribute_Equals_m4545 ();
extern "C" void DefaultValueAttribute_GetHashCode_m4546 ();
extern "C" void EditorBrowsableAttribute__ctor_m4547 ();
extern "C" void EditorBrowsableAttribute_get_State_m4548 ();
extern "C" void EditorBrowsableAttribute_Equals_m4549 ();
extern "C" void EditorBrowsableAttribute_GetHashCode_m4550 ();
extern "C" void TypeConverterAttribute__ctor_m4551 ();
extern "C" void TypeConverterAttribute__ctor_m4552 ();
extern "C" void TypeConverterAttribute__cctor_m4553 ();
extern "C" void TypeConverterAttribute_Equals_m4554 ();
extern "C" void TypeConverterAttribute_GetHashCode_m4555 ();
extern "C" void TypeConverterAttribute_get_ConverterTypeName_m4556 ();
extern "C" void DefaultCertificatePolicy__ctor_m4557 ();
extern "C" void DefaultCertificatePolicy_CheckValidationResult_m4558 ();
extern "C" void FileWebRequest__ctor_m4559 ();
extern "C" void FileWebRequest__ctor_m4560 ();
extern "C" void FileWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m4561 ();
extern "C" void FileWebRequest_GetObjectData_m4562 ();
extern "C" void FileWebRequestCreator__ctor_m4563 ();
extern "C" void FileWebRequestCreator_Create_m4564 ();
extern "C" void FtpRequestCreator__ctor_m4565 ();
extern "C" void FtpRequestCreator_Create_m4566 ();
extern "C" void FtpWebRequest__ctor_m4567 ();
extern "C" void FtpWebRequest__cctor_m4568 ();
extern "C" void FtpWebRequest_U3CcallbackU3Em__B_m4569 ();
extern "C" void GlobalProxySelection_get_Select_m4570 ();
extern "C" void HttpRequestCreator__ctor_m4571 ();
extern "C" void HttpRequestCreator_Create_m4572 ();
extern "C" void HttpVersion__cctor_m4573 ();
extern "C" void HttpWebRequest__ctor_m4574 ();
extern "C" void HttpWebRequest__ctor_m4575 ();
extern "C" void HttpWebRequest__cctor_m4576 ();
extern "C" void HttpWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m4577 ();
extern "C" void HttpWebRequest_get_Address_m4421 ();
extern "C" void HttpWebRequest_get_ServicePoint_m4425 ();
extern "C" void HttpWebRequest_GetServicePoint_m4578 ();
extern "C" void HttpWebRequest_GetObjectData_m4579 ();
extern "C" void IPAddress__ctor_m4580 ();
extern "C" void IPAddress__ctor_m4581 ();
extern "C" void IPAddress__cctor_m4582 ();
extern "C" void IPAddress_SwapShort_m4583 ();
extern "C" void IPAddress_HostToNetworkOrder_m4584 ();
extern "C" void IPAddress_NetworkToHostOrder_m4585 ();
extern "C" void IPAddress_Parse_m4586 ();
extern "C" void IPAddress_TryParse_m4587 ();
extern "C" void IPAddress_ParseIPV4_m4588 ();
extern "C" void IPAddress_ParseIPV6_m4589 ();
extern "C" void IPAddress_get_InternalIPv4Address_m4590 ();
extern "C" void IPAddress_get_ScopeId_m4591 ();
extern "C" void IPAddress_get_AddressFamily_m4592 ();
extern "C" void IPAddress_IsLoopback_m4593 ();
extern "C" void IPAddress_ToString_m4594 ();
extern "C" void IPAddress_ToString_m4595 ();
extern "C" void IPAddress_Equals_m4596 ();
extern "C" void IPAddress_GetHashCode_m4597 ();
extern "C" void IPAddress_Hash_m4598 ();
extern "C" void IPv6Address__ctor_m4599 ();
extern "C" void IPv6Address__ctor_m4600 ();
extern "C" void IPv6Address__ctor_m4601 ();
extern "C" void IPv6Address__cctor_m4602 ();
extern "C" void IPv6Address_Parse_m4603 ();
extern "C" void IPv6Address_Fill_m4604 ();
extern "C" void IPv6Address_TryParse_m4605 ();
extern "C" void IPv6Address_TryParse_m4606 ();
extern "C" void IPv6Address_get_Address_m4607 ();
extern "C" void IPv6Address_get_ScopeId_m4608 ();
extern "C" void IPv6Address_set_ScopeId_m4609 ();
extern "C" void IPv6Address_IsLoopback_m4610 ();
extern "C" void IPv6Address_SwapUShort_m4611 ();
extern "C" void IPv6Address_AsIPv4Int_m4612 ();
extern "C" void IPv6Address_IsIPv4Compatible_m4613 ();
extern "C" void IPv6Address_IsIPv4Mapped_m4614 ();
extern "C" void IPv6Address_ToString_m4615 ();
extern "C" void IPv6Address_ToString_m4616 ();
extern "C" void IPv6Address_Equals_m4617 ();
extern "C" void IPv6Address_GetHashCode_m4618 ();
extern "C" void IPv6Address_Hash_m4619 ();
extern "C" void ServicePoint__ctor_m4620 ();
extern "C" void ServicePoint_get_Address_m4621 ();
extern "C" void ServicePoint_get_CurrentConnections_m4622 ();
extern "C" void ServicePoint_get_IdleSince_m4623 ();
extern "C" void ServicePoint_set_IdleSince_m4624 ();
extern "C" void ServicePoint_set_Expect100Continue_m4625 ();
extern "C" void ServicePoint_set_UseNagleAlgorithm_m4626 ();
extern "C" void ServicePoint_set_SendContinue_m4627 ();
extern "C" void ServicePoint_set_UsesProxy_m4628 ();
extern "C" void ServicePoint_set_UseConnect_m4629 ();
extern "C" void ServicePoint_get_AvailableForRecycling_m4630 ();
extern "C" void SPKey__ctor_m4631 ();
extern "C" void SPKey_GetHashCode_m4632 ();
extern "C" void SPKey_Equals_m4633 ();
extern "C" void ServicePointManager__cctor_m4634 ();
extern "C" void ServicePointManager_get_CertificatePolicy_m4424 ();
extern "C" void ServicePointManager_get_CheckCertificateRevocationList_m4392 ();
extern "C" void ServicePointManager_get_SecurityProtocol_m4423 ();
extern "C" void ServicePointManager_get_ServerCertificateValidationCallback_m4426 ();
extern "C" void ServicePointManager_FindServicePoint_m4635 ();
extern "C" void ServicePointManager_RecycleServicePoints_m4636 ();
extern "C" void WebHeaderCollection__ctor_m4637 ();
extern "C" void WebHeaderCollection__ctor_m4638 ();
extern "C" void WebHeaderCollection__ctor_m4639 ();
extern "C" void WebHeaderCollection__cctor_m4640 ();
extern "C" void WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m4641 ();
extern "C" void WebHeaderCollection_Add_m4642 ();
extern "C" void WebHeaderCollection_AddWithoutValidate_m4643 ();
extern "C" void WebHeaderCollection_IsRestricted_m4644 ();
extern "C" void WebHeaderCollection_OnDeserialization_m4645 ();
extern "C" void WebHeaderCollection_ToString_m4646 ();
extern "C" void WebHeaderCollection_GetObjectData_m4647 ();
extern "C" void WebHeaderCollection_get_Count_m4648 ();
extern "C" void WebHeaderCollection_get_Keys_m4649 ();
extern "C" void WebHeaderCollection_Get_m4650 ();
extern "C" void WebHeaderCollection_GetKey_m4651 ();
extern "C" void WebHeaderCollection_GetEnumerator_m4652 ();
extern "C" void WebHeaderCollection_IsHeaderValue_m4653 ();
extern "C" void WebHeaderCollection_IsHeaderName_m4654 ();
extern "C" void WebProxy__ctor_m4655 ();
extern "C" void WebProxy__ctor_m4656 ();
extern "C" void WebProxy__ctor_m4657 ();
extern "C" void WebProxy_System_Runtime_Serialization_ISerializable_GetObjectData_m4658 ();
extern "C" void WebProxy_get_UseDefaultCredentials_m4659 ();
extern "C" void WebProxy_GetProxy_m4660 ();
extern "C" void WebProxy_IsBypassed_m4661 ();
extern "C" void WebProxy_GetObjectData_m4662 ();
extern "C" void WebProxy_CheckBypassList_m4663 ();
extern "C" void WebRequest__ctor_m4664 ();
extern "C" void WebRequest__ctor_m4665 ();
extern "C" void WebRequest__cctor_m4666 ();
extern "C" void WebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m4667 ();
extern "C" void WebRequest_AddDynamicPrefix_m4668 ();
extern "C" void WebRequest_GetMustImplement_m4669 ();
extern "C" void WebRequest_get_DefaultWebProxy_m4670 ();
extern "C" void WebRequest_GetDefaultWebProxy_m4671 ();
extern "C" void WebRequest_GetObjectData_m4672 ();
extern "C" void WebRequest_AddPrefix_m4673 ();
extern "C" void PublicKey__ctor_m4674 ();
extern "C" void PublicKey_get_EncodedKeyValue_m4675 ();
extern "C" void PublicKey_get_EncodedParameters_m4676 ();
extern "C" void PublicKey_get_Key_m4677 ();
extern "C" void PublicKey_get_Oid_m4678 ();
extern "C" void PublicKey_GetUnsignedBigInteger_m4679 ();
extern "C" void PublicKey_DecodeDSA_m4680 ();
extern "C" void PublicKey_DecodeRSA_m4681 ();
extern "C" void X500DistinguishedName__ctor_m4682 ();
extern "C" void X500DistinguishedName_Decode_m4683 ();
extern "C" void X500DistinguishedName_GetSeparator_m4684 ();
extern "C" void X500DistinguishedName_DecodeRawData_m4685 ();
extern "C" void X500DistinguishedName_Canonize_m4686 ();
extern "C" void X500DistinguishedName_AreEqual_m4687 ();
extern "C" void X509BasicConstraintsExtension__ctor_m4688 ();
extern "C" void X509BasicConstraintsExtension__ctor_m4689 ();
extern "C" void X509BasicConstraintsExtension__ctor_m4690 ();
extern "C" void X509BasicConstraintsExtension_get_CertificateAuthority_m4691 ();
extern "C" void X509BasicConstraintsExtension_get_HasPathLengthConstraint_m4692 ();
extern "C" void X509BasicConstraintsExtension_get_PathLengthConstraint_m4693 ();
extern "C" void X509BasicConstraintsExtension_CopyFrom_m4694 ();
extern "C" void X509BasicConstraintsExtension_Decode_m4695 ();
extern "C" void X509BasicConstraintsExtension_Encode_m4696 ();
extern "C" void X509BasicConstraintsExtension_ToString_m4697 ();
extern "C" void X509Certificate2__ctor_m4427 ();
extern "C" void X509Certificate2__cctor_m4698 ();
extern "C" void X509Certificate2_get_Extensions_m4699 ();
extern "C" void X509Certificate2_get_IssuerName_m4700 ();
extern "C" void X509Certificate2_get_NotAfter_m4701 ();
extern "C" void X509Certificate2_get_NotBefore_m4702 ();
extern "C" void X509Certificate2_get_PrivateKey_m4432 ();
extern "C" void X509Certificate2_get_PublicKey_m4703 ();
extern "C" void X509Certificate2_get_SerialNumber_m4704 ();
extern "C" void X509Certificate2_get_SignatureAlgorithm_m4705 ();
extern "C" void X509Certificate2_get_SubjectName_m4706 ();
extern "C" void X509Certificate2_get_Thumbprint_m4707 ();
extern "C" void X509Certificate2_get_Version_m4708 ();
extern "C" void X509Certificate2_GetNameInfo_m4709 ();
extern "C" void X509Certificate2_Find_m4710 ();
extern "C" void X509Certificate2_GetValueAsString_m4711 ();
extern "C" void X509Certificate2_ImportPkcs12_m4712 ();
extern "C" void X509Certificate2_Import_m4713 ();
extern "C" void X509Certificate2_Reset_m4714 ();
extern "C" void X509Certificate2_ToString_m4715 ();
extern "C" void X509Certificate2_ToString_m4716 ();
extern "C" void X509Certificate2_AppendBuffer_m4717 ();
extern "C" void X509Certificate2_Verify_m4718 ();
extern "C" void X509Certificate2_get_MonoCertificate_m4719 ();
extern "C" void X509Certificate2Collection__ctor_m4720 ();
extern "C" void X509Certificate2Collection__ctor_m4721 ();
extern "C" void X509Certificate2Collection_get_Item_m4722 ();
extern "C" void X509Certificate2Collection_Add_m4723 ();
extern "C" void X509Certificate2Collection_AddRange_m4724 ();
extern "C" void X509Certificate2Collection_Contains_m4725 ();
extern "C" void X509Certificate2Collection_Find_m4726 ();
extern "C" void X509Certificate2Collection_GetEnumerator_m4727 ();
extern "C" void X509Certificate2Enumerator__ctor_m4728 ();
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_get_Current_m4729 ();
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_MoveNext_m4730 ();
extern "C" void X509Certificate2Enumerator_get_Current_m4731 ();
extern "C" void X509Certificate2Enumerator_MoveNext_m4732 ();
extern "C" void X509CertificateEnumerator__ctor_m4733 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m4734 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m4735 ();
extern "C" void X509CertificateEnumerator_get_Current_m4451 ();
extern "C" void X509CertificateEnumerator_MoveNext_m4736 ();
extern "C" void X509CertificateCollection__ctor_m4442 ();
extern "C" void X509CertificateCollection__ctor_m4441 ();
extern "C" void X509CertificateCollection_get_Item_m4431 ();
extern "C" void X509CertificateCollection_AddRange_m4737 ();
extern "C" void X509CertificateCollection_GetEnumerator_m4450 ();
extern "C" void X509CertificateCollection_GetHashCode_m4738 ();
extern "C" void X509Chain__ctor_m4428 ();
extern "C" void X509Chain__ctor_m4739 ();
extern "C" void X509Chain__cctor_m4740 ();
extern "C" void X509Chain_get_ChainPolicy_m4741 ();
extern "C" void X509Chain_Build_m4429 ();
extern "C" void X509Chain_Reset_m4742 ();
extern "C" void X509Chain_get_Roots_m4743 ();
extern "C" void X509Chain_get_CertificateAuthorities_m4744 ();
extern "C" void X509Chain_get_CertificateCollection_m4745 ();
extern "C" void X509Chain_BuildChainFrom_m4746 ();
extern "C" void X509Chain_SelectBestFromCollection_m4747 ();
extern "C" void X509Chain_FindParent_m4748 ();
extern "C" void X509Chain_IsChainComplete_m4749 ();
extern "C" void X509Chain_IsSelfIssued_m4750 ();
extern "C" void X509Chain_ValidateChain_m4751 ();
extern "C" void X509Chain_Process_m4752 ();
extern "C" void X509Chain_PrepareForNextCertificate_m4753 ();
extern "C" void X509Chain_WrapUp_m4754 ();
extern "C" void X509Chain_ProcessCertificateExtensions_m4755 ();
extern "C" void X509Chain_IsSignedWith_m4756 ();
extern "C" void X509Chain_GetSubjectKeyIdentifier_m4757 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m4758 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m4759 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m4760 ();
extern "C" void X509Chain_CheckRevocationOnChain_m4761 ();
extern "C" void X509Chain_CheckRevocation_m4762 ();
extern "C" void X509Chain_CheckRevocation_m4763 ();
extern "C" void X509Chain_FindCrl_m4764 ();
extern "C" void X509Chain_ProcessCrlExtensions_m4765 ();
extern "C" void X509Chain_ProcessCrlEntryExtensions_m4766 ();
extern "C" void X509ChainElement__ctor_m4767 ();
extern "C" void X509ChainElement_get_Certificate_m4768 ();
extern "C" void X509ChainElement_get_ChainElementStatus_m4769 ();
extern "C" void X509ChainElement_get_StatusFlags_m4770 ();
extern "C" void X509ChainElement_set_StatusFlags_m4771 ();
extern "C" void X509ChainElement_Count_m4772 ();
extern "C" void X509ChainElement_Set_m4773 ();
extern "C" void X509ChainElement_UncompressFlags_m4774 ();
extern "C" void X509ChainElementCollection__ctor_m4775 ();
extern "C" void X509ChainElementCollection_System_Collections_ICollection_CopyTo_m4776 ();
extern "C" void X509ChainElementCollection_System_Collections_IEnumerable_GetEnumerator_m4777 ();
extern "C" void X509ChainElementCollection_get_Count_m4778 ();
extern "C" void X509ChainElementCollection_get_IsSynchronized_m4779 ();
extern "C" void X509ChainElementCollection_get_Item_m4780 ();
extern "C" void X509ChainElementCollection_get_SyncRoot_m4781 ();
extern "C" void X509ChainElementCollection_GetEnumerator_m4782 ();
extern "C" void X509ChainElementCollection_Add_m4783 ();
extern "C" void X509ChainElementCollection_Clear_m4784 ();
extern "C" void X509ChainElementCollection_Contains_m4785 ();
extern "C" void X509ChainElementEnumerator__ctor_m4786 ();
extern "C" void X509ChainElementEnumerator_System_Collections_IEnumerator_get_Current_m4787 ();
extern "C" void X509ChainElementEnumerator_get_Current_m4788 ();
extern "C" void X509ChainElementEnumerator_MoveNext_m4789 ();
extern "C" void X509ChainPolicy__ctor_m4790 ();
extern "C" void X509ChainPolicy_get_ExtraStore_m4791 ();
extern "C" void X509ChainPolicy_get_RevocationFlag_m4792 ();
extern "C" void X509ChainPolicy_get_RevocationMode_m4793 ();
extern "C" void X509ChainPolicy_get_VerificationFlags_m4794 ();
extern "C" void X509ChainPolicy_get_VerificationTime_m4795 ();
extern "C" void X509ChainPolicy_Reset_m4796 ();
extern "C" void X509ChainStatus__ctor_m4797 ();
extern "C" void X509ChainStatus_get_Status_m4798 ();
extern "C" void X509ChainStatus_set_Status_m4799 ();
extern "C" void X509ChainStatus_set_StatusInformation_m4800 ();
extern "C" void X509ChainStatus_GetInformation_m4801 ();
extern "C" void X509EnhancedKeyUsageExtension__ctor_m4802 ();
extern "C" void X509EnhancedKeyUsageExtension_CopyFrom_m4803 ();
extern "C" void X509EnhancedKeyUsageExtension_Decode_m4804 ();
extern "C" void X509EnhancedKeyUsageExtension_ToString_m4805 ();
extern "C" void X509Extension__ctor_m4806 ();
extern "C" void X509Extension__ctor_m4807 ();
extern "C" void X509Extension_get_Critical_m4808 ();
extern "C" void X509Extension_set_Critical_m4809 ();
extern "C" void X509Extension_CopyFrom_m4810 ();
extern "C" void X509Extension_FormatUnkownData_m4811 ();
extern "C" void X509ExtensionCollection__ctor_m4812 ();
extern "C" void X509ExtensionCollection_System_Collections_ICollection_CopyTo_m4813 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m4814 ();
extern "C" void X509ExtensionCollection_get_Count_m4815 ();
extern "C" void X509ExtensionCollection_get_IsSynchronized_m4816 ();
extern "C" void X509ExtensionCollection_get_SyncRoot_m4817 ();
extern "C" void X509ExtensionCollection_get_Item_m4818 ();
extern "C" void X509ExtensionCollection_GetEnumerator_m4819 ();
extern "C" void X509ExtensionEnumerator__ctor_m4820 ();
extern "C" void X509ExtensionEnumerator_System_Collections_IEnumerator_get_Current_m4821 ();
extern "C" void X509ExtensionEnumerator_get_Current_m4822 ();
extern "C" void X509ExtensionEnumerator_MoveNext_m4823 ();
extern "C" void X509KeyUsageExtension__ctor_m4824 ();
extern "C" void X509KeyUsageExtension__ctor_m4825 ();
extern "C" void X509KeyUsageExtension__ctor_m4826 ();
extern "C" void X509KeyUsageExtension_get_KeyUsages_m4827 ();
extern "C" void X509KeyUsageExtension_CopyFrom_m4828 ();
extern "C" void X509KeyUsageExtension_GetValidFlags_m4829 ();
extern "C" void X509KeyUsageExtension_Decode_m4830 ();
extern "C" void X509KeyUsageExtension_Encode_m4831 ();
extern "C" void X509KeyUsageExtension_ToString_m4832 ();
extern "C" void X509Store__ctor_m4833 ();
extern "C" void X509Store_get_Certificates_m4834 ();
extern "C" void X509Store_get_Factory_m4835 ();
extern "C" void X509Store_get_Store_m4836 ();
extern "C" void X509Store_Close_m4837 ();
extern "C" void X509Store_Open_m4838 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m4839 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m4840 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m4841 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m4842 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m4843 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m4844 ();
extern "C" void X509SubjectKeyIdentifierExtension_get_SubjectKeyIdentifier_m4845 ();
extern "C" void X509SubjectKeyIdentifierExtension_CopyFrom_m4846 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHexChar_m4847 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHexChars_m4848 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHex_m4849 ();
extern "C" void X509SubjectKeyIdentifierExtension_Decode_m4850 ();
extern "C" void X509SubjectKeyIdentifierExtension_Encode_m4851 ();
extern "C" void X509SubjectKeyIdentifierExtension_ToString_m4852 ();
extern "C" void AsnEncodedData__ctor_m4853 ();
extern "C" void AsnEncodedData__ctor_m4854 ();
extern "C" void AsnEncodedData__ctor_m4855 ();
extern "C" void AsnEncodedData_get_Oid_m4856 ();
extern "C" void AsnEncodedData_set_Oid_m4857 ();
extern "C" void AsnEncodedData_get_RawData_m4858 ();
extern "C" void AsnEncodedData_set_RawData_m4859 ();
extern "C" void AsnEncodedData_CopyFrom_m4860 ();
extern "C" void AsnEncodedData_ToString_m4861 ();
extern "C" void AsnEncodedData_Default_m4862 ();
extern "C" void AsnEncodedData_BasicConstraintsExtension_m4863 ();
extern "C" void AsnEncodedData_EnhancedKeyUsageExtension_m4864 ();
extern "C" void AsnEncodedData_KeyUsageExtension_m4865 ();
extern "C" void AsnEncodedData_SubjectKeyIdentifierExtension_m4866 ();
extern "C" void AsnEncodedData_SubjectAltName_m4867 ();
extern "C" void AsnEncodedData_NetscapeCertType_m4868 ();
extern "C" void Oid__ctor_m4869 ();
extern "C" void Oid__ctor_m4870 ();
extern "C" void Oid__ctor_m4871 ();
extern "C" void Oid__ctor_m4872 ();
extern "C" void Oid_get_FriendlyName_m4873 ();
extern "C" void Oid_get_Value_m4874 ();
extern "C" void Oid_GetName_m4875 ();
extern "C" void OidCollection__ctor_m4876 ();
extern "C" void OidCollection_System_Collections_ICollection_CopyTo_m4877 ();
extern "C" void OidCollection_System_Collections_IEnumerable_GetEnumerator_m4878 ();
extern "C" void OidCollection_get_Count_m4879 ();
extern "C" void OidCollection_get_IsSynchronized_m4880 ();
extern "C" void OidCollection_get_Item_m4881 ();
extern "C" void OidCollection_get_SyncRoot_m4882 ();
extern "C" void OidCollection_Add_m4883 ();
extern "C" void OidEnumerator__ctor_m4884 ();
extern "C" void OidEnumerator_System_Collections_IEnumerator_get_Current_m4885 ();
extern "C" void OidEnumerator_MoveNext_m4886 ();
extern "C" void MatchAppendEvaluator__ctor_m4887 ();
extern "C" void MatchAppendEvaluator_Invoke_m4888 ();
extern "C" void MatchAppendEvaluator_BeginInvoke_m4889 ();
extern "C" void MatchAppendEvaluator_EndInvoke_m4890 ();
extern "C" void BaseMachine__ctor_m4891 ();
extern "C" void BaseMachine_Replace_m4892 ();
extern "C" void BaseMachine_Scan_m4893 ();
extern "C" void BaseMachine_LTRReplace_m4894 ();
extern "C" void BaseMachine_RTLReplace_m4895 ();
extern "C" void Capture__ctor_m4896 ();
extern "C" void Capture__ctor_m4897 ();
extern "C" void Capture_get_Index_m4898 ();
extern "C" void Capture_get_Length_m4899 ();
extern "C" void Capture_get_Value_m4458 ();
extern "C" void Capture_ToString_m4900 ();
extern "C" void Capture_get_Text_m4901 ();
extern "C" void CaptureCollection__ctor_m4902 ();
extern "C" void CaptureCollection_get_Count_m4903 ();
extern "C" void CaptureCollection_get_IsSynchronized_m4904 ();
extern "C" void CaptureCollection_SetValue_m4905 ();
extern "C" void CaptureCollection_get_SyncRoot_m4906 ();
extern "C" void CaptureCollection_CopyTo_m4907 ();
extern "C" void CaptureCollection_GetEnumerator_m4908 ();
extern "C" void Group__ctor_m4909 ();
extern "C" void Group__ctor_m4910 ();
extern "C" void Group__ctor_m4911 ();
extern "C" void Group__cctor_m4912 ();
extern "C" void Group_get_Captures_m4913 ();
extern "C" void Group_get_Success_m4456 ();
extern "C" void GroupCollection__ctor_m4914 ();
extern "C" void GroupCollection_get_Count_m4915 ();
extern "C" void GroupCollection_get_IsSynchronized_m4916 ();
extern "C" void GroupCollection_get_Item_m4457 ();
extern "C" void GroupCollection_SetValue_m4917 ();
extern "C" void GroupCollection_get_SyncRoot_m4918 ();
extern "C" void GroupCollection_CopyTo_m4919 ();
extern "C" void GroupCollection_GetEnumerator_m4920 ();
extern "C" void Match__ctor_m4921 ();
extern "C" void Match__ctor_m4922 ();
extern "C" void Match__ctor_m4923 ();
extern "C" void Match__cctor_m4924 ();
extern "C" void Match_get_Empty_m4925 ();
extern "C" void Match_get_Groups_m4926 ();
extern "C" void Match_NextMatch_m4927 ();
extern "C" void Match_get_Regex_m4928 ();
extern "C" void Enumerator__ctor_m4929 ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m4930 ();
extern "C" void Enumerator_System_Collections_IEnumerator_MoveNext_m4931 ();
extern "C" void MatchCollection__ctor_m4932 ();
extern "C" void MatchCollection_get_Count_m4933 ();
extern "C" void MatchCollection_get_IsSynchronized_m4934 ();
extern "C" void MatchCollection_get_Item_m4935 ();
extern "C" void MatchCollection_get_SyncRoot_m4936 ();
extern "C" void MatchCollection_CopyTo_m4937 ();
extern "C" void MatchCollection_GetEnumerator_m4938 ();
extern "C" void MatchCollection_TryToGet_m4939 ();
extern "C" void MatchCollection_get_FullList_m4940 ();
extern "C" void Regex__ctor_m4941 ();
extern "C" void Regex__ctor_m4454 ();
extern "C" void Regex__ctor_m4942 ();
extern "C" void Regex__ctor_m4943 ();
extern "C" void Regex__cctor_m4944 ();
extern "C" void Regex_System_Runtime_Serialization_ISerializable_GetObjectData_m4945 ();
extern "C" void Regex_Replace_m3381 ();
extern "C" void Regex_Replace_m4946 ();
extern "C" void Regex_validate_options_m4947 ();
extern "C" void Regex_Init_m4948 ();
extern "C" void Regex_InitNewRegex_m4949 ();
extern "C" void Regex_CreateMachineFactory_m4950 ();
extern "C" void Regex_get_Options_m4951 ();
extern "C" void Regex_get_RightToLeft_m4952 ();
extern "C" void Regex_GroupNumberFromName_m4953 ();
extern "C" void Regex_GetGroupIndex_m4954 ();
extern "C" void Regex_default_startat_m4955 ();
extern "C" void Regex_IsMatch_m4956 ();
extern "C" void Regex_IsMatch_m4957 ();
extern "C" void Regex_Match_m4958 ();
extern "C" void Regex_Matches_m4455 ();
extern "C" void Regex_Matches_m4959 ();
extern "C" void Regex_Replace_m4960 ();
extern "C" void Regex_Replace_m4961 ();
extern "C" void Regex_ToString_m4962 ();
extern "C" void Regex_get_GroupCount_m4963 ();
extern "C" void Regex_get_Gap_m4964 ();
extern "C" void Regex_CreateMachine_m4965 ();
extern "C" void Regex_GetGroupNamesArray_m4966 ();
extern "C" void Regex_get_GroupNumbers_m4967 ();
extern "C" void Key__ctor_m4968 ();
extern "C" void Key_GetHashCode_m4969 ();
extern "C" void Key_Equals_m4970 ();
extern "C" void Key_ToString_m4971 ();
extern "C" void FactoryCache__ctor_m4972 ();
extern "C" void FactoryCache_Add_m4973 ();
extern "C" void FactoryCache_Cleanup_m4974 ();
extern "C" void FactoryCache_Lookup_m4975 ();
extern "C" void Node__ctor_m4976 ();
extern "C" void MRUList__ctor_m4977 ();
extern "C" void MRUList_Use_m4978 ();
extern "C" void MRUList_Evict_m4979 ();
extern "C" void CategoryUtils_CategoryFromName_m4980 ();
extern "C" void CategoryUtils_IsCategory_m4981 ();
extern "C" void CategoryUtils_IsCategory_m4982 ();
extern "C" void LinkRef__ctor_m4983 ();
extern "C" void InterpreterFactory__ctor_m4984 ();
extern "C" void InterpreterFactory_NewInstance_m4985 ();
extern "C" void InterpreterFactory_get_GroupCount_m4986 ();
extern "C" void InterpreterFactory_get_Gap_m4987 ();
extern "C" void InterpreterFactory_set_Gap_m4988 ();
extern "C" void InterpreterFactory_get_Mapping_m4989 ();
extern "C" void InterpreterFactory_set_Mapping_m4990 ();
extern "C" void InterpreterFactory_get_NamesMapping_m4991 ();
extern "C" void InterpreterFactory_set_NamesMapping_m4992 ();
extern "C" void PatternLinkStack__ctor_m4993 ();
extern "C" void PatternLinkStack_set_BaseAddress_m4994 ();
extern "C" void PatternLinkStack_get_OffsetAddress_m4995 ();
extern "C" void PatternLinkStack_set_OffsetAddress_m4996 ();
extern "C" void PatternLinkStack_GetOffset_m4997 ();
extern "C" void PatternLinkStack_GetCurrent_m4998 ();
extern "C" void PatternLinkStack_SetCurrent_m4999 ();
extern "C" void PatternCompiler__ctor_m5000 ();
extern "C" void PatternCompiler_EncodeOp_m5001 ();
extern "C" void PatternCompiler_GetMachineFactory_m5002 ();
extern "C" void PatternCompiler_EmitFalse_m5003 ();
extern "C" void PatternCompiler_EmitTrue_m5004 ();
extern "C" void PatternCompiler_EmitCount_m5005 ();
extern "C" void PatternCompiler_EmitCharacter_m5006 ();
extern "C" void PatternCompiler_EmitCategory_m5007 ();
extern "C" void PatternCompiler_EmitNotCategory_m5008 ();
extern "C" void PatternCompiler_EmitRange_m5009 ();
extern "C" void PatternCompiler_EmitSet_m5010 ();
extern "C" void PatternCompiler_EmitString_m5011 ();
extern "C" void PatternCompiler_EmitPosition_m5012 ();
extern "C" void PatternCompiler_EmitOpen_m5013 ();
extern "C" void PatternCompiler_EmitClose_m5014 ();
extern "C" void PatternCompiler_EmitBalanceStart_m5015 ();
extern "C" void PatternCompiler_EmitBalance_m5016 ();
extern "C" void PatternCompiler_EmitReference_m5017 ();
extern "C" void PatternCompiler_EmitIfDefined_m5018 ();
extern "C" void PatternCompiler_EmitSub_m5019 ();
extern "C" void PatternCompiler_EmitTest_m5020 ();
extern "C" void PatternCompiler_EmitBranch_m5021 ();
extern "C" void PatternCompiler_EmitJump_m5022 ();
extern "C" void PatternCompiler_EmitRepeat_m5023 ();
extern "C" void PatternCompiler_EmitUntil_m5024 ();
extern "C" void PatternCompiler_EmitFastRepeat_m5025 ();
extern "C" void PatternCompiler_EmitIn_m5026 ();
extern "C" void PatternCompiler_EmitAnchor_m5027 ();
extern "C" void PatternCompiler_EmitInfo_m5028 ();
extern "C" void PatternCompiler_NewLink_m5029 ();
extern "C" void PatternCompiler_ResolveLink_m5030 ();
extern "C" void PatternCompiler_EmitBranchEnd_m5031 ();
extern "C" void PatternCompiler_EmitAlternationEnd_m5032 ();
extern "C" void PatternCompiler_MakeFlags_m5033 ();
extern "C" void PatternCompiler_Emit_m5034 ();
extern "C" void PatternCompiler_Emit_m5035 ();
extern "C" void PatternCompiler_Emit_m5036 ();
extern "C" void PatternCompiler_get_CurrentAddress_m5037 ();
extern "C" void PatternCompiler_BeginLink_m5038 ();
extern "C" void PatternCompiler_EmitLink_m5039 ();
extern "C" void LinkStack__ctor_m5040 ();
extern "C" void LinkStack_Push_m5041 ();
extern "C" void LinkStack_Pop_m5042 ();
extern "C" void Mark_get_IsDefined_m5043 ();
extern "C" void Mark_get_Index_m5044 ();
extern "C" void Mark_get_Length_m5045 ();
extern "C" void IntStack_Pop_m5046 ();
extern "C" void IntStack_Push_m5047 ();
extern "C" void IntStack_get_Count_m5048 ();
extern "C" void IntStack_set_Count_m5049 ();
extern "C" void RepeatContext__ctor_m5050 ();
extern "C" void RepeatContext_get_Count_m5051 ();
extern "C" void RepeatContext_set_Count_m5052 ();
extern "C" void RepeatContext_get_Start_m5053 ();
extern "C" void RepeatContext_set_Start_m5054 ();
extern "C" void RepeatContext_get_IsMinimum_m5055 ();
extern "C" void RepeatContext_get_IsMaximum_m5056 ();
extern "C" void RepeatContext_get_IsLazy_m5057 ();
extern "C" void RepeatContext_get_Expression_m5058 ();
extern "C" void RepeatContext_get_Previous_m5059 ();
extern "C" void Interpreter__ctor_m5060 ();
extern "C" void Interpreter_ReadProgramCount_m5061 ();
extern "C" void Interpreter_Scan_m5062 ();
extern "C" void Interpreter_Reset_m5063 ();
extern "C" void Interpreter_Eval_m5064 ();
extern "C" void Interpreter_EvalChar_m5065 ();
extern "C" void Interpreter_TryMatch_m5066 ();
extern "C" void Interpreter_IsPosition_m5067 ();
extern "C" void Interpreter_IsWordChar_m5068 ();
extern "C" void Interpreter_GetString_m5069 ();
extern "C" void Interpreter_Open_m5070 ();
extern "C" void Interpreter_Close_m5071 ();
extern "C" void Interpreter_Balance_m5072 ();
extern "C" void Interpreter_Checkpoint_m5073 ();
extern "C" void Interpreter_Backtrack_m5074 ();
extern "C" void Interpreter_ResetGroups_m5075 ();
extern "C" void Interpreter_GetLastDefined_m5076 ();
extern "C" void Interpreter_CreateMark_m5077 ();
extern "C" void Interpreter_GetGroupInfo_m5078 ();
extern "C" void Interpreter_PopulateGroup_m5079 ();
extern "C" void Interpreter_GenerateMatch_m5080 ();
extern "C" void Interval__ctor_m5081 ();
extern "C" void Interval_get_Empty_m5082 ();
extern "C" void Interval_get_IsDiscontiguous_m5083 ();
extern "C" void Interval_get_IsSingleton_m5084 ();
extern "C" void Interval_get_IsEmpty_m5085 ();
extern "C" void Interval_get_Size_m5086 ();
extern "C" void Interval_IsDisjoint_m5087 ();
extern "C" void Interval_IsAdjacent_m5088 ();
extern "C" void Interval_Contains_m5089 ();
extern "C" void Interval_Contains_m5090 ();
extern "C" void Interval_Intersects_m5091 ();
extern "C" void Interval_Merge_m5092 ();
extern "C" void Interval_CompareTo_m5093 ();
extern "C" void Enumerator__ctor_m5094 ();
extern "C" void Enumerator_get_Current_m5095 ();
extern "C" void Enumerator_MoveNext_m5096 ();
extern "C" void Enumerator_Reset_m5097 ();
extern "C" void CostDelegate__ctor_m5098 ();
extern "C" void CostDelegate_Invoke_m5099 ();
extern "C" void CostDelegate_BeginInvoke_m5100 ();
extern "C" void CostDelegate_EndInvoke_m5101 ();
extern "C" void IntervalCollection__ctor_m5102 ();
extern "C" void IntervalCollection_get_Item_m5103 ();
extern "C" void IntervalCollection_Add_m5104 ();
extern "C" void IntervalCollection_Normalize_m5105 ();
extern "C" void IntervalCollection_GetMetaCollection_m5106 ();
extern "C" void IntervalCollection_Optimize_m5107 ();
extern "C" void IntervalCollection_get_Count_m5108 ();
extern "C" void IntervalCollection_get_IsSynchronized_m5109 ();
extern "C" void IntervalCollection_get_SyncRoot_m5110 ();
extern "C" void IntervalCollection_CopyTo_m5111 ();
extern "C" void IntervalCollection_GetEnumerator_m5112 ();
extern "C" void Parser__ctor_m5113 ();
extern "C" void Parser_ParseDecimal_m5114 ();
extern "C" void Parser_ParseOctal_m5115 ();
extern "C" void Parser_ParseHex_m5116 ();
extern "C" void Parser_ParseNumber_m5117 ();
extern "C" void Parser_ParseName_m5118 ();
extern "C" void Parser_ParseRegularExpression_m5119 ();
extern "C" void Parser_GetMapping_m5120 ();
extern "C" void Parser_ParseGroup_m5121 ();
extern "C" void Parser_ParseGroupingConstruct_m5122 ();
extern "C" void Parser_ParseAssertionType_m5123 ();
extern "C" void Parser_ParseOptions_m5124 ();
extern "C" void Parser_ParseCharacterClass_m5125 ();
extern "C" void Parser_ParseRepetitionBounds_m5126 ();
extern "C" void Parser_ParseUnicodeCategory_m5127 ();
extern "C" void Parser_ParseSpecial_m5128 ();
extern "C" void Parser_ParseEscape_m5129 ();
extern "C" void Parser_ParseName_m5130 ();
extern "C" void Parser_IsNameChar_m5131 ();
extern "C" void Parser_ParseNumber_m5132 ();
extern "C" void Parser_ParseDigit_m5133 ();
extern "C" void Parser_ConsumeWhitespace_m5134 ();
extern "C" void Parser_ResolveReferences_m5135 ();
extern "C" void Parser_HandleExplicitNumericGroups_m5136 ();
extern "C" void Parser_IsIgnoreCase_m5137 ();
extern "C" void Parser_IsMultiline_m5138 ();
extern "C" void Parser_IsExplicitCapture_m5139 ();
extern "C" void Parser_IsSingleline_m5140 ();
extern "C" void Parser_IsIgnorePatternWhitespace_m5141 ();
extern "C" void Parser_IsECMAScript_m5142 ();
extern "C" void Parser_NewParseException_m5143 ();
extern "C" void QuickSearch__ctor_m5144 ();
extern "C" void QuickSearch__cctor_m5145 ();
extern "C" void QuickSearch_get_Length_m5146 ();
extern "C" void QuickSearch_Search_m5147 ();
extern "C" void QuickSearch_SetupShiftTable_m5148 ();
extern "C" void QuickSearch_GetShiftDistance_m5149 ();
extern "C" void QuickSearch_GetChar_m5150 ();
extern "C" void ReplacementEvaluator__ctor_m5151 ();
extern "C" void ReplacementEvaluator_Evaluate_m5152 ();
extern "C" void ReplacementEvaluator_EvaluateAppend_m5153 ();
extern "C" void ReplacementEvaluator_get_NeedsGroupsOrCaptures_m5154 ();
extern "C" void ReplacementEvaluator_Ensure_m5155 ();
extern "C" void ReplacementEvaluator_AddFromReplacement_m5156 ();
extern "C" void ReplacementEvaluator_AddInt_m5157 ();
extern "C" void ReplacementEvaluator_Compile_m5158 ();
extern "C" void ReplacementEvaluator_CompileTerm_m5159 ();
extern "C" void ExpressionCollection__ctor_m5160 ();
extern "C" void ExpressionCollection_Add_m5161 ();
extern "C" void ExpressionCollection_get_Item_m5162 ();
extern "C" void ExpressionCollection_set_Item_m5163 ();
extern "C" void ExpressionCollection_OnValidate_m5164 ();
extern "C" void Expression__ctor_m5165 ();
extern "C" void Expression_GetFixedWidth_m5166 ();
extern "C" void Expression_GetAnchorInfo_m5167 ();
extern "C" void CompositeExpression__ctor_m5168 ();
extern "C" void CompositeExpression_get_Expressions_m5169 ();
extern "C" void CompositeExpression_GetWidth_m5170 ();
extern "C" void CompositeExpression_IsComplex_m5171 ();
extern "C" void Group__ctor_m5172 ();
extern "C" void Group_AppendExpression_m5173 ();
extern "C" void Group_Compile_m5174 ();
extern "C" void Group_GetWidth_m5175 ();
extern "C" void Group_GetAnchorInfo_m5176 ();
extern "C" void RegularExpression__ctor_m5177 ();
extern "C" void RegularExpression_set_GroupCount_m5178 ();
extern "C" void RegularExpression_Compile_m5179 ();
extern "C" void CapturingGroup__ctor_m5180 ();
extern "C" void CapturingGroup_get_Index_m5181 ();
extern "C" void CapturingGroup_set_Index_m5182 ();
extern "C" void CapturingGroup_get_Name_m5183 ();
extern "C" void CapturingGroup_set_Name_m5184 ();
extern "C" void CapturingGroup_get_IsNamed_m5185 ();
extern "C" void CapturingGroup_Compile_m5186 ();
extern "C" void CapturingGroup_IsComplex_m5187 ();
extern "C" void CapturingGroup_CompareTo_m5188 ();
extern "C" void BalancingGroup__ctor_m5189 ();
extern "C" void BalancingGroup_set_Balance_m5190 ();
extern "C" void BalancingGroup_Compile_m5191 ();
extern "C" void NonBacktrackingGroup__ctor_m5192 ();
extern "C" void NonBacktrackingGroup_Compile_m5193 ();
extern "C" void NonBacktrackingGroup_IsComplex_m5194 ();
extern "C" void Repetition__ctor_m5195 ();
extern "C" void Repetition_get_Expression_m5196 ();
extern "C" void Repetition_set_Expression_m5197 ();
extern "C" void Repetition_get_Minimum_m5198 ();
extern "C" void Repetition_Compile_m5199 ();
extern "C" void Repetition_GetWidth_m5200 ();
extern "C" void Repetition_GetAnchorInfo_m5201 ();
extern "C" void Assertion__ctor_m5202 ();
extern "C" void Assertion_get_TrueExpression_m5203 ();
extern "C" void Assertion_set_TrueExpression_m5204 ();
extern "C" void Assertion_get_FalseExpression_m5205 ();
extern "C" void Assertion_set_FalseExpression_m5206 ();
extern "C" void Assertion_GetWidth_m5207 ();
extern "C" void CaptureAssertion__ctor_m5208 ();
extern "C" void CaptureAssertion_set_CapturingGroup_m5209 ();
extern "C" void CaptureAssertion_Compile_m5210 ();
extern "C" void CaptureAssertion_IsComplex_m5211 ();
extern "C" void CaptureAssertion_get_Alternate_m5212 ();
extern "C" void ExpressionAssertion__ctor_m5213 ();
extern "C" void ExpressionAssertion_set_Reverse_m5214 ();
extern "C" void ExpressionAssertion_set_Negate_m5215 ();
extern "C" void ExpressionAssertion_get_TestExpression_m5216 ();
extern "C" void ExpressionAssertion_set_TestExpression_m5217 ();
extern "C" void ExpressionAssertion_Compile_m5218 ();
extern "C" void ExpressionAssertion_IsComplex_m5219 ();
extern "C" void Alternation__ctor_m5220 ();
extern "C" void Alternation_get_Alternatives_m5221 ();
extern "C" void Alternation_AddAlternative_m5222 ();
extern "C" void Alternation_Compile_m5223 ();
extern "C" void Alternation_GetWidth_m5224 ();
extern "C" void Literal__ctor_m5225 ();
extern "C" void Literal_CompileLiteral_m5226 ();
extern "C" void Literal_Compile_m5227 ();
extern "C" void Literal_GetWidth_m5228 ();
extern "C" void Literal_GetAnchorInfo_m5229 ();
extern "C" void Literal_IsComplex_m5230 ();
extern "C" void PositionAssertion__ctor_m5231 ();
extern "C" void PositionAssertion_Compile_m5232 ();
extern "C" void PositionAssertion_GetWidth_m5233 ();
extern "C" void PositionAssertion_IsComplex_m5234 ();
extern "C" void PositionAssertion_GetAnchorInfo_m5235 ();
extern "C" void Reference__ctor_m5236 ();
extern "C" void Reference_get_CapturingGroup_m5237 ();
extern "C" void Reference_set_CapturingGroup_m5238 ();
extern "C" void Reference_get_IgnoreCase_m5239 ();
extern "C" void Reference_Compile_m5240 ();
extern "C" void Reference_GetWidth_m5241 ();
extern "C" void Reference_IsComplex_m5242 ();
extern "C" void BackslashNumber__ctor_m5243 ();
extern "C" void BackslashNumber_ResolveReference_m5244 ();
extern "C" void BackslashNumber_Compile_m5245 ();
extern "C" void CharacterClass__ctor_m5246 ();
extern "C" void CharacterClass__ctor_m5247 ();
extern "C" void CharacterClass__cctor_m5248 ();
extern "C" void CharacterClass_AddCategory_m5249 ();
extern "C" void CharacterClass_AddCharacter_m5250 ();
extern "C" void CharacterClass_AddRange_m5251 ();
extern "C" void CharacterClass_Compile_m5252 ();
extern "C" void CharacterClass_GetWidth_m5253 ();
extern "C" void CharacterClass_IsComplex_m5254 ();
extern "C" void CharacterClass_GetIntervalCost_m5255 ();
extern "C" void AnchorInfo__ctor_m5256 ();
extern "C" void AnchorInfo__ctor_m5257 ();
extern "C" void AnchorInfo__ctor_m5258 ();
extern "C" void AnchorInfo_get_Offset_m5259 ();
extern "C" void AnchorInfo_get_Width_m5260 ();
extern "C" void AnchorInfo_get_Length_m5261 ();
extern "C" void AnchorInfo_get_IsUnknownWidth_m5262 ();
extern "C" void AnchorInfo_get_IsComplete_m5263 ();
extern "C" void AnchorInfo_get_Substring_m5264 ();
extern "C" void AnchorInfo_get_IgnoreCase_m5265 ();
extern "C" void AnchorInfo_get_Position_m5266 ();
extern "C" void AnchorInfo_get_IsSubstring_m5267 ();
extern "C" void AnchorInfo_get_IsPosition_m5268 ();
extern "C" void AnchorInfo_GetInterval_m5269 ();
extern "C" void DefaultUriParser__ctor_m5270 ();
extern "C" void DefaultUriParser__ctor_m5271 ();
extern "C" void UriScheme__ctor_m5272 ();
extern "C" void Uri__ctor_m3308 ();
extern "C" void Uri__ctor_m5273 ();
extern "C" void Uri__ctor_m5274 ();
extern "C" void Uri__ctor_m3310 ();
extern "C" void Uri__cctor_m5275 ();
extern "C" void Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m5276 ();
extern "C" void Uri_Merge_m5277 ();
extern "C" void Uri_get_AbsoluteUri_m5278 ();
extern "C" void Uri_get_Authority_m5279 ();
extern "C" void Uri_get_Host_m4422 ();
extern "C" void Uri_get_IsFile_m5280 ();
extern "C" void Uri_get_IsLoopback_m5281 ();
extern "C" void Uri_get_IsUnc_m5282 ();
extern "C" void Uri_get_Scheme_m5283 ();
extern "C" void Uri_get_IsAbsoluteUri_m5284 ();
extern "C" void Uri_CheckHostName_m5285 ();
extern "C" void Uri_IsIPv4Address_m5286 ();
extern "C" void Uri_IsDomainAddress_m5287 ();
extern "C" void Uri_CheckSchemeName_m5288 ();
extern "C" void Uri_IsAlpha_m5289 ();
extern "C" void Uri_Equals_m5290 ();
extern "C" void Uri_InternalEquals_m5291 ();
extern "C" void Uri_GetHashCode_m5292 ();
extern "C" void Uri_GetLeftPart_m5293 ();
extern "C" void Uri_FromHex_m5294 ();
extern "C" void Uri_HexEscape_m5295 ();
extern "C" void Uri_IsHexDigit_m5296 ();
extern "C" void Uri_IsHexEncoding_m5297 ();
extern "C" void Uri_AppendQueryAndFragment_m5298 ();
extern "C" void Uri_ToString_m5299 ();
extern "C" void Uri_EscapeString_m5300 ();
extern "C" void Uri_EscapeString_m5301 ();
extern "C" void Uri_ParseUri_m5302 ();
extern "C" void Uri_Unescape_m5303 ();
extern "C" void Uri_Unescape_m5304 ();
extern "C" void Uri_ParseAsWindowsUNC_m5305 ();
extern "C" void Uri_ParseAsWindowsAbsoluteFilePath_m5306 ();
extern "C" void Uri_ParseAsUnixAbsoluteFilePath_m5307 ();
extern "C" void Uri_Parse_m5308 ();
extern "C" void Uri_ParseNoExceptions_m5309 ();
extern "C" void Uri_CompactEscaped_m5310 ();
extern "C" void Uri_Reduce_m5311 ();
extern "C" void Uri_HexUnescapeMultiByte_m5312 ();
extern "C" void Uri_GetSchemeDelimiter_m5313 ();
extern "C" void Uri_GetDefaultPort_m5314 ();
extern "C" void Uri_GetOpaqueWiseSchemeDelimiter_m5315 ();
extern "C" void Uri_IsPredefinedScheme_m5316 ();
extern "C" void Uri_get_Parser_m5317 ();
extern "C" void Uri_EnsureAbsoluteUri_m5318 ();
extern "C" void Uri_op_Equality_m5319 ();
extern "C" void UriFormatException__ctor_m5320 ();
extern "C" void UriFormatException__ctor_m5321 ();
extern "C" void UriFormatException__ctor_m5322 ();
extern "C" void UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m5323 ();
extern "C" void UriParser__ctor_m5324 ();
extern "C" void UriParser__cctor_m5325 ();
extern "C" void UriParser_InitializeAndValidate_m5326 ();
extern "C" void UriParser_OnRegister_m5327 ();
extern "C" void UriParser_set_SchemeName_m5328 ();
extern "C" void UriParser_get_DefaultPort_m5329 ();
extern "C" void UriParser_set_DefaultPort_m5330 ();
extern "C" void UriParser_CreateDefaults_m5331 ();
extern "C" void UriParser_InternalRegister_m5332 ();
extern "C" void UriParser_GetParser_m5333 ();
extern "C" void RemoteCertificateValidationCallback__ctor_m5334 ();
extern "C" void RemoteCertificateValidationCallback_Invoke_m4430 ();
extern "C" void RemoteCertificateValidationCallback_BeginInvoke_m5335 ();
extern "C" void RemoteCertificateValidationCallback_EndInvoke_m5336 ();
extern "C" void MatchEvaluator__ctor_m5337 ();
extern "C" void MatchEvaluator_Invoke_m5338 ();
extern "C" void MatchEvaluator_BeginInvoke_m5339 ();
extern "C" void MatchEvaluator_EndInvoke_m5340 ();
extern "C" void Object__ctor_m199 ();
extern "C" void Object_Equals_m5429 ();
extern "C" void Object_Equals_m5426 ();
extern "C" void Object_Finalize_m3230 ();
extern "C" void Object_GetHashCode_m5430 ();
extern "C" void Object_GetType_m1538 ();
extern "C" void Object_MemberwiseClone_m5431 ();
extern "C" void Object_ToString_m3295 ();
extern "C" void Object_ReferenceEquals_m3260 ();
extern "C" void Object_InternalGetHashCode_m5432 ();
extern "C" void ValueType__ctor_m5433 ();
extern "C" void ValueType_InternalEquals_m5434 ();
extern "C" void ValueType_DefaultEquals_m5435 ();
extern "C" void ValueType_Equals_m5436 ();
extern "C" void ValueType_InternalGetHashCode_m5437 ();
extern "C" void ValueType_GetHashCode_m5438 ();
extern "C" void ValueType_ToString_m5439 ();
extern "C" void Attribute__ctor_m3265 ();
extern "C" void Attribute_CheckParameters_m5440 ();
extern "C" void Attribute_GetCustomAttribute_m5441 ();
extern "C" void Attribute_GetCustomAttribute_m5442 ();
extern "C" void Attribute_GetHashCode_m3398 ();
extern "C" void Attribute_IsDefined_m5443 ();
extern "C" void Attribute_IsDefined_m5444 ();
extern "C" void Attribute_IsDefined_m5445 ();
extern "C" void Attribute_IsDefined_m5446 ();
extern "C" void Attribute_Equals_m5447 ();
extern "C" void Int32_System_IConvertible_ToBoolean_m5448 ();
extern "C" void Int32_System_IConvertible_ToByte_m5449 ();
extern "C" void Int32_System_IConvertible_ToChar_m5450 ();
extern "C" void Int32_System_IConvertible_ToDateTime_m5451 ();
extern "C" void Int32_System_IConvertible_ToDecimal_m5452 ();
extern "C" void Int32_System_IConvertible_ToDouble_m5453 ();
extern "C" void Int32_System_IConvertible_ToInt16_m5454 ();
extern "C" void Int32_System_IConvertible_ToInt32_m5455 ();
extern "C" void Int32_System_IConvertible_ToInt64_m5456 ();
extern "C" void Int32_System_IConvertible_ToSByte_m5457 ();
extern "C" void Int32_System_IConvertible_ToSingle_m5458 ();
extern "C" void Int32_System_IConvertible_ToType_m5459 ();
extern "C" void Int32_System_IConvertible_ToUInt16_m5460 ();
extern "C" void Int32_System_IConvertible_ToUInt32_m5461 ();
extern "C" void Int32_System_IConvertible_ToUInt64_m5462 ();
extern "C" void Int32_CompareTo_m5463 ();
extern "C" void Int32_Equals_m5464 ();
extern "C" void Int32_GetHashCode_m3244 ();
extern "C" void Int32_CompareTo_m1474 ();
extern "C" void Int32_Equals_m3246 ();
extern "C" void Int32_ProcessTrailingWhitespace_m5465 ();
extern "C" void Int32_Parse_m5466 ();
extern "C" void Int32_Parse_m5467 ();
extern "C" void Int32_CheckStyle_m5468 ();
extern "C" void Int32_JumpOverWhite_m5469 ();
extern "C" void Int32_FindSign_m5470 ();
extern "C" void Int32_FindCurrency_m5471 ();
extern "C" void Int32_FindExponent_m5472 ();
extern "C" void Int32_FindOther_m5473 ();
extern "C" void Int32_ValidDigit_m5474 ();
extern "C" void Int32_GetFormatException_m5475 ();
extern "C" void Int32_Parse_m5476 ();
extern "C" void Int32_Parse_m244 ();
extern "C" void Int32_Parse_m5477 ();
extern "C" void Int32_TryParse_m5478 ();
extern "C" void Int32_TryParse_m5366 ();
extern "C" void Int32_ToString_m3285 ();
extern "C" void Int32_ToString_m3337 ();
extern "C" void Int32_ToString_m5398 ();
extern "C" void Int32_ToString_m4404 ();
extern "C" void Int32_GetTypeCode_m5479 ();
extern "C" void SerializableAttribute__ctor_m5480 ();
extern "C" void AttributeUsageAttribute__ctor_m5481 ();
extern "C" void AttributeUsageAttribute_get_AllowMultiple_m5482 ();
extern "C" void AttributeUsageAttribute_set_AllowMultiple_m5483 ();
extern "C" void AttributeUsageAttribute_get_Inherited_m5484 ();
extern "C" void AttributeUsageAttribute_set_Inherited_m5485 ();
extern "C" void ComVisibleAttribute__ctor_m5486 ();
extern "C" void Int64_System_IConvertible_ToBoolean_m5487 ();
extern "C" void Int64_System_IConvertible_ToByte_m5488 ();
extern "C" void Int64_System_IConvertible_ToChar_m5489 ();
extern "C" void Int64_System_IConvertible_ToDateTime_m5490 ();
extern "C" void Int64_System_IConvertible_ToDecimal_m5491 ();
extern "C" void Int64_System_IConvertible_ToDouble_m5492 ();
extern "C" void Int64_System_IConvertible_ToInt16_m5493 ();
extern "C" void Int64_System_IConvertible_ToInt32_m5494 ();
extern "C" void Int64_System_IConvertible_ToInt64_m5495 ();
extern "C" void Int64_System_IConvertible_ToSByte_m5496 ();
extern "C" void Int64_System_IConvertible_ToSingle_m5497 ();
extern "C" void Int64_System_IConvertible_ToType_m5498 ();
extern "C" void Int64_System_IConvertible_ToUInt16_m5499 ();
extern "C" void Int64_System_IConvertible_ToUInt32_m5500 ();
extern "C" void Int64_System_IConvertible_ToUInt64_m5501 ();
extern "C" void Int64_CompareTo_m5502 ();
extern "C" void Int64_Equals_m5503 ();
extern "C" void Int64_GetHashCode_m5504 ();
extern "C" void Int64_CompareTo_m5505 ();
extern "C" void Int64_Equals_m5506 ();
extern "C" void Int64_Parse_m5507 ();
extern "C" void Int64_Parse_m5508 ();
extern "C" void Int64_Parse_m5509 ();
extern "C" void Int64_Parse_m5510 ();
extern "C" void Int64_Parse_m5511 ();
extern "C" void Int64_TryParse_m5512 ();
extern "C" void Int64_TryParse_m3334 ();
extern "C" void Int64_ToString_m5365 ();
extern "C" void Int64_ToString_m3335 ();
extern "C" void Int64_ToString_m5513 ();
extern "C" void Int64_ToString_m5514 ();
extern "C" void UInt32_System_IConvertible_ToBoolean_m5515 ();
extern "C" void UInt32_System_IConvertible_ToByte_m5516 ();
extern "C" void UInt32_System_IConvertible_ToChar_m5517 ();
extern "C" void UInt32_System_IConvertible_ToDateTime_m5518 ();
extern "C" void UInt32_System_IConvertible_ToDecimal_m5519 ();
extern "C" void UInt32_System_IConvertible_ToDouble_m5520 ();
extern "C" void UInt32_System_IConvertible_ToInt16_m5521 ();
extern "C" void UInt32_System_IConvertible_ToInt32_m5522 ();
extern "C" void UInt32_System_IConvertible_ToInt64_m5523 ();
extern "C" void UInt32_System_IConvertible_ToSByte_m5524 ();
extern "C" void UInt32_System_IConvertible_ToSingle_m5525 ();
extern "C" void UInt32_System_IConvertible_ToType_m5526 ();
extern "C" void UInt32_System_IConvertible_ToUInt16_m5527 ();
extern "C" void UInt32_System_IConvertible_ToUInt32_m5528 ();
extern "C" void UInt32_System_IConvertible_ToUInt64_m5529 ();
extern "C" void UInt32_CompareTo_m5530 ();
extern "C" void UInt32_Equals_m5531 ();
extern "C" void UInt32_GetHashCode_m5532 ();
extern "C" void UInt32_CompareTo_m5533 ();
extern "C" void UInt32_Equals_m5534 ();
extern "C" void UInt32_Parse_m5535 ();
extern "C" void UInt32_Parse_m5536 ();
extern "C" void UInt32_Parse_m5537 ();
extern "C" void UInt32_Parse_m5538 ();
extern "C" void UInt32_TryParse_m5420 ();
extern "C" void UInt32_TryParse_m3329 ();
extern "C" void UInt32_ToString_m3311 ();
extern "C" void UInt32_ToString_m3338 ();
extern "C" void UInt32_ToString_m5539 ();
extern "C" void UInt32_ToString_m5540 ();
extern "C" void CLSCompliantAttribute__ctor_m5541 ();
extern "C" void UInt64_System_IConvertible_ToBoolean_m5542 ();
extern "C" void UInt64_System_IConvertible_ToByte_m5543 ();
extern "C" void UInt64_System_IConvertible_ToChar_m5544 ();
extern "C" void UInt64_System_IConvertible_ToDateTime_m5545 ();
extern "C" void UInt64_System_IConvertible_ToDecimal_m5546 ();
extern "C" void UInt64_System_IConvertible_ToDouble_m5547 ();
extern "C" void UInt64_System_IConvertible_ToInt16_m5548 ();
extern "C" void UInt64_System_IConvertible_ToInt32_m5549 ();
extern "C" void UInt64_System_IConvertible_ToInt64_m5550 ();
extern "C" void UInt64_System_IConvertible_ToSByte_m5551 ();
extern "C" void UInt64_System_IConvertible_ToSingle_m5552 ();
extern "C" void UInt64_System_IConvertible_ToType_m5553 ();
extern "C" void UInt64_System_IConvertible_ToUInt16_m5554 ();
extern "C" void UInt64_System_IConvertible_ToUInt32_m5555 ();
extern "C" void UInt64_System_IConvertible_ToUInt64_m5556 ();
extern "C" void UInt64_CompareTo_m5557 ();
extern "C" void UInt64_Equals_m5558 ();
extern "C" void UInt64_GetHashCode_m5559 ();
extern "C" void UInt64_CompareTo_m5560 ();
extern "C" void UInt64_Equals_m5561 ();
extern "C" void UInt64_Parse_m5562 ();
extern "C" void UInt64_Parse_m5563 ();
extern "C" void UInt64_Parse_m5564 ();
extern "C" void UInt64_TryParse_m3309 ();
extern "C" void UInt64_ToString_m5565 ();
extern "C" void UInt64_ToString_m3336 ();
extern "C" void UInt64_ToString_m5566 ();
extern "C" void UInt64_ToString_m5567 ();
extern "C" void Byte_System_IConvertible_ToType_m5568 ();
extern "C" void Byte_System_IConvertible_ToBoolean_m5569 ();
extern "C" void Byte_System_IConvertible_ToByte_m5570 ();
extern "C" void Byte_System_IConvertible_ToChar_m5571 ();
extern "C" void Byte_System_IConvertible_ToDateTime_m5572 ();
extern "C" void Byte_System_IConvertible_ToDecimal_m5573 ();
extern "C" void Byte_System_IConvertible_ToDouble_m5574 ();
extern "C" void Byte_System_IConvertible_ToInt16_m5575 ();
extern "C" void Byte_System_IConvertible_ToInt32_m5576 ();
extern "C" void Byte_System_IConvertible_ToInt64_m5577 ();
extern "C" void Byte_System_IConvertible_ToSByte_m5578 ();
extern "C" void Byte_System_IConvertible_ToSingle_m5579 ();
extern "C" void Byte_System_IConvertible_ToUInt16_m5580 ();
extern "C" void Byte_System_IConvertible_ToUInt32_m5581 ();
extern "C" void Byte_System_IConvertible_ToUInt64_m5582 ();
extern "C" void Byte_CompareTo_m5583 ();
extern "C" void Byte_Equals_m5584 ();
extern "C" void Byte_GetHashCode_m5585 ();
extern "C" void Byte_CompareTo_m5586 ();
extern "C" void Byte_Equals_m5587 ();
extern "C" void Byte_Parse_m5588 ();
extern "C" void Byte_Parse_m5589 ();
extern "C" void Byte_Parse_m5590 ();
extern "C" void Byte_TryParse_m5591 ();
extern "C" void Byte_TryParse_m5592 ();
extern "C" void Byte_ToString_m4401 ();
extern "C" void Byte_ToString_m4341 ();
extern "C" void Byte_ToString_m4348 ();
extern "C" void Byte_ToString_m4352 ();
extern "C" void SByte_System_IConvertible_ToBoolean_m5593 ();
extern "C" void SByte_System_IConvertible_ToByte_m5594 ();
extern "C" void SByte_System_IConvertible_ToChar_m5595 ();
extern "C" void SByte_System_IConvertible_ToDateTime_m5596 ();
extern "C" void SByte_System_IConvertible_ToDecimal_m5597 ();
extern "C" void SByte_System_IConvertible_ToDouble_m5598 ();
extern "C" void SByte_System_IConvertible_ToInt16_m5599 ();
extern "C" void SByte_System_IConvertible_ToInt32_m5600 ();
extern "C" void SByte_System_IConvertible_ToInt64_m5601 ();
extern "C" void SByte_System_IConvertible_ToSByte_m5602 ();
extern "C" void SByte_System_IConvertible_ToSingle_m5603 ();
extern "C" void SByte_System_IConvertible_ToType_m5604 ();
extern "C" void SByte_System_IConvertible_ToUInt16_m5605 ();
extern "C" void SByte_System_IConvertible_ToUInt32_m5606 ();
extern "C" void SByte_System_IConvertible_ToUInt64_m5607 ();
extern "C" void SByte_CompareTo_m5608 ();
extern "C" void SByte_Equals_m5609 ();
extern "C" void SByte_GetHashCode_m5610 ();
extern "C" void SByte_CompareTo_m5611 ();
extern "C" void SByte_Equals_m5612 ();
extern "C" void SByte_Parse_m5613 ();
extern "C" void SByte_Parse_m5614 ();
extern "C" void SByte_Parse_m5615 ();
extern "C" void SByte_TryParse_m5616 ();
extern "C" void SByte_ToString_m5617 ();
extern "C" void SByte_ToString_m5618 ();
extern "C" void SByte_ToString_m5619 ();
extern "C" void SByte_ToString_m5620 ();
extern "C" void Int16_System_IConvertible_ToBoolean_m5621 ();
extern "C" void Int16_System_IConvertible_ToByte_m5622 ();
extern "C" void Int16_System_IConvertible_ToChar_m5623 ();
extern "C" void Int16_System_IConvertible_ToDateTime_m5624 ();
extern "C" void Int16_System_IConvertible_ToDecimal_m5625 ();
extern "C" void Int16_System_IConvertible_ToDouble_m5626 ();
extern "C" void Int16_System_IConvertible_ToInt16_m5627 ();
extern "C" void Int16_System_IConvertible_ToInt32_m5628 ();
extern "C" void Int16_System_IConvertible_ToInt64_m5629 ();
extern "C" void Int16_System_IConvertible_ToSByte_m5630 ();
extern "C" void Int16_System_IConvertible_ToSingle_m5631 ();
extern "C" void Int16_System_IConvertible_ToType_m5632 ();
extern "C" void Int16_System_IConvertible_ToUInt16_m5633 ();
extern "C" void Int16_System_IConvertible_ToUInt32_m5634 ();
extern "C" void Int16_System_IConvertible_ToUInt64_m5635 ();
extern "C" void Int16_CompareTo_m5636 ();
extern "C" void Int16_Equals_m5637 ();
extern "C" void Int16_GetHashCode_m5638 ();
extern "C" void Int16_CompareTo_m5639 ();
extern "C" void Int16_Equals_m5640 ();
extern "C" void Int16_Parse_m5641 ();
extern "C" void Int16_Parse_m5642 ();
extern "C" void Int16_Parse_m5643 ();
extern "C" void Int16_TryParse_m5644 ();
extern "C" void Int16_ToString_m5645 ();
extern "C" void Int16_ToString_m5646 ();
extern "C" void Int16_ToString_m5647 ();
extern "C" void Int16_ToString_m5648 ();
extern "C" void UInt16_System_IConvertible_ToBoolean_m5649 ();
extern "C" void UInt16_System_IConvertible_ToByte_m5650 ();
extern "C" void UInt16_System_IConvertible_ToChar_m5651 ();
extern "C" void UInt16_System_IConvertible_ToDateTime_m5652 ();
extern "C" void UInt16_System_IConvertible_ToDecimal_m5653 ();
extern "C" void UInt16_System_IConvertible_ToDouble_m5654 ();
extern "C" void UInt16_System_IConvertible_ToInt16_m5655 ();
extern "C" void UInt16_System_IConvertible_ToInt32_m5656 ();
extern "C" void UInt16_System_IConvertible_ToInt64_m5657 ();
extern "C" void UInt16_System_IConvertible_ToSByte_m5658 ();
extern "C" void UInt16_System_IConvertible_ToSingle_m5659 ();
extern "C" void UInt16_System_IConvertible_ToType_m5660 ();
extern "C" void UInt16_System_IConvertible_ToUInt16_m5661 ();
extern "C" void UInt16_System_IConvertible_ToUInt32_m5662 ();
extern "C" void UInt16_System_IConvertible_ToUInt64_m5663 ();
extern "C" void UInt16_CompareTo_m5664 ();
extern "C" void UInt16_Equals_m5665 ();
extern "C" void UInt16_GetHashCode_m5666 ();
extern "C" void UInt16_CompareTo_m5667 ();
extern "C" void UInt16_Equals_m5668 ();
extern "C" void UInt16_Parse_m5669 ();
extern "C" void UInt16_Parse_m5670 ();
extern "C" void UInt16_TryParse_m5671 ();
extern "C" void UInt16_TryParse_m5672 ();
extern "C" void UInt16_ToString_m5673 ();
extern "C" void UInt16_ToString_m5674 ();
extern "C" void UInt16_ToString_m5675 ();
extern "C" void UInt16_ToString_m5676 ();
extern "C" void Char__cctor_m5677 ();
extern "C" void Char_System_IConvertible_ToType_m5678 ();
extern "C" void Char_System_IConvertible_ToBoolean_m5679 ();
extern "C" void Char_System_IConvertible_ToByte_m5680 ();
extern "C" void Char_System_IConvertible_ToChar_m5681 ();
extern "C" void Char_System_IConvertible_ToDateTime_m5682 ();
extern "C" void Char_System_IConvertible_ToDecimal_m5683 ();
extern "C" void Char_System_IConvertible_ToDouble_m5684 ();
extern "C" void Char_System_IConvertible_ToInt16_m5685 ();
extern "C" void Char_System_IConvertible_ToInt32_m5686 ();
extern "C" void Char_System_IConvertible_ToInt64_m5687 ();
extern "C" void Char_System_IConvertible_ToSByte_m5688 ();
extern "C" void Char_System_IConvertible_ToSingle_m5689 ();
extern "C" void Char_System_IConvertible_ToUInt16_m5690 ();
extern "C" void Char_System_IConvertible_ToUInt32_m5691 ();
extern "C" void Char_System_IConvertible_ToUInt64_m5692 ();
extern "C" void Char_GetDataTablePointers_m5693 ();
extern "C" void Char_CompareTo_m5694 ();
extern "C" void Char_Equals_m5695 ();
extern "C" void Char_CompareTo_m5696 ();
extern "C" void Char_Equals_m5697 ();
extern "C" void Char_GetHashCode_m5698 ();
extern "C" void Char_GetUnicodeCategory_m5408 ();
extern "C" void Char_IsDigit_m5406 ();
extern "C" void Char_IsLetter_m1822 ();
extern "C" void Char_IsLetterOrDigit_m5405 ();
extern "C" void Char_IsLower_m1823 ();
extern "C" void Char_IsSurrogate_m5699 ();
extern "C" void Char_IsUpper_m1825 ();
extern "C" void Char_IsWhiteSpace_m5407 ();
extern "C" void Char_IsWhiteSpace_m5380 ();
extern "C" void Char_CheckParameter_m5700 ();
extern "C" void Char_Parse_m5701 ();
extern "C" void Char_ToLower_m1826 ();
extern "C" void Char_ToLowerInvariant_m5702 ();
extern "C" void Char_ToLower_m5703 ();
extern "C" void Char_ToUpper_m1824 ();
extern "C" void Char_ToUpperInvariant_m5382 ();
extern "C" void Char_ToString_m1787 ();
extern "C" void Char_ToString_m5704 ();
extern "C" void Char_GetTypeCode_m5705 ();
extern "C" void String__ctor_m5706 ();
extern "C" void String__ctor_m5707 ();
extern "C" void String__ctor_m5708 ();
extern "C" void String__ctor_m5709 ();
extern "C" void String__cctor_m5710 ();
extern "C" void String_System_IConvertible_ToBoolean_m5711 ();
extern "C" void String_System_IConvertible_ToByte_m5712 ();
extern "C" void String_System_IConvertible_ToChar_m5713 ();
extern "C" void String_System_IConvertible_ToDateTime_m5714 ();
extern "C" void String_System_IConvertible_ToDecimal_m5715 ();
extern "C" void String_System_IConvertible_ToDouble_m5716 ();
extern "C" void String_System_IConvertible_ToInt16_m5717 ();
extern "C" void String_System_IConvertible_ToInt32_m5718 ();
extern "C" void String_System_IConvertible_ToInt64_m5719 ();
extern "C" void String_System_IConvertible_ToSByte_m5720 ();
extern "C" void String_System_IConvertible_ToSingle_m5721 ();
extern "C" void String_System_IConvertible_ToType_m5722 ();
extern "C" void String_System_IConvertible_ToUInt16_m5723 ();
extern "C" void String_System_IConvertible_ToUInt32_m5724 ();
extern "C" void String_System_IConvertible_ToUInt64_m5725 ();
extern "C" void String_System_Collections_Generic_IEnumerableU3CcharU3E_GetEnumerator_m5726 ();
extern "C" void String_System_Collections_IEnumerable_GetEnumerator_m5727 ();
extern "C" void String_Equals_m5728 ();
extern "C" void String_Equals_m5729 ();
extern "C" void String_Equals_m4369 ();
extern "C" void String_get_Chars_m1763 ();
extern "C" void String_CopyTo_m5730 ();
extern "C" void String_ToCharArray_m3325 ();
extern "C" void String_ToCharArray_m5731 ();
extern "C" void String_Split_m219 ();
extern "C" void String_Split_m5732 ();
extern "C" void String_Split_m5733 ();
extern "C" void String_Split_m5734 ();
extern "C" void String_Split_m5383 ();
extern "C" void String_Substring_m1790 ();
extern "C" void String_Substring_m1764 ();
extern "C" void String_SubstringUnchecked_m5735 ();
extern "C" void String_Trim_m3269 ();
extern "C" void String_Trim_m3270 ();
extern "C" void String_TrimStart_m5422 ();
extern "C" void String_TrimEnd_m5381 ();
extern "C" void String_FindNotWhiteSpace_m5736 ();
extern "C" void String_FindNotInTable_m5737 ();
extern "C" void String_Compare_m5738 ();
extern "C" void String_Compare_m5739 ();
extern "C" void String_Compare_m4459 ();
extern "C" void String_Compare_m4460 ();
extern "C" void String_CompareTo_m5740 ();
extern "C" void String_CompareTo_m5741 ();
extern "C" void String_CompareOrdinal_m5418 ();
extern "C" void String_CompareOrdinalUnchecked_m5742 ();
extern "C" void String_CompareOrdinalCaseInsensitiveUnchecked_m5743 ();
extern "C" void String_EndsWith_m3371 ();
extern "C" void String_IndexOfAny_m5417 ();
extern "C" void String_IndexOfAny_m1782 ();
extern "C" void String_IndexOfAny_m4375 ();
extern "C" void String_IndexOfAnyUnchecked_m5744 ();
extern "C" void String_IndexOf_m3267 ();
extern "C" void String_IndexOf_m5745 ();
extern "C" void String_IndexOfOrdinal_m5746 ();
extern "C" void String_IndexOfOrdinalUnchecked_m5747 ();
extern "C" void String_IndexOfOrdinalIgnoreCaseUnchecked_m5748 ();
extern "C" void String_IndexOf_m1827 ();
extern "C" void String_IndexOf_m3268 ();
extern "C" void String_IndexOf_m5423 ();
extern "C" void String_IndexOfUnchecked_m5749 ();
extern "C" void String_IndexOf_m3281 ();
extern "C" void String_IndexOf_m3372 ();
extern "C" void String_IndexOf_m5750 ();
extern "C" void String_LastIndexOfAny_m5751 ();
extern "C" void String_LastIndexOfAny_m1784 ();
extern "C" void String_LastIndexOfAnyUnchecked_m5752 ();
extern "C" void String_LastIndexOf_m5367 ();
extern "C" void String_LastIndexOf_m5419 ();
extern "C" void String_LastIndexOf_m5424 ();
extern "C" void String_LastIndexOfUnchecked_m5753 ();
extern "C" void String_LastIndexOf_m3375 ();
extern "C" void String_LastIndexOf_m5754 ();
extern "C" void String_Contains_m1821 ();
extern "C" void String_IsNullOrEmpty_m1792 ();
extern "C" void String_PadRight_m5755 ();
extern "C" void String_StartsWith_m3280 ();
extern "C" void String_Replace_m3374 ();
extern "C" void String_Replace_m3373 ();
extern "C" void String_ReplaceUnchecked_m5756 ();
extern "C" void String_ReplaceFallback_m5757 ();
extern "C" void String_Remove_m1786 ();
extern "C" void String_ToLower_m5411 ();
extern "C" void String_ToLower_m5421 ();
extern "C" void String_ToLowerInvariant_m5758 ();
extern "C" void String_ToUpper_m3282 ();
extern "C" void String_ToUpper_m5759 ();
extern "C" void String_ToUpperInvariant_m5760 ();
extern "C" void String_ToString_m256 ();
extern "C" void String_ToString_m5761 ();
extern "C" void String_Format_m1610 ();
extern "C" void String_Format_m5762 ();
extern "C" void String_Format_m5763 ();
extern "C" void String_Format_m1918 ();
extern "C" void String_Format_m4414 ();
extern "C" void String_FormatHelper_m5764 ();
extern "C" void String_Concat_m214 ();
extern "C" void String_Concat_m239 ();
extern "C" void String_Concat_m217 ();
extern "C" void String_Concat_m1723 ();
extern "C" void String_Concat_m221 ();
extern "C" void String_Concat_m252 ();
extern "C" void String_Concat_m222 ();
extern "C" void String_ConcatInternal_m5765 ();
extern "C" void String_Insert_m1788 ();
extern "C" void String_Join_m5766 ();
extern "C" void String_Join_m5767 ();
extern "C" void String_JoinUnchecked_m5768 ();
extern "C" void String_get_Length_m1739 ();
extern "C" void String_ParseFormatSpecifier_m5769 ();
extern "C" void String_ParseDecimal_m5770 ();
extern "C" void String_InternalSetChar_m5771 ();
extern "C" void String_InternalSetLength_m5772 ();
extern "C" void String_GetHashCode_m3247 ();
extern "C" void String_GetCaseInsensitiveHashCode_m5773 ();
extern "C" void String_CreateString_m5774 ();
extern "C" void String_CreateString_m5775 ();
extern "C" void String_CreateString_m5776 ();
extern "C" void String_CreateString_m5777 ();
extern "C" void String_CreateString_m5778 ();
extern "C" void String_CreateString_m3327 ();
extern "C" void String_CreateString_m3332 ();
extern "C" void String_CreateString_m1791 ();
extern "C" void String_memcpy4_m5779 ();
extern "C" void String_memcpy2_m5780 ();
extern "C" void String_memcpy1_m5781 ();
extern "C" void String_memcpy_m5782 ();
extern "C" void String_CharCopy_m5783 ();
extern "C" void String_CharCopyReverse_m5784 ();
extern "C" void String_CharCopy_m5785 ();
extern "C" void String_CharCopy_m5786 ();
extern "C" void String_CharCopyReverse_m5787 ();
extern "C" void String_InternalSplit_m5788 ();
extern "C" void String_InternalAllocateStr_m5789 ();
extern "C" void String_op_Equality_m204 ();
extern "C" void String_op_Inequality_m1762 ();
extern "C" void Single_System_IConvertible_ToBoolean_m5790 ();
extern "C" void Single_System_IConvertible_ToByte_m5791 ();
extern "C" void Single_System_IConvertible_ToChar_m5792 ();
extern "C" void Single_System_IConvertible_ToDateTime_m5793 ();
extern "C" void Single_System_IConvertible_ToDecimal_m5794 ();
extern "C" void Single_System_IConvertible_ToDouble_m5795 ();
extern "C" void Single_System_IConvertible_ToInt16_m5796 ();
extern "C" void Single_System_IConvertible_ToInt32_m5797 ();
extern "C" void Single_System_IConvertible_ToInt64_m5798 ();
extern "C" void Single_System_IConvertible_ToSByte_m5799 ();
extern "C" void Single_System_IConvertible_ToSingle_m5800 ();
extern "C" void Single_System_IConvertible_ToType_m5801 ();
extern "C" void Single_System_IConvertible_ToUInt16_m5802 ();
extern "C" void Single_System_IConvertible_ToUInt32_m5803 ();
extern "C" void Single_System_IConvertible_ToUInt64_m5804 ();
extern "C" void Single_CompareTo_m5805 ();
extern "C" void Single_Equals_m5806 ();
extern "C" void Single_CompareTo_m1475 ();
extern "C" void Single_Equals_m3262 ();
extern "C" void Single_GetHashCode_m3245 ();
extern "C" void Single_IsInfinity_m5807 ();
extern "C" void Single_IsNaN_m5808 ();
extern "C" void Single_IsNegativeInfinity_m5809 ();
extern "C" void Single_IsPositiveInfinity_m5810 ();
extern "C" void Single_Parse_m243 ();
extern "C" void Single_Parse_m5811 ();
extern "C" void Single_ToString_m5812 ();
extern "C" void Single_ToString_m3340 ();
extern "C" void Single_ToString_m3263 ();
extern "C" void Single_ToString_m5813 ();
extern "C" void Single_GetTypeCode_m5814 ();
extern "C" void Double_System_IConvertible_ToType_m5815 ();
extern "C" void Double_System_IConvertible_ToBoolean_m5816 ();
extern "C" void Double_System_IConvertible_ToByte_m5817 ();
extern "C" void Double_System_IConvertible_ToChar_m5818 ();
extern "C" void Double_System_IConvertible_ToDateTime_m5819 ();
extern "C" void Double_System_IConvertible_ToDecimal_m5820 ();
extern "C" void Double_System_IConvertible_ToDouble_m5821 ();
extern "C" void Double_System_IConvertible_ToInt16_m5822 ();
extern "C" void Double_System_IConvertible_ToInt32_m5823 ();
extern "C" void Double_System_IConvertible_ToInt64_m5824 ();
extern "C" void Double_System_IConvertible_ToSByte_m5825 ();
extern "C" void Double_System_IConvertible_ToSingle_m5826 ();
extern "C" void Double_System_IConvertible_ToUInt16_m5827 ();
extern "C" void Double_System_IConvertible_ToUInt32_m5828 ();
extern "C" void Double_System_IConvertible_ToUInt64_m5829 ();
extern "C" void Double_CompareTo_m5830 ();
extern "C" void Double_Equals_m5831 ();
extern "C" void Double_CompareTo_m5832 ();
extern "C" void Double_Equals_m5833 ();
extern "C" void Double_GetHashCode_m5834 ();
extern "C" void Double_IsInfinity_m5835 ();
extern "C" void Double_IsNaN_m5836 ();
extern "C" void Double_IsNegativeInfinity_m5837 ();
extern "C" void Double_IsPositiveInfinity_m5838 ();
extern "C" void Double_Parse_m5839 ();
extern "C" void Double_Parse_m5840 ();
extern "C" void Double_Parse_m5841 ();
extern "C" void Double_Parse_m5842 ();
extern "C" void Double_TryParseStringConstant_m5843 ();
extern "C" void Double_ParseImpl_m5844 ();
extern "C" void Double_TryParse_m3333 ();
extern "C" void Double_ToString_m5845 ();
extern "C" void Double_ToString_m5846 ();
extern "C" void Double_ToString_m3342 ();
extern "C" void Decimal__ctor_m5847 ();
extern "C" void Decimal__ctor_m5848 ();
extern "C" void Decimal__ctor_m5849 ();
extern "C" void Decimal__ctor_m5850 ();
extern "C" void Decimal__ctor_m5851 ();
extern "C" void Decimal__ctor_m5852 ();
extern "C" void Decimal__ctor_m5853 ();
extern "C" void Decimal__cctor_m5854 ();
extern "C" void Decimal_System_IConvertible_ToType_m5855 ();
extern "C" void Decimal_System_IConvertible_ToBoolean_m5856 ();
extern "C" void Decimal_System_IConvertible_ToByte_m5857 ();
extern "C" void Decimal_System_IConvertible_ToChar_m5858 ();
extern "C" void Decimal_System_IConvertible_ToDateTime_m5859 ();
extern "C" void Decimal_System_IConvertible_ToDecimal_m5860 ();
extern "C" void Decimal_System_IConvertible_ToDouble_m5861 ();
extern "C" void Decimal_System_IConvertible_ToInt16_m5862 ();
extern "C" void Decimal_System_IConvertible_ToInt32_m5863 ();
extern "C" void Decimal_System_IConvertible_ToInt64_m5864 ();
extern "C" void Decimal_System_IConvertible_ToSByte_m5865 ();
extern "C" void Decimal_System_IConvertible_ToSingle_m5866 ();
extern "C" void Decimal_System_IConvertible_ToUInt16_m5867 ();
extern "C" void Decimal_System_IConvertible_ToUInt32_m5868 ();
extern "C" void Decimal_System_IConvertible_ToUInt64_m5869 ();
extern "C" void Decimal_GetBits_m5870 ();
extern "C" void Decimal_Add_m5871 ();
extern "C" void Decimal_Subtract_m5872 ();
extern "C" void Decimal_GetHashCode_m5873 ();
extern "C" void Decimal_u64_m5874 ();
extern "C" void Decimal_s64_m5875 ();
extern "C" void Decimal_Equals_m5876 ();
extern "C" void Decimal_Equals_m5877 ();
extern "C" void Decimal_IsZero_m5878 ();
extern "C" void Decimal_Floor_m5879 ();
extern "C" void Decimal_Multiply_m5880 ();
extern "C" void Decimal_Divide_m5881 ();
extern "C" void Decimal_Compare_m5882 ();
extern "C" void Decimal_CompareTo_m5883 ();
extern "C" void Decimal_CompareTo_m5884 ();
extern "C" void Decimal_Equals_m5885 ();
extern "C" void Decimal_Parse_m5886 ();
extern "C" void Decimal_ThrowAtPos_m5887 ();
extern "C" void Decimal_ThrowInvalidExp_m5888 ();
extern "C" void Decimal_stripStyles_m5889 ();
extern "C" void Decimal_Parse_m5890 ();
extern "C" void Decimal_PerformParse_m5891 ();
extern "C" void Decimal_ToString_m5892 ();
extern "C" void Decimal_ToString_m5893 ();
extern "C" void Decimal_ToString_m3339 ();
extern "C" void Decimal_decimal2UInt64_m5894 ();
extern "C" void Decimal_decimal2Int64_m5895 ();
extern "C" void Decimal_decimalIncr_m5896 ();
extern "C" void Decimal_string2decimal_m5897 ();
extern "C" void Decimal_decimalSetExponent_m5898 ();
extern "C" void Decimal_decimal2double_m5899 ();
extern "C" void Decimal_decimalFloorAndTrunc_m5900 ();
extern "C" void Decimal_decimalMult_m5901 ();
extern "C" void Decimal_decimalDiv_m5902 ();
extern "C" void Decimal_decimalCompare_m5903 ();
extern "C" void Decimal_op_Increment_m5904 ();
extern "C" void Decimal_op_Subtraction_m5905 ();
extern "C" void Decimal_op_Multiply_m5906 ();
extern "C" void Decimal_op_Division_m5907 ();
extern "C" void Decimal_op_Explicit_m5908 ();
extern "C" void Decimal_op_Explicit_m5909 ();
extern "C" void Decimal_op_Explicit_m5910 ();
extern "C" void Decimal_op_Explicit_m5911 ();
extern "C" void Decimal_op_Explicit_m5912 ();
extern "C" void Decimal_op_Explicit_m5913 ();
extern "C" void Decimal_op_Explicit_m5914 ();
extern "C" void Decimal_op_Explicit_m5915 ();
extern "C" void Decimal_op_Implicit_m5916 ();
extern "C" void Decimal_op_Implicit_m5917 ();
extern "C" void Decimal_op_Implicit_m5918 ();
extern "C" void Decimal_op_Implicit_m5919 ();
extern "C" void Decimal_op_Implicit_m5920 ();
extern "C" void Decimal_op_Implicit_m5921 ();
extern "C" void Decimal_op_Implicit_m5922 ();
extern "C" void Decimal_op_Implicit_m5923 ();
extern "C" void Decimal_op_Explicit_m5924 ();
extern "C" void Decimal_op_Explicit_m5925 ();
extern "C" void Decimal_op_Explicit_m5926 ();
extern "C" void Decimal_op_Explicit_m5927 ();
extern "C" void Decimal_op_Inequality_m5928 ();
extern "C" void Decimal_op_Equality_m5929 ();
extern "C" void Decimal_op_GreaterThan_m5930 ();
extern "C" void Decimal_op_LessThan_m5931 ();
extern "C" void Boolean__cctor_m5932 ();
extern "C" void Boolean_System_IConvertible_ToType_m5933 ();
extern "C" void Boolean_System_IConvertible_ToBoolean_m5934 ();
extern "C" void Boolean_System_IConvertible_ToByte_m5935 ();
extern "C" void Boolean_System_IConvertible_ToChar_m5936 ();
extern "C" void Boolean_System_IConvertible_ToDateTime_m5937 ();
extern "C" void Boolean_System_IConvertible_ToDecimal_m5938 ();
extern "C" void Boolean_System_IConvertible_ToDouble_m5939 ();
extern "C" void Boolean_System_IConvertible_ToInt16_m5940 ();
extern "C" void Boolean_System_IConvertible_ToInt32_m5941 ();
extern "C" void Boolean_System_IConvertible_ToInt64_m5942 ();
extern "C" void Boolean_System_IConvertible_ToSByte_m5943 ();
extern "C" void Boolean_System_IConvertible_ToSingle_m5944 ();
extern "C" void Boolean_System_IConvertible_ToUInt16_m5945 ();
extern "C" void Boolean_System_IConvertible_ToUInt32_m5946 ();
extern "C" void Boolean_System_IConvertible_ToUInt64_m5947 ();
extern "C" void Boolean_CompareTo_m5948 ();
extern "C" void Boolean_Equals_m5949 ();
extern "C" void Boolean_CompareTo_m5950 ();
extern "C" void Boolean_Equals_m5951 ();
extern "C" void Boolean_GetHashCode_m5952 ();
extern "C" void Boolean_Parse_m5953 ();
extern "C" void Boolean_ToString_m3312 ();
extern "C" void Boolean_GetTypeCode_m5954 ();
extern "C" void Boolean_ToString_m5955 ();
extern "C" void IntPtr__ctor_m3287 ();
extern "C" void IntPtr__ctor_m5956 ();
extern "C" void IntPtr__ctor_m5957 ();
extern "C" void IntPtr__ctor_m5958 ();
extern "C" void IntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m5959 ();
extern "C" void IntPtr_get_Size_m5960 ();
extern "C" void IntPtr_Equals_m5961 ();
extern "C" void IntPtr_GetHashCode_m5962 ();
extern "C" void IntPtr_ToInt64_m5963 ();
extern "C" void IntPtr_ToString_m5964 ();
extern "C" void IntPtr_ToString_m5965 ();
extern "C" void IntPtr_op_Equality_m3380 ();
extern "C" void IntPtr_op_Inequality_m3288 ();
extern "C" void IntPtr_op_Explicit_m5966 ();
extern "C" void IntPtr_op_Explicit_m5967 ();
extern "C" void IntPtr_op_Explicit_m5968 ();
extern "C" void IntPtr_op_Explicit_m3379 ();
extern "C" void IntPtr_op_Explicit_m5969 ();
extern "C" void UIntPtr__ctor_m5970 ();
extern "C" void UIntPtr__ctor_m5971 ();
extern "C" void UIntPtr__ctor_m5972 ();
extern "C" void UIntPtr__cctor_m5973 ();
extern "C" void UIntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m5974 ();
extern "C" void UIntPtr_Equals_m5975 ();
extern "C" void UIntPtr_GetHashCode_m5976 ();
extern "C" void UIntPtr_ToUInt32_m5977 ();
extern "C" void UIntPtr_ToUInt64_m5978 ();
extern "C" void UIntPtr_ToPointer_m5979 ();
extern "C" void UIntPtr_ToString_m5980 ();
extern "C" void UIntPtr_get_Size_m5981 ();
extern "C" void UIntPtr_op_Equality_m5982 ();
extern "C" void UIntPtr_op_Inequality_m5983 ();
extern "C" void UIntPtr_op_Explicit_m5984 ();
extern "C" void UIntPtr_op_Explicit_m5985 ();
extern "C" void UIntPtr_op_Explicit_m5986 ();
extern "C" void UIntPtr_op_Explicit_m5987 ();
extern "C" void UIntPtr_op_Explicit_m5988 ();
extern "C" void UIntPtr_op_Explicit_m5989 ();
extern "C" void MulticastDelegate_GetObjectData_m5990 ();
extern "C" void MulticastDelegate_Equals_m5991 ();
extern "C" void MulticastDelegate_GetHashCode_m5992 ();
extern "C" void MulticastDelegate_GetInvocationList_m5993 ();
extern "C" void MulticastDelegate_CombineImpl_m5994 ();
extern "C" void MulticastDelegate_BaseEquals_m5995 ();
extern "C" void MulticastDelegate_KPM_m5996 ();
extern "C" void MulticastDelegate_RemoveImpl_m5997 ();
extern "C" void Delegate_get_Method_m3382 ();
extern "C" void Delegate_get_Target_m3383 ();
extern "C" void Delegate_CreateDelegate_internal_m5998 ();
extern "C" void Delegate_SetMulticastInvoke_m5999 ();
extern "C" void Delegate_arg_type_match_m6000 ();
extern "C" void Delegate_return_type_match_m6001 ();
extern "C" void Delegate_CreateDelegate_m6002 ();
extern "C" void Delegate_CreateDelegate_m3384 ();
extern "C" void Delegate_CreateDelegate_m6003 ();
extern "C" void Delegate_CreateDelegate_m6004 ();
extern "C" void Delegate_GetCandidateMethod_m6005 ();
extern "C" void Delegate_CreateDelegate_m6006 ();
extern "C" void Delegate_CreateDelegate_m6007 ();
extern "C" void Delegate_CreateDelegate_m6008 ();
extern "C" void Delegate_CreateDelegate_m6009 ();
extern "C" void Delegate_Clone_m6010 ();
extern "C" void Delegate_Equals_m6011 ();
extern "C" void Delegate_GetHashCode_m6012 ();
extern "C" void Delegate_GetObjectData_m6013 ();
extern "C" void Delegate_GetInvocationList_m6014 ();
extern "C" void Delegate_Combine_m1656 ();
extern "C" void Delegate_Combine_m6015 ();
extern "C" void Delegate_CombineImpl_m6016 ();
extern "C" void Delegate_Remove_m1657 ();
extern "C" void Delegate_RemoveImpl_m6017 ();
extern "C" void Enum__ctor_m6018 ();
extern "C" void Enum__cctor_m6019 ();
extern "C" void Enum_System_IConvertible_ToBoolean_m6020 ();
extern "C" void Enum_System_IConvertible_ToByte_m6021 ();
extern "C" void Enum_System_IConvertible_ToChar_m6022 ();
extern "C" void Enum_System_IConvertible_ToDateTime_m6023 ();
extern "C" void Enum_System_IConvertible_ToDecimal_m6024 ();
extern "C" void Enum_System_IConvertible_ToDouble_m6025 ();
extern "C" void Enum_System_IConvertible_ToInt16_m6026 ();
extern "C" void Enum_System_IConvertible_ToInt32_m6027 ();
extern "C" void Enum_System_IConvertible_ToInt64_m6028 ();
extern "C" void Enum_System_IConvertible_ToSByte_m6029 ();
extern "C" void Enum_System_IConvertible_ToSingle_m6030 ();
extern "C" void Enum_System_IConvertible_ToType_m6031 ();
extern "C" void Enum_System_IConvertible_ToUInt16_m6032 ();
extern "C" void Enum_System_IConvertible_ToUInt32_m6033 ();
extern "C" void Enum_System_IConvertible_ToUInt64_m6034 ();
extern "C" void Enum_GetTypeCode_m6035 ();
extern "C" void Enum_get_value_m6036 ();
extern "C" void Enum_get_Value_m6037 ();
extern "C" void Enum_FindPosition_m6038 ();
extern "C" void Enum_GetName_m6039 ();
extern "C" void Enum_IsDefined_m4438 ();
extern "C" void Enum_get_underlying_type_m6040 ();
extern "C" void Enum_GetUnderlyingType_m6041 ();
extern "C" void Enum_FindName_m6042 ();
extern "C" void Enum_GetValue_m6043 ();
extern "C" void Enum_Parse_m5404 ();
extern "C" void Enum_compare_value_to_m6044 ();
extern "C" void Enum_CompareTo_m6045 ();
extern "C" void Enum_ToString_m6046 ();
extern "C" void Enum_ToString_m6047 ();
extern "C" void Enum_ToString_m3296 ();
extern "C" void Enum_ToString_m6048 ();
extern "C" void Enum_ToObject_m6049 ();
extern "C" void Enum_ToObject_m6050 ();
extern "C" void Enum_ToObject_m6051 ();
extern "C" void Enum_ToObject_m6052 ();
extern "C" void Enum_ToObject_m6053 ();
extern "C" void Enum_ToObject_m6054 ();
extern "C" void Enum_ToObject_m6055 ();
extern "C" void Enum_ToObject_m6056 ();
extern "C" void Enum_ToObject_m6057 ();
extern "C" void Enum_Equals_m6058 ();
extern "C" void Enum_get_hashcode_m6059 ();
extern "C" void Enum_GetHashCode_m6060 ();
extern "C" void Enum_FormatSpecifier_X_m6061 ();
extern "C" void Enum_FormatFlags_m6062 ();
extern "C" void Enum_Format_m6063 ();
extern "C" void SimpleEnumerator__ctor_m6064 ();
extern "C" void SimpleEnumerator_get_Current_m6065 ();
extern "C" void SimpleEnumerator_MoveNext_m6066 ();
extern "C" void Swapper__ctor_m6067 ();
extern "C" void Swapper_Invoke_m6068 ();
extern "C" void Swapper_BeginInvoke_m6069 ();
extern "C" void Swapper_EndInvoke_m6070 ();
extern "C" void Array__ctor_m6071 ();
extern "C" void Array_System_Collections_IList_get_Item_m6072 ();
extern "C" void Array_System_Collections_IList_set_Item_m6073 ();
extern "C" void Array_System_Collections_IList_Add_m6074 ();
extern "C" void Array_System_Collections_IList_Clear_m6075 ();
extern "C" void Array_System_Collections_IList_Contains_m6076 ();
extern "C" void Array_System_Collections_IList_IndexOf_m6077 ();
extern "C" void Array_System_Collections_IList_Insert_m6078 ();
extern "C" void Array_System_Collections_IList_Remove_m6079 ();
extern "C" void Array_System_Collections_IList_RemoveAt_m6080 ();
extern "C" void Array_System_Collections_ICollection_get_Count_m6081 ();
extern "C" void Array_InternalArray__ICollection_get_Count_m6082 ();
extern "C" void Array_InternalArray__ICollection_get_IsReadOnly_m6083 ();
extern "C" void Array_InternalArray__ICollection_Clear_m6084 ();
extern "C" void Array_InternalArray__RemoveAt_m6085 ();
extern "C" void Array_get_Length_m5347 ();
extern "C" void Array_get_LongLength_m6086 ();
extern "C" void Array_get_Rank_m5350 ();
extern "C" void Array_GetRank_m6087 ();
extern "C" void Array_GetLength_m6088 ();
extern "C" void Array_GetLongLength_m6089 ();
extern "C" void Array_GetLowerBound_m6090 ();
extern "C" void Array_GetValue_m6091 ();
extern "C" void Array_SetValue_m6092 ();
extern "C" void Array_GetValueImpl_m6093 ();
extern "C" void Array_SetValueImpl_m6094 ();
extern "C" void Array_FastCopy_m6095 ();
extern "C" void Array_CreateInstanceImpl_m6096 ();
extern "C" void Array_get_IsSynchronized_m6097 ();
extern "C" void Array_get_SyncRoot_m6098 ();
extern "C" void Array_get_IsFixedSize_m6099 ();
extern "C" void Array_get_IsReadOnly_m6100 ();
extern "C" void Array_GetEnumerator_m6101 ();
extern "C" void Array_GetUpperBound_m6102 ();
extern "C" void Array_GetValue_m6103 ();
extern "C" void Array_GetValue_m6104 ();
extern "C" void Array_GetValue_m6105 ();
extern "C" void Array_GetValue_m6106 ();
extern "C" void Array_GetValue_m6107 ();
extern "C" void Array_GetValue_m6108 ();
extern "C" void Array_SetValue_m6109 ();
extern "C" void Array_SetValue_m6110 ();
extern "C" void Array_SetValue_m6111 ();
extern "C" void Array_SetValue_m5348 ();
extern "C" void Array_SetValue_m6112 ();
extern "C" void Array_SetValue_m6113 ();
extern "C" void Array_CreateInstance_m6114 ();
extern "C" void Array_CreateInstance_m6115 ();
extern "C" void Array_CreateInstance_m6116 ();
extern "C" void Array_CreateInstance_m6117 ();
extern "C" void Array_CreateInstance_m6118 ();
extern "C" void Array_GetIntArray_m6119 ();
extern "C" void Array_CreateInstance_m6120 ();
extern "C" void Array_GetValue_m6121 ();
extern "C" void Array_SetValue_m6122 ();
extern "C" void Array_BinarySearch_m6123 ();
extern "C" void Array_BinarySearch_m6124 ();
extern "C" void Array_BinarySearch_m6125 ();
extern "C" void Array_BinarySearch_m6126 ();
extern "C" void Array_DoBinarySearch_m6127 ();
extern "C" void Array_Clear_m3452 ();
extern "C" void Array_ClearInternal_m6128 ();
extern "C" void Array_Clone_m6129 ();
extern "C" void Array_Copy_m5412 ();
extern "C" void Array_Copy_m6130 ();
extern "C" void Array_Copy_m6131 ();
extern "C" void Array_Copy_m6132 ();
extern "C" void Array_IndexOf_m6133 ();
extern "C" void Array_IndexOf_m6134 ();
extern "C" void Array_IndexOf_m6135 ();
extern "C" void Array_Initialize_m6136 ();
extern "C" void Array_LastIndexOf_m6137 ();
extern "C" void Array_LastIndexOf_m6138 ();
extern "C" void Array_LastIndexOf_m6139 ();
extern "C" void Array_get_swapper_m6140 ();
extern "C" void Array_Reverse_m4346 ();
extern "C" void Array_Reverse_m4377 ();
extern "C" void Array_Sort_m6141 ();
extern "C" void Array_Sort_m6142 ();
extern "C" void Array_Sort_m6143 ();
extern "C" void Array_Sort_m6144 ();
extern "C" void Array_Sort_m6145 ();
extern "C" void Array_Sort_m6146 ();
extern "C" void Array_Sort_m6147 ();
extern "C" void Array_Sort_m6148 ();
extern "C" void Array_int_swapper_m6149 ();
extern "C" void Array_obj_swapper_m6150 ();
extern "C" void Array_slow_swapper_m6151 ();
extern "C" void Array_double_swapper_m6152 ();
extern "C" void Array_new_gap_m6153 ();
extern "C" void Array_combsort_m6154 ();
extern "C" void Array_combsort_m6155 ();
extern "C" void Array_combsort_m6156 ();
extern "C" void Array_qsort_m6157 ();
extern "C" void Array_swap_m6158 ();
extern "C" void Array_compare_m6159 ();
extern "C" void Array_CopyTo_m6160 ();
extern "C" void Array_CopyTo_m6161 ();
extern "C" void Array_ConstrainedCopy_m6162 ();
extern "C" void Type__ctor_m6163 ();
extern "C" void Type__cctor_m6164 ();
extern "C" void Type_FilterName_impl_m6165 ();
extern "C" void Type_FilterNameIgnoreCase_impl_m6166 ();
extern "C" void Type_FilterAttribute_impl_m6167 ();
extern "C" void Type_get_Attributes_m6168 ();
extern "C" void Type_get_DeclaringType_m6169 ();
extern "C" void Type_get_HasElementType_m6170 ();
extern "C" void Type_get_IsAbstract_m6171 ();
extern "C" void Type_get_IsArray_m6172 ();
extern "C" void Type_get_IsByRef_m6173 ();
extern "C" void Type_get_IsClass_m6174 ();
extern "C" void Type_get_IsContextful_m6175 ();
extern "C" void Type_get_IsEnum_m6176 ();
extern "C" void Type_get_IsExplicitLayout_m6177 ();
extern "C" void Type_get_IsInterface_m6178 ();
extern "C" void Type_get_IsMarshalByRef_m6179 ();
extern "C" void Type_get_IsPointer_m6180 ();
extern "C" void Type_get_IsPrimitive_m6181 ();
extern "C" void Type_get_IsSealed_m6182 ();
extern "C" void Type_get_IsSerializable_m6183 ();
extern "C" void Type_get_IsValueType_m6184 ();
extern "C" void Type_get_MemberType_m6185 ();
extern "C" void Type_get_ReflectedType_m6186 ();
extern "C" void Type_get_TypeHandle_m6187 ();
extern "C" void Type_Equals_m6188 ();
extern "C" void Type_Equals_m6189 ();
extern "C" void Type_EqualsInternal_m6190 ();
extern "C" void Type_internal_from_handle_m6191 ();
extern "C" void Type_internal_from_name_m6192 ();
extern "C" void Type_GetType_m6193 ();
extern "C" void Type_GetType_m3389 ();
extern "C" void Type_GetTypeCodeInternal_m6194 ();
extern "C" void Type_GetTypeCode_m6195 ();
extern "C" void Type_GetTypeFromHandle_m1629 ();
extern "C" void Type_GetTypeHandle_m6196 ();
extern "C" void Type_type_is_subtype_of_m6197 ();
extern "C" void Type_type_is_assignable_from_m6198 ();
extern "C" void Type_IsSubclassOf_m6199 ();
extern "C" void Type_IsAssignableFrom_m6200 ();
extern "C" void Type_IsInstanceOfType_m6201 ();
extern "C" void Type_GetHashCode_m6202 ();
extern "C" void Type_GetMethod_m6203 ();
extern "C" void Type_GetMethod_m6204 ();
extern "C" void Type_GetMethod_m6205 ();
extern "C" void Type_GetMethod_m6206 ();
extern "C" void Type_GetProperty_m6207 ();
extern "C" void Type_GetProperty_m6208 ();
extern "C" void Type_GetProperty_m6209 ();
extern "C" void Type_GetProperty_m6210 ();
extern "C" void Type_IsArrayImpl_m6211 ();
extern "C" void Type_IsValueTypeImpl_m6212 ();
extern "C" void Type_IsContextfulImpl_m6213 ();
extern "C" void Type_IsMarshalByRefImpl_m6214 ();
extern "C" void Type_GetConstructor_m6215 ();
extern "C" void Type_GetConstructor_m6216 ();
extern "C" void Type_GetConstructor_m6217 ();
extern "C" void Type_GetConstructors_m6218 ();
extern "C" void Type_ToString_m6219 ();
extern "C" void Type_get_IsSystemType_m6220 ();
extern "C" void Type_GetGenericArguments_m6221 ();
extern "C" void Type_get_ContainsGenericParameters_m6222 ();
extern "C" void Type_get_IsGenericTypeDefinition_m6223 ();
extern "C" void Type_GetGenericTypeDefinition_impl_m6224 ();
extern "C" void Type_GetGenericTypeDefinition_m6225 ();
extern "C" void Type_get_IsGenericType_m6226 ();
extern "C" void Type_MakeGenericType_m6227 ();
extern "C" void Type_MakeGenericType_m6228 ();
extern "C" void Type_get_IsGenericParameter_m6229 ();
extern "C" void Type_get_IsNested_m6230 ();
extern "C" void Type_GetPseudoCustomAttributes_m6231 ();
extern "C" void MemberInfo__ctor_m6232 ();
extern "C" void MemberInfo_get_Module_m6233 ();
extern "C" void Exception__ctor_m4336 ();
extern "C" void Exception__ctor_m3289 ();
extern "C" void Exception__ctor_m3378 ();
extern "C" void Exception__ctor_m3377 ();
extern "C" void Exception_get_InnerException_m6234 ();
extern "C" void Exception_set_HResult_m3376 ();
extern "C" void Exception_get_ClassName_m6235 ();
extern "C" void Exception_get_Message_m6236 ();
extern "C" void Exception_get_Source_m6237 ();
extern "C" void Exception_get_StackTrace_m6238 ();
extern "C" void Exception_GetObjectData_m5428 ();
extern "C" void Exception_ToString_m6239 ();
extern "C" void Exception_GetFullNameForStackTrace_m6240 ();
extern "C" void Exception_GetType_m6241 ();
extern "C" void RuntimeFieldHandle__ctor_m6242 ();
extern "C" void RuntimeFieldHandle_get_Value_m6243 ();
extern "C" void RuntimeFieldHandle_GetObjectData_m6244 ();
extern "C" void RuntimeFieldHandle_Equals_m6245 ();
extern "C" void RuntimeFieldHandle_GetHashCode_m6246 ();
extern "C" void RuntimeTypeHandle__ctor_m6247 ();
extern "C" void RuntimeTypeHandle_get_Value_m6248 ();
extern "C" void RuntimeTypeHandle_GetObjectData_m6249 ();
extern "C" void RuntimeTypeHandle_Equals_m6250 ();
extern "C" void RuntimeTypeHandle_GetHashCode_m6251 ();
extern "C" void ParamArrayAttribute__ctor_m6252 ();
extern "C" void OutAttribute__ctor_m6253 ();
extern "C" void ObsoleteAttribute__ctor_m6254 ();
extern "C" void ObsoleteAttribute__ctor_m6255 ();
extern "C" void ObsoleteAttribute__ctor_m6256 ();
extern "C" void DllImportAttribute__ctor_m6257 ();
extern "C" void DllImportAttribute_get_Value_m6258 ();
extern "C" void MarshalAsAttribute__ctor_m6259 ();
extern "C" void InAttribute__ctor_m6260 ();
extern "C" void ConditionalAttribute__ctor_m6261 ();
extern "C" void GuidAttribute__ctor_m6262 ();
extern "C" void ComImportAttribute__ctor_m6263 ();
extern "C" void OptionalAttribute__ctor_m6264 ();
extern "C" void CompilerGeneratedAttribute__ctor_m6265 ();
extern "C" void InternalsVisibleToAttribute__ctor_m6266 ();
extern "C" void RuntimeCompatibilityAttribute__ctor_m6267 ();
extern "C" void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m6268 ();
extern "C" void DebuggerHiddenAttribute__ctor_m6269 ();
extern "C" void DefaultMemberAttribute__ctor_m6270 ();
extern "C" void DefaultMemberAttribute_get_MemberName_m6271 ();
extern "C" void DecimalConstantAttribute__ctor_m6272 ();
extern "C" void FieldOffsetAttribute__ctor_m6273 ();
extern "C" void AsyncCallback__ctor_m4437 ();
extern "C" void AsyncCallback_Invoke_m6274 ();
extern "C" void AsyncCallback_BeginInvoke_m4435 ();
extern "C" void AsyncCallback_EndInvoke_m6275 ();
extern "C" void TypedReference_Equals_m6276 ();
extern "C" void TypedReference_GetHashCode_m6277 ();
extern "C" void ArgIterator_Equals_m6278 ();
extern "C" void ArgIterator_GetHashCode_m6279 ();
extern "C" void MarshalByRefObject__ctor_m5376 ();
extern "C" void MarshalByRefObject_get_ObjectIdentity_m6280 ();
extern "C" void RuntimeHelpers_InitializeArray_m6281 ();
extern "C" void RuntimeHelpers_InitializeArray_m3466 ();
extern "C" void RuntimeHelpers_get_OffsetToStringData_m6282 ();
extern "C" void Locale_GetText_m6283 ();
extern "C" void Locale_GetText_m6284 ();
extern "C" void MonoTODOAttribute__ctor_m6285 ();
extern "C" void MonoTODOAttribute__ctor_m6286 ();
extern "C" void MonoDocumentationNoteAttribute__ctor_m6287 ();
extern "C" void SafeHandleZeroOrMinusOneIsInvalid__ctor_m6288 ();
extern "C" void SafeHandleZeroOrMinusOneIsInvalid_get_IsInvalid_m6289 ();
extern "C" void SafeWaitHandle__ctor_m6290 ();
extern "C" void SafeWaitHandle_ReleaseHandle_m6291 ();
extern "C" void TableRange__ctor_m6292 ();
extern "C" void CodePointIndexer__ctor_m6293 ();
extern "C" void CodePointIndexer_ToIndex_m6294 ();
extern "C" void TailoringInfo__ctor_m6295 ();
extern "C" void Contraction__ctor_m6296 ();
extern "C" void ContractionComparer__ctor_m6297 ();
extern "C" void ContractionComparer__cctor_m6298 ();
extern "C" void ContractionComparer_Compare_m6299 ();
extern "C" void Level2Map__ctor_m6300 ();
extern "C" void Level2MapComparer__ctor_m6301 ();
extern "C" void Level2MapComparer__cctor_m6302 ();
extern "C" void Level2MapComparer_Compare_m6303 ();
extern "C" void MSCompatUnicodeTable__cctor_m6304 ();
extern "C" void MSCompatUnicodeTable_GetTailoringInfo_m6305 ();
extern "C" void MSCompatUnicodeTable_BuildTailoringTables_m6306 ();
extern "C" void MSCompatUnicodeTable_SetCJKReferences_m6307 ();
extern "C" void MSCompatUnicodeTable_Category_m6308 ();
extern "C" void MSCompatUnicodeTable_Level1_m6309 ();
extern "C" void MSCompatUnicodeTable_Level2_m6310 ();
extern "C" void MSCompatUnicodeTable_Level3_m6311 ();
extern "C" void MSCompatUnicodeTable_IsIgnorable_m6312 ();
extern "C" void MSCompatUnicodeTable_IsIgnorableNonSpacing_m6313 ();
extern "C" void MSCompatUnicodeTable_ToKanaTypeInsensitive_m6314 ();
extern "C" void MSCompatUnicodeTable_ToWidthCompat_m6315 ();
extern "C" void MSCompatUnicodeTable_HasSpecialWeight_m6316 ();
extern "C" void MSCompatUnicodeTable_IsHalfWidthKana_m6317 ();
extern "C" void MSCompatUnicodeTable_IsHiragana_m6318 ();
extern "C" void MSCompatUnicodeTable_IsJapaneseSmallLetter_m6319 ();
extern "C" void MSCompatUnicodeTable_get_IsReady_m6320 ();
extern "C" void MSCompatUnicodeTable_GetResource_m6321 ();
extern "C" void MSCompatUnicodeTable_UInt32FromBytePtr_m6322 ();
extern "C" void MSCompatUnicodeTable_FillCJK_m6323 ();
extern "C" void MSCompatUnicodeTable_FillCJKCore_m6324 ();
extern "C" void MSCompatUnicodeTableUtil__cctor_m6325 ();
extern "C" void Context__ctor_m6326 ();
extern "C" void PreviousInfo__ctor_m6327 ();
extern "C" void SimpleCollator__ctor_m6328 ();
extern "C" void SimpleCollator__cctor_m6329 ();
extern "C" void SimpleCollator_SetCJKTable_m6330 ();
extern "C" void SimpleCollator_GetNeutralCulture_m6331 ();
extern "C" void SimpleCollator_Category_m6332 ();
extern "C" void SimpleCollator_Level1_m6333 ();
extern "C" void SimpleCollator_Level2_m6334 ();
extern "C" void SimpleCollator_IsHalfKana_m6335 ();
extern "C" void SimpleCollator_GetContraction_m6336 ();
extern "C" void SimpleCollator_GetContraction_m6337 ();
extern "C" void SimpleCollator_GetTailContraction_m6338 ();
extern "C" void SimpleCollator_GetTailContraction_m6339 ();
extern "C" void SimpleCollator_FilterOptions_m6340 ();
extern "C" void SimpleCollator_GetExtenderType_m6341 ();
extern "C" void SimpleCollator_ToDashTypeValue_m6342 ();
extern "C" void SimpleCollator_FilterExtender_m6343 ();
extern "C" void SimpleCollator_IsIgnorable_m6344 ();
extern "C" void SimpleCollator_IsSafe_m6345 ();
extern "C" void SimpleCollator_GetSortKey_m6346 ();
extern "C" void SimpleCollator_GetSortKey_m6347 ();
extern "C" void SimpleCollator_GetSortKey_m6348 ();
extern "C" void SimpleCollator_FillSortKeyRaw_m6349 ();
extern "C" void SimpleCollator_FillSurrogateSortKeyRaw_m6350 ();
extern "C" void SimpleCollator_CompareOrdinal_m6351 ();
extern "C" void SimpleCollator_CompareQuick_m6352 ();
extern "C" void SimpleCollator_CompareOrdinalIgnoreCase_m6353 ();
extern "C" void SimpleCollator_Compare_m6354 ();
extern "C" void SimpleCollator_ClearBuffer_m6355 ();
extern "C" void SimpleCollator_QuickCheckPossible_m6356 ();
extern "C" void SimpleCollator_CompareInternal_m6357 ();
extern "C" void SimpleCollator_CompareFlagPair_m6358 ();
extern "C" void SimpleCollator_IsPrefix_m6359 ();
extern "C" void SimpleCollator_IsPrefix_m6360 ();
extern "C" void SimpleCollator_IsPrefix_m6361 ();
extern "C" void SimpleCollator_IsSuffix_m6362 ();
extern "C" void SimpleCollator_IsSuffix_m6363 ();
extern "C" void SimpleCollator_QuickIndexOf_m6364 ();
extern "C" void SimpleCollator_IndexOf_m6365 ();
extern "C" void SimpleCollator_IndexOfOrdinal_m6366 ();
extern "C" void SimpleCollator_IndexOfOrdinalIgnoreCase_m6367 ();
extern "C" void SimpleCollator_IndexOfSortKey_m6368 ();
extern "C" void SimpleCollator_IndexOf_m6369 ();
extern "C" void SimpleCollator_LastIndexOf_m6370 ();
extern "C" void SimpleCollator_LastIndexOfOrdinal_m6371 ();
extern "C" void SimpleCollator_LastIndexOfOrdinalIgnoreCase_m6372 ();
extern "C" void SimpleCollator_LastIndexOfSortKey_m6373 ();
extern "C" void SimpleCollator_LastIndexOf_m6374 ();
extern "C" void SimpleCollator_MatchesForward_m6375 ();
extern "C" void SimpleCollator_MatchesForwardCore_m6376 ();
extern "C" void SimpleCollator_MatchesPrimitive_m6377 ();
extern "C" void SimpleCollator_MatchesBackward_m6378 ();
extern "C" void SimpleCollator_MatchesBackwardCore_m6379 ();
extern "C" void SortKey__ctor_m6380 ();
extern "C" void SortKey__ctor_m6381 ();
extern "C" void SortKey_Compare_m6382 ();
extern "C" void SortKey_get_OriginalString_m6383 ();
extern "C" void SortKey_get_KeyData_m6384 ();
extern "C" void SortKey_Equals_m6385 ();
extern "C" void SortKey_GetHashCode_m6386 ();
extern "C" void SortKey_ToString_m6387 ();
extern "C" void SortKeyBuffer__ctor_m6388 ();
extern "C" void SortKeyBuffer_Reset_m6389 ();
extern "C" void SortKeyBuffer_Initialize_m6390 ();
extern "C" void SortKeyBuffer_AppendCJKExtension_m6391 ();
extern "C" void SortKeyBuffer_AppendKana_m6392 ();
extern "C" void SortKeyBuffer_AppendNormal_m6393 ();
extern "C" void SortKeyBuffer_AppendLevel5_m6394 ();
extern "C" void SortKeyBuffer_AppendBufferPrimitive_m6395 ();
extern "C" void SortKeyBuffer_GetResultAndReset_m6396 ();
extern "C" void SortKeyBuffer_GetOptimizedLength_m6397 ();
extern "C" void SortKeyBuffer_GetResult_m6398 ();
extern "C" void PrimeGeneratorBase__ctor_m6399 ();
extern "C" void PrimeGeneratorBase_get_Confidence_m6400 ();
extern "C" void PrimeGeneratorBase_get_PrimalityTest_m6401 ();
extern "C" void PrimeGeneratorBase_get_TrialDivisionBounds_m6402 ();
extern "C" void SequentialSearchPrimeGeneratorBase__ctor_m6403 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m6404 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m6405 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m6406 ();
extern "C" void SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m6407 ();
extern "C" void PrimalityTests_GetSPPRounds_m6408 ();
extern "C" void PrimalityTests_Test_m6409 ();
extern "C" void PrimalityTests_RabinMillerTest_m6410 ();
extern "C" void PrimalityTests_SmallPrimeSppTest_m6411 ();
extern "C" void ModulusRing__ctor_m6412 ();
extern "C" void ModulusRing_BarrettReduction_m6413 ();
extern "C" void ModulusRing_Multiply_m6414 ();
extern "C" void ModulusRing_Difference_m6415 ();
extern "C" void ModulusRing_Pow_m6416 ();
extern "C" void ModulusRing_Pow_m6417 ();
extern "C" void Kernel_AddSameSign_m6418 ();
extern "C" void Kernel_Subtract_m6419 ();
extern "C" void Kernel_MinusEq_m6420 ();
extern "C" void Kernel_PlusEq_m6421 ();
extern "C" void Kernel_Compare_m6422 ();
extern "C" void Kernel_SingleByteDivideInPlace_m6423 ();
extern "C" void Kernel_DwordMod_m6424 ();
extern "C" void Kernel_DwordDivMod_m6425 ();
extern "C" void Kernel_multiByteDivide_m6426 ();
extern "C" void Kernel_LeftShift_m6427 ();
extern "C" void Kernel_RightShift_m6428 ();
extern "C" void Kernel_MultiplyByDword_m6429 ();
extern "C" void Kernel_Multiply_m6430 ();
extern "C" void Kernel_MultiplyMod2p32pmod_m6431 ();
extern "C" void Kernel_modInverse_m6432 ();
extern "C" void Kernel_modInverse_m6433 ();
extern "C" void BigInteger__ctor_m6434 ();
extern "C" void BigInteger__ctor_m6435 ();
extern "C" void BigInteger__ctor_m6436 ();
extern "C" void BigInteger__ctor_m6437 ();
extern "C" void BigInteger__ctor_m6438 ();
extern "C" void BigInteger__cctor_m6439 ();
extern "C" void BigInteger_get_Rng_m6440 ();
extern "C" void BigInteger_GenerateRandom_m6441 ();
extern "C" void BigInteger_GenerateRandom_m6442 ();
extern "C" void BigInteger_Randomize_m6443 ();
extern "C" void BigInteger_Randomize_m6444 ();
extern "C" void BigInteger_BitCount_m6445 ();
extern "C" void BigInteger_TestBit_m6446 ();
extern "C" void BigInteger_TestBit_m6447 ();
extern "C" void BigInteger_SetBit_m6448 ();
extern "C" void BigInteger_SetBit_m6449 ();
extern "C" void BigInteger_LowestSetBit_m6450 ();
extern "C" void BigInteger_GetBytes_m6451 ();
extern "C" void BigInteger_ToString_m6452 ();
extern "C" void BigInteger_ToString_m6453 ();
extern "C" void BigInteger_Normalize_m6454 ();
extern "C" void BigInteger_Clear_m6455 ();
extern "C" void BigInteger_GetHashCode_m6456 ();
extern "C" void BigInteger_ToString_m6457 ();
extern "C" void BigInteger_Equals_m6458 ();
extern "C" void BigInteger_ModInverse_m6459 ();
extern "C" void BigInteger_ModPow_m6460 ();
extern "C" void BigInteger_IsProbablePrime_m6461 ();
extern "C" void BigInteger_GeneratePseudoPrime_m6462 ();
extern "C" void BigInteger_Incr2_m6463 ();
extern "C" void BigInteger_op_Implicit_m6464 ();
extern "C" void BigInteger_op_Implicit_m6465 ();
extern "C" void BigInteger_op_Addition_m6466 ();
extern "C" void BigInteger_op_Subtraction_m6467 ();
extern "C" void BigInteger_op_Modulus_m6468 ();
extern "C" void BigInteger_op_Modulus_m6469 ();
extern "C" void BigInteger_op_Division_m6470 ();
extern "C" void BigInteger_op_Multiply_m6471 ();
extern "C" void BigInteger_op_Multiply_m6472 ();
extern "C" void BigInteger_op_LeftShift_m6473 ();
extern "C" void BigInteger_op_RightShift_m6474 ();
extern "C" void BigInteger_op_Equality_m6475 ();
extern "C" void BigInteger_op_Inequality_m6476 ();
extern "C" void BigInteger_op_Equality_m6477 ();
extern "C" void BigInteger_op_Inequality_m6478 ();
extern "C" void BigInteger_op_GreaterThan_m6479 ();
extern "C" void BigInteger_op_LessThan_m6480 ();
extern "C" void BigInteger_op_GreaterThanOrEqual_m6481 ();
extern "C" void BigInteger_op_LessThanOrEqual_m6482 ();
extern "C" void CryptoConvert_ToInt32LE_m6483 ();
extern "C" void CryptoConvert_ToUInt32LE_m6484 ();
extern "C" void CryptoConvert_GetBytesLE_m6485 ();
extern "C" void CryptoConvert_ToCapiPrivateKeyBlob_m6486 ();
extern "C" void CryptoConvert_FromCapiPublicKeyBlob_m6487 ();
extern "C" void CryptoConvert_FromCapiPublicKeyBlob_m6488 ();
extern "C" void CryptoConvert_ToCapiPublicKeyBlob_m6489 ();
extern "C" void CryptoConvert_ToCapiKeyBlob_m6490 ();
extern "C" void KeyBuilder_get_Rng_m6491 ();
extern "C" void KeyBuilder_Key_m6492 ();
extern "C" void KeyBuilder_IV_m6493 ();
extern "C" void BlockProcessor__ctor_m6494 ();
extern "C" void BlockProcessor_Finalize_m6495 ();
extern "C" void BlockProcessor_Initialize_m6496 ();
extern "C" void BlockProcessor_Core_m6497 ();
extern "C" void BlockProcessor_Core_m6498 ();
extern "C" void BlockProcessor_Final_m6499 ();
extern "C" void KeyGeneratedEventHandler__ctor_m6500 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m6501 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m6502 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m6503 ();
extern "C" void DSAManaged__ctor_m6504 ();
extern "C" void DSAManaged_add_KeyGenerated_m6505 ();
extern "C" void DSAManaged_remove_KeyGenerated_m6506 ();
extern "C" void DSAManaged_Finalize_m6507 ();
extern "C" void DSAManaged_Generate_m6508 ();
extern "C" void DSAManaged_GenerateKeyPair_m6509 ();
extern "C" void DSAManaged_add_m6510 ();
extern "C" void DSAManaged_GenerateParams_m6511 ();
extern "C" void DSAManaged_get_Random_m6512 ();
extern "C" void DSAManaged_get_KeySize_m6513 ();
extern "C" void DSAManaged_get_PublicOnly_m6514 ();
extern "C" void DSAManaged_NormalizeArray_m6515 ();
extern "C" void DSAManaged_ExportParameters_m6516 ();
extern "C" void DSAManaged_ImportParameters_m6517 ();
extern "C" void DSAManaged_CreateSignature_m6518 ();
extern "C" void DSAManaged_VerifySignature_m6519 ();
extern "C" void DSAManaged_Dispose_m6520 ();
extern "C" void KeyPairPersistence__ctor_m6521 ();
extern "C" void KeyPairPersistence__ctor_m6522 ();
extern "C" void KeyPairPersistence__cctor_m6523 ();
extern "C" void KeyPairPersistence_get_Filename_m6524 ();
extern "C" void KeyPairPersistence_get_KeyValue_m6525 ();
extern "C" void KeyPairPersistence_set_KeyValue_m6526 ();
extern "C" void KeyPairPersistence_Load_m6527 ();
extern "C" void KeyPairPersistence_Save_m6528 ();
extern "C" void KeyPairPersistence_Remove_m6529 ();
extern "C" void KeyPairPersistence_get_UserPath_m6530 ();
extern "C" void KeyPairPersistence_get_MachinePath_m6531 ();
extern "C" void KeyPairPersistence__CanSecure_m6532 ();
extern "C" void KeyPairPersistence__ProtectUser_m6533 ();
extern "C" void KeyPairPersistence__ProtectMachine_m6534 ();
extern "C" void KeyPairPersistence__IsUserProtected_m6535 ();
extern "C" void KeyPairPersistence__IsMachineProtected_m6536 ();
extern "C" void KeyPairPersistence_CanSecure_m6537 ();
extern "C" void KeyPairPersistence_ProtectUser_m6538 ();
extern "C" void KeyPairPersistence_ProtectMachine_m6539 ();
extern "C" void KeyPairPersistence_IsUserProtected_m6540 ();
extern "C" void KeyPairPersistence_IsMachineProtected_m6541 ();
extern "C" void KeyPairPersistence_get_CanChange_m6542 ();
extern "C" void KeyPairPersistence_get_UseDefaultKeyContainer_m6543 ();
extern "C" void KeyPairPersistence_get_UseMachineKeyStore_m6544 ();
extern "C" void KeyPairPersistence_get_ContainerName_m6545 ();
extern "C" void KeyPairPersistence_Copy_m6546 ();
extern "C" void KeyPairPersistence_FromXml_m6547 ();
extern "C" void KeyPairPersistence_ToXml_m6548 ();
extern "C" void MACAlgorithm__ctor_m6549 ();
extern "C" void MACAlgorithm_Initialize_m6550 ();
extern "C" void MACAlgorithm_Core_m6551 ();
extern "C" void MACAlgorithm_Final_m6552 ();
extern "C" void PKCS1__cctor_m6553 ();
extern "C" void PKCS1_Compare_m6554 ();
extern "C" void PKCS1_I2OSP_m6555 ();
extern "C" void PKCS1_OS2IP_m6556 ();
extern "C" void PKCS1_RSAEP_m6557 ();
extern "C" void PKCS1_RSASP1_m6558 ();
extern "C" void PKCS1_RSAVP1_m6559 ();
extern "C" void PKCS1_Encrypt_v15_m6560 ();
extern "C" void PKCS1_Sign_v15_m6561 ();
extern "C" void PKCS1_Verify_v15_m6562 ();
extern "C" void PKCS1_Verify_v15_m6563 ();
extern "C" void PKCS1_Encode_v15_m6564 ();
extern "C" void PrivateKeyInfo__ctor_m6565 ();
extern "C" void PrivateKeyInfo__ctor_m6566 ();
extern "C" void PrivateKeyInfo_get_PrivateKey_m6567 ();
extern "C" void PrivateKeyInfo_Decode_m6568 ();
extern "C" void PrivateKeyInfo_RemoveLeadingZero_m6569 ();
extern "C" void PrivateKeyInfo_Normalize_m6570 ();
extern "C" void PrivateKeyInfo_DecodeRSA_m6571 ();
extern "C" void PrivateKeyInfo_DecodeDSA_m6572 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m6573 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m6574 ();
extern "C" void EncryptedPrivateKeyInfo_get_Algorithm_m6575 ();
extern "C" void EncryptedPrivateKeyInfo_get_EncryptedData_m6576 ();
extern "C" void EncryptedPrivateKeyInfo_get_Salt_m6577 ();
extern "C" void EncryptedPrivateKeyInfo_get_IterationCount_m6578 ();
extern "C" void EncryptedPrivateKeyInfo_Decode_m6579 ();
extern "C" void KeyGeneratedEventHandler__ctor_m6580 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m6581 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m6582 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m6583 ();
extern "C" void RSAManaged__ctor_m6584 ();
extern "C" void RSAManaged_add_KeyGenerated_m6585 ();
extern "C" void RSAManaged_remove_KeyGenerated_m6586 ();
extern "C" void RSAManaged_Finalize_m6587 ();
extern "C" void RSAManaged_GenerateKeyPair_m6588 ();
extern "C" void RSAManaged_get_KeySize_m6589 ();
extern "C" void RSAManaged_get_PublicOnly_m6590 ();
extern "C" void RSAManaged_DecryptValue_m6591 ();
extern "C" void RSAManaged_EncryptValue_m6592 ();
extern "C" void RSAManaged_ExportParameters_m6593 ();
extern "C" void RSAManaged_ImportParameters_m6594 ();
extern "C" void RSAManaged_Dispose_m6595 ();
extern "C" void RSAManaged_ToXmlString_m6596 ();
extern "C" void RSAManaged_get_IsCrtPossible_m6597 ();
extern "C" void RSAManaged_GetPaddedValue_m6598 ();
extern "C" void SymmetricTransform__ctor_m6599 ();
extern "C" void SymmetricTransform_System_IDisposable_Dispose_m6600 ();
extern "C" void SymmetricTransform_Finalize_m6601 ();
extern "C" void SymmetricTransform_Dispose_m6602 ();
extern "C" void SymmetricTransform_get_CanReuseTransform_m6603 ();
extern "C" void SymmetricTransform_Transform_m6604 ();
extern "C" void SymmetricTransform_CBC_m6605 ();
extern "C" void SymmetricTransform_CFB_m6606 ();
extern "C" void SymmetricTransform_OFB_m6607 ();
extern "C" void SymmetricTransform_CTS_m6608 ();
extern "C" void SymmetricTransform_CheckInput_m6609 ();
extern "C" void SymmetricTransform_TransformBlock_m6610 ();
extern "C" void SymmetricTransform_get_KeepLastBlock_m6611 ();
extern "C" void SymmetricTransform_InternalTransformBlock_m6612 ();
extern "C" void SymmetricTransform_Random_m6613 ();
extern "C" void SymmetricTransform_ThrowBadPaddingException_m6614 ();
extern "C" void SymmetricTransform_FinalEncrypt_m6615 ();
extern "C" void SymmetricTransform_FinalDecrypt_m6616 ();
extern "C" void SymmetricTransform_TransformFinalBlock_m6617 ();
extern "C" void SafeBag__ctor_m6618 ();
extern "C" void SafeBag_get_BagOID_m6619 ();
extern "C" void SafeBag_get_ASN1_m6620 ();
extern "C" void DeriveBytes__ctor_m6621 ();
extern "C" void DeriveBytes__cctor_m6622 ();
extern "C" void DeriveBytes_set_HashName_m6623 ();
extern "C" void DeriveBytes_set_IterationCount_m6624 ();
extern "C" void DeriveBytes_set_Password_m6625 ();
extern "C" void DeriveBytes_set_Salt_m6626 ();
extern "C" void DeriveBytes_Adjust_m6627 ();
extern "C" void DeriveBytes_Derive_m6628 ();
extern "C" void DeriveBytes_DeriveKey_m6629 ();
extern "C" void DeriveBytes_DeriveIV_m6630 ();
extern "C" void DeriveBytes_DeriveMAC_m6631 ();
extern "C" void PKCS12__ctor_m6632 ();
extern "C" void PKCS12__ctor_m6633 ();
extern "C" void PKCS12__ctor_m6634 ();
extern "C" void PKCS12__cctor_m6635 ();
extern "C" void PKCS12_Decode_m6636 ();
extern "C" void PKCS12_Finalize_m6637 ();
extern "C" void PKCS12_set_Password_m6638 ();
extern "C" void PKCS12_get_Certificates_m6639 ();
extern "C" void PKCS12_Compare_m6640 ();
extern "C" void PKCS12_GetSymmetricAlgorithm_m6641 ();
extern "C" void PKCS12_Decrypt_m6642 ();
extern "C" void PKCS12_Decrypt_m6643 ();
extern "C" void PKCS12_GetExistingParameters_m6644 ();
extern "C" void PKCS12_AddPrivateKey_m6645 ();
extern "C" void PKCS12_ReadSafeBag_m6646 ();
extern "C" void PKCS12_MAC_m6647 ();
extern "C" void PKCS12_get_MaximumPasswordLength_m6648 ();
extern "C" void X501__cctor_m6649 ();
extern "C" void X501_ToString_m6650 ();
extern "C" void X501_ToString_m6651 ();
extern "C" void X501_AppendEntry_m6652 ();
extern "C" void X509Certificate__ctor_m6653 ();
extern "C" void X509Certificate__cctor_m6654 ();
extern "C" void X509Certificate_Parse_m6655 ();
extern "C" void X509Certificate_GetUnsignedBigInteger_m6656 ();
extern "C" void X509Certificate_get_DSA_m6657 ();
extern "C" void X509Certificate_get_IssuerName_m6658 ();
extern "C" void X509Certificate_get_KeyAlgorithmParameters_m6659 ();
extern "C" void X509Certificate_get_PublicKey_m6660 ();
extern "C" void X509Certificate_get_RawData_m6661 ();
extern "C" void X509Certificate_get_SubjectName_m6662 ();
extern "C" void X509Certificate_get_ValidFrom_m6663 ();
extern "C" void X509Certificate_get_ValidUntil_m6664 ();
extern "C" void X509Certificate_GetIssuerName_m6665 ();
extern "C" void X509Certificate_GetSubjectName_m6666 ();
extern "C" void X509Certificate_GetObjectData_m6667 ();
extern "C" void X509Certificate_PEM_m6668 ();
extern "C" void X509CertificateEnumerator__ctor_m6669 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m6670 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m6671 ();
extern "C" void X509CertificateEnumerator_get_Current_m6672 ();
extern "C" void X509CertificateEnumerator_MoveNext_m6673 ();
extern "C" void X509CertificateCollection__ctor_m6674 ();
extern "C" void X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m6675 ();
extern "C" void X509CertificateCollection_get_Item_m6676 ();
extern "C" void X509CertificateCollection_Add_m6677 ();
extern "C" void X509CertificateCollection_GetEnumerator_m6678 ();
extern "C" void X509CertificateCollection_GetHashCode_m6679 ();
extern "C" void X509Extension__ctor_m6680 ();
extern "C" void X509Extension_Decode_m6681 ();
extern "C" void X509Extension_Equals_m6682 ();
extern "C" void X509Extension_GetHashCode_m6683 ();
extern "C" void X509Extension_WriteLine_m6684 ();
extern "C" void X509Extension_ToString_m6685 ();
extern "C" void X509ExtensionCollection__ctor_m6686 ();
extern "C" void X509ExtensionCollection__ctor_m6687 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m6688 ();
extern "C" void ASN1__ctor_m6689 ();
extern "C" void ASN1__ctor_m6690 ();
extern "C" void ASN1__ctor_m6691 ();
extern "C" void ASN1_get_Count_m6692 ();
extern "C" void ASN1_get_Tag_m6693 ();
extern "C" void ASN1_get_Length_m6694 ();
extern "C" void ASN1_get_Value_m6695 ();
extern "C" void ASN1_set_Value_m6696 ();
extern "C" void ASN1_CompareArray_m6697 ();
extern "C" void ASN1_CompareValue_m6698 ();
extern "C" void ASN1_Add_m6699 ();
extern "C" void ASN1_GetBytes_m6700 ();
extern "C" void ASN1_Decode_m6701 ();
extern "C" void ASN1_DecodeTLV_m6702 ();
extern "C" void ASN1_get_Item_m6703 ();
extern "C" void ASN1_Element_m6704 ();
extern "C" void ASN1_ToString_m6705 ();
extern "C" void ASN1Convert_ToInt32_m6706 ();
extern "C" void ASN1Convert_ToOid_m6707 ();
extern "C" void ASN1Convert_ToDateTime_m6708 ();
extern "C" void BitConverterLE_GetUIntBytes_m6709 ();
extern "C" void BitConverterLE_GetBytes_m6710 ();
extern "C" void BitConverterLE_UShortFromBytes_m6711 ();
extern "C" void BitConverterLE_UIntFromBytes_m6712 ();
extern "C" void BitConverterLE_ULongFromBytes_m6713 ();
extern "C" void BitConverterLE_ToInt16_m6714 ();
extern "C" void BitConverterLE_ToInt32_m6715 ();
extern "C" void BitConverterLE_ToSingle_m6716 ();
extern "C" void BitConverterLE_ToDouble_m6717 ();
extern "C" void ContentInfo__ctor_m6718 ();
extern "C" void ContentInfo__ctor_m6719 ();
extern "C" void ContentInfo__ctor_m6720 ();
extern "C" void ContentInfo__ctor_m6721 ();
extern "C" void ContentInfo_get_Content_m6722 ();
extern "C" void ContentInfo_set_Content_m6723 ();
extern "C" void ContentInfo_get_ContentType_m6724 ();
extern "C" void EncryptedData__ctor_m6725 ();
extern "C" void EncryptedData__ctor_m6726 ();
extern "C" void EncryptedData_get_EncryptionAlgorithm_m6727 ();
extern "C" void EncryptedData_get_EncryptedContent_m6728 ();
extern "C" void StrongName__cctor_m6729 ();
extern "C" void StrongName_get_PublicKey_m6730 ();
extern "C" void StrongName_get_PublicKeyToken_m6731 ();
extern "C" void StrongName_get_TokenAlgorithm_m6732 ();
extern "C" void SecurityParser__ctor_m6733 ();
extern "C" void SecurityParser_LoadXml_m6734 ();
extern "C" void SecurityParser_ToXml_m6735 ();
extern "C" void SecurityParser_OnStartParsing_m6736 ();
extern "C" void SecurityParser_OnProcessingInstruction_m6737 ();
extern "C" void SecurityParser_OnIgnorableWhitespace_m6738 ();
extern "C" void SecurityParser_OnStartElement_m6739 ();
extern "C" void SecurityParser_OnEndElement_m6740 ();
extern "C" void SecurityParser_OnChars_m6741 ();
extern "C" void SecurityParser_OnEndParsing_m6742 ();
extern "C" void AttrListImpl__ctor_m6743 ();
extern "C" void AttrListImpl_get_Length_m6744 ();
extern "C" void AttrListImpl_GetName_m6745 ();
extern "C" void AttrListImpl_GetValue_m6746 ();
extern "C" void AttrListImpl_GetValue_m6747 ();
extern "C" void AttrListImpl_get_Names_m6748 ();
extern "C" void AttrListImpl_get_Values_m6749 ();
extern "C" void AttrListImpl_Clear_m6750 ();
extern "C" void AttrListImpl_Add_m6751 ();
extern "C" void SmallXmlParser__ctor_m6752 ();
extern "C" void SmallXmlParser_Error_m6753 ();
extern "C" void SmallXmlParser_UnexpectedEndError_m6754 ();
extern "C" void SmallXmlParser_IsNameChar_m6755 ();
extern "C" void SmallXmlParser_IsWhitespace_m6756 ();
extern "C" void SmallXmlParser_SkipWhitespaces_m6757 ();
extern "C" void SmallXmlParser_HandleWhitespaces_m6758 ();
extern "C" void SmallXmlParser_SkipWhitespaces_m6759 ();
extern "C" void SmallXmlParser_Peek_m6760 ();
extern "C" void SmallXmlParser_Read_m6761 ();
extern "C" void SmallXmlParser_Expect_m6762 ();
extern "C" void SmallXmlParser_ReadUntil_m6763 ();
extern "C" void SmallXmlParser_ReadName_m6764 ();
extern "C" void SmallXmlParser_Parse_m6765 ();
extern "C" void SmallXmlParser_Cleanup_m6766 ();
extern "C" void SmallXmlParser_ReadContent_m6767 ();
extern "C" void SmallXmlParser_HandleBufferedContent_m6768 ();
extern "C" void SmallXmlParser_ReadCharacters_m6769 ();
extern "C" void SmallXmlParser_ReadReference_m6770 ();
extern "C" void SmallXmlParser_ReadCharacterReference_m6771 ();
extern "C" void SmallXmlParser_ReadAttribute_m6772 ();
extern "C" void SmallXmlParser_ReadCDATASection_m6773 ();
extern "C" void SmallXmlParser_ReadComment_m6774 ();
extern "C" void SmallXmlParserException__ctor_m6775 ();
extern "C" void Runtime_GetDisplayName_m6776 ();
extern "C" void KeyNotFoundException__ctor_m6777 ();
extern "C" void KeyNotFoundException__ctor_m6778 ();
extern "C" void SimpleEnumerator__ctor_m6779 ();
extern "C" void SimpleEnumerator__cctor_m6780 ();
extern "C" void SimpleEnumerator_MoveNext_m6781 ();
extern "C" void SimpleEnumerator_get_Current_m6782 ();
extern "C" void ArrayListWrapper__ctor_m6783 ();
extern "C" void ArrayListWrapper_get_Item_m6784 ();
extern "C" void ArrayListWrapper_set_Item_m6785 ();
extern "C" void ArrayListWrapper_get_Count_m6786 ();
extern "C" void ArrayListWrapper_get_Capacity_m6787 ();
extern "C" void ArrayListWrapper_set_Capacity_m6788 ();
extern "C" void ArrayListWrapper_get_IsFixedSize_m6789 ();
extern "C" void ArrayListWrapper_get_IsReadOnly_m6790 ();
extern "C" void ArrayListWrapper_get_IsSynchronized_m6791 ();
extern "C" void ArrayListWrapper_get_SyncRoot_m6792 ();
extern "C" void ArrayListWrapper_Add_m6793 ();
extern "C" void ArrayListWrapper_Clear_m6794 ();
extern "C" void ArrayListWrapper_Contains_m6795 ();
extern "C" void ArrayListWrapper_IndexOf_m6796 ();
extern "C" void ArrayListWrapper_IndexOf_m6797 ();
extern "C" void ArrayListWrapper_IndexOf_m6798 ();
extern "C" void ArrayListWrapper_Insert_m6799 ();
extern "C" void ArrayListWrapper_InsertRange_m6800 ();
extern "C" void ArrayListWrapper_Remove_m6801 ();
extern "C" void ArrayListWrapper_RemoveAt_m6802 ();
extern "C" void ArrayListWrapper_CopyTo_m6803 ();
extern "C" void ArrayListWrapper_CopyTo_m6804 ();
extern "C" void ArrayListWrapper_CopyTo_m6805 ();
extern "C" void ArrayListWrapper_GetEnumerator_m6806 ();
extern "C" void ArrayListWrapper_AddRange_m6807 ();
extern "C" void ArrayListWrapper_Clone_m6808 ();
extern "C" void ArrayListWrapper_Sort_m6809 ();
extern "C" void ArrayListWrapper_Sort_m6810 ();
extern "C" void ArrayListWrapper_ToArray_m6811 ();
extern "C" void ArrayListWrapper_ToArray_m6812 ();
extern "C" void SynchronizedArrayListWrapper__ctor_m6813 ();
extern "C" void SynchronizedArrayListWrapper_get_Item_m6814 ();
extern "C" void SynchronizedArrayListWrapper_set_Item_m6815 ();
extern "C" void SynchronizedArrayListWrapper_get_Count_m6816 ();
extern "C" void SynchronizedArrayListWrapper_get_Capacity_m6817 ();
extern "C" void SynchronizedArrayListWrapper_set_Capacity_m6818 ();
extern "C" void SynchronizedArrayListWrapper_get_IsFixedSize_m6819 ();
extern "C" void SynchronizedArrayListWrapper_get_IsReadOnly_m6820 ();
extern "C" void SynchronizedArrayListWrapper_get_IsSynchronized_m6821 ();
extern "C" void SynchronizedArrayListWrapper_get_SyncRoot_m6822 ();
extern "C" void SynchronizedArrayListWrapper_Add_m6823 ();
extern "C" void SynchronizedArrayListWrapper_Clear_m6824 ();
extern "C" void SynchronizedArrayListWrapper_Contains_m6825 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m6826 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m6827 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m6828 ();
extern "C" void SynchronizedArrayListWrapper_Insert_m6829 ();
extern "C" void SynchronizedArrayListWrapper_InsertRange_m6830 ();
extern "C" void SynchronizedArrayListWrapper_Remove_m6831 ();
extern "C" void SynchronizedArrayListWrapper_RemoveAt_m6832 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m6833 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m6834 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m6835 ();
extern "C" void SynchronizedArrayListWrapper_GetEnumerator_m6836 ();
extern "C" void SynchronizedArrayListWrapper_AddRange_m6837 ();
extern "C" void SynchronizedArrayListWrapper_Clone_m6838 ();
extern "C" void SynchronizedArrayListWrapper_Sort_m6839 ();
extern "C" void SynchronizedArrayListWrapper_Sort_m6840 ();
extern "C" void SynchronizedArrayListWrapper_ToArray_m6841 ();
extern "C" void SynchronizedArrayListWrapper_ToArray_m6842 ();
extern "C" void FixedSizeArrayListWrapper__ctor_m6843 ();
extern "C" void FixedSizeArrayListWrapper_get_ErrorMessage_m6844 ();
extern "C" void FixedSizeArrayListWrapper_get_Capacity_m6845 ();
extern "C" void FixedSizeArrayListWrapper_set_Capacity_m6846 ();
extern "C" void FixedSizeArrayListWrapper_get_IsFixedSize_m6847 ();
extern "C" void FixedSizeArrayListWrapper_Add_m6848 ();
extern "C" void FixedSizeArrayListWrapper_AddRange_m6849 ();
extern "C" void FixedSizeArrayListWrapper_Clear_m6850 ();
extern "C" void FixedSizeArrayListWrapper_Insert_m6851 ();
extern "C" void FixedSizeArrayListWrapper_InsertRange_m6852 ();
extern "C" void FixedSizeArrayListWrapper_Remove_m6853 ();
extern "C" void FixedSizeArrayListWrapper_RemoveAt_m6854 ();
extern "C" void ReadOnlyArrayListWrapper__ctor_m6855 ();
extern "C" void ReadOnlyArrayListWrapper_get_ErrorMessage_m6856 ();
extern "C" void ReadOnlyArrayListWrapper_get_IsReadOnly_m6857 ();
extern "C" void ReadOnlyArrayListWrapper_get_Item_m6858 ();
extern "C" void ReadOnlyArrayListWrapper_set_Item_m6859 ();
extern "C" void ReadOnlyArrayListWrapper_Sort_m6860 ();
extern "C" void ReadOnlyArrayListWrapper_Sort_m6861 ();
extern "C" void ArrayList__ctor_m4340 ();
extern "C" void ArrayList__ctor_m5375 ();
extern "C" void ArrayList__ctor_m5396 ();
extern "C" void ArrayList__ctor_m6862 ();
extern "C" void ArrayList__cctor_m6863 ();
extern "C" void ArrayList_get_Item_m6864 ();
extern "C" void ArrayList_set_Item_m6865 ();
extern "C" void ArrayList_get_Count_m6866 ();
extern "C" void ArrayList_get_Capacity_m6867 ();
extern "C" void ArrayList_set_Capacity_m6868 ();
extern "C" void ArrayList_get_IsFixedSize_m6869 ();
extern "C" void ArrayList_get_IsReadOnly_m6870 ();
extern "C" void ArrayList_get_IsSynchronized_m6871 ();
extern "C" void ArrayList_get_SyncRoot_m6872 ();
extern "C" void ArrayList_EnsureCapacity_m6873 ();
extern "C" void ArrayList_Shift_m6874 ();
extern "C" void ArrayList_Add_m6875 ();
extern "C" void ArrayList_Clear_m6876 ();
extern "C" void ArrayList_Contains_m6877 ();
extern "C" void ArrayList_IndexOf_m6878 ();
extern "C" void ArrayList_IndexOf_m6879 ();
extern "C" void ArrayList_IndexOf_m6880 ();
extern "C" void ArrayList_Insert_m6881 ();
extern "C" void ArrayList_InsertRange_m6882 ();
extern "C" void ArrayList_Remove_m6883 ();
extern "C" void ArrayList_RemoveAt_m6884 ();
extern "C" void ArrayList_CopyTo_m6885 ();
extern "C" void ArrayList_CopyTo_m6886 ();
extern "C" void ArrayList_CopyTo_m6887 ();
extern "C" void ArrayList_GetEnumerator_m6888 ();
extern "C" void ArrayList_AddRange_m6889 ();
extern "C" void ArrayList_Sort_m6890 ();
extern "C" void ArrayList_Sort_m6891 ();
extern "C" void ArrayList_ToArray_m6892 ();
extern "C" void ArrayList_ToArray_m6893 ();
extern "C" void ArrayList_Clone_m6894 ();
extern "C" void ArrayList_ThrowNewArgumentOutOfRangeException_m6895 ();
extern "C" void ArrayList_Synchronized_m6896 ();
extern "C" void ArrayList_ReadOnly_m4370 ();
extern "C" void BitArrayEnumerator__ctor_m6897 ();
extern "C" void BitArrayEnumerator_get_Current_m6898 ();
extern "C" void BitArrayEnumerator_MoveNext_m6899 ();
extern "C" void BitArrayEnumerator_checkVersion_m6900 ();
extern "C" void BitArray__ctor_m5415 ();
extern "C" void BitArray_getByte_m6901 ();
extern "C" void BitArray_get_Count_m6902 ();
extern "C" void BitArray_get_IsSynchronized_m6903 ();
extern "C" void BitArray_get_Item_m5410 ();
extern "C" void BitArray_set_Item_m5416 ();
extern "C" void BitArray_get_Length_m5409 ();
extern "C" void BitArray_get_SyncRoot_m6904 ();
extern "C" void BitArray_CopyTo_m6905 ();
extern "C" void BitArray_Get_m6906 ();
extern "C" void BitArray_Set_m6907 ();
extern "C" void BitArray_GetEnumerator_m6908 ();
extern "C" void CaseInsensitiveComparer__ctor_m6909 ();
extern "C" void CaseInsensitiveComparer__ctor_m6910 ();
extern "C" void CaseInsensitiveComparer__cctor_m6911 ();
extern "C" void CaseInsensitiveComparer_get_DefaultInvariant_m5341 ();
extern "C" void CaseInsensitiveComparer_Compare_m6912 ();
extern "C" void CaseInsensitiveHashCodeProvider__ctor_m6913 ();
extern "C" void CaseInsensitiveHashCodeProvider__ctor_m6914 ();
extern "C" void CaseInsensitiveHashCodeProvider__cctor_m6915 ();
extern "C" void CaseInsensitiveHashCodeProvider_AreEqual_m6916 ();
extern "C" void CaseInsensitiveHashCodeProvider_AreEqual_m6917 ();
extern "C" void CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m5342 ();
extern "C" void CaseInsensitiveHashCodeProvider_GetHashCode_m6918 ();
extern "C" void CollectionBase__ctor_m4390 ();
extern "C" void CollectionBase_System_Collections_ICollection_CopyTo_m6919 ();
extern "C" void CollectionBase_System_Collections_ICollection_get_SyncRoot_m6920 ();
extern "C" void CollectionBase_System_Collections_ICollection_get_IsSynchronized_m6921 ();
extern "C" void CollectionBase_System_Collections_IList_Add_m6922 ();
extern "C" void CollectionBase_System_Collections_IList_Contains_m6923 ();
extern "C" void CollectionBase_System_Collections_IList_IndexOf_m6924 ();
extern "C" void CollectionBase_System_Collections_IList_Insert_m6925 ();
extern "C" void CollectionBase_System_Collections_IList_Remove_m6926 ();
extern "C" void CollectionBase_System_Collections_IList_get_IsFixedSize_m6927 ();
extern "C" void CollectionBase_System_Collections_IList_get_IsReadOnly_m6928 ();
extern "C" void CollectionBase_System_Collections_IList_get_Item_m6929 ();
extern "C" void CollectionBase_System_Collections_IList_set_Item_m6930 ();
extern "C" void CollectionBase_get_Count_m6931 ();
extern "C" void CollectionBase_GetEnumerator_m6932 ();
extern "C" void CollectionBase_Clear_m6933 ();
extern "C" void CollectionBase_RemoveAt_m6934 ();
extern "C" void CollectionBase_get_InnerList_m4391 ();
extern "C" void CollectionBase_get_List_m5413 ();
extern "C" void CollectionBase_OnClear_m6935 ();
extern "C" void CollectionBase_OnClearComplete_m6936 ();
extern "C" void CollectionBase_OnInsert_m6937 ();
extern "C" void CollectionBase_OnInsertComplete_m6938 ();
extern "C" void CollectionBase_OnRemove_m6939 ();
extern "C" void CollectionBase_OnRemoveComplete_m6940 ();
extern "C" void CollectionBase_OnSet_m6941 ();
extern "C" void CollectionBase_OnSetComplete_m6942 ();
extern "C" void CollectionBase_OnValidate_m6943 ();
extern "C" void Comparer__ctor_m6944 ();
extern "C" void Comparer__ctor_m6945 ();
extern "C" void Comparer__cctor_m6946 ();
extern "C" void Comparer_Compare_m6947 ();
extern "C" void Comparer_GetObjectData_m6948 ();
extern "C" void DictionaryEntry__ctor_m5345 ();
extern "C" void DictionaryEntry_get_Key_m6949 ();
extern "C" void DictionaryEntry_get_Value_m6950 ();
extern "C" void KeyMarker__ctor_m6951 ();
extern "C" void KeyMarker__cctor_m6952 ();
extern "C" void Enumerator__ctor_m6953 ();
extern "C" void Enumerator__cctor_m6954 ();
extern "C" void Enumerator_FailFast_m6955 ();
extern "C" void Enumerator_Reset_m6956 ();
extern "C" void Enumerator_MoveNext_m6957 ();
extern "C" void Enumerator_get_Entry_m6958 ();
extern "C" void Enumerator_get_Key_m6959 ();
extern "C" void Enumerator_get_Value_m6960 ();
extern "C" void Enumerator_get_Current_m6961 ();
extern "C" void HashKeys__ctor_m6962 ();
extern "C" void HashKeys_get_Count_m6963 ();
extern "C" void HashKeys_get_IsSynchronized_m6964 ();
extern "C" void HashKeys_get_SyncRoot_m6965 ();
extern "C" void HashKeys_CopyTo_m6966 ();
extern "C" void HashKeys_GetEnumerator_m6967 ();
extern "C" void HashValues__ctor_m6968 ();
extern "C" void HashValues_get_Count_m6969 ();
extern "C" void HashValues_get_IsSynchronized_m6970 ();
extern "C" void HashValues_get_SyncRoot_m6971 ();
extern "C" void HashValues_CopyTo_m6972 ();
extern "C" void HashValues_GetEnumerator_m6973 ();
extern "C" void Hashtable__ctor_m4416 ();
extern "C" void Hashtable__ctor_m6974 ();
extern "C" void Hashtable__ctor_m6975 ();
extern "C" void Hashtable__ctor_m5403 ();
extern "C" void Hashtable__ctor_m6976 ();
extern "C" void Hashtable__ctor_m5343 ();
extern "C" void Hashtable__ctor_m6977 ();
extern "C" void Hashtable__ctor_m5344 ();
extern "C" void Hashtable__ctor_m5372 ();
extern "C" void Hashtable__ctor_m6978 ();
extern "C" void Hashtable__ctor_m5351 ();
extern "C" void Hashtable__ctor_m6979 ();
extern "C" void Hashtable__cctor_m6980 ();
extern "C" void Hashtable_System_Collections_IEnumerable_GetEnumerator_m6981 ();
extern "C" void Hashtable_set_comparer_m6982 ();
extern "C" void Hashtable_set_hcp_m6983 ();
extern "C" void Hashtable_get_Count_m6984 ();
extern "C" void Hashtable_get_IsSynchronized_m6985 ();
extern "C" void Hashtable_get_SyncRoot_m6986 ();
extern "C" void Hashtable_get_Keys_m6987 ();
extern "C" void Hashtable_get_Values_m6988 ();
extern "C" void Hashtable_get_Item_m6989 ();
extern "C" void Hashtable_set_Item_m6990 ();
extern "C" void Hashtable_CopyTo_m6991 ();
extern "C" void Hashtable_Add_m6992 ();
extern "C" void Hashtable_Clear_m6993 ();
extern "C" void Hashtable_Contains_m6994 ();
extern "C" void Hashtable_GetEnumerator_m6995 ();
extern "C" void Hashtable_Remove_m6996 ();
extern "C" void Hashtable_ContainsKey_m6997 ();
extern "C" void Hashtable_Clone_m6998 ();
extern "C" void Hashtable_GetObjectData_m6999 ();
extern "C" void Hashtable_OnDeserialization_m7000 ();
extern "C" void Hashtable_GetHash_m7001 ();
extern "C" void Hashtable_KeyEquals_m7002 ();
extern "C" void Hashtable_AdjustThreshold_m7003 ();
extern "C" void Hashtable_SetTable_m7004 ();
extern "C" void Hashtable_Find_m7005 ();
extern "C" void Hashtable_Rehash_m7006 ();
extern "C" void Hashtable_PutImpl_m7007 ();
extern "C" void Hashtable_CopyToArray_m7008 ();
extern "C" void Hashtable_TestPrime_m7009 ();
extern "C" void Hashtable_CalcPrime_m7010 ();
extern "C" void Hashtable_ToPrime_m7011 ();
extern "C" void Enumerator__ctor_m7012 ();
extern "C" void Enumerator__cctor_m7013 ();
extern "C" void Enumerator_Reset_m7014 ();
extern "C" void Enumerator_MoveNext_m7015 ();
extern "C" void Enumerator_get_Entry_m7016 ();
extern "C" void Enumerator_get_Key_m7017 ();
extern "C" void Enumerator_get_Value_m7018 ();
extern "C" void Enumerator_get_Current_m7019 ();
extern "C" void SortedList__ctor_m7020 ();
extern "C" void SortedList__ctor_m5371 ();
extern "C" void SortedList__ctor_m7021 ();
extern "C" void SortedList__cctor_m7022 ();
extern "C" void SortedList_System_Collections_IEnumerable_GetEnumerator_m7023 ();
extern "C" void SortedList_get_Count_m7024 ();
extern "C" void SortedList_get_IsSynchronized_m7025 ();
extern "C" void SortedList_get_SyncRoot_m7026 ();
extern "C" void SortedList_get_IsFixedSize_m7027 ();
extern "C" void SortedList_get_IsReadOnly_m7028 ();
extern "C" void SortedList_get_Item_m7029 ();
extern "C" void SortedList_set_Item_m7030 ();
extern "C" void SortedList_get_Capacity_m7031 ();
extern "C" void SortedList_set_Capacity_m7032 ();
extern "C" void SortedList_Add_m7033 ();
extern "C" void SortedList_Contains_m7034 ();
extern "C" void SortedList_GetEnumerator_m7035 ();
extern "C" void SortedList_Remove_m7036 ();
extern "C" void SortedList_CopyTo_m7037 ();
extern "C" void SortedList_RemoveAt_m7038 ();
extern "C" void SortedList_IndexOfKey_m7039 ();
extern "C" void SortedList_ContainsKey_m7040 ();
extern "C" void SortedList_GetByIndex_m7041 ();
extern "C" void SortedList_EnsureCapacity_m7042 ();
extern "C" void SortedList_PutImpl_m7043 ();
extern "C" void SortedList_GetImpl_m7044 ();
extern "C" void SortedList_InitTable_m7045 ();
extern "C" void SortedList_Find_m7046 ();
extern "C" void Enumerator__ctor_m7047 ();
extern "C" void Enumerator_get_Current_m7048 ();
extern "C" void Enumerator_MoveNext_m7049 ();
extern "C" void Stack__ctor_m3399 ();
extern "C" void Stack_Resize_m7050 ();
extern "C" void Stack_get_Count_m7051 ();
extern "C" void Stack_get_IsSynchronized_m7052 ();
extern "C" void Stack_get_SyncRoot_m7053 ();
extern "C" void Stack_Clear_m7054 ();
extern "C" void Stack_CopyTo_m7055 ();
extern "C" void Stack_GetEnumerator_m7056 ();
extern "C" void Stack_Peek_m7057 ();
extern "C" void Stack_Pop_m7058 ();
extern "C" void Stack_Push_m7059 ();
extern "C" void SuppressMessageAttribute__ctor_m7060 ();
extern "C" void SuppressMessageAttribute_set_Justification_m7061 ();
extern "C" void DebuggableAttribute__ctor_m7062 ();
extern "C" void DebuggerDisplayAttribute__ctor_m7063 ();
extern "C" void DebuggerDisplayAttribute_set_Name_m7064 ();
extern "C" void DebuggerStepThroughAttribute__ctor_m7065 ();
extern "C" void DebuggerTypeProxyAttribute__ctor_m7066 ();
extern "C" void StackFrame__ctor_m7067 ();
extern "C" void StackFrame__ctor_m7068 ();
extern "C" void StackFrame_get_frame_info_m7069 ();
extern "C" void StackFrame_GetFileLineNumber_m7070 ();
extern "C" void StackFrame_GetFileName_m7071 ();
extern "C" void StackFrame_GetSecureFileName_m7072 ();
extern "C" void StackFrame_GetILOffset_m7073 ();
extern "C" void StackFrame_GetMethod_m7074 ();
extern "C" void StackFrame_GetNativeOffset_m7075 ();
extern "C" void StackFrame_GetInternalMethodName_m7076 ();
extern "C" void StackFrame_ToString_m7077 ();
extern "C" void StackTrace__ctor_m7078 ();
extern "C" void StackTrace__ctor_m3370 ();
extern "C" void StackTrace__ctor_m7079 ();
extern "C" void StackTrace__ctor_m7080 ();
extern "C" void StackTrace__ctor_m7081 ();
extern "C" void StackTrace_init_frames_m7082 ();
extern "C" void StackTrace_get_trace_m7083 ();
extern "C" void StackTrace_get_FrameCount_m7084 ();
extern "C" void StackTrace_GetFrame_m7085 ();
extern "C" void StackTrace_ToString_m7086 ();
extern "C" void Calendar__ctor_m7087 ();
extern "C" void Calendar_CheckReadOnly_m7088 ();
extern "C" void Calendar_get_EraNames_m7089 ();
extern "C" void CCMath_div_m7090 ();
extern "C" void CCMath_mod_m7091 ();
extern "C" void CCMath_div_mod_m7092 ();
extern "C" void CCFixed_FromDateTime_m7093 ();
extern "C" void CCFixed_day_of_week_m7094 ();
extern "C" void CCGregorianCalendar_is_leap_year_m7095 ();
extern "C" void CCGregorianCalendar_fixed_from_dmy_m7096 ();
extern "C" void CCGregorianCalendar_year_from_fixed_m7097 ();
extern "C" void CCGregorianCalendar_my_from_fixed_m7098 ();
extern "C" void CCGregorianCalendar_dmy_from_fixed_m7099 ();
extern "C" void CCGregorianCalendar_month_from_fixed_m7100 ();
extern "C" void CCGregorianCalendar_day_from_fixed_m7101 ();
extern "C" void CCGregorianCalendar_GetDayOfMonth_m7102 ();
extern "C" void CCGregorianCalendar_GetMonth_m7103 ();
extern "C" void CCGregorianCalendar_GetYear_m7104 ();
extern "C" void CompareInfo__ctor_m7105 ();
extern "C" void CompareInfo__ctor_m7106 ();
extern "C" void CompareInfo__cctor_m7107 ();
extern "C" void CompareInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7108 ();
extern "C" void CompareInfo_get_UseManagedCollation_m7109 ();
extern "C" void CompareInfo_construct_compareinfo_m7110 ();
extern "C" void CompareInfo_free_internal_collator_m7111 ();
extern "C" void CompareInfo_internal_compare_m7112 ();
extern "C" void CompareInfo_assign_sortkey_m7113 ();
extern "C" void CompareInfo_internal_index_m7114 ();
extern "C" void CompareInfo_Finalize_m7115 ();
extern "C" void CompareInfo_internal_compare_managed_m7116 ();
extern "C" void CompareInfo_internal_compare_switch_m7117 ();
extern "C" void CompareInfo_Compare_m7118 ();
extern "C" void CompareInfo_Compare_m7119 ();
extern "C" void CompareInfo_Compare_m7120 ();
extern "C" void CompareInfo_Equals_m7121 ();
extern "C" void CompareInfo_GetHashCode_m7122 ();
extern "C" void CompareInfo_GetSortKey_m7123 ();
extern "C" void CompareInfo_IndexOf_m7124 ();
extern "C" void CompareInfo_internal_index_managed_m7125 ();
extern "C" void CompareInfo_internal_index_switch_m7126 ();
extern "C" void CompareInfo_IndexOf_m7127 ();
extern "C" void CompareInfo_IsPrefix_m7128 ();
extern "C" void CompareInfo_IsSuffix_m7129 ();
extern "C" void CompareInfo_LastIndexOf_m7130 ();
extern "C" void CompareInfo_LastIndexOf_m7131 ();
extern "C" void CompareInfo_ToString_m7132 ();
extern "C" void CompareInfo_get_LCID_m7133 ();
extern "C" void CultureInfo__ctor_m7134 ();
extern "C" void CultureInfo__ctor_m7135 ();
extern "C" void CultureInfo__ctor_m7136 ();
extern "C" void CultureInfo__ctor_m7137 ();
extern "C" void CultureInfo__ctor_m7138 ();
extern "C" void CultureInfo__cctor_m7139 ();
extern "C" void CultureInfo_get_InvariantCulture_m3328 ();
extern "C" void CultureInfo_get_CurrentCulture_m4411 ();
extern "C" void CultureInfo_get_CurrentUICulture_m4413 ();
extern "C" void CultureInfo_ConstructCurrentCulture_m7140 ();
extern "C" void CultureInfo_ConstructCurrentUICulture_m7141 ();
extern "C" void CultureInfo_get_LCID_m7142 ();
extern "C" void CultureInfo_get_Name_m7143 ();
extern "C" void CultureInfo_get_Parent_m7144 ();
extern "C" void CultureInfo_get_TextInfo_m7145 ();
extern "C" void CultureInfo_get_IcuName_m7146 ();
extern "C" void CultureInfo_Equals_m7147 ();
extern "C" void CultureInfo_GetHashCode_m7148 ();
extern "C" void CultureInfo_ToString_m7149 ();
extern "C" void CultureInfo_get_CompareInfo_m7150 ();
extern "C" void CultureInfo_get_IsNeutralCulture_m7151 ();
extern "C" void CultureInfo_CheckNeutral_m7152 ();
extern "C" void CultureInfo_get_NumberFormat_m7153 ();
extern "C" void CultureInfo_get_DateTimeFormat_m7154 ();
extern "C" void CultureInfo_get_IsReadOnly_m7155 ();
extern "C" void CultureInfo_GetFormat_m7156 ();
extern "C" void CultureInfo_Construct_m7157 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromName_m7158 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromLcid_m7159 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromCurrentLocale_m7160 ();
extern "C" void CultureInfo_construct_internal_locale_from_lcid_m7161 ();
extern "C" void CultureInfo_construct_internal_locale_from_name_m7162 ();
extern "C" void CultureInfo_construct_internal_locale_from_current_locale_m7163 ();
extern "C" void CultureInfo_construct_datetime_format_m7164 ();
extern "C" void CultureInfo_construct_number_format_m7165 ();
extern "C" void CultureInfo_ConstructInvariant_m7166 ();
extern "C" void CultureInfo_CreateTextInfo_m7167 ();
extern "C" void CultureInfo_CreateCulture_m7168 ();
extern "C" void DateTimeFormatInfo__ctor_m7169 ();
extern "C" void DateTimeFormatInfo__ctor_m7170 ();
extern "C" void DateTimeFormatInfo__cctor_m7171 ();
extern "C" void DateTimeFormatInfo_GetInstance_m7172 ();
extern "C" void DateTimeFormatInfo_get_IsReadOnly_m7173 ();
extern "C" void DateTimeFormatInfo_ReadOnly_m7174 ();
extern "C" void DateTimeFormatInfo_Clone_m7175 ();
extern "C" void DateTimeFormatInfo_GetFormat_m7176 ();
extern "C" void DateTimeFormatInfo_GetAbbreviatedMonthName_m7177 ();
extern "C" void DateTimeFormatInfo_GetEraName_m7178 ();
extern "C" void DateTimeFormatInfo_GetMonthName_m7179 ();
extern "C" void DateTimeFormatInfo_get_RawAbbreviatedDayNames_m7180 ();
extern "C" void DateTimeFormatInfo_get_RawAbbreviatedMonthNames_m7181 ();
extern "C" void DateTimeFormatInfo_get_RawDayNames_m7182 ();
extern "C" void DateTimeFormatInfo_get_RawMonthNames_m7183 ();
extern "C" void DateTimeFormatInfo_get_AMDesignator_m7184 ();
extern "C" void DateTimeFormatInfo_get_PMDesignator_m7185 ();
extern "C" void DateTimeFormatInfo_get_DateSeparator_m7186 ();
extern "C" void DateTimeFormatInfo_get_TimeSeparator_m7187 ();
extern "C" void DateTimeFormatInfo_get_LongDatePattern_m7188 ();
extern "C" void DateTimeFormatInfo_get_ShortDatePattern_m7189 ();
extern "C" void DateTimeFormatInfo_get_ShortTimePattern_m7190 ();
extern "C" void DateTimeFormatInfo_get_LongTimePattern_m7191 ();
extern "C" void DateTimeFormatInfo_get_MonthDayPattern_m7192 ();
extern "C" void DateTimeFormatInfo_get_YearMonthPattern_m7193 ();
extern "C" void DateTimeFormatInfo_get_FullDateTimePattern_m7194 ();
extern "C" void DateTimeFormatInfo_get_CurrentInfo_m7195 ();
extern "C" void DateTimeFormatInfo_get_InvariantInfo_m7196 ();
extern "C" void DateTimeFormatInfo_get_Calendar_m7197 ();
extern "C" void DateTimeFormatInfo_set_Calendar_m7198 ();
extern "C" void DateTimeFormatInfo_get_RFC1123Pattern_m7199 ();
extern "C" void DateTimeFormatInfo_get_RoundtripPattern_m7200 ();
extern "C" void DateTimeFormatInfo_get_SortableDateTimePattern_m7201 ();
extern "C" void DateTimeFormatInfo_get_UniversalSortableDateTimePattern_m7202 ();
extern "C" void DateTimeFormatInfo_GetAllDateTimePatternsInternal_m7203 ();
extern "C" void DateTimeFormatInfo_FillAllDateTimePatterns_m7204 ();
extern "C" void DateTimeFormatInfo_GetAllRawDateTimePatterns_m7205 ();
extern "C" void DateTimeFormatInfo_GetDayName_m7206 ();
extern "C" void DateTimeFormatInfo_GetAbbreviatedDayName_m7207 ();
extern "C" void DateTimeFormatInfo_FillInvariantPatterns_m7208 ();
extern "C" void DateTimeFormatInfo_PopulateCombinedList_m7209 ();
extern "C" void DaylightTime__ctor_m7210 ();
extern "C" void DaylightTime_get_Start_m7211 ();
extern "C" void DaylightTime_get_End_m7212 ();
extern "C" void DaylightTime_get_Delta_m7213 ();
extern "C" void GregorianCalendar__ctor_m7214 ();
extern "C" void GregorianCalendar__ctor_m7215 ();
extern "C" void GregorianCalendar_get_Eras_m7216 ();
extern "C" void GregorianCalendar_set_CalendarType_m7217 ();
extern "C" void GregorianCalendar_GetDayOfMonth_m7218 ();
extern "C" void GregorianCalendar_GetDayOfWeek_m7219 ();
extern "C" void GregorianCalendar_GetEra_m7220 ();
extern "C" void GregorianCalendar_GetMonth_m7221 ();
extern "C" void GregorianCalendar_GetYear_m7222 ();
extern "C" void NumberFormatInfo__ctor_m7223 ();
extern "C" void NumberFormatInfo__ctor_m7224 ();
extern "C" void NumberFormatInfo__ctor_m7225 ();
extern "C" void NumberFormatInfo__cctor_m7226 ();
extern "C" void NumberFormatInfo_get_CurrencyDecimalDigits_m7227 ();
extern "C" void NumberFormatInfo_get_CurrencyDecimalSeparator_m7228 ();
extern "C" void NumberFormatInfo_get_CurrencyGroupSeparator_m7229 ();
extern "C" void NumberFormatInfo_get_RawCurrencyGroupSizes_m7230 ();
extern "C" void NumberFormatInfo_get_CurrencyNegativePattern_m7231 ();
extern "C" void NumberFormatInfo_get_CurrencyPositivePattern_m7232 ();
extern "C" void NumberFormatInfo_get_CurrencySymbol_m7233 ();
extern "C" void NumberFormatInfo_get_CurrentInfo_m7234 ();
extern "C" void NumberFormatInfo_get_InvariantInfo_m7235 ();
extern "C" void NumberFormatInfo_get_NaNSymbol_m7236 ();
extern "C" void NumberFormatInfo_get_NegativeInfinitySymbol_m7237 ();
extern "C" void NumberFormatInfo_get_NegativeSign_m7238 ();
extern "C" void NumberFormatInfo_get_NumberDecimalDigits_m7239 ();
extern "C" void NumberFormatInfo_get_NumberDecimalSeparator_m7240 ();
extern "C" void NumberFormatInfo_get_NumberGroupSeparator_m7241 ();
extern "C" void NumberFormatInfo_get_RawNumberGroupSizes_m7242 ();
extern "C" void NumberFormatInfo_get_NumberNegativePattern_m7243 ();
extern "C" void NumberFormatInfo_set_NumberNegativePattern_m7244 ();
extern "C" void NumberFormatInfo_get_PercentDecimalDigits_m7245 ();
extern "C" void NumberFormatInfo_get_PercentDecimalSeparator_m7246 ();
extern "C" void NumberFormatInfo_get_PercentGroupSeparator_m7247 ();
extern "C" void NumberFormatInfo_get_RawPercentGroupSizes_m7248 ();
extern "C" void NumberFormatInfo_get_PercentNegativePattern_m7249 ();
extern "C" void NumberFormatInfo_get_PercentPositivePattern_m7250 ();
extern "C" void NumberFormatInfo_get_PercentSymbol_m7251 ();
extern "C" void NumberFormatInfo_get_PerMilleSymbol_m7252 ();
extern "C" void NumberFormatInfo_get_PositiveInfinitySymbol_m7253 ();
extern "C" void NumberFormatInfo_get_PositiveSign_m7254 ();
extern "C" void NumberFormatInfo_GetFormat_m7255 ();
extern "C" void NumberFormatInfo_Clone_m7256 ();
extern "C" void NumberFormatInfo_GetInstance_m7257 ();
extern "C" void TextInfo__ctor_m7258 ();
extern "C" void TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7259 ();
extern "C" void TextInfo_get_CultureName_m7260 ();
extern "C" void TextInfo_Equals_m7261 ();
extern "C" void TextInfo_GetHashCode_m7262 ();
extern "C" void TextInfo_ToString_m7263 ();
extern "C" void TextInfo_ToLower_m7264 ();
extern "C" void TextInfo_ToUpper_m7265 ();
extern "C" void TextInfo_ToLower_m7266 ();
extern "C" void TextInfo_ToUpper_m7267 ();
extern "C" void IsolatedStorageException__ctor_m7268 ();
extern "C" void IsolatedStorageException__ctor_m7269 ();
extern "C" void IsolatedStorageException__ctor_m7270 ();
extern "C" void BinaryReader__ctor_m7271 ();
extern "C" void BinaryReader__ctor_m7272 ();
extern "C" void BinaryReader_System_IDisposable_Dispose_m7273 ();
extern "C" void BinaryReader_Dispose_m7274 ();
extern "C" void BinaryReader_FillBuffer_m7275 ();
extern "C" void BinaryReader_Read_m7276 ();
extern "C" void BinaryReader_Read_m7277 ();
extern "C" void BinaryReader_Read_m7278 ();
extern "C" void BinaryReader_ReadCharBytes_m7279 ();
extern "C" void BinaryReader_Read7BitEncodedInt_m7280 ();
extern "C" void BinaryReader_ReadBoolean_m7281 ();
extern "C" void BinaryReader_ReadByte_m7282 ();
extern "C" void BinaryReader_ReadChar_m7283 ();
extern "C" void BinaryReader_ReadDecimal_m7284 ();
extern "C" void BinaryReader_ReadDouble_m7285 ();
extern "C" void BinaryReader_ReadInt16_m7286 ();
extern "C" void BinaryReader_ReadInt32_m7287 ();
extern "C" void BinaryReader_ReadInt64_m7288 ();
extern "C" void BinaryReader_ReadSByte_m7289 ();
extern "C" void BinaryReader_ReadString_m7290 ();
extern "C" void BinaryReader_ReadSingle_m7291 ();
extern "C" void BinaryReader_ReadUInt16_m7292 ();
extern "C" void BinaryReader_ReadUInt32_m7293 ();
extern "C" void BinaryReader_ReadUInt64_m7294 ();
extern "C" void BinaryReader_CheckBuffer_m7295 ();
extern "C" void Directory_CreateDirectory_m4397 ();
extern "C" void Directory_CreateDirectoriesInternal_m7296 ();
extern "C" void Directory_Exists_m4396 ();
extern "C" void Directory_GetCurrentDirectory_m7297 ();
extern "C" void Directory_GetFiles_m4399 ();
extern "C" void Directory_GetFileSystemEntries_m7298 ();
extern "C" void DirectoryInfo__ctor_m7299 ();
extern "C" void DirectoryInfo__ctor_m7300 ();
extern "C" void DirectoryInfo__ctor_m7301 ();
extern "C" void DirectoryInfo_Initialize_m7302 ();
extern "C" void DirectoryInfo_get_Exists_m7303 ();
extern "C" void DirectoryInfo_get_Parent_m7304 ();
extern "C" void DirectoryInfo_Create_m7305 ();
extern "C" void DirectoryInfo_ToString_m7306 ();
extern "C" void DirectoryNotFoundException__ctor_m7307 ();
extern "C" void DirectoryNotFoundException__ctor_m7308 ();
extern "C" void DirectoryNotFoundException__ctor_m7309 ();
extern "C" void EndOfStreamException__ctor_m7310 ();
extern "C" void EndOfStreamException__ctor_m7311 ();
extern "C" void File_Delete_m7312 ();
extern "C" void File_Exists_m7313 ();
extern "C" void File_Open_m7314 ();
extern "C" void File_OpenRead_m4395 ();
extern "C" void File_OpenText_m7315 ();
extern "C" void FileNotFoundException__ctor_m7316 ();
extern "C" void FileNotFoundException__ctor_m7317 ();
extern "C" void FileNotFoundException__ctor_m7318 ();
extern "C" void FileNotFoundException_get_Message_m7319 ();
extern "C" void FileNotFoundException_GetObjectData_m7320 ();
extern "C" void FileNotFoundException_ToString_m7321 ();
extern "C" void ReadDelegate__ctor_m7322 ();
extern "C" void ReadDelegate_Invoke_m7323 ();
extern "C" void ReadDelegate_BeginInvoke_m7324 ();
extern "C" void ReadDelegate_EndInvoke_m7325 ();
extern "C" void WriteDelegate__ctor_m7326 ();
extern "C" void WriteDelegate_Invoke_m7327 ();
extern "C" void WriteDelegate_BeginInvoke_m7328 ();
extern "C" void WriteDelegate_EndInvoke_m7329 ();
extern "C" void FileStream__ctor_m7330 ();
extern "C" void FileStream__ctor_m7331 ();
extern "C" void FileStream__ctor_m7332 ();
extern "C" void FileStream__ctor_m7333 ();
extern "C" void FileStream__ctor_m7334 ();
extern "C" void FileStream_get_CanRead_m7335 ();
extern "C" void FileStream_get_CanWrite_m7336 ();
extern "C" void FileStream_get_CanSeek_m7337 ();
extern "C" void FileStream_get_Length_m7338 ();
extern "C" void FileStream_get_Position_m7339 ();
extern "C" void FileStream_set_Position_m7340 ();
extern "C" void FileStream_ReadByte_m7341 ();
extern "C" void FileStream_WriteByte_m7342 ();
extern "C" void FileStream_Read_m7343 ();
extern "C" void FileStream_ReadInternal_m7344 ();
extern "C" void FileStream_BeginRead_m7345 ();
extern "C" void FileStream_EndRead_m7346 ();
extern "C" void FileStream_Write_m7347 ();
extern "C" void FileStream_WriteInternal_m7348 ();
extern "C" void FileStream_BeginWrite_m7349 ();
extern "C" void FileStream_EndWrite_m7350 ();
extern "C" void FileStream_Seek_m7351 ();
extern "C" void FileStream_SetLength_m7352 ();
extern "C" void FileStream_Flush_m7353 ();
extern "C" void FileStream_Finalize_m7354 ();
extern "C" void FileStream_Dispose_m7355 ();
extern "C" void FileStream_ReadSegment_m7356 ();
extern "C" void FileStream_WriteSegment_m7357 ();
extern "C" void FileStream_FlushBuffer_m7358 ();
extern "C" void FileStream_FlushBuffer_m7359 ();
extern "C" void FileStream_FlushBufferIfDirty_m7360 ();
extern "C" void FileStream_RefillBuffer_m7361 ();
extern "C" void FileStream_ReadData_m7362 ();
extern "C" void FileStream_InitBuffer_m7363 ();
extern "C" void FileStream_GetSecureFileName_m7364 ();
extern "C" void FileStream_GetSecureFileName_m7365 ();
extern "C" void FileStreamAsyncResult__ctor_m7366 ();
extern "C" void FileStreamAsyncResult_CBWrapper_m7367 ();
extern "C" void FileStreamAsyncResult_get_AsyncState_m7368 ();
extern "C" void FileStreamAsyncResult_get_AsyncWaitHandle_m7369 ();
extern "C" void FileStreamAsyncResult_get_IsCompleted_m7370 ();
extern "C" void FileSystemInfo__ctor_m7371 ();
extern "C" void FileSystemInfo__ctor_m7372 ();
extern "C" void FileSystemInfo_GetObjectData_m7373 ();
extern "C" void FileSystemInfo_get_FullName_m7374 ();
extern "C" void FileSystemInfo_Refresh_m7375 ();
extern "C" void FileSystemInfo_InternalRefresh_m7376 ();
extern "C" void FileSystemInfo_CheckPath_m7377 ();
extern "C" void IOException__ctor_m7378 ();
extern "C" void IOException__ctor_m7379 ();
extern "C" void IOException__ctor_m4443 ();
extern "C" void IOException__ctor_m7380 ();
extern "C" void IOException__ctor_m7381 ();
extern "C" void MemoryStream__ctor_m4444 ();
extern "C" void MemoryStream__ctor_m3286 ();
extern "C" void MemoryStream__ctor_m4449 ();
extern "C" void MemoryStream_InternalConstructor_m7382 ();
extern "C" void MemoryStream_CheckIfClosedThrowDisposed_m7383 ();
extern "C" void MemoryStream_get_CanRead_m7384 ();
extern "C" void MemoryStream_get_CanSeek_m7385 ();
extern "C" void MemoryStream_get_CanWrite_m7386 ();
extern "C" void MemoryStream_set_Capacity_m7387 ();
extern "C" void MemoryStream_get_Length_m7388 ();
extern "C" void MemoryStream_get_Position_m7389 ();
extern "C" void MemoryStream_set_Position_m7390 ();
extern "C" void MemoryStream_Dispose_m7391 ();
extern "C" void MemoryStream_Flush_m7392 ();
extern "C" void MemoryStream_Read_m7393 ();
extern "C" void MemoryStream_ReadByte_m7394 ();
extern "C" void MemoryStream_Seek_m7395 ();
extern "C" void MemoryStream_CalculateNewCapacity_m7396 ();
extern "C" void MemoryStream_Expand_m7397 ();
extern "C" void MemoryStream_SetLength_m7398 ();
extern "C" void MemoryStream_ToArray_m7399 ();
extern "C" void MemoryStream_Write_m7400 ();
extern "C" void MemoryStream_WriteByte_m7401 ();
extern "C" void MonoIO__cctor_m7402 ();
extern "C" void MonoIO_GetException_m7403 ();
extern "C" void MonoIO_GetException_m7404 ();
extern "C" void MonoIO_CreateDirectory_m7405 ();
extern "C" void MonoIO_GetFileSystemEntries_m7406 ();
extern "C" void MonoIO_GetCurrentDirectory_m7407 ();
extern "C" void MonoIO_DeleteFile_m7408 ();
extern "C" void MonoIO_GetFileAttributes_m7409 ();
extern "C" void MonoIO_GetFileType_m7410 ();
extern "C" void MonoIO_ExistsFile_m7411 ();
extern "C" void MonoIO_ExistsDirectory_m7412 ();
extern "C" void MonoIO_GetFileStat_m7413 ();
extern "C" void MonoIO_Open_m7414 ();
extern "C" void MonoIO_Close_m7415 ();
extern "C" void MonoIO_Read_m7416 ();
extern "C" void MonoIO_Write_m7417 ();
extern "C" void MonoIO_Seek_m7418 ();
extern "C" void MonoIO_GetLength_m7419 ();
extern "C" void MonoIO_SetLength_m7420 ();
extern "C" void MonoIO_get_ConsoleOutput_m7421 ();
extern "C" void MonoIO_get_ConsoleInput_m7422 ();
extern "C" void MonoIO_get_ConsoleError_m7423 ();
extern "C" void MonoIO_get_VolumeSeparatorChar_m7424 ();
extern "C" void MonoIO_get_DirectorySeparatorChar_m7425 ();
extern "C" void MonoIO_get_AltDirectorySeparatorChar_m7426 ();
extern "C" void MonoIO_get_PathSeparator_m7427 ();
extern "C" void Path__cctor_m7428 ();
extern "C" void Path_Combine_m4398 ();
extern "C" void Path_CleanPath_m7429 ();
extern "C" void Path_GetDirectoryName_m7430 ();
extern "C" void Path_GetFileName_m7431 ();
extern "C" void Path_GetFullPath_m7432 ();
extern "C" void Path_WindowsDriveAdjustment_m7433 ();
extern "C" void Path_InsecureGetFullPath_m7434 ();
extern "C" void Path_IsDsc_m7435 ();
extern "C" void Path_GetPathRoot_m7436 ();
extern "C" void Path_IsPathRooted_m7437 ();
extern "C" void Path_GetInvalidPathChars_m7438 ();
extern "C" void Path_GetServerAndShare_m7439 ();
extern "C" void Path_SameRoot_m7440 ();
extern "C" void Path_CanonicalizePath_m7441 ();
extern "C" void PathTooLongException__ctor_m7442 ();
extern "C" void PathTooLongException__ctor_m7443 ();
extern "C" void PathTooLongException__ctor_m7444 ();
extern "C" void SearchPattern__cctor_m7445 ();
extern "C" void Stream__ctor_m4445 ();
extern "C" void Stream__cctor_m7446 ();
extern "C" void Stream_Dispose_m7447 ();
extern "C" void Stream_Dispose_m4448 ();
extern "C" void Stream_Close_m4447 ();
extern "C" void Stream_ReadByte_m7448 ();
extern "C" void Stream_WriteByte_m7449 ();
extern "C" void Stream_BeginRead_m7450 ();
extern "C" void Stream_BeginWrite_m7451 ();
extern "C" void Stream_EndRead_m7452 ();
extern "C" void Stream_EndWrite_m7453 ();
extern "C" void NullStream__ctor_m7454 ();
extern "C" void NullStream_get_CanRead_m7455 ();
extern "C" void NullStream_get_CanSeek_m7456 ();
extern "C" void NullStream_get_CanWrite_m7457 ();
extern "C" void NullStream_get_Length_m7458 ();
extern "C" void NullStream_get_Position_m7459 ();
extern "C" void NullStream_set_Position_m7460 ();
extern "C" void NullStream_Flush_m7461 ();
extern "C" void NullStream_Read_m7462 ();
extern "C" void NullStream_ReadByte_m7463 ();
extern "C" void NullStream_Seek_m7464 ();
extern "C" void NullStream_SetLength_m7465 ();
extern "C" void NullStream_Write_m7466 ();
extern "C" void NullStream_WriteByte_m7467 ();
extern "C" void StreamAsyncResult__ctor_m7468 ();
extern "C" void StreamAsyncResult_SetComplete_m7469 ();
extern "C" void StreamAsyncResult_SetComplete_m7470 ();
extern "C" void StreamAsyncResult_get_AsyncState_m7471 ();
extern "C" void StreamAsyncResult_get_AsyncWaitHandle_m7472 ();
extern "C" void StreamAsyncResult_get_IsCompleted_m7473 ();
extern "C" void StreamAsyncResult_get_Exception_m7474 ();
extern "C" void StreamAsyncResult_get_NBytes_m7475 ();
extern "C" void StreamAsyncResult_get_Done_m7476 ();
extern "C" void StreamAsyncResult_set_Done_m7477 ();
extern "C" void NullStreamReader__ctor_m7478 ();
extern "C" void NullStreamReader_Peek_m7479 ();
extern "C" void NullStreamReader_Read_m7480 ();
extern "C" void NullStreamReader_Read_m7481 ();
extern "C" void NullStreamReader_ReadLine_m7482 ();
extern "C" void NullStreamReader_ReadToEnd_m7483 ();
extern "C" void StreamReader__ctor_m7484 ();
extern "C" void StreamReader__ctor_m7485 ();
extern "C" void StreamReader__ctor_m7486 ();
extern "C" void StreamReader__ctor_m7487 ();
extern "C" void StreamReader__ctor_m7488 ();
extern "C" void StreamReader__cctor_m7489 ();
extern "C" void StreamReader_Initialize_m7490 ();
extern "C" void StreamReader_Dispose_m7491 ();
extern "C" void StreamReader_DoChecks_m7492 ();
extern "C" void StreamReader_ReadBuffer_m7493 ();
extern "C" void StreamReader_Peek_m7494 ();
extern "C" void StreamReader_Read_m7495 ();
extern "C" void StreamReader_Read_m7496 ();
extern "C" void StreamReader_FindNextEOL_m7497 ();
extern "C" void StreamReader_ReadLine_m7498 ();
extern "C" void StreamReader_ReadToEnd_m7499 ();
extern "C" void StreamWriter__ctor_m7500 ();
extern "C" void StreamWriter__ctor_m7501 ();
extern "C" void StreamWriter__cctor_m7502 ();
extern "C" void StreamWriter_Initialize_m7503 ();
extern "C" void StreamWriter_set_AutoFlush_m7504 ();
extern "C" void StreamWriter_Dispose_m7505 ();
extern "C" void StreamWriter_Flush_m7506 ();
extern "C" void StreamWriter_FlushBytes_m7507 ();
extern "C" void StreamWriter_Decode_m7508 ();
extern "C" void StreamWriter_Write_m7509 ();
extern "C" void StreamWriter_LowLevelWrite_m7510 ();
extern "C" void StreamWriter_LowLevelWrite_m7511 ();
extern "C" void StreamWriter_Write_m7512 ();
extern "C" void StreamWriter_Write_m7513 ();
extern "C" void StreamWriter_Write_m7514 ();
extern "C" void StreamWriter_Close_m7515 ();
extern "C" void StreamWriter_Finalize_m7516 ();
extern "C" void StringReader__ctor_m3279 ();
extern "C" void StringReader_Dispose_m7517 ();
extern "C" void StringReader_Peek_m7518 ();
extern "C" void StringReader_Read_m7519 ();
extern "C" void StringReader_Read_m7520 ();
extern "C" void StringReader_ReadLine_m7521 ();
extern "C" void StringReader_ReadToEnd_m7522 ();
extern "C" void StringReader_CheckObjectDisposedException_m7523 ();
extern "C" void NullTextReader__ctor_m7524 ();
extern "C" void NullTextReader_ReadLine_m7525 ();
extern "C" void TextReader__ctor_m7526 ();
extern "C" void TextReader__cctor_m7527 ();
extern "C" void TextReader_Dispose_m7528 ();
extern "C" void TextReader_Dispose_m7529 ();
extern "C" void TextReader_Peek_m7530 ();
extern "C" void TextReader_Read_m7531 ();
extern "C" void TextReader_Read_m7532 ();
extern "C" void TextReader_ReadLine_m7533 ();
extern "C" void TextReader_ReadToEnd_m7534 ();
extern "C" void TextReader_Synchronized_m7535 ();
extern "C" void SynchronizedReader__ctor_m7536 ();
extern "C" void SynchronizedReader_Peek_m7537 ();
extern "C" void SynchronizedReader_ReadLine_m7538 ();
extern "C" void SynchronizedReader_ReadToEnd_m7539 ();
extern "C" void SynchronizedReader_Read_m7540 ();
extern "C" void SynchronizedReader_Read_m7541 ();
extern "C" void NullTextWriter__ctor_m7542 ();
extern "C" void NullTextWriter_Write_m7543 ();
extern "C" void NullTextWriter_Write_m7544 ();
extern "C" void NullTextWriter_Write_m7545 ();
extern "C" void TextWriter__ctor_m7546 ();
extern "C" void TextWriter__cctor_m7547 ();
extern "C" void TextWriter_Close_m7548 ();
extern "C" void TextWriter_Dispose_m7549 ();
extern "C" void TextWriter_Dispose_m7550 ();
extern "C" void TextWriter_Flush_m7551 ();
extern "C" void TextWriter_Synchronized_m7552 ();
extern "C" void TextWriter_Write_m7553 ();
extern "C" void TextWriter_Write_m7554 ();
extern "C" void TextWriter_Write_m7555 ();
extern "C" void TextWriter_Write_m7556 ();
extern "C" void TextWriter_WriteLine_m7557 ();
extern "C" void TextWriter_WriteLine_m7558 ();
extern "C" void SynchronizedWriter__ctor_m7559 ();
extern "C" void SynchronizedWriter_Close_m7560 ();
extern "C" void SynchronizedWriter_Flush_m7561 ();
extern "C" void SynchronizedWriter_Write_m7562 ();
extern "C" void SynchronizedWriter_Write_m7563 ();
extern "C" void SynchronizedWriter_Write_m7564 ();
extern "C" void SynchronizedWriter_Write_m7565 ();
extern "C" void SynchronizedWriter_WriteLine_m7566 ();
extern "C" void SynchronizedWriter_WriteLine_m7567 ();
extern "C" void UnexceptionalStreamReader__ctor_m7568 ();
extern "C" void UnexceptionalStreamReader__cctor_m7569 ();
extern "C" void UnexceptionalStreamReader_Peek_m7570 ();
extern "C" void UnexceptionalStreamReader_Read_m7571 ();
extern "C" void UnexceptionalStreamReader_Read_m7572 ();
extern "C" void UnexceptionalStreamReader_CheckEOL_m7573 ();
extern "C" void UnexceptionalStreamReader_ReadLine_m7574 ();
extern "C" void UnexceptionalStreamReader_ReadToEnd_m7575 ();
extern "C" void UnexceptionalStreamWriter__ctor_m7576 ();
extern "C" void UnexceptionalStreamWriter_Flush_m7577 ();
extern "C" void UnexceptionalStreamWriter_Write_m7578 ();
extern "C" void UnexceptionalStreamWriter_Write_m7579 ();
extern "C" void UnexceptionalStreamWriter_Write_m7580 ();
extern "C" void UnexceptionalStreamWriter_Write_m7581 ();
extern "C" void AssemblyBuilder_get_Location_m7582 ();
extern "C" void AssemblyBuilder_GetModulesInternal_m7583 ();
extern "C" void AssemblyBuilder_GetTypes_m7584 ();
extern "C" void AssemblyBuilder_get_IsCompilerContext_m7585 ();
extern "C" void AssemblyBuilder_not_supported_m7586 ();
extern "C" void AssemblyBuilder_UnprotectedGetName_m7587 ();
extern "C" void ConstructorBuilder__ctor_m7588 ();
extern "C" void ConstructorBuilder_get_CallingConvention_m7589 ();
extern "C" void ConstructorBuilder_get_TypeBuilder_m7590 ();
extern "C" void ConstructorBuilder_GetParameters_m7591 ();
extern "C" void ConstructorBuilder_GetParametersInternal_m7592 ();
extern "C" void ConstructorBuilder_GetParameterCount_m7593 ();
extern "C" void ConstructorBuilder_Invoke_m7594 ();
extern "C" void ConstructorBuilder_Invoke_m7595 ();
extern "C" void ConstructorBuilder_get_MethodHandle_m7596 ();
extern "C" void ConstructorBuilder_get_Attributes_m7597 ();
extern "C" void ConstructorBuilder_get_ReflectedType_m7598 ();
extern "C" void ConstructorBuilder_get_DeclaringType_m7599 ();
extern "C" void ConstructorBuilder_get_Name_m7600 ();
extern "C" void ConstructorBuilder_IsDefined_m7601 ();
extern "C" void ConstructorBuilder_GetCustomAttributes_m7602 ();
extern "C" void ConstructorBuilder_GetCustomAttributes_m7603 ();
extern "C" void ConstructorBuilder_GetILGenerator_m7604 ();
extern "C" void ConstructorBuilder_GetILGenerator_m7605 ();
extern "C" void ConstructorBuilder_GetToken_m7606 ();
extern "C" void ConstructorBuilder_get_Module_m7607 ();
extern "C" void ConstructorBuilder_ToString_m7608 ();
extern "C" void ConstructorBuilder_fixup_m7609 ();
extern "C" void ConstructorBuilder_get_next_table_index_m7610 ();
extern "C" void ConstructorBuilder_get_IsCompilerContext_m7611 ();
extern "C" void ConstructorBuilder_not_supported_m7612 ();
extern "C" void ConstructorBuilder_not_created_m7613 ();
extern "C" void EnumBuilder_get_Assembly_m7614 ();
extern "C" void EnumBuilder_get_AssemblyQualifiedName_m7615 ();
extern "C" void EnumBuilder_get_BaseType_m7616 ();
extern "C" void EnumBuilder_get_DeclaringType_m7617 ();
extern "C" void EnumBuilder_get_FullName_m7618 ();
extern "C" void EnumBuilder_get_Module_m7619 ();
extern "C" void EnumBuilder_get_Name_m7620 ();
extern "C" void EnumBuilder_get_Namespace_m7621 ();
extern "C" void EnumBuilder_get_ReflectedType_m7622 ();
extern "C" void EnumBuilder_get_TypeHandle_m7623 ();
extern "C" void EnumBuilder_get_UnderlyingSystemType_m7624 ();
extern "C" void EnumBuilder_GetAttributeFlagsImpl_m7625 ();
extern "C" void EnumBuilder_GetConstructorImpl_m7626 ();
extern "C" void EnumBuilder_GetConstructors_m7627 ();
extern "C" void EnumBuilder_GetCustomAttributes_m7628 ();
extern "C" void EnumBuilder_GetCustomAttributes_m7629 ();
extern "C" void EnumBuilder_GetElementType_m7630 ();
extern "C" void EnumBuilder_GetEvent_m7631 ();
extern "C" void EnumBuilder_GetField_m7632 ();
extern "C" void EnumBuilder_GetFields_m7633 ();
extern "C" void EnumBuilder_GetInterfaces_m7634 ();
extern "C" void EnumBuilder_GetMethodImpl_m7635 ();
extern "C" void EnumBuilder_GetMethods_m7636 ();
extern "C" void EnumBuilder_GetProperties_m7637 ();
extern "C" void EnumBuilder_GetPropertyImpl_m7638 ();
extern "C" void EnumBuilder_HasElementTypeImpl_m7639 ();
extern "C" void EnumBuilder_InvokeMember_m7640 ();
extern "C" void EnumBuilder_IsArrayImpl_m7641 ();
extern "C" void EnumBuilder_IsByRefImpl_m7642 ();
extern "C" void EnumBuilder_IsPointerImpl_m7643 ();
extern "C" void EnumBuilder_IsPrimitiveImpl_m7644 ();
extern "C" void EnumBuilder_IsValueTypeImpl_m7645 ();
extern "C" void EnumBuilder_IsDefined_m7646 ();
extern "C" void EnumBuilder_CreateNotSupportedException_m7647 ();
extern "C" void FieldBuilder_get_Attributes_m7648 ();
extern "C" void FieldBuilder_get_DeclaringType_m7649 ();
extern "C" void FieldBuilder_get_FieldHandle_m7650 ();
extern "C" void FieldBuilder_get_FieldType_m7651 ();
extern "C" void FieldBuilder_get_Name_m7652 ();
extern "C" void FieldBuilder_get_ReflectedType_m7653 ();
extern "C" void FieldBuilder_GetCustomAttributes_m7654 ();
extern "C" void FieldBuilder_GetCustomAttributes_m7655 ();
extern "C" void FieldBuilder_GetValue_m7656 ();
extern "C" void FieldBuilder_IsDefined_m7657 ();
extern "C" void FieldBuilder_GetFieldOffset_m7658 ();
extern "C" void FieldBuilder_SetValue_m7659 ();
extern "C" void FieldBuilder_get_UMarshal_m7660 ();
extern "C" void FieldBuilder_CreateNotSupportedException_m7661 ();
extern "C" void FieldBuilder_get_Module_m7662 ();
extern "C" void GenericTypeParameterBuilder_IsSubclassOf_m7663 ();
extern "C" void GenericTypeParameterBuilder_GetAttributeFlagsImpl_m7664 ();
extern "C" void GenericTypeParameterBuilder_GetConstructorImpl_m7665 ();
extern "C" void GenericTypeParameterBuilder_GetConstructors_m7666 ();
extern "C" void GenericTypeParameterBuilder_GetEvent_m7667 ();
extern "C" void GenericTypeParameterBuilder_GetField_m7668 ();
extern "C" void GenericTypeParameterBuilder_GetFields_m7669 ();
extern "C" void GenericTypeParameterBuilder_GetInterfaces_m7670 ();
extern "C" void GenericTypeParameterBuilder_GetMethods_m7671 ();
extern "C" void GenericTypeParameterBuilder_GetMethodImpl_m7672 ();
extern "C" void GenericTypeParameterBuilder_GetProperties_m7673 ();
extern "C" void GenericTypeParameterBuilder_GetPropertyImpl_m7674 ();
extern "C" void GenericTypeParameterBuilder_HasElementTypeImpl_m7675 ();
extern "C" void GenericTypeParameterBuilder_IsAssignableFrom_m7676 ();
extern "C" void GenericTypeParameterBuilder_IsInstanceOfType_m7677 ();
extern "C" void GenericTypeParameterBuilder_IsArrayImpl_m7678 ();
extern "C" void GenericTypeParameterBuilder_IsByRefImpl_m7679 ();
extern "C" void GenericTypeParameterBuilder_IsPointerImpl_m7680 ();
extern "C" void GenericTypeParameterBuilder_IsPrimitiveImpl_m7681 ();
extern "C" void GenericTypeParameterBuilder_IsValueTypeImpl_m7682 ();
extern "C" void GenericTypeParameterBuilder_InvokeMember_m7683 ();
extern "C" void GenericTypeParameterBuilder_GetElementType_m7684 ();
extern "C" void GenericTypeParameterBuilder_get_UnderlyingSystemType_m7685 ();
extern "C" void GenericTypeParameterBuilder_get_Assembly_m7686 ();
extern "C" void GenericTypeParameterBuilder_get_AssemblyQualifiedName_m7687 ();
extern "C" void GenericTypeParameterBuilder_get_BaseType_m7688 ();
extern "C" void GenericTypeParameterBuilder_get_FullName_m7689 ();
extern "C" void GenericTypeParameterBuilder_IsDefined_m7690 ();
extern "C" void GenericTypeParameterBuilder_GetCustomAttributes_m7691 ();
extern "C" void GenericTypeParameterBuilder_GetCustomAttributes_m7692 ();
extern "C" void GenericTypeParameterBuilder_get_Name_m7693 ();
extern "C" void GenericTypeParameterBuilder_get_Namespace_m7694 ();
extern "C" void GenericTypeParameterBuilder_get_Module_m7695 ();
extern "C" void GenericTypeParameterBuilder_get_DeclaringType_m7696 ();
extern "C" void GenericTypeParameterBuilder_get_ReflectedType_m7697 ();
extern "C" void GenericTypeParameterBuilder_get_TypeHandle_m7698 ();
extern "C" void GenericTypeParameterBuilder_GetGenericArguments_m7699 ();
extern "C" void GenericTypeParameterBuilder_GetGenericTypeDefinition_m7700 ();
extern "C" void GenericTypeParameterBuilder_get_ContainsGenericParameters_m7701 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericParameter_m7702 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericType_m7703 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericTypeDefinition_m7704 ();
extern "C" void GenericTypeParameterBuilder_not_supported_m7705 ();
extern "C" void GenericTypeParameterBuilder_ToString_m7706 ();
extern "C" void GenericTypeParameterBuilder_Equals_m7707 ();
extern "C" void GenericTypeParameterBuilder_GetHashCode_m7708 ();
extern "C" void GenericTypeParameterBuilder_MakeGenericType_m7709 ();
extern "C" void ILGenerator__ctor_m7710 ();
extern "C" void ILGenerator__cctor_m7711 ();
extern "C" void ILGenerator_add_token_fixup_m7712 ();
extern "C" void ILGenerator_make_room_m7713 ();
extern "C" void ILGenerator_emit_int_m7714 ();
extern "C" void ILGenerator_ll_emit_m7715 ();
extern "C" void ILGenerator_Emit_m7716 ();
extern "C" void ILGenerator_Emit_m7717 ();
extern "C" void ILGenerator_label_fixup_m7718 ();
extern "C" void ILGenerator_Mono_GetCurrentOffset_m7719 ();
extern "C" void MethodBuilder_get_ContainsGenericParameters_m7720 ();
extern "C" void MethodBuilder_get_MethodHandle_m7721 ();
extern "C" void MethodBuilder_get_ReturnType_m7722 ();
extern "C" void MethodBuilder_get_ReflectedType_m7723 ();
extern "C" void MethodBuilder_get_DeclaringType_m7724 ();
extern "C" void MethodBuilder_get_Name_m7725 ();
extern "C" void MethodBuilder_get_Attributes_m7726 ();
extern "C" void MethodBuilder_get_CallingConvention_m7727 ();
extern "C" void MethodBuilder_GetBaseDefinition_m7728 ();
extern "C" void MethodBuilder_GetParameters_m7729 ();
extern "C" void MethodBuilder_GetParameterCount_m7730 ();
extern "C" void MethodBuilder_Invoke_m7731 ();
extern "C" void MethodBuilder_IsDefined_m7732 ();
extern "C" void MethodBuilder_GetCustomAttributes_m7733 ();
extern "C" void MethodBuilder_GetCustomAttributes_m7734 ();
extern "C" void MethodBuilder_check_override_m7735 ();
extern "C" void MethodBuilder_fixup_m7736 ();
extern "C" void MethodBuilder_ToString_m7737 ();
extern "C" void MethodBuilder_Equals_m7738 ();
extern "C" void MethodBuilder_GetHashCode_m7739 ();
extern "C" void MethodBuilder_get_next_table_index_m7740 ();
extern "C" void MethodBuilder_NotSupported_m7741 ();
extern "C" void MethodBuilder_MakeGenericMethod_m7742 ();
extern "C" void MethodBuilder_get_IsGenericMethodDefinition_m7743 ();
extern "C" void MethodBuilder_get_IsGenericMethod_m7744 ();
extern "C" void MethodBuilder_GetGenericArguments_m7745 ();
extern "C" void MethodBuilder_get_Module_m7746 ();
extern "C" void MethodToken__ctor_m7747 ();
extern "C" void MethodToken__cctor_m7748 ();
extern "C" void MethodToken_Equals_m7749 ();
extern "C" void MethodToken_GetHashCode_m7750 ();
extern "C" void MethodToken_get_Token_m7751 ();
extern "C" void ModuleBuilder__cctor_m7752 ();
extern "C" void ModuleBuilder_get_next_table_index_m7753 ();
extern "C" void ModuleBuilder_GetTypes_m7754 ();
extern "C" void ModuleBuilder_getToken_m7755 ();
extern "C" void ModuleBuilder_GetToken_m7756 ();
extern "C" void ModuleBuilder_RegisterToken_m7757 ();
extern "C" void ModuleBuilder_GetTokenGenerator_m7758 ();
extern "C" void ModuleBuilderTokenGenerator__ctor_m7759 ();
extern "C" void ModuleBuilderTokenGenerator_GetToken_m7760 ();
extern "C" void OpCode__ctor_m7761 ();
extern "C" void OpCode_GetHashCode_m7762 ();
extern "C" void OpCode_Equals_m7763 ();
extern "C" void OpCode_ToString_m7764 ();
extern "C" void OpCode_get_Name_m7765 ();
extern "C" void OpCode_get_Size_m7766 ();
extern "C" void OpCode_get_StackBehaviourPop_m7767 ();
extern "C" void OpCode_get_StackBehaviourPush_m7768 ();
extern "C" void OpCodeNames__cctor_m7769 ();
extern "C" void OpCodes__cctor_m7770 ();
extern "C" void ParameterBuilder_get_Attributes_m7771 ();
extern "C" void ParameterBuilder_get_Name_m7772 ();
extern "C" void ParameterBuilder_get_Position_m7773 ();
extern "C" void PropertyBuilder_get_Attributes_m7774 ();
extern "C" void PropertyBuilder_get_CanRead_m7775 ();
extern "C" void PropertyBuilder_get_CanWrite_m7776 ();
extern "C" void PropertyBuilder_get_DeclaringType_m7777 ();
extern "C" void PropertyBuilder_get_Name_m7778 ();
extern "C" void PropertyBuilder_get_PropertyType_m7779 ();
extern "C" void PropertyBuilder_get_ReflectedType_m7780 ();
extern "C" void PropertyBuilder_GetAccessors_m7781 ();
extern "C" void PropertyBuilder_GetCustomAttributes_m7782 ();
extern "C" void PropertyBuilder_GetCustomAttributes_m7783 ();
extern "C" void PropertyBuilder_GetGetMethod_m7784 ();
extern "C" void PropertyBuilder_GetIndexParameters_m7785 ();
extern "C" void PropertyBuilder_GetSetMethod_m7786 ();
extern "C" void PropertyBuilder_GetValue_m7787 ();
extern "C" void PropertyBuilder_GetValue_m7788 ();
extern "C" void PropertyBuilder_IsDefined_m7789 ();
extern "C" void PropertyBuilder_SetValue_m7790 ();
extern "C" void PropertyBuilder_SetValue_m7791 ();
extern "C" void PropertyBuilder_get_Module_m7792 ();
extern "C" void PropertyBuilder_not_supported_m7793 ();
extern "C" void TypeBuilder_GetAttributeFlagsImpl_m7794 ();
extern "C" void TypeBuilder_setup_internal_class_m7795 ();
extern "C" void TypeBuilder_create_generic_class_m7796 ();
extern "C" void TypeBuilder_get_Assembly_m7797 ();
extern "C" void TypeBuilder_get_AssemblyQualifiedName_m7798 ();
extern "C" void TypeBuilder_get_BaseType_m7799 ();
extern "C" void TypeBuilder_get_DeclaringType_m7800 ();
extern "C" void TypeBuilder_get_UnderlyingSystemType_m7801 ();
extern "C" void TypeBuilder_get_FullName_m7802 ();
extern "C" void TypeBuilder_get_Module_m7803 ();
extern "C" void TypeBuilder_get_Name_m7804 ();
extern "C" void TypeBuilder_get_Namespace_m7805 ();
extern "C" void TypeBuilder_get_ReflectedType_m7806 ();
extern "C" void TypeBuilder_GetConstructorImpl_m7807 ();
extern "C" void TypeBuilder_IsDefined_m7808 ();
extern "C" void TypeBuilder_GetCustomAttributes_m7809 ();
extern "C" void TypeBuilder_GetCustomAttributes_m7810 ();
extern "C" void TypeBuilder_DefineConstructor_m7811 ();
extern "C" void TypeBuilder_DefineConstructor_m7812 ();
extern "C" void TypeBuilder_DefineDefaultConstructor_m7813 ();
extern "C" void TypeBuilder_create_runtime_class_m7814 ();
extern "C" void TypeBuilder_is_nested_in_m7815 ();
extern "C" void TypeBuilder_has_ctor_method_m7816 ();
extern "C" void TypeBuilder_CreateType_m7817 ();
extern "C" void TypeBuilder_GetConstructors_m7818 ();
extern "C" void TypeBuilder_GetConstructorsInternal_m7819 ();
extern "C" void TypeBuilder_GetElementType_m7820 ();
extern "C" void TypeBuilder_GetEvent_m7821 ();
extern "C" void TypeBuilder_GetField_m7822 ();
extern "C" void TypeBuilder_GetFields_m7823 ();
extern "C" void TypeBuilder_GetInterfaces_m7824 ();
extern "C" void TypeBuilder_GetMethodsByName_m7825 ();
extern "C" void TypeBuilder_GetMethods_m7826 ();
extern "C" void TypeBuilder_GetMethodImpl_m7827 ();
extern "C" void TypeBuilder_GetProperties_m7828 ();
extern "C" void TypeBuilder_GetPropertyImpl_m7829 ();
extern "C" void TypeBuilder_HasElementTypeImpl_m7830 ();
extern "C" void TypeBuilder_InvokeMember_m7831 ();
extern "C" void TypeBuilder_IsArrayImpl_m7832 ();
extern "C" void TypeBuilder_IsByRefImpl_m7833 ();
extern "C" void TypeBuilder_IsPointerImpl_m7834 ();
extern "C" void TypeBuilder_IsPrimitiveImpl_m7835 ();
extern "C" void TypeBuilder_IsValueTypeImpl_m7836 ();
extern "C" void TypeBuilder_MakeGenericType_m7837 ();
extern "C" void TypeBuilder_get_TypeHandle_m7838 ();
extern "C" void TypeBuilder_SetParent_m7839 ();
extern "C" void TypeBuilder_get_next_table_index_m7840 ();
extern "C" void TypeBuilder_get_IsCompilerContext_m7841 ();
extern "C" void TypeBuilder_get_is_created_m7842 ();
extern "C" void TypeBuilder_not_supported_m7843 ();
extern "C" void TypeBuilder_check_not_created_m7844 ();
extern "C" void TypeBuilder_check_created_m7845 ();
extern "C" void TypeBuilder_ToString_m7846 ();
extern "C" void TypeBuilder_IsAssignableFrom_m7847 ();
extern "C" void TypeBuilder_IsSubclassOf_m7848 ();
extern "C" void TypeBuilder_IsAssignableTo_m7849 ();
extern "C" void TypeBuilder_GetGenericArguments_m7850 ();
extern "C" void TypeBuilder_GetGenericTypeDefinition_m7851 ();
extern "C" void TypeBuilder_get_ContainsGenericParameters_m7852 ();
extern "C" void TypeBuilder_get_IsGenericParameter_m7853 ();
extern "C" void TypeBuilder_get_IsGenericTypeDefinition_m7854 ();
extern "C" void TypeBuilder_get_IsGenericType_m7855 ();
extern "C" void UnmanagedMarshal_ToMarshalAsAttribute_m7856 ();
extern "C" void AmbiguousMatchException__ctor_m7857 ();
extern "C" void AmbiguousMatchException__ctor_m7858 ();
extern "C" void AmbiguousMatchException__ctor_m7859 ();
extern "C" void ResolveEventHolder__ctor_m7860 ();
extern "C" void Assembly__ctor_m7861 ();
extern "C" void Assembly_get_code_base_m7862 ();
extern "C" void Assembly_get_fullname_m7863 ();
extern "C" void Assembly_get_location_m7864 ();
extern "C" void Assembly_GetCodeBase_m7865 ();
extern "C" void Assembly_get_FullName_m7866 ();
extern "C" void Assembly_get_Location_m7867 ();
extern "C" void Assembly_IsDefined_m7868 ();
extern "C" void Assembly_GetCustomAttributes_m7869 ();
extern "C" void Assembly_GetManifestResourceInternal_m7870 ();
extern "C" void Assembly_GetTypes_m7871 ();
extern "C" void Assembly_GetTypes_m7872 ();
extern "C" void Assembly_GetType_m7873 ();
extern "C" void Assembly_GetType_m7874 ();
extern "C" void Assembly_InternalGetType_m7875 ();
extern "C" void Assembly_GetType_m7876 ();
extern "C" void Assembly_FillName_m7877 ();
extern "C" void Assembly_GetName_m7878 ();
extern "C" void Assembly_GetName_m7879 ();
extern "C" void Assembly_UnprotectedGetName_m7880 ();
extern "C" void Assembly_ToString_m7881 ();
extern "C" void Assembly_Load_m7882 ();
extern "C" void Assembly_GetModule_m7883 ();
extern "C" void Assembly_GetModulesInternal_m7884 ();
extern "C" void Assembly_GetModules_m7885 ();
extern "C" void Assembly_GetExecutingAssembly_m7886 ();
extern "C" void AssemblyCompanyAttribute__ctor_m7887 ();
extern "C" void AssemblyConfigurationAttribute__ctor_m7888 ();
extern "C" void AssemblyCopyrightAttribute__ctor_m7889 ();
extern "C" void AssemblyDefaultAliasAttribute__ctor_m7890 ();
extern "C" void AssemblyDelaySignAttribute__ctor_m7891 ();
extern "C" void AssemblyDescriptionAttribute__ctor_m7892 ();
extern "C" void AssemblyFileVersionAttribute__ctor_m7893 ();
extern "C" void AssemblyInformationalVersionAttribute__ctor_m7894 ();
extern "C" void AssemblyKeyFileAttribute__ctor_m7895 ();
extern "C" void AssemblyName__ctor_m7896 ();
extern "C" void AssemblyName__ctor_m7897 ();
extern "C" void AssemblyName_get_Name_m7898 ();
extern "C" void AssemblyName_get_Flags_m7899 ();
extern "C" void AssemblyName_get_FullName_m7900 ();
extern "C" void AssemblyName_get_Version_m7901 ();
extern "C" void AssemblyName_set_Version_m7902 ();
extern "C" void AssemblyName_ToString_m7903 ();
extern "C" void AssemblyName_get_IsPublicKeyValid_m7904 ();
extern "C" void AssemblyName_InternalGetPublicKeyToken_m7905 ();
extern "C" void AssemblyName_ComputePublicKeyToken_m7906 ();
extern "C" void AssemblyName_SetPublicKey_m7907 ();
extern "C" void AssemblyName_SetPublicKeyToken_m7908 ();
extern "C" void AssemblyName_GetObjectData_m7909 ();
extern "C" void AssemblyName_OnDeserialization_m7910 ();
extern "C" void AssemblyProductAttribute__ctor_m7911 ();
extern "C" void AssemblyTitleAttribute__ctor_m7912 ();
extern "C" void AssemblyTrademarkAttribute__ctor_m7913 ();
extern "C" void Default__ctor_m7914 ();
extern "C" void Default_BindToMethod_m7915 ();
extern "C" void Default_ReorderParameters_m7916 ();
extern "C" void Default_IsArrayAssignable_m7917 ();
extern "C" void Default_ChangeType_m7918 ();
extern "C" void Default_ReorderArgumentArray_m7919 ();
extern "C" void Default_check_type_m7920 ();
extern "C" void Default_check_arguments_m7921 ();
extern "C" void Default_SelectMethod_m7922 ();
extern "C" void Default_SelectMethod_m7923 ();
extern "C" void Default_GetBetterMethod_m7924 ();
extern "C" void Default_CompareCloserType_m7925 ();
extern "C" void Default_SelectProperty_m7926 ();
extern "C" void Default_check_arguments_with_score_m7927 ();
extern "C" void Default_check_type_with_score_m7928 ();
extern "C" void Binder__ctor_m7929 ();
extern "C" void Binder__cctor_m7930 ();
extern "C" void Binder_get_DefaultBinder_m7931 ();
extern "C" void Binder_ConvertArgs_m7932 ();
extern "C" void Binder_GetDerivedLevel_m7933 ();
extern "C" void Binder_FindMostDerivedMatch_m7934 ();
extern "C" void ConstructorInfo__ctor_m7935 ();
extern "C" void ConstructorInfo__cctor_m7936 ();
extern "C" void ConstructorInfo_get_MemberType_m7937 ();
extern "C" void ConstructorInfo_Invoke_m3359 ();
extern "C" void AddEventAdapter__ctor_m7938 ();
extern "C" void AddEventAdapter_Invoke_m7939 ();
extern "C" void AddEventAdapter_BeginInvoke_m7940 ();
extern "C" void AddEventAdapter_EndInvoke_m7941 ();
extern "C" void EventInfo__ctor_m7942 ();
extern "C" void EventInfo_get_EventHandlerType_m7943 ();
extern "C" void EventInfo_get_MemberType_m7944 ();
extern "C" void FieldInfo__ctor_m7945 ();
extern "C" void FieldInfo_get_MemberType_m7946 ();
extern "C" void FieldInfo_get_IsLiteral_m7947 ();
extern "C" void FieldInfo_get_IsStatic_m7948 ();
extern "C" void FieldInfo_get_IsInitOnly_m7949 ();
extern "C" void FieldInfo_get_IsPublic_m7950 ();
extern "C" void FieldInfo_get_IsNotSerialized_m7951 ();
extern "C" void FieldInfo_SetValue_m7952 ();
extern "C" void FieldInfo_internal_from_handle_type_m7953 ();
extern "C" void FieldInfo_GetFieldFromHandle_m7954 ();
extern "C" void FieldInfo_GetFieldOffset_m7955 ();
extern "C" void FieldInfo_GetUnmanagedMarshal_m7956 ();
extern "C" void FieldInfo_get_UMarshal_m7957 ();
extern "C" void FieldInfo_GetPseudoCustomAttributes_m7958 ();
extern "C" void MemberInfoSerializationHolder__ctor_m7959 ();
extern "C" void MemberInfoSerializationHolder_Serialize_m7960 ();
extern "C" void MemberInfoSerializationHolder_Serialize_m7961 ();
extern "C" void MemberInfoSerializationHolder_GetObjectData_m7962 ();
extern "C" void MemberInfoSerializationHolder_GetRealObject_m7963 ();
extern "C" void MethodBase__ctor_m7964 ();
extern "C" void MethodBase_GetMethodFromHandleNoGenericCheck_m7965 ();
extern "C" void MethodBase_GetMethodFromIntPtr_m7966 ();
extern "C" void MethodBase_GetMethodFromHandle_m7967 ();
extern "C" void MethodBase_GetMethodFromHandleInternalType_m7968 ();
extern "C" void MethodBase_GetParameterCount_m7969 ();
extern "C" void MethodBase_Invoke_m7970 ();
extern "C" void MethodBase_get_CallingConvention_m7971 ();
extern "C" void MethodBase_get_IsPublic_m7972 ();
extern "C" void MethodBase_get_IsStatic_m7973 ();
extern "C" void MethodBase_get_IsVirtual_m7974 ();
extern "C" void MethodBase_get_IsAbstract_m7975 ();
extern "C" void MethodBase_get_next_table_index_m7976 ();
extern "C" void MethodBase_GetGenericArguments_m7977 ();
extern "C" void MethodBase_get_ContainsGenericParameters_m7978 ();
extern "C" void MethodBase_get_IsGenericMethodDefinition_m7979 ();
extern "C" void MethodBase_get_IsGenericMethod_m7980 ();
extern "C" void MethodInfo__ctor_m7981 ();
extern "C" void MethodInfo_get_MemberType_m7982 ();
extern "C" void MethodInfo_get_ReturnType_m7983 ();
extern "C" void MethodInfo_MakeGenericMethod_m7984 ();
extern "C" void MethodInfo_GetGenericArguments_m7985 ();
extern "C" void MethodInfo_get_IsGenericMethod_m7986 ();
extern "C" void MethodInfo_get_IsGenericMethodDefinition_m7987 ();
extern "C" void MethodInfo_get_ContainsGenericParameters_m7988 ();
extern "C" void Missing__ctor_m7989 ();
extern "C" void Missing__cctor_m7990 ();
extern "C" void Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m7991 ();
extern "C" void Module__ctor_m7992 ();
extern "C" void Module__cctor_m7993 ();
extern "C" void Module_get_Assembly_m7994 ();
extern "C" void Module_get_ScopeName_m7995 ();
extern "C" void Module_GetCustomAttributes_m7996 ();
extern "C" void Module_GetObjectData_m7997 ();
extern "C" void Module_InternalGetTypes_m7998 ();
extern "C" void Module_GetTypes_m7999 ();
extern "C" void Module_IsDefined_m8000 ();
extern "C" void Module_IsResource_m8001 ();
extern "C" void Module_ToString_m8002 ();
extern "C" void Module_filter_by_type_name_m8003 ();
extern "C" void Module_filter_by_type_name_ignore_case_m8004 ();
extern "C" void MonoEventInfo_get_event_info_m8005 ();
extern "C" void MonoEventInfo_GetEventInfo_m8006 ();
extern "C" void MonoEvent__ctor_m8007 ();
extern "C" void MonoEvent_get_Attributes_m8008 ();
extern "C" void MonoEvent_GetAddMethod_m8009 ();
extern "C" void MonoEvent_get_DeclaringType_m8010 ();
extern "C" void MonoEvent_get_ReflectedType_m8011 ();
extern "C" void MonoEvent_get_Name_m8012 ();
extern "C" void MonoEvent_ToString_m8013 ();
extern "C" void MonoEvent_IsDefined_m8014 ();
extern "C" void MonoEvent_GetCustomAttributes_m8015 ();
extern "C" void MonoEvent_GetCustomAttributes_m8016 ();
extern "C" void MonoEvent_GetObjectData_m8017 ();
extern "C" void MonoField__ctor_m8018 ();
extern "C" void MonoField_get_Attributes_m8019 ();
extern "C" void MonoField_get_FieldHandle_m8020 ();
extern "C" void MonoField_get_FieldType_m8021 ();
extern "C" void MonoField_GetParentType_m8022 ();
extern "C" void MonoField_get_ReflectedType_m8023 ();
extern "C" void MonoField_get_DeclaringType_m8024 ();
extern "C" void MonoField_get_Name_m8025 ();
extern "C" void MonoField_IsDefined_m8026 ();
extern "C" void MonoField_GetCustomAttributes_m8027 ();
extern "C" void MonoField_GetCustomAttributes_m8028 ();
extern "C" void MonoField_GetFieldOffset_m8029 ();
extern "C" void MonoField_GetValueInternal_m8030 ();
extern "C" void MonoField_GetValue_m8031 ();
extern "C" void MonoField_ToString_m8032 ();
extern "C" void MonoField_SetValueInternal_m8033 ();
extern "C" void MonoField_SetValue_m8034 ();
extern "C" void MonoField_GetObjectData_m8035 ();
extern "C" void MonoField_CheckGeneric_m8036 ();
extern "C" void MonoGenericMethod__ctor_m8037 ();
extern "C" void MonoGenericMethod_get_ReflectedType_m8038 ();
extern "C" void MonoGenericCMethod__ctor_m8039 ();
extern "C" void MonoGenericCMethod_get_ReflectedType_m8040 ();
extern "C" void MonoMethodInfo_get_method_info_m8041 ();
extern "C" void MonoMethodInfo_GetMethodInfo_m8042 ();
extern "C" void MonoMethodInfo_GetDeclaringType_m8043 ();
extern "C" void MonoMethodInfo_GetReturnType_m8044 ();
extern "C" void MonoMethodInfo_GetAttributes_m8045 ();
extern "C" void MonoMethodInfo_GetCallingConvention_m8046 ();
extern "C" void MonoMethodInfo_get_parameter_info_m8047 ();
extern "C" void MonoMethodInfo_GetParametersInfo_m8048 ();
extern "C" void MonoMethod__ctor_m8049 ();
extern "C" void MonoMethod_get_name_m8050 ();
extern "C" void MonoMethod_get_base_definition_m8051 ();
extern "C" void MonoMethod_GetBaseDefinition_m8052 ();
extern "C" void MonoMethod_get_ReturnType_m8053 ();
extern "C" void MonoMethod_GetParameters_m8054 ();
extern "C" void MonoMethod_InternalInvoke_m8055 ();
extern "C" void MonoMethod_Invoke_m8056 ();
extern "C" void MonoMethod_get_MethodHandle_m8057 ();
extern "C" void MonoMethod_get_Attributes_m8058 ();
extern "C" void MonoMethod_get_CallingConvention_m8059 ();
extern "C" void MonoMethod_get_ReflectedType_m8060 ();
extern "C" void MonoMethod_get_DeclaringType_m8061 ();
extern "C" void MonoMethod_get_Name_m8062 ();
extern "C" void MonoMethod_IsDefined_m8063 ();
extern "C" void MonoMethod_GetCustomAttributes_m8064 ();
extern "C" void MonoMethod_GetCustomAttributes_m8065 ();
extern "C" void MonoMethod_GetDllImportAttribute_m8066 ();
extern "C" void MonoMethod_GetPseudoCustomAttributes_m8067 ();
extern "C" void MonoMethod_ShouldPrintFullName_m8068 ();
extern "C" void MonoMethod_ToString_m8069 ();
extern "C" void MonoMethod_GetObjectData_m8070 ();
extern "C" void MonoMethod_MakeGenericMethod_m8071 ();
extern "C" void MonoMethod_MakeGenericMethod_impl_m8072 ();
extern "C" void MonoMethod_GetGenericArguments_m8073 ();
extern "C" void MonoMethod_get_IsGenericMethodDefinition_m8074 ();
extern "C" void MonoMethod_get_IsGenericMethod_m8075 ();
extern "C" void MonoMethod_get_ContainsGenericParameters_m8076 ();
extern "C" void MonoCMethod__ctor_m8077 ();
extern "C" void MonoCMethod_GetParameters_m8078 ();
extern "C" void MonoCMethod_InternalInvoke_m8079 ();
extern "C" void MonoCMethod_Invoke_m8080 ();
extern "C" void MonoCMethod_Invoke_m8081 ();
extern "C" void MonoCMethod_get_MethodHandle_m8082 ();
extern "C" void MonoCMethod_get_Attributes_m8083 ();
extern "C" void MonoCMethod_get_CallingConvention_m8084 ();
extern "C" void MonoCMethod_get_ReflectedType_m8085 ();
extern "C" void MonoCMethod_get_DeclaringType_m8086 ();
extern "C" void MonoCMethod_get_Name_m8087 ();
extern "C" void MonoCMethod_IsDefined_m8088 ();
extern "C" void MonoCMethod_GetCustomAttributes_m8089 ();
extern "C" void MonoCMethod_GetCustomAttributes_m8090 ();
extern "C" void MonoCMethod_ToString_m8091 ();
extern "C" void MonoCMethod_GetObjectData_m8092 ();
extern "C" void MonoPropertyInfo_get_property_info_m8093 ();
extern "C" void MonoPropertyInfo_GetTypeModifiers_m8094 ();
extern "C" void GetterAdapter__ctor_m8095 ();
extern "C" void GetterAdapter_Invoke_m8096 ();
extern "C" void GetterAdapter_BeginInvoke_m8097 ();
extern "C" void GetterAdapter_EndInvoke_m8098 ();
extern "C" void MonoProperty__ctor_m8099 ();
extern "C" void MonoProperty_CachePropertyInfo_m8100 ();
extern "C" void MonoProperty_get_Attributes_m8101 ();
extern "C" void MonoProperty_get_CanRead_m8102 ();
extern "C" void MonoProperty_get_CanWrite_m8103 ();
extern "C" void MonoProperty_get_PropertyType_m8104 ();
extern "C" void MonoProperty_get_ReflectedType_m8105 ();
extern "C" void MonoProperty_get_DeclaringType_m8106 ();
extern "C" void MonoProperty_get_Name_m8107 ();
extern "C" void MonoProperty_GetAccessors_m8108 ();
extern "C" void MonoProperty_GetGetMethod_m8109 ();
extern "C" void MonoProperty_GetIndexParameters_m8110 ();
extern "C" void MonoProperty_GetSetMethod_m8111 ();
extern "C" void MonoProperty_IsDefined_m8112 ();
extern "C" void MonoProperty_GetCustomAttributes_m8113 ();
extern "C" void MonoProperty_GetCustomAttributes_m8114 ();
extern "C" void MonoProperty_CreateGetterDelegate_m8115 ();
extern "C" void MonoProperty_GetValue_m8116 ();
extern "C" void MonoProperty_GetValue_m8117 ();
extern "C" void MonoProperty_SetValue_m8118 ();
extern "C" void MonoProperty_ToString_m8119 ();
extern "C" void MonoProperty_GetOptionalCustomModifiers_m8120 ();
extern "C" void MonoProperty_GetRequiredCustomModifiers_m8121 ();
extern "C" void MonoProperty_GetObjectData_m8122 ();
extern "C" void ParameterInfo__ctor_m8123 ();
extern "C" void ParameterInfo__ctor_m8124 ();
extern "C" void ParameterInfo__ctor_m8125 ();
extern "C" void ParameterInfo_ToString_m8126 ();
extern "C" void ParameterInfo_get_ParameterType_m8127 ();
extern "C" void ParameterInfo_get_Attributes_m8128 ();
extern "C" void ParameterInfo_get_IsIn_m8129 ();
extern "C" void ParameterInfo_get_IsOptional_m8130 ();
extern "C" void ParameterInfo_get_IsOut_m8131 ();
extern "C" void ParameterInfo_get_IsRetval_m8132 ();
extern "C" void ParameterInfo_get_Member_m8133 ();
extern "C" void ParameterInfo_get_Name_m8134 ();
extern "C" void ParameterInfo_get_Position_m8135 ();
extern "C" void ParameterInfo_GetCustomAttributes_m8136 ();
extern "C" void ParameterInfo_IsDefined_m8137 ();
extern "C" void ParameterInfo_GetPseudoCustomAttributes_m8138 ();
extern "C" void Pointer__ctor_m8139 ();
extern "C" void Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m8140 ();
extern "C" void PropertyInfo__ctor_m8141 ();
extern "C" void PropertyInfo_get_MemberType_m8142 ();
extern "C" void PropertyInfo_GetValue_m8143 ();
extern "C" void PropertyInfo_SetValue_m8144 ();
extern "C" void PropertyInfo_GetOptionalCustomModifiers_m8145 ();
extern "C" void PropertyInfo_GetRequiredCustomModifiers_m8146 ();
extern "C" void StrongNameKeyPair__ctor_m8147 ();
extern "C" void StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m8148 ();
extern "C" void StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m8149 ();
extern "C" void TargetException__ctor_m8150 ();
extern "C" void TargetException__ctor_m8151 ();
extern "C" void TargetException__ctor_m8152 ();
extern "C" void TargetInvocationException__ctor_m8153 ();
extern "C" void TargetInvocationException__ctor_m8154 ();
extern "C" void TargetParameterCountException__ctor_m8155 ();
extern "C" void TargetParameterCountException__ctor_m8156 ();
extern "C" void TargetParameterCountException__ctor_m8157 ();
extern "C" void NeutralResourcesLanguageAttribute__ctor_m8158 ();
extern "C" void SatelliteContractVersionAttribute__ctor_m8159 ();
extern "C" void CompilationRelaxationsAttribute__ctor_m8160 ();
extern "C" void DefaultDependencyAttribute__ctor_m8161 ();
extern "C" void StringFreezingAttribute__ctor_m8162 ();
extern "C" void CriticalFinalizerObject__ctor_m8163 ();
extern "C" void CriticalFinalizerObject_Finalize_m8164 ();
extern "C" void ReliabilityContractAttribute__ctor_m8165 ();
extern "C" void ClassInterfaceAttribute__ctor_m8166 ();
extern "C" void ComDefaultInterfaceAttribute__ctor_m8167 ();
extern "C" void DispIdAttribute__ctor_m8168 ();
extern "C" void GCHandle__ctor_m8169 ();
extern "C" void GCHandle_get_IsAllocated_m8170 ();
extern "C" void GCHandle_get_Target_m8171 ();
extern "C" void GCHandle_Alloc_m8172 ();
extern "C" void GCHandle_Free_m8173 ();
extern "C" void GCHandle_GetTarget_m8174 ();
extern "C" void GCHandle_GetTargetHandle_m8175 ();
extern "C" void GCHandle_FreeHandle_m8176 ();
extern "C" void GCHandle_Equals_m8177 ();
extern "C" void GCHandle_GetHashCode_m8178 ();
extern "C" void InterfaceTypeAttribute__ctor_m8179 ();
extern "C" void Marshal__cctor_m8180 ();
extern "C" void Marshal_copy_from_unmanaged_m8181 ();
extern "C" void Marshal_Copy_m8182 ();
extern "C" void Marshal_Copy_m8183 ();
extern "C" void MarshalDirectiveException__ctor_m8184 ();
extern "C" void MarshalDirectiveException__ctor_m8185 ();
extern "C" void PreserveSigAttribute__ctor_m8186 ();
extern "C" void SafeHandle__ctor_m8187 ();
extern "C" void SafeHandle_Close_m8188 ();
extern "C" void SafeHandle_DangerousAddRef_m8189 ();
extern "C" void SafeHandle_DangerousGetHandle_m8190 ();
extern "C" void SafeHandle_DangerousRelease_m8191 ();
extern "C" void SafeHandle_Dispose_m8192 ();
extern "C" void SafeHandle_Dispose_m8193 ();
extern "C" void SafeHandle_SetHandle_m8194 ();
extern "C" void SafeHandle_Finalize_m8195 ();
extern "C" void TypeLibImportClassAttribute__ctor_m8196 ();
extern "C" void TypeLibVersionAttribute__ctor_m8197 ();
extern "C" void ActivationServices_get_ConstructionActivator_m8198 ();
extern "C" void ActivationServices_CreateProxyFromAttributes_m8199 ();
extern "C" void ActivationServices_CreateConstructionCall_m8200 ();
extern "C" void ActivationServices_AllocateUninitializedClassInstance_m8201 ();
extern "C" void ActivationServices_EnableProxyActivation_m8202 ();
extern "C" void AppDomainLevelActivator__ctor_m8203 ();
extern "C" void ConstructionLevelActivator__ctor_m8204 ();
extern "C" void ContextLevelActivator__ctor_m8205 ();
extern "C" void UrlAttribute_get_UrlValue_m8206 ();
extern "C" void UrlAttribute_Equals_m8207 ();
extern "C" void UrlAttribute_GetHashCode_m8208 ();
extern "C" void UrlAttribute_GetPropertiesForNewContext_m8209 ();
extern "C" void UrlAttribute_IsContextOK_m8210 ();
extern "C" void ChannelInfo__ctor_m8211 ();
extern "C" void ChannelInfo_get_ChannelData_m8212 ();
extern "C" void ChannelServices__cctor_m8213 ();
extern "C" void ChannelServices_CreateClientChannelSinkChain_m8214 ();
extern "C" void ChannelServices_CreateClientChannelSinkChain_m8215 ();
extern "C" void ChannelServices_RegisterChannel_m8216 ();
extern "C" void ChannelServices_RegisterChannel_m8217 ();
extern "C" void ChannelServices_RegisterChannelConfig_m8218 ();
extern "C" void ChannelServices_CreateProvider_m8219 ();
extern "C" void ChannelServices_GetCurrentChannelInfo_m8220 ();
extern "C" void CrossAppDomainData__ctor_m8221 ();
extern "C" void CrossAppDomainData_get_DomainID_m8222 ();
extern "C" void CrossAppDomainData_get_ProcessID_m8223 ();
extern "C" void CrossAppDomainChannel__ctor_m8224 ();
extern "C" void CrossAppDomainChannel__cctor_m8225 ();
extern "C" void CrossAppDomainChannel_RegisterCrossAppDomainChannel_m8226 ();
extern "C" void CrossAppDomainChannel_get_ChannelName_m8227 ();
extern "C" void CrossAppDomainChannel_get_ChannelPriority_m8228 ();
extern "C" void CrossAppDomainChannel_get_ChannelData_m8229 ();
extern "C" void CrossAppDomainChannel_StartListening_m8230 ();
extern "C" void CrossAppDomainChannel_CreateMessageSink_m8231 ();
extern "C" void CrossAppDomainSink__ctor_m8232 ();
extern "C" void CrossAppDomainSink__cctor_m8233 ();
extern "C" void CrossAppDomainSink_GetSink_m8234 ();
extern "C" void CrossAppDomainSink_get_TargetDomainId_m8235 ();
extern "C" void SinkProviderData__ctor_m8236 ();
extern "C" void SinkProviderData_get_Children_m8237 ();
extern "C" void SinkProviderData_get_Properties_m8238 ();
extern "C" void Context__ctor_m8239 ();
extern "C" void Context__cctor_m8240 ();
extern "C" void Context_Finalize_m8241 ();
extern "C" void Context_get_DefaultContext_m8242 ();
extern "C" void Context_get_ContextID_m8243 ();
extern "C" void Context_get_ContextProperties_m8244 ();
extern "C" void Context_get_IsDefaultContext_m8245 ();
extern "C" void Context_get_NeedsContextSink_m8246 ();
extern "C" void Context_RegisterDynamicProperty_m8247 ();
extern "C" void Context_UnregisterDynamicProperty_m8248 ();
extern "C" void Context_GetDynamicPropertyCollection_m8249 ();
extern "C" void Context_NotifyGlobalDynamicSinks_m8250 ();
extern "C" void Context_get_HasGlobalDynamicSinks_m8251 ();
extern "C" void Context_NotifyDynamicSinks_m8252 ();
extern "C" void Context_get_HasDynamicSinks_m8253 ();
extern "C" void Context_get_HasExitSinks_m8254 ();
extern "C" void Context_GetProperty_m8255 ();
extern "C" void Context_SetProperty_m8256 ();
extern "C" void Context_Freeze_m8257 ();
extern "C" void Context_ToString_m8258 ();
extern "C" void Context_GetServerContextSinkChain_m8259 ();
extern "C" void Context_GetClientContextSinkChain_m8260 ();
extern "C" void Context_CreateServerObjectSinkChain_m8261 ();
extern "C" void Context_CreateEnvoySink_m8262 ();
extern "C" void Context_SwitchToContext_m8263 ();
extern "C" void Context_CreateNewContext_m8264 ();
extern "C" void Context_DoCallBack_m8265 ();
extern "C" void Context_AllocateDataSlot_m8266 ();
extern "C" void Context_AllocateNamedDataSlot_m8267 ();
extern "C" void Context_FreeNamedDataSlot_m8268 ();
extern "C" void Context_GetData_m8269 ();
extern "C" void Context_GetNamedDataSlot_m8270 ();
extern "C" void Context_SetData_m8271 ();
extern "C" void DynamicPropertyReg__ctor_m8272 ();
extern "C" void DynamicPropertyCollection__ctor_m8273 ();
extern "C" void DynamicPropertyCollection_get_HasProperties_m8274 ();
extern "C" void DynamicPropertyCollection_RegisterDynamicProperty_m8275 ();
extern "C" void DynamicPropertyCollection_UnregisterDynamicProperty_m8276 ();
extern "C" void DynamicPropertyCollection_NotifyMessage_m8277 ();
extern "C" void DynamicPropertyCollection_FindProperty_m8278 ();
extern "C" void ContextCallbackObject__ctor_m8279 ();
extern "C" void ContextCallbackObject_DoCallBack_m8280 ();
extern "C" void ContextAttribute__ctor_m8281 ();
extern "C" void ContextAttribute_get_Name_m8282 ();
extern "C" void ContextAttribute_Equals_m8283 ();
extern "C" void ContextAttribute_Freeze_m8284 ();
extern "C" void ContextAttribute_GetHashCode_m8285 ();
extern "C" void ContextAttribute_GetPropertiesForNewContext_m8286 ();
extern "C" void ContextAttribute_IsContextOK_m8287 ();
extern "C" void ContextAttribute_IsNewContextOK_m8288 ();
extern "C" void CrossContextChannel__ctor_m8289 ();
extern "C" void SynchronizationAttribute__ctor_m8290 ();
extern "C" void SynchronizationAttribute__ctor_m8291 ();
extern "C" void SynchronizationAttribute_set_Locked_m8292 ();
extern "C" void SynchronizationAttribute_ReleaseLock_m8293 ();
extern "C" void SynchronizationAttribute_GetPropertiesForNewContext_m8294 ();
extern "C" void SynchronizationAttribute_GetClientContextSink_m8295 ();
extern "C" void SynchronizationAttribute_GetServerContextSink_m8296 ();
extern "C" void SynchronizationAttribute_IsContextOK_m8297 ();
extern "C" void SynchronizationAttribute_ExitContext_m8298 ();
extern "C" void SynchronizationAttribute_EnterContext_m8299 ();
extern "C" void SynchronizedClientContextSink__ctor_m8300 ();
extern "C" void SynchronizedServerContextSink__ctor_m8301 ();
extern "C" void LeaseManager__ctor_m8302 ();
extern "C" void LeaseManager_SetPollTime_m8303 ();
extern "C" void LeaseSink__ctor_m8304 ();
extern "C" void LifetimeServices__cctor_m8305 ();
extern "C" void LifetimeServices_set_LeaseManagerPollTime_m8306 ();
extern "C" void LifetimeServices_set_LeaseTime_m8307 ();
extern "C" void LifetimeServices_set_RenewOnCallTime_m8308 ();
extern "C" void LifetimeServices_set_SponsorshipTimeout_m8309 ();
extern "C" void ArgInfo__ctor_m8310 ();
extern "C" void ArgInfo_GetInOutArgs_m8311 ();
extern "C" void AsyncResult__ctor_m8312 ();
extern "C" void AsyncResult_get_AsyncState_m8313 ();
extern "C" void AsyncResult_get_AsyncWaitHandle_m8314 ();
extern "C" void AsyncResult_get_CompletedSynchronously_m8315 ();
extern "C" void AsyncResult_get_IsCompleted_m8316 ();
extern "C" void AsyncResult_get_EndInvokeCalled_m8317 ();
extern "C" void AsyncResult_set_EndInvokeCalled_m8318 ();
extern "C" void AsyncResult_get_AsyncDelegate_m8319 ();
extern "C" void AsyncResult_get_NextSink_m8320 ();
extern "C" void AsyncResult_AsyncProcessMessage_m8321 ();
extern "C" void AsyncResult_GetReplyMessage_m8322 ();
extern "C" void AsyncResult_SetMessageCtrl_m8323 ();
extern "C" void AsyncResult_SetCompletedSynchronously_m8324 ();
extern "C" void AsyncResult_EndInvoke_m8325 ();
extern "C" void AsyncResult_SyncProcessMessage_m8326 ();
extern "C" void AsyncResult_get_CallMessage_m8327 ();
extern "C" void AsyncResult_set_CallMessage_m8328 ();
extern "C" void ClientContextTerminatorSink__ctor_m8329 ();
extern "C" void ConstructionCall__ctor_m8330 ();
extern "C" void ConstructionCall__ctor_m8331 ();
extern "C" void ConstructionCall_InitDictionary_m8332 ();
extern "C" void ConstructionCall_set_IsContextOk_m8333 ();
extern "C" void ConstructionCall_get_ActivationType_m8334 ();
extern "C" void ConstructionCall_get_ActivationTypeName_m8335 ();
extern "C" void ConstructionCall_get_Activator_m8336 ();
extern "C" void ConstructionCall_set_Activator_m8337 ();
extern "C" void ConstructionCall_get_CallSiteActivationAttributes_m8338 ();
extern "C" void ConstructionCall_SetActivationAttributes_m8339 ();
extern "C" void ConstructionCall_get_ContextProperties_m8340 ();
extern "C" void ConstructionCall_InitMethodProperty_m8341 ();
extern "C" void ConstructionCall_GetObjectData_m8342 ();
extern "C" void ConstructionCall_get_Properties_m8343 ();
extern "C" void ConstructionCallDictionary__ctor_m8344 ();
extern "C" void ConstructionCallDictionary__cctor_m8345 ();
extern "C" void ConstructionCallDictionary_GetMethodProperty_m8346 ();
extern "C" void ConstructionCallDictionary_SetMethodProperty_m8347 ();
extern "C" void EnvoyTerminatorSink__ctor_m8348 ();
extern "C" void EnvoyTerminatorSink__cctor_m8349 ();
extern "C" void Header__ctor_m8350 ();
extern "C" void Header__ctor_m8351 ();
extern "C" void Header__ctor_m8352 ();
extern "C" void LogicalCallContext__ctor_m8353 ();
extern "C" void LogicalCallContext__ctor_m8354 ();
extern "C" void LogicalCallContext_GetObjectData_m8355 ();
extern "C" void LogicalCallContext_SetData_m8356 ();
extern "C" void CallContextRemotingData__ctor_m8357 ();
extern "C" void MethodCall__ctor_m8358 ();
extern "C" void MethodCall__ctor_m8359 ();
extern "C" void MethodCall__ctor_m8360 ();
extern "C" void MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8361 ();
extern "C" void MethodCall_InitMethodProperty_m8362 ();
extern "C" void MethodCall_GetObjectData_m8363 ();
extern "C" void MethodCall_get_Args_m8364 ();
extern "C" void MethodCall_get_LogicalCallContext_m8365 ();
extern "C" void MethodCall_get_MethodBase_m8366 ();
extern "C" void MethodCall_get_MethodName_m8367 ();
extern "C" void MethodCall_get_MethodSignature_m8368 ();
extern "C" void MethodCall_get_Properties_m8369 ();
extern "C" void MethodCall_InitDictionary_m8370 ();
extern "C" void MethodCall_get_TypeName_m8371 ();
extern "C" void MethodCall_get_Uri_m8372 ();
extern "C" void MethodCall_set_Uri_m8373 ();
extern "C" void MethodCall_Init_m8374 ();
extern "C" void MethodCall_ResolveMethod_m8375 ();
extern "C" void MethodCall_CastTo_m8376 ();
extern "C" void MethodCall_GetTypeNameFromAssemblyQualifiedName_m8377 ();
extern "C" void MethodCall_get_GenericArguments_m8378 ();
extern "C" void MethodCallDictionary__ctor_m8379 ();
extern "C" void MethodCallDictionary__cctor_m8380 ();
extern "C" void DictionaryEnumerator__ctor_m8381 ();
extern "C" void DictionaryEnumerator_get_Current_m8382 ();
extern "C" void DictionaryEnumerator_MoveNext_m8383 ();
extern "C" void DictionaryEnumerator_get_Entry_m8384 ();
extern "C" void DictionaryEnumerator_get_Key_m8385 ();
extern "C" void DictionaryEnumerator_get_Value_m8386 ();
extern "C" void MethodDictionary__ctor_m8387 ();
extern "C" void MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m8388 ();
extern "C" void MethodDictionary_set_MethodKeys_m8389 ();
extern "C" void MethodDictionary_AllocInternalProperties_m8390 ();
extern "C" void MethodDictionary_GetInternalProperties_m8391 ();
extern "C" void MethodDictionary_IsOverridenKey_m8392 ();
extern "C" void MethodDictionary_get_Item_m8393 ();
extern "C" void MethodDictionary_set_Item_m8394 ();
extern "C" void MethodDictionary_GetMethodProperty_m8395 ();
extern "C" void MethodDictionary_SetMethodProperty_m8396 ();
extern "C" void MethodDictionary_get_Values_m8397 ();
extern "C" void MethodDictionary_Add_m8398 ();
extern "C" void MethodDictionary_Contains_m8399 ();
extern "C" void MethodDictionary_Remove_m8400 ();
extern "C" void MethodDictionary_get_Count_m8401 ();
extern "C" void MethodDictionary_get_IsSynchronized_m8402 ();
extern "C" void MethodDictionary_get_SyncRoot_m8403 ();
extern "C" void MethodDictionary_CopyTo_m8404 ();
extern "C" void MethodDictionary_GetEnumerator_m8405 ();
extern "C" void MethodReturnDictionary__ctor_m8406 ();
extern "C" void MethodReturnDictionary__cctor_m8407 ();
extern "C" void MonoMethodMessage_get_Args_m8408 ();
extern "C" void MonoMethodMessage_get_LogicalCallContext_m8409 ();
extern "C" void MonoMethodMessage_get_MethodBase_m8410 ();
extern "C" void MonoMethodMessage_get_MethodName_m8411 ();
extern "C" void MonoMethodMessage_get_MethodSignature_m8412 ();
extern "C" void MonoMethodMessage_get_TypeName_m8413 ();
extern "C" void MonoMethodMessage_get_Uri_m8414 ();
extern "C" void MonoMethodMessage_set_Uri_m8415 ();
extern "C" void MonoMethodMessage_get_Exception_m8416 ();
extern "C" void MonoMethodMessage_get_OutArgCount_m8417 ();
extern "C" void MonoMethodMessage_get_OutArgs_m8418 ();
extern "C" void MonoMethodMessage_get_ReturnValue_m8419 ();
extern "C" void RemotingSurrogate__ctor_m8420 ();
extern "C" void RemotingSurrogate_SetObjectData_m8421 ();
extern "C" void ObjRefSurrogate__ctor_m8422 ();
extern "C" void ObjRefSurrogate_SetObjectData_m8423 ();
extern "C" void RemotingSurrogateSelector__ctor_m8424 ();
extern "C" void RemotingSurrogateSelector__cctor_m8425 ();
extern "C" void RemotingSurrogateSelector_GetSurrogate_m8426 ();
extern "C" void ReturnMessage__ctor_m8427 ();
extern "C" void ReturnMessage__ctor_m8428 ();
extern "C" void ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8429 ();
extern "C" void ReturnMessage_get_Args_m8430 ();
extern "C" void ReturnMessage_get_LogicalCallContext_m8431 ();
extern "C" void ReturnMessage_get_MethodBase_m8432 ();
extern "C" void ReturnMessage_get_MethodName_m8433 ();
extern "C" void ReturnMessage_get_MethodSignature_m8434 ();
extern "C" void ReturnMessage_get_Properties_m8435 ();
extern "C" void ReturnMessage_get_TypeName_m8436 ();
extern "C" void ReturnMessage_get_Uri_m8437 ();
extern "C" void ReturnMessage_set_Uri_m8438 ();
extern "C" void ReturnMessage_get_Exception_m8439 ();
extern "C" void ReturnMessage_get_OutArgs_m8440 ();
extern "C" void ReturnMessage_get_ReturnValue_m8441 ();
extern "C" void ServerContextTerminatorSink__ctor_m8442 ();
extern "C" void ServerObjectTerminatorSink__ctor_m8443 ();
extern "C" void StackBuilderSink__ctor_m8444 ();
extern "C" void SoapAttribute__ctor_m8445 ();
extern "C" void SoapAttribute_get_UseAttribute_m8446 ();
extern "C" void SoapAttribute_get_XmlNamespace_m8447 ();
extern "C" void SoapAttribute_SetReflectionObject_m8448 ();
extern "C" void SoapFieldAttribute__ctor_m8449 ();
extern "C" void SoapFieldAttribute_get_XmlElementName_m8450 ();
extern "C" void SoapFieldAttribute_IsInteropXmlElement_m8451 ();
extern "C" void SoapFieldAttribute_SetReflectionObject_m8452 ();
extern "C" void SoapMethodAttribute__ctor_m8453 ();
extern "C" void SoapMethodAttribute_get_UseAttribute_m8454 ();
extern "C" void SoapMethodAttribute_get_XmlNamespace_m8455 ();
extern "C" void SoapMethodAttribute_SetReflectionObject_m8456 ();
extern "C" void SoapParameterAttribute__ctor_m8457 ();
extern "C" void SoapTypeAttribute__ctor_m8458 ();
extern "C" void SoapTypeAttribute_get_UseAttribute_m8459 ();
extern "C" void SoapTypeAttribute_get_XmlElementName_m8460 ();
extern "C" void SoapTypeAttribute_get_XmlNamespace_m8461 ();
extern "C" void SoapTypeAttribute_get_XmlTypeName_m8462 ();
extern "C" void SoapTypeAttribute_get_XmlTypeNamespace_m8463 ();
extern "C" void SoapTypeAttribute_get_IsInteropXmlElement_m8464 ();
extern "C" void SoapTypeAttribute_get_IsInteropXmlType_m8465 ();
extern "C" void SoapTypeAttribute_SetReflectionObject_m8466 ();
extern "C" void ProxyAttribute_CreateInstance_m8467 ();
extern "C" void ProxyAttribute_CreateProxy_m8468 ();
extern "C" void ProxyAttribute_GetPropertiesForNewContext_m8469 ();
extern "C" void ProxyAttribute_IsContextOK_m8470 ();
extern "C" void RealProxy__ctor_m8471 ();
extern "C" void RealProxy__ctor_m8472 ();
extern "C" void RealProxy__ctor_m8473 ();
extern "C" void RealProxy_InternalGetProxyType_m8474 ();
extern "C" void RealProxy_GetProxiedType_m8475 ();
extern "C" void RealProxy_get_ObjectIdentity_m8476 ();
extern "C" void RealProxy_InternalGetTransparentProxy_m8477 ();
extern "C" void RealProxy_GetTransparentProxy_m8478 ();
extern "C" void RealProxy_SetTargetDomain_m8479 ();
extern "C" void RemotingProxy__ctor_m8480 ();
extern "C" void RemotingProxy__ctor_m8481 ();
extern "C" void RemotingProxy__cctor_m8482 ();
extern "C" void RemotingProxy_get_TypeName_m8483 ();
extern "C" void RemotingProxy_Finalize_m8484 ();
extern "C" void TrackingServices__cctor_m8485 ();
extern "C" void TrackingServices_NotifyUnmarshaledObject_m8486 ();
extern "C" void ActivatedClientTypeEntry__ctor_m8487 ();
extern "C" void ActivatedClientTypeEntry_get_ApplicationUrl_m8488 ();
extern "C" void ActivatedClientTypeEntry_get_ContextAttributes_m8489 ();
extern "C" void ActivatedClientTypeEntry_get_ObjectType_m8490 ();
extern "C" void ActivatedClientTypeEntry_ToString_m8491 ();
extern "C" void ActivatedServiceTypeEntry__ctor_m8492 ();
extern "C" void ActivatedServiceTypeEntry_get_ObjectType_m8493 ();
extern "C" void ActivatedServiceTypeEntry_ToString_m8494 ();
extern "C" void EnvoyInfo__ctor_m8495 ();
extern "C" void EnvoyInfo_get_EnvoySinks_m8496 ();
extern "C" void Identity__ctor_m8497 ();
extern "C" void Identity_get_ChannelSink_m8498 ();
extern "C" void Identity_set_ChannelSink_m8499 ();
extern "C" void Identity_get_ObjectUri_m8500 ();
extern "C" void Identity_get_Disposed_m8501 ();
extern "C" void Identity_set_Disposed_m8502 ();
extern "C" void Identity_get_ClientDynamicProperties_m8503 ();
extern "C" void Identity_get_ServerDynamicProperties_m8504 ();
extern "C" void ClientIdentity__ctor_m8505 ();
extern "C" void ClientIdentity_get_ClientProxy_m8506 ();
extern "C" void ClientIdentity_set_ClientProxy_m8507 ();
extern "C" void ClientIdentity_CreateObjRef_m8508 ();
extern "C" void ClientIdentity_get_TargetUri_m8509 ();
extern "C" void InternalRemotingServices__cctor_m8510 ();
extern "C" void InternalRemotingServices_GetCachedSoapAttribute_m8511 ();
extern "C" void ObjRef__ctor_m8512 ();
extern "C" void ObjRef__ctor_m8513 ();
extern "C" void ObjRef__cctor_m8514 ();
extern "C" void ObjRef_get_IsReferenceToWellKnow_m8515 ();
extern "C" void ObjRef_get_ChannelInfo_m8516 ();
extern "C" void ObjRef_get_EnvoyInfo_m8517 ();
extern "C" void ObjRef_set_EnvoyInfo_m8518 ();
extern "C" void ObjRef_get_TypeInfo_m8519 ();
extern "C" void ObjRef_set_TypeInfo_m8520 ();
extern "C" void ObjRef_get_URI_m8521 ();
extern "C" void ObjRef_set_URI_m8522 ();
extern "C" void ObjRef_GetObjectData_m8523 ();
extern "C" void ObjRef_GetRealObject_m8524 ();
extern "C" void ObjRef_UpdateChannelInfo_m8525 ();
extern "C" void ObjRef_get_ServerType_m8526 ();
extern "C" void RemotingConfiguration__cctor_m8527 ();
extern "C" void RemotingConfiguration_get_ApplicationName_m8528 ();
extern "C" void RemotingConfiguration_set_ApplicationName_m8529 ();
extern "C" void RemotingConfiguration_get_ProcessId_m8530 ();
extern "C" void RemotingConfiguration_LoadDefaultDelayedChannels_m8531 ();
extern "C" void RemotingConfiguration_IsRemotelyActivatedClientType_m8532 ();
extern "C" void RemotingConfiguration_RegisterActivatedClientType_m8533 ();
extern "C" void RemotingConfiguration_RegisterActivatedServiceType_m8534 ();
extern "C" void RemotingConfiguration_RegisterWellKnownClientType_m8535 ();
extern "C" void RemotingConfiguration_RegisterWellKnownServiceType_m8536 ();
extern "C" void RemotingConfiguration_RegisterChannelTemplate_m8537 ();
extern "C" void RemotingConfiguration_RegisterClientProviderTemplate_m8538 ();
extern "C" void RemotingConfiguration_RegisterServerProviderTemplate_m8539 ();
extern "C" void RemotingConfiguration_RegisterChannels_m8540 ();
extern "C" void RemotingConfiguration_RegisterTypes_m8541 ();
extern "C" void RemotingConfiguration_SetCustomErrorsMode_m8542 ();
extern "C" void ConfigHandler__ctor_m8543 ();
extern "C" void ConfigHandler_ValidatePath_m8544 ();
extern "C" void ConfigHandler_CheckPath_m8545 ();
extern "C" void ConfigHandler_OnStartParsing_m8546 ();
extern "C" void ConfigHandler_OnProcessingInstruction_m8547 ();
extern "C" void ConfigHandler_OnIgnorableWhitespace_m8548 ();
extern "C" void ConfigHandler_OnStartElement_m8549 ();
extern "C" void ConfigHandler_ParseElement_m8550 ();
extern "C" void ConfigHandler_OnEndElement_m8551 ();
extern "C" void ConfigHandler_ReadCustomProviderData_m8552 ();
extern "C" void ConfigHandler_ReadLifetine_m8553 ();
extern "C" void ConfigHandler_ParseTime_m8554 ();
extern "C" void ConfigHandler_ReadChannel_m8555 ();
extern "C" void ConfigHandler_ReadProvider_m8556 ();
extern "C" void ConfigHandler_ReadClientActivated_m8557 ();
extern "C" void ConfigHandler_ReadServiceActivated_m8558 ();
extern "C" void ConfigHandler_ReadClientWellKnown_m8559 ();
extern "C" void ConfigHandler_ReadServiceWellKnown_m8560 ();
extern "C" void ConfigHandler_ReadInteropXml_m8561 ();
extern "C" void ConfigHandler_ReadPreload_m8562 ();
extern "C" void ConfigHandler_GetNotNull_m8563 ();
extern "C" void ConfigHandler_ExtractAssembly_m8564 ();
extern "C" void ConfigHandler_OnChars_m8565 ();
extern "C" void ConfigHandler_OnEndParsing_m8566 ();
extern "C" void ChannelData__ctor_m8567 ();
extern "C" void ChannelData_get_ServerProviders_m8568 ();
extern "C" void ChannelData_get_ClientProviders_m8569 ();
extern "C" void ChannelData_get_CustomProperties_m8570 ();
extern "C" void ChannelData_CopyFrom_m8571 ();
extern "C" void ProviderData__ctor_m8572 ();
extern "C" void ProviderData_CopyFrom_m8573 ();
extern "C" void FormatterData__ctor_m8574 ();
extern "C" void RemotingException__ctor_m8575 ();
extern "C" void RemotingException__ctor_m8576 ();
extern "C" void RemotingException__ctor_m8577 ();
extern "C" void RemotingException__ctor_m8578 ();
extern "C" void RemotingServices__cctor_m8579 ();
extern "C" void RemotingServices_GetVirtualMethod_m8580 ();
extern "C" void RemotingServices_IsTransparentProxy_m8581 ();
extern "C" void RemotingServices_GetServerTypeForUri_m8582 ();
extern "C" void RemotingServices_Unmarshal_m8583 ();
extern "C" void RemotingServices_Unmarshal_m8584 ();
extern "C" void RemotingServices_GetRealProxy_m8585 ();
extern "C" void RemotingServices_GetMethodBaseFromMethodMessage_m8586 ();
extern "C" void RemotingServices_GetMethodBaseFromName_m8587 ();
extern "C" void RemotingServices_FindInterfaceMethod_m8588 ();
extern "C" void RemotingServices_CreateClientProxy_m8589 ();
extern "C" void RemotingServices_CreateClientProxy_m8590 ();
extern "C" void RemotingServices_CreateClientProxyForContextBound_m8591 ();
extern "C" void RemotingServices_GetIdentityForUri_m8592 ();
extern "C" void RemotingServices_RemoveAppNameFromUri_m8593 ();
extern "C" void RemotingServices_GetOrCreateClientIdentity_m8594 ();
extern "C" void RemotingServices_GetClientChannelSinkChain_m8595 ();
extern "C" void RemotingServices_CreateWellKnownServerIdentity_m8596 ();
extern "C" void RemotingServices_RegisterServerIdentity_m8597 ();
extern "C" void RemotingServices_GetProxyForRemoteObject_m8598 ();
extern "C" void RemotingServices_GetRemoteObject_m8599 ();
extern "C" void RemotingServices_RegisterInternalChannels_m8600 ();
extern "C" void RemotingServices_DisposeIdentity_m8601 ();
extern "C" void RemotingServices_GetNormalizedUri_m8602 ();
extern "C" void ServerIdentity__ctor_m8603 ();
extern "C" void ServerIdentity_get_ObjectType_m8604 ();
extern "C" void ServerIdentity_CreateObjRef_m8605 ();
extern "C" void ClientActivatedIdentity_GetServerObject_m8606 ();
extern "C" void SingletonIdentity__ctor_m8607 ();
extern "C" void SingleCallIdentity__ctor_m8608 ();
extern "C" void TypeInfo__ctor_m8609 ();
extern "C" void SoapServices__cctor_m8610 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithAssembly_m8611 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithNs_m8612 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithNsAndAssembly_m8613 ();
extern "C" void SoapServices_CodeXmlNamespaceForClrTypeNamespace_m8614 ();
extern "C" void SoapServices_GetNameKey_m8615 ();
extern "C" void SoapServices_GetAssemblyName_m8616 ();
extern "C" void SoapServices_GetXmlElementForInteropType_m8617 ();
extern "C" void SoapServices_GetXmlNamespaceForMethodCall_m8618 ();
extern "C" void SoapServices_GetXmlNamespaceForMethodResponse_m8619 ();
extern "C" void SoapServices_GetXmlTypeForInteropType_m8620 ();
extern "C" void SoapServices_PreLoad_m8621 ();
extern "C" void SoapServices_PreLoad_m8622 ();
extern "C" void SoapServices_RegisterInteropXmlElement_m8623 ();
extern "C" void SoapServices_RegisterInteropXmlType_m8624 ();
extern "C" void SoapServices_EncodeNs_m8625 ();
extern "C" void TypeEntry__ctor_m8626 ();
extern "C" void TypeEntry_get_AssemblyName_m8627 ();
extern "C" void TypeEntry_set_AssemblyName_m8628 ();
extern "C" void TypeEntry_get_TypeName_m8629 ();
extern "C" void TypeEntry_set_TypeName_m8630 ();
extern "C" void TypeInfo__ctor_m8631 ();
extern "C" void TypeInfo_get_TypeName_m8632 ();
extern "C" void WellKnownClientTypeEntry__ctor_m8633 ();
extern "C" void WellKnownClientTypeEntry_get_ApplicationUrl_m8634 ();
extern "C" void WellKnownClientTypeEntry_get_ObjectType_m8635 ();
extern "C" void WellKnownClientTypeEntry_get_ObjectUrl_m8636 ();
extern "C" void WellKnownClientTypeEntry_ToString_m8637 ();
extern "C" void WellKnownServiceTypeEntry__ctor_m8638 ();
extern "C" void WellKnownServiceTypeEntry_get_Mode_m8639 ();
extern "C" void WellKnownServiceTypeEntry_get_ObjectType_m8640 ();
extern "C" void WellKnownServiceTypeEntry_get_ObjectUri_m8641 ();
extern "C" void WellKnownServiceTypeEntry_ToString_m8642 ();
extern "C" void BinaryCommon__cctor_m8643 ();
extern "C" void BinaryCommon_IsPrimitive_m8644 ();
extern "C" void BinaryCommon_GetTypeFromCode_m8645 ();
extern "C" void BinaryCommon_SwapBytes_m8646 ();
extern "C" void BinaryFormatter__ctor_m8647 ();
extern "C" void BinaryFormatter__ctor_m8648 ();
extern "C" void BinaryFormatter_get_DefaultSurrogateSelector_m8649 ();
extern "C" void BinaryFormatter_set_AssemblyFormat_m8650 ();
extern "C" void BinaryFormatter_get_Binder_m8651 ();
extern "C" void BinaryFormatter_get_Context_m8652 ();
extern "C" void BinaryFormatter_get_SurrogateSelector_m8653 ();
extern "C" void BinaryFormatter_get_FilterLevel_m8654 ();
extern "C" void BinaryFormatter_Deserialize_m8655 ();
extern "C" void BinaryFormatter_NoCheckDeserialize_m8656 ();
extern "C" void BinaryFormatter_ReadBinaryHeader_m8657 ();
extern "C" void MessageFormatter_ReadMethodCall_m8658 ();
extern "C" void MessageFormatter_ReadMethodResponse_m8659 ();
extern "C" void TypeMetadata__ctor_m8660 ();
extern "C" void ArrayNullFiller__ctor_m8661 ();
extern "C" void ObjectReader__ctor_m8662 ();
extern "C" void ObjectReader_ReadObjectGraph_m8663 ();
extern "C" void ObjectReader_ReadObjectGraph_m8664 ();
extern "C" void ObjectReader_ReadNextObject_m8665 ();
extern "C" void ObjectReader_ReadNextObject_m8666 ();
extern "C" void ObjectReader_get_CurrentObject_m8667 ();
extern "C" void ObjectReader_ReadObject_m8668 ();
extern "C" void ObjectReader_ReadAssembly_m8669 ();
extern "C" void ObjectReader_ReadObjectInstance_m8670 ();
extern "C" void ObjectReader_ReadRefTypeObjectInstance_m8671 ();
extern "C" void ObjectReader_ReadObjectContent_m8672 ();
extern "C" void ObjectReader_RegisterObject_m8673 ();
extern "C" void ObjectReader_ReadStringIntance_m8674 ();
extern "C" void ObjectReader_ReadGenericArray_m8675 ();
extern "C" void ObjectReader_ReadBoxedPrimitiveTypeValue_m8676 ();
extern "C" void ObjectReader_ReadArrayOfPrimitiveType_m8677 ();
extern "C" void ObjectReader_BlockRead_m8678 ();
extern "C" void ObjectReader_ReadArrayOfObject_m8679 ();
extern "C" void ObjectReader_ReadArrayOfString_m8680 ();
extern "C" void ObjectReader_ReadSimpleArray_m8681 ();
extern "C" void ObjectReader_ReadTypeMetadata_m8682 ();
extern "C" void ObjectReader_ReadValue_m8683 ();
extern "C" void ObjectReader_SetObjectValue_m8684 ();
extern "C" void ObjectReader_RecordFixup_m8685 ();
extern "C" void ObjectReader_GetDeserializationType_m8686 ();
extern "C" void ObjectReader_ReadType_m8687 ();
extern "C" void ObjectReader_ReadPrimitiveTypeValue_m8688 ();
extern "C" void FormatterConverter__ctor_m8689 ();
extern "C" void FormatterConverter_Convert_m8690 ();
extern "C" void FormatterConverter_ToBoolean_m8691 ();
extern "C" void FormatterConverter_ToInt16_m8692 ();
extern "C" void FormatterConverter_ToInt32_m8693 ();
extern "C" void FormatterConverter_ToInt64_m8694 ();
extern "C" void FormatterConverter_ToString_m8695 ();
extern "C" void FormatterServices_GetUninitializedObject_m8696 ();
extern "C" void FormatterServices_GetSafeUninitializedObject_m8697 ();
extern "C" void ObjectManager__ctor_m8698 ();
extern "C" void ObjectManager_DoFixups_m8699 ();
extern "C" void ObjectManager_GetObjectRecord_m8700 ();
extern "C" void ObjectManager_GetObject_m8701 ();
extern "C" void ObjectManager_RaiseDeserializationEvent_m8702 ();
extern "C" void ObjectManager_RaiseOnDeserializingEvent_m8703 ();
extern "C" void ObjectManager_RaiseOnDeserializedEvent_m8704 ();
extern "C" void ObjectManager_AddFixup_m8705 ();
extern "C" void ObjectManager_RecordArrayElementFixup_m8706 ();
extern "C" void ObjectManager_RecordArrayElementFixup_m8707 ();
extern "C" void ObjectManager_RecordDelayedFixup_m8708 ();
extern "C" void ObjectManager_RecordFixup_m8709 ();
extern "C" void ObjectManager_RegisterObjectInternal_m8710 ();
extern "C" void ObjectManager_RegisterObject_m8711 ();
extern "C" void BaseFixupRecord__ctor_m8712 ();
extern "C" void BaseFixupRecord_DoFixup_m8713 ();
extern "C" void ArrayFixupRecord__ctor_m8714 ();
extern "C" void ArrayFixupRecord_FixupImpl_m8715 ();
extern "C" void MultiArrayFixupRecord__ctor_m8716 ();
extern "C" void MultiArrayFixupRecord_FixupImpl_m8717 ();
extern "C" void FixupRecord__ctor_m8718 ();
extern "C" void FixupRecord_FixupImpl_m8719 ();
extern "C" void DelayedFixupRecord__ctor_m8720 ();
extern "C" void DelayedFixupRecord_FixupImpl_m8721 ();
extern "C" void ObjectRecord__ctor_m8722 ();
extern "C" void ObjectRecord_SetMemberValue_m8723 ();
extern "C" void ObjectRecord_SetArrayValue_m8724 ();
extern "C" void ObjectRecord_SetMemberValue_m8725 ();
extern "C" void ObjectRecord_get_IsInstanceReady_m8726 ();
extern "C" void ObjectRecord_get_IsUnsolvedObjectReference_m8727 ();
extern "C" void ObjectRecord_get_IsRegistered_m8728 ();
extern "C" void ObjectRecord_DoFixups_m8729 ();
extern "C" void ObjectRecord_RemoveFixup_m8730 ();
extern "C" void ObjectRecord_UnchainFixup_m8731 ();
extern "C" void ObjectRecord_ChainFixup_m8732 ();
extern "C" void ObjectRecord_LoadData_m8733 ();
extern "C" void ObjectRecord_get_HasPendingFixups_m8734 ();
extern "C" void SerializationBinder__ctor_m8735 ();
extern "C" void CallbackHandler__ctor_m8736 ();
extern "C" void CallbackHandler_Invoke_m8737 ();
extern "C" void CallbackHandler_BeginInvoke_m8738 ();
extern "C" void CallbackHandler_EndInvoke_m8739 ();
extern "C" void SerializationCallbacks__ctor_m8740 ();
extern "C" void SerializationCallbacks__cctor_m8741 ();
extern "C" void SerializationCallbacks_get_HasDeserializedCallbacks_m8742 ();
extern "C" void SerializationCallbacks_GetMethodsByAttribute_m8743 ();
extern "C" void SerializationCallbacks_Invoke_m8744 ();
extern "C" void SerializationCallbacks_RaiseOnDeserializing_m8745 ();
extern "C" void SerializationCallbacks_RaiseOnDeserialized_m8746 ();
extern "C" void SerializationCallbacks_GetSerializationCallbacks_m8747 ();
extern "C" void SerializationEntry__ctor_m8748 ();
extern "C" void SerializationEntry_get_Name_m8749 ();
extern "C" void SerializationEntry_get_Value_m8750 ();
extern "C" void SerializationException__ctor_m8751 ();
extern "C" void SerializationException__ctor_m5356 ();
extern "C" void SerializationException__ctor_m8752 ();
extern "C" void SerializationInfo__ctor_m8753 ();
extern "C" void SerializationInfo_AddValue_m5352 ();
extern "C" void SerializationInfo_GetValue_m5355 ();
extern "C" void SerializationInfo_SetType_m8754 ();
extern "C" void SerializationInfo_GetEnumerator_m8755 ();
extern "C" void SerializationInfo_AddValue_m8756 ();
extern "C" void SerializationInfo_AddValue_m5354 ();
extern "C" void SerializationInfo_AddValue_m5353 ();
extern "C" void SerializationInfo_AddValue_m8757 ();
extern "C" void SerializationInfo_AddValue_m8758 ();
extern "C" void SerializationInfo_AddValue_m5363 ();
extern "C" void SerializationInfo_AddValue_m8759 ();
extern "C" void SerializationInfo_AddValue_m4388 ();
extern "C" void SerializationInfo_GetBoolean_m5357 ();
extern "C" void SerializationInfo_GetInt16_m8760 ();
extern "C" void SerializationInfo_GetInt32_m5362 ();
extern "C" void SerializationInfo_GetInt64_m5361 ();
extern "C" void SerializationInfo_GetString_m5360 ();
extern "C" void SerializationInfoEnumerator__ctor_m8761 ();
extern "C" void SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m8762 ();
extern "C" void SerializationInfoEnumerator_get_Current_m8763 ();
extern "C" void SerializationInfoEnumerator_get_Name_m8764 ();
extern "C" void SerializationInfoEnumerator_get_Value_m8765 ();
extern "C" void SerializationInfoEnumerator_MoveNext_m8766 ();
extern "C" void StreamingContext__ctor_m8767 ();
extern "C" void StreamingContext__ctor_m8768 ();
extern "C" void StreamingContext_get_State_m8769 ();
extern "C" void StreamingContext_Equals_m8770 ();
extern "C" void StreamingContext_GetHashCode_m8771 ();
extern "C" void X509Certificate__ctor_m8772 ();
extern "C" void X509Certificate__ctor_m4446 ();
extern "C" void X509Certificate__ctor_m5385 ();
extern "C" void X509Certificate__ctor_m8773 ();
extern "C" void X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m8774 ();
extern "C" void X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m8775 ();
extern "C" void X509Certificate_tostr_m8776 ();
extern "C" void X509Certificate_Equals_m8777 ();
extern "C" void X509Certificate_GetCertHash_m8778 ();
extern "C" void X509Certificate_GetCertHashString_m5387 ();
extern "C" void X509Certificate_GetEffectiveDateString_m8779 ();
extern "C" void X509Certificate_GetExpirationDateString_m8780 ();
extern "C" void X509Certificate_GetHashCode_m8781 ();
extern "C" void X509Certificate_GetIssuerName_m8782 ();
extern "C" void X509Certificate_GetName_m8783 ();
extern "C" void X509Certificate_GetPublicKey_m8784 ();
extern "C" void X509Certificate_GetRawCertData_m8785 ();
extern "C" void X509Certificate_ToString_m8786 ();
extern "C" void X509Certificate_ToString_m5390 ();
extern "C" void X509Certificate_get_Issuer_m5392 ();
extern "C" void X509Certificate_get_Subject_m5391 ();
extern "C" void X509Certificate_Equals_m8787 ();
extern "C" void X509Certificate_Import_m5388 ();
extern "C" void X509Certificate_Reset_m5389 ();
extern "C" void AsymmetricAlgorithm__ctor_m8788 ();
extern "C" void AsymmetricAlgorithm_System_IDisposable_Dispose_m8789 ();
extern "C" void AsymmetricAlgorithm_get_KeySize_m4364 ();
extern "C" void AsymmetricAlgorithm_set_KeySize_m4363 ();
extern "C" void AsymmetricAlgorithm_Clear_m4453 ();
extern "C" void AsymmetricAlgorithm_GetNamedParam_m8790 ();
extern "C" void AsymmetricKeyExchangeFormatter__ctor_m8791 ();
extern "C" void AsymmetricSignatureDeformatter__ctor_m4439 ();
extern "C" void AsymmetricSignatureFormatter__ctor_m4440 ();
extern "C" void Base64Constants__cctor_m8792 ();
extern "C" void CryptoConfig__cctor_m8793 ();
extern "C" void CryptoConfig_Initialize_m8794 ();
extern "C" void CryptoConfig_CreateFromName_m4354 ();
extern "C" void CryptoConfig_CreateFromName_m5397 ();
extern "C" void CryptoConfig_MapNameToOID_m4355 ();
extern "C" void CryptoConfig_EncodeOID_m4347 ();
extern "C" void CryptoConfig_EncodeLongNumber_m8795 ();
extern "C" void CryptographicException__ctor_m8796 ();
extern "C" void CryptographicException__ctor_m3448 ();
extern "C" void CryptographicException__ctor_m4376 ();
extern "C" void CryptographicException__ctor_m3456 ();
extern "C" void CryptographicException__ctor_m8797 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m8798 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m4406 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m8799 ();
extern "C" void CspParameters__ctor_m4358 ();
extern "C" void CspParameters__ctor_m8800 ();
extern "C" void CspParameters__ctor_m8801 ();
extern "C" void CspParameters__ctor_m8802 ();
extern "C" void CspParameters_get_Flags_m8803 ();
extern "C" void CspParameters_set_Flags_m4359 ();
extern "C" void DES__ctor_m8804 ();
extern "C" void DES__cctor_m8805 ();
extern "C" void DES_Create_m4407 ();
extern "C" void DES_Create_m8806 ();
extern "C" void DES_IsWeakKey_m8807 ();
extern "C" void DES_IsSemiWeakKey_m8808 ();
extern "C" void DES_get_Key_m8809 ();
extern "C" void DES_set_Key_m8810 ();
extern "C" void DESTransform__ctor_m8811 ();
extern "C" void DESTransform__cctor_m8812 ();
extern "C" void DESTransform_CipherFunct_m8813 ();
extern "C" void DESTransform_Permutation_m8814 ();
extern "C" void DESTransform_BSwap_m8815 ();
extern "C" void DESTransform_SetKey_m8816 ();
extern "C" void DESTransform_ProcessBlock_m8817 ();
extern "C" void DESTransform_ECB_m8818 ();
extern "C" void DESTransform_GetStrongKey_m8819 ();
extern "C" void DESCryptoServiceProvider__ctor_m8820 ();
extern "C" void DESCryptoServiceProvider_CreateDecryptor_m8821 ();
extern "C" void DESCryptoServiceProvider_CreateEncryptor_m8822 ();
extern "C" void DESCryptoServiceProvider_GenerateIV_m8823 ();
extern "C" void DESCryptoServiceProvider_GenerateKey_m8824 ();
extern "C" void DSA__ctor_m8825 ();
extern "C" void DSA_Create_m4361 ();
extern "C" void DSA_Create_m8826 ();
extern "C" void DSA_ZeroizePrivateKey_m8827 ();
extern "C" void DSA_FromXmlString_m8828 ();
extern "C" void DSA_ToXmlString_m8829 ();
extern "C" void DSACryptoServiceProvider__ctor_m8830 ();
extern "C" void DSACryptoServiceProvider__ctor_m4378 ();
extern "C" void DSACryptoServiceProvider__ctor_m8831 ();
extern "C" void DSACryptoServiceProvider__cctor_m8832 ();
extern "C" void DSACryptoServiceProvider_Finalize_m8833 ();
extern "C" void DSACryptoServiceProvider_get_KeySize_m8834 ();
extern "C" void DSACryptoServiceProvider_get_PublicOnly_m5379 ();
extern "C" void DSACryptoServiceProvider_ExportParameters_m8835 ();
extern "C" void DSACryptoServiceProvider_ImportParameters_m8836 ();
extern "C" void DSACryptoServiceProvider_CreateSignature_m8837 ();
extern "C" void DSACryptoServiceProvider_VerifySignature_m8838 ();
extern "C" void DSACryptoServiceProvider_Dispose_m8839 ();
extern "C" void DSACryptoServiceProvider_OnKeyGenerated_m8840 ();
extern "C" void DSASignatureDeformatter__ctor_m8841 ();
extern "C" void DSASignatureDeformatter__ctor_m4386 ();
extern "C" void DSASignatureDeformatter_SetHashAlgorithm_m8842 ();
extern "C" void DSASignatureDeformatter_SetKey_m8843 ();
extern "C" void DSASignatureDeformatter_VerifySignature_m8844 ();
extern "C" void DSASignatureFormatter__ctor_m8845 ();
extern "C" void DSASignatureFormatter_CreateSignature_m8846 ();
extern "C" void DSASignatureFormatter_SetHashAlgorithm_m8847 ();
extern "C" void DSASignatureFormatter_SetKey_m8848 ();
extern "C" void HMAC__ctor_m8849 ();
extern "C" void HMAC_get_BlockSizeValue_m8850 ();
extern "C" void HMAC_set_BlockSizeValue_m8851 ();
extern "C" void HMAC_set_HashName_m8852 ();
extern "C" void HMAC_get_Key_m8853 ();
extern "C" void HMAC_set_Key_m8854 ();
extern "C" void HMAC_get_Block_m8855 ();
extern "C" void HMAC_KeySetup_m8856 ();
extern "C" void HMAC_Dispose_m8857 ();
extern "C" void HMAC_HashCore_m8858 ();
extern "C" void HMAC_HashFinal_m8859 ();
extern "C" void HMAC_Initialize_m8860 ();
extern "C" void HMAC_Create_m4373 ();
extern "C" void HMAC_Create_m8861 ();
extern "C" void HMACMD5__ctor_m8862 ();
extern "C" void HMACMD5__ctor_m8863 ();
extern "C" void HMACRIPEMD160__ctor_m8864 ();
extern "C" void HMACRIPEMD160__ctor_m8865 ();
extern "C" void HMACSHA1__ctor_m8866 ();
extern "C" void HMACSHA1__ctor_m8867 ();
extern "C" void HMACSHA256__ctor_m8868 ();
extern "C" void HMACSHA256__ctor_m8869 ();
extern "C" void HMACSHA384__ctor_m8870 ();
extern "C" void HMACSHA384__ctor_m8871 ();
extern "C" void HMACSHA384__cctor_m8872 ();
extern "C" void HMACSHA384_set_ProduceLegacyHmacValues_m8873 ();
extern "C" void HMACSHA512__ctor_m8874 ();
extern "C" void HMACSHA512__ctor_m8875 ();
extern "C" void HMACSHA512__cctor_m8876 ();
extern "C" void HMACSHA512_set_ProduceLegacyHmacValues_m8877 ();
extern "C" void HashAlgorithm__ctor_m4353 ();
extern "C" void HashAlgorithm_System_IDisposable_Dispose_m8878 ();
extern "C" void HashAlgorithm_get_CanReuseTransform_m8879 ();
extern "C" void HashAlgorithm_ComputeHash_m4393 ();
extern "C" void HashAlgorithm_ComputeHash_m4366 ();
extern "C" void HashAlgorithm_Create_m4365 ();
extern "C" void HashAlgorithm_get_Hash_m8880 ();
extern "C" void HashAlgorithm_get_HashSize_m8881 ();
extern "C" void HashAlgorithm_Dispose_m8882 ();
extern "C" void HashAlgorithm_TransformBlock_m8883 ();
extern "C" void HashAlgorithm_TransformFinalBlock_m8884 ();
extern "C" void KeySizes__ctor_m3458 ();
extern "C" void KeySizes_get_MaxSize_m8885 ();
extern "C" void KeySizes_get_MinSize_m8886 ();
extern "C" void KeySizes_get_SkipSize_m8887 ();
extern "C" void KeySizes_IsLegal_m8888 ();
extern "C" void KeySizes_IsLegalKeySize_m8889 ();
extern "C" void KeyedHashAlgorithm__ctor_m4405 ();
extern "C" void KeyedHashAlgorithm_Finalize_m8890 ();
extern "C" void KeyedHashAlgorithm_get_Key_m8891 ();
extern "C" void KeyedHashAlgorithm_set_Key_m8892 ();
extern "C" void KeyedHashAlgorithm_Dispose_m8893 ();
extern "C" void KeyedHashAlgorithm_ZeroizeKey_m8894 ();
extern "C" void MACTripleDES__ctor_m8895 ();
extern "C" void MACTripleDES_Setup_m8896 ();
extern "C" void MACTripleDES_Finalize_m8897 ();
extern "C" void MACTripleDES_Dispose_m8898 ();
extern "C" void MACTripleDES_Initialize_m8899 ();
extern "C" void MACTripleDES_HashCore_m8900 ();
extern "C" void MACTripleDES_HashFinal_m8901 ();
extern "C" void MD5__ctor_m8902 ();
extern "C" void MD5_Create_m4379 ();
extern "C" void MD5_Create_m8903 ();
extern "C" void MD5CryptoServiceProvider__ctor_m8904 ();
extern "C" void MD5CryptoServiceProvider__cctor_m8905 ();
extern "C" void MD5CryptoServiceProvider_Finalize_m8906 ();
extern "C" void MD5CryptoServiceProvider_Dispose_m8907 ();
extern "C" void MD5CryptoServiceProvider_HashCore_m8908 ();
extern "C" void MD5CryptoServiceProvider_HashFinal_m8909 ();
extern "C" void MD5CryptoServiceProvider_Initialize_m8910 ();
extern "C" void MD5CryptoServiceProvider_ProcessBlock_m8911 ();
extern "C" void MD5CryptoServiceProvider_ProcessFinalBlock_m8912 ();
extern "C" void MD5CryptoServiceProvider_AddLength_m8913 ();
extern "C" void RC2__ctor_m8914 ();
extern "C" void RC2_Create_m4408 ();
extern "C" void RC2_Create_m8915 ();
extern "C" void RC2_get_EffectiveKeySize_m8916 ();
extern "C" void RC2_get_KeySize_m8917 ();
extern "C" void RC2_set_KeySize_m8918 ();
extern "C" void RC2CryptoServiceProvider__ctor_m8919 ();
extern "C" void RC2CryptoServiceProvider_get_EffectiveKeySize_m8920 ();
extern "C" void RC2CryptoServiceProvider_CreateDecryptor_m8921 ();
extern "C" void RC2CryptoServiceProvider_CreateEncryptor_m8922 ();
extern "C" void RC2CryptoServiceProvider_GenerateIV_m8923 ();
extern "C" void RC2CryptoServiceProvider_GenerateKey_m8924 ();
extern "C" void RC2Transform__ctor_m8925 ();
extern "C" void RC2Transform__cctor_m8926 ();
extern "C" void RC2Transform_ECB_m8927 ();
extern "C" void RIPEMD160__ctor_m8928 ();
extern "C" void RIPEMD160Managed__ctor_m8929 ();
extern "C" void RIPEMD160Managed_Initialize_m8930 ();
extern "C" void RIPEMD160Managed_HashCore_m8931 ();
extern "C" void RIPEMD160Managed_HashFinal_m8932 ();
extern "C" void RIPEMD160Managed_Finalize_m8933 ();
extern "C" void RIPEMD160Managed_ProcessBlock_m8934 ();
extern "C" void RIPEMD160Managed_Compress_m8935 ();
extern "C" void RIPEMD160Managed_CompressFinal_m8936 ();
extern "C" void RIPEMD160Managed_ROL_m8937 ();
extern "C" void RIPEMD160Managed_F_m8938 ();
extern "C" void RIPEMD160Managed_G_m8939 ();
extern "C" void RIPEMD160Managed_H_m8940 ();
extern "C" void RIPEMD160Managed_I_m8941 ();
extern "C" void RIPEMD160Managed_J_m8942 ();
extern "C" void RIPEMD160Managed_FF_m8943 ();
extern "C" void RIPEMD160Managed_GG_m8944 ();
extern "C" void RIPEMD160Managed_HH_m8945 ();
extern "C" void RIPEMD160Managed_II_m8946 ();
extern "C" void RIPEMD160Managed_JJ_m8947 ();
extern "C" void RIPEMD160Managed_FFF_m8948 ();
extern "C" void RIPEMD160Managed_GGG_m8949 ();
extern "C" void RIPEMD160Managed_HHH_m8950 ();
extern "C" void RIPEMD160Managed_III_m8951 ();
extern "C" void RIPEMD160Managed_JJJ_m8952 ();
extern "C" void RNGCryptoServiceProvider__ctor_m8953 ();
extern "C" void RNGCryptoServiceProvider__cctor_m8954 ();
extern "C" void RNGCryptoServiceProvider_Check_m8955 ();
extern "C" void RNGCryptoServiceProvider_RngOpen_m8956 ();
extern "C" void RNGCryptoServiceProvider_RngInitialize_m8957 ();
extern "C" void RNGCryptoServiceProvider_RngGetBytes_m8958 ();
extern "C" void RNGCryptoServiceProvider_RngClose_m8959 ();
extern "C" void RNGCryptoServiceProvider_GetBytes_m8960 ();
extern "C" void RNGCryptoServiceProvider_GetNonZeroBytes_m8961 ();
extern "C" void RNGCryptoServiceProvider_Finalize_m8962 ();
extern "C" void RSA__ctor_m4362 ();
extern "C" void RSA_Create_m4357 ();
extern "C" void RSA_Create_m8963 ();
extern "C" void RSA_ZeroizePrivateKey_m8964 ();
extern "C" void RSA_FromXmlString_m8965 ();
extern "C" void RSA_ToXmlString_m8966 ();
extern "C" void RSACryptoServiceProvider__ctor_m8967 ();
extern "C" void RSACryptoServiceProvider__ctor_m4360 ();
extern "C" void RSACryptoServiceProvider__ctor_m4382 ();
extern "C" void RSACryptoServiceProvider__cctor_m8968 ();
extern "C" void RSACryptoServiceProvider_Common_m8969 ();
extern "C" void RSACryptoServiceProvider_Finalize_m8970 ();
extern "C" void RSACryptoServiceProvider_get_KeySize_m8971 ();
extern "C" void RSACryptoServiceProvider_get_PublicOnly_m5378 ();
extern "C" void RSACryptoServiceProvider_DecryptValue_m8972 ();
extern "C" void RSACryptoServiceProvider_EncryptValue_m8973 ();
extern "C" void RSACryptoServiceProvider_ExportParameters_m8974 ();
extern "C" void RSACryptoServiceProvider_ImportParameters_m8975 ();
extern "C" void RSACryptoServiceProvider_Dispose_m8976 ();
extern "C" void RSACryptoServiceProvider_OnKeyGenerated_m8977 ();
extern "C" void RSAPKCS1KeyExchangeFormatter__ctor_m4452 ();
extern "C" void RSAPKCS1KeyExchangeFormatter_CreateKeyExchange_m8978 ();
extern "C" void RSAPKCS1KeyExchangeFormatter_SetRSAKey_m8979 ();
extern "C" void RSAPKCS1SignatureDeformatter__ctor_m8980 ();
extern "C" void RSAPKCS1SignatureDeformatter__ctor_m4387 ();
extern "C" void RSAPKCS1SignatureDeformatter_SetHashAlgorithm_m8981 ();
extern "C" void RSAPKCS1SignatureDeformatter_SetKey_m8982 ();
extern "C" void RSAPKCS1SignatureDeformatter_VerifySignature_m8983 ();
extern "C" void RSAPKCS1SignatureFormatter__ctor_m8984 ();
extern "C" void RSAPKCS1SignatureFormatter_CreateSignature_m8985 ();
extern "C" void RSAPKCS1SignatureFormatter_SetHashAlgorithm_m8986 ();
extern "C" void RSAPKCS1SignatureFormatter_SetKey_m8987 ();
extern "C" void RandomNumberGenerator__ctor_m8988 ();
extern "C" void RandomNumberGenerator_Create_m3447 ();
extern "C" void RandomNumberGenerator_Create_m8989 ();
extern "C" void Rijndael__ctor_m8990 ();
extern "C" void Rijndael_Create_m4410 ();
extern "C" void Rijndael_Create_m8991 ();
extern "C" void RijndaelManaged__ctor_m8992 ();
extern "C" void RijndaelManaged_GenerateIV_m8993 ();
extern "C" void RijndaelManaged_GenerateKey_m8994 ();
extern "C" void RijndaelManaged_CreateDecryptor_m8995 ();
extern "C" void RijndaelManaged_CreateEncryptor_m8996 ();
extern "C" void RijndaelTransform__ctor_m8997 ();
extern "C" void RijndaelTransform__cctor_m8998 ();
extern "C" void RijndaelTransform_Clear_m8999 ();
extern "C" void RijndaelTransform_ECB_m9000 ();
extern "C" void RijndaelTransform_SubByte_m9001 ();
extern "C" void RijndaelTransform_Encrypt128_m9002 ();
extern "C" void RijndaelTransform_Encrypt192_m9003 ();
extern "C" void RijndaelTransform_Encrypt256_m9004 ();
extern "C" void RijndaelTransform_Decrypt128_m9005 ();
extern "C" void RijndaelTransform_Decrypt192_m9006 ();
extern "C" void RijndaelTransform_Decrypt256_m9007 ();
extern "C" void RijndaelManagedTransform__ctor_m9008 ();
extern "C" void RijndaelManagedTransform_System_IDisposable_Dispose_m9009 ();
extern "C" void RijndaelManagedTransform_get_CanReuseTransform_m9010 ();
extern "C" void RijndaelManagedTransform_TransformBlock_m9011 ();
extern "C" void RijndaelManagedTransform_TransformFinalBlock_m9012 ();
extern "C" void SHA1__ctor_m9013 ();
extern "C" void SHA1_Create_m4380 ();
extern "C" void SHA1_Create_m9014 ();
extern "C" void SHA1Internal__ctor_m9015 ();
extern "C" void SHA1Internal_HashCore_m9016 ();
extern "C" void SHA1Internal_HashFinal_m9017 ();
extern "C" void SHA1Internal_Initialize_m9018 ();
extern "C" void SHA1Internal_ProcessBlock_m9019 ();
extern "C" void SHA1Internal_InitialiseBuff_m9020 ();
extern "C" void SHA1Internal_FillBuff_m9021 ();
extern "C" void SHA1Internal_ProcessFinalBlock_m9022 ();
extern "C" void SHA1Internal_AddLength_m9023 ();
extern "C" void SHA1CryptoServiceProvider__ctor_m9024 ();
extern "C" void SHA1CryptoServiceProvider_Finalize_m9025 ();
extern "C" void SHA1CryptoServiceProvider_Dispose_m9026 ();
extern "C" void SHA1CryptoServiceProvider_HashCore_m9027 ();
extern "C" void SHA1CryptoServiceProvider_HashFinal_m9028 ();
extern "C" void SHA1CryptoServiceProvider_Initialize_m9029 ();
extern "C" void SHA1Managed__ctor_m9030 ();
extern "C" void SHA1Managed_HashCore_m9031 ();
extern "C" void SHA1Managed_HashFinal_m9032 ();
extern "C" void SHA1Managed_Initialize_m9033 ();
extern "C" void SHA256__ctor_m9034 ();
extern "C" void SHA256_Create_m4381 ();
extern "C" void SHA256_Create_m9035 ();
extern "C" void SHA256Managed__ctor_m9036 ();
extern "C" void SHA256Managed_HashCore_m9037 ();
extern "C" void SHA256Managed_HashFinal_m9038 ();
extern "C" void SHA256Managed_Initialize_m9039 ();
extern "C" void SHA256Managed_ProcessBlock_m9040 ();
extern "C" void SHA256Managed_ProcessFinalBlock_m9041 ();
extern "C" void SHA256Managed_AddLength_m9042 ();
extern "C" void SHA384__ctor_m9043 ();
extern "C" void SHA384Managed__ctor_m9044 ();
extern "C" void SHA384Managed_Initialize_m9045 ();
extern "C" void SHA384Managed_Initialize_m9046 ();
extern "C" void SHA384Managed_HashCore_m9047 ();
extern "C" void SHA384Managed_HashFinal_m9048 ();
extern "C" void SHA384Managed_update_m9049 ();
extern "C" void SHA384Managed_processWord_m9050 ();
extern "C" void SHA384Managed_unpackWord_m9051 ();
extern "C" void SHA384Managed_adjustByteCounts_m9052 ();
extern "C" void SHA384Managed_processLength_m9053 ();
extern "C" void SHA384Managed_processBlock_m9054 ();
extern "C" void SHA512__ctor_m9055 ();
extern "C" void SHA512Managed__ctor_m9056 ();
extern "C" void SHA512Managed_Initialize_m9057 ();
extern "C" void SHA512Managed_Initialize_m9058 ();
extern "C" void SHA512Managed_HashCore_m9059 ();
extern "C" void SHA512Managed_HashFinal_m9060 ();
extern "C" void SHA512Managed_update_m9061 ();
extern "C" void SHA512Managed_processWord_m9062 ();
extern "C" void SHA512Managed_unpackWord_m9063 ();
extern "C" void SHA512Managed_adjustByteCounts_m9064 ();
extern "C" void SHA512Managed_processLength_m9065 ();
extern "C" void SHA512Managed_processBlock_m9066 ();
extern "C" void SHA512Managed_rotateRight_m9067 ();
extern "C" void SHA512Managed_Ch_m9068 ();
extern "C" void SHA512Managed_Maj_m9069 ();
extern "C" void SHA512Managed_Sum0_m9070 ();
extern "C" void SHA512Managed_Sum1_m9071 ();
extern "C" void SHA512Managed_Sigma0_m9072 ();
extern "C" void SHA512Managed_Sigma1_m9073 ();
extern "C" void SHAConstants__cctor_m9074 ();
extern "C" void SignatureDescription__ctor_m9075 ();
extern "C" void SignatureDescription_set_DeformatterAlgorithm_m9076 ();
extern "C" void SignatureDescription_set_DigestAlgorithm_m9077 ();
extern "C" void SignatureDescription_set_FormatterAlgorithm_m9078 ();
extern "C" void SignatureDescription_set_KeyAlgorithm_m9079 ();
extern "C" void DSASignatureDescription__ctor_m9080 ();
extern "C" void RSAPKCS1SHA1SignatureDescription__ctor_m9081 ();
extern "C" void SymmetricAlgorithm__ctor_m3457 ();
extern "C" void SymmetricAlgorithm_System_IDisposable_Dispose_m9082 ();
extern "C" void SymmetricAlgorithm_Finalize_m4351 ();
extern "C" void SymmetricAlgorithm_Clear_m4372 ();
extern "C" void SymmetricAlgorithm_Dispose_m3465 ();
extern "C" void SymmetricAlgorithm_get_BlockSize_m9083 ();
extern "C" void SymmetricAlgorithm_set_BlockSize_m9084 ();
extern "C" void SymmetricAlgorithm_get_FeedbackSize_m9085 ();
extern "C" void SymmetricAlgorithm_get_IV_m3459 ();
extern "C" void SymmetricAlgorithm_set_IV_m3460 ();
extern "C" void SymmetricAlgorithm_get_Key_m3461 ();
extern "C" void SymmetricAlgorithm_set_Key_m3462 ();
extern "C" void SymmetricAlgorithm_get_KeySize_m3463 ();
extern "C" void SymmetricAlgorithm_set_KeySize_m3464 ();
extern "C" void SymmetricAlgorithm_get_LegalKeySizes_m9086 ();
extern "C" void SymmetricAlgorithm_get_Mode_m9087 ();
extern "C" void SymmetricAlgorithm_set_Mode_m9088 ();
extern "C" void SymmetricAlgorithm_get_Padding_m9089 ();
extern "C" void SymmetricAlgorithm_set_Padding_m9090 ();
extern "C" void SymmetricAlgorithm_CreateDecryptor_m9091 ();
extern "C" void SymmetricAlgorithm_CreateEncryptor_m9092 ();
extern "C" void SymmetricAlgorithm_Create_m4371 ();
extern "C" void ToBase64Transform_System_IDisposable_Dispose_m9093 ();
extern "C" void ToBase64Transform_Finalize_m9094 ();
extern "C" void ToBase64Transform_get_CanReuseTransform_m9095 ();
extern "C" void ToBase64Transform_get_InputBlockSize_m9096 ();
extern "C" void ToBase64Transform_get_OutputBlockSize_m9097 ();
extern "C" void ToBase64Transform_Dispose_m9098 ();
extern "C" void ToBase64Transform_TransformBlock_m9099 ();
extern "C" void ToBase64Transform_InternalTransformBlock_m9100 ();
extern "C" void ToBase64Transform_TransformFinalBlock_m9101 ();
extern "C" void ToBase64Transform_InternalTransformFinalBlock_m9102 ();
extern "C" void TripleDES__ctor_m9103 ();
extern "C" void TripleDES_get_Key_m9104 ();
extern "C" void TripleDES_set_Key_m9105 ();
extern "C" void TripleDES_IsWeakKey_m9106 ();
extern "C" void TripleDES_Create_m4409 ();
extern "C" void TripleDES_Create_m9107 ();
extern "C" void TripleDESCryptoServiceProvider__ctor_m9108 ();
extern "C" void TripleDESCryptoServiceProvider_GenerateIV_m9109 ();
extern "C" void TripleDESCryptoServiceProvider_GenerateKey_m9110 ();
extern "C" void TripleDESCryptoServiceProvider_CreateDecryptor_m9111 ();
extern "C" void TripleDESCryptoServiceProvider_CreateEncryptor_m9112 ();
extern "C" void TripleDESTransform__ctor_m9113 ();
extern "C" void TripleDESTransform_ECB_m9114 ();
extern "C" void TripleDESTransform_GetStrongKey_m9115 ();
extern "C" void SecurityPermission__ctor_m9116 ();
extern "C" void SecurityPermission_set_Flags_m9117 ();
extern "C" void SecurityPermission_IsUnrestricted_m9118 ();
extern "C" void SecurityPermission_IsSubsetOf_m9119 ();
extern "C" void SecurityPermission_ToXml_m9120 ();
extern "C" void SecurityPermission_IsEmpty_m9121 ();
extern "C" void SecurityPermission_Cast_m9122 ();
extern "C" void StrongNamePublicKeyBlob_Equals_m9123 ();
extern "C" void StrongNamePublicKeyBlob_GetHashCode_m9124 ();
extern "C" void StrongNamePublicKeyBlob_ToString_m9125 ();
extern "C" void ApplicationTrust__ctor_m9126 ();
extern "C" void EvidenceEnumerator__ctor_m9127 ();
extern "C" void EvidenceEnumerator_MoveNext_m9128 ();
extern "C" void EvidenceEnumerator_get_Current_m9129 ();
extern "C" void Evidence__ctor_m9130 ();
extern "C" void Evidence_get_Count_m9131 ();
extern "C" void Evidence_get_IsSynchronized_m9132 ();
extern "C" void Evidence_get_SyncRoot_m9133 ();
extern "C" void Evidence_get_HostEvidenceList_m9134 ();
extern "C" void Evidence_get_AssemblyEvidenceList_m9135 ();
extern "C" void Evidence_CopyTo_m9136 ();
extern "C" void Evidence_Equals_m9137 ();
extern "C" void Evidence_GetEnumerator_m9138 ();
extern "C" void Evidence_GetHashCode_m9139 ();
extern "C" void Hash__ctor_m9140 ();
extern "C" void Hash__ctor_m9141 ();
extern "C" void Hash_GetObjectData_m9142 ();
extern "C" void Hash_ToString_m9143 ();
extern "C" void Hash_GetData_m9144 ();
extern "C" void StrongName_get_Name_m9145 ();
extern "C" void StrongName_get_PublicKey_m9146 ();
extern "C" void StrongName_get_Version_m9147 ();
extern "C" void StrongName_Equals_m9148 ();
extern "C" void StrongName_GetHashCode_m9149 ();
extern "C" void StrongName_ToString_m9150 ();
extern "C" void WindowsIdentity__ctor_m9151 ();
extern "C" void WindowsIdentity__cctor_m9152 ();
extern "C" void WindowsIdentity_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m9153 ();
extern "C" void WindowsIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m9154 ();
extern "C" void WindowsIdentity_Dispose_m9155 ();
extern "C" void WindowsIdentity_GetCurrentToken_m9156 ();
extern "C" void WindowsIdentity_GetTokenName_m9157 ();
extern "C" void CodeAccessPermission__ctor_m9158 ();
extern "C" void CodeAccessPermission_Equals_m9159 ();
extern "C" void CodeAccessPermission_GetHashCode_m9160 ();
extern "C" void CodeAccessPermission_ToString_m9161 ();
extern "C" void CodeAccessPermission_Element_m9162 ();
extern "C" void CodeAccessPermission_ThrowInvalidPermission_m9163 ();
extern "C" void PermissionSet__ctor_m9164 ();
extern "C" void PermissionSet__ctor_m9165 ();
extern "C" void PermissionSet_set_DeclarativeSecurity_m9166 ();
extern "C" void PermissionSet_CreateFromBinaryFormat_m9167 ();
extern "C" void SecurityContext__ctor_m9168 ();
extern "C" void SecurityContext__ctor_m9169 ();
extern "C" void SecurityContext_Capture_m9170 ();
extern "C" void SecurityContext_get_FlowSuppressed_m9171 ();
extern "C" void SecurityContext_get_CompressedStack_m9172 ();
extern "C" void SecurityAttribute__ctor_m9173 ();
extern "C" void SecurityAttribute_get_Name_m9174 ();
extern "C" void SecurityAttribute_get_Value_m9175 ();
extern "C" void SecurityElement__ctor_m9176 ();
extern "C" void SecurityElement__ctor_m9177 ();
extern "C" void SecurityElement__cctor_m9178 ();
extern "C" void SecurityElement_get_Children_m9179 ();
extern "C" void SecurityElement_get_Tag_m9180 ();
extern "C" void SecurityElement_set_Text_m9181 ();
extern "C" void SecurityElement_AddAttribute_m9182 ();
extern "C" void SecurityElement_AddChild_m9183 ();
extern "C" void SecurityElement_Escape_m9184 ();
extern "C" void SecurityElement_Unescape_m9185 ();
extern "C" void SecurityElement_IsValidAttributeName_m9186 ();
extern "C" void SecurityElement_IsValidAttributeValue_m9187 ();
extern "C" void SecurityElement_IsValidTag_m9188 ();
extern "C" void SecurityElement_IsValidText_m9189 ();
extern "C" void SecurityElement_SearchForChildByTag_m9190 ();
extern "C" void SecurityElement_ToString_m9191 ();
extern "C" void SecurityElement_ToXml_m9192 ();
extern "C" void SecurityElement_GetAttribute_m9193 ();
extern "C" void SecurityException__ctor_m9194 ();
extern "C" void SecurityException__ctor_m9195 ();
extern "C" void SecurityException__ctor_m9196 ();
extern "C" void SecurityException_get_Demanded_m9197 ();
extern "C" void SecurityException_get_FirstPermissionThatFailed_m9198 ();
extern "C" void SecurityException_get_PermissionState_m9199 ();
extern "C" void SecurityException_get_PermissionType_m9200 ();
extern "C" void SecurityException_get_GrantedSet_m9201 ();
extern "C" void SecurityException_get_RefusedSet_m9202 ();
extern "C" void SecurityException_GetObjectData_m9203 ();
extern "C" void SecurityException_ToString_m9204 ();
extern "C" void SecurityFrame__ctor_m9205 ();
extern "C" void SecurityFrame__GetSecurityStack_m9206 ();
extern "C" void SecurityFrame_InitFromRuntimeFrame_m9207 ();
extern "C" void SecurityFrame_get_Assembly_m9208 ();
extern "C" void SecurityFrame_get_Domain_m9209 ();
extern "C" void SecurityFrame_ToString_m9210 ();
extern "C" void SecurityFrame_GetStack_m9211 ();
extern "C" void SecurityManager__cctor_m9212 ();
extern "C" void SecurityManager_get_SecurityEnabled_m9213 ();
extern "C" void SecurityManager_Decode_m9214 ();
extern "C" void SecurityManager_Decode_m9215 ();
extern "C" void SecuritySafeCriticalAttribute__ctor_m9216 ();
extern "C" void SuppressUnmanagedCodeSecurityAttribute__ctor_m9217 ();
extern "C" void UnverifiableCodeAttribute__ctor_m9218 ();
extern "C" void ASCIIEncoding__ctor_m9219 ();
extern "C" void ASCIIEncoding_GetByteCount_m9220 ();
extern "C" void ASCIIEncoding_GetByteCount_m9221 ();
extern "C" void ASCIIEncoding_GetBytes_m9222 ();
extern "C" void ASCIIEncoding_GetBytes_m9223 ();
extern "C" void ASCIIEncoding_GetBytes_m9224 ();
extern "C" void ASCIIEncoding_GetBytes_m9225 ();
extern "C" void ASCIIEncoding_GetCharCount_m9226 ();
extern "C" void ASCIIEncoding_GetChars_m9227 ();
extern "C" void ASCIIEncoding_GetChars_m9228 ();
extern "C" void ASCIIEncoding_GetMaxByteCount_m9229 ();
extern "C" void ASCIIEncoding_GetMaxCharCount_m9230 ();
extern "C" void ASCIIEncoding_GetString_m9231 ();
extern "C" void ASCIIEncoding_GetBytes_m9232 ();
extern "C" void ASCIIEncoding_GetByteCount_m9233 ();
extern "C" void ASCIIEncoding_GetDecoder_m9234 ();
extern "C" void Decoder__ctor_m9235 ();
extern "C" void Decoder_set_Fallback_m9236 ();
extern "C" void Decoder_get_FallbackBuffer_m9237 ();
extern "C" void DecoderExceptionFallback__ctor_m9238 ();
extern "C" void DecoderExceptionFallback_CreateFallbackBuffer_m9239 ();
extern "C" void DecoderExceptionFallback_Equals_m9240 ();
extern "C" void DecoderExceptionFallback_GetHashCode_m9241 ();
extern "C" void DecoderExceptionFallbackBuffer__ctor_m9242 ();
extern "C" void DecoderExceptionFallbackBuffer_get_Remaining_m9243 ();
extern "C" void DecoderExceptionFallbackBuffer_Fallback_m9244 ();
extern "C" void DecoderExceptionFallbackBuffer_GetNextChar_m9245 ();
extern "C" void DecoderFallback__ctor_m9246 ();
extern "C" void DecoderFallback__cctor_m9247 ();
extern "C" void DecoderFallback_get_ExceptionFallback_m9248 ();
extern "C" void DecoderFallback_get_ReplacementFallback_m9249 ();
extern "C" void DecoderFallback_get_StandardSafeFallback_m9250 ();
extern "C" void DecoderFallbackBuffer__ctor_m9251 ();
extern "C" void DecoderFallbackBuffer_Reset_m9252 ();
extern "C" void DecoderFallbackException__ctor_m9253 ();
extern "C" void DecoderFallbackException__ctor_m9254 ();
extern "C" void DecoderFallbackException__ctor_m9255 ();
extern "C" void DecoderReplacementFallback__ctor_m9256 ();
extern "C" void DecoderReplacementFallback__ctor_m9257 ();
extern "C" void DecoderReplacementFallback_get_DefaultString_m9258 ();
extern "C" void DecoderReplacementFallback_CreateFallbackBuffer_m9259 ();
extern "C" void DecoderReplacementFallback_Equals_m9260 ();
extern "C" void DecoderReplacementFallback_GetHashCode_m9261 ();
extern "C" void DecoderReplacementFallbackBuffer__ctor_m9262 ();
extern "C" void DecoderReplacementFallbackBuffer_get_Remaining_m9263 ();
extern "C" void DecoderReplacementFallbackBuffer_Fallback_m9264 ();
extern "C" void DecoderReplacementFallbackBuffer_GetNextChar_m9265 ();
extern "C" void DecoderReplacementFallbackBuffer_Reset_m9266 ();
extern "C" void EncoderExceptionFallback__ctor_m9267 ();
extern "C" void EncoderExceptionFallback_CreateFallbackBuffer_m9268 ();
extern "C" void EncoderExceptionFallback_Equals_m9269 ();
extern "C" void EncoderExceptionFallback_GetHashCode_m9270 ();
extern "C" void EncoderExceptionFallbackBuffer__ctor_m9271 ();
extern "C" void EncoderExceptionFallbackBuffer_get_Remaining_m9272 ();
extern "C" void EncoderExceptionFallbackBuffer_Fallback_m9273 ();
extern "C" void EncoderExceptionFallbackBuffer_Fallback_m9274 ();
extern "C" void EncoderExceptionFallbackBuffer_GetNextChar_m9275 ();
extern "C" void EncoderFallback__ctor_m9276 ();
extern "C" void EncoderFallback__cctor_m9277 ();
extern "C" void EncoderFallback_get_ExceptionFallback_m9278 ();
extern "C" void EncoderFallback_get_ReplacementFallback_m9279 ();
extern "C" void EncoderFallback_get_StandardSafeFallback_m9280 ();
extern "C" void EncoderFallbackBuffer__ctor_m9281 ();
extern "C" void EncoderFallbackException__ctor_m9282 ();
extern "C" void EncoderFallbackException__ctor_m9283 ();
extern "C" void EncoderFallbackException__ctor_m9284 ();
extern "C" void EncoderFallbackException__ctor_m9285 ();
extern "C" void EncoderReplacementFallback__ctor_m9286 ();
extern "C" void EncoderReplacementFallback__ctor_m9287 ();
extern "C" void EncoderReplacementFallback_get_DefaultString_m9288 ();
extern "C" void EncoderReplacementFallback_CreateFallbackBuffer_m9289 ();
extern "C" void EncoderReplacementFallback_Equals_m9290 ();
extern "C" void EncoderReplacementFallback_GetHashCode_m9291 ();
extern "C" void EncoderReplacementFallbackBuffer__ctor_m9292 ();
extern "C" void EncoderReplacementFallbackBuffer_get_Remaining_m9293 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m9294 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m9295 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m9296 ();
extern "C" void EncoderReplacementFallbackBuffer_GetNextChar_m9297 ();
extern "C" void ForwardingDecoder__ctor_m9298 ();
extern "C" void ForwardingDecoder_GetChars_m9299 ();
extern "C" void Encoding__ctor_m9300 ();
extern "C" void Encoding__ctor_m9301 ();
extern "C" void Encoding__cctor_m9302 ();
extern "C" void Encoding___m9303 ();
extern "C" void Encoding_get_IsReadOnly_m9304 ();
extern "C" void Encoding_get_DecoderFallback_m9305 ();
extern "C" void Encoding_set_DecoderFallback_m9306 ();
extern "C" void Encoding_get_EncoderFallback_m9307 ();
extern "C" void Encoding_SetFallbackInternal_m9308 ();
extern "C" void Encoding_Equals_m9309 ();
extern "C" void Encoding_GetByteCount_m9310 ();
extern "C" void Encoding_GetByteCount_m9311 ();
extern "C" void Encoding_GetBytes_m9312 ();
extern "C" void Encoding_GetBytes_m9313 ();
extern "C" void Encoding_GetBytes_m9314 ();
extern "C" void Encoding_GetBytes_m9315 ();
extern "C" void Encoding_GetChars_m9316 ();
extern "C" void Encoding_GetDecoder_m9317 ();
extern "C" void Encoding_InvokeI18N_m9318 ();
extern "C" void Encoding_GetEncoding_m9319 ();
extern "C" void Encoding_GetEncoding_m3271 ();
extern "C" void Encoding_GetHashCode_m9320 ();
extern "C" void Encoding_GetPreamble_m9321 ();
extern "C" void Encoding_GetString_m9322 ();
extern "C" void Encoding_GetString_m9323 ();
extern "C" void Encoding_get_HeaderName_m9324 ();
extern "C" void Encoding_get_WebName_m9325 ();
extern "C" void Encoding_get_ASCII_m3266 ();
extern "C" void Encoding_get_BigEndianUnicode_m4368 ();
extern "C" void Encoding_InternalCodePage_m9326 ();
extern "C" void Encoding_get_Default_m9327 ();
extern "C" void Encoding_get_ISOLatin1_m9328 ();
extern "C" void Encoding_get_UTF7_m4374 ();
extern "C" void Encoding_get_UTF8_m3272 ();
extern "C" void Encoding_get_UTF8Unmarked_m9329 ();
extern "C" void Encoding_get_UTF8UnmarkedUnsafe_m9330 ();
extern "C" void Encoding_get_Unicode_m9331 ();
extern "C" void Encoding_get_UTF32_m9332 ();
extern "C" void Encoding_get_BigEndianUTF32_m9333 ();
extern "C" void Encoding_GetByteCount_m9334 ();
extern "C" void Encoding_GetBytes_m9335 ();
extern "C" void Latin1Encoding__ctor_m9336 ();
extern "C" void Latin1Encoding_GetByteCount_m9337 ();
extern "C" void Latin1Encoding_GetByteCount_m9338 ();
extern "C" void Latin1Encoding_GetBytes_m9339 ();
extern "C" void Latin1Encoding_GetBytes_m9340 ();
extern "C" void Latin1Encoding_GetBytes_m9341 ();
extern "C" void Latin1Encoding_GetBytes_m9342 ();
extern "C" void Latin1Encoding_GetCharCount_m9343 ();
extern "C" void Latin1Encoding_GetChars_m9344 ();
extern "C" void Latin1Encoding_GetMaxByteCount_m9345 ();
extern "C" void Latin1Encoding_GetMaxCharCount_m9346 ();
extern "C" void Latin1Encoding_GetString_m9347 ();
extern "C" void Latin1Encoding_GetString_m9348 ();
extern "C" void Latin1Encoding_get_HeaderName_m9349 ();
extern "C" void Latin1Encoding_get_WebName_m9350 ();
extern "C" void StringBuilder__ctor_m9351 ();
extern "C" void StringBuilder__ctor_m9352 ();
extern "C" void StringBuilder__ctor_m1477 ();
extern "C" void StringBuilder__ctor_m3326 ();
extern "C" void StringBuilder__ctor_m1539 ();
extern "C" void StringBuilder__ctor_m5358 ();
extern "C" void StringBuilder__ctor_m9353 ();
extern "C" void StringBuilder_System_Runtime_Serialization_ISerializable_GetObjectData_m9354 ();
extern "C" void StringBuilder_get_Capacity_m9355 ();
extern "C" void StringBuilder_set_Capacity_m9356 ();
extern "C" void StringBuilder_get_Length_m4403 ();
extern "C" void StringBuilder_set_Length_m5425 ();
extern "C" void StringBuilder_get_Chars_m9357 ();
extern "C" void StringBuilder_set_Chars_m9358 ();
extern "C" void StringBuilder_ToString_m1480 ();
extern "C" void StringBuilder_ToString_m9359 ();
extern "C" void StringBuilder_Remove_m9360 ();
extern "C" void StringBuilder_Replace_m9361 ();
extern "C" void StringBuilder_Replace_m9362 ();
extern "C" void StringBuilder_Append_m3330 ();
extern "C" void StringBuilder_Append_m5384 ();
extern "C" void StringBuilder_Append_m5368 ();
extern "C" void StringBuilder_Append_m5359 ();
extern "C" void StringBuilder_Append_m1781 ();
extern "C" void StringBuilder_Append_m9363 ();
extern "C" void StringBuilder_Append_m9364 ();
extern "C" void StringBuilder_Append_m5400 ();
extern "C" void StringBuilder_AppendLine_m1479 ();
extern "C" void StringBuilder_AppendLine_m1478 ();
extern "C" void StringBuilder_AppendFormat_m4345 ();
extern "C" void StringBuilder_AppendFormat_m9365 ();
extern "C" void StringBuilder_AppendFormat_m4344 ();
extern "C" void StringBuilder_AppendFormat_m4343 ();
extern "C" void StringBuilder_AppendFormat_m5393 ();
extern "C" void StringBuilder_Insert_m9366 ();
extern "C" void StringBuilder_Insert_m9367 ();
extern "C" void StringBuilder_Insert_m9368 ();
extern "C" void StringBuilder_InternalEnsureCapacity_m9369 ();
extern "C" void UTF32Decoder__ctor_m9370 ();
extern "C" void UTF32Decoder_GetChars_m9371 ();
extern "C" void UTF32Encoding__ctor_m9372 ();
extern "C" void UTF32Encoding__ctor_m9373 ();
extern "C" void UTF32Encoding__ctor_m9374 ();
extern "C" void UTF32Encoding_GetByteCount_m9375 ();
extern "C" void UTF32Encoding_GetBytes_m9376 ();
extern "C" void UTF32Encoding_GetCharCount_m9377 ();
extern "C" void UTF32Encoding_GetChars_m9378 ();
extern "C" void UTF32Encoding_GetMaxByteCount_m9379 ();
extern "C" void UTF32Encoding_GetMaxCharCount_m9380 ();
extern "C" void UTF32Encoding_GetDecoder_m9381 ();
extern "C" void UTF32Encoding_GetPreamble_m9382 ();
extern "C" void UTF32Encoding_Equals_m9383 ();
extern "C" void UTF32Encoding_GetHashCode_m9384 ();
extern "C" void UTF32Encoding_GetByteCount_m9385 ();
extern "C" void UTF32Encoding_GetByteCount_m9386 ();
extern "C" void UTF32Encoding_GetBytes_m9387 ();
extern "C" void UTF32Encoding_GetBytes_m9388 ();
extern "C" void UTF32Encoding_GetString_m9389 ();
extern "C" void UTF7Decoder__ctor_m9390 ();
extern "C" void UTF7Decoder_GetChars_m9391 ();
extern "C" void UTF7Encoding__ctor_m9392 ();
extern "C" void UTF7Encoding__ctor_m9393 ();
extern "C" void UTF7Encoding__cctor_m9394 ();
extern "C" void UTF7Encoding_GetHashCode_m9395 ();
extern "C" void UTF7Encoding_Equals_m9396 ();
extern "C" void UTF7Encoding_InternalGetByteCount_m9397 ();
extern "C" void UTF7Encoding_GetByteCount_m9398 ();
extern "C" void UTF7Encoding_InternalGetBytes_m9399 ();
extern "C" void UTF7Encoding_GetBytes_m9400 ();
extern "C" void UTF7Encoding_InternalGetCharCount_m9401 ();
extern "C" void UTF7Encoding_GetCharCount_m9402 ();
extern "C" void UTF7Encoding_InternalGetChars_m9403 ();
extern "C" void UTF7Encoding_GetChars_m9404 ();
extern "C" void UTF7Encoding_GetMaxByteCount_m9405 ();
extern "C" void UTF7Encoding_GetMaxCharCount_m9406 ();
extern "C" void UTF7Encoding_GetDecoder_m9407 ();
extern "C" void UTF7Encoding_GetByteCount_m9408 ();
extern "C" void UTF7Encoding_GetByteCount_m9409 ();
extern "C" void UTF7Encoding_GetBytes_m9410 ();
extern "C" void UTF7Encoding_GetBytes_m9411 ();
extern "C" void UTF7Encoding_GetString_m9412 ();
extern "C" void UTF8Decoder__ctor_m9413 ();
extern "C" void UTF8Decoder_GetChars_m9414 ();
extern "C" void UTF8Encoding__ctor_m9415 ();
extern "C" void UTF8Encoding__ctor_m9416 ();
extern "C" void UTF8Encoding__ctor_m9417 ();
extern "C" void UTF8Encoding_InternalGetByteCount_m9418 ();
extern "C" void UTF8Encoding_InternalGetByteCount_m9419 ();
extern "C" void UTF8Encoding_GetByteCount_m9420 ();
extern "C" void UTF8Encoding_GetByteCount_m9421 ();
extern "C" void UTF8Encoding_InternalGetBytes_m9422 ();
extern "C" void UTF8Encoding_InternalGetBytes_m9423 ();
extern "C" void UTF8Encoding_GetBytes_m9424 ();
extern "C" void UTF8Encoding_GetBytes_m9425 ();
extern "C" void UTF8Encoding_GetBytes_m9426 ();
extern "C" void UTF8Encoding_InternalGetCharCount_m9427 ();
extern "C" void UTF8Encoding_InternalGetCharCount_m9428 ();
extern "C" void UTF8Encoding_Fallback_m9429 ();
extern "C" void UTF8Encoding_Fallback_m9430 ();
extern "C" void UTF8Encoding_GetCharCount_m9431 ();
extern "C" void UTF8Encoding_InternalGetChars_m9432 ();
extern "C" void UTF8Encoding_InternalGetChars_m9433 ();
extern "C" void UTF8Encoding_GetChars_m9434 ();
extern "C" void UTF8Encoding_GetMaxByteCount_m9435 ();
extern "C" void UTF8Encoding_GetMaxCharCount_m9436 ();
extern "C" void UTF8Encoding_GetDecoder_m9437 ();
extern "C" void UTF8Encoding_GetPreamble_m9438 ();
extern "C" void UTF8Encoding_Equals_m9439 ();
extern "C" void UTF8Encoding_GetHashCode_m9440 ();
extern "C" void UTF8Encoding_GetByteCount_m9441 ();
extern "C" void UTF8Encoding_GetString_m9442 ();
extern "C" void UnicodeDecoder__ctor_m9443 ();
extern "C" void UnicodeDecoder_GetChars_m9444 ();
extern "C" void UnicodeEncoding__ctor_m9445 ();
extern "C" void UnicodeEncoding__ctor_m9446 ();
extern "C" void UnicodeEncoding__ctor_m9447 ();
extern "C" void UnicodeEncoding_GetByteCount_m9448 ();
extern "C" void UnicodeEncoding_GetByteCount_m9449 ();
extern "C" void UnicodeEncoding_GetByteCount_m9450 ();
extern "C" void UnicodeEncoding_GetBytes_m9451 ();
extern "C" void UnicodeEncoding_GetBytes_m9452 ();
extern "C" void UnicodeEncoding_GetBytes_m9453 ();
extern "C" void UnicodeEncoding_GetBytesInternal_m9454 ();
extern "C" void UnicodeEncoding_GetCharCount_m9455 ();
extern "C" void UnicodeEncoding_GetChars_m9456 ();
extern "C" void UnicodeEncoding_GetString_m9457 ();
extern "C" void UnicodeEncoding_GetCharsInternal_m9458 ();
extern "C" void UnicodeEncoding_GetMaxByteCount_m9459 ();
extern "C" void UnicodeEncoding_GetMaxCharCount_m9460 ();
extern "C" void UnicodeEncoding_GetDecoder_m9461 ();
extern "C" void UnicodeEncoding_GetPreamble_m9462 ();
extern "C" void UnicodeEncoding_Equals_m9463 ();
extern "C" void UnicodeEncoding_GetHashCode_m9464 ();
extern "C" void UnicodeEncoding_CopyChars_m9465 ();
extern "C" void CompressedStack__ctor_m9466 ();
extern "C" void CompressedStack__ctor_m9467 ();
extern "C" void CompressedStack_CreateCopy_m9468 ();
extern "C" void CompressedStack_Capture_m9469 ();
extern "C" void CompressedStack_GetObjectData_m9470 ();
extern "C" void CompressedStack_IsEmpty_m9471 ();
extern "C" void EventWaitHandle__ctor_m9472 ();
extern "C" void EventWaitHandle_IsManualReset_m9473 ();
extern "C" void EventWaitHandle_Reset_m4436 ();
extern "C" void EventWaitHandle_Set_m4434 ();
extern "C" void ExecutionContext__ctor_m9474 ();
extern "C" void ExecutionContext__ctor_m9475 ();
extern "C" void ExecutionContext__ctor_m9476 ();
extern "C" void ExecutionContext_Capture_m9477 ();
extern "C" void ExecutionContext_GetObjectData_m9478 ();
extern "C" void ExecutionContext_get_SecurityContext_m9479 ();
extern "C" void ExecutionContext_set_SecurityContext_m9480 ();
extern "C" void ExecutionContext_get_FlowSuppressed_m9481 ();
extern "C" void ExecutionContext_IsFlowSuppressed_m9482 ();
extern "C" void Interlocked_CompareExchange_m9483 ();
extern "C" void ManualResetEvent__ctor_m4433 ();
extern "C" void Monitor_Enter_m4417 ();
extern "C" void Monitor_Exit_m4419 ();
extern "C" void Monitor_Monitor_pulse_m9484 ();
extern "C" void Monitor_Monitor_test_synchronised_m9485 ();
extern "C" void Monitor_Pulse_m9486 ();
extern "C" void Monitor_Monitor_wait_m9487 ();
extern "C" void Monitor_Wait_m9488 ();
extern "C" void Mutex__ctor_m9489 ();
extern "C" void Mutex_CreateMutex_internal_m9490 ();
extern "C" void Mutex_ReleaseMutex_internal_m9491 ();
extern "C" void Mutex_ReleaseMutex_m9492 ();
extern "C" void NativeEventCalls_CreateEvent_internal_m9493 ();
extern "C" void NativeEventCalls_SetEvent_internal_m9494 ();
extern "C" void NativeEventCalls_ResetEvent_internal_m9495 ();
extern "C" void NativeEventCalls_CloseEvent_internal_m9496 ();
extern "C" void SynchronizationLockException__ctor_m9497 ();
extern "C" void SynchronizationLockException__ctor_m9498 ();
extern "C" void SynchronizationLockException__ctor_m9499 ();
extern "C" void Thread__ctor_m9500 ();
extern "C" void Thread__cctor_m9501 ();
extern "C" void Thread_get_CurrentContext_m9502 ();
extern "C" void Thread_CurrentThread_internal_m9503 ();
extern "C" void Thread_get_CurrentThread_m9504 ();
extern "C" void Thread_FreeLocalSlotValues_m9505 ();
extern "C" void Thread_GetDomainID_m9506 ();
extern "C" void Thread_Thread_internal_m9507 ();
extern "C" void Thread_Thread_init_m9508 ();
extern "C" void Thread_GetCachedCurrentCulture_m9509 ();
extern "C" void Thread_GetSerializedCurrentCulture_m9510 ();
extern "C" void Thread_SetCachedCurrentCulture_m9511 ();
extern "C" void Thread_GetCachedCurrentUICulture_m9512 ();
extern "C" void Thread_GetSerializedCurrentUICulture_m9513 ();
extern "C" void Thread_SetCachedCurrentUICulture_m9514 ();
extern "C" void Thread_get_CurrentCulture_m9515 ();
extern "C" void Thread_get_CurrentUICulture_m9516 ();
extern "C" void Thread_set_IsBackground_m9517 ();
extern "C" void Thread_SetName_internal_m9518 ();
extern "C" void Thread_set_Name_m9519 ();
extern "C" void Thread_Start_m9520 ();
extern "C" void Thread_Thread_free_internal_m9521 ();
extern "C" void Thread_Finalize_m9522 ();
extern "C" void Thread_SetState_m9523 ();
extern "C" void Thread_ClrState_m9524 ();
extern "C" void Thread_GetNewManagedId_m9525 ();
extern "C" void Thread_GetNewManagedId_internal_m9526 ();
extern "C" void Thread_get_ExecutionContext_m9527 ();
extern "C" void Thread_get_ManagedThreadId_m9528 ();
extern "C" void Thread_GetHashCode_m9529 ();
extern "C" void Thread_GetCompressedStack_m9530 ();
extern "C" void ThreadAbortException__ctor_m9531 ();
extern "C" void ThreadAbortException__ctor_m9532 ();
extern "C" void ThreadInterruptedException__ctor_m9533 ();
extern "C" void ThreadInterruptedException__ctor_m9534 ();
extern "C" void ThreadPool_QueueUserWorkItem_m9535 ();
extern "C" void ThreadStateException__ctor_m9536 ();
extern "C" void ThreadStateException__ctor_m9537 ();
extern "C" void TimerComparer__ctor_m9538 ();
extern "C" void TimerComparer_Compare_m9539 ();
extern "C" void Scheduler__ctor_m9540 ();
extern "C" void Scheduler__cctor_m9541 ();
extern "C" void Scheduler_get_Instance_m9542 ();
extern "C" void Scheduler_Remove_m9543 ();
extern "C" void Scheduler_Change_m9544 ();
extern "C" void Scheduler_Add_m9545 ();
extern "C" void Scheduler_InternalRemove_m9546 ();
extern "C" void Scheduler_SchedulerThread_m9547 ();
extern "C" void Scheduler_ShrinkIfNeeded_m9548 ();
extern "C" void Timer__cctor_m9549 ();
extern "C" void Timer_Change_m9550 ();
extern "C" void Timer_Dispose_m9551 ();
extern "C" void Timer_Change_m9552 ();
extern "C" void WaitHandle__ctor_m9553 ();
extern "C" void WaitHandle__cctor_m9554 ();
extern "C" void WaitHandle_System_IDisposable_Dispose_m9555 ();
extern "C" void WaitHandle_get_Handle_m9556 ();
extern "C" void WaitHandle_set_Handle_m9557 ();
extern "C" void WaitHandle_WaitOne_internal_m9558 ();
extern "C" void WaitHandle_Dispose_m9559 ();
extern "C" void WaitHandle_WaitOne_m9560 ();
extern "C" void WaitHandle_WaitOne_m9561 ();
extern "C" void WaitHandle_CheckDisposed_m9562 ();
extern "C" void WaitHandle_Finalize_m9563 ();
extern "C" void AccessViolationException__ctor_m9564 ();
extern "C" void AccessViolationException__ctor_m9565 ();
extern "C" void ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m9566 ();
extern "C" void ActivationContext_Finalize_m9567 ();
extern "C" void ActivationContext_Dispose_m9568 ();
extern "C" void ActivationContext_Dispose_m9569 ();
extern "C" void Activator_CreateInstance_m9570 ();
extern "C" void Activator_CreateInstance_m9571 ();
extern "C" void Activator_CreateInstance_m9572 ();
extern "C" void Activator_CreateInstance_m9573 ();
extern "C" void Activator_CreateInstance_m5377 ();
extern "C" void Activator_CheckType_m9574 ();
extern "C" void Activator_CheckAbstractType_m9575 ();
extern "C" void Activator_CreateInstanceInternal_m9576 ();
extern "C" void AppDomain_add_UnhandledException_m3233 ();
extern "C" void AppDomain_remove_UnhandledException_m9577 ();
extern "C" void AppDomain_getFriendlyName_m9578 ();
extern "C" void AppDomain_getCurDomain_m9579 ();
extern "C" void AppDomain_get_CurrentDomain_m3231 ();
extern "C" void AppDomain_LoadAssembly_m9580 ();
extern "C" void AppDomain_Load_m9581 ();
extern "C" void AppDomain_Load_m9582 ();
extern "C" void AppDomain_InternalSetContext_m9583 ();
extern "C" void AppDomain_InternalGetContext_m9584 ();
extern "C" void AppDomain_InternalGetDefaultContext_m9585 ();
extern "C" void AppDomain_InternalGetProcessGuid_m9586 ();
extern "C" void AppDomain_GetProcessGuid_m9587 ();
extern "C" void AppDomain_ToString_m9588 ();
extern "C" void AppDomain_DoTypeResolve_m9589 ();
extern "C" void AppDomainSetup__ctor_m9590 ();
extern "C" void ApplicationException__ctor_m9591 ();
extern "C" void ApplicationException__ctor_m9592 ();
extern "C" void ApplicationException__ctor_m9593 ();
extern "C" void ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m9594 ();
extern "C" void ApplicationIdentity_ToString_m9595 ();
extern "C" void ArgumentException__ctor_m9596 ();
extern "C" void ArgumentException__ctor_m1919 ();
extern "C" void ArgumentException__ctor_m5395 ();
extern "C" void ArgumentException__ctor_m3454 ();
extern "C" void ArgumentException__ctor_m9597 ();
extern "C" void ArgumentException__ctor_m9598 ();
extern "C" void ArgumentException_get_ParamName_m9599 ();
extern "C" void ArgumentException_get_Message_m9600 ();
extern "C" void ArgumentException_GetObjectData_m9601 ();
extern "C" void ArgumentNullException__ctor_m9602 ();
extern "C" void ArgumentNullException__ctor_m3324 ();
extern "C" void ArgumentNullException__ctor_m5346 ();
extern "C" void ArgumentNullException__ctor_m9603 ();
extern "C" void ArgumentOutOfRangeException__ctor_m5399 ();
extern "C" void ArgumentOutOfRangeException__ctor_m4338 ();
extern "C" void ArgumentOutOfRangeException__ctor_m3331 ();
extern "C" void ArgumentOutOfRangeException__ctor_m9604 ();
extern "C" void ArgumentOutOfRangeException__ctor_m9605 ();
extern "C" void ArgumentOutOfRangeException_get_Message_m9606 ();
extern "C" void ArgumentOutOfRangeException_GetObjectData_m9607 ();
extern "C" void ArithmeticException__ctor_m9608 ();
extern "C" void ArithmeticException__ctor_m4337 ();
extern "C" void ArithmeticException__ctor_m9609 ();
extern "C" void ArrayTypeMismatchException__ctor_m9610 ();
extern "C" void ArrayTypeMismatchException__ctor_m9611 ();
extern "C" void ArrayTypeMismatchException__ctor_m9612 ();
extern "C" void BitConverter__cctor_m9613 ();
extern "C" void BitConverter_AmILittleEndian_m9614 ();
extern "C" void BitConverter_DoubleWordsAreSwapped_m9615 ();
extern "C" void BitConverter_DoubleToInt64Bits_m9616 ();
extern "C" void BitConverter_GetBytes_m9617 ();
extern "C" void BitConverter_GetBytes_m9618 ();
extern "C" void BitConverter_PutBytes_m9619 ();
extern "C" void BitConverter_ToInt64_m9620 ();
extern "C" void BitConverter_ToString_m4418 ();
extern "C" void BitConverter_ToString_m9621 ();
extern "C" void Buffer_ByteLength_m9622 ();
extern "C" void Buffer_BlockCopy_m3450 ();
extern "C" void Buffer_ByteLengthInternal_m9623 ();
extern "C" void Buffer_BlockCopyInternal_m9624 ();
extern "C" void CharEnumerator__ctor_m9625 ();
extern "C" void CharEnumerator_System_Collections_IEnumerator_get_Current_m9626 ();
extern "C" void CharEnumerator_System_IDisposable_Dispose_m9627 ();
extern "C" void CharEnumerator_get_Current_m9628 ();
extern "C" void CharEnumerator_MoveNext_m9629 ();
extern "C" void Console__cctor_m9630 ();
extern "C" void Console_SetEncodings_m9631 ();
extern "C" void Console_get_Error_m5414 ();
extern "C" void Console_Open_m9632 ();
extern "C" void Console_OpenStandardError_m9633 ();
extern "C" void Console_OpenStandardInput_m9634 ();
extern "C" void Console_OpenStandardOutput_m9635 ();
extern "C" void ContextBoundObject__ctor_m9636 ();
extern "C" void Convert__cctor_m9637 ();
extern "C" void Convert_InternalFromBase64String_m9638 ();
extern "C" void Convert_FromBase64String_m4389 ();
extern "C" void Convert_ToBase64String_m3304 ();
extern "C" void Convert_ToBase64String_m9639 ();
extern "C" void Convert_ToBoolean_m9640 ();
extern "C" void Convert_ToBoolean_m9641 ();
extern "C" void Convert_ToBoolean_m9642 ();
extern "C" void Convert_ToBoolean_m9643 ();
extern "C" void Convert_ToBoolean_m9644 ();
extern "C" void Convert_ToBoolean_m9645 ();
extern "C" void Convert_ToBoolean_m9646 ();
extern "C" void Convert_ToBoolean_m9647 ();
extern "C" void Convert_ToBoolean_m9648 ();
extern "C" void Convert_ToBoolean_m9649 ();
extern "C" void Convert_ToBoolean_m9650 ();
extern "C" void Convert_ToBoolean_m9651 ();
extern "C" void Convert_ToBoolean_m3301 ();
extern "C" void Convert_ToBoolean_m9652 ();
extern "C" void Convert_ToByte_m9653 ();
extern "C" void Convert_ToByte_m9654 ();
extern "C" void Convert_ToByte_m9655 ();
extern "C" void Convert_ToByte_m9656 ();
extern "C" void Convert_ToByte_m9657 ();
extern "C" void Convert_ToByte_m9658 ();
extern "C" void Convert_ToByte_m9659 ();
extern "C" void Convert_ToByte_m9660 ();
extern "C" void Convert_ToByte_m9661 ();
extern "C" void Convert_ToByte_m9662 ();
extern "C" void Convert_ToByte_m9663 ();
extern "C" void Convert_ToByte_m9664 ();
extern "C" void Convert_ToByte_m9665 ();
extern "C" void Convert_ToByte_m9666 ();
extern "C" void Convert_ToByte_m9667 ();
extern "C" void Convert_ToChar_m4394 ();
extern "C" void Convert_ToChar_m9668 ();
extern "C" void Convert_ToChar_m9669 ();
extern "C" void Convert_ToChar_m9670 ();
extern "C" void Convert_ToChar_m9671 ();
extern "C" void Convert_ToChar_m9672 ();
extern "C" void Convert_ToChar_m9673 ();
extern "C" void Convert_ToChar_m9674 ();
extern "C" void Convert_ToChar_m9675 ();
extern "C" void Convert_ToChar_m9676 ();
extern "C" void Convert_ToChar_m9677 ();
extern "C" void Convert_ToDateTime_m9678 ();
extern "C" void Convert_ToDateTime_m9679 ();
extern "C" void Convert_ToDateTime_m9680 ();
extern "C" void Convert_ToDateTime_m9681 ();
extern "C" void Convert_ToDateTime_m9682 ();
extern "C" void Convert_ToDateTime_m9683 ();
extern "C" void Convert_ToDateTime_m9684 ();
extern "C" void Convert_ToDateTime_m9685 ();
extern "C" void Convert_ToDateTime_m9686 ();
extern "C" void Convert_ToDateTime_m9687 ();
extern "C" void Convert_ToDecimal_m9688 ();
extern "C" void Convert_ToDecimal_m9689 ();
extern "C" void Convert_ToDecimal_m9690 ();
extern "C" void Convert_ToDecimal_m9691 ();
extern "C" void Convert_ToDecimal_m9692 ();
extern "C" void Convert_ToDecimal_m9693 ();
extern "C" void Convert_ToDecimal_m9694 ();
extern "C" void Convert_ToDecimal_m9695 ();
extern "C" void Convert_ToDecimal_m9696 ();
extern "C" void Convert_ToDecimal_m9697 ();
extern "C" void Convert_ToDecimal_m9698 ();
extern "C" void Convert_ToDecimal_m9699 ();
extern "C" void Convert_ToDecimal_m9700 ();
extern "C" void Convert_ToDouble_m9701 ();
extern "C" void Convert_ToDouble_m9702 ();
extern "C" void Convert_ToDouble_m9703 ();
extern "C" void Convert_ToDouble_m9704 ();
extern "C" void Convert_ToDouble_m9705 ();
extern "C" void Convert_ToDouble_m9706 ();
extern "C" void Convert_ToDouble_m9707 ();
extern "C" void Convert_ToDouble_m9708 ();
extern "C" void Convert_ToDouble_m9709 ();
extern "C" void Convert_ToDouble_m9710 ();
extern "C" void Convert_ToDouble_m9711 ();
extern "C" void Convert_ToDouble_m9712 ();
extern "C" void Convert_ToDouble_m9713 ();
extern "C" void Convert_ToDouble_m3341 ();
extern "C" void Convert_ToInt16_m9714 ();
extern "C" void Convert_ToInt16_m9715 ();
extern "C" void Convert_ToInt16_m9716 ();
extern "C" void Convert_ToInt16_m9717 ();
extern "C" void Convert_ToInt16_m9718 ();
extern "C" void Convert_ToInt16_m9719 ();
extern "C" void Convert_ToInt16_m9720 ();
extern "C" void Convert_ToInt16_m9721 ();
extern "C" void Convert_ToInt16_m9722 ();
extern "C" void Convert_ToInt16_m9723 ();
extern "C" void Convert_ToInt16_m4349 ();
extern "C" void Convert_ToInt16_m9724 ();
extern "C" void Convert_ToInt16_m9725 ();
extern "C" void Convert_ToInt16_m9726 ();
extern "C" void Convert_ToInt16_m9727 ();
extern "C" void Convert_ToInt16_m9728 ();
extern "C" void Convert_ToInt32_m9729 ();
extern "C" void Convert_ToInt32_m9730 ();
extern "C" void Convert_ToInt32_m9731 ();
extern "C" void Convert_ToInt32_m9732 ();
extern "C" void Convert_ToInt32_m9733 ();
extern "C" void Convert_ToInt32_m9734 ();
extern "C" void Convert_ToInt32_m9735 ();
extern "C" void Convert_ToInt32_m9736 ();
extern "C" void Convert_ToInt32_m9737 ();
extern "C" void Convert_ToInt32_m9738 ();
extern "C" void Convert_ToInt32_m9739 ();
extern "C" void Convert_ToInt32_m9740 ();
extern "C" void Convert_ToInt32_m9741 ();
extern "C" void Convert_ToInt32_m3298 ();
extern "C" void Convert_ToInt32_m4402 ();
extern "C" void Convert_ToInt64_m9742 ();
extern "C" void Convert_ToInt64_m9743 ();
extern "C" void Convert_ToInt64_m9744 ();
extern "C" void Convert_ToInt64_m9745 ();
extern "C" void Convert_ToInt64_m9746 ();
extern "C" void Convert_ToInt64_m9747 ();
extern "C" void Convert_ToInt64_m9748 ();
extern "C" void Convert_ToInt64_m9749 ();
extern "C" void Convert_ToInt64_m9750 ();
extern "C" void Convert_ToInt64_m9751 ();
extern "C" void Convert_ToInt64_m9752 ();
extern "C" void Convert_ToInt64_m9753 ();
extern "C" void Convert_ToInt64_m9754 ();
extern "C" void Convert_ToInt64_m9755 ();
extern "C" void Convert_ToInt64_m9756 ();
extern "C" void Convert_ToInt64_m9757 ();
extern "C" void Convert_ToInt64_m9758 ();
extern "C" void Convert_ToSByte_m9759 ();
extern "C" void Convert_ToSByte_m9760 ();
extern "C" void Convert_ToSByte_m9761 ();
extern "C" void Convert_ToSByte_m9762 ();
extern "C" void Convert_ToSByte_m9763 ();
extern "C" void Convert_ToSByte_m9764 ();
extern "C" void Convert_ToSByte_m9765 ();
extern "C" void Convert_ToSByte_m9766 ();
extern "C" void Convert_ToSByte_m9767 ();
extern "C" void Convert_ToSByte_m9768 ();
extern "C" void Convert_ToSByte_m9769 ();
extern "C" void Convert_ToSByte_m9770 ();
extern "C" void Convert_ToSByte_m9771 ();
extern "C" void Convert_ToSByte_m9772 ();
extern "C" void Convert_ToSingle_m9773 ();
extern "C" void Convert_ToSingle_m9774 ();
extern "C" void Convert_ToSingle_m9775 ();
extern "C" void Convert_ToSingle_m9776 ();
extern "C" void Convert_ToSingle_m9777 ();
extern "C" void Convert_ToSingle_m9778 ();
extern "C" void Convert_ToSingle_m9779 ();
extern "C" void Convert_ToSingle_m9780 ();
extern "C" void Convert_ToSingle_m9781 ();
extern "C" void Convert_ToSingle_m9782 ();
extern "C" void Convert_ToSingle_m9783 ();
extern "C" void Convert_ToSingle_m9784 ();
extern "C" void Convert_ToSingle_m9785 ();
extern "C" void Convert_ToSingle_m9786 ();
extern "C" void Convert_ToString_m9787 ();
extern "C" void Convert_ToString_m9788 ();
extern "C" void Convert_ToUInt16_m9789 ();
extern "C" void Convert_ToUInt16_m9790 ();
extern "C" void Convert_ToUInt16_m9791 ();
extern "C" void Convert_ToUInt16_m9792 ();
extern "C" void Convert_ToUInt16_m9793 ();
extern "C" void Convert_ToUInt16_m9794 ();
extern "C" void Convert_ToUInt16_m9795 ();
extern "C" void Convert_ToUInt16_m9796 ();
extern "C" void Convert_ToUInt16_m9797 ();
extern "C" void Convert_ToUInt16_m9798 ();
extern "C" void Convert_ToUInt16_m9799 ();
extern "C" void Convert_ToUInt16_m9800 ();
extern "C" void Convert_ToUInt16_m9801 ();
extern "C" void Convert_ToUInt16_m3299 ();
extern "C" void Convert_ToUInt16_m9802 ();
extern "C" void Convert_ToUInt32_m3259 ();
extern "C" void Convert_ToUInt32_m9803 ();
extern "C" void Convert_ToUInt32_m9804 ();
extern "C" void Convert_ToUInt32_m9805 ();
extern "C" void Convert_ToUInt32_m9806 ();
extern "C" void Convert_ToUInt32_m9807 ();
extern "C" void Convert_ToUInt32_m9808 ();
extern "C" void Convert_ToUInt32_m9809 ();
extern "C" void Convert_ToUInt32_m9810 ();
extern "C" void Convert_ToUInt32_m9811 ();
extern "C" void Convert_ToUInt32_m9812 ();
extern "C" void Convert_ToUInt32_m9813 ();
extern "C" void Convert_ToUInt32_m9814 ();
extern "C" void Convert_ToUInt32_m3258 ();
extern "C" void Convert_ToUInt32_m9815 ();
extern "C" void Convert_ToUInt64_m9816 ();
extern "C" void Convert_ToUInt64_m9817 ();
extern "C" void Convert_ToUInt64_m9818 ();
extern "C" void Convert_ToUInt64_m9819 ();
extern "C" void Convert_ToUInt64_m9820 ();
extern "C" void Convert_ToUInt64_m9821 ();
extern "C" void Convert_ToUInt64_m9822 ();
extern "C" void Convert_ToUInt64_m9823 ();
extern "C" void Convert_ToUInt64_m9824 ();
extern "C" void Convert_ToUInt64_m9825 ();
extern "C" void Convert_ToUInt64_m9826 ();
extern "C" void Convert_ToUInt64_m9827 ();
extern "C" void Convert_ToUInt64_m9828 ();
extern "C" void Convert_ToUInt64_m3300 ();
extern "C" void Convert_ToUInt64_m9829 ();
extern "C" void Convert_ChangeType_m9830 ();
extern "C" void Convert_ToType_m9831 ();
extern "C" void DBNull__ctor_m9832 ();
extern "C" void DBNull__ctor_m9833 ();
extern "C" void DBNull__cctor_m9834 ();
extern "C" void DBNull_System_IConvertible_ToBoolean_m9835 ();
extern "C" void DBNull_System_IConvertible_ToByte_m9836 ();
extern "C" void DBNull_System_IConvertible_ToChar_m9837 ();
extern "C" void DBNull_System_IConvertible_ToDateTime_m9838 ();
extern "C" void DBNull_System_IConvertible_ToDecimal_m9839 ();
extern "C" void DBNull_System_IConvertible_ToDouble_m9840 ();
extern "C" void DBNull_System_IConvertible_ToInt16_m9841 ();
extern "C" void DBNull_System_IConvertible_ToInt32_m9842 ();
extern "C" void DBNull_System_IConvertible_ToInt64_m9843 ();
extern "C" void DBNull_System_IConvertible_ToSByte_m9844 ();
extern "C" void DBNull_System_IConvertible_ToSingle_m9845 ();
extern "C" void DBNull_System_IConvertible_ToType_m9846 ();
extern "C" void DBNull_System_IConvertible_ToUInt16_m9847 ();
extern "C" void DBNull_System_IConvertible_ToUInt32_m9848 ();
extern "C" void DBNull_System_IConvertible_ToUInt64_m9849 ();
extern "C" void DBNull_GetObjectData_m9850 ();
extern "C" void DBNull_ToString_m9851 ();
extern "C" void DBNull_ToString_m9852 ();
extern "C" void DateTime__ctor_m9853 ();
extern "C" void DateTime__ctor_m9854 ();
extern "C" void DateTime__ctor_m3367 ();
extern "C" void DateTime__ctor_m9855 ();
extern "C" void DateTime__ctor_m9856 ();
extern "C" void DateTime__cctor_m9857 ();
extern "C" void DateTime_System_IConvertible_ToBoolean_m9858 ();
extern "C" void DateTime_System_IConvertible_ToByte_m9859 ();
extern "C" void DateTime_System_IConvertible_ToChar_m9860 ();
extern "C" void DateTime_System_IConvertible_ToDateTime_m9861 ();
extern "C" void DateTime_System_IConvertible_ToDecimal_m9862 ();
extern "C" void DateTime_System_IConvertible_ToDouble_m9863 ();
extern "C" void DateTime_System_IConvertible_ToInt16_m9864 ();
extern "C" void DateTime_System_IConvertible_ToInt32_m9865 ();
extern "C" void DateTime_System_IConvertible_ToInt64_m9866 ();
extern "C" void DateTime_System_IConvertible_ToSByte_m9867 ();
extern "C" void DateTime_System_IConvertible_ToSingle_m9868 ();
extern "C" void DateTime_System_IConvertible_ToType_m9869 ();
extern "C" void DateTime_System_IConvertible_ToUInt16_m9870 ();
extern "C" void DateTime_System_IConvertible_ToUInt32_m9871 ();
extern "C" void DateTime_System_IConvertible_ToUInt64_m9872 ();
extern "C" void DateTime_AbsoluteDays_m9873 ();
extern "C" void DateTime_FromTicks_m9874 ();
extern "C" void DateTime_get_Month_m9875 ();
extern "C" void DateTime_get_Day_m9876 ();
extern "C" void DateTime_get_DayOfWeek_m9877 ();
extern "C" void DateTime_get_Hour_m9878 ();
extern "C" void DateTime_get_Minute_m9879 ();
extern "C" void DateTime_get_Second_m9880 ();
extern "C" void DateTime_GetTimeMonotonic_m9881 ();
extern "C" void DateTime_GetNow_m9882 ();
extern "C" void DateTime_get_Now_m3248 ();
extern "C" void DateTime_get_Ticks_m4420 ();
extern "C" void DateTime_get_Today_m9883 ();
extern "C" void DateTime_get_UtcNow_m4383 ();
extern "C" void DateTime_get_Year_m9884 ();
extern "C" void DateTime_get_Kind_m9885 ();
extern "C" void DateTime_Add_m9886 ();
extern "C" void DateTime_AddTicks_m9887 ();
extern "C" void DateTime_AddMilliseconds_m5369 ();
extern "C" void DateTime_AddSeconds_m3368 ();
extern "C" void DateTime_Compare_m9888 ();
extern "C" void DateTime_CompareTo_m9889 ();
extern "C" void DateTime_CompareTo_m9890 ();
extern "C" void DateTime_Equals_m9891 ();
extern "C" void DateTime_FromBinary_m9892 ();
extern "C" void DateTime_SpecifyKind_m9893 ();
extern "C" void DateTime_DaysInMonth_m9894 ();
extern "C" void DateTime_Equals_m9895 ();
extern "C" void DateTime_CheckDateTimeKind_m9896 ();
extern "C" void DateTime_GetHashCode_m9897 ();
extern "C" void DateTime_IsLeapYear_m9898 ();
extern "C" void DateTime_Parse_m9899 ();
extern "C" void DateTime_Parse_m9900 ();
extern "C" void DateTime_CoreParse_m9901 ();
extern "C" void DateTime_YearMonthDayFormats_m9902 ();
extern "C" void DateTime__ParseNumber_m9903 ();
extern "C" void DateTime__ParseEnum_m9904 ();
extern "C" void DateTime__ParseString_m9905 ();
extern "C" void DateTime__ParseAmPm_m9906 ();
extern "C" void DateTime__ParseTimeSeparator_m9907 ();
extern "C" void DateTime__ParseDateSeparator_m9908 ();
extern "C" void DateTime_IsLetter_m9909 ();
extern "C" void DateTime__DoParse_m9910 ();
extern "C" void DateTime_ParseExact_m4350 ();
extern "C" void DateTime_ParseExact_m9911 ();
extern "C" void DateTime_CheckStyle_m9912 ();
extern "C" void DateTime_ParseExact_m9913 ();
extern "C" void DateTime_Subtract_m9914 ();
extern "C" void DateTime_ToString_m9915 ();
extern "C" void DateTime_ToString_m9916 ();
extern "C" void DateTime_ToString_m3353 ();
extern "C" void DateTime_ToLocalTime_m5386 ();
extern "C" void DateTime_ToUniversalTime_m3352 ();
extern "C" void DateTime_op_Addition_m9917 ();
extern "C" void DateTime_op_Equality_m9918 ();
extern "C" void DateTime_op_GreaterThan_m4384 ();
extern "C" void DateTime_op_GreaterThanOrEqual_m5370 ();
extern "C" void DateTime_op_Inequality_m9919 ();
extern "C" void DateTime_op_LessThan_m5394 ();
extern "C" void DateTime_op_LessThanOrEqual_m4385 ();
extern "C" void DateTime_op_Subtraction_m9920 ();
extern "C" void DateTimeOffset__ctor_m9921 ();
extern "C" void DateTimeOffset__ctor_m9922 ();
extern "C" void DateTimeOffset__ctor_m9923 ();
extern "C" void DateTimeOffset__ctor_m9924 ();
extern "C" void DateTimeOffset__cctor_m9925 ();
extern "C" void DateTimeOffset_System_IComparable_CompareTo_m9926 ();
extern "C" void DateTimeOffset_System_Runtime_Serialization_ISerializable_GetObjectData_m9927 ();
extern "C" void DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m9928 ();
extern "C" void DateTimeOffset_CompareTo_m9929 ();
extern "C" void DateTimeOffset_Equals_m9930 ();
extern "C" void DateTimeOffset_Equals_m9931 ();
extern "C" void DateTimeOffset_GetHashCode_m9932 ();
extern "C" void DateTimeOffset_ToString_m9933 ();
extern "C" void DateTimeOffset_ToString_m3355 ();
extern "C" void DateTimeOffset_ToUniversalTime_m3354 ();
extern "C" void DateTimeOffset_get_DateTime_m9934 ();
extern "C" void DateTimeOffset_get_Offset_m9935 ();
extern "C" void DateTimeOffset_get_UtcDateTime_m9936 ();
extern "C" void DateTimeUtils_CountRepeat_m9937 ();
extern "C" void DateTimeUtils_ZeroPad_m9938 ();
extern "C" void DateTimeUtils_ParseQuotedString_m9939 ();
extern "C" void DateTimeUtils_GetStandardPattern_m9940 ();
extern "C" void DateTimeUtils_GetStandardPattern_m9941 ();
extern "C" void DateTimeUtils_ToString_m9942 ();
extern "C" void DateTimeUtils_ToString_m9943 ();
extern "C" void DelegateEntry__ctor_m9944 ();
extern "C" void DelegateEntry_DeserializeDelegate_m9945 ();
extern "C" void DelegateSerializationHolder__ctor_m9946 ();
extern "C" void DelegateSerializationHolder_GetDelegateData_m9947 ();
extern "C" void DelegateSerializationHolder_GetObjectData_m9948 ();
extern "C" void DelegateSerializationHolder_GetRealObject_m9949 ();
extern "C" void DivideByZeroException__ctor_m9950 ();
extern "C" void DivideByZeroException__ctor_m9951 ();
extern "C" void DllNotFoundException__ctor_m9952 ();
extern "C" void DllNotFoundException__ctor_m9953 ();
extern "C" void EntryPointNotFoundException__ctor_m9954 ();
extern "C" void EntryPointNotFoundException__ctor_m9955 ();
extern "C" void SByteComparer__ctor_m9956 ();
extern "C" void SByteComparer_Compare_m9957 ();
extern "C" void SByteComparer_Compare_m9958 ();
extern "C" void ShortComparer__ctor_m9959 ();
extern "C" void ShortComparer_Compare_m9960 ();
extern "C" void ShortComparer_Compare_m9961 ();
extern "C" void IntComparer__ctor_m9962 ();
extern "C" void IntComparer_Compare_m9963 ();
extern "C" void IntComparer_Compare_m9964 ();
extern "C" void LongComparer__ctor_m9965 ();
extern "C" void LongComparer_Compare_m9966 ();
extern "C" void LongComparer_Compare_m9967 ();
extern "C" void MonoEnumInfo__ctor_m9968 ();
extern "C" void MonoEnumInfo__cctor_m9969 ();
extern "C" void MonoEnumInfo_get_enum_info_m9970 ();
extern "C" void MonoEnumInfo_get_Cache_m9971 ();
extern "C" void MonoEnumInfo_GetInfo_m9972 ();
extern "C" void Environment_get_SocketSecurityEnabled_m9973 ();
extern "C" void Environment_get_NewLine_m4342 ();
extern "C" void Environment_get_Platform_m9974 ();
extern "C" void Environment_GetOSVersionString_m9975 ();
extern "C" void Environment_get_OSVersion_m9976 ();
extern "C" void Environment_get_TickCount_m3305 ();
extern "C" void Environment_internalGetEnvironmentVariable_m9977 ();
extern "C" void Environment_GetEnvironmentVariable_m4415 ();
extern "C" void Environment_GetWindowsFolderPath_m9978 ();
extern "C" void Environment_GetFolderPath_m4400 ();
extern "C" void Environment_ReadXdgUserDir_m9979 ();
extern "C" void Environment_InternalGetFolderPath_m9980 ();
extern "C" void Environment_get_IsRunningOnWindows_m9981 ();
extern "C" void Environment_GetMachineConfigPath_m9982 ();
extern "C" void Environment_internalGetHome_m9983 ();
extern "C" void EventArgs__ctor_m9984 ();
extern "C" void EventArgs__cctor_m9985 ();
extern "C" void ExecutionEngineException__ctor_m9986 ();
extern "C" void ExecutionEngineException__ctor_m9987 ();
extern "C" void FieldAccessException__ctor_m9988 ();
extern "C" void FieldAccessException__ctor_m9989 ();
extern "C" void FieldAccessException__ctor_m9990 ();
extern "C" void FlagsAttribute__ctor_m9991 ();
extern "C" void FormatException__ctor_m9992 ();
extern "C" void FormatException__ctor_m3297 ();
extern "C" void FormatException__ctor_m5427 ();
extern "C" void GC_SuppressFinalize_m3451 ();
extern "C" void Guid__ctor_m9993 ();
extern "C" void Guid__ctor_m9994 ();
extern "C" void Guid__cctor_m9995 ();
extern "C" void Guid_CheckNull_m9996 ();
extern "C" void Guid_CheckLength_m9997 ();
extern "C" void Guid_CheckArray_m9998 ();
extern "C" void Guid_Compare_m9999 ();
extern "C" void Guid_CompareTo_m10000 ();
extern "C" void Guid_Equals_m10001 ();
extern "C" void Guid_CompareTo_m10002 ();
extern "C" void Guid_Equals_m10003 ();
extern "C" void Guid_GetHashCode_m10004 ();
extern "C" void Guid_ToHex_m10005 ();
extern "C" void Guid_NewGuid_m10006 ();
extern "C" void Guid_AppendInt_m10007 ();
extern "C" void Guid_AppendShort_m10008 ();
extern "C" void Guid_AppendByte_m10009 ();
extern "C" void Guid_BaseToString_m10010 ();
extern "C" void Guid_ToString_m10011 ();
extern "C" void Guid_ToString_m3356 ();
extern "C" void Guid_ToString_m10012 ();
extern "C" void IndexOutOfRangeException__ctor_m10013 ();
extern "C" void IndexOutOfRangeException__ctor_m3261 ();
extern "C" void IndexOutOfRangeException__ctor_m10014 ();
extern "C" void InvalidCastException__ctor_m10015 ();
extern "C" void InvalidCastException__ctor_m10016 ();
extern "C" void InvalidCastException__ctor_m10017 ();
extern "C" void InvalidOperationException__ctor_m5349 ();
extern "C" void InvalidOperationException__ctor_m4412 ();
extern "C" void InvalidOperationException__ctor_m10018 ();
extern "C" void InvalidOperationException__ctor_m10019 ();
extern "C" void LocalDataStoreSlot__ctor_m10020 ();
extern "C" void LocalDataStoreSlot__cctor_m10021 ();
extern "C" void LocalDataStoreSlot_Finalize_m10022 ();
extern "C" void Math_Abs_m10023 ();
extern "C" void Math_Abs_m10024 ();
extern "C" void Math_Abs_m10025 ();
extern "C" void Math_Ceiling_m10026 ();
extern "C" void Math_Floor_m10027 ();
extern "C" void Math_Log_m3264 ();
extern "C" void Math_Max_m4356 ();
extern "C" void Math_Min_m3449 ();
extern "C" void Math_Round_m10028 ();
extern "C" void Math_Round_m10029 ();
extern "C" void Math_Sin_m10030 ();
extern "C" void Math_Cos_m10031 ();
extern "C" void Math_Log_m10032 ();
extern "C" void Math_Pow_m10033 ();
extern "C" void Math_Sqrt_m10034 ();
extern "C" void MemberAccessException__ctor_m10035 ();
extern "C" void MemberAccessException__ctor_m10036 ();
extern "C" void MemberAccessException__ctor_m10037 ();
extern "C" void MethodAccessException__ctor_m10038 ();
extern "C" void MethodAccessException__ctor_m10039 ();
extern "C" void MissingFieldException__ctor_m10040 ();
extern "C" void MissingFieldException__ctor_m10041 ();
extern "C" void MissingFieldException__ctor_m10042 ();
extern "C" void MissingFieldException_get_Message_m10043 ();
extern "C" void MissingMemberException__ctor_m10044 ();
extern "C" void MissingMemberException__ctor_m10045 ();
extern "C" void MissingMemberException__ctor_m10046 ();
extern "C" void MissingMemberException__ctor_m10047 ();
extern "C" void MissingMemberException_GetObjectData_m10048 ();
extern "C" void MissingMemberException_get_Message_m10049 ();
extern "C" void MissingMethodException__ctor_m10050 ();
extern "C" void MissingMethodException__ctor_m10051 ();
extern "C" void MissingMethodException__ctor_m10052 ();
extern "C" void MissingMethodException__ctor_m10053 ();
extern "C" void MissingMethodException_get_Message_m10054 ();
extern "C" void MonoAsyncCall__ctor_m10055 ();
extern "C" void AttributeInfo__ctor_m10056 ();
extern "C" void AttributeInfo_get_Usage_m10057 ();
extern "C" void AttributeInfo_get_InheritanceLevel_m10058 ();
extern "C" void MonoCustomAttrs__cctor_m10059 ();
extern "C" void MonoCustomAttrs_IsUserCattrProvider_m10060 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesInternal_m10061 ();
extern "C" void MonoCustomAttrs_GetPseudoCustomAttributes_m10062 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesBase_m10063 ();
extern "C" void MonoCustomAttrs_GetCustomAttribute_m10064 ();
extern "C" void MonoCustomAttrs_GetCustomAttributes_m10065 ();
extern "C" void MonoCustomAttrs_GetCustomAttributes_m10066 ();
extern "C" void MonoCustomAttrs_IsDefined_m10067 ();
extern "C" void MonoCustomAttrs_IsDefinedInternal_m10068 ();
extern "C" void MonoCustomAttrs_GetBasePropertyDefinition_m10069 ();
extern "C" void MonoCustomAttrs_GetBase_m10070 ();
extern "C" void MonoCustomAttrs_RetrieveAttributeUsage_m10071 ();
extern "C" void MonoTouchAOTHelper__cctor_m10072 ();
extern "C" void MonoTypeInfo__ctor_m10073 ();
extern "C" void MonoType_get_attributes_m10074 ();
extern "C" void MonoType_GetDefaultConstructor_m10075 ();
extern "C" void MonoType_GetAttributeFlagsImpl_m10076 ();
extern "C" void MonoType_GetConstructorImpl_m10077 ();
extern "C" void MonoType_GetConstructors_internal_m10078 ();
extern "C" void MonoType_GetConstructors_m10079 ();
extern "C" void MonoType_InternalGetEvent_m10080 ();
extern "C" void MonoType_GetEvent_m10081 ();
extern "C" void MonoType_GetField_m10082 ();
extern "C" void MonoType_GetFields_internal_m10083 ();
extern "C" void MonoType_GetFields_m10084 ();
extern "C" void MonoType_GetInterfaces_m10085 ();
extern "C" void MonoType_GetMethodsByName_m10086 ();
extern "C" void MonoType_GetMethods_m10087 ();
extern "C" void MonoType_GetMethodImpl_m10088 ();
extern "C" void MonoType_GetPropertiesByName_m10089 ();
extern "C" void MonoType_GetProperties_m10090 ();
extern "C" void MonoType_GetPropertyImpl_m10091 ();
extern "C" void MonoType_HasElementTypeImpl_m10092 ();
extern "C" void MonoType_IsArrayImpl_m10093 ();
extern "C" void MonoType_IsByRefImpl_m10094 ();
extern "C" void MonoType_IsPointerImpl_m10095 ();
extern "C" void MonoType_IsPrimitiveImpl_m10096 ();
extern "C" void MonoType_IsSubclassOf_m10097 ();
extern "C" void MonoType_InvokeMember_m10098 ();
extern "C" void MonoType_GetElementType_m10099 ();
extern "C" void MonoType_get_UnderlyingSystemType_m10100 ();
extern "C" void MonoType_get_Assembly_m10101 ();
extern "C" void MonoType_get_AssemblyQualifiedName_m10102 ();
extern "C" void MonoType_getFullName_m10103 ();
extern "C" void MonoType_get_BaseType_m10104 ();
extern "C" void MonoType_get_FullName_m10105 ();
extern "C" void MonoType_IsDefined_m10106 ();
extern "C" void MonoType_GetCustomAttributes_m10107 ();
extern "C" void MonoType_GetCustomAttributes_m10108 ();
extern "C" void MonoType_get_MemberType_m10109 ();
extern "C" void MonoType_get_Name_m10110 ();
extern "C" void MonoType_get_Namespace_m10111 ();
extern "C" void MonoType_get_Module_m10112 ();
extern "C" void MonoType_get_DeclaringType_m10113 ();
extern "C" void MonoType_get_ReflectedType_m10114 ();
extern "C" void MonoType_get_TypeHandle_m10115 ();
extern "C" void MonoType_GetObjectData_m10116 ();
extern "C" void MonoType_ToString_m10117 ();
extern "C" void MonoType_GetGenericArguments_m10118 ();
extern "C" void MonoType_get_ContainsGenericParameters_m10119 ();
extern "C" void MonoType_get_IsGenericParameter_m10120 ();
extern "C" void MonoType_GetGenericTypeDefinition_m10121 ();
extern "C" void MonoType_CheckMethodSecurity_m10122 ();
extern "C" void MonoType_ReorderParamArrayArguments_m10123 ();
extern "C" void MulticastNotSupportedException__ctor_m10124 ();
extern "C" void MulticastNotSupportedException__ctor_m10125 ();
extern "C" void MulticastNotSupportedException__ctor_m10126 ();
extern "C" void NonSerializedAttribute__ctor_m10127 ();
extern "C" void NotImplementedException__ctor_m10128 ();
extern "C" void NotImplementedException__ctor_m3453 ();
extern "C" void NotImplementedException__ctor_m10129 ();
extern "C" void NotSupportedException__ctor_m208 ();
extern "C" void NotSupportedException__ctor_m4339 ();
extern "C" void NotSupportedException__ctor_m10130 ();
extern "C" void NullReferenceException__ctor_m10131 ();
extern "C" void NullReferenceException__ctor_m3229 ();
extern "C" void NullReferenceException__ctor_m10132 ();
extern "C" void CustomInfo__ctor_m10133 ();
extern "C" void CustomInfo_GetActiveSection_m10134 ();
extern "C" void CustomInfo_Parse_m10135 ();
extern "C" void CustomInfo_Format_m10136 ();
extern "C" void NumberFormatter__ctor_m10137 ();
extern "C" void NumberFormatter__cctor_m10138 ();
extern "C" void NumberFormatter_GetFormatterTables_m10139 ();
extern "C" void NumberFormatter_GetTenPowerOf_m10140 ();
extern "C" void NumberFormatter_InitDecHexDigits_m10141 ();
extern "C" void NumberFormatter_InitDecHexDigits_m10142 ();
extern "C" void NumberFormatter_InitDecHexDigits_m10143 ();
extern "C" void NumberFormatter_FastToDecHex_m10144 ();
extern "C" void NumberFormatter_ToDecHex_m10145 ();
extern "C" void NumberFormatter_FastDecHexLen_m10146 ();
extern "C" void NumberFormatter_DecHexLen_m10147 ();
extern "C" void NumberFormatter_DecHexLen_m10148 ();
extern "C" void NumberFormatter_ScaleOrder_m10149 ();
extern "C" void NumberFormatter_InitialFloatingPrecision_m10150 ();
extern "C" void NumberFormatter_ParsePrecision_m10151 ();
extern "C" void NumberFormatter_Init_m10152 ();
extern "C" void NumberFormatter_InitHex_m10153 ();
extern "C" void NumberFormatter_Init_m10154 ();
extern "C" void NumberFormatter_Init_m10155 ();
extern "C" void NumberFormatter_Init_m10156 ();
extern "C" void NumberFormatter_Init_m10157 ();
extern "C" void NumberFormatter_Init_m10158 ();
extern "C" void NumberFormatter_Init_m10159 ();
extern "C" void NumberFormatter_ResetCharBuf_m10160 ();
extern "C" void NumberFormatter_Resize_m10161 ();
extern "C" void NumberFormatter_Append_m10162 ();
extern "C" void NumberFormatter_Append_m10163 ();
extern "C" void NumberFormatter_Append_m10164 ();
extern "C" void NumberFormatter_GetNumberFormatInstance_m10165 ();
extern "C" void NumberFormatter_set_CurrentCulture_m10166 ();
extern "C" void NumberFormatter_get_IntegerDigits_m10167 ();
extern "C" void NumberFormatter_get_DecimalDigits_m10168 ();
extern "C" void NumberFormatter_get_IsFloatingSource_m10169 ();
extern "C" void NumberFormatter_get_IsZero_m10170 ();
extern "C" void NumberFormatter_get_IsZeroInteger_m10171 ();
extern "C" void NumberFormatter_RoundPos_m10172 ();
extern "C" void NumberFormatter_RoundDecimal_m10173 ();
extern "C" void NumberFormatter_RoundBits_m10174 ();
extern "C" void NumberFormatter_RemoveTrailingZeros_m10175 ();
extern "C" void NumberFormatter_AddOneToDecHex_m10176 ();
extern "C" void NumberFormatter_AddOneToDecHex_m10177 ();
extern "C" void NumberFormatter_CountTrailingZeros_m10178 ();
extern "C" void NumberFormatter_CountTrailingZeros_m10179 ();
extern "C" void NumberFormatter_GetInstance_m10180 ();
extern "C" void NumberFormatter_Release_m10181 ();
extern "C" void NumberFormatter_SetThreadCurrentCulture_m10182 ();
extern "C" void NumberFormatter_NumberToString_m10183 ();
extern "C" void NumberFormatter_NumberToString_m10184 ();
extern "C" void NumberFormatter_NumberToString_m10185 ();
extern "C" void NumberFormatter_NumberToString_m10186 ();
extern "C" void NumberFormatter_NumberToString_m10187 ();
extern "C" void NumberFormatter_NumberToString_m10188 ();
extern "C" void NumberFormatter_NumberToString_m10189 ();
extern "C" void NumberFormatter_NumberToString_m10190 ();
extern "C" void NumberFormatter_NumberToString_m10191 ();
extern "C" void NumberFormatter_NumberToString_m10192 ();
extern "C" void NumberFormatter_NumberToString_m10193 ();
extern "C" void NumberFormatter_NumberToString_m10194 ();
extern "C" void NumberFormatter_NumberToString_m10195 ();
extern "C" void NumberFormatter_NumberToString_m10196 ();
extern "C" void NumberFormatter_NumberToString_m10197 ();
extern "C" void NumberFormatter_NumberToString_m10198 ();
extern "C" void NumberFormatter_NumberToString_m10199 ();
extern "C" void NumberFormatter_FastIntegerToString_m10200 ();
extern "C" void NumberFormatter_IntegerToString_m10201 ();
extern "C" void NumberFormatter_NumberToString_m10202 ();
extern "C" void NumberFormatter_FormatCurrency_m10203 ();
extern "C" void NumberFormatter_FormatDecimal_m10204 ();
extern "C" void NumberFormatter_FormatHexadecimal_m10205 ();
extern "C" void NumberFormatter_FormatFixedPoint_m10206 ();
extern "C" void NumberFormatter_FormatRoundtrip_m10207 ();
extern "C" void NumberFormatter_FormatRoundtrip_m10208 ();
extern "C" void NumberFormatter_FormatGeneral_m10209 ();
extern "C" void NumberFormatter_FormatNumber_m10210 ();
extern "C" void NumberFormatter_FormatPercent_m10211 ();
extern "C" void NumberFormatter_FormatExponential_m10212 ();
extern "C" void NumberFormatter_FormatExponential_m10213 ();
extern "C" void NumberFormatter_FormatCustom_m10214 ();
extern "C" void NumberFormatter_ZeroTrimEnd_m10215 ();
extern "C" void NumberFormatter_IsZeroOnly_m10216 ();
extern "C" void NumberFormatter_AppendNonNegativeNumber_m10217 ();
extern "C" void NumberFormatter_AppendIntegerString_m10218 ();
extern "C" void NumberFormatter_AppendIntegerString_m10219 ();
extern "C" void NumberFormatter_AppendDecimalString_m10220 ();
extern "C" void NumberFormatter_AppendDecimalString_m10221 ();
extern "C" void NumberFormatter_AppendIntegerStringWithGroupSeparator_m10222 ();
extern "C" void NumberFormatter_AppendExponent_m10223 ();
extern "C" void NumberFormatter_AppendOneDigit_m10224 ();
extern "C" void NumberFormatter_FastAppendDigits_m10225 ();
extern "C" void NumberFormatter_AppendDigits_m10226 ();
extern "C" void NumberFormatter_AppendDigits_m10227 ();
extern "C" void NumberFormatter_Multiply10_m10228 ();
extern "C" void NumberFormatter_Divide10_m10229 ();
extern "C" void NumberFormatter_GetClone_m10230 ();
extern "C" void ObjectDisposedException__ctor_m3455 ();
extern "C" void ObjectDisposedException__ctor_m10231 ();
extern "C" void ObjectDisposedException__ctor_m10232 ();
extern "C" void ObjectDisposedException_get_Message_m10233 ();
extern "C" void ObjectDisposedException_GetObjectData_m10234 ();
extern "C" void OperatingSystem__ctor_m10235 ();
extern "C" void OperatingSystem_get_Platform_m10236 ();
extern "C" void OperatingSystem_GetObjectData_m10237 ();
extern "C" void OperatingSystem_ToString_m10238 ();
extern "C" void OutOfMemoryException__ctor_m10239 ();
extern "C" void OutOfMemoryException__ctor_m10240 ();
extern "C" void OverflowException__ctor_m10241 ();
extern "C" void OverflowException__ctor_m10242 ();
extern "C" void OverflowException__ctor_m10243 ();
extern "C" void Random__ctor_m10244 ();
extern "C" void Random__ctor_m3306 ();
extern "C" void RankException__ctor_m10245 ();
extern "C" void RankException__ctor_m10246 ();
extern "C" void RankException__ctor_m10247 ();
extern "C" void ResolveEventArgs__ctor_m10248 ();
extern "C" void RuntimeMethodHandle__ctor_m10249 ();
extern "C" void RuntimeMethodHandle__ctor_m10250 ();
extern "C" void RuntimeMethodHandle_get_Value_m10251 ();
extern "C" void RuntimeMethodHandle_GetObjectData_m10252 ();
extern "C" void RuntimeMethodHandle_Equals_m10253 ();
extern "C" void RuntimeMethodHandle_GetHashCode_m10254 ();
extern "C" void StringComparer__ctor_m10255 ();
extern "C" void StringComparer__cctor_m10256 ();
extern "C" void StringComparer_get_InvariantCultureIgnoreCase_m5373 ();
extern "C" void StringComparer_get_OrdinalIgnoreCase_m3254 ();
extern "C" void StringComparer_Compare_m10257 ();
extern "C" void StringComparer_Equals_m10258 ();
extern "C" void StringComparer_GetHashCode_m10259 ();
extern "C" void CultureAwareComparer__ctor_m10260 ();
extern "C" void CultureAwareComparer_Compare_m10261 ();
extern "C" void CultureAwareComparer_Equals_m10262 ();
extern "C" void CultureAwareComparer_GetHashCode_m10263 ();
extern "C" void OrdinalComparer__ctor_m10264 ();
extern "C" void OrdinalComparer_Compare_m10265 ();
extern "C" void OrdinalComparer_Equals_m10266 ();
extern "C" void OrdinalComparer_GetHashCode_m10267 ();
extern "C" void SystemException__ctor_m10268 ();
extern "C" void SystemException__ctor_m5401 ();
extern "C" void SystemException__ctor_m10269 ();
extern "C" void SystemException__ctor_m10270 ();
extern "C" void ThreadStaticAttribute__ctor_m10271 ();
extern "C" void TimeSpan__ctor_m10272 ();
extern "C" void TimeSpan__ctor_m10273 ();
extern "C" void TimeSpan__ctor_m10274 ();
extern "C" void TimeSpan__cctor_m10275 ();
extern "C" void TimeSpan_CalculateTicks_m10276 ();
extern "C" void TimeSpan_get_Days_m10277 ();
extern "C" void TimeSpan_get_Hours_m10278 ();
extern "C" void TimeSpan_get_Milliseconds_m10279 ();
extern "C" void TimeSpan_get_Minutes_m10280 ();
extern "C" void TimeSpan_get_Seconds_m10281 ();
extern "C" void TimeSpan_get_Ticks_m10282 ();
extern "C" void TimeSpan_get_TotalDays_m10283 ();
extern "C" void TimeSpan_get_TotalHours_m10284 ();
extern "C" void TimeSpan_get_TotalMilliseconds_m10285 ();
extern "C" void TimeSpan_get_TotalMinutes_m10286 ();
extern "C" void TimeSpan_get_TotalSeconds_m10287 ();
extern "C" void TimeSpan_Add_m10288 ();
extern "C" void TimeSpan_Compare_m10289 ();
extern "C" void TimeSpan_CompareTo_m10290 ();
extern "C" void TimeSpan_CompareTo_m10291 ();
extern "C" void TimeSpan_Equals_m10292 ();
extern "C" void TimeSpan_Duration_m10293 ();
extern "C" void TimeSpan_Equals_m10294 ();
extern "C" void TimeSpan_FromDays_m10295 ();
extern "C" void TimeSpan_FromHours_m10296 ();
extern "C" void TimeSpan_FromMinutes_m10297 ();
extern "C" void TimeSpan_FromSeconds_m10298 ();
extern "C" void TimeSpan_FromMilliseconds_m10299 ();
extern "C" void TimeSpan_From_m10300 ();
extern "C" void TimeSpan_GetHashCode_m10301 ();
extern "C" void TimeSpan_Negate_m10302 ();
extern "C" void TimeSpan_Subtract_m10303 ();
extern "C" void TimeSpan_ToString_m10304 ();
extern "C" void TimeSpan_op_Addition_m10305 ();
extern "C" void TimeSpan_op_Equality_m10306 ();
extern "C" void TimeSpan_op_GreaterThan_m10307 ();
extern "C" void TimeSpan_op_GreaterThanOrEqual_m10308 ();
extern "C" void TimeSpan_op_Inequality_m10309 ();
extern "C" void TimeSpan_op_LessThan_m10310 ();
extern "C" void TimeSpan_op_LessThanOrEqual_m10311 ();
extern "C" void TimeSpan_op_Subtraction_m10312 ();
extern "C" void TimeZone__ctor_m10313 ();
extern "C" void TimeZone__cctor_m10314 ();
extern "C" void TimeZone_get_CurrentTimeZone_m10315 ();
extern "C" void TimeZone_IsDaylightSavingTime_m10316 ();
extern "C" void TimeZone_IsDaylightSavingTime_m10317 ();
extern "C" void TimeZone_ToLocalTime_m10318 ();
extern "C" void TimeZone_ToUniversalTime_m10319 ();
extern "C" void TimeZone_GetLocalTimeDiff_m10320 ();
extern "C" void TimeZone_GetLocalTimeDiff_m10321 ();
extern "C" void CurrentSystemTimeZone__ctor_m10322 ();
extern "C" void CurrentSystemTimeZone__ctor_m10323 ();
extern "C" void CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m10324 ();
extern "C" void CurrentSystemTimeZone_GetTimeZoneData_m10325 ();
extern "C" void CurrentSystemTimeZone_GetDaylightChanges_m10326 ();
extern "C" void CurrentSystemTimeZone_GetUtcOffset_m10327 ();
extern "C" void CurrentSystemTimeZone_OnDeserialization_m10328 ();
extern "C" void CurrentSystemTimeZone_GetDaylightTimeFromData_m10329 ();
extern "C" void TypeInitializationException__ctor_m10330 ();
extern "C" void TypeInitializationException_GetObjectData_m10331 ();
extern "C" void TypeLoadException__ctor_m10332 ();
extern "C" void TypeLoadException__ctor_m10333 ();
extern "C" void TypeLoadException__ctor_m10334 ();
extern "C" void TypeLoadException_get_Message_m10335 ();
extern "C" void TypeLoadException_GetObjectData_m10336 ();
extern "C" void UnauthorizedAccessException__ctor_m10337 ();
extern "C" void UnauthorizedAccessException__ctor_m10338 ();
extern "C" void UnauthorizedAccessException__ctor_m10339 ();
extern "C" void UnhandledExceptionEventArgs__ctor_m10340 ();
extern "C" void UnhandledExceptionEventArgs_get_ExceptionObject_m3234 ();
extern "C" void UnhandledExceptionEventArgs_get_IsTerminating_m10341 ();
extern "C" void UnitySerializationHolder__ctor_m10342 ();
extern "C" void UnitySerializationHolder_GetTypeData_m10343 ();
extern "C" void UnitySerializationHolder_GetDBNullData_m10344 ();
extern "C" void UnitySerializationHolder_GetModuleData_m10345 ();
extern "C" void UnitySerializationHolder_GetObjectData_m10346 ();
extern "C" void UnitySerializationHolder_GetRealObject_m10347 ();
extern "C" void Version__ctor_m10348 ();
extern "C" void Version__ctor_m10349 ();
extern "C" void Version__ctor_m5364 ();
extern "C" void Version__ctor_m10350 ();
extern "C" void Version_CheckedSet_m10351 ();
extern "C" void Version_get_Build_m10352 ();
extern "C" void Version_get_Major_m10353 ();
extern "C" void Version_get_Minor_m10354 ();
extern "C" void Version_get_Revision_m10355 ();
extern "C" void Version_CompareTo_m10356 ();
extern "C" void Version_Equals_m10357 ();
extern "C" void Version_CompareTo_m10358 ();
extern "C" void Version_Equals_m10359 ();
extern "C" void Version_GetHashCode_m10360 ();
extern "C" void Version_ToString_m10361 ();
extern "C" void Version_CreateFromString_m10362 ();
extern "C" void Version_op_Equality_m10363 ();
extern "C" void Version_op_Inequality_m10364 ();
extern "C" void WeakReference__ctor_m10365 ();
extern "C" void WeakReference__ctor_m10366 ();
extern "C" void WeakReference__ctor_m10367 ();
extern "C" void WeakReference__ctor_m10368 ();
extern "C" void WeakReference_AllocateHandle_m10369 ();
extern "C" void WeakReference_get_Target_m10370 ();
extern "C" void WeakReference_get_TrackResurrection_m10371 ();
extern "C" void WeakReference_Finalize_m10372 ();
extern "C" void WeakReference_GetObjectData_m10373 ();
extern "C" void PrimalityTest__ctor_m10374 ();
extern "C" void PrimalityTest_Invoke_m10375 ();
extern "C" void PrimalityTest_BeginInvoke_m10376 ();
extern "C" void PrimalityTest_EndInvoke_m10377 ();
extern "C" void MemberFilter__ctor_m10378 ();
extern "C" void MemberFilter_Invoke_m10379 ();
extern "C" void MemberFilter_BeginInvoke_m10380 ();
extern "C" void MemberFilter_EndInvoke_m10381 ();
extern "C" void TypeFilter__ctor_m10382 ();
extern "C" void TypeFilter_Invoke_m10383 ();
extern "C" void TypeFilter_BeginInvoke_m10384 ();
extern "C" void TypeFilter_EndInvoke_m10385 ();
extern "C" void CrossContextDelegate__ctor_m10386 ();
extern "C" void CrossContextDelegate_Invoke_m10387 ();
extern "C" void CrossContextDelegate_BeginInvoke_m10388 ();
extern "C" void CrossContextDelegate_EndInvoke_m10389 ();
extern "C" void HeaderHandler__ctor_m10390 ();
extern "C" void HeaderHandler_Invoke_m10391 ();
extern "C" void HeaderHandler_BeginInvoke_m10392 ();
extern "C" void HeaderHandler_EndInvoke_m10393 ();
extern "C" void ThreadStart__ctor_m10394 ();
extern "C" void ThreadStart_Invoke_m10395 ();
extern "C" void ThreadStart_BeginInvoke_m10396 ();
extern "C" void ThreadStart_EndInvoke_m10397 ();
extern "C" void TimerCallback__ctor_m10398 ();
extern "C" void TimerCallback_Invoke_m10399 ();
extern "C" void TimerCallback_BeginInvoke_m10400 ();
extern "C" void TimerCallback_EndInvoke_m10401 ();
extern "C" void WaitCallback__ctor_m10402 ();
extern "C" void WaitCallback_Invoke_m10403 ();
extern "C" void WaitCallback_BeginInvoke_m10404 ();
extern "C" void WaitCallback_EndInvoke_m10405 ();
extern "C" void AppDomainInitializer__ctor_m10406 ();
extern "C" void AppDomainInitializer_Invoke_m10407 ();
extern "C" void AppDomainInitializer_BeginInvoke_m10408 ();
extern "C" void AppDomainInitializer_EndInvoke_m10409 ();
extern "C" void AssemblyLoadEventHandler__ctor_m10410 ();
extern "C" void AssemblyLoadEventHandler_Invoke_m10411 ();
extern "C" void AssemblyLoadEventHandler_BeginInvoke_m10412 ();
extern "C" void AssemblyLoadEventHandler_EndInvoke_m10413 ();
extern "C" void EventHandler__ctor_m10414 ();
extern "C" void EventHandler_Invoke_m10415 ();
extern "C" void EventHandler_BeginInvoke_m10416 ();
extern "C" void EventHandler_EndInvoke_m10417 ();
extern "C" void ResolveEventHandler__ctor_m10418 ();
extern "C" void ResolveEventHandler_Invoke_m10419 ();
extern "C" void ResolveEventHandler_BeginInvoke_m10420 ();
extern "C" void ResolveEventHandler_EndInvoke_m10421 ();
extern "C" void UnhandledExceptionEventHandler__ctor_m3232 ();
extern "C" void UnhandledExceptionEventHandler_Invoke_m10422 ();
extern "C" void UnhandledExceptionEventHandler_BeginInvoke_m10423 ();
extern "C" void UnhandledExceptionEventHandler_EndInvoke_m10424 ();
extern const methodPointerType g_MethodPointers[10142] = 
{
	BoardManager__ctor_m0,
	BoardManager_Start_m1,
	BoardManager_InitializeRedPath_m2,
	BoardManager_InitializeBluePath_m3,
	BoardManager_BoardSetup_m4,
	BoardManager_SetupScene_m5,
	U3CAutomaticTurnU3Ec__Iterator0__ctor_m6,
	U3CAutomaticTurnU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m7,
	U3CAutomaticTurnU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m8,
	U3CAutomaticTurnU3Ec__Iterator0_MoveNext_m9,
	U3CAutomaticTurnU3Ec__Iterator0_Dispose_m10,
	U3CAutomaticTurnU3Ec__Iterator0_Reset_m11,
	CountdownTimer__ctor_m12,
	CountdownTimer__cctor_m13,
	CountdownTimer_Start_m14,
	CountdownTimer_Update_m15,
	CountdownTimer_ShiftTimer_m16,
	CountdownTimer_AutomaticTurn_m17,
	U3CMoveTowardsTargetU3Ec__Iterator1__ctor_m18,
	U3CMoveTowardsTargetU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m19,
	U3CMoveTowardsTargetU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m20,
	U3CMoveTowardsTargetU3Ec__Iterator1_MoveNext_m21,
	U3CMoveTowardsTargetU3Ec__Iterator1_Dispose_m22,
	U3CMoveTowardsTargetU3Ec__Iterator1_Reset_m23,
	U3CGetActiveUsersU3Ec__Iterator2__ctor_m24,
	U3CGetActiveUsersU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m25,
	U3CGetActiveUsersU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m26,
	U3CGetActiveUsersU3Ec__Iterator2_MoveNext_m27,
	U3CGetActiveUsersU3Ec__Iterator2_Dispose_m28,
	U3CGetActiveUsersU3Ec__Iterator2_Reset_m29,
	DynamicScrollView__ctor_m30,
	DynamicScrollView_OnEnable_m31,
	DynamicScrollView_ClearOldElement_m32,
	DynamicScrollView_SetContentHeight_m33,
	DynamicScrollView_InitializeList_m34,
	DynamicScrollView_InitializeListForGames_m35,
	DynamicScrollView_InitializeNewItem_m36,
	DynamicScrollView_MoveTowardsTarget_m37,
	DynamicScrollView_GetActiveUsers_m38,
	DynamicScrollView_AddNewElement_m39,
	U3CResetBoardU3Ec__Iterator3__ctor_m40,
	U3CResetBoardU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m41,
	U3CResetBoardU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m42,
	U3CResetBoardU3Ec__Iterator3_MoveNext_m43,
	U3CResetBoardU3Ec__Iterator3_Dispose_m44,
	U3CResetBoardU3Ec__Iterator3_Reset_m45,
	U3CGetPlayer1TurnU3Ec__Iterator4__ctor_m46,
	U3CGetPlayer1TurnU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m47,
	U3CGetPlayer1TurnU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m48,
	U3CGetPlayer1TurnU3Ec__Iterator4_MoveNext_m49,
	U3CGetPlayer1TurnU3Ec__Iterator4_Dispose_m50,
	U3CGetPlayer1TurnU3Ec__Iterator4_Reset_m51,
	U3CGetPlayer2TurnU3Ec__Iterator5__ctor_m52,
	U3CGetPlayer2TurnU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m53,
	U3CGetPlayer2TurnU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m54,
	U3CGetPlayer2TurnU3Ec__Iterator5_MoveNext_m55,
	U3CGetPlayer2TurnU3Ec__Iterator5_Dispose_m56,
	U3CGetPlayer2TurnU3Ec__Iterator5_Reset_m57,
	U3CAutomaticMovePlayer1U3Ec__Iterator6__ctor_m58,
	U3CAutomaticMovePlayer1U3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m59,
	U3CAutomaticMovePlayer1U3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m60,
	U3CAutomaticMovePlayer1U3Ec__Iterator6_MoveNext_m61,
	U3CAutomaticMovePlayer1U3Ec__Iterator6_Dispose_m62,
	U3CAutomaticMovePlayer1U3Ec__Iterator6_Reset_m63,
	U3CAutomaticMovePlayer2U3Ec__Iterator7__ctor_m64,
	U3CAutomaticMovePlayer2U3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m65,
	U3CAutomaticMovePlayer2U3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m66,
	U3CAutomaticMovePlayer2U3Ec__Iterator7_MoveNext_m67,
	U3CAutomaticMovePlayer2U3Ec__Iterator7_Dispose_m68,
	U3CAutomaticMovePlayer2U3Ec__Iterator7_Reset_m69,
	GameManager__ctor_m70,
	GameManager__cctor_m71,
	GameManager_Awake_m72,
	GameManager_Start_m73,
	GameManager_InitGame_m74,
	GameManager_GameOver_m75,
	GameManager_GetNewPosition_m76,
	GameManager_RollDice_m77,
	GameManager_Move_Dice_m78,
	GameManager_Move_OpponentsDice_m79,
	GameManager_Stop_Dice_m80,
	GameManager_Stop_OpponentsDice_m81,
	GameManager_Update_m82,
	GameManager_ResetButtonPressed_m83,
	GameManager_ResetBoard_m84,
	GameManager_CallGetPlayerTurn_m85,
	GameManager_GetPlayer1Turn_m86,
	GameManager_GetPlayer2Turn_m87,
	GameManager_AutomaticMovePlayer1_m88,
	GameManager_AutomaticMovePlayer2_m89,
	U3CSetUserOnlineU3Ec__Iterator8__ctor_m90,
	U3CSetUserOnlineU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m91,
	U3CSetUserOnlineU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m92,
	U3CSetUserOnlineU3Ec__Iterator8_MoveNext_m93,
	U3CSetUserOnlineU3Ec__Iterator8_Dispose_m94,
	U3CSetUserOnlineU3Ec__Iterator8_Reset_m95,
	U3CGetActiveUsersU3Ec__Iterator9__ctor_m96,
	U3CGetActiveUsersU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m97,
	U3CGetActiveUsersU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m98,
	U3CGetActiveUsersU3Ec__Iterator9_MoveNext_m99,
	U3CGetActiveUsersU3Ec__Iterator9_Dispose_m100,
	U3CGetActiveUsersU3Ec__Iterator9_Reset_m101,
	GameMenu__ctor_m102,
	GameMenu_Start_m103,
	GameMenu_CallSetActiveUsers_m104,
	GameMenu_SetUserOnline_m105,
	GameMenu_GetActiveUsers_m106,
	GameMenu_Refresh_m107,
	Loader__ctor_m108,
	Loader_Awake_m109,
	U3CCreateUniquePlayerU3Ec__IteratorA__ctor_m110,
	U3CCreateUniquePlayerU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m111,
	U3CCreateUniquePlayerU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m112,
	U3CCreateUniquePlayerU3Ec__IteratorA_MoveNext_m113,
	U3CCreateUniquePlayerU3Ec__IteratorA_Dispose_m114,
	U3CCreateUniquePlayerU3Ec__IteratorA_Reset_m115,
	Menu__ctor_m116,
	Menu_Start_m117,
	Menu_CreateUser_m118,
	Menu_PlayGame_m119,
	Menu_CreateUniquePlayer_m120,
	U3CSmoothMovementU3Ec__IteratorB__ctor_m121,
	U3CSmoothMovementU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m122,
	U3CSmoothMovementU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m123,
	U3CSmoothMovementU3Ec__IteratorB_MoveNext_m124,
	U3CSmoothMovementU3Ec__IteratorB_Dispose_m125,
	U3CSmoothMovementU3Ec__IteratorB_Reset_m126,
	MovingObject__ctor_m127,
	MovingObject_Start_m128,
	MovingObject_Move_m129,
	MovingObject_SmoothMovement_m130,
	MovingObject_AttemptMove_m131,
	U3CSendPlayer1PositionU3Ec__IteratorC__ctor_m132,
	U3CSendPlayer1PositionU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m133,
	U3CSendPlayer1PositionU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m134,
	U3CSendPlayer1PositionU3Ec__IteratorC_MoveNext_m135,
	U3CSendPlayer1PositionU3Ec__IteratorC_Dispose_m136,
	U3CSendPlayer1PositionU3Ec__IteratorC_Reset_m137,
	U3CGetPlayer1PositionU3Ec__IteratorD__ctor_m138,
	U3CGetPlayer1PositionU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m139,
	U3CGetPlayer1PositionU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m140,
	U3CGetPlayer1PositionU3Ec__IteratorD_MoveNext_m141,
	U3CGetPlayer1PositionU3Ec__IteratorD_Dispose_m142,
	U3CGetPlayer1PositionU3Ec__IteratorD_Reset_m143,
	U3CExecuteAfterTimeU3Ec__IteratorE__ctor_m144,
	U3CExecuteAfterTimeU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m145,
	U3CExecuteAfterTimeU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m146,
	U3CExecuteAfterTimeU3Ec__IteratorE_MoveNext_m147,
	U3CExecuteAfterTimeU3Ec__IteratorE_Dispose_m148,
	U3CExecuteAfterTimeU3Ec__IteratorE_Reset_m149,
	Player__ctor_m150,
	Player_Start_m151,
	Player_MoveThePlayer_m152,
	Player_AttemptMove_m153,
	Player_SendPlayer1Position_m154,
	Player_ChangeTransform_m155,
	Player_GetPlayer1Position_m156,
	Player_CallExecuteAfterTime_m157,
	Player_ExecuteAfterTime_m158,
	U3CSendPlayer2PositionU3Ec__IteratorF__ctor_m159,
	U3CSendPlayer2PositionU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m160,
	U3CSendPlayer2PositionU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m161,
	U3CSendPlayer2PositionU3Ec__IteratorF_MoveNext_m162,
	U3CSendPlayer2PositionU3Ec__IteratorF_Dispose_m163,
	U3CSendPlayer2PositionU3Ec__IteratorF_Reset_m164,
	U3CGetPlayer2PositionU3Ec__Iterator10__ctor_m165,
	U3CGetPlayer2PositionU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m166,
	U3CGetPlayer2PositionU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m167,
	U3CGetPlayer2PositionU3Ec__Iterator10_MoveNext_m168,
	U3CGetPlayer2PositionU3Ec__Iterator10_Dispose_m169,
	U3CGetPlayer2PositionU3Ec__Iterator10_Reset_m170,
	PlayerTwo__ctor_m171,
	PlayerTwo_Start_m172,
	PlayerTwo_MoveThePlayer_m173,
	PlayerTwo_AttemptMove_m174,
	PlayerTwo_SendPlayer2Position_m175,
	PlayerTwo_ChangeTransform_m176,
	PlayerTwo_GetPlayer2Position_m177,
	U3CCreateGameU3Ec__Iterator11__ctor_m178,
	U3CCreateGameU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m179,
	U3CCreateGameU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m180,
	U3CCreateGameU3Ec__Iterator11_MoveNext_m181,
	U3CCreateGameU3Ec__Iterator11_Dispose_m182,
	U3CCreateGameU3Ec__Iterator11_Reset_m183,
	ScrollItem__ctor_m184,
	ScrollItem_OnEnable_m185,
	ScrollItem_CreateGame_m186,
	ScrollItem_OnRemoveMe_m187,
	Wall__ctor_m188,
	Wall_Awake_m189,
	Wall_DamageWall_m190,
	EventSystem__ctor_m277,
	EventSystem__cctor_m278,
	EventSystem_get_current_m279,
	EventSystem_set_current_m280,
	EventSystem_get_sendNavigationEvents_m281,
	EventSystem_set_sendNavigationEvents_m282,
	EventSystem_get_pixelDragThreshold_m283,
	EventSystem_set_pixelDragThreshold_m284,
	EventSystem_get_currentInputModule_m285,
	EventSystem_get_firstSelectedGameObject_m286,
	EventSystem_set_firstSelectedGameObject_m287,
	EventSystem_get_currentSelectedGameObject_m288,
	EventSystem_get_lastSelectedGameObject_m289,
	EventSystem_UpdateModules_m290,
	EventSystem_get_alreadySelecting_m291,
	EventSystem_SetSelectedGameObject_m292,
	EventSystem_get_baseEventDataCache_m293,
	EventSystem_SetSelectedGameObject_m294,
	EventSystem_RaycastComparer_m295,
	EventSystem_RaycastAll_m296,
	EventSystem_IsPointerOverGameObject_m297,
	EventSystem_IsPointerOverGameObject_m298,
	EventSystem_OnEnable_m299,
	EventSystem_OnDisable_m300,
	EventSystem_TickModules_m301,
	EventSystem_Update_m302,
	EventSystem_ChangeEventModule_m303,
	EventSystem_ToString_m304,
	TriggerEvent__ctor_m305,
	Entry__ctor_m306,
	EventTrigger__ctor_m307,
	EventTrigger_get_triggers_m308,
	EventTrigger_set_triggers_m309,
	EventTrigger_Execute_m310,
	EventTrigger_OnPointerEnter_m311,
	EventTrigger_OnPointerExit_m312,
	EventTrigger_OnDrag_m313,
	EventTrigger_OnDrop_m314,
	EventTrigger_OnPointerDown_m315,
	EventTrigger_OnPointerUp_m316,
	EventTrigger_OnPointerClick_m317,
	EventTrigger_OnSelect_m318,
	EventTrigger_OnDeselect_m319,
	EventTrigger_OnScroll_m320,
	EventTrigger_OnMove_m321,
	EventTrigger_OnUpdateSelected_m322,
	EventTrigger_OnInitializePotentialDrag_m323,
	EventTrigger_OnBeginDrag_m324,
	EventTrigger_OnEndDrag_m325,
	EventTrigger_OnSubmit_m326,
	EventTrigger_OnCancel_m327,
	ExecuteEvents__cctor_m328,
	ExecuteEvents_Execute_m329,
	ExecuteEvents_Execute_m330,
	ExecuteEvents_Execute_m331,
	ExecuteEvents_Execute_m332,
	ExecuteEvents_Execute_m333,
	ExecuteEvents_Execute_m334,
	ExecuteEvents_Execute_m335,
	ExecuteEvents_Execute_m336,
	ExecuteEvents_Execute_m337,
	ExecuteEvents_Execute_m338,
	ExecuteEvents_Execute_m339,
	ExecuteEvents_Execute_m340,
	ExecuteEvents_Execute_m341,
	ExecuteEvents_Execute_m342,
	ExecuteEvents_Execute_m343,
	ExecuteEvents_Execute_m344,
	ExecuteEvents_Execute_m345,
	ExecuteEvents_get_pointerEnterHandler_m346,
	ExecuteEvents_get_pointerExitHandler_m347,
	ExecuteEvents_get_pointerDownHandler_m348,
	ExecuteEvents_get_pointerUpHandler_m349,
	ExecuteEvents_get_pointerClickHandler_m350,
	ExecuteEvents_get_initializePotentialDrag_m351,
	ExecuteEvents_get_beginDragHandler_m352,
	ExecuteEvents_get_dragHandler_m353,
	ExecuteEvents_get_endDragHandler_m354,
	ExecuteEvents_get_dropHandler_m355,
	ExecuteEvents_get_scrollHandler_m356,
	ExecuteEvents_get_updateSelectedHandler_m357,
	ExecuteEvents_get_selectHandler_m358,
	ExecuteEvents_get_deselectHandler_m359,
	ExecuteEvents_get_moveHandler_m360,
	ExecuteEvents_get_submitHandler_m361,
	ExecuteEvents_get_cancelHandler_m362,
	ExecuteEvents_GetEventChain_m363,
	ExecuteEvents_U3Cs_HandlerListPoolU3Em__0_m364,
	RaycasterManager__cctor_m365,
	RaycasterManager_AddRaycaster_m366,
	RaycasterManager_GetRaycasters_m367,
	RaycasterManager_RemoveRaycasters_m368,
	RaycastResult_get_gameObject_m369,
	RaycastResult_set_gameObject_m370,
	RaycastResult_get_isValid_m371,
	RaycastResult_Clear_m372,
	RaycastResult_ToString_m373,
	UIBehaviour__ctor_m374,
	UIBehaviour_Awake_m375,
	UIBehaviour_OnEnable_m376,
	UIBehaviour_Start_m377,
	UIBehaviour_OnDisable_m378,
	UIBehaviour_OnDestroy_m379,
	UIBehaviour_IsActive_m380,
	UIBehaviour_OnRectTransformDimensionsChange_m381,
	UIBehaviour_OnBeforeTransformParentChanged_m382,
	UIBehaviour_OnTransformParentChanged_m383,
	UIBehaviour_OnDidApplyAnimationProperties_m384,
	UIBehaviour_OnCanvasGroupChanged_m385,
	UIBehaviour_OnCanvasHierarchyChanged_m386,
	UIBehaviour_IsDestroyed_m387,
	AxisEventData__ctor_m388,
	AxisEventData_get_moveVector_m389,
	AxisEventData_set_moveVector_m390,
	AxisEventData_get_moveDir_m391,
	AxisEventData_set_moveDir_m392,
	BaseEventData__ctor_m393,
	BaseEventData_Reset_m394,
	BaseEventData_Use_m395,
	BaseEventData_get_used_m396,
	BaseEventData_get_currentInputModule_m397,
	BaseEventData_get_selectedObject_m398,
	BaseEventData_set_selectedObject_m399,
	PointerEventData__ctor_m400,
	PointerEventData_get_pointerEnter_m401,
	PointerEventData_set_pointerEnter_m402,
	PointerEventData_get_lastPress_m403,
	PointerEventData_set_lastPress_m404,
	PointerEventData_get_rawPointerPress_m405,
	PointerEventData_set_rawPointerPress_m406,
	PointerEventData_get_pointerDrag_m407,
	PointerEventData_set_pointerDrag_m408,
	PointerEventData_get_pointerCurrentRaycast_m409,
	PointerEventData_set_pointerCurrentRaycast_m410,
	PointerEventData_get_pointerPressRaycast_m411,
	PointerEventData_set_pointerPressRaycast_m412,
	PointerEventData_get_eligibleForClick_m413,
	PointerEventData_set_eligibleForClick_m414,
	PointerEventData_get_pointerId_m415,
	PointerEventData_set_pointerId_m416,
	PointerEventData_get_position_m417,
	PointerEventData_set_position_m418,
	PointerEventData_get_delta_m419,
	PointerEventData_set_delta_m420,
	PointerEventData_get_pressPosition_m421,
	PointerEventData_set_pressPosition_m422,
	PointerEventData_get_worldPosition_m423,
	PointerEventData_set_worldPosition_m424,
	PointerEventData_get_worldNormal_m425,
	PointerEventData_set_worldNormal_m426,
	PointerEventData_get_clickTime_m427,
	PointerEventData_set_clickTime_m428,
	PointerEventData_get_clickCount_m429,
	PointerEventData_set_clickCount_m430,
	PointerEventData_get_scrollDelta_m431,
	PointerEventData_set_scrollDelta_m432,
	PointerEventData_get_useDragThreshold_m433,
	PointerEventData_set_useDragThreshold_m434,
	PointerEventData_get_dragging_m435,
	PointerEventData_set_dragging_m436,
	PointerEventData_get_button_m437,
	PointerEventData_set_button_m438,
	PointerEventData_IsPointerMoving_m439,
	PointerEventData_IsScrolling_m440,
	PointerEventData_get_enterEventCamera_m441,
	PointerEventData_get_pressEventCamera_m442,
	PointerEventData_get_pointerPress_m443,
	PointerEventData_set_pointerPress_m444,
	PointerEventData_ToString_m445,
	BaseInputModule__ctor_m446,
	BaseInputModule_get_eventSystem_m447,
	BaseInputModule_OnEnable_m448,
	BaseInputModule_OnDisable_m449,
	BaseInputModule_FindFirstRaycast_m450,
	BaseInputModule_DetermineMoveDirection_m451,
	BaseInputModule_DetermineMoveDirection_m452,
	BaseInputModule_FindCommonRoot_m453,
	BaseInputModule_HandlePointerExitAndEnter_m454,
	BaseInputModule_GetAxisEventData_m455,
	BaseInputModule_GetBaseEventData_m456,
	BaseInputModule_IsPointerOverGameObject_m457,
	BaseInputModule_ShouldActivateModule_m458,
	BaseInputModule_DeactivateModule_m459,
	BaseInputModule_ActivateModule_m460,
	BaseInputModule_UpdateModule_m461,
	BaseInputModule_IsModuleSupported_m462,
	ButtonState__ctor_m463,
	ButtonState_get_eventData_m464,
	ButtonState_set_eventData_m465,
	ButtonState_get_button_m466,
	ButtonState_set_button_m467,
	MouseState__ctor_m468,
	MouseState_AnyPressesThisFrame_m469,
	MouseState_AnyReleasesThisFrame_m470,
	MouseState_GetButtonState_m471,
	MouseState_SetButtonState_m472,
	MouseButtonEventData__ctor_m473,
	MouseButtonEventData_PressedThisFrame_m474,
	MouseButtonEventData_ReleasedThisFrame_m475,
	PointerInputModule__ctor_m476,
	PointerInputModule_GetPointerData_m477,
	PointerInputModule_RemovePointerData_m478,
	PointerInputModule_GetTouchPointerEventData_m479,
	PointerInputModule_CopyFromTo_m480,
	PointerInputModule_StateForMouseButton_m481,
	PointerInputModule_GetMousePointerEventData_m482,
	PointerInputModule_GetLastPointerEventData_m483,
	PointerInputModule_ShouldStartDrag_m484,
	PointerInputModule_ProcessMove_m485,
	PointerInputModule_ProcessDrag_m486,
	PointerInputModule_IsPointerOverGameObject_m487,
	PointerInputModule_ClearSelection_m488,
	PointerInputModule_ToString_m489,
	PointerInputModule_DeselectIfSelectionChanged_m490,
	StandaloneInputModule__ctor_m491,
	StandaloneInputModule_get_inputMode_m492,
	StandaloneInputModule_get_allowActivationOnMobileDevice_m493,
	StandaloneInputModule_set_allowActivationOnMobileDevice_m494,
	StandaloneInputModule_get_inputActionsPerSecond_m495,
	StandaloneInputModule_set_inputActionsPerSecond_m496,
	StandaloneInputModule_get_repeatDelay_m497,
	StandaloneInputModule_set_repeatDelay_m498,
	StandaloneInputModule_get_horizontalAxis_m499,
	StandaloneInputModule_set_horizontalAxis_m500,
	StandaloneInputModule_get_verticalAxis_m501,
	StandaloneInputModule_set_verticalAxis_m502,
	StandaloneInputModule_get_submitButton_m503,
	StandaloneInputModule_set_submitButton_m504,
	StandaloneInputModule_get_cancelButton_m505,
	StandaloneInputModule_set_cancelButton_m506,
	StandaloneInputModule_UpdateModule_m507,
	StandaloneInputModule_IsModuleSupported_m508,
	StandaloneInputModule_ShouldActivateModule_m509,
	StandaloneInputModule_ActivateModule_m510,
	StandaloneInputModule_DeactivateModule_m511,
	StandaloneInputModule_Process_m512,
	StandaloneInputModule_SendSubmitEventToSelectedObject_m513,
	StandaloneInputModule_GetRawMoveVector_m514,
	StandaloneInputModule_SendMoveEventToSelectedObject_m515,
	StandaloneInputModule_ProcessMouseEvent_m516,
	StandaloneInputModule_ProcessMouseEvent_m517,
	StandaloneInputModule_SendUpdateEventToSelectedObject_m518,
	StandaloneInputModule_ProcessMousePress_m519,
	TouchInputModule__ctor_m520,
	TouchInputModule_get_allowActivationOnStandalone_m521,
	TouchInputModule_set_allowActivationOnStandalone_m522,
	TouchInputModule_UpdateModule_m523,
	TouchInputModule_IsModuleSupported_m524,
	TouchInputModule_ShouldActivateModule_m525,
	TouchInputModule_UseFakeInput_m526,
	TouchInputModule_Process_m527,
	TouchInputModule_FakeTouches_m528,
	TouchInputModule_ProcessTouchEvents_m529,
	TouchInputModule_ProcessTouchPress_m530,
	TouchInputModule_DeactivateModule_m531,
	TouchInputModule_ToString_m532,
	BaseRaycaster__ctor_m533,
	BaseRaycaster_get_priority_m534,
	BaseRaycaster_get_sortOrderPriority_m535,
	BaseRaycaster_get_renderOrderPriority_m536,
	BaseRaycaster_OnEnable_m537,
	BaseRaycaster_OnDisable_m538,
	Physics2DRaycaster__ctor_m539,
	Physics2DRaycaster_Raycast_m540,
	PhysicsRaycaster__ctor_m541,
	PhysicsRaycaster_get_eventCamera_m542,
	PhysicsRaycaster_get_depth_m543,
	PhysicsRaycaster_get_finalEventMask_m544,
	PhysicsRaycaster_get_eventMask_m545,
	PhysicsRaycaster_set_eventMask_m546,
	PhysicsRaycaster_Raycast_m547,
	PhysicsRaycaster_U3CRaycastU3Em__1_m548,
	ColorTweenCallback__ctor_m549,
	ColorTween_get_startColor_m550,
	ColorTween_set_startColor_m551,
	ColorTween_get_targetColor_m552,
	ColorTween_set_targetColor_m553,
	ColorTween_get_tweenMode_m554,
	ColorTween_set_tweenMode_m555,
	ColorTween_get_duration_m556,
	ColorTween_set_duration_m557,
	ColorTween_get_ignoreTimeScale_m558,
	ColorTween_set_ignoreTimeScale_m559,
	ColorTween_TweenValue_m560,
	ColorTween_AddOnChangedCallback_m561,
	ColorTween_GetIgnoreTimescale_m562,
	ColorTween_GetDuration_m563,
	ColorTween_ValidTarget_m564,
	AnimationTriggers__ctor_m565,
	AnimationTriggers_get_normalTrigger_m566,
	AnimationTriggers_set_normalTrigger_m567,
	AnimationTriggers_get_highlightedTrigger_m568,
	AnimationTriggers_set_highlightedTrigger_m569,
	AnimationTriggers_get_pressedTrigger_m570,
	AnimationTriggers_set_pressedTrigger_m571,
	AnimationTriggers_get_disabledTrigger_m572,
	AnimationTriggers_set_disabledTrigger_m573,
	ButtonClickedEvent__ctor_m574,
	U3COnFinishSubmitU3Ec__Iterator1__ctor_m575,
	U3COnFinishSubmitU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m576,
	U3COnFinishSubmitU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m577,
	U3COnFinishSubmitU3Ec__Iterator1_MoveNext_m578,
	U3COnFinishSubmitU3Ec__Iterator1_Dispose_m579,
	U3COnFinishSubmitU3Ec__Iterator1_Reset_m580,
	Button__ctor_m581,
	Button_get_onClick_m582,
	Button_set_onClick_m583,
	Button_Press_m584,
	Button_OnPointerClick_m585,
	Button_OnSubmit_m586,
	Button_OnFinishSubmit_m587,
	CanvasUpdateRegistry__ctor_m588,
	CanvasUpdateRegistry__cctor_m589,
	CanvasUpdateRegistry_get_instance_m590,
	CanvasUpdateRegistry_ObjectValidForUpdate_m591,
	CanvasUpdateRegistry_PerformUpdate_m592,
	CanvasUpdateRegistry_ParentCount_m593,
	CanvasUpdateRegistry_SortLayoutList_m594,
	CanvasUpdateRegistry_RegisterCanvasElementForLayoutRebuild_m595,
	CanvasUpdateRegistry_InternalRegisterCanvasElementForLayoutRebuild_m596,
	CanvasUpdateRegistry_RegisterCanvasElementForGraphicRebuild_m597,
	CanvasUpdateRegistry_InternalRegisterCanvasElementForGraphicRebuild_m598,
	CanvasUpdateRegistry_UnRegisterCanvasElementForRebuild_m599,
	CanvasUpdateRegistry_InternalUnRegisterCanvasElementForLayoutRebuild_m600,
	CanvasUpdateRegistry_InternalUnRegisterCanvasElementForGraphicRebuild_m601,
	CanvasUpdateRegistry_IsRebuildingLayout_m602,
	CanvasUpdateRegistry_IsRebuildingGraphics_m603,
	CanvasUpdateRegistry_U3CPerformUpdateU3Em__2_m604,
	CanvasUpdateRegistry_U3CPerformUpdateU3Em__3_m605,
	ColorBlock_get_normalColor_m606,
	ColorBlock_set_normalColor_m607,
	ColorBlock_get_highlightedColor_m608,
	ColorBlock_set_highlightedColor_m609,
	ColorBlock_get_pressedColor_m610,
	ColorBlock_set_pressedColor_m611,
	ColorBlock_get_disabledColor_m612,
	ColorBlock_set_disabledColor_m613,
	ColorBlock_get_colorMultiplier_m614,
	ColorBlock_set_colorMultiplier_m615,
	ColorBlock_get_fadeDuration_m616,
	ColorBlock_set_fadeDuration_m617,
	ColorBlock_get_defaultColorBlock_m618,
	FontData__ctor_m619,
	FontData_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m620,
	FontData_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m621,
	FontData_get_defaultFontData_m622,
	FontData_get_font_m623,
	FontData_set_font_m624,
	FontData_get_fontSize_m625,
	FontData_set_fontSize_m626,
	FontData_get_fontStyle_m627,
	FontData_set_fontStyle_m628,
	FontData_get_bestFit_m629,
	FontData_set_bestFit_m630,
	FontData_get_minSize_m631,
	FontData_set_minSize_m632,
	FontData_get_maxSize_m633,
	FontData_set_maxSize_m634,
	FontData_get_alignment_m635,
	FontData_set_alignment_m636,
	FontData_get_richText_m637,
	FontData_set_richText_m638,
	FontData_get_horizontalOverflow_m639,
	FontData_set_horizontalOverflow_m640,
	FontData_get_verticalOverflow_m641,
	FontData_set_verticalOverflow_m642,
	FontData_get_lineSpacing_m643,
	FontData_set_lineSpacing_m644,
	FontUpdateTracker__cctor_m645,
	FontUpdateTracker_TrackText_m646,
	FontUpdateTracker_RebuildForFont_m647,
	FontUpdateTracker_UntrackText_m648,
	Graphic__ctor_m649,
	Graphic__cctor_m650,
	Graphic_get_defaultGraphicMaterial_m651,
	Graphic_get_color_m652,
	Graphic_set_color_m653,
	Graphic_SetAllDirty_m654,
	Graphic_SetLayoutDirty_m655,
	Graphic_SetVerticesDirty_m656,
	Graphic_SetMaterialDirty_m657,
	Graphic_OnRectTransformDimensionsChange_m658,
	Graphic_OnBeforeTransformParentChanged_m659,
	Graphic_OnTransformParentChanged_m660,
	Graphic_get_depth_m661,
	Graphic_get_rectTransform_m662,
	Graphic_get_canvas_m663,
	Graphic_CacheCanvas_m664,
	Graphic_get_canvasRenderer_m665,
	Graphic_get_defaultMaterial_m666,
	Graphic_get_material_m667,
	Graphic_set_material_m668,
	Graphic_get_materialForRendering_m669,
	Graphic_get_mainTexture_m670,
	Graphic_OnEnable_m671,
	Graphic_OnDisable_m672,
	Graphic_SendGraphicEnabledDisabled_m673,
	Graphic_OnCanvasHierarchyChanged_m674,
	Graphic_Rebuild_m675,
	Graphic_UpdateGeometry_m676,
	Graphic_UpdateMaterial_m677,
	Graphic_OnFillVBO_m678,
	Graphic_OnDidApplyAnimationProperties_m679,
	Graphic_SetNativeSize_m680,
	Graphic_Raycast_m681,
	Graphic_PixelAdjustPoint_m682,
	Graphic_GetPixelAdjustedRect_m683,
	Graphic_CrossFadeColor_m684,
	Graphic_CrossFadeColor_m685,
	Graphic_CreateColorFromAlpha_m686,
	Graphic_CrossFadeAlpha_m687,
	Graphic_RegisterDirtyLayoutCallback_m688,
	Graphic_UnregisterDirtyLayoutCallback_m689,
	Graphic_RegisterDirtyVerticesCallback_m690,
	Graphic_UnregisterDirtyVerticesCallback_m691,
	Graphic_RegisterDirtyMaterialCallback_m692,
	Graphic_UnregisterDirtyMaterialCallback_m693,
	Graphic_U3Cs_VboPoolU3Em__4_m694,
	Graphic_U3Cs_VboPoolU3Em__5_m695,
	Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m696,
	Graphic_UnityEngine_UI_ICanvasElement_get_transform_m697,
	GraphicRaycaster__ctor_m698,
	GraphicRaycaster__cctor_m699,
	GraphicRaycaster_get_sortOrderPriority_m700,
	GraphicRaycaster_get_renderOrderPriority_m701,
	GraphicRaycaster_get_ignoreReversedGraphics_m702,
	GraphicRaycaster_set_ignoreReversedGraphics_m703,
	GraphicRaycaster_get_blockingObjects_m704,
	GraphicRaycaster_set_blockingObjects_m705,
	GraphicRaycaster_get_canvas_m706,
	GraphicRaycaster_Raycast_m707,
	GraphicRaycaster_get_eventCamera_m708,
	GraphicRaycaster_Raycast_m709,
	GraphicRaycaster_U3CRaycastU3Em__6_m710,
	GraphicRegistry__ctor_m711,
	GraphicRegistry__cctor_m712,
	GraphicRegistry_get_instance_m713,
	GraphicRegistry_RegisterGraphicForCanvas_m714,
	GraphicRegistry_UnregisterGraphicForCanvas_m715,
	GraphicRegistry_GetGraphicsForCanvas_m716,
	Image__ctor_m717,
	Image__cctor_m718,
	Image_get_sprite_m719,
	Image_set_sprite_m720,
	Image_get_overrideSprite_m721,
	Image_set_overrideSprite_m722,
	Image_get_type_m723,
	Image_set_type_m724,
	Image_get_preserveAspect_m725,
	Image_set_preserveAspect_m726,
	Image_get_fillCenter_m727,
	Image_set_fillCenter_m728,
	Image_get_fillMethod_m729,
	Image_set_fillMethod_m730,
	Image_get_fillAmount_m731,
	Image_set_fillAmount_m732,
	Image_get_fillClockwise_m733,
	Image_set_fillClockwise_m734,
	Image_get_fillOrigin_m735,
	Image_set_fillOrigin_m736,
	Image_get_eventAlphaThreshold_m737,
	Image_set_eventAlphaThreshold_m738,
	Image_get_mainTexture_m739,
	Image_get_hasBorder_m740,
	Image_get_pixelsPerUnit_m741,
	Image_OnBeforeSerialize_m742,
	Image_OnAfterDeserialize_m743,
	Image_GetDrawingDimensions_m744,
	Image_SetNativeSize_m745,
	Image_OnFillVBO_m746,
	Image_GenerateSimpleSprite_m747,
	Image_GenerateSlicedSprite_m748,
	Image_GenerateTiledSprite_m749,
	Image_AddQuad_m750,
	Image_GetAdjustedBorders_m751,
	Image_GenerateFilledSprite_m752,
	Image_RadialCut_m753,
	Image_RadialCut_m754,
	Image_CalculateLayoutInputHorizontal_m755,
	Image_CalculateLayoutInputVertical_m756,
	Image_get_minWidth_m757,
	Image_get_preferredWidth_m758,
	Image_get_flexibleWidth_m759,
	Image_get_minHeight_m760,
	Image_get_preferredHeight_m761,
	Image_get_flexibleHeight_m762,
	Image_get_layoutPriority_m763,
	Image_IsRaycastLocationValid_m764,
	Image_MapCoordinate_m765,
	SubmitEvent__ctor_m766,
	OnChangeEvent__ctor_m767,
	OnValidateInput__ctor_m768,
	OnValidateInput_Invoke_m769,
	OnValidateInput_BeginInvoke_m770,
	OnValidateInput_EndInvoke_m771,
	U3CCaretBlinkU3Ec__Iterator2__ctor_m772,
	U3CCaretBlinkU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m773,
	U3CCaretBlinkU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m774,
	U3CCaretBlinkU3Ec__Iterator2_MoveNext_m775,
	U3CCaretBlinkU3Ec__Iterator2_Dispose_m776,
	U3CCaretBlinkU3Ec__Iterator2_Reset_m777,
	U3CMouseDragOutsideRectU3Ec__Iterator3__ctor_m778,
	U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m779,
	U3CMouseDragOutsideRectU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m780,
	U3CMouseDragOutsideRectU3Ec__Iterator3_MoveNext_m781,
	U3CMouseDragOutsideRectU3Ec__Iterator3_Dispose_m782,
	U3CMouseDragOutsideRectU3Ec__Iterator3_Reset_m783,
	InputField__ctor_m784,
	InputField__cctor_m785,
	InputField_get_cachedInputTextGenerator_m786,
	InputField_set_shouldHideMobileInput_m787,
	InputField_get_shouldHideMobileInput_m788,
	InputField_get_text_m255,
	InputField_set_text_m789,
	InputField_get_isFocused_m790,
	InputField_get_caretBlinkRate_m791,
	InputField_set_caretBlinkRate_m792,
	InputField_get_textComponent_m793,
	InputField_set_textComponent_m794,
	InputField_get_placeholder_m795,
	InputField_set_placeholder_m796,
	InputField_get_selectionColor_m797,
	InputField_set_selectionColor_m798,
	InputField_get_onEndEdit_m799,
	InputField_set_onEndEdit_m800,
	InputField_get_onValueChange_m801,
	InputField_set_onValueChange_m802,
	InputField_get_onValidateInput_m803,
	InputField_set_onValidateInput_m804,
	InputField_get_characterLimit_m805,
	InputField_set_characterLimit_m806,
	InputField_get_contentType_m807,
	InputField_set_contentType_m808,
	InputField_get_lineType_m809,
	InputField_set_lineType_m810,
	InputField_get_inputType_m811,
	InputField_set_inputType_m812,
	InputField_get_keyboardType_m813,
	InputField_set_keyboardType_m814,
	InputField_get_characterValidation_m815,
	InputField_set_characterValidation_m816,
	InputField_get_multiLine_m817,
	InputField_get_asteriskChar_m818,
	InputField_set_asteriskChar_m819,
	InputField_get_wasCanceled_m820,
	InputField_ClampPos_m821,
	InputField_get_caretPositionInternal_m822,
	InputField_set_caretPositionInternal_m823,
	InputField_get_caretSelectPositionInternal_m824,
	InputField_set_caretSelectPositionInternal_m825,
	InputField_get_hasSelection_m826,
	InputField_get_caretPosition_m827,
	InputField_set_caretPosition_m828,
	InputField_get_selectionAnchorPosition_m829,
	InputField_set_selectionAnchorPosition_m830,
	InputField_get_selectionFocusPosition_m831,
	InputField_set_selectionFocusPosition_m832,
	InputField_OnEnable_m833,
	InputField_OnDisable_m834,
	InputField_CaretBlink_m835,
	InputField_SetCaretVisible_m836,
	InputField_SetCaretActive_m837,
	InputField_OnFocus_m838,
	InputField_SelectAll_m839,
	InputField_MoveTextEnd_m840,
	InputField_MoveTextStart_m841,
	InputField_get_clipboard_m842,
	InputField_set_clipboard_m843,
	InputField_InPlaceEditing_m844,
	InputField_LateUpdate_m845,
	InputField_ScreenToLocal_m846,
	InputField_GetUnclampedCharacterLineFromPosition_m847,
	InputField_GetCharacterIndexFromPosition_m848,
	InputField_MayDrag_m849,
	InputField_OnBeginDrag_m850,
	InputField_OnDrag_m851,
	InputField_MouseDragOutsideRect_m852,
	InputField_OnEndDrag_m853,
	InputField_OnPointerDown_m854,
	InputField_KeyPressed_m855,
	InputField_IsValidChar_m856,
	InputField_ProcessEvent_m857,
	InputField_OnUpdateSelected_m858,
	InputField_GetSelectedString_m859,
	InputField_FindtNextWordBegin_m860,
	InputField_MoveRight_m861,
	InputField_FindtPrevWordBegin_m862,
	InputField_MoveLeft_m863,
	InputField_DetermineCharacterLine_m864,
	InputField_LineUpCharacterPosition_m865,
	InputField_LineDownCharacterPosition_m866,
	InputField_MoveDown_m867,
	InputField_MoveDown_m868,
	InputField_MoveUp_m869,
	InputField_MoveUp_m870,
	InputField_Delete_m871,
	InputField_ForwardSpace_m872,
	InputField_Backspace_m873,
	InputField_Insert_m874,
	InputField_SendOnValueChangedAndUpdateLabel_m875,
	InputField_SendOnValueChanged_m876,
	InputField_SendOnSubmit_m877,
	InputField_Append_m878,
	InputField_Append_m879,
	InputField_UpdateLabel_m880,
	InputField_IsSelectionVisible_m881,
	InputField_GetLineStartPosition_m882,
	InputField_GetLineEndPosition_m883,
	InputField_SetDrawRangeToContainCaretPosition_m884,
	InputField_MarkGeometryAsDirty_m885,
	InputField_Rebuild_m886,
	InputField_UpdateGeometry_m887,
	InputField_AssignPositioningIfNeeded_m888,
	InputField_OnFillVBO_m889,
	InputField_GenerateCursor_m890,
	InputField_CreateCursorVerts_m891,
	InputField_SumLineHeights_m892,
	InputField_GenerateHightlight_m893,
	InputField_Validate_m894,
	InputField_ActivateInputField_m895,
	InputField_ActivateInputFieldInternal_m896,
	InputField_OnSelect_m897,
	InputField_OnPointerClick_m898,
	InputField_DeactivateInputField_m899,
	InputField_OnDeselect_m900,
	InputField_OnSubmit_m901,
	InputField_EnforceContentType_m902,
	InputField_SetToCustomIfContentTypeIsNot_m903,
	InputField_SetToCustom_m904,
	InputField_DoStateTransition_m905,
	InputField_UnityEngine_UI_ICanvasElement_IsDestroyed_m906,
	InputField_UnityEngine_UI_ICanvasElement_get_transform_m907,
	MaskableGraphic__ctor_m908,
	MaskableGraphic_get_maskable_m909,
	MaskableGraphic_set_maskable_m910,
	MaskableGraphic_get_material_m911,
	MaskableGraphic_set_material_m912,
	MaskableGraphic_UpdateInternalState_m913,
	MaskableGraphic_OnEnable_m914,
	MaskableGraphic_OnDisable_m915,
	MaskableGraphic_OnTransformParentChanged_m916,
	MaskableGraphic_ParentMaskStateChanged_m917,
	MaskableGraphic_ClearMaskMaterial_m918,
	MaskableGraphic_SetMaterialDirty_m919,
	MaskableGraphic_GetStencilForGraphic_m920,
	Misc_Destroy_m921,
	Misc_DestroyImmediate_m922,
	Navigation_get_mode_m923,
	Navigation_set_mode_m924,
	Navigation_get_selectOnUp_m925,
	Navigation_set_selectOnUp_m926,
	Navigation_get_selectOnDown_m927,
	Navigation_set_selectOnDown_m928,
	Navigation_get_selectOnLeft_m929,
	Navigation_set_selectOnLeft_m930,
	Navigation_get_selectOnRight_m931,
	Navigation_set_selectOnRight_m932,
	Navigation_get_defaultNavigation_m933,
	RawImage__ctor_m934,
	RawImage_get_mainTexture_m935,
	RawImage_get_texture_m936,
	RawImage_set_texture_m937,
	RawImage_get_uvRect_m938,
	RawImage_set_uvRect_m939,
	RawImage_SetNativeSize_m940,
	RawImage_OnFillVBO_m941,
	ScrollEvent__ctor_m942,
	U3CClickRepeatU3Ec__Iterator4__ctor_m943,
	U3CClickRepeatU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m944,
	U3CClickRepeatU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m945,
	U3CClickRepeatU3Ec__Iterator4_MoveNext_m946,
	U3CClickRepeatU3Ec__Iterator4_Dispose_m947,
	U3CClickRepeatU3Ec__Iterator4_Reset_m948,
	Scrollbar__ctor_m949,
	Scrollbar_get_handleRect_m950,
	Scrollbar_set_handleRect_m951,
	Scrollbar_get_direction_m952,
	Scrollbar_set_direction_m953,
	Scrollbar_get_value_m954,
	Scrollbar_set_value_m955,
	Scrollbar_get_size_m956,
	Scrollbar_set_size_m957,
	Scrollbar_get_numberOfSteps_m958,
	Scrollbar_set_numberOfSteps_m959,
	Scrollbar_get_onValueChanged_m960,
	Scrollbar_set_onValueChanged_m961,
	Scrollbar_get_stepSize_m962,
	Scrollbar_Rebuild_m963,
	Scrollbar_OnEnable_m964,
	Scrollbar_OnDisable_m965,
	Scrollbar_UpdateCachedReferences_m966,
	Scrollbar_Set_m967,
	Scrollbar_Set_m968,
	Scrollbar_OnRectTransformDimensionsChange_m969,
	Scrollbar_get_axis_m970,
	Scrollbar_get_reverseValue_m971,
	Scrollbar_UpdateVisuals_m972,
	Scrollbar_UpdateDrag_m973,
	Scrollbar_MayDrag_m974,
	Scrollbar_OnBeginDrag_m975,
	Scrollbar_OnDrag_m976,
	Scrollbar_OnPointerDown_m977,
	Scrollbar_ClickRepeat_m978,
	Scrollbar_OnPointerUp_m979,
	Scrollbar_OnMove_m980,
	Scrollbar_FindSelectableOnLeft_m981,
	Scrollbar_FindSelectableOnRight_m982,
	Scrollbar_FindSelectableOnUp_m983,
	Scrollbar_FindSelectableOnDown_m984,
	Scrollbar_OnInitializePotentialDrag_m985,
	Scrollbar_SetDirection_m986,
	Scrollbar_UnityEngine_UI_ICanvasElement_IsDestroyed_m987,
	Scrollbar_UnityEngine_UI_ICanvasElement_get_transform_m988,
	ScrollRectEvent__ctor_m989,
	ScrollRect__ctor_m990,
	ScrollRect_get_content_m991,
	ScrollRect_set_content_m992,
	ScrollRect_get_horizontal_m993,
	ScrollRect_set_horizontal_m994,
	ScrollRect_get_vertical_m995,
	ScrollRect_set_vertical_m996,
	ScrollRect_get_movementType_m997,
	ScrollRect_set_movementType_m998,
	ScrollRect_get_elasticity_m999,
	ScrollRect_set_elasticity_m1000,
	ScrollRect_get_inertia_m1001,
	ScrollRect_set_inertia_m1002,
	ScrollRect_get_decelerationRate_m1003,
	ScrollRect_set_decelerationRate_m1004,
	ScrollRect_get_scrollSensitivity_m1005,
	ScrollRect_set_scrollSensitivity_m1006,
	ScrollRect_get_horizontalScrollbar_m1007,
	ScrollRect_set_horizontalScrollbar_m1008,
	ScrollRect_get_verticalScrollbar_m1009,
	ScrollRect_set_verticalScrollbar_m1010,
	ScrollRect_get_onValueChanged_m1011,
	ScrollRect_set_onValueChanged_m1012,
	ScrollRect_get_viewRect_m1013,
	ScrollRect_get_velocity_m1014,
	ScrollRect_set_velocity_m1015,
	ScrollRect_Rebuild_m1016,
	ScrollRect_OnEnable_m1017,
	ScrollRect_OnDisable_m1018,
	ScrollRect_IsActive_m1019,
	ScrollRect_EnsureLayoutHasRebuilt_m1020,
	ScrollRect_StopMovement_m1021,
	ScrollRect_OnScroll_m1022,
	ScrollRect_OnInitializePotentialDrag_m1023,
	ScrollRect_OnBeginDrag_m1024,
	ScrollRect_OnEndDrag_m1025,
	ScrollRect_OnDrag_m1026,
	ScrollRect_SetContentAnchoredPosition_m1027,
	ScrollRect_LateUpdate_m1028,
	ScrollRect_UpdatePrevData_m1029,
	ScrollRect_UpdateScrollbars_m1030,
	ScrollRect_get_normalizedPosition_m1031,
	ScrollRect_set_normalizedPosition_m1032,
	ScrollRect_get_horizontalNormalizedPosition_m1033,
	ScrollRect_set_horizontalNormalizedPosition_m1034,
	ScrollRect_get_verticalNormalizedPosition_m240,
	ScrollRect_set_verticalNormalizedPosition_m216,
	ScrollRect_SetHorizontalNormalizedPosition_m1035,
	ScrollRect_SetVerticalNormalizedPosition_m1036,
	ScrollRect_SetNormalizedPosition_m1037,
	ScrollRect_RubberDelta_m1038,
	ScrollRect_UpdateBounds_m1039,
	ScrollRect_GetBounds_m1040,
	ScrollRect_CalculateOffset_m1041,
	ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1042,
	ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1043,
	Selectable__ctor_m1044,
	Selectable__cctor_m1045,
	Selectable_get_allSelectables_m1046,
	Selectable_get_navigation_m1047,
	Selectable_set_navigation_m1048,
	Selectable_get_transition_m1049,
	Selectable_set_transition_m1050,
	Selectable_get_colors_m1051,
	Selectable_set_colors_m1052,
	Selectable_get_spriteState_m1053,
	Selectable_set_spriteState_m1054,
	Selectable_get_animationTriggers_m1055,
	Selectable_set_animationTriggers_m1056,
	Selectable_get_targetGraphic_m1057,
	Selectable_set_targetGraphic_m1058,
	Selectable_get_interactable_m1059,
	Selectable_set_interactable_m1060,
	Selectable_get_isPointerInside_m1061,
	Selectable_set_isPointerInside_m1062,
	Selectable_get_isPointerDown_m1063,
	Selectable_set_isPointerDown_m1064,
	Selectable_get_hasSelection_m1065,
	Selectable_set_hasSelection_m1066,
	Selectable_get_image_m1067,
	Selectable_set_image_m1068,
	Selectable_get_animator_m1069,
	Selectable_Awake_m1070,
	Selectable_OnCanvasGroupChanged_m1071,
	Selectable_IsInteractable_m1072,
	Selectable_OnDidApplyAnimationProperties_m1073,
	Selectable_OnEnable_m1074,
	Selectable_OnSetProperty_m1075,
	Selectable_OnDisable_m1076,
	Selectable_get_currentSelectionState_m1077,
	Selectable_InstantClearState_m1078,
	Selectable_DoStateTransition_m1079,
	Selectable_FindSelectable_m1080,
	Selectable_GetPointOnRectEdge_m1081,
	Selectable_Navigate_m1082,
	Selectable_FindSelectableOnLeft_m1083,
	Selectable_FindSelectableOnRight_m1084,
	Selectable_FindSelectableOnUp_m1085,
	Selectable_FindSelectableOnDown_m1086,
	Selectable_OnMove_m1087,
	Selectable_StartColorTween_m1088,
	Selectable_DoSpriteSwap_m1089,
	Selectable_TriggerAnimation_m1090,
	Selectable_IsHighlighted_m1091,
	Selectable_IsPressed_m1092,
	Selectable_IsPressed_m1093,
	Selectable_UpdateSelectionState_m1094,
	Selectable_EvaluateAndTransitionToSelectionState_m1095,
	Selectable_InternalEvaluateAndTransitionToSelectionState_m1096,
	Selectable_OnPointerDown_m1097,
	Selectable_OnPointerUp_m1098,
	Selectable_OnPointerEnter_m1099,
	Selectable_OnPointerExit_m1100,
	Selectable_OnSelect_m1101,
	Selectable_OnDeselect_m1102,
	Selectable_Select_m1103,
	SetPropertyUtility_SetColor_m1104,
	SliderEvent__ctor_m1105,
	Slider__ctor_m1106,
	Slider_get_fillRect_m1107,
	Slider_set_fillRect_m1108,
	Slider_get_handleRect_m1109,
	Slider_set_handleRect_m1110,
	Slider_get_direction_m1111,
	Slider_set_direction_m1112,
	Slider_get_minValue_m1113,
	Slider_set_minValue_m1114,
	Slider_get_maxValue_m1115,
	Slider_set_maxValue_m1116,
	Slider_get_wholeNumbers_m1117,
	Slider_set_wholeNumbers_m1118,
	Slider_get_value_m1119,
	Slider_set_value_m1120,
	Slider_get_normalizedValue_m1121,
	Slider_set_normalizedValue_m1122,
	Slider_get_onValueChanged_m1123,
	Slider_set_onValueChanged_m1124,
	Slider_get_stepSize_m1125,
	Slider_Rebuild_m1126,
	Slider_OnEnable_m1127,
	Slider_OnDisable_m1128,
	Slider_OnDidApplyAnimationProperties_m1129,
	Slider_UpdateCachedReferences_m1130,
	Slider_ClampValue_m1131,
	Slider_Set_m1132,
	Slider_Set_m1133,
	Slider_OnRectTransformDimensionsChange_m1134,
	Slider_get_axis_m1135,
	Slider_get_reverseValue_m1136,
	Slider_UpdateVisuals_m1137,
	Slider_UpdateDrag_m1138,
	Slider_MayDrag_m1139,
	Slider_OnPointerDown_m1140,
	Slider_OnDrag_m1141,
	Slider_OnMove_m1142,
	Slider_FindSelectableOnLeft_m1143,
	Slider_FindSelectableOnRight_m1144,
	Slider_FindSelectableOnUp_m1145,
	Slider_FindSelectableOnDown_m1146,
	Slider_OnInitializePotentialDrag_m1147,
	Slider_SetDirection_m1148,
	Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1149,
	Slider_UnityEngine_UI_ICanvasElement_get_transform_m1150,
	SpriteState_get_highlightedSprite_m1151,
	SpriteState_set_highlightedSprite_m1152,
	SpriteState_get_pressedSprite_m1153,
	SpriteState_set_pressedSprite_m1154,
	SpriteState_get_disabledSprite_m1155,
	SpriteState_set_disabledSprite_m1156,
	MatEntry__ctor_m1157,
	StencilMaterial__cctor_m1158,
	StencilMaterial_Add_m1159,
	StencilMaterial_Remove_m1160,
	Text__ctor_m1161,
	Text__cctor_m1162,
	Text_get_cachedTextGenerator_m1163,
	Text_get_cachedTextGeneratorForLayout_m1164,
	Text_get_defaultMaterial_m1165,
	Text_get_mainTexture_m1166,
	Text_FontTextureChanged_m1167,
	Text_get_font_m1168,
	Text_set_font_m1169,
	Text_get_text_m1170,
	Text_set_text_m1171,
	Text_get_supportRichText_m1172,
	Text_set_supportRichText_m1173,
	Text_get_resizeTextForBestFit_m1174,
	Text_set_resizeTextForBestFit_m1175,
	Text_get_resizeTextMinSize_m1176,
	Text_set_resizeTextMinSize_m1177,
	Text_get_resizeTextMaxSize_m1178,
	Text_set_resizeTextMaxSize_m1179,
	Text_get_alignment_m1180,
	Text_set_alignment_m1181,
	Text_get_fontSize_m1182,
	Text_set_fontSize_m1183,
	Text_get_horizontalOverflow_m1184,
	Text_set_horizontalOverflow_m1185,
	Text_get_verticalOverflow_m1186,
	Text_set_verticalOverflow_m1187,
	Text_get_lineSpacing_m1188,
	Text_set_lineSpacing_m1189,
	Text_get_fontStyle_m1190,
	Text_set_fontStyle_m1191,
	Text_get_pixelsPerUnit_m1192,
	Text_OnEnable_m1193,
	Text_OnDisable_m1194,
	Text_UpdateGeometry_m1195,
	Text_GetGenerationSettings_m1196,
	Text_GetTextAnchorPivot_m1197,
	Text_OnFillVBO_m1198,
	Text_CalculateLayoutInputHorizontal_m1199,
	Text_CalculateLayoutInputVertical_m1200,
	Text_get_minWidth_m1201,
	Text_get_preferredWidth_m1202,
	Text_get_flexibleWidth_m1203,
	Text_get_minHeight_m1204,
	Text_get_preferredHeight_m1205,
	Text_get_flexibleHeight_m1206,
	Text_get_layoutPriority_m1207,
	ToggleEvent__ctor_m1208,
	Toggle__ctor_m1209,
	Toggle_get_group_m1210,
	Toggle_set_group_m1211,
	Toggle_Rebuild_m1212,
	Toggle_OnEnable_m1213,
	Toggle_OnDisable_m1214,
	Toggle_OnDidApplyAnimationProperties_m1215,
	Toggle_SetToggleGroup_m1216,
	Toggle_get_isOn_m1217,
	Toggle_set_isOn_m1218,
	Toggle_Set_m1219,
	Toggle_Set_m1220,
	Toggle_PlayEffect_m1221,
	Toggle_Start_m1222,
	Toggle_InternalToggle_m1223,
	Toggle_OnPointerClick_m1224,
	Toggle_OnSubmit_m1225,
	Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1226,
	Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1227,
	ToggleGroup__ctor_m1228,
	ToggleGroup_get_allowSwitchOff_m1229,
	ToggleGroup_set_allowSwitchOff_m1230,
	ToggleGroup_ValidateToggleIsInGroup_m1231,
	ToggleGroup_NotifyToggleOn_m1232,
	ToggleGroup_UnregisterToggle_m1233,
	ToggleGroup_RegisterToggle_m1234,
	ToggleGroup_AnyTogglesOn_m1235,
	ToggleGroup_ActiveToggles_m1236,
	ToggleGroup_SetAllTogglesOff_m1237,
	ToggleGroup_U3CAnyTogglesOnU3Em__7_m1238,
	ToggleGroup_U3CActiveTogglesU3Em__8_m1239,
	AspectRatioFitter__ctor_m1240,
	AspectRatioFitter_get_aspectMode_m1241,
	AspectRatioFitter_set_aspectMode_m1242,
	AspectRatioFitter_get_aspectRatio_m1243,
	AspectRatioFitter_set_aspectRatio_m1244,
	AspectRatioFitter_get_rectTransform_m1245,
	AspectRatioFitter_OnEnable_m1246,
	AspectRatioFitter_OnDisable_m1247,
	AspectRatioFitter_OnRectTransformDimensionsChange_m1248,
	AspectRatioFitter_UpdateRect_m1249,
	AspectRatioFitter_GetSizeDeltaToProduceSize_m1250,
	AspectRatioFitter_GetParentSize_m1251,
	AspectRatioFitter_SetLayoutHorizontal_m1252,
	AspectRatioFitter_SetLayoutVertical_m1253,
	AspectRatioFitter_SetDirty_m1254,
	CanvasScaler__ctor_m1255,
	CanvasScaler_get_uiScaleMode_m1256,
	CanvasScaler_set_uiScaleMode_m1257,
	CanvasScaler_get_referencePixelsPerUnit_m1258,
	CanvasScaler_set_referencePixelsPerUnit_m1259,
	CanvasScaler_get_scaleFactor_m1260,
	CanvasScaler_set_scaleFactor_m1261,
	CanvasScaler_get_referenceResolution_m1262,
	CanvasScaler_set_referenceResolution_m1263,
	CanvasScaler_get_screenMatchMode_m1264,
	CanvasScaler_set_screenMatchMode_m1265,
	CanvasScaler_get_matchWidthOrHeight_m1266,
	CanvasScaler_set_matchWidthOrHeight_m1267,
	CanvasScaler_get_physicalUnit_m1268,
	CanvasScaler_set_physicalUnit_m1269,
	CanvasScaler_get_fallbackScreenDPI_m1270,
	CanvasScaler_set_fallbackScreenDPI_m1271,
	CanvasScaler_get_defaultSpriteDPI_m1272,
	CanvasScaler_set_defaultSpriteDPI_m1273,
	CanvasScaler_get_dynamicPixelsPerUnit_m1274,
	CanvasScaler_set_dynamicPixelsPerUnit_m1275,
	CanvasScaler_OnEnable_m1276,
	CanvasScaler_OnDisable_m1277,
	CanvasScaler_Update_m1278,
	CanvasScaler_Handle_m1279,
	CanvasScaler_HandleWorldCanvas_m1280,
	CanvasScaler_HandleConstantPixelSize_m1281,
	CanvasScaler_HandleScaleWithScreenSize_m1282,
	CanvasScaler_HandleConstantPhysicalSize_m1283,
	CanvasScaler_SetScaleFactor_m1284,
	CanvasScaler_SetReferencePixelsPerUnit_m1285,
	ContentSizeFitter__ctor_m1286,
	ContentSizeFitter_get_horizontalFit_m1287,
	ContentSizeFitter_set_horizontalFit_m1288,
	ContentSizeFitter_get_verticalFit_m1289,
	ContentSizeFitter_set_verticalFit_m1290,
	ContentSizeFitter_get_rectTransform_m1291,
	ContentSizeFitter_OnEnable_m1292,
	ContentSizeFitter_OnDisable_m1293,
	ContentSizeFitter_OnRectTransformDimensionsChange_m1294,
	ContentSizeFitter_HandleSelfFittingAlongAxis_m1295,
	ContentSizeFitter_SetLayoutHorizontal_m1296,
	ContentSizeFitter_SetLayoutVertical_m1297,
	ContentSizeFitter_SetDirty_m1298,
	GridLayoutGroup__ctor_m1299,
	GridLayoutGroup_get_startCorner_m1300,
	GridLayoutGroup_set_startCorner_m1301,
	GridLayoutGroup_get_startAxis_m1302,
	GridLayoutGroup_set_startAxis_m1303,
	GridLayoutGroup_get_cellSize_m229,
	GridLayoutGroup_set_cellSize_m1304,
	GridLayoutGroup_get_spacing_m230,
	GridLayoutGroup_set_spacing_m1305,
	GridLayoutGroup_get_constraint_m1306,
	GridLayoutGroup_set_constraint_m1307,
	GridLayoutGroup_get_constraintCount_m1308,
	GridLayoutGroup_set_constraintCount_m1309,
	GridLayoutGroup_CalculateLayoutInputHorizontal_m1310,
	GridLayoutGroup_CalculateLayoutInputVertical_m1311,
	GridLayoutGroup_SetLayoutHorizontal_m1312,
	GridLayoutGroup_SetLayoutVertical_m1313,
	GridLayoutGroup_SetCellsAlongAxis_m1314,
	HorizontalLayoutGroup__ctor_m1315,
	HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1316,
	HorizontalLayoutGroup_CalculateLayoutInputVertical_m1317,
	HorizontalLayoutGroup_SetLayoutHorizontal_m1318,
	HorizontalLayoutGroup_SetLayoutVertical_m1319,
	HorizontalOrVerticalLayoutGroup__ctor_m1320,
	HorizontalOrVerticalLayoutGroup_get_spacing_m1321,
	HorizontalOrVerticalLayoutGroup_set_spacing_m1322,
	HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1323,
	HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1324,
	HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1325,
	HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1326,
	HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1327,
	HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1328,
	LayoutElement__ctor_m1329,
	LayoutElement_get_ignoreLayout_m1330,
	LayoutElement_set_ignoreLayout_m1331,
	LayoutElement_CalculateLayoutInputHorizontal_m1332,
	LayoutElement_CalculateLayoutInputVertical_m1333,
	LayoutElement_get_minWidth_m1334,
	LayoutElement_set_minWidth_m1335,
	LayoutElement_get_minHeight_m1336,
	LayoutElement_set_minHeight_m1337,
	LayoutElement_get_preferredWidth_m1338,
	LayoutElement_set_preferredWidth_m1339,
	LayoutElement_get_preferredHeight_m1340,
	LayoutElement_set_preferredHeight_m1341,
	LayoutElement_get_flexibleWidth_m1342,
	LayoutElement_set_flexibleWidth_m1343,
	LayoutElement_get_flexibleHeight_m1344,
	LayoutElement_set_flexibleHeight_m1345,
	LayoutElement_get_layoutPriority_m1346,
	LayoutElement_OnEnable_m1347,
	LayoutElement_OnTransformParentChanged_m1348,
	LayoutElement_OnDisable_m1349,
	LayoutElement_OnDidApplyAnimationProperties_m1350,
	LayoutElement_OnBeforeTransformParentChanged_m1351,
	LayoutElement_SetDirty_m1352,
	LayoutGroup__ctor_m1353,
	LayoutGroup_get_padding_m1354,
	LayoutGroup_set_padding_m1355,
	LayoutGroup_get_childAlignment_m1356,
	LayoutGroup_set_childAlignment_m1357,
	LayoutGroup_get_rectTransform_m1358,
	LayoutGroup_get_rectChildren_m1359,
	LayoutGroup_CalculateLayoutInputHorizontal_m1360,
	LayoutGroup_get_minWidth_m1361,
	LayoutGroup_get_preferredWidth_m1362,
	LayoutGroup_get_flexibleWidth_m1363,
	LayoutGroup_get_minHeight_m1364,
	LayoutGroup_get_preferredHeight_m1365,
	LayoutGroup_get_flexibleHeight_m1366,
	LayoutGroup_get_layoutPriority_m1367,
	LayoutGroup_OnEnable_m1368,
	LayoutGroup_OnDisable_m1369,
	LayoutGroup_OnDidApplyAnimationProperties_m1370,
	LayoutGroup_GetTotalMinSize_m1371,
	LayoutGroup_GetTotalPreferredSize_m1372,
	LayoutGroup_GetTotalFlexibleSize_m1373,
	LayoutGroup_GetStartOffset_m1374,
	LayoutGroup_SetLayoutInputForAxis_m1375,
	LayoutGroup_SetChildAlongAxis_m1376,
	LayoutGroup_get_isRootLayoutGroup_m1377,
	LayoutGroup_OnRectTransformDimensionsChange_m1378,
	LayoutGroup_OnTransformChildrenChanged_m1379,
	LayoutGroup_SetDirty_m1380,
	LayoutRebuilder__ctor_m1381,
	LayoutRebuilder__cctor_m1382,
	LayoutRebuilder_UnityEngine_UI_ICanvasElement_Rebuild_m1383,
	LayoutRebuilder_ReapplyDrivenProperties_m1384,
	LayoutRebuilder_get_transform_m1385,
	LayoutRebuilder_IsDestroyed_m1386,
	LayoutRebuilder_StripDisabledBehavioursFromList_m1387,
	LayoutRebuilder_PerformLayoutControl_m1388,
	LayoutRebuilder_PerformLayoutCalculation_m1389,
	LayoutRebuilder_MarkLayoutForRebuild_m1390,
	LayoutRebuilder_ValidLayoutGroup_m1391,
	LayoutRebuilder_ValidController_m1392,
	LayoutRebuilder_MarkLayoutRootForRebuild_m1393,
	LayoutRebuilder_Equals_m1394,
	LayoutRebuilder_GetHashCode_m1395,
	LayoutRebuilder_ToString_m1396,
	LayoutRebuilder_U3CRebuildU3Em__9_m1397,
	LayoutRebuilder_U3CRebuildU3Em__A_m1398,
	LayoutRebuilder_U3CRebuildU3Em__B_m1399,
	LayoutRebuilder_U3CRebuildU3Em__C_m1400,
	LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__D_m1401,
	LayoutUtility_GetMinSize_m1402,
	LayoutUtility_GetPreferredSize_m1403,
	LayoutUtility_GetFlexibleSize_m1404,
	LayoutUtility_GetMinWidth_m1405,
	LayoutUtility_GetPreferredWidth_m1406,
	LayoutUtility_GetFlexibleWidth_m1407,
	LayoutUtility_GetMinHeight_m1408,
	LayoutUtility_GetPreferredHeight_m1409,
	LayoutUtility_GetFlexibleHeight_m1410,
	LayoutUtility_GetLayoutProperty_m1411,
	LayoutUtility_GetLayoutProperty_m1412,
	LayoutUtility_U3CGetMinWidthU3Em__E_m1413,
	LayoutUtility_U3CGetPreferredWidthU3Em__F_m1414,
	LayoutUtility_U3CGetPreferredWidthU3Em__10_m1415,
	LayoutUtility_U3CGetFlexibleWidthU3Em__11_m1416,
	LayoutUtility_U3CGetMinHeightU3Em__12_m1417,
	LayoutUtility_U3CGetPreferredHeightU3Em__13_m1418,
	LayoutUtility_U3CGetPreferredHeightU3Em__14_m1419,
	LayoutUtility_U3CGetFlexibleHeightU3Em__15_m1420,
	VerticalLayoutGroup__ctor_m1421,
	VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1422,
	VerticalLayoutGroup_CalculateLayoutInputVertical_m1423,
	VerticalLayoutGroup_SetLayoutHorizontal_m1424,
	VerticalLayoutGroup_SetLayoutVertical_m1425,
	Mask__ctor_m1426,
	Mask_get_graphic_m1427,
	Mask_get_showMaskGraphic_m1428,
	Mask_set_showMaskGraphic_m1429,
	Mask_get_rectTransform_m1430,
	Mask_MaskEnabled_m1431,
	Mask_OnSiblingGraphicEnabledDisabled_m1432,
	Mask_NotifyMaskStateChanged_m1433,
	Mask_ClearCachedMaterial_m1434,
	Mask_OnEnable_m1435,
	Mask_OnDisable_m1436,
	Mask_IsRaycastLocationValid_m1437,
	Mask_GetModifiedMaterial_m1438,
	CanvasListPool__cctor_m1439,
	CanvasListPool_Get_m1440,
	CanvasListPool_Release_m1441,
	CanvasListPool_U3Cs_CanvasListPoolU3Em__16_m1442,
	ComponentListPool__cctor_m1443,
	ComponentListPool_Get_m1444,
	ComponentListPool_Release_m1445,
	ComponentListPool_U3Cs_ComponentListPoolU3Em__17_m1446,
	BaseVertexEffect__ctor_m1447,
	BaseVertexEffect_get_graphic_m1448,
	BaseVertexEffect_OnEnable_m1449,
	BaseVertexEffect_OnDisable_m1450,
	BaseVertexEffect_OnDidApplyAnimationProperties_m1451,
	Outline__ctor_m1452,
	Outline_ModifyVertices_m1453,
	PositionAsUV1__ctor_m1454,
	PositionAsUV1_ModifyVertices_m1455,
	Shadow__ctor_m1456,
	Shadow_get_effectColor_m1457,
	Shadow_set_effectColor_m1458,
	Shadow_get_effectDistance_m1459,
	Shadow_set_effectDistance_m1460,
	Shadow_get_useGraphicAlpha_m1461,
	Shadow_set_useGraphicAlpha_m1462,
	Shadow_ApplyShadow_m1463,
	Shadow_ModifyVertices_m1464,
	AssetBundleCreateRequest__ctor_m1983,
	AssetBundleCreateRequest_get_assetBundle_m1984,
	AssetBundleCreateRequest_DisableCompatibilityChecks_m1985,
	AssetBundleRequest__ctor_m1986,
	AssetBundleRequest_get_asset_m1987,
	AssetBundleRequest_get_allAssets_m1988,
	AssetBundle_LoadAsset_m1989,
	AssetBundle_LoadAsset_Internal_m1990,
	AssetBundle_LoadAssetWithSubAssets_Internal_m1991,
	LayerMask_get_value_m1992,
	LayerMask_set_value_m1993,
	LayerMask_LayerToName_m1994,
	LayerMask_NameToLayer_m1995,
	LayerMask_GetMask_m1996,
	LayerMask_op_Implicit_m267,
	LayerMask_op_Implicit_m1585,
	SystemInfo_get_deviceUniqueIdentifier_m1997,
	WaitForSeconds__ctor_m270,
	WaitForFixedUpdate__ctor_m1998,
	WaitForEndOfFrame__ctor_m1839,
	Coroutine__ctor_m1999,
	Coroutine_ReleaseCoroutine_m2000,
	Coroutine_Finalize_m2001,
	ScriptableObject__ctor_m2002,
	ScriptableObject_Internal_CreateScriptableObject_m2003,
	ScriptableObject_CreateInstance_m2004,
	ScriptableObject_CreateInstance_m2005,
	ScriptableObject_CreateInstanceFromType_m2006,
	UnhandledExceptionHandler__ctor_m2007,
	UnhandledExceptionHandler_RegisterUECatcher_m2008,
	UnhandledExceptionHandler_HandleUnhandledException_m2009,
	UnhandledExceptionHandler_PrintException_m2010,
	UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m2011,
	GameCenterPlatform__ctor_m2012,
	GameCenterPlatform__cctor_m2013,
	GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m2014,
	GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m2015,
	GameCenterPlatform_Internal_Authenticate_m2016,
	GameCenterPlatform_Internal_Authenticated_m2017,
	GameCenterPlatform_Internal_UserName_m2018,
	GameCenterPlatform_Internal_UserID_m2019,
	GameCenterPlatform_Internal_Underage_m2020,
	GameCenterPlatform_Internal_UserImage_m2021,
	GameCenterPlatform_Internal_LoadFriends_m2022,
	GameCenterPlatform_Internal_LoadAchievementDescriptions_m2023,
	GameCenterPlatform_Internal_LoadAchievements_m2024,
	GameCenterPlatform_Internal_ReportProgress_m2025,
	GameCenterPlatform_Internal_ReportScore_m2026,
	GameCenterPlatform_Internal_LoadScores_m2027,
	GameCenterPlatform_Internal_ShowAchievementsUI_m2028,
	GameCenterPlatform_Internal_ShowLeaderboardUI_m2029,
	GameCenterPlatform_Internal_LoadUsers_m2030,
	GameCenterPlatform_Internal_ResetAllAchievements_m2031,
	GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m2032,
	GameCenterPlatform_ResetAllAchievements_m2033,
	GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m2034,
	GameCenterPlatform_ShowLeaderboardUI_m2035,
	GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m2036,
	GameCenterPlatform_ClearAchievementDescriptions_m2037,
	GameCenterPlatform_SetAchievementDescription_m2038,
	GameCenterPlatform_SetAchievementDescriptionImage_m2039,
	GameCenterPlatform_TriggerAchievementDescriptionCallback_m2040,
	GameCenterPlatform_AuthenticateCallbackWrapper_m2041,
	GameCenterPlatform_ClearFriends_m2042,
	GameCenterPlatform_SetFriends_m2043,
	GameCenterPlatform_SetFriendImage_m2044,
	GameCenterPlatform_TriggerFriendsCallbackWrapper_m2045,
	GameCenterPlatform_AchievementCallbackWrapper_m2046,
	GameCenterPlatform_ProgressCallbackWrapper_m2047,
	GameCenterPlatform_ScoreCallbackWrapper_m2048,
	GameCenterPlatform_ScoreLoaderCallbackWrapper_m2049,
	GameCenterPlatform_get_localUser_m2050,
	GameCenterPlatform_PopulateLocalUser_m2051,
	GameCenterPlatform_LoadAchievementDescriptions_m2052,
	GameCenterPlatform_ReportProgress_m2053,
	GameCenterPlatform_LoadAchievements_m2054,
	GameCenterPlatform_ReportScore_m2055,
	GameCenterPlatform_LoadScores_m2056,
	GameCenterPlatform_LoadScores_m2057,
	GameCenterPlatform_LeaderboardCallbackWrapper_m2058,
	GameCenterPlatform_GetLoading_m2059,
	GameCenterPlatform_VerifyAuthentication_m2060,
	GameCenterPlatform_ShowAchievementsUI_m2061,
	GameCenterPlatform_ShowLeaderboardUI_m2062,
	GameCenterPlatform_ClearUsers_m2063,
	GameCenterPlatform_SetUser_m2064,
	GameCenterPlatform_SetUserImage_m2065,
	GameCenterPlatform_TriggerUsersCallbackWrapper_m2066,
	GameCenterPlatform_LoadUsers_m2067,
	GameCenterPlatform_SafeSetUserImage_m2068,
	GameCenterPlatform_SafeClearArray_m2069,
	GameCenterPlatform_CreateLeaderboard_m2070,
	GameCenterPlatform_CreateAchievement_m2071,
	GameCenterPlatform_TriggerResetAchievementCallback_m2072,
	GcLeaderboard__ctor_m2073,
	GcLeaderboard_Finalize_m2074,
	GcLeaderboard_Contains_m2075,
	GcLeaderboard_SetScores_m2076,
	GcLeaderboard_SetLocalScore_m2077,
	GcLeaderboard_SetMaxRange_m2078,
	GcLeaderboard_SetTitle_m2079,
	GcLeaderboard_Internal_LoadScores_m2080,
	GcLeaderboard_Internal_LoadScoresWithUsers_m2081,
	GcLeaderboard_Loading_m2082,
	GcLeaderboard_Dispose_m2083,
	BoneWeight_get_weight0_m2084,
	BoneWeight_set_weight0_m2085,
	BoneWeight_get_weight1_m2086,
	BoneWeight_set_weight1_m2087,
	BoneWeight_get_weight2_m2088,
	BoneWeight_set_weight2_m2089,
	BoneWeight_get_weight3_m2090,
	BoneWeight_set_weight3_m2091,
	BoneWeight_get_boneIndex0_m2092,
	BoneWeight_set_boneIndex0_m2093,
	BoneWeight_get_boneIndex1_m2094,
	BoneWeight_set_boneIndex1_m2095,
	BoneWeight_get_boneIndex2_m2096,
	BoneWeight_set_boneIndex2_m2097,
	BoneWeight_get_boneIndex3_m2098,
	BoneWeight_set_boneIndex3_m2099,
	BoneWeight_GetHashCode_m2100,
	BoneWeight_Equals_m2101,
	BoneWeight_op_Equality_m2102,
	BoneWeight_op_Inequality_m2103,
	Renderer_get_sortingLayerID_m1583,
	Renderer_get_sortingOrder_m1584,
	Screen_get_width_m1665,
	Screen_get_height_m1666,
	Screen_get_dpi_m1929,
	Texture__ctor_m2104,
	Texture_Internal_GetWidth_m2105,
	Texture_Internal_GetHeight_m2106,
	Texture_get_width_m2107,
	Texture_get_height_m2108,
	Texture2D__ctor_m2109,
	Texture2D_Internal_Create_m2110,
	Texture2D_get_whiteTexture_m1631,
	Texture2D_GetPixelBilinear_m1722,
	RenderTexture_Internal_GetWidth_m2111,
	RenderTexture_Internal_GetHeight_m2112,
	RenderTexture_get_width_m2113,
	RenderTexture_get_height_m2114,
	GUILayer_HitTest_m2115,
	GUILayer_INTERNAL_CALL_HitTest_m2116,
	GradientColorKey__ctor_m2117,
	GradientAlphaKey__ctor_m2118,
	Gradient__ctor_m2119,
	Gradient_Init_m2120,
	Gradient_Cleanup_m2121,
	Gradient_Finalize_m2122,
	ScrollViewState__ctor_m2123,
	WindowFunction__ctor_m2124,
	WindowFunction_Invoke_m2125,
	WindowFunction_BeginInvoke_m2126,
	WindowFunction_EndInvoke_m2127,
	GUI__cctor_m2128,
	GUI_set_nextScrollStepTime_m2129,
	GUI_set_skin_m2130,
	GUI_get_skin_m2131,
	GUI_set_changed_m2132,
	GUI_CallWindowDelegate_m2133,
	GUILayout_Width_m2134,
	GUILayout_Height_m2135,
	LayoutCache__ctor_m2136,
	GUILayoutUtility__cctor_m2137,
	GUILayoutUtility_SelectIDList_m2138,
	GUILayoutUtility_Begin_m2139,
	GUILayoutUtility_BeginWindow_m2140,
	GUILayoutUtility_Layout_m2141,
	GUILayoutUtility_LayoutFromEditorWindow_m2142,
	GUILayoutUtility_LayoutFreeGroup_m2143,
	GUILayoutUtility_LayoutSingleGroup_m2144,
	GUILayoutUtility_Internal_GetWindowRect_m2145,
	GUILayoutUtility_Internal_MoveWindow_m2146,
	GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m2147,
	GUILayoutUtility_get_spaceStyle_m2148,
	GUILayoutEntry__ctor_m2149,
	GUILayoutEntry__cctor_m2150,
	GUILayoutEntry_get_style_m2151,
	GUILayoutEntry_set_style_m2152,
	GUILayoutEntry_get_margin_m2153,
	GUILayoutEntry_CalcWidth_m2154,
	GUILayoutEntry_CalcHeight_m2155,
	GUILayoutEntry_SetHorizontal_m2156,
	GUILayoutEntry_SetVertical_m2157,
	GUILayoutEntry_ApplyStyleSettings_m2158,
	GUILayoutEntry_ApplyOptions_m2159,
	GUILayoutEntry_ToString_m2160,
	GUILayoutGroup__ctor_m2161,
	GUILayoutGroup_get_margin_m2162,
	GUILayoutGroup_ApplyOptions_m2163,
	GUILayoutGroup_ApplyStyleSettings_m2164,
	GUILayoutGroup_ResetCursor_m2165,
	GUILayoutGroup_CalcWidth_m2166,
	GUILayoutGroup_SetHorizontal_m2167,
	GUILayoutGroup_CalcHeight_m2168,
	GUILayoutGroup_SetVertical_m2169,
	GUILayoutGroup_ToString_m2170,
	GUIScrollGroup__ctor_m2171,
	GUIScrollGroup_CalcWidth_m2172,
	GUIScrollGroup_SetHorizontal_m2173,
	GUIScrollGroup_CalcHeight_m2174,
	GUIScrollGroup_SetVertical_m2175,
	GUILayoutOption__ctor_m2176,
	GUIUtility__cctor_m2177,
	GUIUtility_get_systemCopyBuffer_m2178,
	GUIUtility_set_systemCopyBuffer_m2179,
	GUIUtility_GetDefaultSkin_m2180,
	GUIUtility_Internal_GetDefaultSkin_m2181,
	GUIUtility_BeginGUI_m2182,
	GUIUtility_Internal_ExitGUI_m2183,
	GUIUtility_EndGUI_m2184,
	GUIUtility_EndGUIFromException_m2185,
	GUIUtility_CheckOnGUI_m2186,
	GUIUtility_Internal_GetGUIDepth_m2187,
	GUISettings__ctor_m2188,
	SkinChangedDelegate__ctor_m2189,
	SkinChangedDelegate_Invoke_m2190,
	SkinChangedDelegate_BeginInvoke_m2191,
	SkinChangedDelegate_EndInvoke_m2192,
	GUISkin__ctor_m2193,
	GUISkin_OnEnable_m2194,
	GUISkin_get_font_m2195,
	GUISkin_set_font_m2196,
	GUISkin_get_box_m2197,
	GUISkin_set_box_m2198,
	GUISkin_get_label_m2199,
	GUISkin_set_label_m2200,
	GUISkin_get_textField_m2201,
	GUISkin_set_textField_m2202,
	GUISkin_get_textArea_m2203,
	GUISkin_set_textArea_m2204,
	GUISkin_get_button_m2205,
	GUISkin_set_button_m2206,
	GUISkin_get_toggle_m2207,
	GUISkin_set_toggle_m2208,
	GUISkin_get_window_m2209,
	GUISkin_set_window_m2210,
	GUISkin_get_horizontalSlider_m2211,
	GUISkin_set_horizontalSlider_m2212,
	GUISkin_get_horizontalSliderThumb_m2213,
	GUISkin_set_horizontalSliderThumb_m2214,
	GUISkin_get_verticalSlider_m2215,
	GUISkin_set_verticalSlider_m2216,
	GUISkin_get_verticalSliderThumb_m2217,
	GUISkin_set_verticalSliderThumb_m2218,
	GUISkin_get_horizontalScrollbar_m2219,
	GUISkin_set_horizontalScrollbar_m2220,
	GUISkin_get_horizontalScrollbarThumb_m2221,
	GUISkin_set_horizontalScrollbarThumb_m2222,
	GUISkin_get_horizontalScrollbarLeftButton_m2223,
	GUISkin_set_horizontalScrollbarLeftButton_m2224,
	GUISkin_get_horizontalScrollbarRightButton_m2225,
	GUISkin_set_horizontalScrollbarRightButton_m2226,
	GUISkin_get_verticalScrollbar_m2227,
	GUISkin_set_verticalScrollbar_m2228,
	GUISkin_get_verticalScrollbarThumb_m2229,
	GUISkin_set_verticalScrollbarThumb_m2230,
	GUISkin_get_verticalScrollbarUpButton_m2231,
	GUISkin_set_verticalScrollbarUpButton_m2232,
	GUISkin_get_verticalScrollbarDownButton_m2233,
	GUISkin_set_verticalScrollbarDownButton_m2234,
	GUISkin_get_scrollView_m2235,
	GUISkin_set_scrollView_m2236,
	GUISkin_get_customStyles_m2237,
	GUISkin_set_customStyles_m2238,
	GUISkin_get_settings_m2239,
	GUISkin_get_error_m2240,
	GUISkin_Apply_m2241,
	GUISkin_BuildStyleCache_m2242,
	GUISkin_GetStyle_m2243,
	GUISkin_FindStyle_m2244,
	GUISkin_MakeCurrent_m2245,
	GUISkin_GetEnumerator_m2246,
	GUIContent__ctor_m2247,
	GUIContent__ctor_m1757,
	GUIContent__cctor_m2248,
	GUIContent_get_text_m1756,
	GUIContent_set_text_m2249,
	GUIContent_ClearStaticCache_m2250,
	GUIStyleState__ctor_m2251,
	GUIStyleState__ctor_m2252,
	GUIStyleState_Finalize_m2253,
	GUIStyleState_Init_m2254,
	GUIStyleState_Cleanup_m2255,
	GUIStyleState_GetBackgroundInternal_m2256,
	GUIStyleState_set_textColor_m2257,
	GUIStyleState_INTERNAL_set_textColor_m2258,
	RectOffset__ctor_m1956,
	RectOffset__ctor_m2259,
	RectOffset__ctor_m2260,
	RectOffset_Finalize_m2261,
	RectOffset_Init_m2262,
	RectOffset_Cleanup_m2263,
	RectOffset_get_left_m1954,
	RectOffset_set_left_m2264,
	RectOffset_get_right_m2265,
	RectOffset_set_right_m2266,
	RectOffset_get_top_m1955,
	RectOffset_set_top_m2267,
	RectOffset_get_bottom_m2268,
	RectOffset_set_bottom_m2269,
	RectOffset_get_horizontal_m1948,
	RectOffset_get_vertical_m1950,
	RectOffset_Remove_m2270,
	RectOffset_INTERNAL_CALL_Remove_m2271,
	RectOffset_ToString_m2272,
	GUIStyle__ctor_m2273,
	GUIStyle__cctor_m2274,
	GUIStyle_Finalize_m2275,
	GUIStyle_Init_m2276,
	GUIStyle_Cleanup_m2277,
	GUIStyle_get_name_m2278,
	GUIStyle_set_name_m2279,
	GUIStyle_get_normal_m2280,
	GUIStyle_GetStyleStatePtr_m2281,
	GUIStyle_get_margin_m2282,
	GUIStyle_get_padding_m2283,
	GUIStyle_GetRectOffsetPtr_m2284,
	GUIStyle_get_fixedWidth_m2285,
	GUIStyle_get_fixedHeight_m2286,
	GUIStyle_get_stretchWidth_m2287,
	GUIStyle_set_stretchWidth_m2288,
	GUIStyle_get_stretchHeight_m2289,
	GUIStyle_set_stretchHeight_m2290,
	GUIStyle_Internal_GetLineHeight_m2291,
	GUIStyle_get_lineHeight_m2292,
	GUIStyle_SetDefaultFont_m2293,
	GUIStyle_get_none_m2294,
	GUIStyle_GetCursorPixelPosition_m2295,
	GUIStyle_Internal_GetCursorPixelPosition_m2296,
	GUIStyle_INTERNAL_CALL_Internal_GetCursorPixelPosition_m2297,
	GUIStyle_CalcSize_m2298,
	GUIStyle_Internal_CalcSize_m2299,
	GUIStyle_CalcHeight_m2300,
	GUIStyle_Internal_CalcHeight_m2301,
	GUIStyle_ToString_m2302,
	TouchScreenKeyboard__ctor_m2303,
	TouchScreenKeyboard_Destroy_m2304,
	TouchScreenKeyboard_Finalize_m2305,
	TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m2306,
	TouchScreenKeyboard_get_isSupported_m1760,
	TouchScreenKeyboard_Open_m1830,
	TouchScreenKeyboard_Open_m1831,
	TouchScreenKeyboard_Open_m2307,
	TouchScreenKeyboard_get_text_m1737,
	TouchScreenKeyboard_set_text_m1738,
	TouchScreenKeyboard_set_hideInput_m1829,
	TouchScreenKeyboard_get_active_m1736,
	TouchScreenKeyboard_set_active_m1828,
	TouchScreenKeyboard_get_done_m1765,
	TouchScreenKeyboard_get_wasCanceled_m1761,
	Event__ctor_m1733,
	Event_Init_m2308,
	Event_Finalize_m2309,
	Event_Cleanup_m2310,
	Event_get_rawType_m1779,
	Event_get_type_m2311,
	Event_get_mousePosition_m2312,
	Event_Internal_GetMousePosition_m2313,
	Event_get_modifiers_m1775,
	Event_get_character_m1777,
	Event_get_commandName_m2314,
	Event_get_keyCode_m1776,
	Event_get_current_m2315,
	Event_Internal_SetNativeEvent_m2316,
	Event_Internal_MakeMasterEventCurrent_m2317,
	Event_PopEvent_m1780,
	Event_get_isKey_m2318,
	Event_get_isMouse_m2319,
	Event_GetHashCode_m2320,
	Event_Equals_m2321,
	Event_ToString_m2322,
	Vector2__ctor_m231,
	Vector2_get_Item_m1715,
	Vector2_set_Item_m1725,
	Vector2_Scale_m1817,
	Vector2_ToString_m2323,
	Vector2_GetHashCode_m2324,
	Vector2_Equals_m2325,
	Vector2_Dot_m1553,
	Vector2_get_sqrMagnitude_m1516,
	Vector2_SqrMagnitude_m2326,
	Vector2_get_zero_m1510,
	Vector2_get_one_m1711,
	Vector2_get_up_m1951,
	Vector2_op_Addition_m266,
	Vector2_op_Subtraction_m1526,
	Vector2_op_Multiply_m1712,
	Vector2_op_Division_m1774,
	Vector2_op_Equality_m1980,
	Vector2_op_Inequality_m1808,
	Vector2_op_Implicit_m262,
	Vector2_op_Implicit_m260,
	Vector3__ctor_m192,
	Vector3__ctor_m1644,
	Vector3_Lerp_m1856,
	Vector3_MoveTowards_m261,
	Vector3_get_Item_m1860,
	Vector3_set_Item_m1861,
	Vector3_GetHashCode_m2327,
	Vector3_Equals_m2328,
	Vector3_Normalize_m2329,
	Vector3_get_normalized_m1886,
	Vector3_ToString_m2330,
	Vector3_ToString_m2331,
	Vector3_Dot_m1674,
	Vector3_Distance_m1580,
	Vector3_Magnitude_m2332,
	Vector3_get_magnitude_m2333,
	Vector3_SqrMagnitude_m2334,
	Vector3_get_sqrMagnitude_m258,
	Vector3_Min_m1870,
	Vector3_Max_m1871,
	Vector3_get_zero_m1509,
	Vector3_get_one_m236,
	Vector3_get_forward_m1672,
	Vector3_get_back_m2335,
	Vector3_get_up_m1508,
	Vector3_get_down_m1892,
	Vector3_get_left_m1890,
	Vector3_get_right_m1891,
	Vector3_op_Addition_m1820,
	Vector3_op_Subtraction_m251,
	Vector3_op_Multiply_m1912,
	Vector3_op_Division_m2336,
	Vector3_op_Equality_m2337,
	Vector3_op_Inequality_m1804,
	Color__ctor_m1731,
	Color__ctor_m2338,
	Color_ToString_m2339,
	Color_GetHashCode_m2340,
	Color_Equals_m1651,
	Color_Lerp_m1596,
	Color_get_red_m2341,
	Color_get_white_m1618,
	Color_get_black_m1655,
	Color_op_Multiply_m1885,
	Color_op_Implicit_m2342,
	Color32__ctor_m1611,
	Color32_ToString_m2343,
	Color32_op_Implicit_m1643,
	Color32_op_Implicit_m1612,
	Quaternion__ctor_m2344,
	Quaternion_get_identity_m196,
	Quaternion_Dot_m2345,
	Quaternion_Inverse_m1887,
	Quaternion_INTERNAL_CALL_Inverse_m2346,
	Quaternion_ToString_m2347,
	Quaternion_GetHashCode_m2348,
	Quaternion_Equals_m2349,
	Quaternion_op_Multiply_m1673,
	Quaternion_op_Inequality_m1806,
	Rect__ctor_m1836,
	Rect_get_x_m1640,
	Rect_set_x_m1704,
	Rect_get_y_m1641,
	Rect_set_y_m1702,
	Rect_get_position_m1713,
	Rect_get_center_m1846,
	Rect_get_width_m1635,
	Rect_set_width_m1703,
	Rect_get_height_m1636,
	Rect_set_height_m1700,
	Rect_get_size_m1710,
	Rect_get_xMin_m1730,
	Rect_get_yMin_m1729,
	Rect_get_xMax_m1720,
	Rect_get_yMax_m1721,
	Rect_ToString_m2350,
	Rect_Contains_m2351,
	Rect_GetHashCode_m2352,
	Rect_Equals_m2353,
	Rect_op_Equality_m1837,
	Matrix4x4_get_Item_m2354,
	Matrix4x4_set_Item_m2355,
	Matrix4x4_get_Item_m2356,
	Matrix4x4_set_Item_m2357,
	Matrix4x4_GetHashCode_m2358,
	Matrix4x4_Equals_m2359,
	Matrix4x4_Inverse_m2360,
	Matrix4x4_INTERNAL_CALL_Inverse_m2361,
	Matrix4x4_Transpose_m2362,
	Matrix4x4_INTERNAL_CALL_Transpose_m2363,
	Matrix4x4_Invert_m2364,
	Matrix4x4_INTERNAL_CALL_Invert_m2365,
	Matrix4x4_get_inverse_m2366,
	Matrix4x4_get_transpose_m2367,
	Matrix4x4_get_isIdentity_m2368,
	Matrix4x4_GetColumn_m2369,
	Matrix4x4_GetRow_m2370,
	Matrix4x4_SetColumn_m2371,
	Matrix4x4_SetRow_m2372,
	Matrix4x4_MultiplyPoint_m2373,
	Matrix4x4_MultiplyPoint3x4_m1869,
	Matrix4x4_MultiplyVector_m2374,
	Matrix4x4_Scale_m2375,
	Matrix4x4_get_zero_m2376,
	Matrix4x4_get_identity_m2377,
	Matrix4x4_SetTRS_m2378,
	Matrix4x4_TRS_m2379,
	Matrix4x4_INTERNAL_CALL_TRS_m2380,
	Matrix4x4_ToString_m2381,
	Matrix4x4_ToString_m2382,
	Matrix4x4_Ortho_m2383,
	Matrix4x4_Perspective_m2384,
	Matrix4x4_op_Multiply_m2385,
	Matrix4x4_op_Multiply_m2386,
	Matrix4x4_op_Equality_m2387,
	Matrix4x4_op_Inequality_m2388,
	Bounds__ctor_m1863,
	Bounds_GetHashCode_m2389,
	Bounds_Equals_m2390,
	Bounds_get_center_m1864,
	Bounds_set_center_m1866,
	Bounds_get_size_m1854,
	Bounds_set_size_m1865,
	Bounds_get_extents_m2391,
	Bounds_set_extents_m2392,
	Bounds_get_min_m1859,
	Bounds_set_min_m2393,
	Bounds_get_max_m1873,
	Bounds_set_max_m2394,
	Bounds_SetMinMax_m2395,
	Bounds_Encapsulate_m1872,
	Bounds_Encapsulate_m2396,
	Bounds_Expand_m2397,
	Bounds_Expand_m2398,
	Bounds_Intersects_m2399,
	Bounds_Internal_Contains_m2400,
	Bounds_INTERNAL_CALL_Internal_Contains_m2401,
	Bounds_Contains_m2402,
	Bounds_Internal_SqrDistance_m2403,
	Bounds_INTERNAL_CALL_Internal_SqrDistance_m2404,
	Bounds_SqrDistance_m2405,
	Bounds_Internal_IntersectRay_m2406,
	Bounds_INTERNAL_CALL_Internal_IntersectRay_m2407,
	Bounds_IntersectRay_m2408,
	Bounds_IntersectRay_m2409,
	Bounds_Internal_GetClosestPoint_m2410,
	Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m2411,
	Bounds_ClosestPoint_m2412,
	Bounds_ToString_m2413,
	Bounds_ToString_m2414,
	Bounds_op_Equality_m2415,
	Bounds_op_Inequality_m1857,
	Vector4__ctor_m1642,
	Vector4_get_Item_m1714,
	Vector4_set_Item_m1716,
	Vector4_GetHashCode_m2416,
	Vector4_Equals_m2417,
	Vector4_ToString_m2418,
	Vector4_Dot_m2419,
	Vector4_SqrMagnitude_m2420,
	Vector4_get_sqrMagnitude_m1692,
	Vector4_get_zero_m1696,
	Vector4_op_Subtraction_m2421,
	Vector4_op_Division_m1709,
	Vector4_op_Equality_m2422,
	Ray__ctor_m2423,
	Ray_get_origin_m1574,
	Ray_get_direction_m1575,
	Ray_GetPoint_m1769,
	Ray_ToString_m2424,
	Plane__ctor_m1767,
	Plane_get_normal_m2425,
	Plane_get_distance_m2426,
	Plane_Raycast_m1768,
	MathfInternal__cctor_m2427,
	Mathf__cctor_m2428,
	Mathf_Sin_m2429,
	Mathf_Cos_m2430,
	Mathf_Sqrt_m2431,
	Mathf_Abs_m2432,
	Mathf_Min_m1928,
	Mathf_Min_m1785,
	Mathf_Max_m1889,
	Mathf_Max_m1783,
	Mathf_Pow_m2433,
	Mathf_Log_m1927,
	Mathf_Floor_m2434,
	Mathf_Round_m2435,
	Mathf_CeilToInt_m1947,
	Mathf_FloorToInt_m1949,
	Mathf_RoundToInt_m1699,
	Mathf_Sign_m1862,
	Mathf_Clamp_m1695,
	Mathf_Clamp_m1613,
	Mathf_Clamp01_m1687,
	Mathf_Lerp_m215,
	Mathf_Approximately_m1549,
	Mathf_SmoothDamp_m1855,
	Mathf_Repeat_m1727,
	Mathf_InverseLerp_m1726,
	DrivenRectTransformTracker_Add_m1845,
	DrivenRectTransformTracker_Clear_m1843,
	ReapplyDrivenProperties__ctor_m1962,
	ReapplyDrivenProperties_Invoke_m2436,
	ReapplyDrivenProperties_BeginInvoke_m2437,
	ReapplyDrivenProperties_EndInvoke_m2438,
	RectTransform_add_reapplyDrivenProperties_m1963,
	RectTransform_remove_reapplyDrivenProperties_m2439,
	RectTransform_get_rect_m1634,
	RectTransform_INTERNAL_get_rect_m2440,
	RectTransform_get_anchorMin_m1705,
	RectTransform_set_anchorMin_m1814,
	RectTransform_INTERNAL_get_anchorMin_m2441,
	RectTransform_INTERNAL_set_anchorMin_m2442,
	RectTransform_get_anchorMax_m1809,
	RectTransform_set_anchorMax_m1706,
	RectTransform_INTERNAL_get_anchorMax_m2443,
	RectTransform_INTERNAL_set_anchorMax_m2444,
	RectTransform_get_anchoredPosition_m1810,
	RectTransform_set_anchoredPosition_m1815,
	RectTransform_INTERNAL_get_anchoredPosition_m2445,
	RectTransform_INTERNAL_set_anchoredPosition_m2446,
	RectTransform_get_sizeDelta_m1811,
	RectTransform_set_sizeDelta_m232,
	RectTransform_INTERNAL_get_sizeDelta_m2447,
	RectTransform_INTERNAL_set_sizeDelta_m2448,
	RectTransform_get_pivot_m1701,
	RectTransform_set_pivot_m1816,
	RectTransform_INTERNAL_get_pivot_m2449,
	RectTransform_INTERNAL_set_pivot_m2450,
	RectTransform_SendReapplyDrivenProperties_m2451,
	RectTransform_GetLocalCorners_m2452,
	RectTransform_GetWorldCorners_m1868,
	RectTransform_SetInsetAndSizeFromParentEdge_m1961,
	RectTransform_SetSizeWithCurrentAnchors_m1925,
	RectTransform_GetParentSize_m2453,
	ResourceRequest__ctor_m2454,
	ResourceRequest_get_asset_m2455,
	Resources_Load_m2456,
	SerializePrivateVariables__ctor_m2457,
	SerializeField__ctor_m2458,
	Shader_PropertyToID_m2459,
	Material__ctor_m1901,
	Material_get_mainTexture_m1906,
	Material_GetTexture_m2460,
	Material_GetTexture_m2461,
	Material_SetFloat_m2462,
	Material_SetFloat_m2463,
	Material_SetInt_m1902,
	Material_HasProperty_m1899,
	Material_HasProperty_m2464,
	Material_Internal_CreateWithMaterial_m2465,
	SphericalHarmonicsL2_Clear_m2466,
	SphericalHarmonicsL2_ClearInternal_m2467,
	SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m2468,
	SphericalHarmonicsL2_AddAmbientLight_m2469,
	SphericalHarmonicsL2_AddAmbientLightInternal_m2470,
	SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m2471,
	SphericalHarmonicsL2_AddDirectionalLight_m2472,
	SphericalHarmonicsL2_AddDirectionalLightInternal_m2473,
	SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m2474,
	SphericalHarmonicsL2_get_Item_m2475,
	SphericalHarmonicsL2_set_Item_m2476,
	SphericalHarmonicsL2_GetHashCode_m2477,
	SphericalHarmonicsL2_Equals_m2478,
	SphericalHarmonicsL2_op_Multiply_m2479,
	SphericalHarmonicsL2_op_Multiply_m2480,
	SphericalHarmonicsL2_op_Addition_m2481,
	SphericalHarmonicsL2_op_Equality_m2482,
	SphericalHarmonicsL2_op_Inequality_m2483,
	Sprite_get_rect_m1698,
	Sprite_INTERNAL_get_rect_m2484,
	Sprite_get_pixelsPerUnit_m1693,
	Sprite_get_texture_m1690,
	Sprite_get_textureRect_m1719,
	Sprite_INTERNAL_get_textureRect_m2485,
	Sprite_get_border_m1691,
	Sprite_INTERNAL_get_border_m2486,
	SpriteRenderer_set_sprite_m273,
	SpriteRenderer_SetSprite_INTERNAL_m2487,
	DataUtility_GetInnerUV_m1708,
	DataUtility_GetOuterUV_m1707,
	DataUtility_GetPadding_m1697,
	DataUtility_GetMinSize_m1717,
	DataUtility_Internal_GetMinSize_m2488,
	WWW__ctor_m205,
	WWW_Dispose_m2489,
	WWW_Finalize_m2490,
	WWW_DestroyWWW_m2491,
	WWW_InitWWW_m2492,
	WWW_get_responseHeaders_m2493,
	WWW_get_responseHeadersString_m2494,
	WWW_get_text_m206,
	WWW_get_DefaultEncoding_m2495,
	WWW_GetTextEncoder_m2496,
	WWW_get_bytes_m2497,
	WWW_get_error_m218,
	WWW_get_isDone_m2498,
	WWW_FlattenedHeadersFrom_m2499,
	WWW_ParseHTTPHeaderString_m2500,
	WWWForm__ctor_m200,
	WWWForm_AddField_m203,
	WWWForm_AddField_m2501,
	WWWForm_AddField_m269,
	WWWForm_get_headers_m2502,
	WWWForm_get_data_m2503,
	WWWTranscoder__cctor_m2504,
	WWWTranscoder_Byte2Hex_m2505,
	WWWTranscoder_URLEncode_m2506,
	WWWTranscoder_QPEncode_m2507,
	WWWTranscoder_Encode_m2508,
	WWWTranscoder_ByteArrayContains_m2509,
	WWWTranscoder_SevenBitClean_m2510,
	WWWTranscoder_SevenBitClean_m2511,
	UnityString_Format_m2512,
	AsyncOperation__ctor_m2513,
	AsyncOperation_InternalDestroy_m2514,
	AsyncOperation_Finalize_m2515,
	LogCallback__ctor_m2516,
	LogCallback_Invoke_m2517,
	LogCallback_BeginInvoke_m2518,
	LogCallback_EndInvoke_m2519,
	Application_LoadLevel_m241,
	Application_LoadLevelAsync_m2520,
	Application_get_isPlaying_m1833,
	Application_get_isEditor_m1835,
	Application_get_platform_m1735,
	Application_get_cloudProjectId_m2521,
	Application_CallLogCallback_m2522,
	Behaviour__ctor_m2523,
	Behaviour_get_enabled_m1512,
	Behaviour_set_enabled_m250,
	Behaviour_get_isActiveAndEnabled_m1513,
	CameraCallback__ctor_m2524,
	CameraCallback_Invoke_m2525,
	CameraCallback_BeginInvoke_m2526,
	CameraCallback_EndInvoke_m2527,
	Camera_get_nearClipPlane_m1573,
	Camera_get_farClipPlane_m1572,
	Camera_get_depth_m1473,
	Camera_get_cullingMask_m1587,
	Camera_get_eventMask_m2528,
	Camera_get_pixelRect_m2529,
	Camera_INTERNAL_get_pixelRect_m2530,
	Camera_get_targetTexture_m2531,
	Camera_get_clearFlags_m2532,
	Camera_ScreenToViewportPoint_m1667,
	Camera_INTERNAL_CALL_ScreenToViewportPoint_m2533,
	Camera_ScreenPointToRay_m1571,
	Camera_INTERNAL_CALL_ScreenPointToRay_m2534,
	Camera_get_main_m1586,
	Camera_get_allCamerasCount_m2535,
	Camera_GetAllCameras_m2536,
	Camera_FireOnPreCull_m2537,
	Camera_FireOnPreRender_m2538,
	Camera_FireOnPostRender_m2539,
	Camera_RaycastTry_m2540,
	Camera_INTERNAL_CALL_RaycastTry_m2541,
	Camera_RaycastTry2D_m2542,
	Camera_INTERNAL_CALL_RaycastTry2D_m2543,
	Debug_Internal_Log_m2544,
	Debug_Internal_LogException_m2545,
	Debug_Log_m202,
	Debug_LogError_m1469,
	Debug_LogError_m1724,
	Debug_LogException_m2546,
	Debug_LogException_m1609,
	Debug_LogWarning_m2547,
	Debug_LogWarning_m1900,
	DisplaysUpdatedDelegate__ctor_m2548,
	DisplaysUpdatedDelegate_Invoke_m2549,
	DisplaysUpdatedDelegate_BeginInvoke_m2550,
	DisplaysUpdatedDelegate_EndInvoke_m2551,
	Display__ctor_m2552,
	Display__ctor_m2553,
	Display__cctor_m2554,
	Display_add_onDisplaysUpdated_m2555,
	Display_remove_onDisplaysUpdated_m2556,
	Display_get_renderingWidth_m2557,
	Display_get_renderingHeight_m2558,
	Display_get_systemWidth_m2559,
	Display_get_systemHeight_m2560,
	Display_get_colorBuffer_m2561,
	Display_get_depthBuffer_m2562,
	Display_Activate_m2563,
	Display_Activate_m2564,
	Display_SetParams_m2565,
	Display_SetRenderingResolution_m2566,
	Display_MultiDisplayLicense_m2567,
	Display_RelativeMouseAt_m2568,
	Display_get_main_m2569,
	Display_RecreateDisplayList_m2570,
	Display_FireDisplaysUpdated_m2571,
	Display_GetSystemExtImpl_m2572,
	Display_GetRenderingExtImpl_m2573,
	Display_GetRenderingBuffersImpl_m2574,
	Display_SetRenderingResolutionImpl_m2575,
	Display_ActivateDisplayImpl_m2576,
	Display_SetParamsImpl_m2577,
	Display_MultiDisplayLicenseImpl_m2578,
	Display_RelativeMouseAtImpl_m2579,
	MonoBehaviour__ctor_m191,
	MonoBehaviour_InvokeRepeating_m242,
	MonoBehaviour_CancelInvoke_m253,
	MonoBehaviour_StartCoroutine_m207,
	MonoBehaviour_StartCoroutine_Auto_m2580,
	MonoBehaviour_StartCoroutine_m2581,
	MonoBehaviour_StartCoroutine_m223,
	MonoBehaviour_StopCoroutine_m2582,
	MonoBehaviour_StopCoroutine_m1840,
	MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2583,
	MonoBehaviour_StopCoroutine_Auto_m2584,
	Touch_get_fingerId_m1523,
	Touch_get_position_m1525,
	Touch_get_phase_m1524,
	Input__cctor_m2585,
	Input_GetAxisRaw_m1548,
	Input_GetButtonDown_m1547,
	Input_GetMouseButton_m1568,
	Input_GetMouseButtonDown_m1527,
	Input_GetMouseButtonUp_m1528,
	Input_get_mousePosition_m1529,
	Input_INTERNAL_get_mousePosition_m2586,
	Input_get_mouseScrollDelta_m1530,
	Input_INTERNAL_get_mouseScrollDelta_m2587,
	Input_get_mousePresent_m1546,
	Input_GetTouch_m1566,
	Input_get_touchCount_m1567,
	Input_get_touchSupported_m1565,
	Input_set_imeCompositionMode_m1832,
	Input_get_compositionString_m1751,
	Input_set_compositionCursorPos_m1819,
	Input_INTERNAL_set_compositionCursorPos_m2588,
	Object__ctor_m2589,
	Object_Internal_CloneSingle_m2590,
	Object_Internal_InstantiateSingle_m2591,
	Object_INTERNAL_CALL_Internal_InstantiateSingle_m2592,
	Object_Destroy_m2593,
	Object_Destroy_m227,
	Object_DestroyImmediate_m2594,
	Object_DestroyImmediate_m1834,
	Object_get_name_m271,
	Object_set_name_m234,
	Object_set_hideFlags_m1795,
	Object_ToString_m2595,
	Object_Equals_m2596,
	Object_GetHashCode_m2597,
	Object_CompareBaseObjects_m2598,
	Object_IsNativeObjectAlive_m2599,
	Object_GetInstanceID_m2600,
	Object_GetCachedPtr_m2601,
	Object_Instantiate_m197,
	Object_CheckNullArgument_m2602,
	Object_op_Implicit_m1468,
	Object_op_Equality_m247,
	Object_op_Inequality_m1472,
	Component__ctor_m2603,
	Component_get_transform_m224,
	Component_get_gameObject_m226,
	Component_GetComponent_m1960,
	Component_GetComponentFastPath_m2604,
	Component_GetComponentsForListInternal_m2605,
	Component_GetComponents_m1630,
	GameObject__ctor_m193,
	GameObject_GetComponent_m2606,
	GameObject_GetComponentFastPath_m2607,
	GameObject_GetComponentsInternal_m2608,
	GameObject_get_transform_m194,
	GameObject_get_layer_m1797,
	GameObject_set_layer_m1798,
	GameObject_SetActive_m238,
	GameObject_get_activeInHierarchy_m1514,
	GameObject_SendMessage_m2609,
	GameObject_Internal_AddComponentWithType_m2610,
	GameObject_AddComponent_m2611,
	GameObject_Internal_CreateGameObject_m2612,
	GameObject_Find_m209,
	Enumerator__ctor_m2613,
	Enumerator_get_Current_m2614,
	Enumerator_MoveNext_m2615,
	Transform_get_position_m257,
	Transform_set_position_m245,
	Transform_INTERNAL_get_position_m2616,
	Transform_INTERNAL_set_position_m2617,
	Transform_get_localPosition_m1803,
	Transform_set_localPosition_m1812,
	Transform_INTERNAL_get_localPosition_m2618,
	Transform_INTERNAL_set_localPosition_m2619,
	Transform_get_forward_m1675,
	Transform_get_rotation_m1671,
	Transform_INTERNAL_get_rotation_m2620,
	Transform_get_localRotation_m1805,
	Transform_set_localRotation_m1813,
	Transform_INTERNAL_get_localRotation_m2621,
	Transform_INTERNAL_set_localRotation_m2622,
	Transform_get_localScale_m1807,
	Transform_set_localScale_m237,
	Transform_INTERNAL_get_localScale_m2623,
	Transform_INTERNAL_set_localScale_m2624,
	Transform_get_parent_m1506,
	Transform_set_parent_m235,
	Transform_get_parentInternal_m2625,
	Transform_set_parentInternal_m2626,
	Transform_SetParent_m198,
	Transform_SetParent_m2627,
	Transform_get_worldToLocalMatrix_m1867,
	Transform_INTERNAL_get_worldToLocalMatrix_m2628,
	Transform_TransformPoint_m1888,
	Transform_INTERNAL_CALL_TransformPoint_m2629,
	Transform_InverseTransformPoint_m1766,
	Transform_INTERNAL_CALL_InverseTransformPoint_m2630,
	Transform_get_childCount_m228,
	Transform_SetAsFirstSibling_m1796,
	Transform_GetEnumerator_m2631,
	Transform_GetChild_m225,
	Time_get_deltaTime_m212,
	Time_get_unscaledTime_m1552,
	Time_get_unscaledDeltaTime_m1600,
	Random_Range_m195,
	Random_RandomRangeInt_m2632,
	YieldInstruction__ctor_m2633,
	PlayerPrefsException__ctor_m2634,
	PlayerPrefs_TrySetInt_m2635,
	PlayerPrefs_TrySetSetString_m2636,
	PlayerPrefs_SetInt_m246,
	PlayerPrefs_SetString_m220,
	PlayerPrefs_GetString_m2637,
	PlayerPrefs_GetString_m201,
	PlayerPrefs_HasKey_m254,
	Particle_get_position_m2638,
	Particle_set_position_m2639,
	Particle_get_velocity_m2640,
	Particle_set_velocity_m2641,
	Particle_get_energy_m2642,
	Particle_set_energy_m2643,
	Particle_get_startEnergy_m2644,
	Particle_set_startEnergy_m2645,
	Particle_get_size_m2646,
	Particle_set_size_m2647,
	Particle_get_rotation_m2648,
	Particle_set_rotation_m2649,
	Particle_get_angularVelocity_m2650,
	Particle_set_angularVelocity_m2651,
	Particle_get_color_m2652,
	Particle_set_color_m2653,
	Physics_Internal_Raycast_m2654,
	Physics_INTERNAL_CALL_Internal_Raycast_m2655,
	Physics_Raycast_m2656,
	Physics_Raycast_m1668,
	Physics_RaycastAll_m1588,
	Physics_RaycastAll_m2657,
	Physics_INTERNAL_CALL_RaycastAll_m2658,
	RaycastHit_get_point_m1593,
	RaycastHit_get_normal_m1594,
	RaycastHit_get_distance_m1592,
	RaycastHit_get_collider_m1591,
	Physics2D__cctor_m2659,
	Physics2D_Internal_Linecast_m2660,
	Physics2D_INTERNAL_CALL_Internal_Linecast_m2661,
	Physics2D_Linecast_m268,
	Physics2D_Linecast_m2662,
	Physics2D_Internal_Raycast_m2663,
	Physics2D_INTERNAL_CALL_Internal_Raycast_m2664,
	Physics2D_Raycast_m1669,
	Physics2D_Raycast_m2665,
	Physics2D_RaycastAll_m1576,
	Physics2D_INTERNAL_CALL_RaycastAll_m2666,
	RaycastHit2D_get_point_m1581,
	RaycastHit2D_get_normal_m1582,
	RaycastHit2D_get_fraction_m1670,
	RaycastHit2D_get_collider_m1577,
	RaycastHit2D_get_rigidbody_m2667,
	RaycastHit2D_get_transform_m1579,
	Rigidbody2D_get_position_m259,
	Rigidbody2D_INTERNAL_get_position_m2668,
	Rigidbody2D_MovePosition_m263,
	Rigidbody2D_INTERNAL_CALL_MovePosition_m2669,
	Collider2D_get_attachedRigidbody_m2670,
	AudioConfigurationChangeHandler__ctor_m2671,
	AudioConfigurationChangeHandler_Invoke_m2672,
	AudioConfigurationChangeHandler_BeginInvoke_m2673,
	AudioConfigurationChangeHandler_EndInvoke_m2674,
	AudioSettings_InvokeOnAudioConfigurationChanged_m2675,
	PCMReaderCallback__ctor_m2676,
	PCMReaderCallback_Invoke_m2677,
	PCMReaderCallback_BeginInvoke_m2678,
	PCMReaderCallback_EndInvoke_m2679,
	PCMSetPositionCallback__ctor_m2680,
	PCMSetPositionCallback_Invoke_m2681,
	PCMSetPositionCallback_BeginInvoke_m2682,
	PCMSetPositionCallback_EndInvoke_m2683,
	AudioClip_InvokePCMReaderCallback_Internal_m2684,
	AudioClip_InvokePCMSetPositionCallback_Internal_m2685,
	WebCamDevice_get_name_m2686,
	WebCamDevice_get_isFrontFacing_m2687,
	AnimationEvent__ctor_m2688,
	AnimationEvent_get_data_m2689,
	AnimationEvent_set_data_m2690,
	AnimationEvent_get_stringParameter_m2691,
	AnimationEvent_set_stringParameter_m2692,
	AnimationEvent_get_floatParameter_m2693,
	AnimationEvent_set_floatParameter_m2694,
	AnimationEvent_get_intParameter_m2695,
	AnimationEvent_set_intParameter_m2696,
	AnimationEvent_get_objectReferenceParameter_m2697,
	AnimationEvent_set_objectReferenceParameter_m2698,
	AnimationEvent_get_functionName_m2699,
	AnimationEvent_set_functionName_m2700,
	AnimationEvent_get_time_m2701,
	AnimationEvent_set_time_m2702,
	AnimationEvent_get_messageOptions_m2703,
	AnimationEvent_set_messageOptions_m2704,
	AnimationEvent_get_isFiredByLegacy_m2705,
	AnimationEvent_get_isFiredByAnimator_m2706,
	AnimationEvent_get_animationState_m2707,
	AnimationEvent_get_animatorStateInfo_m2708,
	AnimationEvent_get_animatorClipInfo_m2709,
	AnimationEvent_GetHash_m2710,
	AnimationCurve__ctor_m2711,
	AnimationCurve__ctor_m2712,
	AnimationCurve_Cleanup_m2713,
	AnimationCurve_Finalize_m2714,
	AnimationCurve_Init_m2715,
	AnimatorStateInfo_IsName_m2716,
	AnimatorStateInfo_get_fullPathHash_m2717,
	AnimatorStateInfo_get_nameHash_m2718,
	AnimatorStateInfo_get_shortNameHash_m2719,
	AnimatorStateInfo_get_normalizedTime_m2720,
	AnimatorStateInfo_get_length_m2721,
	AnimatorStateInfo_get_tagHash_m2722,
	AnimatorStateInfo_IsTag_m2723,
	AnimatorStateInfo_get_loop_m2724,
	AnimatorTransitionInfo_IsName_m2725,
	AnimatorTransitionInfo_IsUserName_m2726,
	AnimatorTransitionInfo_get_fullPathHash_m2727,
	AnimatorTransitionInfo_get_nameHash_m2728,
	AnimatorTransitionInfo_get_userNameHash_m2729,
	AnimatorTransitionInfo_get_normalizedTime_m2730,
	AnimatorTransitionInfo_get_anyState_m2731,
	AnimatorTransitionInfo_get_entry_m2732,
	AnimatorTransitionInfo_get_exit_m2733,
	Animator_SetTrigger_m1895,
	Animator_ResetTrigger_m1894,
	Animator_get_runtimeAnimatorController_m1893,
	Animator_StringToHash_m2734,
	Animator_SetTriggerString_m2735,
	Animator_ResetTriggerString_m2736,
	HumanBone_get_boneName_m2737,
	HumanBone_set_boneName_m2738,
	HumanBone_get_humanName_m2739,
	HumanBone_set_humanName_m2740,
	CharacterInfo_get_advance_m2741,
	CharacterInfo_get_glyphWidth_m2742,
	CharacterInfo_get_glyphHeight_m2743,
	CharacterInfo_get_bearing_m2744,
	CharacterInfo_get_minY_m2745,
	CharacterInfo_get_maxY_m2746,
	CharacterInfo_get_minX_m2747,
	CharacterInfo_get_maxX_m2748,
	CharacterInfo_get_uvBottomLeftUnFlipped_m2749,
	CharacterInfo_get_uvBottomRightUnFlipped_m2750,
	CharacterInfo_get_uvTopRightUnFlipped_m2751,
	CharacterInfo_get_uvTopLeftUnFlipped_m2752,
	CharacterInfo_get_uvBottomLeft_m2753,
	CharacterInfo_get_uvBottomRight_m2754,
	CharacterInfo_get_uvTopRight_m2755,
	CharacterInfo_get_uvTopLeft_m2756,
	FontTextureRebuildCallback__ctor_m2757,
	FontTextureRebuildCallback_Invoke_m2758,
	FontTextureRebuildCallback_BeginInvoke_m2759,
	FontTextureRebuildCallback_EndInvoke_m2760,
	Font_add_textureRebuilt_m1617,
	Font_remove_textureRebuilt_m2761,
	Font_get_material_m1905,
	Font_HasCharacter_m1778,
	Font_InvokeTextureRebuilt_Internal_m2762,
	Font_get_dynamic_m1908,
	Font_get_fontSize_m1910,
	TextGenerator__ctor_m1734,
	TextGenerator__ctor_m1903,
	TextGenerator_System_IDisposable_Dispose_m2763,
	TextGenerator_Init_m2764,
	TextGenerator_Dispose_cpp_m2765,
	TextGenerator_Populate_Internal_m2766,
	TextGenerator_Populate_Internal_cpp_m2767,
	TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m2768,
	TextGenerator_get_rectExtents_m1794,
	TextGenerator_INTERNAL_get_rectExtents_m2769,
	TextGenerator_get_vertexCount_m2770,
	TextGenerator_GetVerticesInternal_m2771,
	TextGenerator_GetVerticesArray_m2772,
	TextGenerator_get_characterCount_m2773,
	TextGenerator_get_characterCountVisible_m1772,
	TextGenerator_GetCharactersInternal_m2774,
	TextGenerator_GetCharactersArray_m2775,
	TextGenerator_get_lineCount_m1771,
	TextGenerator_GetLinesInternal_m2776,
	TextGenerator_GetLinesArray_m2777,
	TextGenerator_get_fontSizeUsedForBestFit_m1818,
	TextGenerator_Finalize_m2778,
	TextGenerator_ValidatedSettings_m2779,
	TextGenerator_Invalidate_m1907,
	TextGenerator_GetCharacters_m2780,
	TextGenerator_GetLines_m2781,
	TextGenerator_GetVertices_m2782,
	TextGenerator_GetPreferredWidth_m1913,
	TextGenerator_GetPreferredHeight_m1914,
	TextGenerator_Populate_m1793,
	TextGenerator_PopulateAlways_m2783,
	TextGenerator_get_verts_m1911,
	TextGenerator_get_characters_m1773,
	TextGenerator_get_lines_m1770,
	WillRenderCanvases__ctor_m1603,
	WillRenderCanvases_Invoke_m2784,
	WillRenderCanvases_BeginInvoke_m2785,
	WillRenderCanvases_EndInvoke_m2786,
	Canvas_add_willRenderCanvases_m1604,
	Canvas_remove_willRenderCanvases_m2787,
	Canvas_get_renderMode_m1661,
	Canvas_get_isRootCanvas_m1926,
	Canvas_get_worldCamera_m1677,
	Canvas_get_scaleFactor_m1909,
	Canvas_set_scaleFactor_m1930,
	Canvas_get_referencePixelsPerUnit_m1694,
	Canvas_set_referencePixelsPerUnit_m1931,
	Canvas_get_pixelPerfect_m1647,
	Canvas_get_renderOrder_m1663,
	Canvas_get_sortingOrder_m1662,
	Canvas_get_cachedSortingLayerValue_m1676,
	Canvas_GetDefaultCanvasMaterial_m1623,
	Canvas_GetDefaultCanvasTextMaterial_m1904,
	Canvas_SendWillRenderCanvases_m2788,
	Canvas_ForceUpdateCanvases_m1853,
	CanvasGroup_get_interactable_m1884,
	CanvasGroup_get_blocksRaycasts_m2789,
	CanvasGroup_get_ignoreParentGroups_m1646,
	CanvasGroup_IsRaycastLocationValid_m2790,
	UIVertex__cctor_m2791,
	CanvasRenderer_SetColor_m1652,
	CanvasRenderer_INTERNAL_CALL_SetColor_m2792,
	CanvasRenderer_GetColor_m1650,
	CanvasRenderer_set_isMask_m1970,
	CanvasRenderer_SetMaterial_m1639,
	CanvasRenderer_SetVertices_m1637,
	CanvasRenderer_SetVerticesInternal_m2793,
	CanvasRenderer_SetVertices_m1753,
	CanvasRenderer_SetVerticesInternalArray_m2794,
	CanvasRenderer_Clear_m1632,
	CanvasRenderer_get_absoluteDepth_m1625,
	RectTransformUtility__cctor_m2795,
	RectTransformUtility_RectangleContainsScreenPoint_m1678,
	RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m2796,
	RectTransformUtility_PixelAdjustPoint_m1648,
	RectTransformUtility_PixelAdjustPoint_m2797,
	RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m2798,
	RectTransformUtility_PixelAdjustRect_m1649,
	RectTransformUtility_ScreenPointToWorldPointInRectangle_m2799,
	RectTransformUtility_ScreenPointToLocalPointInRectangle_m1718,
	RectTransformUtility_ScreenPointToRay_m2800,
	RectTransformUtility_FlipLayoutOnAxis_m1848,
	RectTransformUtility_FlipLayoutAxes_m1847,
	RectTransformUtility_GetTransposed_m2801,
	Request__ctor_m2802,
	Request_get_sourceId_m2803,
	Request_get_appId_m2804,
	Request_get_domain_m2805,
	Request_ToString_m2806,
	ResponseBase__ctor_m2807,
	ResponseBase_ParseJSONString_m2808,
	ResponseBase_ParseJSONInt32_m2809,
	ResponseBase_ParseJSONUInt16_m2810,
	ResponseBase_ParseJSONUInt64_m2811,
	ResponseBase_ParseJSONBool_m2812,
	Response__ctor_m2813,
	Response_get_success_m2814,
	Response_set_success_m2815,
	Response_get_extendedInfo_m2816,
	Response_set_extendedInfo_m2817,
	Response_ToString_m2818,
	Response_Parse_m2819,
	BasicResponse__ctor_m2820,
	CreateMatchRequest__ctor_m2821,
	CreateMatchRequest_get_name_m2822,
	CreateMatchRequest_set_name_m2823,
	CreateMatchRequest_get_size_m2824,
	CreateMatchRequest_set_size_m2825,
	CreateMatchRequest_get_advertise_m2826,
	CreateMatchRequest_set_advertise_m2827,
	CreateMatchRequest_get_password_m2828,
	CreateMatchRequest_set_password_m2829,
	CreateMatchRequest_get_matchAttributes_m2830,
	CreateMatchRequest_ToString_m2831,
	CreateMatchResponse__ctor_m2832,
	CreateMatchResponse_get_address_m2833,
	CreateMatchResponse_set_address_m2834,
	CreateMatchResponse_get_port_m2835,
	CreateMatchResponse_set_port_m2836,
	CreateMatchResponse_get_networkId_m2837,
	CreateMatchResponse_set_networkId_m2838,
	CreateMatchResponse_get_accessTokenString_m2839,
	CreateMatchResponse_set_accessTokenString_m2840,
	CreateMatchResponse_get_nodeId_m2841,
	CreateMatchResponse_set_nodeId_m2842,
	CreateMatchResponse_get_usingRelay_m2843,
	CreateMatchResponse_set_usingRelay_m2844,
	CreateMatchResponse_ToString_m2845,
	CreateMatchResponse_Parse_m2846,
	JoinMatchRequest__ctor_m2847,
	JoinMatchRequest_get_networkId_m2848,
	JoinMatchRequest_set_networkId_m2849,
	JoinMatchRequest_get_password_m2850,
	JoinMatchRequest_set_password_m2851,
	JoinMatchRequest_ToString_m2852,
	JoinMatchResponse__ctor_m2853,
	JoinMatchResponse_get_address_m2854,
	JoinMatchResponse_set_address_m2855,
	JoinMatchResponse_get_port_m2856,
	JoinMatchResponse_set_port_m2857,
	JoinMatchResponse_get_networkId_m2858,
	JoinMatchResponse_set_networkId_m2859,
	JoinMatchResponse_get_accessTokenString_m2860,
	JoinMatchResponse_set_accessTokenString_m2861,
	JoinMatchResponse_get_nodeId_m2862,
	JoinMatchResponse_set_nodeId_m2863,
	JoinMatchResponse_get_usingRelay_m2864,
	JoinMatchResponse_set_usingRelay_m2865,
	JoinMatchResponse_ToString_m2866,
	JoinMatchResponse_Parse_m2867,
	DestroyMatchRequest__ctor_m2868,
	DestroyMatchRequest_get_networkId_m2869,
	DestroyMatchRequest_set_networkId_m2870,
	DestroyMatchRequest_ToString_m2871,
	DropConnectionRequest__ctor_m2872,
	DropConnectionRequest_get_networkId_m2873,
	DropConnectionRequest_set_networkId_m2874,
	DropConnectionRequest_get_nodeId_m2875,
	DropConnectionRequest_set_nodeId_m2876,
	DropConnectionRequest_ToString_m2877,
	ListMatchRequest__ctor_m2878,
	ListMatchRequest_get_pageSize_m2879,
	ListMatchRequest_set_pageSize_m2880,
	ListMatchRequest_get_pageNum_m2881,
	ListMatchRequest_set_pageNum_m2882,
	ListMatchRequest_get_nameFilter_m2883,
	ListMatchRequest_set_nameFilter_m2884,
	ListMatchRequest_get_includePasswordMatches_m2885,
	ListMatchRequest_get_matchAttributeFilterLessThan_m2886,
	ListMatchRequest_get_matchAttributeFilterGreaterThan_m2887,
	ListMatchRequest_ToString_m2888,
	MatchDirectConnectInfo__ctor_m2889,
	MatchDirectConnectInfo_get_nodeId_m2890,
	MatchDirectConnectInfo_set_nodeId_m2891,
	MatchDirectConnectInfo_get_publicAddress_m2892,
	MatchDirectConnectInfo_set_publicAddress_m2893,
	MatchDirectConnectInfo_get_privateAddress_m2894,
	MatchDirectConnectInfo_set_privateAddress_m2895,
	MatchDirectConnectInfo_ToString_m2896,
	MatchDirectConnectInfo_Parse_m2897,
	MatchDesc__ctor_m2898,
	MatchDesc_get_networkId_m2899,
	MatchDesc_set_networkId_m2900,
	MatchDesc_get_name_m2901,
	MatchDesc_set_name_m2902,
	MatchDesc_get_averageEloScore_m2903,
	MatchDesc_get_maxSize_m2904,
	MatchDesc_set_maxSize_m2905,
	MatchDesc_get_currentSize_m2906,
	MatchDesc_set_currentSize_m2907,
	MatchDesc_get_isPrivate_m2908,
	MatchDesc_set_isPrivate_m2909,
	MatchDesc_get_matchAttributes_m2910,
	MatchDesc_get_hostNodeId_m2911,
	MatchDesc_get_directConnectInfos_m2912,
	MatchDesc_set_directConnectInfos_m2913,
	MatchDesc_ToString_m2914,
	MatchDesc_Parse_m2915,
	ListMatchResponse__ctor_m2916,
	ListMatchResponse_get_matches_m2917,
	ListMatchResponse_set_matches_m2918,
	ListMatchResponse_ToString_m2919,
	ListMatchResponse_Parse_m2920,
	NetworkAccessToken__ctor_m2921,
	NetworkAccessToken_GetByteString_m2922,
	Utility__cctor_m2923,
	Utility_GetSourceID_m2924,
	Utility_SetAppID_m2925,
	Utility_GetAppID_m2926,
	Utility_GetAccessTokenForNetwork_m2927,
	NetworkMatch__ctor_m2928,
	NetworkMatch_get_baseUri_m2929,
	NetworkMatch_set_baseUri_m2930,
	NetworkMatch_SetProgramAppID_m2931,
	NetworkMatch_CreateMatch_m2932,
	NetworkMatch_CreateMatch_m2933,
	NetworkMatch_JoinMatch_m2934,
	NetworkMatch_JoinMatch_m2935,
	NetworkMatch_DestroyMatch_m2936,
	NetworkMatch_DestroyMatch_m2937,
	NetworkMatch_DropConnection_m2938,
	NetworkMatch_DropConnection_m2939,
	NetworkMatch_ListMatches_m2940,
	NetworkMatch_ListMatches_m2941,
	JsonArray__ctor_m2942,
	JsonArray_ToString_m2943,
	JsonObject__ctor_m2944,
	JsonObject_System_Collections_IEnumerable_GetEnumerator_m2945,
	JsonObject_Add_m2946,
	JsonObject_get_Keys_m2947,
	JsonObject_TryGetValue_m2948,
	JsonObject_get_Values_m2949,
	JsonObject_get_Item_m2950,
	JsonObject_set_Item_m2951,
	JsonObject_Add_m2952,
	JsonObject_Clear_m2953,
	JsonObject_Contains_m2954,
	JsonObject_CopyTo_m2955,
	JsonObject_get_Count_m2956,
	JsonObject_get_IsReadOnly_m2957,
	JsonObject_Remove_m2958,
	JsonObject_GetEnumerator_m2959,
	JsonObject_ToString_m2960,
	SimpleJson_TryDeserializeObject_m2961,
	SimpleJson_SerializeObject_m2962,
	SimpleJson_SerializeObject_m2963,
	SimpleJson_ParseObject_m2964,
	SimpleJson_ParseArray_m2965,
	SimpleJson_ParseValue_m2966,
	SimpleJson_ParseString_m2967,
	SimpleJson_ConvertFromUtf32_m2968,
	SimpleJson_ParseNumber_m2969,
	SimpleJson_GetLastIndexOfNumber_m2970,
	SimpleJson_EatWhitespace_m2971,
	SimpleJson_LookAhead_m2972,
	SimpleJson_NextToken_m2973,
	SimpleJson_SerializeValue_m2974,
	SimpleJson_SerializeObject_m2975,
	SimpleJson_SerializeArray_m2976,
	SimpleJson_SerializeString_m2977,
	SimpleJson_SerializeNumber_m2978,
	SimpleJson_IsNumeric_m2979,
	SimpleJson_get_CurrentJsonSerializerStrategy_m2980,
	SimpleJson_get_PocoJsonSerializerStrategy_m2981,
	PocoJsonSerializerStrategy__ctor_m2982,
	PocoJsonSerializerStrategy__cctor_m2983,
	PocoJsonSerializerStrategy_MapClrMemberNameToJsonFieldName_m2984,
	PocoJsonSerializerStrategy_ContructorDelegateFactory_m2985,
	PocoJsonSerializerStrategy_GetterValueFactory_m2986,
	PocoJsonSerializerStrategy_SetterValueFactory_m2987,
	PocoJsonSerializerStrategy_TrySerializeNonPrimitiveObject_m2988,
	PocoJsonSerializerStrategy_SerializeEnum_m2989,
	PocoJsonSerializerStrategy_TrySerializeKnownTypes_m2990,
	PocoJsonSerializerStrategy_TrySerializeUnknownTypes_m2991,
	GetDelegate__ctor_m2992,
	GetDelegate_Invoke_m2993,
	GetDelegate_BeginInvoke_m2994,
	GetDelegate_EndInvoke_m2995,
	SetDelegate__ctor_m2996,
	SetDelegate_Invoke_m2997,
	SetDelegate_BeginInvoke_m2998,
	SetDelegate_EndInvoke_m2999,
	ConstructorDelegate__ctor_m3000,
	ConstructorDelegate_Invoke_m3001,
	ConstructorDelegate_BeginInvoke_m3002,
	ConstructorDelegate_EndInvoke_m3003,
	U3CGetConstructorByReflectionU3Ec__AnonStorey1__ctor_m3004,
	U3CGetConstructorByReflectionU3Ec__AnonStorey1_U3CU3Em__0_m3005,
	U3CGetGetMethodByReflectionU3Ec__AnonStorey2__ctor_m3006,
	U3CGetGetMethodByReflectionU3Ec__AnonStorey2_U3CU3Em__1_m3007,
	U3CGetGetMethodByReflectionU3Ec__AnonStorey3__ctor_m3008,
	U3CGetGetMethodByReflectionU3Ec__AnonStorey3_U3CU3Em__2_m3009,
	U3CGetSetMethodByReflectionU3Ec__AnonStorey4__ctor_m3010,
	U3CGetSetMethodByReflectionU3Ec__AnonStorey4_U3CU3Em__3_m3011,
	U3CGetSetMethodByReflectionU3Ec__AnonStorey5__ctor_m3012,
	U3CGetSetMethodByReflectionU3Ec__AnonStorey5_U3CU3Em__4_m3013,
	ReflectionUtils__cctor_m3014,
	ReflectionUtils_GetConstructors_m3015,
	ReflectionUtils_GetConstructorInfo_m3016,
	ReflectionUtils_GetProperties_m3017,
	ReflectionUtils_GetFields_m3018,
	ReflectionUtils_GetGetterMethodInfo_m3019,
	ReflectionUtils_GetSetterMethodInfo_m3020,
	ReflectionUtils_GetContructor_m3021,
	ReflectionUtils_GetConstructorByReflection_m3022,
	ReflectionUtils_GetConstructorByReflection_m3023,
	ReflectionUtils_GetGetMethod_m3024,
	ReflectionUtils_GetGetMethod_m3025,
	ReflectionUtils_GetGetMethodByReflection_m3026,
	ReflectionUtils_GetGetMethodByReflection_m3027,
	ReflectionUtils_GetSetMethod_m3028,
	ReflectionUtils_GetSetMethod_m3029,
	ReflectionUtils_GetSetMethodByReflection_m3030,
	ReflectionUtils_GetSetMethodByReflection_m3031,
	WrapperlessIcall__ctor_m3032,
	IL2CPPStructAlignmentAttribute__ctor_m3033,
	AttributeHelperEngine__cctor_m3034,
	AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m3035,
	AttributeHelperEngine_GetRequiredComponents_m3036,
	AttributeHelperEngine_CheckIsEditorScript_m3037,
	DisallowMultipleComponent__ctor_m3038,
	RequireComponent__ctor_m3039,
	AddComponentMenu__ctor_m3040,
	AddComponentMenu__ctor_m3041,
	ExecuteInEditMode__ctor_m3042,
	HideInInspector__ctor_m3043,
	SetupCoroutine__ctor_m3044,
	SetupCoroutine_InvokeMember_m3045,
	SetupCoroutine_InvokeStatic_m3046,
	WritableAttribute__ctor_m3047,
	AssemblyIsEditorAssembly__ctor_m3048,
	GcUserProfileData_ToUserProfile_m3049,
	GcUserProfileData_AddToArray_m3050,
	GcAchievementDescriptionData_ToAchievementDescription_m3051,
	GcAchievementData_ToAchievement_m3052,
	GcScoreData_ToScore_m3053,
	Resolution_get_width_m3054,
	Resolution_set_width_m3055,
	Resolution_get_height_m3056,
	Resolution_set_height_m3057,
	Resolution_get_refreshRate_m3058,
	Resolution_set_refreshRate_m3059,
	Resolution_ToString_m3060,
	LocalUser__ctor_m3061,
	LocalUser_SetFriends_m3062,
	LocalUser_SetAuthenticated_m3063,
	LocalUser_SetUnderage_m3064,
	LocalUser_get_authenticated_m3065,
	UserProfile__ctor_m3066,
	UserProfile__ctor_m3067,
	UserProfile_ToString_m3068,
	UserProfile_SetUserName_m3069,
	UserProfile_SetUserID_m3070,
	UserProfile_SetImage_m3071,
	UserProfile_get_userName_m3072,
	UserProfile_get_id_m3073,
	UserProfile_get_isFriend_m3074,
	UserProfile_get_state_m3075,
	Achievement__ctor_m3076,
	Achievement__ctor_m3077,
	Achievement__ctor_m3078,
	Achievement_ToString_m3079,
	Achievement_get_id_m3080,
	Achievement_set_id_m3081,
	Achievement_get_percentCompleted_m3082,
	Achievement_set_percentCompleted_m3083,
	Achievement_get_completed_m3084,
	Achievement_get_hidden_m3085,
	Achievement_get_lastReportedDate_m3086,
	AchievementDescription__ctor_m3087,
	AchievementDescription_ToString_m3088,
	AchievementDescription_SetImage_m3089,
	AchievementDescription_get_id_m3090,
	AchievementDescription_set_id_m3091,
	AchievementDescription_get_title_m3092,
	AchievementDescription_get_achievedDescription_m3093,
	AchievementDescription_get_unachievedDescription_m3094,
	AchievementDescription_get_hidden_m3095,
	AchievementDescription_get_points_m3096,
	Score__ctor_m3097,
	Score__ctor_m3098,
	Score_ToString_m3099,
	Score_get_leaderboardID_m3100,
	Score_set_leaderboardID_m3101,
	Score_get_value_m3102,
	Score_set_value_m3103,
	Leaderboard__ctor_m3104,
	Leaderboard_ToString_m3105,
	Leaderboard_SetLocalUserScore_m3106,
	Leaderboard_SetMaxRange_m3107,
	Leaderboard_SetScores_m3108,
	Leaderboard_SetTitle_m3109,
	Leaderboard_GetUserFilter_m3110,
	Leaderboard_get_id_m3111,
	Leaderboard_set_id_m3112,
	Leaderboard_get_userScope_m3113,
	Leaderboard_set_userScope_m3114,
	Leaderboard_get_range_m3115,
	Leaderboard_set_range_m3116,
	Leaderboard_get_timeScope_m3117,
	Leaderboard_set_timeScope_m3118,
	HitInfo_SendMessage_m3119,
	HitInfo_Compare_m3120,
	HitInfo_op_Implicit_m3121,
	SendMouseEvents__cctor_m3122,
	SendMouseEvents_DoSendMouseEvents_m3123,
	SendMouseEvents_SendEvents_m3124,
	Range__ctor_m3125,
	PropertyAttribute__ctor_m3126,
	TooltipAttribute__ctor_m3127,
	SpaceAttribute__ctor_m3128,
	RangeAttribute__ctor_m3129,
	TextAreaAttribute__ctor_m3130,
	SelectionBaseAttribute__ctor_m3131,
	SliderState__ctor_m3132,
	StackTraceUtility__ctor_m3133,
	StackTraceUtility__cctor_m3134,
	StackTraceUtility_SetProjectFolder_m3135,
	StackTraceUtility_ExtractStackTrace_m3136,
	StackTraceUtility_IsSystemStacktraceType_m3137,
	StackTraceUtility_ExtractStringFromException_m3138,
	StackTraceUtility_ExtractStringFromExceptionInternal_m3139,
	StackTraceUtility_PostprocessStacktrace_m3140,
	StackTraceUtility_ExtractFormattedStackTrace_m3141,
	UnityException__ctor_m3142,
	UnityException__ctor_m3143,
	UnityException__ctor_m3144,
	UnityException__ctor_m3145,
	SharedBetweenAnimatorsAttribute__ctor_m3146,
	StateMachineBehaviour__ctor_m3147,
	StateMachineBehaviour_OnStateEnter_m3148,
	StateMachineBehaviour_OnStateUpdate_m3149,
	StateMachineBehaviour_OnStateExit_m3150,
	StateMachineBehaviour_OnStateMove_m3151,
	StateMachineBehaviour_OnStateIK_m3152,
	StateMachineBehaviour_OnStateMachineEnter_m3153,
	StateMachineBehaviour_OnStateMachineExit_m3154,
	TextEditor__ctor_m1754,
	TextEditor_ClearCursorPos_m3155,
	TextEditor_OnFocus_m1758,
	TextEditor_SelectAll_m3156,
	TextEditor_DeleteSelection_m3157,
	TextEditor_ReplaceSelection_m3158,
	TextEditor_UpdateScrollOffset_m3159,
	TextEditor_Copy_m1759,
	TextEditor_ReplaceNewlinesWithSpaces_m3160,
	TextEditor_Paste_m1755,
	TextGenerationSettings_CompareColors_m3161,
	TextGenerationSettings_CompareVector2_m3162,
	TextGenerationSettings_Equals_m3163,
	TrackedReference_Equals_m3164,
	TrackedReference_GetHashCode_m3165,
	TrackedReference_op_Equality_m3166,
	ArgumentCache__ctor_m3167,
	ArgumentCache_get_unityObjectArgument_m3168,
	ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3169,
	ArgumentCache_get_intArgument_m3170,
	ArgumentCache_get_floatArgument_m3171,
	ArgumentCache_get_stringArgument_m3172,
	ArgumentCache_get_boolArgument_m3173,
	ArgumentCache_TidyAssemblyTypeName_m3174,
	ArgumentCache_OnBeforeSerialize_m3175,
	ArgumentCache_OnAfterDeserialize_m3176,
	BaseInvokableCall__ctor_m3177,
	BaseInvokableCall__ctor_m3178,
	BaseInvokableCall_AllowInvoke_m3179,
	InvokableCall__ctor_m3180,
	InvokableCall_Invoke_m3181,
	InvokableCall_Find_m3182,
	PersistentCall__ctor_m3183,
	PersistentCall_get_target_m3184,
	PersistentCall_get_methodName_m3185,
	PersistentCall_get_mode_m3186,
	PersistentCall_get_arguments_m3187,
	PersistentCall_IsValid_m3188,
	PersistentCall_GetRuntimeCall_m3189,
	PersistentCall_GetObjectCall_m3190,
	PersistentCallGroup__ctor_m3191,
	PersistentCallGroup_Initialize_m3192,
	InvokableCallList__ctor_m3193,
	InvokableCallList_AddPersistentInvokableCall_m3194,
	InvokableCallList_AddListener_m3195,
	InvokableCallList_RemoveListener_m3196,
	InvokableCallList_ClearPersistent_m3197,
	InvokableCallList_Invoke_m3198,
	UnityEventBase__ctor_m3199,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3200,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3201,
	UnityEventBase_FindMethod_m3202,
	UnityEventBase_FindMethod_m3203,
	UnityEventBase_DirtyPersistentCalls_m3204,
	UnityEventBase_RebuildPersistentCallsIfNeeded_m3205,
	UnityEventBase_AddCall_m3206,
	UnityEventBase_RemoveListener_m3207,
	UnityEventBase_Invoke_m3208,
	UnityEventBase_ToString_m3209,
	UnityEventBase_GetValidMethodInfo_m3210,
	UnityEvent__ctor_m1599,
	UnityEvent_FindMethod_Impl_m3211,
	UnityEvent_GetDelegate_m3212,
	UnityEvent_Invoke_m1601,
	UserAuthorizationDialog__ctor_m3213,
	UserAuthorizationDialog_Start_m3214,
	UserAuthorizationDialog_OnGUI_m3215,
	UserAuthorizationDialog_DoUserAuthorizationDialog_m3216,
	DefaultValueAttribute__ctor_m3217,
	DefaultValueAttribute_get_Value_m3218,
	DefaultValueAttribute_Equals_m3219,
	DefaultValueAttribute_GetHashCode_m3220,
	ExcludeFromDocsAttribute__ctor_m3221,
	FormerlySerializedAsAttribute__ctor_m3222,
	TypeInferenceRuleAttribute__ctor_m3223,
	TypeInferenceRuleAttribute__ctor_m3224,
	TypeInferenceRuleAttribute_ToString_m3225,
	GenericStack__ctor_m3226,
	UnityAction__ctor_m1752,
	UnityAction_Invoke_m1624,
	UnityAction_BeginInvoke_m3227,
	UnityAction_EndInvoke_m3228,
	ExtensionAttribute__ctor_m3400,
	Locale_GetText_m3401,
	Locale_GetText_m3402,
	KeyBuilder_get_Rng_m3403,
	KeyBuilder_Key_m3404,
	KeyBuilder_IV_m3405,
	SymmetricTransform__ctor_m3406,
	SymmetricTransform_System_IDisposable_Dispose_m3407,
	SymmetricTransform_Finalize_m3408,
	SymmetricTransform_Dispose_m3409,
	SymmetricTransform_get_CanReuseTransform_m3410,
	SymmetricTransform_Transform_m3411,
	SymmetricTransform_CBC_m3412,
	SymmetricTransform_CFB_m3413,
	SymmetricTransform_OFB_m3414,
	SymmetricTransform_CTS_m3415,
	SymmetricTransform_CheckInput_m3416,
	SymmetricTransform_TransformBlock_m3417,
	SymmetricTransform_get_KeepLastBlock_m3418,
	SymmetricTransform_InternalTransformBlock_m3419,
	SymmetricTransform_Random_m3420,
	SymmetricTransform_ThrowBadPaddingException_m3421,
	SymmetricTransform_FinalEncrypt_m3422,
	SymmetricTransform_FinalDecrypt_m3423,
	SymmetricTransform_TransformFinalBlock_m3424,
	Check_SourceAndPredicate_m3425,
	Aes__ctor_m3426,
	AesManaged__ctor_m3427,
	AesManaged_GenerateIV_m3428,
	AesManaged_GenerateKey_m3429,
	AesManaged_CreateDecryptor_m3430,
	AesManaged_CreateEncryptor_m3431,
	AesManaged_get_IV_m3432,
	AesManaged_set_IV_m3433,
	AesManaged_get_Key_m3434,
	AesManaged_set_Key_m3435,
	AesManaged_get_KeySize_m3436,
	AesManaged_set_KeySize_m3437,
	AesManaged_CreateDecryptor_m3438,
	AesManaged_CreateEncryptor_m3439,
	AesManaged_Dispose_m3440,
	AesTransform__ctor_m3441,
	AesTransform__cctor_m3442,
	AesTransform_ECB_m3443,
	AesTransform_SubByte_m3444,
	AesTransform_Encrypt128_m3445,
	AesTransform_Decrypt128_m3446,
	Locale_GetText_m3467,
	ModulusRing__ctor_m3468,
	ModulusRing_BarrettReduction_m3469,
	ModulusRing_Multiply_m3470,
	ModulusRing_Difference_m3471,
	ModulusRing_Pow_m3472,
	ModulusRing_Pow_m3473,
	Kernel_AddSameSign_m3474,
	Kernel_Subtract_m3475,
	Kernel_MinusEq_m3476,
	Kernel_PlusEq_m3477,
	Kernel_Compare_m3478,
	Kernel_SingleByteDivideInPlace_m3479,
	Kernel_DwordMod_m3480,
	Kernel_DwordDivMod_m3481,
	Kernel_multiByteDivide_m3482,
	Kernel_LeftShift_m3483,
	Kernel_RightShift_m3484,
	Kernel_Multiply_m3485,
	Kernel_MultiplyMod2p32pmod_m3486,
	Kernel_modInverse_m3487,
	Kernel_modInverse_m3488,
	BigInteger__ctor_m3489,
	BigInteger__ctor_m3490,
	BigInteger__ctor_m3491,
	BigInteger__ctor_m3492,
	BigInteger__ctor_m3493,
	BigInteger__cctor_m3494,
	BigInteger_get_Rng_m3495,
	BigInteger_GenerateRandom_m3496,
	BigInteger_GenerateRandom_m3497,
	BigInteger_BitCount_m3498,
	BigInteger_TestBit_m3499,
	BigInteger_SetBit_m3500,
	BigInteger_SetBit_m3501,
	BigInteger_LowestSetBit_m3502,
	BigInteger_GetBytes_m3503,
	BigInteger_ToString_m3504,
	BigInteger_ToString_m3505,
	BigInteger_Normalize_m3506,
	BigInteger_Clear_m3507,
	BigInteger_GetHashCode_m3508,
	BigInteger_ToString_m3509,
	BigInteger_Equals_m3510,
	BigInteger_ModInverse_m3511,
	BigInteger_ModPow_m3512,
	BigInteger_GeneratePseudoPrime_m3513,
	BigInteger_Incr2_m3514,
	BigInteger_op_Implicit_m3515,
	BigInteger_op_Implicit_m3516,
	BigInteger_op_Addition_m3517,
	BigInteger_op_Subtraction_m3518,
	BigInteger_op_Modulus_m3519,
	BigInteger_op_Modulus_m3520,
	BigInteger_op_Division_m3521,
	BigInteger_op_Multiply_m3522,
	BigInteger_op_LeftShift_m3523,
	BigInteger_op_RightShift_m3524,
	BigInteger_op_Equality_m3525,
	BigInteger_op_Inequality_m3526,
	BigInteger_op_Equality_m3527,
	BigInteger_op_Inequality_m3528,
	BigInteger_op_GreaterThan_m3529,
	BigInteger_op_LessThan_m3530,
	BigInteger_op_GreaterThanOrEqual_m3531,
	BigInteger_op_LessThanOrEqual_m3532,
	PrimalityTests_GetSPPRounds_m3533,
	PrimalityTests_RabinMillerTest_m3534,
	PrimeGeneratorBase__ctor_m3535,
	PrimeGeneratorBase_get_Confidence_m3536,
	PrimeGeneratorBase_get_PrimalityTest_m3537,
	PrimeGeneratorBase_get_TrialDivisionBounds_m3538,
	SequentialSearchPrimeGeneratorBase__ctor_m3539,
	SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m3540,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m3541,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m3542,
	SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m3543,
	ASN1__ctor_m3544,
	ASN1__ctor_m3545,
	ASN1__ctor_m3546,
	ASN1_get_Count_m3547,
	ASN1_get_Tag_m3548,
	ASN1_get_Length_m3549,
	ASN1_get_Value_m3550,
	ASN1_set_Value_m3551,
	ASN1_CompareArray_m3552,
	ASN1_CompareValue_m3553,
	ASN1_Add_m3554,
	ASN1_GetBytes_m3555,
	ASN1_Decode_m3556,
	ASN1_DecodeTLV_m3557,
	ASN1_get_Item_m3558,
	ASN1_Element_m3559,
	ASN1_ToString_m3560,
	ASN1Convert_FromInt32_m3561,
	ASN1Convert_FromOid_m3562,
	ASN1Convert_ToInt32_m3563,
	ASN1Convert_ToOid_m3564,
	ASN1Convert_ToDateTime_m3565,
	BitConverterLE_GetUIntBytes_m3566,
	BitConverterLE_GetBytes_m3567,
	ContentInfo__ctor_m3568,
	ContentInfo__ctor_m3569,
	ContentInfo__ctor_m3570,
	ContentInfo__ctor_m3571,
	ContentInfo_get_Content_m3572,
	ContentInfo_set_Content_m3573,
	ContentInfo_get_ContentType_m3574,
	EncryptedData__ctor_m3575,
	EncryptedData__ctor_m3576,
	EncryptedData_get_EncryptionAlgorithm_m3577,
	EncryptedData_get_EncryptedContent_m3578,
	ARC4Managed__ctor_m3579,
	ARC4Managed_Finalize_m3580,
	ARC4Managed_Dispose_m3581,
	ARC4Managed_get_Key_m3582,
	ARC4Managed_set_Key_m3583,
	ARC4Managed_get_CanReuseTransform_m3584,
	ARC4Managed_CreateEncryptor_m3585,
	ARC4Managed_CreateDecryptor_m3586,
	ARC4Managed_GenerateIV_m3587,
	ARC4Managed_GenerateKey_m3588,
	ARC4Managed_KeySetup_m3589,
	ARC4Managed_CheckInput_m3590,
	ARC4Managed_TransformBlock_m3591,
	ARC4Managed_InternalTransformBlock_m3592,
	ARC4Managed_TransformFinalBlock_m3593,
	CryptoConvert_ToHex_m3594,
	KeyBuilder_get_Rng_m3595,
	KeyBuilder_Key_m3596,
	MD2__ctor_m3597,
	MD2_Create_m3598,
	MD2_Create_m3599,
	MD2Managed__ctor_m3600,
	MD2Managed__cctor_m3601,
	MD2Managed_Padding_m3602,
	MD2Managed_Initialize_m3603,
	MD2Managed_HashCore_m3604,
	MD2Managed_HashFinal_m3605,
	MD2Managed_MD2Transform_m3606,
	PKCS1__cctor_m3607,
	PKCS1_Compare_m3608,
	PKCS1_I2OSP_m3609,
	PKCS1_OS2IP_m3610,
	PKCS1_RSASP1_m3611,
	PKCS1_RSAVP1_m3612,
	PKCS1_Sign_v15_m3613,
	PKCS1_Verify_v15_m3614,
	PKCS1_Verify_v15_m3615,
	PKCS1_Encode_v15_m3616,
	PrivateKeyInfo__ctor_m3617,
	PrivateKeyInfo__ctor_m3618,
	PrivateKeyInfo_get_PrivateKey_m3619,
	PrivateKeyInfo_Decode_m3620,
	PrivateKeyInfo_RemoveLeadingZero_m3621,
	PrivateKeyInfo_Normalize_m3622,
	PrivateKeyInfo_DecodeRSA_m3623,
	PrivateKeyInfo_DecodeDSA_m3624,
	EncryptedPrivateKeyInfo__ctor_m3625,
	EncryptedPrivateKeyInfo__ctor_m3626,
	EncryptedPrivateKeyInfo_get_Algorithm_m3627,
	EncryptedPrivateKeyInfo_get_EncryptedData_m3628,
	EncryptedPrivateKeyInfo_get_Salt_m3629,
	EncryptedPrivateKeyInfo_get_IterationCount_m3630,
	EncryptedPrivateKeyInfo_Decode_m3631,
	RC4__ctor_m3632,
	RC4__cctor_m3633,
	RC4_get_IV_m3634,
	RC4_set_IV_m3635,
	KeyGeneratedEventHandler__ctor_m3636,
	KeyGeneratedEventHandler_Invoke_m3637,
	KeyGeneratedEventHandler_BeginInvoke_m3638,
	KeyGeneratedEventHandler_EndInvoke_m3639,
	RSAManaged__ctor_m3640,
	RSAManaged__ctor_m3641,
	RSAManaged_Finalize_m3642,
	RSAManaged_GenerateKeyPair_m3643,
	RSAManaged_get_KeySize_m3644,
	RSAManaged_get_PublicOnly_m3645,
	RSAManaged_DecryptValue_m3646,
	RSAManaged_EncryptValue_m3647,
	RSAManaged_ExportParameters_m3648,
	RSAManaged_ImportParameters_m3649,
	RSAManaged_Dispose_m3650,
	RSAManaged_ToXmlString_m3651,
	RSAManaged_GetPaddedValue_m3652,
	SafeBag__ctor_m3653,
	SafeBag_get_BagOID_m3654,
	SafeBag_get_ASN1_m3655,
	DeriveBytes__ctor_m3656,
	DeriveBytes__cctor_m3657,
	DeriveBytes_set_HashName_m3658,
	DeriveBytes_set_IterationCount_m3659,
	DeriveBytes_set_Password_m3660,
	DeriveBytes_set_Salt_m3661,
	DeriveBytes_Adjust_m3662,
	DeriveBytes_Derive_m3663,
	DeriveBytes_DeriveKey_m3664,
	DeriveBytes_DeriveIV_m3665,
	DeriveBytes_DeriveMAC_m3666,
	PKCS12__ctor_m3667,
	PKCS12__ctor_m3668,
	PKCS12__ctor_m3669,
	PKCS12__cctor_m3670,
	PKCS12_Decode_m3671,
	PKCS12_Finalize_m3672,
	PKCS12_set_Password_m3673,
	PKCS12_get_Keys_m3674,
	PKCS12_get_Certificates_m3675,
	PKCS12_Compare_m3676,
	PKCS12_GetSymmetricAlgorithm_m3677,
	PKCS12_Decrypt_m3678,
	PKCS12_Decrypt_m3679,
	PKCS12_GetExistingParameters_m3680,
	PKCS12_AddPrivateKey_m3681,
	PKCS12_ReadSafeBag_m3682,
	PKCS12_MAC_m3683,
	PKCS12_get_MaximumPasswordLength_m3684,
	X501__cctor_m3685,
	X501_ToString_m3686,
	X501_ToString_m3687,
	X501_AppendEntry_m3688,
	X509Certificate__ctor_m3689,
	X509Certificate__cctor_m3690,
	X509Certificate_Parse_m3691,
	X509Certificate_GetUnsignedBigInteger_m3692,
	X509Certificate_get_DSA_m3693,
	X509Certificate_set_DSA_m3694,
	X509Certificate_get_Extensions_m3695,
	X509Certificate_get_Hash_m3696,
	X509Certificate_get_IssuerName_m3697,
	X509Certificate_get_KeyAlgorithm_m3698,
	X509Certificate_get_KeyAlgorithmParameters_m3699,
	X509Certificate_set_KeyAlgorithmParameters_m3700,
	X509Certificate_get_PublicKey_m3701,
	X509Certificate_get_RSA_m3702,
	X509Certificate_set_RSA_m3703,
	X509Certificate_get_RawData_m3704,
	X509Certificate_get_SerialNumber_m3705,
	X509Certificate_get_Signature_m3706,
	X509Certificate_get_SignatureAlgorithm_m3707,
	X509Certificate_get_SubjectName_m3708,
	X509Certificate_get_ValidFrom_m3709,
	X509Certificate_get_ValidUntil_m3710,
	X509Certificate_get_Version_m3711,
	X509Certificate_get_IsCurrent_m3712,
	X509Certificate_WasCurrent_m3713,
	X509Certificate_VerifySignature_m3714,
	X509Certificate_VerifySignature_m3715,
	X509Certificate_VerifySignature_m3716,
	X509Certificate_get_IsSelfSigned_m3717,
	X509Certificate_GetIssuerName_m3718,
	X509Certificate_GetSubjectName_m3719,
	X509Certificate_GetObjectData_m3720,
	X509Certificate_PEM_m3721,
	X509CertificateEnumerator__ctor_m3722,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m3723,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m3724,
	X509CertificateEnumerator_get_Current_m3725,
	X509CertificateEnumerator_MoveNext_m3726,
	X509CertificateCollection__ctor_m3727,
	X509CertificateCollection__ctor_m3728,
	X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m3729,
	X509CertificateCollection_get_Item_m3730,
	X509CertificateCollection_Add_m3731,
	X509CertificateCollection_AddRange_m3732,
	X509CertificateCollection_Contains_m3733,
	X509CertificateCollection_GetEnumerator_m3734,
	X509CertificateCollection_GetHashCode_m3735,
	X509CertificateCollection_IndexOf_m3736,
	X509CertificateCollection_Remove_m3737,
	X509CertificateCollection_Compare_m3738,
	X509Chain__ctor_m3739,
	X509Chain__ctor_m3740,
	X509Chain_get_Status_m3741,
	X509Chain_get_TrustAnchors_m3742,
	X509Chain_Build_m3743,
	X509Chain_IsValid_m3744,
	X509Chain_FindCertificateParent_m3745,
	X509Chain_FindCertificateRoot_m3746,
	X509Chain_IsTrusted_m3747,
	X509Chain_IsParent_m3748,
	X509CrlEntry__ctor_m3749,
	X509CrlEntry_get_SerialNumber_m3750,
	X509CrlEntry_get_RevocationDate_m3751,
	X509CrlEntry_get_Extensions_m3752,
	X509Crl__ctor_m3753,
	X509Crl_Parse_m3754,
	X509Crl_get_Extensions_m3755,
	X509Crl_get_Hash_m3756,
	X509Crl_get_IssuerName_m3757,
	X509Crl_get_NextUpdate_m3758,
	X509Crl_Compare_m3759,
	X509Crl_GetCrlEntry_m3760,
	X509Crl_GetCrlEntry_m3761,
	X509Crl_GetHashName_m3762,
	X509Crl_VerifySignature_m3763,
	X509Crl_VerifySignature_m3764,
	X509Crl_VerifySignature_m3765,
	X509Extension__ctor_m3766,
	X509Extension__ctor_m3767,
	X509Extension_Decode_m3768,
	X509Extension_Encode_m3769,
	X509Extension_get_Oid_m3770,
	X509Extension_get_Critical_m3771,
	X509Extension_get_Value_m3772,
	X509Extension_Equals_m3773,
	X509Extension_GetHashCode_m3774,
	X509Extension_WriteLine_m3775,
	X509Extension_ToString_m3776,
	X509ExtensionCollection__ctor_m3777,
	X509ExtensionCollection__ctor_m3778,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m3779,
	X509ExtensionCollection_IndexOf_m3780,
	X509ExtensionCollection_get_Item_m3781,
	X509Store__ctor_m3782,
	X509Store_get_Certificates_m3783,
	X509Store_get_Crls_m3784,
	X509Store_Load_m3785,
	X509Store_LoadCertificate_m3786,
	X509Store_LoadCrl_m3787,
	X509Store_CheckStore_m3788,
	X509Store_BuildCertificatesCollection_m3789,
	X509Store_BuildCrlsCollection_m3790,
	X509StoreManager_get_CurrentUser_m3791,
	X509StoreManager_get_LocalMachine_m3792,
	X509StoreManager_get_TrustedRootCertificates_m3793,
	X509Stores__ctor_m3794,
	X509Stores_get_TrustedRoot_m3795,
	X509Stores_Open_m3796,
	AuthorityKeyIdentifierExtension__ctor_m3797,
	AuthorityKeyIdentifierExtension_Decode_m3798,
	AuthorityKeyIdentifierExtension_get_Identifier_m3799,
	AuthorityKeyIdentifierExtension_ToString_m3800,
	BasicConstraintsExtension__ctor_m3801,
	BasicConstraintsExtension_Decode_m3802,
	BasicConstraintsExtension_Encode_m3803,
	BasicConstraintsExtension_get_CertificateAuthority_m3804,
	BasicConstraintsExtension_ToString_m3805,
	ExtendedKeyUsageExtension__ctor_m3806,
	ExtendedKeyUsageExtension_Decode_m3807,
	ExtendedKeyUsageExtension_Encode_m3808,
	ExtendedKeyUsageExtension_get_KeyPurpose_m3809,
	ExtendedKeyUsageExtension_ToString_m3810,
	GeneralNames__ctor_m3811,
	GeneralNames_get_DNSNames_m3812,
	GeneralNames_get_IPAddresses_m3813,
	GeneralNames_ToString_m3814,
	KeyUsageExtension__ctor_m3815,
	KeyUsageExtension_Decode_m3816,
	KeyUsageExtension_Encode_m3817,
	KeyUsageExtension_Support_m3818,
	KeyUsageExtension_ToString_m3819,
	NetscapeCertTypeExtension__ctor_m3820,
	NetscapeCertTypeExtension_Decode_m3821,
	NetscapeCertTypeExtension_Support_m3822,
	NetscapeCertTypeExtension_ToString_m3823,
	SubjectAltNameExtension__ctor_m3824,
	SubjectAltNameExtension_Decode_m3825,
	SubjectAltNameExtension_get_DNSNames_m3826,
	SubjectAltNameExtension_get_IPAddresses_m3827,
	SubjectAltNameExtension_ToString_m3828,
	HMAC__ctor_m3829,
	HMAC_get_Key_m3830,
	HMAC_set_Key_m3831,
	HMAC_Initialize_m3832,
	HMAC_HashFinal_m3833,
	HMAC_HashCore_m3834,
	HMAC_initializePad_m3835,
	MD5SHA1__ctor_m3836,
	MD5SHA1_Initialize_m3837,
	MD5SHA1_HashFinal_m3838,
	MD5SHA1_HashCore_m3839,
	MD5SHA1_CreateSignature_m3840,
	MD5SHA1_VerifySignature_m3841,
	Alert__ctor_m3842,
	Alert__ctor_m3843,
	Alert_get_Level_m3844,
	Alert_get_Description_m3845,
	Alert_get_IsWarning_m3846,
	Alert_get_IsCloseNotify_m3847,
	Alert_inferAlertLevel_m3848,
	Alert_GetAlertMessage_m3849,
	CipherSuite__ctor_m3850,
	CipherSuite__cctor_m3851,
	CipherSuite_get_EncryptionCipher_m3852,
	CipherSuite_get_DecryptionCipher_m3853,
	CipherSuite_get_ClientHMAC_m3854,
	CipherSuite_get_ServerHMAC_m3855,
	CipherSuite_get_CipherAlgorithmType_m3856,
	CipherSuite_get_HashAlgorithmName_m3857,
	CipherSuite_get_HashAlgorithmType_m3858,
	CipherSuite_get_HashSize_m3859,
	CipherSuite_get_ExchangeAlgorithmType_m3860,
	CipherSuite_get_CipherMode_m3861,
	CipherSuite_get_Code_m3862,
	CipherSuite_get_Name_m3863,
	CipherSuite_get_IsExportable_m3864,
	CipherSuite_get_KeyMaterialSize_m3865,
	CipherSuite_get_KeyBlockSize_m3866,
	CipherSuite_get_ExpandedKeyMaterialSize_m3867,
	CipherSuite_get_EffectiveKeyBits_m3868,
	CipherSuite_get_IvSize_m3869,
	CipherSuite_get_Context_m3870,
	CipherSuite_set_Context_m3871,
	CipherSuite_Write_m3872,
	CipherSuite_Write_m3873,
	CipherSuite_InitializeCipher_m3874,
	CipherSuite_EncryptRecord_m3875,
	CipherSuite_DecryptRecord_m3876,
	CipherSuite_CreatePremasterSecret_m3877,
	CipherSuite_PRF_m3878,
	CipherSuite_Expand_m3879,
	CipherSuite_createEncryptionCipher_m3880,
	CipherSuite_createDecryptionCipher_m3881,
	CipherSuiteCollection__ctor_m3882,
	CipherSuiteCollection_System_Collections_IList_get_Item_m3883,
	CipherSuiteCollection_System_Collections_IList_set_Item_m3884,
	CipherSuiteCollection_System_Collections_ICollection_get_IsSynchronized_m3885,
	CipherSuiteCollection_System_Collections_ICollection_get_SyncRoot_m3886,
	CipherSuiteCollection_System_Collections_IEnumerable_GetEnumerator_m3887,
	CipherSuiteCollection_System_Collections_IList_Contains_m3888,
	CipherSuiteCollection_System_Collections_IList_IndexOf_m3889,
	CipherSuiteCollection_System_Collections_IList_Insert_m3890,
	CipherSuiteCollection_System_Collections_IList_Remove_m3891,
	CipherSuiteCollection_System_Collections_IList_RemoveAt_m3892,
	CipherSuiteCollection_System_Collections_IList_Add_m3893,
	CipherSuiteCollection_get_Item_m3894,
	CipherSuiteCollection_get_Item_m3895,
	CipherSuiteCollection_set_Item_m3896,
	CipherSuiteCollection_get_Item_m3897,
	CipherSuiteCollection_get_Count_m3898,
	CipherSuiteCollection_get_IsFixedSize_m3899,
	CipherSuiteCollection_get_IsReadOnly_m3900,
	CipherSuiteCollection_CopyTo_m3901,
	CipherSuiteCollection_Clear_m3902,
	CipherSuiteCollection_IndexOf_m3903,
	CipherSuiteCollection_IndexOf_m3904,
	CipherSuiteCollection_Add_m3905,
	CipherSuiteCollection_add_m3906,
	CipherSuiteCollection_add_m3907,
	CipherSuiteCollection_cultureAwareCompare_m3908,
	CipherSuiteFactory_GetSupportedCiphers_m3909,
	CipherSuiteFactory_GetTls1SupportedCiphers_m3910,
	CipherSuiteFactory_GetSsl3SupportedCiphers_m3911,
	ClientContext__ctor_m3912,
	ClientContext_get_SslStream_m3913,
	ClientContext_get_ClientHelloProtocol_m3914,
	ClientContext_set_ClientHelloProtocol_m3915,
	ClientContext_Clear_m3916,
	ClientRecordProtocol__ctor_m3917,
	ClientRecordProtocol_GetMessage_m3918,
	ClientRecordProtocol_ProcessHandshakeMessage_m3919,
	ClientRecordProtocol_createClientHandshakeMessage_m3920,
	ClientRecordProtocol_createServerHandshakeMessage_m3921,
	ClientSessionInfo__ctor_m3922,
	ClientSessionInfo__cctor_m3923,
	ClientSessionInfo_Finalize_m3924,
	ClientSessionInfo_get_HostName_m3925,
	ClientSessionInfo_get_Id_m3926,
	ClientSessionInfo_get_Valid_m3927,
	ClientSessionInfo_GetContext_m3928,
	ClientSessionInfo_SetContext_m3929,
	ClientSessionInfo_KeepAlive_m3930,
	ClientSessionInfo_Dispose_m3931,
	ClientSessionInfo_Dispose_m3932,
	ClientSessionInfo_CheckDisposed_m3933,
	ClientSessionCache__cctor_m3934,
	ClientSessionCache_Add_m3935,
	ClientSessionCache_FromHost_m3936,
	ClientSessionCache_FromContext_m3937,
	ClientSessionCache_SetContextInCache_m3938,
	ClientSessionCache_SetContextFromCache_m3939,
	Context__ctor_m3940,
	Context_get_AbbreviatedHandshake_m3941,
	Context_set_AbbreviatedHandshake_m3942,
	Context_get_ProtocolNegotiated_m3943,
	Context_set_ProtocolNegotiated_m3944,
	Context_get_SecurityProtocol_m3945,
	Context_set_SecurityProtocol_m3946,
	Context_get_SecurityProtocolFlags_m3947,
	Context_get_Protocol_m3948,
	Context_get_SessionId_m3949,
	Context_set_SessionId_m3950,
	Context_get_CompressionMethod_m3951,
	Context_set_CompressionMethod_m3952,
	Context_get_ServerSettings_m3953,
	Context_get_ClientSettings_m3954,
	Context_get_LastHandshakeMsg_m3955,
	Context_set_LastHandshakeMsg_m3956,
	Context_get_HandshakeState_m3957,
	Context_set_HandshakeState_m3958,
	Context_get_ReceivedConnectionEnd_m3959,
	Context_set_ReceivedConnectionEnd_m3960,
	Context_get_SentConnectionEnd_m3961,
	Context_set_SentConnectionEnd_m3962,
	Context_get_SupportedCiphers_m3963,
	Context_set_SupportedCiphers_m3964,
	Context_get_HandshakeMessages_m3965,
	Context_get_WriteSequenceNumber_m3966,
	Context_set_WriteSequenceNumber_m3967,
	Context_get_ReadSequenceNumber_m3968,
	Context_set_ReadSequenceNumber_m3969,
	Context_get_ClientRandom_m3970,
	Context_set_ClientRandom_m3971,
	Context_get_ServerRandom_m3972,
	Context_set_ServerRandom_m3973,
	Context_get_RandomCS_m3974,
	Context_set_RandomCS_m3975,
	Context_get_RandomSC_m3976,
	Context_set_RandomSC_m3977,
	Context_get_MasterSecret_m3978,
	Context_set_MasterSecret_m3979,
	Context_get_ClientWriteKey_m3980,
	Context_set_ClientWriteKey_m3981,
	Context_get_ServerWriteKey_m3982,
	Context_set_ServerWriteKey_m3983,
	Context_get_ClientWriteIV_m3984,
	Context_set_ClientWriteIV_m3985,
	Context_get_ServerWriteIV_m3986,
	Context_set_ServerWriteIV_m3987,
	Context_get_RecordProtocol_m3988,
	Context_set_RecordProtocol_m3989,
	Context_GetUnixTime_m3990,
	Context_GetSecureRandomBytes_m3991,
	Context_Clear_m3992,
	Context_ClearKeyInfo_m3993,
	Context_DecodeProtocolCode_m3994,
	Context_ChangeProtocol_m3995,
	Context_get_Current_m3996,
	Context_get_Negotiating_m3997,
	Context_get_Read_m3998,
	Context_get_Write_m3999,
	Context_StartSwitchingSecurityParameters_m4000,
	Context_EndSwitchingSecurityParameters_m4001,
	HttpsClientStream__ctor_m4002,
	HttpsClientStream_get_TrustFailure_m4003,
	HttpsClientStream_RaiseServerCertificateValidation_m4004,
	HttpsClientStream_U3CHttpsClientStreamU3Em__0_m4005,
	HttpsClientStream_U3CHttpsClientStreamU3Em__1_m4006,
	ReceiveRecordAsyncResult__ctor_m4007,
	ReceiveRecordAsyncResult_get_Record_m4008,
	ReceiveRecordAsyncResult_get_ResultingBuffer_m4009,
	ReceiveRecordAsyncResult_get_InitialBuffer_m4010,
	ReceiveRecordAsyncResult_get_AsyncState_m4011,
	ReceiveRecordAsyncResult_get_AsyncException_m4012,
	ReceiveRecordAsyncResult_get_CompletedWithError_m4013,
	ReceiveRecordAsyncResult_get_AsyncWaitHandle_m4014,
	ReceiveRecordAsyncResult_get_IsCompleted_m4015,
	ReceiveRecordAsyncResult_SetComplete_m4016,
	ReceiveRecordAsyncResult_SetComplete_m4017,
	ReceiveRecordAsyncResult_SetComplete_m4018,
	SendRecordAsyncResult__ctor_m4019,
	SendRecordAsyncResult_get_Message_m4020,
	SendRecordAsyncResult_get_AsyncState_m4021,
	SendRecordAsyncResult_get_AsyncException_m4022,
	SendRecordAsyncResult_get_CompletedWithError_m4023,
	SendRecordAsyncResult_get_AsyncWaitHandle_m4024,
	SendRecordAsyncResult_get_IsCompleted_m4025,
	SendRecordAsyncResult_SetComplete_m4026,
	SendRecordAsyncResult_SetComplete_m4027,
	RecordProtocol__ctor_m4028,
	RecordProtocol__cctor_m4029,
	RecordProtocol_get_Context_m4030,
	RecordProtocol_SendRecord_m4031,
	RecordProtocol_ProcessChangeCipherSpec_m4032,
	RecordProtocol_GetMessage_m4033,
	RecordProtocol_BeginReceiveRecord_m4034,
	RecordProtocol_InternalReceiveRecordCallback_m4035,
	RecordProtocol_EndReceiveRecord_m4036,
	RecordProtocol_ReceiveRecord_m4037,
	RecordProtocol_ReadRecordBuffer_m4038,
	RecordProtocol_ReadClientHelloV2_m4039,
	RecordProtocol_ReadStandardRecordBuffer_m4040,
	RecordProtocol_ProcessAlert_m4041,
	RecordProtocol_SendAlert_m4042,
	RecordProtocol_SendAlert_m4043,
	RecordProtocol_SendAlert_m4044,
	RecordProtocol_SendChangeCipherSpec_m4045,
	RecordProtocol_BeginSendRecord_m4046,
	RecordProtocol_InternalSendRecordCallback_m4047,
	RecordProtocol_BeginSendRecord_m4048,
	RecordProtocol_EndSendRecord_m4049,
	RecordProtocol_SendRecord_m4050,
	RecordProtocol_EncodeRecord_m4051,
	RecordProtocol_EncodeRecord_m4052,
	RecordProtocol_encryptRecordFragment_m4053,
	RecordProtocol_decryptRecordFragment_m4054,
	RecordProtocol_Compare_m4055,
	RecordProtocol_ProcessCipherSpecV2Buffer_m4056,
	RecordProtocol_MapV2CipherCode_m4057,
	RSASslSignatureDeformatter__ctor_m4058,
	RSASslSignatureDeformatter_VerifySignature_m4059,
	RSASslSignatureDeformatter_SetHashAlgorithm_m4060,
	RSASslSignatureDeformatter_SetKey_m4061,
	RSASslSignatureFormatter__ctor_m4062,
	RSASslSignatureFormatter_CreateSignature_m4063,
	RSASslSignatureFormatter_SetHashAlgorithm_m4064,
	RSASslSignatureFormatter_SetKey_m4065,
	SecurityParameters__ctor_m4066,
	SecurityParameters_get_Cipher_m4067,
	SecurityParameters_set_Cipher_m4068,
	SecurityParameters_get_ClientWriteMAC_m4069,
	SecurityParameters_set_ClientWriteMAC_m4070,
	SecurityParameters_get_ServerWriteMAC_m4071,
	SecurityParameters_set_ServerWriteMAC_m4072,
	SecurityParameters_Clear_m4073,
	ValidationResult_get_Trusted_m4074,
	ValidationResult_get_ErrorCode_m4075,
	SslClientStream__ctor_m4076,
	SslClientStream__ctor_m4077,
	SslClientStream__ctor_m4078,
	SslClientStream__ctor_m4079,
	SslClientStream__ctor_m4080,
	SslClientStream_add_ServerCertValidation_m4081,
	SslClientStream_remove_ServerCertValidation_m4082,
	SslClientStream_add_ClientCertSelection_m4083,
	SslClientStream_remove_ClientCertSelection_m4084,
	SslClientStream_add_PrivateKeySelection_m4085,
	SslClientStream_remove_PrivateKeySelection_m4086,
	SslClientStream_add_ServerCertValidation2_m4087,
	SslClientStream_remove_ServerCertValidation2_m4088,
	SslClientStream_get_InputBuffer_m4089,
	SslClientStream_get_ClientCertificates_m4090,
	SslClientStream_get_SelectedClientCertificate_m4091,
	SslClientStream_get_ServerCertValidationDelegate_m4092,
	SslClientStream_set_ServerCertValidationDelegate_m4093,
	SslClientStream_get_ClientCertSelectionDelegate_m4094,
	SslClientStream_set_ClientCertSelectionDelegate_m4095,
	SslClientStream_get_PrivateKeyCertSelectionDelegate_m4096,
	SslClientStream_set_PrivateKeyCertSelectionDelegate_m4097,
	SslClientStream_Finalize_m4098,
	SslClientStream_Dispose_m4099,
	SslClientStream_OnBeginNegotiateHandshake_m4100,
	SslClientStream_SafeReceiveRecord_m4101,
	SslClientStream_OnNegotiateHandshakeCallback_m4102,
	SslClientStream_OnLocalCertificateSelection_m4103,
	SslClientStream_get_HaveRemoteValidation2Callback_m4104,
	SslClientStream_OnRemoteCertificateValidation2_m4105,
	SslClientStream_OnRemoteCertificateValidation_m4106,
	SslClientStream_RaiseServerCertificateValidation_m4107,
	SslClientStream_RaiseServerCertificateValidation2_m4108,
	SslClientStream_RaiseClientCertificateSelection_m4109,
	SslClientStream_OnLocalPrivateKeySelection_m4110,
	SslClientStream_RaisePrivateKeySelection_m4111,
	SslCipherSuite__ctor_m4112,
	SslCipherSuite_ComputeServerRecordMAC_m4113,
	SslCipherSuite_ComputeClientRecordMAC_m4114,
	SslCipherSuite_ComputeMasterSecret_m4115,
	SslCipherSuite_ComputeKeys_m4116,
	SslCipherSuite_prf_m4117,
	SslHandshakeHash__ctor_m4118,
	SslHandshakeHash_Initialize_m4119,
	SslHandshakeHash_HashFinal_m4120,
	SslHandshakeHash_HashCore_m4121,
	SslHandshakeHash_CreateSignature_m4122,
	SslHandshakeHash_initializePad_m4123,
	InternalAsyncResult__ctor_m4124,
	InternalAsyncResult_get_ProceedAfterHandshake_m4125,
	InternalAsyncResult_get_FromWrite_m4126,
	InternalAsyncResult_get_Buffer_m4127,
	InternalAsyncResult_get_Offset_m4128,
	InternalAsyncResult_get_Count_m4129,
	InternalAsyncResult_get_BytesRead_m4130,
	InternalAsyncResult_get_AsyncState_m4131,
	InternalAsyncResult_get_AsyncException_m4132,
	InternalAsyncResult_get_CompletedWithError_m4133,
	InternalAsyncResult_get_AsyncWaitHandle_m4134,
	InternalAsyncResult_get_IsCompleted_m4135,
	InternalAsyncResult_SetComplete_m4136,
	InternalAsyncResult_SetComplete_m4137,
	InternalAsyncResult_SetComplete_m4138,
	InternalAsyncResult_SetComplete_m4139,
	SslStreamBase__ctor_m4140,
	SslStreamBase__cctor_m4141,
	SslStreamBase_AsyncHandshakeCallback_m4142,
	SslStreamBase_get_MightNeedHandshake_m4143,
	SslStreamBase_NegotiateHandshake_m4144,
	SslStreamBase_RaiseLocalCertificateSelection_m4145,
	SslStreamBase_RaiseRemoteCertificateValidation_m4146,
	SslStreamBase_RaiseRemoteCertificateValidation2_m4147,
	SslStreamBase_RaiseLocalPrivateKeySelection_m4148,
	SslStreamBase_get_CheckCertRevocationStatus_m4149,
	SslStreamBase_set_CheckCertRevocationStatus_m4150,
	SslStreamBase_get_CipherAlgorithm_m4151,
	SslStreamBase_get_CipherStrength_m4152,
	SslStreamBase_get_HashAlgorithm_m4153,
	SslStreamBase_get_HashStrength_m4154,
	SslStreamBase_get_KeyExchangeStrength_m4155,
	SslStreamBase_get_KeyExchangeAlgorithm_m4156,
	SslStreamBase_get_SecurityProtocol_m4157,
	SslStreamBase_get_ServerCertificate_m4158,
	SslStreamBase_get_ServerCertificates_m4159,
	SslStreamBase_BeginNegotiateHandshake_m4160,
	SslStreamBase_EndNegotiateHandshake_m4161,
	SslStreamBase_BeginRead_m4162,
	SslStreamBase_InternalBeginRead_m4163,
	SslStreamBase_InternalReadCallback_m4164,
	SslStreamBase_InternalBeginWrite_m4165,
	SslStreamBase_InternalWriteCallback_m4166,
	SslStreamBase_BeginWrite_m4167,
	SslStreamBase_EndRead_m4168,
	SslStreamBase_EndWrite_m4169,
	SslStreamBase_Close_m4170,
	SslStreamBase_Flush_m4171,
	SslStreamBase_Read_m4172,
	SslStreamBase_Read_m4173,
	SslStreamBase_Seek_m4174,
	SslStreamBase_SetLength_m4175,
	SslStreamBase_Write_m4176,
	SslStreamBase_Write_m4177,
	SslStreamBase_get_CanRead_m4178,
	SslStreamBase_get_CanSeek_m4179,
	SslStreamBase_get_CanWrite_m4180,
	SslStreamBase_get_Length_m4181,
	SslStreamBase_get_Position_m4182,
	SslStreamBase_set_Position_m4183,
	SslStreamBase_Finalize_m4184,
	SslStreamBase_Dispose_m4185,
	SslStreamBase_resetBuffer_m4186,
	SslStreamBase_checkDisposed_m4187,
	TlsCipherSuite__ctor_m4188,
	TlsCipherSuite_ComputeServerRecordMAC_m4189,
	TlsCipherSuite_ComputeClientRecordMAC_m4190,
	TlsCipherSuite_ComputeMasterSecret_m4191,
	TlsCipherSuite_ComputeKeys_m4192,
	TlsClientSettings__ctor_m4193,
	TlsClientSettings_get_TargetHost_m4194,
	TlsClientSettings_set_TargetHost_m4195,
	TlsClientSettings_get_Certificates_m4196,
	TlsClientSettings_set_Certificates_m4197,
	TlsClientSettings_get_ClientCertificate_m4198,
	TlsClientSettings_set_ClientCertificate_m4199,
	TlsClientSettings_UpdateCertificateRSA_m4200,
	TlsException__ctor_m4201,
	TlsException__ctor_m4202,
	TlsException__ctor_m4203,
	TlsException__ctor_m4204,
	TlsException__ctor_m4205,
	TlsException__ctor_m4206,
	TlsException_get_Alert_m4207,
	TlsServerSettings__ctor_m4208,
	TlsServerSettings_get_ServerKeyExchange_m4209,
	TlsServerSettings_set_ServerKeyExchange_m4210,
	TlsServerSettings_get_Certificates_m4211,
	TlsServerSettings_set_Certificates_m4212,
	TlsServerSettings_get_CertificateRSA_m4213,
	TlsServerSettings_get_RsaParameters_m4214,
	TlsServerSettings_set_RsaParameters_m4215,
	TlsServerSettings_set_SignedParams_m4216,
	TlsServerSettings_get_CertificateRequest_m4217,
	TlsServerSettings_set_CertificateRequest_m4218,
	TlsServerSettings_set_CertificateTypes_m4219,
	TlsServerSettings_set_DistinguisedNames_m4220,
	TlsServerSettings_UpdateCertificateRSA_m4221,
	TlsStream__ctor_m4222,
	TlsStream__ctor_m4223,
	TlsStream_get_EOF_m4224,
	TlsStream_get_CanWrite_m4225,
	TlsStream_get_CanRead_m4226,
	TlsStream_get_CanSeek_m4227,
	TlsStream_get_Position_m4228,
	TlsStream_set_Position_m4229,
	TlsStream_get_Length_m4230,
	TlsStream_ReadSmallValue_m4231,
	TlsStream_ReadByte_m4232,
	TlsStream_ReadInt16_m4233,
	TlsStream_ReadInt24_m4234,
	TlsStream_ReadBytes_m4235,
	TlsStream_Write_m4236,
	TlsStream_Write_m4237,
	TlsStream_WriteInt24_m4238,
	TlsStream_Write_m4239,
	TlsStream_Write_m4240,
	TlsStream_Reset_m4241,
	TlsStream_ToArray_m4242,
	TlsStream_Flush_m4243,
	TlsStream_SetLength_m4244,
	TlsStream_Seek_m4245,
	TlsStream_Read_m4246,
	TlsStream_Write_m4247,
	HandshakeMessage__ctor_m4248,
	HandshakeMessage__ctor_m4249,
	HandshakeMessage__ctor_m4250,
	HandshakeMessage_get_Context_m4251,
	HandshakeMessage_get_HandshakeType_m4252,
	HandshakeMessage_get_ContentType_m4253,
	HandshakeMessage_Process_m4254,
	HandshakeMessage_Update_m4255,
	HandshakeMessage_EncodeMessage_m4256,
	HandshakeMessage_Compare_m4257,
	TlsClientCertificate__ctor_m4258,
	TlsClientCertificate_get_ClientCertificate_m4259,
	TlsClientCertificate_Update_m4260,
	TlsClientCertificate_GetClientCertificate_m4261,
	TlsClientCertificate_SendCertificates_m4262,
	TlsClientCertificate_ProcessAsSsl3_m4263,
	TlsClientCertificate_ProcessAsTls1_m4264,
	TlsClientCertificate_FindParentCertificate_m4265,
	TlsClientCertificateVerify__ctor_m4266,
	TlsClientCertificateVerify_Update_m4267,
	TlsClientCertificateVerify_ProcessAsSsl3_m4268,
	TlsClientCertificateVerify_ProcessAsTls1_m4269,
	TlsClientCertificateVerify_getClientCertRSA_m4270,
	TlsClientCertificateVerify_getUnsignedBigInteger_m4271,
	TlsClientFinished__ctor_m4272,
	TlsClientFinished__cctor_m4273,
	TlsClientFinished_Update_m4274,
	TlsClientFinished_ProcessAsSsl3_m4275,
	TlsClientFinished_ProcessAsTls1_m4276,
	TlsClientHello__ctor_m4277,
	TlsClientHello_Update_m4278,
	TlsClientHello_ProcessAsSsl3_m4279,
	TlsClientHello_ProcessAsTls1_m4280,
	TlsClientKeyExchange__ctor_m4281,
	TlsClientKeyExchange_ProcessAsSsl3_m4282,
	TlsClientKeyExchange_ProcessAsTls1_m4283,
	TlsClientKeyExchange_ProcessCommon_m4284,
	TlsServerCertificate__ctor_m4285,
	TlsServerCertificate_Update_m4286,
	TlsServerCertificate_ProcessAsSsl3_m4287,
	TlsServerCertificate_ProcessAsTls1_m4288,
	TlsServerCertificate_checkCertificateUsage_m4289,
	TlsServerCertificate_validateCertificates_m4290,
	TlsServerCertificate_checkServerIdentity_m4291,
	TlsServerCertificate_checkDomainName_m4292,
	TlsServerCertificate_Match_m4293,
	TlsServerCertificateRequest__ctor_m4294,
	TlsServerCertificateRequest_Update_m4295,
	TlsServerCertificateRequest_ProcessAsSsl3_m4296,
	TlsServerCertificateRequest_ProcessAsTls1_m4297,
	TlsServerFinished__ctor_m4298,
	TlsServerFinished__cctor_m4299,
	TlsServerFinished_Update_m4300,
	TlsServerFinished_ProcessAsSsl3_m4301,
	TlsServerFinished_ProcessAsTls1_m4302,
	TlsServerHello__ctor_m4303,
	TlsServerHello_Update_m4304,
	TlsServerHello_ProcessAsSsl3_m4305,
	TlsServerHello_ProcessAsTls1_m4306,
	TlsServerHello_processProtocol_m4307,
	TlsServerHelloDone__ctor_m4308,
	TlsServerHelloDone_ProcessAsSsl3_m4309,
	TlsServerHelloDone_ProcessAsTls1_m4310,
	TlsServerKeyExchange__ctor_m4311,
	TlsServerKeyExchange_Update_m4312,
	TlsServerKeyExchange_ProcessAsSsl3_m4313,
	TlsServerKeyExchange_ProcessAsTls1_m4314,
	TlsServerKeyExchange_verifySignature_m4315,
	PrimalityTest__ctor_m4316,
	PrimalityTest_Invoke_m4317,
	PrimalityTest_BeginInvoke_m4318,
	PrimalityTest_EndInvoke_m4319,
	CertificateValidationCallback__ctor_m4320,
	CertificateValidationCallback_Invoke_m4321,
	CertificateValidationCallback_BeginInvoke_m4322,
	CertificateValidationCallback_EndInvoke_m4323,
	CertificateValidationCallback2__ctor_m4324,
	CertificateValidationCallback2_Invoke_m4325,
	CertificateValidationCallback2_BeginInvoke_m4326,
	CertificateValidationCallback2_EndInvoke_m4327,
	CertificateSelectionCallback__ctor_m4328,
	CertificateSelectionCallback_Invoke_m4329,
	CertificateSelectionCallback_BeginInvoke_m4330,
	CertificateSelectionCallback_EndInvoke_m4331,
	PrivateKeySelectionCallback__ctor_m4332,
	PrivateKeySelectionCallback_Invoke_m4333,
	PrivateKeySelectionCallback_BeginInvoke_m4334,
	PrivateKeySelectionCallback_EndInvoke_m4335,
	Locale_GetText_m4461,
	Locale_GetText_m4462,
	MonoTODOAttribute__ctor_m4463,
	MonoTODOAttribute__ctor_m4464,
	GeneratedCodeAttribute__ctor_m4465,
	HybridDictionary__ctor_m4466,
	HybridDictionary__ctor_m4467,
	HybridDictionary_System_Collections_IEnumerable_GetEnumerator_m4468,
	HybridDictionary_get_inner_m4469,
	HybridDictionary_get_Count_m4470,
	HybridDictionary_get_IsSynchronized_m4471,
	HybridDictionary_get_Item_m4472,
	HybridDictionary_set_Item_m4473,
	HybridDictionary_get_SyncRoot_m4474,
	HybridDictionary_Add_m4475,
	HybridDictionary_Contains_m4476,
	HybridDictionary_CopyTo_m4477,
	HybridDictionary_GetEnumerator_m4478,
	HybridDictionary_Remove_m4479,
	HybridDictionary_Switch_m4480,
	DictionaryNode__ctor_m4481,
	DictionaryNodeEnumerator__ctor_m4482,
	DictionaryNodeEnumerator_FailFast_m4483,
	DictionaryNodeEnumerator_MoveNext_m4484,
	DictionaryNodeEnumerator_Reset_m4485,
	DictionaryNodeEnumerator_get_Current_m4486,
	DictionaryNodeEnumerator_get_DictionaryNode_m4487,
	DictionaryNodeEnumerator_get_Entry_m4488,
	DictionaryNodeEnumerator_get_Key_m4489,
	DictionaryNodeEnumerator_get_Value_m4490,
	ListDictionary__ctor_m4491,
	ListDictionary__ctor_m4492,
	ListDictionary_System_Collections_IEnumerable_GetEnumerator_m4493,
	ListDictionary_FindEntry_m4494,
	ListDictionary_FindEntry_m4495,
	ListDictionary_AddImpl_m4496,
	ListDictionary_get_Count_m4497,
	ListDictionary_get_IsSynchronized_m4498,
	ListDictionary_get_SyncRoot_m4499,
	ListDictionary_CopyTo_m4500,
	ListDictionary_get_Item_m4501,
	ListDictionary_set_Item_m4502,
	ListDictionary_Add_m4503,
	ListDictionary_Clear_m4504,
	ListDictionary_Contains_m4505,
	ListDictionary_GetEnumerator_m4506,
	ListDictionary_Remove_m4507,
	_Item__ctor_m4508,
	_KeysEnumerator__ctor_m4509,
	_KeysEnumerator_get_Current_m4510,
	_KeysEnumerator_MoveNext_m4511,
	_KeysEnumerator_Reset_m4512,
	KeysCollection__ctor_m4513,
	KeysCollection_System_Collections_ICollection_CopyTo_m4514,
	KeysCollection_System_Collections_ICollection_get_IsSynchronized_m4515,
	KeysCollection_System_Collections_ICollection_get_SyncRoot_m4516,
	KeysCollection_get_Count_m4517,
	KeysCollection_GetEnumerator_m4518,
	NameObjectCollectionBase__ctor_m4519,
	NameObjectCollectionBase__ctor_m4520,
	NameObjectCollectionBase_System_Collections_ICollection_get_IsSynchronized_m4521,
	NameObjectCollectionBase_System_Collections_ICollection_get_SyncRoot_m4522,
	NameObjectCollectionBase_System_Collections_ICollection_CopyTo_m4523,
	NameObjectCollectionBase_Init_m4524,
	NameObjectCollectionBase_get_Keys_m4525,
	NameObjectCollectionBase_GetEnumerator_m4526,
	NameObjectCollectionBase_GetObjectData_m4527,
	NameObjectCollectionBase_get_Count_m4528,
	NameObjectCollectionBase_OnDeserialization_m4529,
	NameObjectCollectionBase_get_IsReadOnly_m4530,
	NameObjectCollectionBase_BaseAdd_m4531,
	NameObjectCollectionBase_BaseGet_m4532,
	NameObjectCollectionBase_BaseGet_m4533,
	NameObjectCollectionBase_BaseGetKey_m4534,
	NameObjectCollectionBase_FindFirstMatchedItem_m4535,
	NameValueCollection__ctor_m4536,
	NameValueCollection__ctor_m4537,
	NameValueCollection_Add_m4538,
	NameValueCollection_Get_m4539,
	NameValueCollection_AsSingleString_m4540,
	NameValueCollection_GetKey_m4541,
	NameValueCollection_InvalidateCachedArrays_m4542,
	DefaultValueAttribute__ctor_m4543,
	DefaultValueAttribute_get_Value_m4544,
	DefaultValueAttribute_Equals_m4545,
	DefaultValueAttribute_GetHashCode_m4546,
	EditorBrowsableAttribute__ctor_m4547,
	EditorBrowsableAttribute_get_State_m4548,
	EditorBrowsableAttribute_Equals_m4549,
	EditorBrowsableAttribute_GetHashCode_m4550,
	TypeConverterAttribute__ctor_m4551,
	TypeConverterAttribute__ctor_m4552,
	TypeConverterAttribute__cctor_m4553,
	TypeConverterAttribute_Equals_m4554,
	TypeConverterAttribute_GetHashCode_m4555,
	TypeConverterAttribute_get_ConverterTypeName_m4556,
	DefaultCertificatePolicy__ctor_m4557,
	DefaultCertificatePolicy_CheckValidationResult_m4558,
	FileWebRequest__ctor_m4559,
	FileWebRequest__ctor_m4560,
	FileWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m4561,
	FileWebRequest_GetObjectData_m4562,
	FileWebRequestCreator__ctor_m4563,
	FileWebRequestCreator_Create_m4564,
	FtpRequestCreator__ctor_m4565,
	FtpRequestCreator_Create_m4566,
	FtpWebRequest__ctor_m4567,
	FtpWebRequest__cctor_m4568,
	FtpWebRequest_U3CcallbackU3Em__B_m4569,
	GlobalProxySelection_get_Select_m4570,
	HttpRequestCreator__ctor_m4571,
	HttpRequestCreator_Create_m4572,
	HttpVersion__cctor_m4573,
	HttpWebRequest__ctor_m4574,
	HttpWebRequest__ctor_m4575,
	HttpWebRequest__cctor_m4576,
	HttpWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m4577,
	HttpWebRequest_get_Address_m4421,
	HttpWebRequest_get_ServicePoint_m4425,
	HttpWebRequest_GetServicePoint_m4578,
	HttpWebRequest_GetObjectData_m4579,
	IPAddress__ctor_m4580,
	IPAddress__ctor_m4581,
	IPAddress__cctor_m4582,
	IPAddress_SwapShort_m4583,
	IPAddress_HostToNetworkOrder_m4584,
	IPAddress_NetworkToHostOrder_m4585,
	IPAddress_Parse_m4586,
	IPAddress_TryParse_m4587,
	IPAddress_ParseIPV4_m4588,
	IPAddress_ParseIPV6_m4589,
	IPAddress_get_InternalIPv4Address_m4590,
	IPAddress_get_ScopeId_m4591,
	IPAddress_get_AddressFamily_m4592,
	IPAddress_IsLoopback_m4593,
	IPAddress_ToString_m4594,
	IPAddress_ToString_m4595,
	IPAddress_Equals_m4596,
	IPAddress_GetHashCode_m4597,
	IPAddress_Hash_m4598,
	IPv6Address__ctor_m4599,
	IPv6Address__ctor_m4600,
	IPv6Address__ctor_m4601,
	IPv6Address__cctor_m4602,
	IPv6Address_Parse_m4603,
	IPv6Address_Fill_m4604,
	IPv6Address_TryParse_m4605,
	IPv6Address_TryParse_m4606,
	IPv6Address_get_Address_m4607,
	IPv6Address_get_ScopeId_m4608,
	IPv6Address_set_ScopeId_m4609,
	IPv6Address_IsLoopback_m4610,
	IPv6Address_SwapUShort_m4611,
	IPv6Address_AsIPv4Int_m4612,
	IPv6Address_IsIPv4Compatible_m4613,
	IPv6Address_IsIPv4Mapped_m4614,
	IPv6Address_ToString_m4615,
	IPv6Address_ToString_m4616,
	IPv6Address_Equals_m4617,
	IPv6Address_GetHashCode_m4618,
	IPv6Address_Hash_m4619,
	ServicePoint__ctor_m4620,
	ServicePoint_get_Address_m4621,
	ServicePoint_get_CurrentConnections_m4622,
	ServicePoint_get_IdleSince_m4623,
	ServicePoint_set_IdleSince_m4624,
	ServicePoint_set_Expect100Continue_m4625,
	ServicePoint_set_UseNagleAlgorithm_m4626,
	ServicePoint_set_SendContinue_m4627,
	ServicePoint_set_UsesProxy_m4628,
	ServicePoint_set_UseConnect_m4629,
	ServicePoint_get_AvailableForRecycling_m4630,
	SPKey__ctor_m4631,
	SPKey_GetHashCode_m4632,
	SPKey_Equals_m4633,
	ServicePointManager__cctor_m4634,
	ServicePointManager_get_CertificatePolicy_m4424,
	ServicePointManager_get_CheckCertificateRevocationList_m4392,
	ServicePointManager_get_SecurityProtocol_m4423,
	ServicePointManager_get_ServerCertificateValidationCallback_m4426,
	ServicePointManager_FindServicePoint_m4635,
	ServicePointManager_RecycleServicePoints_m4636,
	WebHeaderCollection__ctor_m4637,
	WebHeaderCollection__ctor_m4638,
	WebHeaderCollection__ctor_m4639,
	WebHeaderCollection__cctor_m4640,
	WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m4641,
	WebHeaderCollection_Add_m4642,
	WebHeaderCollection_AddWithoutValidate_m4643,
	WebHeaderCollection_IsRestricted_m4644,
	WebHeaderCollection_OnDeserialization_m4645,
	WebHeaderCollection_ToString_m4646,
	WebHeaderCollection_GetObjectData_m4647,
	WebHeaderCollection_get_Count_m4648,
	WebHeaderCollection_get_Keys_m4649,
	WebHeaderCollection_Get_m4650,
	WebHeaderCollection_GetKey_m4651,
	WebHeaderCollection_GetEnumerator_m4652,
	WebHeaderCollection_IsHeaderValue_m4653,
	WebHeaderCollection_IsHeaderName_m4654,
	WebProxy__ctor_m4655,
	WebProxy__ctor_m4656,
	WebProxy__ctor_m4657,
	WebProxy_System_Runtime_Serialization_ISerializable_GetObjectData_m4658,
	WebProxy_get_UseDefaultCredentials_m4659,
	WebProxy_GetProxy_m4660,
	WebProxy_IsBypassed_m4661,
	WebProxy_GetObjectData_m4662,
	WebProxy_CheckBypassList_m4663,
	WebRequest__ctor_m4664,
	WebRequest__ctor_m4665,
	WebRequest__cctor_m4666,
	WebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m4667,
	WebRequest_AddDynamicPrefix_m4668,
	WebRequest_GetMustImplement_m4669,
	WebRequest_get_DefaultWebProxy_m4670,
	WebRequest_GetDefaultWebProxy_m4671,
	WebRequest_GetObjectData_m4672,
	WebRequest_AddPrefix_m4673,
	PublicKey__ctor_m4674,
	PublicKey_get_EncodedKeyValue_m4675,
	PublicKey_get_EncodedParameters_m4676,
	PublicKey_get_Key_m4677,
	PublicKey_get_Oid_m4678,
	PublicKey_GetUnsignedBigInteger_m4679,
	PublicKey_DecodeDSA_m4680,
	PublicKey_DecodeRSA_m4681,
	X500DistinguishedName__ctor_m4682,
	X500DistinguishedName_Decode_m4683,
	X500DistinguishedName_GetSeparator_m4684,
	X500DistinguishedName_DecodeRawData_m4685,
	X500DistinguishedName_Canonize_m4686,
	X500DistinguishedName_AreEqual_m4687,
	X509BasicConstraintsExtension__ctor_m4688,
	X509BasicConstraintsExtension__ctor_m4689,
	X509BasicConstraintsExtension__ctor_m4690,
	X509BasicConstraintsExtension_get_CertificateAuthority_m4691,
	X509BasicConstraintsExtension_get_HasPathLengthConstraint_m4692,
	X509BasicConstraintsExtension_get_PathLengthConstraint_m4693,
	X509BasicConstraintsExtension_CopyFrom_m4694,
	X509BasicConstraintsExtension_Decode_m4695,
	X509BasicConstraintsExtension_Encode_m4696,
	X509BasicConstraintsExtension_ToString_m4697,
	X509Certificate2__ctor_m4427,
	X509Certificate2__cctor_m4698,
	X509Certificate2_get_Extensions_m4699,
	X509Certificate2_get_IssuerName_m4700,
	X509Certificate2_get_NotAfter_m4701,
	X509Certificate2_get_NotBefore_m4702,
	X509Certificate2_get_PrivateKey_m4432,
	X509Certificate2_get_PublicKey_m4703,
	X509Certificate2_get_SerialNumber_m4704,
	X509Certificate2_get_SignatureAlgorithm_m4705,
	X509Certificate2_get_SubjectName_m4706,
	X509Certificate2_get_Thumbprint_m4707,
	X509Certificate2_get_Version_m4708,
	X509Certificate2_GetNameInfo_m4709,
	X509Certificate2_Find_m4710,
	X509Certificate2_GetValueAsString_m4711,
	X509Certificate2_ImportPkcs12_m4712,
	X509Certificate2_Import_m4713,
	X509Certificate2_Reset_m4714,
	X509Certificate2_ToString_m4715,
	X509Certificate2_ToString_m4716,
	X509Certificate2_AppendBuffer_m4717,
	X509Certificate2_Verify_m4718,
	X509Certificate2_get_MonoCertificate_m4719,
	X509Certificate2Collection__ctor_m4720,
	X509Certificate2Collection__ctor_m4721,
	X509Certificate2Collection_get_Item_m4722,
	X509Certificate2Collection_Add_m4723,
	X509Certificate2Collection_AddRange_m4724,
	X509Certificate2Collection_Contains_m4725,
	X509Certificate2Collection_Find_m4726,
	X509Certificate2Collection_GetEnumerator_m4727,
	X509Certificate2Enumerator__ctor_m4728,
	X509Certificate2Enumerator_System_Collections_IEnumerator_get_Current_m4729,
	X509Certificate2Enumerator_System_Collections_IEnumerator_MoveNext_m4730,
	X509Certificate2Enumerator_get_Current_m4731,
	X509Certificate2Enumerator_MoveNext_m4732,
	X509CertificateEnumerator__ctor_m4733,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m4734,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m4735,
	X509CertificateEnumerator_get_Current_m4451,
	X509CertificateEnumerator_MoveNext_m4736,
	X509CertificateCollection__ctor_m4442,
	X509CertificateCollection__ctor_m4441,
	X509CertificateCollection_get_Item_m4431,
	X509CertificateCollection_AddRange_m4737,
	X509CertificateCollection_GetEnumerator_m4450,
	X509CertificateCollection_GetHashCode_m4738,
	X509Chain__ctor_m4428,
	X509Chain__ctor_m4739,
	X509Chain__cctor_m4740,
	X509Chain_get_ChainPolicy_m4741,
	X509Chain_Build_m4429,
	X509Chain_Reset_m4742,
	X509Chain_get_Roots_m4743,
	X509Chain_get_CertificateAuthorities_m4744,
	X509Chain_get_CertificateCollection_m4745,
	X509Chain_BuildChainFrom_m4746,
	X509Chain_SelectBestFromCollection_m4747,
	X509Chain_FindParent_m4748,
	X509Chain_IsChainComplete_m4749,
	X509Chain_IsSelfIssued_m4750,
	X509Chain_ValidateChain_m4751,
	X509Chain_Process_m4752,
	X509Chain_PrepareForNextCertificate_m4753,
	X509Chain_WrapUp_m4754,
	X509Chain_ProcessCertificateExtensions_m4755,
	X509Chain_IsSignedWith_m4756,
	X509Chain_GetSubjectKeyIdentifier_m4757,
	X509Chain_GetAuthorityKeyIdentifier_m4758,
	X509Chain_GetAuthorityKeyIdentifier_m4759,
	X509Chain_GetAuthorityKeyIdentifier_m4760,
	X509Chain_CheckRevocationOnChain_m4761,
	X509Chain_CheckRevocation_m4762,
	X509Chain_CheckRevocation_m4763,
	X509Chain_FindCrl_m4764,
	X509Chain_ProcessCrlExtensions_m4765,
	X509Chain_ProcessCrlEntryExtensions_m4766,
	X509ChainElement__ctor_m4767,
	X509ChainElement_get_Certificate_m4768,
	X509ChainElement_get_ChainElementStatus_m4769,
	X509ChainElement_get_StatusFlags_m4770,
	X509ChainElement_set_StatusFlags_m4771,
	X509ChainElement_Count_m4772,
	X509ChainElement_Set_m4773,
	X509ChainElement_UncompressFlags_m4774,
	X509ChainElementCollection__ctor_m4775,
	X509ChainElementCollection_System_Collections_ICollection_CopyTo_m4776,
	X509ChainElementCollection_System_Collections_IEnumerable_GetEnumerator_m4777,
	X509ChainElementCollection_get_Count_m4778,
	X509ChainElementCollection_get_IsSynchronized_m4779,
	X509ChainElementCollection_get_Item_m4780,
	X509ChainElementCollection_get_SyncRoot_m4781,
	X509ChainElementCollection_GetEnumerator_m4782,
	X509ChainElementCollection_Add_m4783,
	X509ChainElementCollection_Clear_m4784,
	X509ChainElementCollection_Contains_m4785,
	X509ChainElementEnumerator__ctor_m4786,
	X509ChainElementEnumerator_System_Collections_IEnumerator_get_Current_m4787,
	X509ChainElementEnumerator_get_Current_m4788,
	X509ChainElementEnumerator_MoveNext_m4789,
	X509ChainPolicy__ctor_m4790,
	X509ChainPolicy_get_ExtraStore_m4791,
	X509ChainPolicy_get_RevocationFlag_m4792,
	X509ChainPolicy_get_RevocationMode_m4793,
	X509ChainPolicy_get_VerificationFlags_m4794,
	X509ChainPolicy_get_VerificationTime_m4795,
	X509ChainPolicy_Reset_m4796,
	X509ChainStatus__ctor_m4797,
	X509ChainStatus_get_Status_m4798,
	X509ChainStatus_set_Status_m4799,
	X509ChainStatus_set_StatusInformation_m4800,
	X509ChainStatus_GetInformation_m4801,
	X509EnhancedKeyUsageExtension__ctor_m4802,
	X509EnhancedKeyUsageExtension_CopyFrom_m4803,
	X509EnhancedKeyUsageExtension_Decode_m4804,
	X509EnhancedKeyUsageExtension_ToString_m4805,
	X509Extension__ctor_m4806,
	X509Extension__ctor_m4807,
	X509Extension_get_Critical_m4808,
	X509Extension_set_Critical_m4809,
	X509Extension_CopyFrom_m4810,
	X509Extension_FormatUnkownData_m4811,
	X509ExtensionCollection__ctor_m4812,
	X509ExtensionCollection_System_Collections_ICollection_CopyTo_m4813,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m4814,
	X509ExtensionCollection_get_Count_m4815,
	X509ExtensionCollection_get_IsSynchronized_m4816,
	X509ExtensionCollection_get_SyncRoot_m4817,
	X509ExtensionCollection_get_Item_m4818,
	X509ExtensionCollection_GetEnumerator_m4819,
	X509ExtensionEnumerator__ctor_m4820,
	X509ExtensionEnumerator_System_Collections_IEnumerator_get_Current_m4821,
	X509ExtensionEnumerator_get_Current_m4822,
	X509ExtensionEnumerator_MoveNext_m4823,
	X509KeyUsageExtension__ctor_m4824,
	X509KeyUsageExtension__ctor_m4825,
	X509KeyUsageExtension__ctor_m4826,
	X509KeyUsageExtension_get_KeyUsages_m4827,
	X509KeyUsageExtension_CopyFrom_m4828,
	X509KeyUsageExtension_GetValidFlags_m4829,
	X509KeyUsageExtension_Decode_m4830,
	X509KeyUsageExtension_Encode_m4831,
	X509KeyUsageExtension_ToString_m4832,
	X509Store__ctor_m4833,
	X509Store_get_Certificates_m4834,
	X509Store_get_Factory_m4835,
	X509Store_get_Store_m4836,
	X509Store_Close_m4837,
	X509Store_Open_m4838,
	X509SubjectKeyIdentifierExtension__ctor_m4839,
	X509SubjectKeyIdentifierExtension__ctor_m4840,
	X509SubjectKeyIdentifierExtension__ctor_m4841,
	X509SubjectKeyIdentifierExtension__ctor_m4842,
	X509SubjectKeyIdentifierExtension__ctor_m4843,
	X509SubjectKeyIdentifierExtension__ctor_m4844,
	X509SubjectKeyIdentifierExtension_get_SubjectKeyIdentifier_m4845,
	X509SubjectKeyIdentifierExtension_CopyFrom_m4846,
	X509SubjectKeyIdentifierExtension_FromHexChar_m4847,
	X509SubjectKeyIdentifierExtension_FromHexChars_m4848,
	X509SubjectKeyIdentifierExtension_FromHex_m4849,
	X509SubjectKeyIdentifierExtension_Decode_m4850,
	X509SubjectKeyIdentifierExtension_Encode_m4851,
	X509SubjectKeyIdentifierExtension_ToString_m4852,
	AsnEncodedData__ctor_m4853,
	AsnEncodedData__ctor_m4854,
	AsnEncodedData__ctor_m4855,
	AsnEncodedData_get_Oid_m4856,
	AsnEncodedData_set_Oid_m4857,
	AsnEncodedData_get_RawData_m4858,
	AsnEncodedData_set_RawData_m4859,
	AsnEncodedData_CopyFrom_m4860,
	AsnEncodedData_ToString_m4861,
	AsnEncodedData_Default_m4862,
	AsnEncodedData_BasicConstraintsExtension_m4863,
	AsnEncodedData_EnhancedKeyUsageExtension_m4864,
	AsnEncodedData_KeyUsageExtension_m4865,
	AsnEncodedData_SubjectKeyIdentifierExtension_m4866,
	AsnEncodedData_SubjectAltName_m4867,
	AsnEncodedData_NetscapeCertType_m4868,
	Oid__ctor_m4869,
	Oid__ctor_m4870,
	Oid__ctor_m4871,
	Oid__ctor_m4872,
	Oid_get_FriendlyName_m4873,
	Oid_get_Value_m4874,
	Oid_GetName_m4875,
	OidCollection__ctor_m4876,
	OidCollection_System_Collections_ICollection_CopyTo_m4877,
	OidCollection_System_Collections_IEnumerable_GetEnumerator_m4878,
	OidCollection_get_Count_m4879,
	OidCollection_get_IsSynchronized_m4880,
	OidCollection_get_Item_m4881,
	OidCollection_get_SyncRoot_m4882,
	OidCollection_Add_m4883,
	OidEnumerator__ctor_m4884,
	OidEnumerator_System_Collections_IEnumerator_get_Current_m4885,
	OidEnumerator_MoveNext_m4886,
	MatchAppendEvaluator__ctor_m4887,
	MatchAppendEvaluator_Invoke_m4888,
	MatchAppendEvaluator_BeginInvoke_m4889,
	MatchAppendEvaluator_EndInvoke_m4890,
	BaseMachine__ctor_m4891,
	BaseMachine_Replace_m4892,
	BaseMachine_Scan_m4893,
	BaseMachine_LTRReplace_m4894,
	BaseMachine_RTLReplace_m4895,
	Capture__ctor_m4896,
	Capture__ctor_m4897,
	Capture_get_Index_m4898,
	Capture_get_Length_m4899,
	Capture_get_Value_m4458,
	Capture_ToString_m4900,
	Capture_get_Text_m4901,
	CaptureCollection__ctor_m4902,
	CaptureCollection_get_Count_m4903,
	CaptureCollection_get_IsSynchronized_m4904,
	CaptureCollection_SetValue_m4905,
	CaptureCollection_get_SyncRoot_m4906,
	CaptureCollection_CopyTo_m4907,
	CaptureCollection_GetEnumerator_m4908,
	Group__ctor_m4909,
	Group__ctor_m4910,
	Group__ctor_m4911,
	Group__cctor_m4912,
	Group_get_Captures_m4913,
	Group_get_Success_m4456,
	GroupCollection__ctor_m4914,
	GroupCollection_get_Count_m4915,
	GroupCollection_get_IsSynchronized_m4916,
	GroupCollection_get_Item_m4457,
	GroupCollection_SetValue_m4917,
	GroupCollection_get_SyncRoot_m4918,
	GroupCollection_CopyTo_m4919,
	GroupCollection_GetEnumerator_m4920,
	Match__ctor_m4921,
	Match__ctor_m4922,
	Match__ctor_m4923,
	Match__cctor_m4924,
	Match_get_Empty_m4925,
	Match_get_Groups_m4926,
	Match_NextMatch_m4927,
	Match_get_Regex_m4928,
	Enumerator__ctor_m4929,
	Enumerator_System_Collections_IEnumerator_get_Current_m4930,
	Enumerator_System_Collections_IEnumerator_MoveNext_m4931,
	MatchCollection__ctor_m4932,
	MatchCollection_get_Count_m4933,
	MatchCollection_get_IsSynchronized_m4934,
	MatchCollection_get_Item_m4935,
	MatchCollection_get_SyncRoot_m4936,
	MatchCollection_CopyTo_m4937,
	MatchCollection_GetEnumerator_m4938,
	MatchCollection_TryToGet_m4939,
	MatchCollection_get_FullList_m4940,
	Regex__ctor_m4941,
	Regex__ctor_m4454,
	Regex__ctor_m4942,
	Regex__ctor_m4943,
	Regex__cctor_m4944,
	Regex_System_Runtime_Serialization_ISerializable_GetObjectData_m4945,
	Regex_Replace_m3381,
	Regex_Replace_m4946,
	Regex_validate_options_m4947,
	Regex_Init_m4948,
	Regex_InitNewRegex_m4949,
	Regex_CreateMachineFactory_m4950,
	Regex_get_Options_m4951,
	Regex_get_RightToLeft_m4952,
	Regex_GroupNumberFromName_m4953,
	Regex_GetGroupIndex_m4954,
	Regex_default_startat_m4955,
	Regex_IsMatch_m4956,
	Regex_IsMatch_m4957,
	Regex_Match_m4958,
	Regex_Matches_m4455,
	Regex_Matches_m4959,
	Regex_Replace_m4960,
	Regex_Replace_m4961,
	Regex_ToString_m4962,
	Regex_get_GroupCount_m4963,
	Regex_get_Gap_m4964,
	Regex_CreateMachine_m4965,
	Regex_GetGroupNamesArray_m4966,
	Regex_get_GroupNumbers_m4967,
	Key__ctor_m4968,
	Key_GetHashCode_m4969,
	Key_Equals_m4970,
	Key_ToString_m4971,
	FactoryCache__ctor_m4972,
	FactoryCache_Add_m4973,
	FactoryCache_Cleanup_m4974,
	FactoryCache_Lookup_m4975,
	Node__ctor_m4976,
	MRUList__ctor_m4977,
	MRUList_Use_m4978,
	MRUList_Evict_m4979,
	CategoryUtils_CategoryFromName_m4980,
	CategoryUtils_IsCategory_m4981,
	CategoryUtils_IsCategory_m4982,
	LinkRef__ctor_m4983,
	InterpreterFactory__ctor_m4984,
	InterpreterFactory_NewInstance_m4985,
	InterpreterFactory_get_GroupCount_m4986,
	InterpreterFactory_get_Gap_m4987,
	InterpreterFactory_set_Gap_m4988,
	InterpreterFactory_get_Mapping_m4989,
	InterpreterFactory_set_Mapping_m4990,
	InterpreterFactory_get_NamesMapping_m4991,
	InterpreterFactory_set_NamesMapping_m4992,
	PatternLinkStack__ctor_m4993,
	PatternLinkStack_set_BaseAddress_m4994,
	PatternLinkStack_get_OffsetAddress_m4995,
	PatternLinkStack_set_OffsetAddress_m4996,
	PatternLinkStack_GetOffset_m4997,
	PatternLinkStack_GetCurrent_m4998,
	PatternLinkStack_SetCurrent_m4999,
	PatternCompiler__ctor_m5000,
	PatternCompiler_EncodeOp_m5001,
	PatternCompiler_GetMachineFactory_m5002,
	PatternCompiler_EmitFalse_m5003,
	PatternCompiler_EmitTrue_m5004,
	PatternCompiler_EmitCount_m5005,
	PatternCompiler_EmitCharacter_m5006,
	PatternCompiler_EmitCategory_m5007,
	PatternCompiler_EmitNotCategory_m5008,
	PatternCompiler_EmitRange_m5009,
	PatternCompiler_EmitSet_m5010,
	PatternCompiler_EmitString_m5011,
	PatternCompiler_EmitPosition_m5012,
	PatternCompiler_EmitOpen_m5013,
	PatternCompiler_EmitClose_m5014,
	PatternCompiler_EmitBalanceStart_m5015,
	PatternCompiler_EmitBalance_m5016,
	PatternCompiler_EmitReference_m5017,
	PatternCompiler_EmitIfDefined_m5018,
	PatternCompiler_EmitSub_m5019,
	PatternCompiler_EmitTest_m5020,
	PatternCompiler_EmitBranch_m5021,
	PatternCompiler_EmitJump_m5022,
	PatternCompiler_EmitRepeat_m5023,
	PatternCompiler_EmitUntil_m5024,
	PatternCompiler_EmitFastRepeat_m5025,
	PatternCompiler_EmitIn_m5026,
	PatternCompiler_EmitAnchor_m5027,
	PatternCompiler_EmitInfo_m5028,
	PatternCompiler_NewLink_m5029,
	PatternCompiler_ResolveLink_m5030,
	PatternCompiler_EmitBranchEnd_m5031,
	PatternCompiler_EmitAlternationEnd_m5032,
	PatternCompiler_MakeFlags_m5033,
	PatternCompiler_Emit_m5034,
	PatternCompiler_Emit_m5035,
	PatternCompiler_Emit_m5036,
	PatternCompiler_get_CurrentAddress_m5037,
	PatternCompiler_BeginLink_m5038,
	PatternCompiler_EmitLink_m5039,
	LinkStack__ctor_m5040,
	LinkStack_Push_m5041,
	LinkStack_Pop_m5042,
	Mark_get_IsDefined_m5043,
	Mark_get_Index_m5044,
	Mark_get_Length_m5045,
	IntStack_Pop_m5046,
	IntStack_Push_m5047,
	IntStack_get_Count_m5048,
	IntStack_set_Count_m5049,
	RepeatContext__ctor_m5050,
	RepeatContext_get_Count_m5051,
	RepeatContext_set_Count_m5052,
	RepeatContext_get_Start_m5053,
	RepeatContext_set_Start_m5054,
	RepeatContext_get_IsMinimum_m5055,
	RepeatContext_get_IsMaximum_m5056,
	RepeatContext_get_IsLazy_m5057,
	RepeatContext_get_Expression_m5058,
	RepeatContext_get_Previous_m5059,
	Interpreter__ctor_m5060,
	Interpreter_ReadProgramCount_m5061,
	Interpreter_Scan_m5062,
	Interpreter_Reset_m5063,
	Interpreter_Eval_m5064,
	Interpreter_EvalChar_m5065,
	Interpreter_TryMatch_m5066,
	Interpreter_IsPosition_m5067,
	Interpreter_IsWordChar_m5068,
	Interpreter_GetString_m5069,
	Interpreter_Open_m5070,
	Interpreter_Close_m5071,
	Interpreter_Balance_m5072,
	Interpreter_Checkpoint_m5073,
	Interpreter_Backtrack_m5074,
	Interpreter_ResetGroups_m5075,
	Interpreter_GetLastDefined_m5076,
	Interpreter_CreateMark_m5077,
	Interpreter_GetGroupInfo_m5078,
	Interpreter_PopulateGroup_m5079,
	Interpreter_GenerateMatch_m5080,
	Interval__ctor_m5081,
	Interval_get_Empty_m5082,
	Interval_get_IsDiscontiguous_m5083,
	Interval_get_IsSingleton_m5084,
	Interval_get_IsEmpty_m5085,
	Interval_get_Size_m5086,
	Interval_IsDisjoint_m5087,
	Interval_IsAdjacent_m5088,
	Interval_Contains_m5089,
	Interval_Contains_m5090,
	Interval_Intersects_m5091,
	Interval_Merge_m5092,
	Interval_CompareTo_m5093,
	Enumerator__ctor_m5094,
	Enumerator_get_Current_m5095,
	Enumerator_MoveNext_m5096,
	Enumerator_Reset_m5097,
	CostDelegate__ctor_m5098,
	CostDelegate_Invoke_m5099,
	CostDelegate_BeginInvoke_m5100,
	CostDelegate_EndInvoke_m5101,
	IntervalCollection__ctor_m5102,
	IntervalCollection_get_Item_m5103,
	IntervalCollection_Add_m5104,
	IntervalCollection_Normalize_m5105,
	IntervalCollection_GetMetaCollection_m5106,
	IntervalCollection_Optimize_m5107,
	IntervalCollection_get_Count_m5108,
	IntervalCollection_get_IsSynchronized_m5109,
	IntervalCollection_get_SyncRoot_m5110,
	IntervalCollection_CopyTo_m5111,
	IntervalCollection_GetEnumerator_m5112,
	Parser__ctor_m5113,
	Parser_ParseDecimal_m5114,
	Parser_ParseOctal_m5115,
	Parser_ParseHex_m5116,
	Parser_ParseNumber_m5117,
	Parser_ParseName_m5118,
	Parser_ParseRegularExpression_m5119,
	Parser_GetMapping_m5120,
	Parser_ParseGroup_m5121,
	Parser_ParseGroupingConstruct_m5122,
	Parser_ParseAssertionType_m5123,
	Parser_ParseOptions_m5124,
	Parser_ParseCharacterClass_m5125,
	Parser_ParseRepetitionBounds_m5126,
	Parser_ParseUnicodeCategory_m5127,
	Parser_ParseSpecial_m5128,
	Parser_ParseEscape_m5129,
	Parser_ParseName_m5130,
	Parser_IsNameChar_m5131,
	Parser_ParseNumber_m5132,
	Parser_ParseDigit_m5133,
	Parser_ConsumeWhitespace_m5134,
	Parser_ResolveReferences_m5135,
	Parser_HandleExplicitNumericGroups_m5136,
	Parser_IsIgnoreCase_m5137,
	Parser_IsMultiline_m5138,
	Parser_IsExplicitCapture_m5139,
	Parser_IsSingleline_m5140,
	Parser_IsIgnorePatternWhitespace_m5141,
	Parser_IsECMAScript_m5142,
	Parser_NewParseException_m5143,
	QuickSearch__ctor_m5144,
	QuickSearch__cctor_m5145,
	QuickSearch_get_Length_m5146,
	QuickSearch_Search_m5147,
	QuickSearch_SetupShiftTable_m5148,
	QuickSearch_GetShiftDistance_m5149,
	QuickSearch_GetChar_m5150,
	ReplacementEvaluator__ctor_m5151,
	ReplacementEvaluator_Evaluate_m5152,
	ReplacementEvaluator_EvaluateAppend_m5153,
	ReplacementEvaluator_get_NeedsGroupsOrCaptures_m5154,
	ReplacementEvaluator_Ensure_m5155,
	ReplacementEvaluator_AddFromReplacement_m5156,
	ReplacementEvaluator_AddInt_m5157,
	ReplacementEvaluator_Compile_m5158,
	ReplacementEvaluator_CompileTerm_m5159,
	ExpressionCollection__ctor_m5160,
	ExpressionCollection_Add_m5161,
	ExpressionCollection_get_Item_m5162,
	ExpressionCollection_set_Item_m5163,
	ExpressionCollection_OnValidate_m5164,
	Expression__ctor_m5165,
	Expression_GetFixedWidth_m5166,
	Expression_GetAnchorInfo_m5167,
	CompositeExpression__ctor_m5168,
	CompositeExpression_get_Expressions_m5169,
	CompositeExpression_GetWidth_m5170,
	CompositeExpression_IsComplex_m5171,
	Group__ctor_m5172,
	Group_AppendExpression_m5173,
	Group_Compile_m5174,
	Group_GetWidth_m5175,
	Group_GetAnchorInfo_m5176,
	RegularExpression__ctor_m5177,
	RegularExpression_set_GroupCount_m5178,
	RegularExpression_Compile_m5179,
	CapturingGroup__ctor_m5180,
	CapturingGroup_get_Index_m5181,
	CapturingGroup_set_Index_m5182,
	CapturingGroup_get_Name_m5183,
	CapturingGroup_set_Name_m5184,
	CapturingGroup_get_IsNamed_m5185,
	CapturingGroup_Compile_m5186,
	CapturingGroup_IsComplex_m5187,
	CapturingGroup_CompareTo_m5188,
	BalancingGroup__ctor_m5189,
	BalancingGroup_set_Balance_m5190,
	BalancingGroup_Compile_m5191,
	NonBacktrackingGroup__ctor_m5192,
	NonBacktrackingGroup_Compile_m5193,
	NonBacktrackingGroup_IsComplex_m5194,
	Repetition__ctor_m5195,
	Repetition_get_Expression_m5196,
	Repetition_set_Expression_m5197,
	Repetition_get_Minimum_m5198,
	Repetition_Compile_m5199,
	Repetition_GetWidth_m5200,
	Repetition_GetAnchorInfo_m5201,
	Assertion__ctor_m5202,
	Assertion_get_TrueExpression_m5203,
	Assertion_set_TrueExpression_m5204,
	Assertion_get_FalseExpression_m5205,
	Assertion_set_FalseExpression_m5206,
	Assertion_GetWidth_m5207,
	CaptureAssertion__ctor_m5208,
	CaptureAssertion_set_CapturingGroup_m5209,
	CaptureAssertion_Compile_m5210,
	CaptureAssertion_IsComplex_m5211,
	CaptureAssertion_get_Alternate_m5212,
	ExpressionAssertion__ctor_m5213,
	ExpressionAssertion_set_Reverse_m5214,
	ExpressionAssertion_set_Negate_m5215,
	ExpressionAssertion_get_TestExpression_m5216,
	ExpressionAssertion_set_TestExpression_m5217,
	ExpressionAssertion_Compile_m5218,
	ExpressionAssertion_IsComplex_m5219,
	Alternation__ctor_m5220,
	Alternation_get_Alternatives_m5221,
	Alternation_AddAlternative_m5222,
	Alternation_Compile_m5223,
	Alternation_GetWidth_m5224,
	Literal__ctor_m5225,
	Literal_CompileLiteral_m5226,
	Literal_Compile_m5227,
	Literal_GetWidth_m5228,
	Literal_GetAnchorInfo_m5229,
	Literal_IsComplex_m5230,
	PositionAssertion__ctor_m5231,
	PositionAssertion_Compile_m5232,
	PositionAssertion_GetWidth_m5233,
	PositionAssertion_IsComplex_m5234,
	PositionAssertion_GetAnchorInfo_m5235,
	Reference__ctor_m5236,
	Reference_get_CapturingGroup_m5237,
	Reference_set_CapturingGroup_m5238,
	Reference_get_IgnoreCase_m5239,
	Reference_Compile_m5240,
	Reference_GetWidth_m5241,
	Reference_IsComplex_m5242,
	BackslashNumber__ctor_m5243,
	BackslashNumber_ResolveReference_m5244,
	BackslashNumber_Compile_m5245,
	CharacterClass__ctor_m5246,
	CharacterClass__ctor_m5247,
	CharacterClass__cctor_m5248,
	CharacterClass_AddCategory_m5249,
	CharacterClass_AddCharacter_m5250,
	CharacterClass_AddRange_m5251,
	CharacterClass_Compile_m5252,
	CharacterClass_GetWidth_m5253,
	CharacterClass_IsComplex_m5254,
	CharacterClass_GetIntervalCost_m5255,
	AnchorInfo__ctor_m5256,
	AnchorInfo__ctor_m5257,
	AnchorInfo__ctor_m5258,
	AnchorInfo_get_Offset_m5259,
	AnchorInfo_get_Width_m5260,
	AnchorInfo_get_Length_m5261,
	AnchorInfo_get_IsUnknownWidth_m5262,
	AnchorInfo_get_IsComplete_m5263,
	AnchorInfo_get_Substring_m5264,
	AnchorInfo_get_IgnoreCase_m5265,
	AnchorInfo_get_Position_m5266,
	AnchorInfo_get_IsSubstring_m5267,
	AnchorInfo_get_IsPosition_m5268,
	AnchorInfo_GetInterval_m5269,
	DefaultUriParser__ctor_m5270,
	DefaultUriParser__ctor_m5271,
	UriScheme__ctor_m5272,
	Uri__ctor_m3308,
	Uri__ctor_m5273,
	Uri__ctor_m5274,
	Uri__ctor_m3310,
	Uri__cctor_m5275,
	Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m5276,
	Uri_Merge_m5277,
	Uri_get_AbsoluteUri_m5278,
	Uri_get_Authority_m5279,
	Uri_get_Host_m4422,
	Uri_get_IsFile_m5280,
	Uri_get_IsLoopback_m5281,
	Uri_get_IsUnc_m5282,
	Uri_get_Scheme_m5283,
	Uri_get_IsAbsoluteUri_m5284,
	Uri_CheckHostName_m5285,
	Uri_IsIPv4Address_m5286,
	Uri_IsDomainAddress_m5287,
	Uri_CheckSchemeName_m5288,
	Uri_IsAlpha_m5289,
	Uri_Equals_m5290,
	Uri_InternalEquals_m5291,
	Uri_GetHashCode_m5292,
	Uri_GetLeftPart_m5293,
	Uri_FromHex_m5294,
	Uri_HexEscape_m5295,
	Uri_IsHexDigit_m5296,
	Uri_IsHexEncoding_m5297,
	Uri_AppendQueryAndFragment_m5298,
	Uri_ToString_m5299,
	Uri_EscapeString_m5300,
	Uri_EscapeString_m5301,
	Uri_ParseUri_m5302,
	Uri_Unescape_m5303,
	Uri_Unescape_m5304,
	Uri_ParseAsWindowsUNC_m5305,
	Uri_ParseAsWindowsAbsoluteFilePath_m5306,
	Uri_ParseAsUnixAbsoluteFilePath_m5307,
	Uri_Parse_m5308,
	Uri_ParseNoExceptions_m5309,
	Uri_CompactEscaped_m5310,
	Uri_Reduce_m5311,
	Uri_HexUnescapeMultiByte_m5312,
	Uri_GetSchemeDelimiter_m5313,
	Uri_GetDefaultPort_m5314,
	Uri_GetOpaqueWiseSchemeDelimiter_m5315,
	Uri_IsPredefinedScheme_m5316,
	Uri_get_Parser_m5317,
	Uri_EnsureAbsoluteUri_m5318,
	Uri_op_Equality_m5319,
	UriFormatException__ctor_m5320,
	UriFormatException__ctor_m5321,
	UriFormatException__ctor_m5322,
	UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m5323,
	UriParser__ctor_m5324,
	UriParser__cctor_m5325,
	UriParser_InitializeAndValidate_m5326,
	UriParser_OnRegister_m5327,
	UriParser_set_SchemeName_m5328,
	UriParser_get_DefaultPort_m5329,
	UriParser_set_DefaultPort_m5330,
	UriParser_CreateDefaults_m5331,
	UriParser_InternalRegister_m5332,
	UriParser_GetParser_m5333,
	RemoteCertificateValidationCallback__ctor_m5334,
	RemoteCertificateValidationCallback_Invoke_m4430,
	RemoteCertificateValidationCallback_BeginInvoke_m5335,
	RemoteCertificateValidationCallback_EndInvoke_m5336,
	MatchEvaluator__ctor_m5337,
	MatchEvaluator_Invoke_m5338,
	MatchEvaluator_BeginInvoke_m5339,
	MatchEvaluator_EndInvoke_m5340,
	Object__ctor_m199,
	Object_Equals_m5429,
	Object_Equals_m5426,
	Object_Finalize_m3230,
	Object_GetHashCode_m5430,
	Object_GetType_m1538,
	Object_MemberwiseClone_m5431,
	Object_ToString_m3295,
	Object_ReferenceEquals_m3260,
	Object_InternalGetHashCode_m5432,
	ValueType__ctor_m5433,
	ValueType_InternalEquals_m5434,
	ValueType_DefaultEquals_m5435,
	ValueType_Equals_m5436,
	ValueType_InternalGetHashCode_m5437,
	ValueType_GetHashCode_m5438,
	ValueType_ToString_m5439,
	Attribute__ctor_m3265,
	Attribute_CheckParameters_m5440,
	Attribute_GetCustomAttribute_m5441,
	Attribute_GetCustomAttribute_m5442,
	Attribute_GetHashCode_m3398,
	Attribute_IsDefined_m5443,
	Attribute_IsDefined_m5444,
	Attribute_IsDefined_m5445,
	Attribute_IsDefined_m5446,
	Attribute_Equals_m5447,
	Int32_System_IConvertible_ToBoolean_m5448,
	Int32_System_IConvertible_ToByte_m5449,
	Int32_System_IConvertible_ToChar_m5450,
	Int32_System_IConvertible_ToDateTime_m5451,
	Int32_System_IConvertible_ToDecimal_m5452,
	Int32_System_IConvertible_ToDouble_m5453,
	Int32_System_IConvertible_ToInt16_m5454,
	Int32_System_IConvertible_ToInt32_m5455,
	Int32_System_IConvertible_ToInt64_m5456,
	Int32_System_IConvertible_ToSByte_m5457,
	Int32_System_IConvertible_ToSingle_m5458,
	Int32_System_IConvertible_ToType_m5459,
	Int32_System_IConvertible_ToUInt16_m5460,
	Int32_System_IConvertible_ToUInt32_m5461,
	Int32_System_IConvertible_ToUInt64_m5462,
	Int32_CompareTo_m5463,
	Int32_Equals_m5464,
	Int32_GetHashCode_m3244,
	Int32_CompareTo_m1474,
	Int32_Equals_m3246,
	Int32_ProcessTrailingWhitespace_m5465,
	Int32_Parse_m5466,
	Int32_Parse_m5467,
	Int32_CheckStyle_m5468,
	Int32_JumpOverWhite_m5469,
	Int32_FindSign_m5470,
	Int32_FindCurrency_m5471,
	Int32_FindExponent_m5472,
	Int32_FindOther_m5473,
	Int32_ValidDigit_m5474,
	Int32_GetFormatException_m5475,
	Int32_Parse_m5476,
	Int32_Parse_m244,
	Int32_Parse_m5477,
	Int32_TryParse_m5478,
	Int32_TryParse_m5366,
	Int32_ToString_m3285,
	Int32_ToString_m3337,
	Int32_ToString_m5398,
	Int32_ToString_m4404,
	Int32_GetTypeCode_m5479,
	SerializableAttribute__ctor_m5480,
	AttributeUsageAttribute__ctor_m5481,
	AttributeUsageAttribute_get_AllowMultiple_m5482,
	AttributeUsageAttribute_set_AllowMultiple_m5483,
	AttributeUsageAttribute_get_Inherited_m5484,
	AttributeUsageAttribute_set_Inherited_m5485,
	ComVisibleAttribute__ctor_m5486,
	Int64_System_IConvertible_ToBoolean_m5487,
	Int64_System_IConvertible_ToByte_m5488,
	Int64_System_IConvertible_ToChar_m5489,
	Int64_System_IConvertible_ToDateTime_m5490,
	Int64_System_IConvertible_ToDecimal_m5491,
	Int64_System_IConvertible_ToDouble_m5492,
	Int64_System_IConvertible_ToInt16_m5493,
	Int64_System_IConvertible_ToInt32_m5494,
	Int64_System_IConvertible_ToInt64_m5495,
	Int64_System_IConvertible_ToSByte_m5496,
	Int64_System_IConvertible_ToSingle_m5497,
	Int64_System_IConvertible_ToType_m5498,
	Int64_System_IConvertible_ToUInt16_m5499,
	Int64_System_IConvertible_ToUInt32_m5500,
	Int64_System_IConvertible_ToUInt64_m5501,
	Int64_CompareTo_m5502,
	Int64_Equals_m5503,
	Int64_GetHashCode_m5504,
	Int64_CompareTo_m5505,
	Int64_Equals_m5506,
	Int64_Parse_m5507,
	Int64_Parse_m5508,
	Int64_Parse_m5509,
	Int64_Parse_m5510,
	Int64_Parse_m5511,
	Int64_TryParse_m5512,
	Int64_TryParse_m3334,
	Int64_ToString_m5365,
	Int64_ToString_m3335,
	Int64_ToString_m5513,
	Int64_ToString_m5514,
	UInt32_System_IConvertible_ToBoolean_m5515,
	UInt32_System_IConvertible_ToByte_m5516,
	UInt32_System_IConvertible_ToChar_m5517,
	UInt32_System_IConvertible_ToDateTime_m5518,
	UInt32_System_IConvertible_ToDecimal_m5519,
	UInt32_System_IConvertible_ToDouble_m5520,
	UInt32_System_IConvertible_ToInt16_m5521,
	UInt32_System_IConvertible_ToInt32_m5522,
	UInt32_System_IConvertible_ToInt64_m5523,
	UInt32_System_IConvertible_ToSByte_m5524,
	UInt32_System_IConvertible_ToSingle_m5525,
	UInt32_System_IConvertible_ToType_m5526,
	UInt32_System_IConvertible_ToUInt16_m5527,
	UInt32_System_IConvertible_ToUInt32_m5528,
	UInt32_System_IConvertible_ToUInt64_m5529,
	UInt32_CompareTo_m5530,
	UInt32_Equals_m5531,
	UInt32_GetHashCode_m5532,
	UInt32_CompareTo_m5533,
	UInt32_Equals_m5534,
	UInt32_Parse_m5535,
	UInt32_Parse_m5536,
	UInt32_Parse_m5537,
	UInt32_Parse_m5538,
	UInt32_TryParse_m5420,
	UInt32_TryParse_m3329,
	UInt32_ToString_m3311,
	UInt32_ToString_m3338,
	UInt32_ToString_m5539,
	UInt32_ToString_m5540,
	CLSCompliantAttribute__ctor_m5541,
	UInt64_System_IConvertible_ToBoolean_m5542,
	UInt64_System_IConvertible_ToByte_m5543,
	UInt64_System_IConvertible_ToChar_m5544,
	UInt64_System_IConvertible_ToDateTime_m5545,
	UInt64_System_IConvertible_ToDecimal_m5546,
	UInt64_System_IConvertible_ToDouble_m5547,
	UInt64_System_IConvertible_ToInt16_m5548,
	UInt64_System_IConvertible_ToInt32_m5549,
	UInt64_System_IConvertible_ToInt64_m5550,
	UInt64_System_IConvertible_ToSByte_m5551,
	UInt64_System_IConvertible_ToSingle_m5552,
	UInt64_System_IConvertible_ToType_m5553,
	UInt64_System_IConvertible_ToUInt16_m5554,
	UInt64_System_IConvertible_ToUInt32_m5555,
	UInt64_System_IConvertible_ToUInt64_m5556,
	UInt64_CompareTo_m5557,
	UInt64_Equals_m5558,
	UInt64_GetHashCode_m5559,
	UInt64_CompareTo_m5560,
	UInt64_Equals_m5561,
	UInt64_Parse_m5562,
	UInt64_Parse_m5563,
	UInt64_Parse_m5564,
	UInt64_TryParse_m3309,
	UInt64_ToString_m5565,
	UInt64_ToString_m3336,
	UInt64_ToString_m5566,
	UInt64_ToString_m5567,
	Byte_System_IConvertible_ToType_m5568,
	Byte_System_IConvertible_ToBoolean_m5569,
	Byte_System_IConvertible_ToByte_m5570,
	Byte_System_IConvertible_ToChar_m5571,
	Byte_System_IConvertible_ToDateTime_m5572,
	Byte_System_IConvertible_ToDecimal_m5573,
	Byte_System_IConvertible_ToDouble_m5574,
	Byte_System_IConvertible_ToInt16_m5575,
	Byte_System_IConvertible_ToInt32_m5576,
	Byte_System_IConvertible_ToInt64_m5577,
	Byte_System_IConvertible_ToSByte_m5578,
	Byte_System_IConvertible_ToSingle_m5579,
	Byte_System_IConvertible_ToUInt16_m5580,
	Byte_System_IConvertible_ToUInt32_m5581,
	Byte_System_IConvertible_ToUInt64_m5582,
	Byte_CompareTo_m5583,
	Byte_Equals_m5584,
	Byte_GetHashCode_m5585,
	Byte_CompareTo_m5586,
	Byte_Equals_m5587,
	Byte_Parse_m5588,
	Byte_Parse_m5589,
	Byte_Parse_m5590,
	Byte_TryParse_m5591,
	Byte_TryParse_m5592,
	Byte_ToString_m4401,
	Byte_ToString_m4341,
	Byte_ToString_m4348,
	Byte_ToString_m4352,
	SByte_System_IConvertible_ToBoolean_m5593,
	SByte_System_IConvertible_ToByte_m5594,
	SByte_System_IConvertible_ToChar_m5595,
	SByte_System_IConvertible_ToDateTime_m5596,
	SByte_System_IConvertible_ToDecimal_m5597,
	SByte_System_IConvertible_ToDouble_m5598,
	SByte_System_IConvertible_ToInt16_m5599,
	SByte_System_IConvertible_ToInt32_m5600,
	SByte_System_IConvertible_ToInt64_m5601,
	SByte_System_IConvertible_ToSByte_m5602,
	SByte_System_IConvertible_ToSingle_m5603,
	SByte_System_IConvertible_ToType_m5604,
	SByte_System_IConvertible_ToUInt16_m5605,
	SByte_System_IConvertible_ToUInt32_m5606,
	SByte_System_IConvertible_ToUInt64_m5607,
	SByte_CompareTo_m5608,
	SByte_Equals_m5609,
	SByte_GetHashCode_m5610,
	SByte_CompareTo_m5611,
	SByte_Equals_m5612,
	SByte_Parse_m5613,
	SByte_Parse_m5614,
	SByte_Parse_m5615,
	SByte_TryParse_m5616,
	SByte_ToString_m5617,
	SByte_ToString_m5618,
	SByte_ToString_m5619,
	SByte_ToString_m5620,
	Int16_System_IConvertible_ToBoolean_m5621,
	Int16_System_IConvertible_ToByte_m5622,
	Int16_System_IConvertible_ToChar_m5623,
	Int16_System_IConvertible_ToDateTime_m5624,
	Int16_System_IConvertible_ToDecimal_m5625,
	Int16_System_IConvertible_ToDouble_m5626,
	Int16_System_IConvertible_ToInt16_m5627,
	Int16_System_IConvertible_ToInt32_m5628,
	Int16_System_IConvertible_ToInt64_m5629,
	Int16_System_IConvertible_ToSByte_m5630,
	Int16_System_IConvertible_ToSingle_m5631,
	Int16_System_IConvertible_ToType_m5632,
	Int16_System_IConvertible_ToUInt16_m5633,
	Int16_System_IConvertible_ToUInt32_m5634,
	Int16_System_IConvertible_ToUInt64_m5635,
	Int16_CompareTo_m5636,
	Int16_Equals_m5637,
	Int16_GetHashCode_m5638,
	Int16_CompareTo_m5639,
	Int16_Equals_m5640,
	Int16_Parse_m5641,
	Int16_Parse_m5642,
	Int16_Parse_m5643,
	Int16_TryParse_m5644,
	Int16_ToString_m5645,
	Int16_ToString_m5646,
	Int16_ToString_m5647,
	Int16_ToString_m5648,
	UInt16_System_IConvertible_ToBoolean_m5649,
	UInt16_System_IConvertible_ToByte_m5650,
	UInt16_System_IConvertible_ToChar_m5651,
	UInt16_System_IConvertible_ToDateTime_m5652,
	UInt16_System_IConvertible_ToDecimal_m5653,
	UInt16_System_IConvertible_ToDouble_m5654,
	UInt16_System_IConvertible_ToInt16_m5655,
	UInt16_System_IConvertible_ToInt32_m5656,
	UInt16_System_IConvertible_ToInt64_m5657,
	UInt16_System_IConvertible_ToSByte_m5658,
	UInt16_System_IConvertible_ToSingle_m5659,
	UInt16_System_IConvertible_ToType_m5660,
	UInt16_System_IConvertible_ToUInt16_m5661,
	UInt16_System_IConvertible_ToUInt32_m5662,
	UInt16_System_IConvertible_ToUInt64_m5663,
	UInt16_CompareTo_m5664,
	UInt16_Equals_m5665,
	UInt16_GetHashCode_m5666,
	UInt16_CompareTo_m5667,
	UInt16_Equals_m5668,
	UInt16_Parse_m5669,
	UInt16_Parse_m5670,
	UInt16_TryParse_m5671,
	UInt16_TryParse_m5672,
	UInt16_ToString_m5673,
	UInt16_ToString_m5674,
	UInt16_ToString_m5675,
	UInt16_ToString_m5676,
	Char__cctor_m5677,
	Char_System_IConvertible_ToType_m5678,
	Char_System_IConvertible_ToBoolean_m5679,
	Char_System_IConvertible_ToByte_m5680,
	Char_System_IConvertible_ToChar_m5681,
	Char_System_IConvertible_ToDateTime_m5682,
	Char_System_IConvertible_ToDecimal_m5683,
	Char_System_IConvertible_ToDouble_m5684,
	Char_System_IConvertible_ToInt16_m5685,
	Char_System_IConvertible_ToInt32_m5686,
	Char_System_IConvertible_ToInt64_m5687,
	Char_System_IConvertible_ToSByte_m5688,
	Char_System_IConvertible_ToSingle_m5689,
	Char_System_IConvertible_ToUInt16_m5690,
	Char_System_IConvertible_ToUInt32_m5691,
	Char_System_IConvertible_ToUInt64_m5692,
	Char_GetDataTablePointers_m5693,
	Char_CompareTo_m5694,
	Char_Equals_m5695,
	Char_CompareTo_m5696,
	Char_Equals_m5697,
	Char_GetHashCode_m5698,
	Char_GetUnicodeCategory_m5408,
	Char_IsDigit_m5406,
	Char_IsLetter_m1822,
	Char_IsLetterOrDigit_m5405,
	Char_IsLower_m1823,
	Char_IsSurrogate_m5699,
	Char_IsUpper_m1825,
	Char_IsWhiteSpace_m5407,
	Char_IsWhiteSpace_m5380,
	Char_CheckParameter_m5700,
	Char_Parse_m5701,
	Char_ToLower_m1826,
	Char_ToLowerInvariant_m5702,
	Char_ToLower_m5703,
	Char_ToUpper_m1824,
	Char_ToUpperInvariant_m5382,
	Char_ToString_m1787,
	Char_ToString_m5704,
	Char_GetTypeCode_m5705,
	String__ctor_m5706,
	String__ctor_m5707,
	String__ctor_m5708,
	String__ctor_m5709,
	String__cctor_m5710,
	String_System_IConvertible_ToBoolean_m5711,
	String_System_IConvertible_ToByte_m5712,
	String_System_IConvertible_ToChar_m5713,
	String_System_IConvertible_ToDateTime_m5714,
	String_System_IConvertible_ToDecimal_m5715,
	String_System_IConvertible_ToDouble_m5716,
	String_System_IConvertible_ToInt16_m5717,
	String_System_IConvertible_ToInt32_m5718,
	String_System_IConvertible_ToInt64_m5719,
	String_System_IConvertible_ToSByte_m5720,
	String_System_IConvertible_ToSingle_m5721,
	String_System_IConvertible_ToType_m5722,
	String_System_IConvertible_ToUInt16_m5723,
	String_System_IConvertible_ToUInt32_m5724,
	String_System_IConvertible_ToUInt64_m5725,
	String_System_Collections_Generic_IEnumerableU3CcharU3E_GetEnumerator_m5726,
	String_System_Collections_IEnumerable_GetEnumerator_m5727,
	String_Equals_m5728,
	String_Equals_m5729,
	String_Equals_m4369,
	String_get_Chars_m1763,
	String_CopyTo_m5730,
	String_ToCharArray_m3325,
	String_ToCharArray_m5731,
	String_Split_m219,
	String_Split_m5732,
	String_Split_m5733,
	String_Split_m5734,
	String_Split_m5383,
	String_Substring_m1790,
	String_Substring_m1764,
	String_SubstringUnchecked_m5735,
	String_Trim_m3269,
	String_Trim_m3270,
	String_TrimStart_m5422,
	String_TrimEnd_m5381,
	String_FindNotWhiteSpace_m5736,
	String_FindNotInTable_m5737,
	String_Compare_m5738,
	String_Compare_m5739,
	String_Compare_m4459,
	String_Compare_m4460,
	String_CompareTo_m5740,
	String_CompareTo_m5741,
	String_CompareOrdinal_m5418,
	String_CompareOrdinalUnchecked_m5742,
	String_CompareOrdinalCaseInsensitiveUnchecked_m5743,
	String_EndsWith_m3371,
	String_IndexOfAny_m5417,
	String_IndexOfAny_m1782,
	String_IndexOfAny_m4375,
	String_IndexOfAnyUnchecked_m5744,
	String_IndexOf_m3267,
	String_IndexOf_m5745,
	String_IndexOfOrdinal_m5746,
	String_IndexOfOrdinalUnchecked_m5747,
	String_IndexOfOrdinalIgnoreCaseUnchecked_m5748,
	String_IndexOf_m1827,
	String_IndexOf_m3268,
	String_IndexOf_m5423,
	String_IndexOfUnchecked_m5749,
	String_IndexOf_m3281,
	String_IndexOf_m3372,
	String_IndexOf_m5750,
	String_LastIndexOfAny_m5751,
	String_LastIndexOfAny_m1784,
	String_LastIndexOfAnyUnchecked_m5752,
	String_LastIndexOf_m5367,
	String_LastIndexOf_m5419,
	String_LastIndexOf_m5424,
	String_LastIndexOfUnchecked_m5753,
	String_LastIndexOf_m3375,
	String_LastIndexOf_m5754,
	String_Contains_m1821,
	String_IsNullOrEmpty_m1792,
	String_PadRight_m5755,
	String_StartsWith_m3280,
	String_Replace_m3374,
	String_Replace_m3373,
	String_ReplaceUnchecked_m5756,
	String_ReplaceFallback_m5757,
	String_Remove_m1786,
	String_ToLower_m5411,
	String_ToLower_m5421,
	String_ToLowerInvariant_m5758,
	String_ToUpper_m3282,
	String_ToUpper_m5759,
	String_ToUpperInvariant_m5760,
	String_ToString_m256,
	String_ToString_m5761,
	String_Format_m1610,
	String_Format_m5762,
	String_Format_m5763,
	String_Format_m1918,
	String_Format_m4414,
	String_FormatHelper_m5764,
	String_Concat_m214,
	String_Concat_m239,
	String_Concat_m217,
	String_Concat_m1723,
	String_Concat_m221,
	String_Concat_m252,
	String_Concat_m222,
	String_ConcatInternal_m5765,
	String_Insert_m1788,
	String_Join_m5766,
	String_Join_m5767,
	String_JoinUnchecked_m5768,
	String_get_Length_m1739,
	String_ParseFormatSpecifier_m5769,
	String_ParseDecimal_m5770,
	String_InternalSetChar_m5771,
	String_InternalSetLength_m5772,
	String_GetHashCode_m3247,
	String_GetCaseInsensitiveHashCode_m5773,
	String_CreateString_m5774,
	String_CreateString_m5775,
	String_CreateString_m5776,
	String_CreateString_m5777,
	String_CreateString_m5778,
	String_CreateString_m3327,
	String_CreateString_m3332,
	String_CreateString_m1791,
	String_memcpy4_m5779,
	String_memcpy2_m5780,
	String_memcpy1_m5781,
	String_memcpy_m5782,
	String_CharCopy_m5783,
	String_CharCopyReverse_m5784,
	String_CharCopy_m5785,
	String_CharCopy_m5786,
	String_CharCopyReverse_m5787,
	String_InternalSplit_m5788,
	String_InternalAllocateStr_m5789,
	String_op_Equality_m204,
	String_op_Inequality_m1762,
	Single_System_IConvertible_ToBoolean_m5790,
	Single_System_IConvertible_ToByte_m5791,
	Single_System_IConvertible_ToChar_m5792,
	Single_System_IConvertible_ToDateTime_m5793,
	Single_System_IConvertible_ToDecimal_m5794,
	Single_System_IConvertible_ToDouble_m5795,
	Single_System_IConvertible_ToInt16_m5796,
	Single_System_IConvertible_ToInt32_m5797,
	Single_System_IConvertible_ToInt64_m5798,
	Single_System_IConvertible_ToSByte_m5799,
	Single_System_IConvertible_ToSingle_m5800,
	Single_System_IConvertible_ToType_m5801,
	Single_System_IConvertible_ToUInt16_m5802,
	Single_System_IConvertible_ToUInt32_m5803,
	Single_System_IConvertible_ToUInt64_m5804,
	Single_CompareTo_m5805,
	Single_Equals_m5806,
	Single_CompareTo_m1475,
	Single_Equals_m3262,
	Single_GetHashCode_m3245,
	Single_IsInfinity_m5807,
	Single_IsNaN_m5808,
	Single_IsNegativeInfinity_m5809,
	Single_IsPositiveInfinity_m5810,
	Single_Parse_m243,
	Single_Parse_m5811,
	Single_ToString_m5812,
	Single_ToString_m3340,
	Single_ToString_m3263,
	Single_ToString_m5813,
	Single_GetTypeCode_m5814,
	Double_System_IConvertible_ToType_m5815,
	Double_System_IConvertible_ToBoolean_m5816,
	Double_System_IConvertible_ToByte_m5817,
	Double_System_IConvertible_ToChar_m5818,
	Double_System_IConvertible_ToDateTime_m5819,
	Double_System_IConvertible_ToDecimal_m5820,
	Double_System_IConvertible_ToDouble_m5821,
	Double_System_IConvertible_ToInt16_m5822,
	Double_System_IConvertible_ToInt32_m5823,
	Double_System_IConvertible_ToInt64_m5824,
	Double_System_IConvertible_ToSByte_m5825,
	Double_System_IConvertible_ToSingle_m5826,
	Double_System_IConvertible_ToUInt16_m5827,
	Double_System_IConvertible_ToUInt32_m5828,
	Double_System_IConvertible_ToUInt64_m5829,
	Double_CompareTo_m5830,
	Double_Equals_m5831,
	Double_CompareTo_m5832,
	Double_Equals_m5833,
	Double_GetHashCode_m5834,
	Double_IsInfinity_m5835,
	Double_IsNaN_m5836,
	Double_IsNegativeInfinity_m5837,
	Double_IsPositiveInfinity_m5838,
	Double_Parse_m5839,
	Double_Parse_m5840,
	Double_Parse_m5841,
	Double_Parse_m5842,
	Double_TryParseStringConstant_m5843,
	Double_ParseImpl_m5844,
	Double_TryParse_m3333,
	Double_ToString_m5845,
	Double_ToString_m5846,
	Double_ToString_m3342,
	Decimal__ctor_m5847,
	Decimal__ctor_m5848,
	Decimal__ctor_m5849,
	Decimal__ctor_m5850,
	Decimal__ctor_m5851,
	Decimal__ctor_m5852,
	Decimal__ctor_m5853,
	Decimal__cctor_m5854,
	Decimal_System_IConvertible_ToType_m5855,
	Decimal_System_IConvertible_ToBoolean_m5856,
	Decimal_System_IConvertible_ToByte_m5857,
	Decimal_System_IConvertible_ToChar_m5858,
	Decimal_System_IConvertible_ToDateTime_m5859,
	Decimal_System_IConvertible_ToDecimal_m5860,
	Decimal_System_IConvertible_ToDouble_m5861,
	Decimal_System_IConvertible_ToInt16_m5862,
	Decimal_System_IConvertible_ToInt32_m5863,
	Decimal_System_IConvertible_ToInt64_m5864,
	Decimal_System_IConvertible_ToSByte_m5865,
	Decimal_System_IConvertible_ToSingle_m5866,
	Decimal_System_IConvertible_ToUInt16_m5867,
	Decimal_System_IConvertible_ToUInt32_m5868,
	Decimal_System_IConvertible_ToUInt64_m5869,
	Decimal_GetBits_m5870,
	Decimal_Add_m5871,
	Decimal_Subtract_m5872,
	Decimal_GetHashCode_m5873,
	Decimal_u64_m5874,
	Decimal_s64_m5875,
	Decimal_Equals_m5876,
	Decimal_Equals_m5877,
	Decimal_IsZero_m5878,
	Decimal_Floor_m5879,
	Decimal_Multiply_m5880,
	Decimal_Divide_m5881,
	Decimal_Compare_m5882,
	Decimal_CompareTo_m5883,
	Decimal_CompareTo_m5884,
	Decimal_Equals_m5885,
	Decimal_Parse_m5886,
	Decimal_ThrowAtPos_m5887,
	Decimal_ThrowInvalidExp_m5888,
	Decimal_stripStyles_m5889,
	Decimal_Parse_m5890,
	Decimal_PerformParse_m5891,
	Decimal_ToString_m5892,
	Decimal_ToString_m5893,
	Decimal_ToString_m3339,
	Decimal_decimal2UInt64_m5894,
	Decimal_decimal2Int64_m5895,
	Decimal_decimalIncr_m5896,
	Decimal_string2decimal_m5897,
	Decimal_decimalSetExponent_m5898,
	Decimal_decimal2double_m5899,
	Decimal_decimalFloorAndTrunc_m5900,
	Decimal_decimalMult_m5901,
	Decimal_decimalDiv_m5902,
	Decimal_decimalCompare_m5903,
	Decimal_op_Increment_m5904,
	Decimal_op_Subtraction_m5905,
	Decimal_op_Multiply_m5906,
	Decimal_op_Division_m5907,
	Decimal_op_Explicit_m5908,
	Decimal_op_Explicit_m5909,
	Decimal_op_Explicit_m5910,
	Decimal_op_Explicit_m5911,
	Decimal_op_Explicit_m5912,
	Decimal_op_Explicit_m5913,
	Decimal_op_Explicit_m5914,
	Decimal_op_Explicit_m5915,
	Decimal_op_Implicit_m5916,
	Decimal_op_Implicit_m5917,
	Decimal_op_Implicit_m5918,
	Decimal_op_Implicit_m5919,
	Decimal_op_Implicit_m5920,
	Decimal_op_Implicit_m5921,
	Decimal_op_Implicit_m5922,
	Decimal_op_Implicit_m5923,
	Decimal_op_Explicit_m5924,
	Decimal_op_Explicit_m5925,
	Decimal_op_Explicit_m5926,
	Decimal_op_Explicit_m5927,
	Decimal_op_Inequality_m5928,
	Decimal_op_Equality_m5929,
	Decimal_op_GreaterThan_m5930,
	Decimal_op_LessThan_m5931,
	Boolean__cctor_m5932,
	Boolean_System_IConvertible_ToType_m5933,
	Boolean_System_IConvertible_ToBoolean_m5934,
	Boolean_System_IConvertible_ToByte_m5935,
	Boolean_System_IConvertible_ToChar_m5936,
	Boolean_System_IConvertible_ToDateTime_m5937,
	Boolean_System_IConvertible_ToDecimal_m5938,
	Boolean_System_IConvertible_ToDouble_m5939,
	Boolean_System_IConvertible_ToInt16_m5940,
	Boolean_System_IConvertible_ToInt32_m5941,
	Boolean_System_IConvertible_ToInt64_m5942,
	Boolean_System_IConvertible_ToSByte_m5943,
	Boolean_System_IConvertible_ToSingle_m5944,
	Boolean_System_IConvertible_ToUInt16_m5945,
	Boolean_System_IConvertible_ToUInt32_m5946,
	Boolean_System_IConvertible_ToUInt64_m5947,
	Boolean_CompareTo_m5948,
	Boolean_Equals_m5949,
	Boolean_CompareTo_m5950,
	Boolean_Equals_m5951,
	Boolean_GetHashCode_m5952,
	Boolean_Parse_m5953,
	Boolean_ToString_m3312,
	Boolean_GetTypeCode_m5954,
	Boolean_ToString_m5955,
	IntPtr__ctor_m3287,
	IntPtr__ctor_m5956,
	IntPtr__ctor_m5957,
	IntPtr__ctor_m5958,
	IntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m5959,
	IntPtr_get_Size_m5960,
	IntPtr_Equals_m5961,
	IntPtr_GetHashCode_m5962,
	IntPtr_ToInt64_m5963,
	IntPtr_ToString_m5964,
	IntPtr_ToString_m5965,
	IntPtr_op_Equality_m3380,
	IntPtr_op_Inequality_m3288,
	IntPtr_op_Explicit_m5966,
	IntPtr_op_Explicit_m5967,
	IntPtr_op_Explicit_m5968,
	IntPtr_op_Explicit_m3379,
	IntPtr_op_Explicit_m5969,
	UIntPtr__ctor_m5970,
	UIntPtr__ctor_m5971,
	UIntPtr__ctor_m5972,
	UIntPtr__cctor_m5973,
	UIntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m5974,
	UIntPtr_Equals_m5975,
	UIntPtr_GetHashCode_m5976,
	UIntPtr_ToUInt32_m5977,
	UIntPtr_ToUInt64_m5978,
	UIntPtr_ToPointer_m5979,
	UIntPtr_ToString_m5980,
	UIntPtr_get_Size_m5981,
	UIntPtr_op_Equality_m5982,
	UIntPtr_op_Inequality_m5983,
	UIntPtr_op_Explicit_m5984,
	UIntPtr_op_Explicit_m5985,
	UIntPtr_op_Explicit_m5986,
	UIntPtr_op_Explicit_m5987,
	UIntPtr_op_Explicit_m5988,
	UIntPtr_op_Explicit_m5989,
	MulticastDelegate_GetObjectData_m5990,
	MulticastDelegate_Equals_m5991,
	MulticastDelegate_GetHashCode_m5992,
	MulticastDelegate_GetInvocationList_m5993,
	MulticastDelegate_CombineImpl_m5994,
	MulticastDelegate_BaseEquals_m5995,
	MulticastDelegate_KPM_m5996,
	MulticastDelegate_RemoveImpl_m5997,
	Delegate_get_Method_m3382,
	Delegate_get_Target_m3383,
	Delegate_CreateDelegate_internal_m5998,
	Delegate_SetMulticastInvoke_m5999,
	Delegate_arg_type_match_m6000,
	Delegate_return_type_match_m6001,
	Delegate_CreateDelegate_m6002,
	Delegate_CreateDelegate_m3384,
	Delegate_CreateDelegate_m6003,
	Delegate_CreateDelegate_m6004,
	Delegate_GetCandidateMethod_m6005,
	Delegate_CreateDelegate_m6006,
	Delegate_CreateDelegate_m6007,
	Delegate_CreateDelegate_m6008,
	Delegate_CreateDelegate_m6009,
	Delegate_Clone_m6010,
	Delegate_Equals_m6011,
	Delegate_GetHashCode_m6012,
	Delegate_GetObjectData_m6013,
	Delegate_GetInvocationList_m6014,
	Delegate_Combine_m1656,
	Delegate_Combine_m6015,
	Delegate_CombineImpl_m6016,
	Delegate_Remove_m1657,
	Delegate_RemoveImpl_m6017,
	Enum__ctor_m6018,
	Enum__cctor_m6019,
	Enum_System_IConvertible_ToBoolean_m6020,
	Enum_System_IConvertible_ToByte_m6021,
	Enum_System_IConvertible_ToChar_m6022,
	Enum_System_IConvertible_ToDateTime_m6023,
	Enum_System_IConvertible_ToDecimal_m6024,
	Enum_System_IConvertible_ToDouble_m6025,
	Enum_System_IConvertible_ToInt16_m6026,
	Enum_System_IConvertible_ToInt32_m6027,
	Enum_System_IConvertible_ToInt64_m6028,
	Enum_System_IConvertible_ToSByte_m6029,
	Enum_System_IConvertible_ToSingle_m6030,
	Enum_System_IConvertible_ToType_m6031,
	Enum_System_IConvertible_ToUInt16_m6032,
	Enum_System_IConvertible_ToUInt32_m6033,
	Enum_System_IConvertible_ToUInt64_m6034,
	Enum_GetTypeCode_m6035,
	Enum_get_value_m6036,
	Enum_get_Value_m6037,
	Enum_FindPosition_m6038,
	Enum_GetName_m6039,
	Enum_IsDefined_m4438,
	Enum_get_underlying_type_m6040,
	Enum_GetUnderlyingType_m6041,
	Enum_FindName_m6042,
	Enum_GetValue_m6043,
	Enum_Parse_m5404,
	Enum_compare_value_to_m6044,
	Enum_CompareTo_m6045,
	Enum_ToString_m6046,
	Enum_ToString_m6047,
	Enum_ToString_m3296,
	Enum_ToString_m6048,
	Enum_ToObject_m6049,
	Enum_ToObject_m6050,
	Enum_ToObject_m6051,
	Enum_ToObject_m6052,
	Enum_ToObject_m6053,
	Enum_ToObject_m6054,
	Enum_ToObject_m6055,
	Enum_ToObject_m6056,
	Enum_ToObject_m6057,
	Enum_Equals_m6058,
	Enum_get_hashcode_m6059,
	Enum_GetHashCode_m6060,
	Enum_FormatSpecifier_X_m6061,
	Enum_FormatFlags_m6062,
	Enum_Format_m6063,
	SimpleEnumerator__ctor_m6064,
	SimpleEnumerator_get_Current_m6065,
	SimpleEnumerator_MoveNext_m6066,
	Swapper__ctor_m6067,
	Swapper_Invoke_m6068,
	Swapper_BeginInvoke_m6069,
	Swapper_EndInvoke_m6070,
	Array__ctor_m6071,
	Array_System_Collections_IList_get_Item_m6072,
	Array_System_Collections_IList_set_Item_m6073,
	Array_System_Collections_IList_Add_m6074,
	Array_System_Collections_IList_Clear_m6075,
	Array_System_Collections_IList_Contains_m6076,
	Array_System_Collections_IList_IndexOf_m6077,
	Array_System_Collections_IList_Insert_m6078,
	Array_System_Collections_IList_Remove_m6079,
	Array_System_Collections_IList_RemoveAt_m6080,
	Array_System_Collections_ICollection_get_Count_m6081,
	Array_InternalArray__ICollection_get_Count_m6082,
	Array_InternalArray__ICollection_get_IsReadOnly_m6083,
	Array_InternalArray__ICollection_Clear_m6084,
	Array_InternalArray__RemoveAt_m6085,
	Array_get_Length_m5347,
	Array_get_LongLength_m6086,
	Array_get_Rank_m5350,
	Array_GetRank_m6087,
	Array_GetLength_m6088,
	Array_GetLongLength_m6089,
	Array_GetLowerBound_m6090,
	Array_GetValue_m6091,
	Array_SetValue_m6092,
	Array_GetValueImpl_m6093,
	Array_SetValueImpl_m6094,
	Array_FastCopy_m6095,
	Array_CreateInstanceImpl_m6096,
	Array_get_IsSynchronized_m6097,
	Array_get_SyncRoot_m6098,
	Array_get_IsFixedSize_m6099,
	Array_get_IsReadOnly_m6100,
	Array_GetEnumerator_m6101,
	Array_GetUpperBound_m6102,
	Array_GetValue_m6103,
	Array_GetValue_m6104,
	Array_GetValue_m6105,
	Array_GetValue_m6106,
	Array_GetValue_m6107,
	Array_GetValue_m6108,
	Array_SetValue_m6109,
	Array_SetValue_m6110,
	Array_SetValue_m6111,
	Array_SetValue_m5348,
	Array_SetValue_m6112,
	Array_SetValue_m6113,
	Array_CreateInstance_m6114,
	Array_CreateInstance_m6115,
	Array_CreateInstance_m6116,
	Array_CreateInstance_m6117,
	Array_CreateInstance_m6118,
	Array_GetIntArray_m6119,
	Array_CreateInstance_m6120,
	Array_GetValue_m6121,
	Array_SetValue_m6122,
	Array_BinarySearch_m6123,
	Array_BinarySearch_m6124,
	Array_BinarySearch_m6125,
	Array_BinarySearch_m6126,
	Array_DoBinarySearch_m6127,
	Array_Clear_m3452,
	Array_ClearInternal_m6128,
	Array_Clone_m6129,
	Array_Copy_m5412,
	Array_Copy_m6130,
	Array_Copy_m6131,
	Array_Copy_m6132,
	Array_IndexOf_m6133,
	Array_IndexOf_m6134,
	Array_IndexOf_m6135,
	Array_Initialize_m6136,
	Array_LastIndexOf_m6137,
	Array_LastIndexOf_m6138,
	Array_LastIndexOf_m6139,
	Array_get_swapper_m6140,
	Array_Reverse_m4346,
	Array_Reverse_m4377,
	Array_Sort_m6141,
	Array_Sort_m6142,
	Array_Sort_m6143,
	Array_Sort_m6144,
	Array_Sort_m6145,
	Array_Sort_m6146,
	Array_Sort_m6147,
	Array_Sort_m6148,
	Array_int_swapper_m6149,
	Array_obj_swapper_m6150,
	Array_slow_swapper_m6151,
	Array_double_swapper_m6152,
	Array_new_gap_m6153,
	Array_combsort_m6154,
	Array_combsort_m6155,
	Array_combsort_m6156,
	Array_qsort_m6157,
	Array_swap_m6158,
	Array_compare_m6159,
	Array_CopyTo_m6160,
	Array_CopyTo_m6161,
	Array_ConstrainedCopy_m6162,
	Type__ctor_m6163,
	Type__cctor_m6164,
	Type_FilterName_impl_m6165,
	Type_FilterNameIgnoreCase_impl_m6166,
	Type_FilterAttribute_impl_m6167,
	Type_get_Attributes_m6168,
	Type_get_DeclaringType_m6169,
	Type_get_HasElementType_m6170,
	Type_get_IsAbstract_m6171,
	Type_get_IsArray_m6172,
	Type_get_IsByRef_m6173,
	Type_get_IsClass_m6174,
	Type_get_IsContextful_m6175,
	Type_get_IsEnum_m6176,
	Type_get_IsExplicitLayout_m6177,
	Type_get_IsInterface_m6178,
	Type_get_IsMarshalByRef_m6179,
	Type_get_IsPointer_m6180,
	Type_get_IsPrimitive_m6181,
	Type_get_IsSealed_m6182,
	Type_get_IsSerializable_m6183,
	Type_get_IsValueType_m6184,
	Type_get_MemberType_m6185,
	Type_get_ReflectedType_m6186,
	Type_get_TypeHandle_m6187,
	Type_Equals_m6188,
	Type_Equals_m6189,
	Type_EqualsInternal_m6190,
	Type_internal_from_handle_m6191,
	Type_internal_from_name_m6192,
	Type_GetType_m6193,
	Type_GetType_m3389,
	Type_GetTypeCodeInternal_m6194,
	Type_GetTypeCode_m6195,
	Type_GetTypeFromHandle_m1629,
	Type_GetTypeHandle_m6196,
	Type_type_is_subtype_of_m6197,
	Type_type_is_assignable_from_m6198,
	Type_IsSubclassOf_m6199,
	Type_IsAssignableFrom_m6200,
	Type_IsInstanceOfType_m6201,
	Type_GetHashCode_m6202,
	Type_GetMethod_m6203,
	Type_GetMethod_m6204,
	Type_GetMethod_m6205,
	Type_GetMethod_m6206,
	Type_GetProperty_m6207,
	Type_GetProperty_m6208,
	Type_GetProperty_m6209,
	Type_GetProperty_m6210,
	Type_IsArrayImpl_m6211,
	Type_IsValueTypeImpl_m6212,
	Type_IsContextfulImpl_m6213,
	Type_IsMarshalByRefImpl_m6214,
	Type_GetConstructor_m6215,
	Type_GetConstructor_m6216,
	Type_GetConstructor_m6217,
	Type_GetConstructors_m6218,
	Type_ToString_m6219,
	Type_get_IsSystemType_m6220,
	Type_GetGenericArguments_m6221,
	Type_get_ContainsGenericParameters_m6222,
	Type_get_IsGenericTypeDefinition_m6223,
	Type_GetGenericTypeDefinition_impl_m6224,
	Type_GetGenericTypeDefinition_m6225,
	Type_get_IsGenericType_m6226,
	Type_MakeGenericType_m6227,
	Type_MakeGenericType_m6228,
	Type_get_IsGenericParameter_m6229,
	Type_get_IsNested_m6230,
	Type_GetPseudoCustomAttributes_m6231,
	MemberInfo__ctor_m6232,
	MemberInfo_get_Module_m6233,
	Exception__ctor_m4336,
	Exception__ctor_m3289,
	Exception__ctor_m3378,
	Exception__ctor_m3377,
	Exception_get_InnerException_m6234,
	Exception_set_HResult_m3376,
	Exception_get_ClassName_m6235,
	Exception_get_Message_m6236,
	Exception_get_Source_m6237,
	Exception_get_StackTrace_m6238,
	Exception_GetObjectData_m5428,
	Exception_ToString_m6239,
	Exception_GetFullNameForStackTrace_m6240,
	Exception_GetType_m6241,
	RuntimeFieldHandle__ctor_m6242,
	RuntimeFieldHandle_get_Value_m6243,
	RuntimeFieldHandle_GetObjectData_m6244,
	RuntimeFieldHandle_Equals_m6245,
	RuntimeFieldHandle_GetHashCode_m6246,
	RuntimeTypeHandle__ctor_m6247,
	RuntimeTypeHandle_get_Value_m6248,
	RuntimeTypeHandle_GetObjectData_m6249,
	RuntimeTypeHandle_Equals_m6250,
	RuntimeTypeHandle_GetHashCode_m6251,
	ParamArrayAttribute__ctor_m6252,
	OutAttribute__ctor_m6253,
	ObsoleteAttribute__ctor_m6254,
	ObsoleteAttribute__ctor_m6255,
	ObsoleteAttribute__ctor_m6256,
	DllImportAttribute__ctor_m6257,
	DllImportAttribute_get_Value_m6258,
	MarshalAsAttribute__ctor_m6259,
	InAttribute__ctor_m6260,
	ConditionalAttribute__ctor_m6261,
	GuidAttribute__ctor_m6262,
	ComImportAttribute__ctor_m6263,
	OptionalAttribute__ctor_m6264,
	CompilerGeneratedAttribute__ctor_m6265,
	InternalsVisibleToAttribute__ctor_m6266,
	RuntimeCompatibilityAttribute__ctor_m6267,
	RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m6268,
	DebuggerHiddenAttribute__ctor_m6269,
	DefaultMemberAttribute__ctor_m6270,
	DefaultMemberAttribute_get_MemberName_m6271,
	DecimalConstantAttribute__ctor_m6272,
	FieldOffsetAttribute__ctor_m6273,
	AsyncCallback__ctor_m4437,
	AsyncCallback_Invoke_m6274,
	AsyncCallback_BeginInvoke_m4435,
	AsyncCallback_EndInvoke_m6275,
	TypedReference_Equals_m6276,
	TypedReference_GetHashCode_m6277,
	ArgIterator_Equals_m6278,
	ArgIterator_GetHashCode_m6279,
	MarshalByRefObject__ctor_m5376,
	MarshalByRefObject_get_ObjectIdentity_m6280,
	RuntimeHelpers_InitializeArray_m6281,
	RuntimeHelpers_InitializeArray_m3466,
	RuntimeHelpers_get_OffsetToStringData_m6282,
	Locale_GetText_m6283,
	Locale_GetText_m6284,
	MonoTODOAttribute__ctor_m6285,
	MonoTODOAttribute__ctor_m6286,
	MonoDocumentationNoteAttribute__ctor_m6287,
	SafeHandleZeroOrMinusOneIsInvalid__ctor_m6288,
	SafeHandleZeroOrMinusOneIsInvalid_get_IsInvalid_m6289,
	SafeWaitHandle__ctor_m6290,
	SafeWaitHandle_ReleaseHandle_m6291,
	TableRange__ctor_m6292,
	CodePointIndexer__ctor_m6293,
	CodePointIndexer_ToIndex_m6294,
	TailoringInfo__ctor_m6295,
	Contraction__ctor_m6296,
	ContractionComparer__ctor_m6297,
	ContractionComparer__cctor_m6298,
	ContractionComparer_Compare_m6299,
	Level2Map__ctor_m6300,
	Level2MapComparer__ctor_m6301,
	Level2MapComparer__cctor_m6302,
	Level2MapComparer_Compare_m6303,
	MSCompatUnicodeTable__cctor_m6304,
	MSCompatUnicodeTable_GetTailoringInfo_m6305,
	MSCompatUnicodeTable_BuildTailoringTables_m6306,
	MSCompatUnicodeTable_SetCJKReferences_m6307,
	MSCompatUnicodeTable_Category_m6308,
	MSCompatUnicodeTable_Level1_m6309,
	MSCompatUnicodeTable_Level2_m6310,
	MSCompatUnicodeTable_Level3_m6311,
	MSCompatUnicodeTable_IsIgnorable_m6312,
	MSCompatUnicodeTable_IsIgnorableNonSpacing_m6313,
	MSCompatUnicodeTable_ToKanaTypeInsensitive_m6314,
	MSCompatUnicodeTable_ToWidthCompat_m6315,
	MSCompatUnicodeTable_HasSpecialWeight_m6316,
	MSCompatUnicodeTable_IsHalfWidthKana_m6317,
	MSCompatUnicodeTable_IsHiragana_m6318,
	MSCompatUnicodeTable_IsJapaneseSmallLetter_m6319,
	MSCompatUnicodeTable_get_IsReady_m6320,
	MSCompatUnicodeTable_GetResource_m6321,
	MSCompatUnicodeTable_UInt32FromBytePtr_m6322,
	MSCompatUnicodeTable_FillCJK_m6323,
	MSCompatUnicodeTable_FillCJKCore_m6324,
	MSCompatUnicodeTableUtil__cctor_m6325,
	Context__ctor_m6326,
	PreviousInfo__ctor_m6327,
	SimpleCollator__ctor_m6328,
	SimpleCollator__cctor_m6329,
	SimpleCollator_SetCJKTable_m6330,
	SimpleCollator_GetNeutralCulture_m6331,
	SimpleCollator_Category_m6332,
	SimpleCollator_Level1_m6333,
	SimpleCollator_Level2_m6334,
	SimpleCollator_IsHalfKana_m6335,
	SimpleCollator_GetContraction_m6336,
	SimpleCollator_GetContraction_m6337,
	SimpleCollator_GetTailContraction_m6338,
	SimpleCollator_GetTailContraction_m6339,
	SimpleCollator_FilterOptions_m6340,
	SimpleCollator_GetExtenderType_m6341,
	SimpleCollator_ToDashTypeValue_m6342,
	SimpleCollator_FilterExtender_m6343,
	SimpleCollator_IsIgnorable_m6344,
	SimpleCollator_IsSafe_m6345,
	SimpleCollator_GetSortKey_m6346,
	SimpleCollator_GetSortKey_m6347,
	SimpleCollator_GetSortKey_m6348,
	SimpleCollator_FillSortKeyRaw_m6349,
	SimpleCollator_FillSurrogateSortKeyRaw_m6350,
	SimpleCollator_CompareOrdinal_m6351,
	SimpleCollator_CompareQuick_m6352,
	SimpleCollator_CompareOrdinalIgnoreCase_m6353,
	SimpleCollator_Compare_m6354,
	SimpleCollator_ClearBuffer_m6355,
	SimpleCollator_QuickCheckPossible_m6356,
	SimpleCollator_CompareInternal_m6357,
	SimpleCollator_CompareFlagPair_m6358,
	SimpleCollator_IsPrefix_m6359,
	SimpleCollator_IsPrefix_m6360,
	SimpleCollator_IsPrefix_m6361,
	SimpleCollator_IsSuffix_m6362,
	SimpleCollator_IsSuffix_m6363,
	SimpleCollator_QuickIndexOf_m6364,
	SimpleCollator_IndexOf_m6365,
	SimpleCollator_IndexOfOrdinal_m6366,
	SimpleCollator_IndexOfOrdinalIgnoreCase_m6367,
	SimpleCollator_IndexOfSortKey_m6368,
	SimpleCollator_IndexOf_m6369,
	SimpleCollator_LastIndexOf_m6370,
	SimpleCollator_LastIndexOfOrdinal_m6371,
	SimpleCollator_LastIndexOfOrdinalIgnoreCase_m6372,
	SimpleCollator_LastIndexOfSortKey_m6373,
	SimpleCollator_LastIndexOf_m6374,
	SimpleCollator_MatchesForward_m6375,
	SimpleCollator_MatchesForwardCore_m6376,
	SimpleCollator_MatchesPrimitive_m6377,
	SimpleCollator_MatchesBackward_m6378,
	SimpleCollator_MatchesBackwardCore_m6379,
	SortKey__ctor_m6380,
	SortKey__ctor_m6381,
	SortKey_Compare_m6382,
	SortKey_get_OriginalString_m6383,
	SortKey_get_KeyData_m6384,
	SortKey_Equals_m6385,
	SortKey_GetHashCode_m6386,
	SortKey_ToString_m6387,
	SortKeyBuffer__ctor_m6388,
	SortKeyBuffer_Reset_m6389,
	SortKeyBuffer_Initialize_m6390,
	SortKeyBuffer_AppendCJKExtension_m6391,
	SortKeyBuffer_AppendKana_m6392,
	SortKeyBuffer_AppendNormal_m6393,
	SortKeyBuffer_AppendLevel5_m6394,
	SortKeyBuffer_AppendBufferPrimitive_m6395,
	SortKeyBuffer_GetResultAndReset_m6396,
	SortKeyBuffer_GetOptimizedLength_m6397,
	SortKeyBuffer_GetResult_m6398,
	PrimeGeneratorBase__ctor_m6399,
	PrimeGeneratorBase_get_Confidence_m6400,
	PrimeGeneratorBase_get_PrimalityTest_m6401,
	PrimeGeneratorBase_get_TrialDivisionBounds_m6402,
	SequentialSearchPrimeGeneratorBase__ctor_m6403,
	SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m6404,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m6405,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m6406,
	SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m6407,
	PrimalityTests_GetSPPRounds_m6408,
	PrimalityTests_Test_m6409,
	PrimalityTests_RabinMillerTest_m6410,
	PrimalityTests_SmallPrimeSppTest_m6411,
	ModulusRing__ctor_m6412,
	ModulusRing_BarrettReduction_m6413,
	ModulusRing_Multiply_m6414,
	ModulusRing_Difference_m6415,
	ModulusRing_Pow_m6416,
	ModulusRing_Pow_m6417,
	Kernel_AddSameSign_m6418,
	Kernel_Subtract_m6419,
	Kernel_MinusEq_m6420,
	Kernel_PlusEq_m6421,
	Kernel_Compare_m6422,
	Kernel_SingleByteDivideInPlace_m6423,
	Kernel_DwordMod_m6424,
	Kernel_DwordDivMod_m6425,
	Kernel_multiByteDivide_m6426,
	Kernel_LeftShift_m6427,
	Kernel_RightShift_m6428,
	Kernel_MultiplyByDword_m6429,
	Kernel_Multiply_m6430,
	Kernel_MultiplyMod2p32pmod_m6431,
	Kernel_modInverse_m6432,
	Kernel_modInverse_m6433,
	BigInteger__ctor_m6434,
	BigInteger__ctor_m6435,
	BigInteger__ctor_m6436,
	BigInteger__ctor_m6437,
	BigInteger__ctor_m6438,
	BigInteger__cctor_m6439,
	BigInteger_get_Rng_m6440,
	BigInteger_GenerateRandom_m6441,
	BigInteger_GenerateRandom_m6442,
	BigInteger_Randomize_m6443,
	BigInteger_Randomize_m6444,
	BigInteger_BitCount_m6445,
	BigInteger_TestBit_m6446,
	BigInteger_TestBit_m6447,
	BigInteger_SetBit_m6448,
	BigInteger_SetBit_m6449,
	BigInteger_LowestSetBit_m6450,
	BigInteger_GetBytes_m6451,
	BigInteger_ToString_m6452,
	BigInteger_ToString_m6453,
	BigInteger_Normalize_m6454,
	BigInteger_Clear_m6455,
	BigInteger_GetHashCode_m6456,
	BigInteger_ToString_m6457,
	BigInteger_Equals_m6458,
	BigInteger_ModInverse_m6459,
	BigInteger_ModPow_m6460,
	BigInteger_IsProbablePrime_m6461,
	BigInteger_GeneratePseudoPrime_m6462,
	BigInteger_Incr2_m6463,
	BigInteger_op_Implicit_m6464,
	BigInteger_op_Implicit_m6465,
	BigInteger_op_Addition_m6466,
	BigInteger_op_Subtraction_m6467,
	BigInteger_op_Modulus_m6468,
	BigInteger_op_Modulus_m6469,
	BigInteger_op_Division_m6470,
	BigInteger_op_Multiply_m6471,
	BigInteger_op_Multiply_m6472,
	BigInteger_op_LeftShift_m6473,
	BigInteger_op_RightShift_m6474,
	BigInteger_op_Equality_m6475,
	BigInteger_op_Inequality_m6476,
	BigInteger_op_Equality_m6477,
	BigInteger_op_Inequality_m6478,
	BigInteger_op_GreaterThan_m6479,
	BigInteger_op_LessThan_m6480,
	BigInteger_op_GreaterThanOrEqual_m6481,
	BigInteger_op_LessThanOrEqual_m6482,
	CryptoConvert_ToInt32LE_m6483,
	CryptoConvert_ToUInt32LE_m6484,
	CryptoConvert_GetBytesLE_m6485,
	CryptoConvert_ToCapiPrivateKeyBlob_m6486,
	CryptoConvert_FromCapiPublicKeyBlob_m6487,
	CryptoConvert_FromCapiPublicKeyBlob_m6488,
	CryptoConvert_ToCapiPublicKeyBlob_m6489,
	CryptoConvert_ToCapiKeyBlob_m6490,
	KeyBuilder_get_Rng_m6491,
	KeyBuilder_Key_m6492,
	KeyBuilder_IV_m6493,
	BlockProcessor__ctor_m6494,
	BlockProcessor_Finalize_m6495,
	BlockProcessor_Initialize_m6496,
	BlockProcessor_Core_m6497,
	BlockProcessor_Core_m6498,
	BlockProcessor_Final_m6499,
	KeyGeneratedEventHandler__ctor_m6500,
	KeyGeneratedEventHandler_Invoke_m6501,
	KeyGeneratedEventHandler_BeginInvoke_m6502,
	KeyGeneratedEventHandler_EndInvoke_m6503,
	DSAManaged__ctor_m6504,
	DSAManaged_add_KeyGenerated_m6505,
	DSAManaged_remove_KeyGenerated_m6506,
	DSAManaged_Finalize_m6507,
	DSAManaged_Generate_m6508,
	DSAManaged_GenerateKeyPair_m6509,
	DSAManaged_add_m6510,
	DSAManaged_GenerateParams_m6511,
	DSAManaged_get_Random_m6512,
	DSAManaged_get_KeySize_m6513,
	DSAManaged_get_PublicOnly_m6514,
	DSAManaged_NormalizeArray_m6515,
	DSAManaged_ExportParameters_m6516,
	DSAManaged_ImportParameters_m6517,
	DSAManaged_CreateSignature_m6518,
	DSAManaged_VerifySignature_m6519,
	DSAManaged_Dispose_m6520,
	KeyPairPersistence__ctor_m6521,
	KeyPairPersistence__ctor_m6522,
	KeyPairPersistence__cctor_m6523,
	KeyPairPersistence_get_Filename_m6524,
	KeyPairPersistence_get_KeyValue_m6525,
	KeyPairPersistence_set_KeyValue_m6526,
	KeyPairPersistence_Load_m6527,
	KeyPairPersistence_Save_m6528,
	KeyPairPersistence_Remove_m6529,
	KeyPairPersistence_get_UserPath_m6530,
	KeyPairPersistence_get_MachinePath_m6531,
	KeyPairPersistence__CanSecure_m6532,
	KeyPairPersistence__ProtectUser_m6533,
	KeyPairPersistence__ProtectMachine_m6534,
	KeyPairPersistence__IsUserProtected_m6535,
	KeyPairPersistence__IsMachineProtected_m6536,
	KeyPairPersistence_CanSecure_m6537,
	KeyPairPersistence_ProtectUser_m6538,
	KeyPairPersistence_ProtectMachine_m6539,
	KeyPairPersistence_IsUserProtected_m6540,
	KeyPairPersistence_IsMachineProtected_m6541,
	KeyPairPersistence_get_CanChange_m6542,
	KeyPairPersistence_get_UseDefaultKeyContainer_m6543,
	KeyPairPersistence_get_UseMachineKeyStore_m6544,
	KeyPairPersistence_get_ContainerName_m6545,
	KeyPairPersistence_Copy_m6546,
	KeyPairPersistence_FromXml_m6547,
	KeyPairPersistence_ToXml_m6548,
	MACAlgorithm__ctor_m6549,
	MACAlgorithm_Initialize_m6550,
	MACAlgorithm_Core_m6551,
	MACAlgorithm_Final_m6552,
	PKCS1__cctor_m6553,
	PKCS1_Compare_m6554,
	PKCS1_I2OSP_m6555,
	PKCS1_OS2IP_m6556,
	PKCS1_RSAEP_m6557,
	PKCS1_RSASP1_m6558,
	PKCS1_RSAVP1_m6559,
	PKCS1_Encrypt_v15_m6560,
	PKCS1_Sign_v15_m6561,
	PKCS1_Verify_v15_m6562,
	PKCS1_Verify_v15_m6563,
	PKCS1_Encode_v15_m6564,
	PrivateKeyInfo__ctor_m6565,
	PrivateKeyInfo__ctor_m6566,
	PrivateKeyInfo_get_PrivateKey_m6567,
	PrivateKeyInfo_Decode_m6568,
	PrivateKeyInfo_RemoveLeadingZero_m6569,
	PrivateKeyInfo_Normalize_m6570,
	PrivateKeyInfo_DecodeRSA_m6571,
	PrivateKeyInfo_DecodeDSA_m6572,
	EncryptedPrivateKeyInfo__ctor_m6573,
	EncryptedPrivateKeyInfo__ctor_m6574,
	EncryptedPrivateKeyInfo_get_Algorithm_m6575,
	EncryptedPrivateKeyInfo_get_EncryptedData_m6576,
	EncryptedPrivateKeyInfo_get_Salt_m6577,
	EncryptedPrivateKeyInfo_get_IterationCount_m6578,
	EncryptedPrivateKeyInfo_Decode_m6579,
	KeyGeneratedEventHandler__ctor_m6580,
	KeyGeneratedEventHandler_Invoke_m6581,
	KeyGeneratedEventHandler_BeginInvoke_m6582,
	KeyGeneratedEventHandler_EndInvoke_m6583,
	RSAManaged__ctor_m6584,
	RSAManaged_add_KeyGenerated_m6585,
	RSAManaged_remove_KeyGenerated_m6586,
	RSAManaged_Finalize_m6587,
	RSAManaged_GenerateKeyPair_m6588,
	RSAManaged_get_KeySize_m6589,
	RSAManaged_get_PublicOnly_m6590,
	RSAManaged_DecryptValue_m6591,
	RSAManaged_EncryptValue_m6592,
	RSAManaged_ExportParameters_m6593,
	RSAManaged_ImportParameters_m6594,
	RSAManaged_Dispose_m6595,
	RSAManaged_ToXmlString_m6596,
	RSAManaged_get_IsCrtPossible_m6597,
	RSAManaged_GetPaddedValue_m6598,
	SymmetricTransform__ctor_m6599,
	SymmetricTransform_System_IDisposable_Dispose_m6600,
	SymmetricTransform_Finalize_m6601,
	SymmetricTransform_Dispose_m6602,
	SymmetricTransform_get_CanReuseTransform_m6603,
	SymmetricTransform_Transform_m6604,
	SymmetricTransform_CBC_m6605,
	SymmetricTransform_CFB_m6606,
	SymmetricTransform_OFB_m6607,
	SymmetricTransform_CTS_m6608,
	SymmetricTransform_CheckInput_m6609,
	SymmetricTransform_TransformBlock_m6610,
	SymmetricTransform_get_KeepLastBlock_m6611,
	SymmetricTransform_InternalTransformBlock_m6612,
	SymmetricTransform_Random_m6613,
	SymmetricTransform_ThrowBadPaddingException_m6614,
	SymmetricTransform_FinalEncrypt_m6615,
	SymmetricTransform_FinalDecrypt_m6616,
	SymmetricTransform_TransformFinalBlock_m6617,
	SafeBag__ctor_m6618,
	SafeBag_get_BagOID_m6619,
	SafeBag_get_ASN1_m6620,
	DeriveBytes__ctor_m6621,
	DeriveBytes__cctor_m6622,
	DeriveBytes_set_HashName_m6623,
	DeriveBytes_set_IterationCount_m6624,
	DeriveBytes_set_Password_m6625,
	DeriveBytes_set_Salt_m6626,
	DeriveBytes_Adjust_m6627,
	DeriveBytes_Derive_m6628,
	DeriveBytes_DeriveKey_m6629,
	DeriveBytes_DeriveIV_m6630,
	DeriveBytes_DeriveMAC_m6631,
	PKCS12__ctor_m6632,
	PKCS12__ctor_m6633,
	PKCS12__ctor_m6634,
	PKCS12__cctor_m6635,
	PKCS12_Decode_m6636,
	PKCS12_Finalize_m6637,
	PKCS12_set_Password_m6638,
	PKCS12_get_Certificates_m6639,
	PKCS12_Compare_m6640,
	PKCS12_GetSymmetricAlgorithm_m6641,
	PKCS12_Decrypt_m6642,
	PKCS12_Decrypt_m6643,
	PKCS12_GetExistingParameters_m6644,
	PKCS12_AddPrivateKey_m6645,
	PKCS12_ReadSafeBag_m6646,
	PKCS12_MAC_m6647,
	PKCS12_get_MaximumPasswordLength_m6648,
	X501__cctor_m6649,
	X501_ToString_m6650,
	X501_ToString_m6651,
	X501_AppendEntry_m6652,
	X509Certificate__ctor_m6653,
	X509Certificate__cctor_m6654,
	X509Certificate_Parse_m6655,
	X509Certificate_GetUnsignedBigInteger_m6656,
	X509Certificate_get_DSA_m6657,
	X509Certificate_get_IssuerName_m6658,
	X509Certificate_get_KeyAlgorithmParameters_m6659,
	X509Certificate_get_PublicKey_m6660,
	X509Certificate_get_RawData_m6661,
	X509Certificate_get_SubjectName_m6662,
	X509Certificate_get_ValidFrom_m6663,
	X509Certificate_get_ValidUntil_m6664,
	X509Certificate_GetIssuerName_m6665,
	X509Certificate_GetSubjectName_m6666,
	X509Certificate_GetObjectData_m6667,
	X509Certificate_PEM_m6668,
	X509CertificateEnumerator__ctor_m6669,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m6670,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m6671,
	X509CertificateEnumerator_get_Current_m6672,
	X509CertificateEnumerator_MoveNext_m6673,
	X509CertificateCollection__ctor_m6674,
	X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m6675,
	X509CertificateCollection_get_Item_m6676,
	X509CertificateCollection_Add_m6677,
	X509CertificateCollection_GetEnumerator_m6678,
	X509CertificateCollection_GetHashCode_m6679,
	X509Extension__ctor_m6680,
	X509Extension_Decode_m6681,
	X509Extension_Equals_m6682,
	X509Extension_GetHashCode_m6683,
	X509Extension_WriteLine_m6684,
	X509Extension_ToString_m6685,
	X509ExtensionCollection__ctor_m6686,
	X509ExtensionCollection__ctor_m6687,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m6688,
	ASN1__ctor_m6689,
	ASN1__ctor_m6690,
	ASN1__ctor_m6691,
	ASN1_get_Count_m6692,
	ASN1_get_Tag_m6693,
	ASN1_get_Length_m6694,
	ASN1_get_Value_m6695,
	ASN1_set_Value_m6696,
	ASN1_CompareArray_m6697,
	ASN1_CompareValue_m6698,
	ASN1_Add_m6699,
	ASN1_GetBytes_m6700,
	ASN1_Decode_m6701,
	ASN1_DecodeTLV_m6702,
	ASN1_get_Item_m6703,
	ASN1_Element_m6704,
	ASN1_ToString_m6705,
	ASN1Convert_ToInt32_m6706,
	ASN1Convert_ToOid_m6707,
	ASN1Convert_ToDateTime_m6708,
	BitConverterLE_GetUIntBytes_m6709,
	BitConverterLE_GetBytes_m6710,
	BitConverterLE_UShortFromBytes_m6711,
	BitConverterLE_UIntFromBytes_m6712,
	BitConverterLE_ULongFromBytes_m6713,
	BitConverterLE_ToInt16_m6714,
	BitConverterLE_ToInt32_m6715,
	BitConverterLE_ToSingle_m6716,
	BitConverterLE_ToDouble_m6717,
	ContentInfo__ctor_m6718,
	ContentInfo__ctor_m6719,
	ContentInfo__ctor_m6720,
	ContentInfo__ctor_m6721,
	ContentInfo_get_Content_m6722,
	ContentInfo_set_Content_m6723,
	ContentInfo_get_ContentType_m6724,
	EncryptedData__ctor_m6725,
	EncryptedData__ctor_m6726,
	EncryptedData_get_EncryptionAlgorithm_m6727,
	EncryptedData_get_EncryptedContent_m6728,
	StrongName__cctor_m6729,
	StrongName_get_PublicKey_m6730,
	StrongName_get_PublicKeyToken_m6731,
	StrongName_get_TokenAlgorithm_m6732,
	SecurityParser__ctor_m6733,
	SecurityParser_LoadXml_m6734,
	SecurityParser_ToXml_m6735,
	SecurityParser_OnStartParsing_m6736,
	SecurityParser_OnProcessingInstruction_m6737,
	SecurityParser_OnIgnorableWhitespace_m6738,
	SecurityParser_OnStartElement_m6739,
	SecurityParser_OnEndElement_m6740,
	SecurityParser_OnChars_m6741,
	SecurityParser_OnEndParsing_m6742,
	AttrListImpl__ctor_m6743,
	AttrListImpl_get_Length_m6744,
	AttrListImpl_GetName_m6745,
	AttrListImpl_GetValue_m6746,
	AttrListImpl_GetValue_m6747,
	AttrListImpl_get_Names_m6748,
	AttrListImpl_get_Values_m6749,
	AttrListImpl_Clear_m6750,
	AttrListImpl_Add_m6751,
	SmallXmlParser__ctor_m6752,
	SmallXmlParser_Error_m6753,
	SmallXmlParser_UnexpectedEndError_m6754,
	SmallXmlParser_IsNameChar_m6755,
	SmallXmlParser_IsWhitespace_m6756,
	SmallXmlParser_SkipWhitespaces_m6757,
	SmallXmlParser_HandleWhitespaces_m6758,
	SmallXmlParser_SkipWhitespaces_m6759,
	SmallXmlParser_Peek_m6760,
	SmallXmlParser_Read_m6761,
	SmallXmlParser_Expect_m6762,
	SmallXmlParser_ReadUntil_m6763,
	SmallXmlParser_ReadName_m6764,
	SmallXmlParser_Parse_m6765,
	SmallXmlParser_Cleanup_m6766,
	SmallXmlParser_ReadContent_m6767,
	SmallXmlParser_HandleBufferedContent_m6768,
	SmallXmlParser_ReadCharacters_m6769,
	SmallXmlParser_ReadReference_m6770,
	SmallXmlParser_ReadCharacterReference_m6771,
	SmallXmlParser_ReadAttribute_m6772,
	SmallXmlParser_ReadCDATASection_m6773,
	SmallXmlParser_ReadComment_m6774,
	SmallXmlParserException__ctor_m6775,
	Runtime_GetDisplayName_m6776,
	KeyNotFoundException__ctor_m6777,
	KeyNotFoundException__ctor_m6778,
	SimpleEnumerator__ctor_m6779,
	SimpleEnumerator__cctor_m6780,
	SimpleEnumerator_MoveNext_m6781,
	SimpleEnumerator_get_Current_m6782,
	ArrayListWrapper__ctor_m6783,
	ArrayListWrapper_get_Item_m6784,
	ArrayListWrapper_set_Item_m6785,
	ArrayListWrapper_get_Count_m6786,
	ArrayListWrapper_get_Capacity_m6787,
	ArrayListWrapper_set_Capacity_m6788,
	ArrayListWrapper_get_IsFixedSize_m6789,
	ArrayListWrapper_get_IsReadOnly_m6790,
	ArrayListWrapper_get_IsSynchronized_m6791,
	ArrayListWrapper_get_SyncRoot_m6792,
	ArrayListWrapper_Add_m6793,
	ArrayListWrapper_Clear_m6794,
	ArrayListWrapper_Contains_m6795,
	ArrayListWrapper_IndexOf_m6796,
	ArrayListWrapper_IndexOf_m6797,
	ArrayListWrapper_IndexOf_m6798,
	ArrayListWrapper_Insert_m6799,
	ArrayListWrapper_InsertRange_m6800,
	ArrayListWrapper_Remove_m6801,
	ArrayListWrapper_RemoveAt_m6802,
	ArrayListWrapper_CopyTo_m6803,
	ArrayListWrapper_CopyTo_m6804,
	ArrayListWrapper_CopyTo_m6805,
	ArrayListWrapper_GetEnumerator_m6806,
	ArrayListWrapper_AddRange_m6807,
	ArrayListWrapper_Clone_m6808,
	ArrayListWrapper_Sort_m6809,
	ArrayListWrapper_Sort_m6810,
	ArrayListWrapper_ToArray_m6811,
	ArrayListWrapper_ToArray_m6812,
	SynchronizedArrayListWrapper__ctor_m6813,
	SynchronizedArrayListWrapper_get_Item_m6814,
	SynchronizedArrayListWrapper_set_Item_m6815,
	SynchronizedArrayListWrapper_get_Count_m6816,
	SynchronizedArrayListWrapper_get_Capacity_m6817,
	SynchronizedArrayListWrapper_set_Capacity_m6818,
	SynchronizedArrayListWrapper_get_IsFixedSize_m6819,
	SynchronizedArrayListWrapper_get_IsReadOnly_m6820,
	SynchronizedArrayListWrapper_get_IsSynchronized_m6821,
	SynchronizedArrayListWrapper_get_SyncRoot_m6822,
	SynchronizedArrayListWrapper_Add_m6823,
	SynchronizedArrayListWrapper_Clear_m6824,
	SynchronizedArrayListWrapper_Contains_m6825,
	SynchronizedArrayListWrapper_IndexOf_m6826,
	SynchronizedArrayListWrapper_IndexOf_m6827,
	SynchronizedArrayListWrapper_IndexOf_m6828,
	SynchronizedArrayListWrapper_Insert_m6829,
	SynchronizedArrayListWrapper_InsertRange_m6830,
	SynchronizedArrayListWrapper_Remove_m6831,
	SynchronizedArrayListWrapper_RemoveAt_m6832,
	SynchronizedArrayListWrapper_CopyTo_m6833,
	SynchronizedArrayListWrapper_CopyTo_m6834,
	SynchronizedArrayListWrapper_CopyTo_m6835,
	SynchronizedArrayListWrapper_GetEnumerator_m6836,
	SynchronizedArrayListWrapper_AddRange_m6837,
	SynchronizedArrayListWrapper_Clone_m6838,
	SynchronizedArrayListWrapper_Sort_m6839,
	SynchronizedArrayListWrapper_Sort_m6840,
	SynchronizedArrayListWrapper_ToArray_m6841,
	SynchronizedArrayListWrapper_ToArray_m6842,
	FixedSizeArrayListWrapper__ctor_m6843,
	FixedSizeArrayListWrapper_get_ErrorMessage_m6844,
	FixedSizeArrayListWrapper_get_Capacity_m6845,
	FixedSizeArrayListWrapper_set_Capacity_m6846,
	FixedSizeArrayListWrapper_get_IsFixedSize_m6847,
	FixedSizeArrayListWrapper_Add_m6848,
	FixedSizeArrayListWrapper_AddRange_m6849,
	FixedSizeArrayListWrapper_Clear_m6850,
	FixedSizeArrayListWrapper_Insert_m6851,
	FixedSizeArrayListWrapper_InsertRange_m6852,
	FixedSizeArrayListWrapper_Remove_m6853,
	FixedSizeArrayListWrapper_RemoveAt_m6854,
	ReadOnlyArrayListWrapper__ctor_m6855,
	ReadOnlyArrayListWrapper_get_ErrorMessage_m6856,
	ReadOnlyArrayListWrapper_get_IsReadOnly_m6857,
	ReadOnlyArrayListWrapper_get_Item_m6858,
	ReadOnlyArrayListWrapper_set_Item_m6859,
	ReadOnlyArrayListWrapper_Sort_m6860,
	ReadOnlyArrayListWrapper_Sort_m6861,
	ArrayList__ctor_m4340,
	ArrayList__ctor_m5375,
	ArrayList__ctor_m5396,
	ArrayList__ctor_m6862,
	ArrayList__cctor_m6863,
	ArrayList_get_Item_m6864,
	ArrayList_set_Item_m6865,
	ArrayList_get_Count_m6866,
	ArrayList_get_Capacity_m6867,
	ArrayList_set_Capacity_m6868,
	ArrayList_get_IsFixedSize_m6869,
	ArrayList_get_IsReadOnly_m6870,
	ArrayList_get_IsSynchronized_m6871,
	ArrayList_get_SyncRoot_m6872,
	ArrayList_EnsureCapacity_m6873,
	ArrayList_Shift_m6874,
	ArrayList_Add_m6875,
	ArrayList_Clear_m6876,
	ArrayList_Contains_m6877,
	ArrayList_IndexOf_m6878,
	ArrayList_IndexOf_m6879,
	ArrayList_IndexOf_m6880,
	ArrayList_Insert_m6881,
	ArrayList_InsertRange_m6882,
	ArrayList_Remove_m6883,
	ArrayList_RemoveAt_m6884,
	ArrayList_CopyTo_m6885,
	ArrayList_CopyTo_m6886,
	ArrayList_CopyTo_m6887,
	ArrayList_GetEnumerator_m6888,
	ArrayList_AddRange_m6889,
	ArrayList_Sort_m6890,
	ArrayList_Sort_m6891,
	ArrayList_ToArray_m6892,
	ArrayList_ToArray_m6893,
	ArrayList_Clone_m6894,
	ArrayList_ThrowNewArgumentOutOfRangeException_m6895,
	ArrayList_Synchronized_m6896,
	ArrayList_ReadOnly_m4370,
	BitArrayEnumerator__ctor_m6897,
	BitArrayEnumerator_get_Current_m6898,
	BitArrayEnumerator_MoveNext_m6899,
	BitArrayEnumerator_checkVersion_m6900,
	BitArray__ctor_m5415,
	BitArray_getByte_m6901,
	BitArray_get_Count_m6902,
	BitArray_get_IsSynchronized_m6903,
	BitArray_get_Item_m5410,
	BitArray_set_Item_m5416,
	BitArray_get_Length_m5409,
	BitArray_get_SyncRoot_m6904,
	BitArray_CopyTo_m6905,
	BitArray_Get_m6906,
	BitArray_Set_m6907,
	BitArray_GetEnumerator_m6908,
	CaseInsensitiveComparer__ctor_m6909,
	CaseInsensitiveComparer__ctor_m6910,
	CaseInsensitiveComparer__cctor_m6911,
	CaseInsensitiveComparer_get_DefaultInvariant_m5341,
	CaseInsensitiveComparer_Compare_m6912,
	CaseInsensitiveHashCodeProvider__ctor_m6913,
	CaseInsensitiveHashCodeProvider__ctor_m6914,
	CaseInsensitiveHashCodeProvider__cctor_m6915,
	CaseInsensitiveHashCodeProvider_AreEqual_m6916,
	CaseInsensitiveHashCodeProvider_AreEqual_m6917,
	CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m5342,
	CaseInsensitiveHashCodeProvider_GetHashCode_m6918,
	CollectionBase__ctor_m4390,
	CollectionBase_System_Collections_ICollection_CopyTo_m6919,
	CollectionBase_System_Collections_ICollection_get_SyncRoot_m6920,
	CollectionBase_System_Collections_ICollection_get_IsSynchronized_m6921,
	CollectionBase_System_Collections_IList_Add_m6922,
	CollectionBase_System_Collections_IList_Contains_m6923,
	CollectionBase_System_Collections_IList_IndexOf_m6924,
	CollectionBase_System_Collections_IList_Insert_m6925,
	CollectionBase_System_Collections_IList_Remove_m6926,
	CollectionBase_System_Collections_IList_get_IsFixedSize_m6927,
	CollectionBase_System_Collections_IList_get_IsReadOnly_m6928,
	CollectionBase_System_Collections_IList_get_Item_m6929,
	CollectionBase_System_Collections_IList_set_Item_m6930,
	CollectionBase_get_Count_m6931,
	CollectionBase_GetEnumerator_m6932,
	CollectionBase_Clear_m6933,
	CollectionBase_RemoveAt_m6934,
	CollectionBase_get_InnerList_m4391,
	CollectionBase_get_List_m5413,
	CollectionBase_OnClear_m6935,
	CollectionBase_OnClearComplete_m6936,
	CollectionBase_OnInsert_m6937,
	CollectionBase_OnInsertComplete_m6938,
	CollectionBase_OnRemove_m6939,
	CollectionBase_OnRemoveComplete_m6940,
	CollectionBase_OnSet_m6941,
	CollectionBase_OnSetComplete_m6942,
	CollectionBase_OnValidate_m6943,
	Comparer__ctor_m6944,
	Comparer__ctor_m6945,
	Comparer__cctor_m6946,
	Comparer_Compare_m6947,
	Comparer_GetObjectData_m6948,
	DictionaryEntry__ctor_m5345,
	DictionaryEntry_get_Key_m6949,
	DictionaryEntry_get_Value_m6950,
	KeyMarker__ctor_m6951,
	KeyMarker__cctor_m6952,
	Enumerator__ctor_m6953,
	Enumerator__cctor_m6954,
	Enumerator_FailFast_m6955,
	Enumerator_Reset_m6956,
	Enumerator_MoveNext_m6957,
	Enumerator_get_Entry_m6958,
	Enumerator_get_Key_m6959,
	Enumerator_get_Value_m6960,
	Enumerator_get_Current_m6961,
	HashKeys__ctor_m6962,
	HashKeys_get_Count_m6963,
	HashKeys_get_IsSynchronized_m6964,
	HashKeys_get_SyncRoot_m6965,
	HashKeys_CopyTo_m6966,
	HashKeys_GetEnumerator_m6967,
	HashValues__ctor_m6968,
	HashValues_get_Count_m6969,
	HashValues_get_IsSynchronized_m6970,
	HashValues_get_SyncRoot_m6971,
	HashValues_CopyTo_m6972,
	HashValues_GetEnumerator_m6973,
	Hashtable__ctor_m4416,
	Hashtable__ctor_m6974,
	Hashtable__ctor_m6975,
	Hashtable__ctor_m5403,
	Hashtable__ctor_m6976,
	Hashtable__ctor_m5343,
	Hashtable__ctor_m6977,
	Hashtable__ctor_m5344,
	Hashtable__ctor_m5372,
	Hashtable__ctor_m6978,
	Hashtable__ctor_m5351,
	Hashtable__ctor_m6979,
	Hashtable__cctor_m6980,
	Hashtable_System_Collections_IEnumerable_GetEnumerator_m6981,
	Hashtable_set_comparer_m6982,
	Hashtable_set_hcp_m6983,
	Hashtable_get_Count_m6984,
	Hashtable_get_IsSynchronized_m6985,
	Hashtable_get_SyncRoot_m6986,
	Hashtable_get_Keys_m6987,
	Hashtable_get_Values_m6988,
	Hashtable_get_Item_m6989,
	Hashtable_set_Item_m6990,
	Hashtable_CopyTo_m6991,
	Hashtable_Add_m6992,
	Hashtable_Clear_m6993,
	Hashtable_Contains_m6994,
	Hashtable_GetEnumerator_m6995,
	Hashtable_Remove_m6996,
	Hashtable_ContainsKey_m6997,
	Hashtable_Clone_m6998,
	Hashtable_GetObjectData_m6999,
	Hashtable_OnDeserialization_m7000,
	Hashtable_GetHash_m7001,
	Hashtable_KeyEquals_m7002,
	Hashtable_AdjustThreshold_m7003,
	Hashtable_SetTable_m7004,
	Hashtable_Find_m7005,
	Hashtable_Rehash_m7006,
	Hashtable_PutImpl_m7007,
	Hashtable_CopyToArray_m7008,
	Hashtable_TestPrime_m7009,
	Hashtable_CalcPrime_m7010,
	Hashtable_ToPrime_m7011,
	Enumerator__ctor_m7012,
	Enumerator__cctor_m7013,
	Enumerator_Reset_m7014,
	Enumerator_MoveNext_m7015,
	Enumerator_get_Entry_m7016,
	Enumerator_get_Key_m7017,
	Enumerator_get_Value_m7018,
	Enumerator_get_Current_m7019,
	SortedList__ctor_m7020,
	SortedList__ctor_m5371,
	SortedList__ctor_m7021,
	SortedList__cctor_m7022,
	SortedList_System_Collections_IEnumerable_GetEnumerator_m7023,
	SortedList_get_Count_m7024,
	SortedList_get_IsSynchronized_m7025,
	SortedList_get_SyncRoot_m7026,
	SortedList_get_IsFixedSize_m7027,
	SortedList_get_IsReadOnly_m7028,
	SortedList_get_Item_m7029,
	SortedList_set_Item_m7030,
	SortedList_get_Capacity_m7031,
	SortedList_set_Capacity_m7032,
	SortedList_Add_m7033,
	SortedList_Contains_m7034,
	SortedList_GetEnumerator_m7035,
	SortedList_Remove_m7036,
	SortedList_CopyTo_m7037,
	SortedList_RemoveAt_m7038,
	SortedList_IndexOfKey_m7039,
	SortedList_ContainsKey_m7040,
	SortedList_GetByIndex_m7041,
	SortedList_EnsureCapacity_m7042,
	SortedList_PutImpl_m7043,
	SortedList_GetImpl_m7044,
	SortedList_InitTable_m7045,
	SortedList_Find_m7046,
	Enumerator__ctor_m7047,
	Enumerator_get_Current_m7048,
	Enumerator_MoveNext_m7049,
	Stack__ctor_m3399,
	Stack_Resize_m7050,
	Stack_get_Count_m7051,
	Stack_get_IsSynchronized_m7052,
	Stack_get_SyncRoot_m7053,
	Stack_Clear_m7054,
	Stack_CopyTo_m7055,
	Stack_GetEnumerator_m7056,
	Stack_Peek_m7057,
	Stack_Pop_m7058,
	Stack_Push_m7059,
	SuppressMessageAttribute__ctor_m7060,
	SuppressMessageAttribute_set_Justification_m7061,
	DebuggableAttribute__ctor_m7062,
	DebuggerDisplayAttribute__ctor_m7063,
	DebuggerDisplayAttribute_set_Name_m7064,
	DebuggerStepThroughAttribute__ctor_m7065,
	DebuggerTypeProxyAttribute__ctor_m7066,
	StackFrame__ctor_m7067,
	StackFrame__ctor_m7068,
	StackFrame_get_frame_info_m7069,
	StackFrame_GetFileLineNumber_m7070,
	StackFrame_GetFileName_m7071,
	StackFrame_GetSecureFileName_m7072,
	StackFrame_GetILOffset_m7073,
	StackFrame_GetMethod_m7074,
	StackFrame_GetNativeOffset_m7075,
	StackFrame_GetInternalMethodName_m7076,
	StackFrame_ToString_m7077,
	StackTrace__ctor_m7078,
	StackTrace__ctor_m3370,
	StackTrace__ctor_m7079,
	StackTrace__ctor_m7080,
	StackTrace__ctor_m7081,
	StackTrace_init_frames_m7082,
	StackTrace_get_trace_m7083,
	StackTrace_get_FrameCount_m7084,
	StackTrace_GetFrame_m7085,
	StackTrace_ToString_m7086,
	Calendar__ctor_m7087,
	Calendar_CheckReadOnly_m7088,
	Calendar_get_EraNames_m7089,
	CCMath_div_m7090,
	CCMath_mod_m7091,
	CCMath_div_mod_m7092,
	CCFixed_FromDateTime_m7093,
	CCFixed_day_of_week_m7094,
	CCGregorianCalendar_is_leap_year_m7095,
	CCGregorianCalendar_fixed_from_dmy_m7096,
	CCGregorianCalendar_year_from_fixed_m7097,
	CCGregorianCalendar_my_from_fixed_m7098,
	CCGregorianCalendar_dmy_from_fixed_m7099,
	CCGregorianCalendar_month_from_fixed_m7100,
	CCGregorianCalendar_day_from_fixed_m7101,
	CCGregorianCalendar_GetDayOfMonth_m7102,
	CCGregorianCalendar_GetMonth_m7103,
	CCGregorianCalendar_GetYear_m7104,
	CompareInfo__ctor_m7105,
	CompareInfo__ctor_m7106,
	CompareInfo__cctor_m7107,
	CompareInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7108,
	CompareInfo_get_UseManagedCollation_m7109,
	CompareInfo_construct_compareinfo_m7110,
	CompareInfo_free_internal_collator_m7111,
	CompareInfo_internal_compare_m7112,
	CompareInfo_assign_sortkey_m7113,
	CompareInfo_internal_index_m7114,
	CompareInfo_Finalize_m7115,
	CompareInfo_internal_compare_managed_m7116,
	CompareInfo_internal_compare_switch_m7117,
	CompareInfo_Compare_m7118,
	CompareInfo_Compare_m7119,
	CompareInfo_Compare_m7120,
	CompareInfo_Equals_m7121,
	CompareInfo_GetHashCode_m7122,
	CompareInfo_GetSortKey_m7123,
	CompareInfo_IndexOf_m7124,
	CompareInfo_internal_index_managed_m7125,
	CompareInfo_internal_index_switch_m7126,
	CompareInfo_IndexOf_m7127,
	CompareInfo_IsPrefix_m7128,
	CompareInfo_IsSuffix_m7129,
	CompareInfo_LastIndexOf_m7130,
	CompareInfo_LastIndexOf_m7131,
	CompareInfo_ToString_m7132,
	CompareInfo_get_LCID_m7133,
	CultureInfo__ctor_m7134,
	CultureInfo__ctor_m7135,
	CultureInfo__ctor_m7136,
	CultureInfo__ctor_m7137,
	CultureInfo__ctor_m7138,
	CultureInfo__cctor_m7139,
	CultureInfo_get_InvariantCulture_m3328,
	CultureInfo_get_CurrentCulture_m4411,
	CultureInfo_get_CurrentUICulture_m4413,
	CultureInfo_ConstructCurrentCulture_m7140,
	CultureInfo_ConstructCurrentUICulture_m7141,
	CultureInfo_get_LCID_m7142,
	CultureInfo_get_Name_m7143,
	CultureInfo_get_Parent_m7144,
	CultureInfo_get_TextInfo_m7145,
	CultureInfo_get_IcuName_m7146,
	CultureInfo_Equals_m7147,
	CultureInfo_GetHashCode_m7148,
	CultureInfo_ToString_m7149,
	CultureInfo_get_CompareInfo_m7150,
	CultureInfo_get_IsNeutralCulture_m7151,
	CultureInfo_CheckNeutral_m7152,
	CultureInfo_get_NumberFormat_m7153,
	CultureInfo_get_DateTimeFormat_m7154,
	CultureInfo_get_IsReadOnly_m7155,
	CultureInfo_GetFormat_m7156,
	CultureInfo_Construct_m7157,
	CultureInfo_ConstructInternalLocaleFromName_m7158,
	CultureInfo_ConstructInternalLocaleFromLcid_m7159,
	CultureInfo_ConstructInternalLocaleFromCurrentLocale_m7160,
	CultureInfo_construct_internal_locale_from_lcid_m7161,
	CultureInfo_construct_internal_locale_from_name_m7162,
	CultureInfo_construct_internal_locale_from_current_locale_m7163,
	CultureInfo_construct_datetime_format_m7164,
	CultureInfo_construct_number_format_m7165,
	CultureInfo_ConstructInvariant_m7166,
	CultureInfo_CreateTextInfo_m7167,
	CultureInfo_CreateCulture_m7168,
	DateTimeFormatInfo__ctor_m7169,
	DateTimeFormatInfo__ctor_m7170,
	DateTimeFormatInfo__cctor_m7171,
	DateTimeFormatInfo_GetInstance_m7172,
	DateTimeFormatInfo_get_IsReadOnly_m7173,
	DateTimeFormatInfo_ReadOnly_m7174,
	DateTimeFormatInfo_Clone_m7175,
	DateTimeFormatInfo_GetFormat_m7176,
	DateTimeFormatInfo_GetAbbreviatedMonthName_m7177,
	DateTimeFormatInfo_GetEraName_m7178,
	DateTimeFormatInfo_GetMonthName_m7179,
	DateTimeFormatInfo_get_RawAbbreviatedDayNames_m7180,
	DateTimeFormatInfo_get_RawAbbreviatedMonthNames_m7181,
	DateTimeFormatInfo_get_RawDayNames_m7182,
	DateTimeFormatInfo_get_RawMonthNames_m7183,
	DateTimeFormatInfo_get_AMDesignator_m7184,
	DateTimeFormatInfo_get_PMDesignator_m7185,
	DateTimeFormatInfo_get_DateSeparator_m7186,
	DateTimeFormatInfo_get_TimeSeparator_m7187,
	DateTimeFormatInfo_get_LongDatePattern_m7188,
	DateTimeFormatInfo_get_ShortDatePattern_m7189,
	DateTimeFormatInfo_get_ShortTimePattern_m7190,
	DateTimeFormatInfo_get_LongTimePattern_m7191,
	DateTimeFormatInfo_get_MonthDayPattern_m7192,
	DateTimeFormatInfo_get_YearMonthPattern_m7193,
	DateTimeFormatInfo_get_FullDateTimePattern_m7194,
	DateTimeFormatInfo_get_CurrentInfo_m7195,
	DateTimeFormatInfo_get_InvariantInfo_m7196,
	DateTimeFormatInfo_get_Calendar_m7197,
	DateTimeFormatInfo_set_Calendar_m7198,
	DateTimeFormatInfo_get_RFC1123Pattern_m7199,
	DateTimeFormatInfo_get_RoundtripPattern_m7200,
	DateTimeFormatInfo_get_SortableDateTimePattern_m7201,
	DateTimeFormatInfo_get_UniversalSortableDateTimePattern_m7202,
	DateTimeFormatInfo_GetAllDateTimePatternsInternal_m7203,
	DateTimeFormatInfo_FillAllDateTimePatterns_m7204,
	DateTimeFormatInfo_GetAllRawDateTimePatterns_m7205,
	DateTimeFormatInfo_GetDayName_m7206,
	DateTimeFormatInfo_GetAbbreviatedDayName_m7207,
	DateTimeFormatInfo_FillInvariantPatterns_m7208,
	DateTimeFormatInfo_PopulateCombinedList_m7209,
	DaylightTime__ctor_m7210,
	DaylightTime_get_Start_m7211,
	DaylightTime_get_End_m7212,
	DaylightTime_get_Delta_m7213,
	GregorianCalendar__ctor_m7214,
	GregorianCalendar__ctor_m7215,
	GregorianCalendar_get_Eras_m7216,
	GregorianCalendar_set_CalendarType_m7217,
	GregorianCalendar_GetDayOfMonth_m7218,
	GregorianCalendar_GetDayOfWeek_m7219,
	GregorianCalendar_GetEra_m7220,
	GregorianCalendar_GetMonth_m7221,
	GregorianCalendar_GetYear_m7222,
	NumberFormatInfo__ctor_m7223,
	NumberFormatInfo__ctor_m7224,
	NumberFormatInfo__ctor_m7225,
	NumberFormatInfo__cctor_m7226,
	NumberFormatInfo_get_CurrencyDecimalDigits_m7227,
	NumberFormatInfo_get_CurrencyDecimalSeparator_m7228,
	NumberFormatInfo_get_CurrencyGroupSeparator_m7229,
	NumberFormatInfo_get_RawCurrencyGroupSizes_m7230,
	NumberFormatInfo_get_CurrencyNegativePattern_m7231,
	NumberFormatInfo_get_CurrencyPositivePattern_m7232,
	NumberFormatInfo_get_CurrencySymbol_m7233,
	NumberFormatInfo_get_CurrentInfo_m7234,
	NumberFormatInfo_get_InvariantInfo_m7235,
	NumberFormatInfo_get_NaNSymbol_m7236,
	NumberFormatInfo_get_NegativeInfinitySymbol_m7237,
	NumberFormatInfo_get_NegativeSign_m7238,
	NumberFormatInfo_get_NumberDecimalDigits_m7239,
	NumberFormatInfo_get_NumberDecimalSeparator_m7240,
	NumberFormatInfo_get_NumberGroupSeparator_m7241,
	NumberFormatInfo_get_RawNumberGroupSizes_m7242,
	NumberFormatInfo_get_NumberNegativePattern_m7243,
	NumberFormatInfo_set_NumberNegativePattern_m7244,
	NumberFormatInfo_get_PercentDecimalDigits_m7245,
	NumberFormatInfo_get_PercentDecimalSeparator_m7246,
	NumberFormatInfo_get_PercentGroupSeparator_m7247,
	NumberFormatInfo_get_RawPercentGroupSizes_m7248,
	NumberFormatInfo_get_PercentNegativePattern_m7249,
	NumberFormatInfo_get_PercentPositivePattern_m7250,
	NumberFormatInfo_get_PercentSymbol_m7251,
	NumberFormatInfo_get_PerMilleSymbol_m7252,
	NumberFormatInfo_get_PositiveInfinitySymbol_m7253,
	NumberFormatInfo_get_PositiveSign_m7254,
	NumberFormatInfo_GetFormat_m7255,
	NumberFormatInfo_Clone_m7256,
	NumberFormatInfo_GetInstance_m7257,
	TextInfo__ctor_m7258,
	TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7259,
	TextInfo_get_CultureName_m7260,
	TextInfo_Equals_m7261,
	TextInfo_GetHashCode_m7262,
	TextInfo_ToString_m7263,
	TextInfo_ToLower_m7264,
	TextInfo_ToUpper_m7265,
	TextInfo_ToLower_m7266,
	TextInfo_ToUpper_m7267,
	IsolatedStorageException__ctor_m7268,
	IsolatedStorageException__ctor_m7269,
	IsolatedStorageException__ctor_m7270,
	BinaryReader__ctor_m7271,
	BinaryReader__ctor_m7272,
	BinaryReader_System_IDisposable_Dispose_m7273,
	BinaryReader_Dispose_m7274,
	BinaryReader_FillBuffer_m7275,
	BinaryReader_Read_m7276,
	BinaryReader_Read_m7277,
	BinaryReader_Read_m7278,
	BinaryReader_ReadCharBytes_m7279,
	BinaryReader_Read7BitEncodedInt_m7280,
	BinaryReader_ReadBoolean_m7281,
	BinaryReader_ReadByte_m7282,
	BinaryReader_ReadChar_m7283,
	BinaryReader_ReadDecimal_m7284,
	BinaryReader_ReadDouble_m7285,
	BinaryReader_ReadInt16_m7286,
	BinaryReader_ReadInt32_m7287,
	BinaryReader_ReadInt64_m7288,
	BinaryReader_ReadSByte_m7289,
	BinaryReader_ReadString_m7290,
	BinaryReader_ReadSingle_m7291,
	BinaryReader_ReadUInt16_m7292,
	BinaryReader_ReadUInt32_m7293,
	BinaryReader_ReadUInt64_m7294,
	BinaryReader_CheckBuffer_m7295,
	Directory_CreateDirectory_m4397,
	Directory_CreateDirectoriesInternal_m7296,
	Directory_Exists_m4396,
	Directory_GetCurrentDirectory_m7297,
	Directory_GetFiles_m4399,
	Directory_GetFileSystemEntries_m7298,
	DirectoryInfo__ctor_m7299,
	DirectoryInfo__ctor_m7300,
	DirectoryInfo__ctor_m7301,
	DirectoryInfo_Initialize_m7302,
	DirectoryInfo_get_Exists_m7303,
	DirectoryInfo_get_Parent_m7304,
	DirectoryInfo_Create_m7305,
	DirectoryInfo_ToString_m7306,
	DirectoryNotFoundException__ctor_m7307,
	DirectoryNotFoundException__ctor_m7308,
	DirectoryNotFoundException__ctor_m7309,
	EndOfStreamException__ctor_m7310,
	EndOfStreamException__ctor_m7311,
	File_Delete_m7312,
	File_Exists_m7313,
	File_Open_m7314,
	File_OpenRead_m4395,
	File_OpenText_m7315,
	FileNotFoundException__ctor_m7316,
	FileNotFoundException__ctor_m7317,
	FileNotFoundException__ctor_m7318,
	FileNotFoundException_get_Message_m7319,
	FileNotFoundException_GetObjectData_m7320,
	FileNotFoundException_ToString_m7321,
	ReadDelegate__ctor_m7322,
	ReadDelegate_Invoke_m7323,
	ReadDelegate_BeginInvoke_m7324,
	ReadDelegate_EndInvoke_m7325,
	WriteDelegate__ctor_m7326,
	WriteDelegate_Invoke_m7327,
	WriteDelegate_BeginInvoke_m7328,
	WriteDelegate_EndInvoke_m7329,
	FileStream__ctor_m7330,
	FileStream__ctor_m7331,
	FileStream__ctor_m7332,
	FileStream__ctor_m7333,
	FileStream__ctor_m7334,
	FileStream_get_CanRead_m7335,
	FileStream_get_CanWrite_m7336,
	FileStream_get_CanSeek_m7337,
	FileStream_get_Length_m7338,
	FileStream_get_Position_m7339,
	FileStream_set_Position_m7340,
	FileStream_ReadByte_m7341,
	FileStream_WriteByte_m7342,
	FileStream_Read_m7343,
	FileStream_ReadInternal_m7344,
	FileStream_BeginRead_m7345,
	FileStream_EndRead_m7346,
	FileStream_Write_m7347,
	FileStream_WriteInternal_m7348,
	FileStream_BeginWrite_m7349,
	FileStream_EndWrite_m7350,
	FileStream_Seek_m7351,
	FileStream_SetLength_m7352,
	FileStream_Flush_m7353,
	FileStream_Finalize_m7354,
	FileStream_Dispose_m7355,
	FileStream_ReadSegment_m7356,
	FileStream_WriteSegment_m7357,
	FileStream_FlushBuffer_m7358,
	FileStream_FlushBuffer_m7359,
	FileStream_FlushBufferIfDirty_m7360,
	FileStream_RefillBuffer_m7361,
	FileStream_ReadData_m7362,
	FileStream_InitBuffer_m7363,
	FileStream_GetSecureFileName_m7364,
	FileStream_GetSecureFileName_m7365,
	FileStreamAsyncResult__ctor_m7366,
	FileStreamAsyncResult_CBWrapper_m7367,
	FileStreamAsyncResult_get_AsyncState_m7368,
	FileStreamAsyncResult_get_AsyncWaitHandle_m7369,
	FileStreamAsyncResult_get_IsCompleted_m7370,
	FileSystemInfo__ctor_m7371,
	FileSystemInfo__ctor_m7372,
	FileSystemInfo_GetObjectData_m7373,
	FileSystemInfo_get_FullName_m7374,
	FileSystemInfo_Refresh_m7375,
	FileSystemInfo_InternalRefresh_m7376,
	FileSystemInfo_CheckPath_m7377,
	IOException__ctor_m7378,
	IOException__ctor_m7379,
	IOException__ctor_m4443,
	IOException__ctor_m7380,
	IOException__ctor_m7381,
	MemoryStream__ctor_m4444,
	MemoryStream__ctor_m3286,
	MemoryStream__ctor_m4449,
	MemoryStream_InternalConstructor_m7382,
	MemoryStream_CheckIfClosedThrowDisposed_m7383,
	MemoryStream_get_CanRead_m7384,
	MemoryStream_get_CanSeek_m7385,
	MemoryStream_get_CanWrite_m7386,
	MemoryStream_set_Capacity_m7387,
	MemoryStream_get_Length_m7388,
	MemoryStream_get_Position_m7389,
	MemoryStream_set_Position_m7390,
	MemoryStream_Dispose_m7391,
	MemoryStream_Flush_m7392,
	MemoryStream_Read_m7393,
	MemoryStream_ReadByte_m7394,
	MemoryStream_Seek_m7395,
	MemoryStream_CalculateNewCapacity_m7396,
	MemoryStream_Expand_m7397,
	MemoryStream_SetLength_m7398,
	MemoryStream_ToArray_m7399,
	MemoryStream_Write_m7400,
	MemoryStream_WriteByte_m7401,
	MonoIO__cctor_m7402,
	MonoIO_GetException_m7403,
	MonoIO_GetException_m7404,
	MonoIO_CreateDirectory_m7405,
	MonoIO_GetFileSystemEntries_m7406,
	MonoIO_GetCurrentDirectory_m7407,
	MonoIO_DeleteFile_m7408,
	MonoIO_GetFileAttributes_m7409,
	MonoIO_GetFileType_m7410,
	MonoIO_ExistsFile_m7411,
	MonoIO_ExistsDirectory_m7412,
	MonoIO_GetFileStat_m7413,
	MonoIO_Open_m7414,
	MonoIO_Close_m7415,
	MonoIO_Read_m7416,
	MonoIO_Write_m7417,
	MonoIO_Seek_m7418,
	MonoIO_GetLength_m7419,
	MonoIO_SetLength_m7420,
	MonoIO_get_ConsoleOutput_m7421,
	MonoIO_get_ConsoleInput_m7422,
	MonoIO_get_ConsoleError_m7423,
	MonoIO_get_VolumeSeparatorChar_m7424,
	MonoIO_get_DirectorySeparatorChar_m7425,
	MonoIO_get_AltDirectorySeparatorChar_m7426,
	MonoIO_get_PathSeparator_m7427,
	Path__cctor_m7428,
	Path_Combine_m4398,
	Path_CleanPath_m7429,
	Path_GetDirectoryName_m7430,
	Path_GetFileName_m7431,
	Path_GetFullPath_m7432,
	Path_WindowsDriveAdjustment_m7433,
	Path_InsecureGetFullPath_m7434,
	Path_IsDsc_m7435,
	Path_GetPathRoot_m7436,
	Path_IsPathRooted_m7437,
	Path_GetInvalidPathChars_m7438,
	Path_GetServerAndShare_m7439,
	Path_SameRoot_m7440,
	Path_CanonicalizePath_m7441,
	PathTooLongException__ctor_m7442,
	PathTooLongException__ctor_m7443,
	PathTooLongException__ctor_m7444,
	SearchPattern__cctor_m7445,
	Stream__ctor_m4445,
	Stream__cctor_m7446,
	Stream_Dispose_m7447,
	Stream_Dispose_m4448,
	Stream_Close_m4447,
	Stream_ReadByte_m7448,
	Stream_WriteByte_m7449,
	Stream_BeginRead_m7450,
	Stream_BeginWrite_m7451,
	Stream_EndRead_m7452,
	Stream_EndWrite_m7453,
	NullStream__ctor_m7454,
	NullStream_get_CanRead_m7455,
	NullStream_get_CanSeek_m7456,
	NullStream_get_CanWrite_m7457,
	NullStream_get_Length_m7458,
	NullStream_get_Position_m7459,
	NullStream_set_Position_m7460,
	NullStream_Flush_m7461,
	NullStream_Read_m7462,
	NullStream_ReadByte_m7463,
	NullStream_Seek_m7464,
	NullStream_SetLength_m7465,
	NullStream_Write_m7466,
	NullStream_WriteByte_m7467,
	StreamAsyncResult__ctor_m7468,
	StreamAsyncResult_SetComplete_m7469,
	StreamAsyncResult_SetComplete_m7470,
	StreamAsyncResult_get_AsyncState_m7471,
	StreamAsyncResult_get_AsyncWaitHandle_m7472,
	StreamAsyncResult_get_IsCompleted_m7473,
	StreamAsyncResult_get_Exception_m7474,
	StreamAsyncResult_get_NBytes_m7475,
	StreamAsyncResult_get_Done_m7476,
	StreamAsyncResult_set_Done_m7477,
	NullStreamReader__ctor_m7478,
	NullStreamReader_Peek_m7479,
	NullStreamReader_Read_m7480,
	NullStreamReader_Read_m7481,
	NullStreamReader_ReadLine_m7482,
	NullStreamReader_ReadToEnd_m7483,
	StreamReader__ctor_m7484,
	StreamReader__ctor_m7485,
	StreamReader__ctor_m7486,
	StreamReader__ctor_m7487,
	StreamReader__ctor_m7488,
	StreamReader__cctor_m7489,
	StreamReader_Initialize_m7490,
	StreamReader_Dispose_m7491,
	StreamReader_DoChecks_m7492,
	StreamReader_ReadBuffer_m7493,
	StreamReader_Peek_m7494,
	StreamReader_Read_m7495,
	StreamReader_Read_m7496,
	StreamReader_FindNextEOL_m7497,
	StreamReader_ReadLine_m7498,
	StreamReader_ReadToEnd_m7499,
	StreamWriter__ctor_m7500,
	StreamWriter__ctor_m7501,
	StreamWriter__cctor_m7502,
	StreamWriter_Initialize_m7503,
	StreamWriter_set_AutoFlush_m7504,
	StreamWriter_Dispose_m7505,
	StreamWriter_Flush_m7506,
	StreamWriter_FlushBytes_m7507,
	StreamWriter_Decode_m7508,
	StreamWriter_Write_m7509,
	StreamWriter_LowLevelWrite_m7510,
	StreamWriter_LowLevelWrite_m7511,
	StreamWriter_Write_m7512,
	StreamWriter_Write_m7513,
	StreamWriter_Write_m7514,
	StreamWriter_Close_m7515,
	StreamWriter_Finalize_m7516,
	StringReader__ctor_m3279,
	StringReader_Dispose_m7517,
	StringReader_Peek_m7518,
	StringReader_Read_m7519,
	StringReader_Read_m7520,
	StringReader_ReadLine_m7521,
	StringReader_ReadToEnd_m7522,
	StringReader_CheckObjectDisposedException_m7523,
	NullTextReader__ctor_m7524,
	NullTextReader_ReadLine_m7525,
	TextReader__ctor_m7526,
	TextReader__cctor_m7527,
	TextReader_Dispose_m7528,
	TextReader_Dispose_m7529,
	TextReader_Peek_m7530,
	TextReader_Read_m7531,
	TextReader_Read_m7532,
	TextReader_ReadLine_m7533,
	TextReader_ReadToEnd_m7534,
	TextReader_Synchronized_m7535,
	SynchronizedReader__ctor_m7536,
	SynchronizedReader_Peek_m7537,
	SynchronizedReader_ReadLine_m7538,
	SynchronizedReader_ReadToEnd_m7539,
	SynchronizedReader_Read_m7540,
	SynchronizedReader_Read_m7541,
	NullTextWriter__ctor_m7542,
	NullTextWriter_Write_m7543,
	NullTextWriter_Write_m7544,
	NullTextWriter_Write_m7545,
	TextWriter__ctor_m7546,
	TextWriter__cctor_m7547,
	TextWriter_Close_m7548,
	TextWriter_Dispose_m7549,
	TextWriter_Dispose_m7550,
	TextWriter_Flush_m7551,
	TextWriter_Synchronized_m7552,
	TextWriter_Write_m7553,
	TextWriter_Write_m7554,
	TextWriter_Write_m7555,
	TextWriter_Write_m7556,
	TextWriter_WriteLine_m7557,
	TextWriter_WriteLine_m7558,
	SynchronizedWriter__ctor_m7559,
	SynchronizedWriter_Close_m7560,
	SynchronizedWriter_Flush_m7561,
	SynchronizedWriter_Write_m7562,
	SynchronizedWriter_Write_m7563,
	SynchronizedWriter_Write_m7564,
	SynchronizedWriter_Write_m7565,
	SynchronizedWriter_WriteLine_m7566,
	SynchronizedWriter_WriteLine_m7567,
	UnexceptionalStreamReader__ctor_m7568,
	UnexceptionalStreamReader__cctor_m7569,
	UnexceptionalStreamReader_Peek_m7570,
	UnexceptionalStreamReader_Read_m7571,
	UnexceptionalStreamReader_Read_m7572,
	UnexceptionalStreamReader_CheckEOL_m7573,
	UnexceptionalStreamReader_ReadLine_m7574,
	UnexceptionalStreamReader_ReadToEnd_m7575,
	UnexceptionalStreamWriter__ctor_m7576,
	UnexceptionalStreamWriter_Flush_m7577,
	UnexceptionalStreamWriter_Write_m7578,
	UnexceptionalStreamWriter_Write_m7579,
	UnexceptionalStreamWriter_Write_m7580,
	UnexceptionalStreamWriter_Write_m7581,
	AssemblyBuilder_get_Location_m7582,
	AssemblyBuilder_GetModulesInternal_m7583,
	AssemblyBuilder_GetTypes_m7584,
	AssemblyBuilder_get_IsCompilerContext_m7585,
	AssemblyBuilder_not_supported_m7586,
	AssemblyBuilder_UnprotectedGetName_m7587,
	ConstructorBuilder__ctor_m7588,
	ConstructorBuilder_get_CallingConvention_m7589,
	ConstructorBuilder_get_TypeBuilder_m7590,
	ConstructorBuilder_GetParameters_m7591,
	ConstructorBuilder_GetParametersInternal_m7592,
	ConstructorBuilder_GetParameterCount_m7593,
	ConstructorBuilder_Invoke_m7594,
	ConstructorBuilder_Invoke_m7595,
	ConstructorBuilder_get_MethodHandle_m7596,
	ConstructorBuilder_get_Attributes_m7597,
	ConstructorBuilder_get_ReflectedType_m7598,
	ConstructorBuilder_get_DeclaringType_m7599,
	ConstructorBuilder_get_Name_m7600,
	ConstructorBuilder_IsDefined_m7601,
	ConstructorBuilder_GetCustomAttributes_m7602,
	ConstructorBuilder_GetCustomAttributes_m7603,
	ConstructorBuilder_GetILGenerator_m7604,
	ConstructorBuilder_GetILGenerator_m7605,
	ConstructorBuilder_GetToken_m7606,
	ConstructorBuilder_get_Module_m7607,
	ConstructorBuilder_ToString_m7608,
	ConstructorBuilder_fixup_m7609,
	ConstructorBuilder_get_next_table_index_m7610,
	ConstructorBuilder_get_IsCompilerContext_m7611,
	ConstructorBuilder_not_supported_m7612,
	ConstructorBuilder_not_created_m7613,
	EnumBuilder_get_Assembly_m7614,
	EnumBuilder_get_AssemblyQualifiedName_m7615,
	EnumBuilder_get_BaseType_m7616,
	EnumBuilder_get_DeclaringType_m7617,
	EnumBuilder_get_FullName_m7618,
	EnumBuilder_get_Module_m7619,
	EnumBuilder_get_Name_m7620,
	EnumBuilder_get_Namespace_m7621,
	EnumBuilder_get_ReflectedType_m7622,
	EnumBuilder_get_TypeHandle_m7623,
	EnumBuilder_get_UnderlyingSystemType_m7624,
	EnumBuilder_GetAttributeFlagsImpl_m7625,
	EnumBuilder_GetConstructorImpl_m7626,
	EnumBuilder_GetConstructors_m7627,
	EnumBuilder_GetCustomAttributes_m7628,
	EnumBuilder_GetCustomAttributes_m7629,
	EnumBuilder_GetElementType_m7630,
	EnumBuilder_GetEvent_m7631,
	EnumBuilder_GetField_m7632,
	EnumBuilder_GetFields_m7633,
	EnumBuilder_GetInterfaces_m7634,
	EnumBuilder_GetMethodImpl_m7635,
	EnumBuilder_GetMethods_m7636,
	EnumBuilder_GetProperties_m7637,
	EnumBuilder_GetPropertyImpl_m7638,
	EnumBuilder_HasElementTypeImpl_m7639,
	EnumBuilder_InvokeMember_m7640,
	EnumBuilder_IsArrayImpl_m7641,
	EnumBuilder_IsByRefImpl_m7642,
	EnumBuilder_IsPointerImpl_m7643,
	EnumBuilder_IsPrimitiveImpl_m7644,
	EnumBuilder_IsValueTypeImpl_m7645,
	EnumBuilder_IsDefined_m7646,
	EnumBuilder_CreateNotSupportedException_m7647,
	FieldBuilder_get_Attributes_m7648,
	FieldBuilder_get_DeclaringType_m7649,
	FieldBuilder_get_FieldHandle_m7650,
	FieldBuilder_get_FieldType_m7651,
	FieldBuilder_get_Name_m7652,
	FieldBuilder_get_ReflectedType_m7653,
	FieldBuilder_GetCustomAttributes_m7654,
	FieldBuilder_GetCustomAttributes_m7655,
	FieldBuilder_GetValue_m7656,
	FieldBuilder_IsDefined_m7657,
	FieldBuilder_GetFieldOffset_m7658,
	FieldBuilder_SetValue_m7659,
	FieldBuilder_get_UMarshal_m7660,
	FieldBuilder_CreateNotSupportedException_m7661,
	FieldBuilder_get_Module_m7662,
	GenericTypeParameterBuilder_IsSubclassOf_m7663,
	GenericTypeParameterBuilder_GetAttributeFlagsImpl_m7664,
	GenericTypeParameterBuilder_GetConstructorImpl_m7665,
	GenericTypeParameterBuilder_GetConstructors_m7666,
	GenericTypeParameterBuilder_GetEvent_m7667,
	GenericTypeParameterBuilder_GetField_m7668,
	GenericTypeParameterBuilder_GetFields_m7669,
	GenericTypeParameterBuilder_GetInterfaces_m7670,
	GenericTypeParameterBuilder_GetMethods_m7671,
	GenericTypeParameterBuilder_GetMethodImpl_m7672,
	GenericTypeParameterBuilder_GetProperties_m7673,
	GenericTypeParameterBuilder_GetPropertyImpl_m7674,
	GenericTypeParameterBuilder_HasElementTypeImpl_m7675,
	GenericTypeParameterBuilder_IsAssignableFrom_m7676,
	GenericTypeParameterBuilder_IsInstanceOfType_m7677,
	GenericTypeParameterBuilder_IsArrayImpl_m7678,
	GenericTypeParameterBuilder_IsByRefImpl_m7679,
	GenericTypeParameterBuilder_IsPointerImpl_m7680,
	GenericTypeParameterBuilder_IsPrimitiveImpl_m7681,
	GenericTypeParameterBuilder_IsValueTypeImpl_m7682,
	GenericTypeParameterBuilder_InvokeMember_m7683,
	GenericTypeParameterBuilder_GetElementType_m7684,
	GenericTypeParameterBuilder_get_UnderlyingSystemType_m7685,
	GenericTypeParameterBuilder_get_Assembly_m7686,
	GenericTypeParameterBuilder_get_AssemblyQualifiedName_m7687,
	GenericTypeParameterBuilder_get_BaseType_m7688,
	GenericTypeParameterBuilder_get_FullName_m7689,
	GenericTypeParameterBuilder_IsDefined_m7690,
	GenericTypeParameterBuilder_GetCustomAttributes_m7691,
	GenericTypeParameterBuilder_GetCustomAttributes_m7692,
	GenericTypeParameterBuilder_get_Name_m7693,
	GenericTypeParameterBuilder_get_Namespace_m7694,
	GenericTypeParameterBuilder_get_Module_m7695,
	GenericTypeParameterBuilder_get_DeclaringType_m7696,
	GenericTypeParameterBuilder_get_ReflectedType_m7697,
	GenericTypeParameterBuilder_get_TypeHandle_m7698,
	GenericTypeParameterBuilder_GetGenericArguments_m7699,
	GenericTypeParameterBuilder_GetGenericTypeDefinition_m7700,
	GenericTypeParameterBuilder_get_ContainsGenericParameters_m7701,
	GenericTypeParameterBuilder_get_IsGenericParameter_m7702,
	GenericTypeParameterBuilder_get_IsGenericType_m7703,
	GenericTypeParameterBuilder_get_IsGenericTypeDefinition_m7704,
	GenericTypeParameterBuilder_not_supported_m7705,
	GenericTypeParameterBuilder_ToString_m7706,
	GenericTypeParameterBuilder_Equals_m7707,
	GenericTypeParameterBuilder_GetHashCode_m7708,
	GenericTypeParameterBuilder_MakeGenericType_m7709,
	ILGenerator__ctor_m7710,
	ILGenerator__cctor_m7711,
	ILGenerator_add_token_fixup_m7712,
	ILGenerator_make_room_m7713,
	ILGenerator_emit_int_m7714,
	ILGenerator_ll_emit_m7715,
	ILGenerator_Emit_m7716,
	ILGenerator_Emit_m7717,
	ILGenerator_label_fixup_m7718,
	ILGenerator_Mono_GetCurrentOffset_m7719,
	MethodBuilder_get_ContainsGenericParameters_m7720,
	MethodBuilder_get_MethodHandle_m7721,
	MethodBuilder_get_ReturnType_m7722,
	MethodBuilder_get_ReflectedType_m7723,
	MethodBuilder_get_DeclaringType_m7724,
	MethodBuilder_get_Name_m7725,
	MethodBuilder_get_Attributes_m7726,
	MethodBuilder_get_CallingConvention_m7727,
	MethodBuilder_GetBaseDefinition_m7728,
	MethodBuilder_GetParameters_m7729,
	MethodBuilder_GetParameterCount_m7730,
	MethodBuilder_Invoke_m7731,
	MethodBuilder_IsDefined_m7732,
	MethodBuilder_GetCustomAttributes_m7733,
	MethodBuilder_GetCustomAttributes_m7734,
	MethodBuilder_check_override_m7735,
	MethodBuilder_fixup_m7736,
	MethodBuilder_ToString_m7737,
	MethodBuilder_Equals_m7738,
	MethodBuilder_GetHashCode_m7739,
	MethodBuilder_get_next_table_index_m7740,
	MethodBuilder_NotSupported_m7741,
	MethodBuilder_MakeGenericMethod_m7742,
	MethodBuilder_get_IsGenericMethodDefinition_m7743,
	MethodBuilder_get_IsGenericMethod_m7744,
	MethodBuilder_GetGenericArguments_m7745,
	MethodBuilder_get_Module_m7746,
	MethodToken__ctor_m7747,
	MethodToken__cctor_m7748,
	MethodToken_Equals_m7749,
	MethodToken_GetHashCode_m7750,
	MethodToken_get_Token_m7751,
	ModuleBuilder__cctor_m7752,
	ModuleBuilder_get_next_table_index_m7753,
	ModuleBuilder_GetTypes_m7754,
	ModuleBuilder_getToken_m7755,
	ModuleBuilder_GetToken_m7756,
	ModuleBuilder_RegisterToken_m7757,
	ModuleBuilder_GetTokenGenerator_m7758,
	ModuleBuilderTokenGenerator__ctor_m7759,
	ModuleBuilderTokenGenerator_GetToken_m7760,
	OpCode__ctor_m7761,
	OpCode_GetHashCode_m7762,
	OpCode_Equals_m7763,
	OpCode_ToString_m7764,
	OpCode_get_Name_m7765,
	OpCode_get_Size_m7766,
	OpCode_get_StackBehaviourPop_m7767,
	OpCode_get_StackBehaviourPush_m7768,
	OpCodeNames__cctor_m7769,
	OpCodes__cctor_m7770,
	ParameterBuilder_get_Attributes_m7771,
	ParameterBuilder_get_Name_m7772,
	ParameterBuilder_get_Position_m7773,
	PropertyBuilder_get_Attributes_m7774,
	PropertyBuilder_get_CanRead_m7775,
	PropertyBuilder_get_CanWrite_m7776,
	PropertyBuilder_get_DeclaringType_m7777,
	PropertyBuilder_get_Name_m7778,
	PropertyBuilder_get_PropertyType_m7779,
	PropertyBuilder_get_ReflectedType_m7780,
	PropertyBuilder_GetAccessors_m7781,
	PropertyBuilder_GetCustomAttributes_m7782,
	PropertyBuilder_GetCustomAttributes_m7783,
	PropertyBuilder_GetGetMethod_m7784,
	PropertyBuilder_GetIndexParameters_m7785,
	PropertyBuilder_GetSetMethod_m7786,
	PropertyBuilder_GetValue_m7787,
	PropertyBuilder_GetValue_m7788,
	PropertyBuilder_IsDefined_m7789,
	PropertyBuilder_SetValue_m7790,
	PropertyBuilder_SetValue_m7791,
	PropertyBuilder_get_Module_m7792,
	PropertyBuilder_not_supported_m7793,
	TypeBuilder_GetAttributeFlagsImpl_m7794,
	TypeBuilder_setup_internal_class_m7795,
	TypeBuilder_create_generic_class_m7796,
	TypeBuilder_get_Assembly_m7797,
	TypeBuilder_get_AssemblyQualifiedName_m7798,
	TypeBuilder_get_BaseType_m7799,
	TypeBuilder_get_DeclaringType_m7800,
	TypeBuilder_get_UnderlyingSystemType_m7801,
	TypeBuilder_get_FullName_m7802,
	TypeBuilder_get_Module_m7803,
	TypeBuilder_get_Name_m7804,
	TypeBuilder_get_Namespace_m7805,
	TypeBuilder_get_ReflectedType_m7806,
	TypeBuilder_GetConstructorImpl_m7807,
	TypeBuilder_IsDefined_m7808,
	TypeBuilder_GetCustomAttributes_m7809,
	TypeBuilder_GetCustomAttributes_m7810,
	TypeBuilder_DefineConstructor_m7811,
	TypeBuilder_DefineConstructor_m7812,
	TypeBuilder_DefineDefaultConstructor_m7813,
	TypeBuilder_create_runtime_class_m7814,
	TypeBuilder_is_nested_in_m7815,
	TypeBuilder_has_ctor_method_m7816,
	TypeBuilder_CreateType_m7817,
	TypeBuilder_GetConstructors_m7818,
	TypeBuilder_GetConstructorsInternal_m7819,
	TypeBuilder_GetElementType_m7820,
	TypeBuilder_GetEvent_m7821,
	TypeBuilder_GetField_m7822,
	TypeBuilder_GetFields_m7823,
	TypeBuilder_GetInterfaces_m7824,
	TypeBuilder_GetMethodsByName_m7825,
	TypeBuilder_GetMethods_m7826,
	TypeBuilder_GetMethodImpl_m7827,
	TypeBuilder_GetProperties_m7828,
	TypeBuilder_GetPropertyImpl_m7829,
	TypeBuilder_HasElementTypeImpl_m7830,
	TypeBuilder_InvokeMember_m7831,
	TypeBuilder_IsArrayImpl_m7832,
	TypeBuilder_IsByRefImpl_m7833,
	TypeBuilder_IsPointerImpl_m7834,
	TypeBuilder_IsPrimitiveImpl_m7835,
	TypeBuilder_IsValueTypeImpl_m7836,
	TypeBuilder_MakeGenericType_m7837,
	TypeBuilder_get_TypeHandle_m7838,
	TypeBuilder_SetParent_m7839,
	TypeBuilder_get_next_table_index_m7840,
	TypeBuilder_get_IsCompilerContext_m7841,
	TypeBuilder_get_is_created_m7842,
	TypeBuilder_not_supported_m7843,
	TypeBuilder_check_not_created_m7844,
	TypeBuilder_check_created_m7845,
	TypeBuilder_ToString_m7846,
	TypeBuilder_IsAssignableFrom_m7847,
	TypeBuilder_IsSubclassOf_m7848,
	TypeBuilder_IsAssignableTo_m7849,
	TypeBuilder_GetGenericArguments_m7850,
	TypeBuilder_GetGenericTypeDefinition_m7851,
	TypeBuilder_get_ContainsGenericParameters_m7852,
	TypeBuilder_get_IsGenericParameter_m7853,
	TypeBuilder_get_IsGenericTypeDefinition_m7854,
	TypeBuilder_get_IsGenericType_m7855,
	UnmanagedMarshal_ToMarshalAsAttribute_m7856,
	AmbiguousMatchException__ctor_m7857,
	AmbiguousMatchException__ctor_m7858,
	AmbiguousMatchException__ctor_m7859,
	ResolveEventHolder__ctor_m7860,
	Assembly__ctor_m7861,
	Assembly_get_code_base_m7862,
	Assembly_get_fullname_m7863,
	Assembly_get_location_m7864,
	Assembly_GetCodeBase_m7865,
	Assembly_get_FullName_m7866,
	Assembly_get_Location_m7867,
	Assembly_IsDefined_m7868,
	Assembly_GetCustomAttributes_m7869,
	Assembly_GetManifestResourceInternal_m7870,
	Assembly_GetTypes_m7871,
	Assembly_GetTypes_m7872,
	Assembly_GetType_m7873,
	Assembly_GetType_m7874,
	Assembly_InternalGetType_m7875,
	Assembly_GetType_m7876,
	Assembly_FillName_m7877,
	Assembly_GetName_m7878,
	Assembly_GetName_m7879,
	Assembly_UnprotectedGetName_m7880,
	Assembly_ToString_m7881,
	Assembly_Load_m7882,
	Assembly_GetModule_m7883,
	Assembly_GetModulesInternal_m7884,
	Assembly_GetModules_m7885,
	Assembly_GetExecutingAssembly_m7886,
	AssemblyCompanyAttribute__ctor_m7887,
	AssemblyConfigurationAttribute__ctor_m7888,
	AssemblyCopyrightAttribute__ctor_m7889,
	AssemblyDefaultAliasAttribute__ctor_m7890,
	AssemblyDelaySignAttribute__ctor_m7891,
	AssemblyDescriptionAttribute__ctor_m7892,
	AssemblyFileVersionAttribute__ctor_m7893,
	AssemblyInformationalVersionAttribute__ctor_m7894,
	AssemblyKeyFileAttribute__ctor_m7895,
	AssemblyName__ctor_m7896,
	AssemblyName__ctor_m7897,
	AssemblyName_get_Name_m7898,
	AssemblyName_get_Flags_m7899,
	AssemblyName_get_FullName_m7900,
	AssemblyName_get_Version_m7901,
	AssemblyName_set_Version_m7902,
	AssemblyName_ToString_m7903,
	AssemblyName_get_IsPublicKeyValid_m7904,
	AssemblyName_InternalGetPublicKeyToken_m7905,
	AssemblyName_ComputePublicKeyToken_m7906,
	AssemblyName_SetPublicKey_m7907,
	AssemblyName_SetPublicKeyToken_m7908,
	AssemblyName_GetObjectData_m7909,
	AssemblyName_OnDeserialization_m7910,
	AssemblyProductAttribute__ctor_m7911,
	AssemblyTitleAttribute__ctor_m7912,
	AssemblyTrademarkAttribute__ctor_m7913,
	Default__ctor_m7914,
	Default_BindToMethod_m7915,
	Default_ReorderParameters_m7916,
	Default_IsArrayAssignable_m7917,
	Default_ChangeType_m7918,
	Default_ReorderArgumentArray_m7919,
	Default_check_type_m7920,
	Default_check_arguments_m7921,
	Default_SelectMethod_m7922,
	Default_SelectMethod_m7923,
	Default_GetBetterMethod_m7924,
	Default_CompareCloserType_m7925,
	Default_SelectProperty_m7926,
	Default_check_arguments_with_score_m7927,
	Default_check_type_with_score_m7928,
	Binder__ctor_m7929,
	Binder__cctor_m7930,
	Binder_get_DefaultBinder_m7931,
	Binder_ConvertArgs_m7932,
	Binder_GetDerivedLevel_m7933,
	Binder_FindMostDerivedMatch_m7934,
	ConstructorInfo__ctor_m7935,
	ConstructorInfo__cctor_m7936,
	ConstructorInfo_get_MemberType_m7937,
	ConstructorInfo_Invoke_m3359,
	AddEventAdapter__ctor_m7938,
	AddEventAdapter_Invoke_m7939,
	AddEventAdapter_BeginInvoke_m7940,
	AddEventAdapter_EndInvoke_m7941,
	EventInfo__ctor_m7942,
	EventInfo_get_EventHandlerType_m7943,
	EventInfo_get_MemberType_m7944,
	FieldInfo__ctor_m7945,
	FieldInfo_get_MemberType_m7946,
	FieldInfo_get_IsLiteral_m7947,
	FieldInfo_get_IsStatic_m7948,
	FieldInfo_get_IsInitOnly_m7949,
	FieldInfo_get_IsPublic_m7950,
	FieldInfo_get_IsNotSerialized_m7951,
	FieldInfo_SetValue_m7952,
	FieldInfo_internal_from_handle_type_m7953,
	FieldInfo_GetFieldFromHandle_m7954,
	FieldInfo_GetFieldOffset_m7955,
	FieldInfo_GetUnmanagedMarshal_m7956,
	FieldInfo_get_UMarshal_m7957,
	FieldInfo_GetPseudoCustomAttributes_m7958,
	MemberInfoSerializationHolder__ctor_m7959,
	MemberInfoSerializationHolder_Serialize_m7960,
	MemberInfoSerializationHolder_Serialize_m7961,
	MemberInfoSerializationHolder_GetObjectData_m7962,
	MemberInfoSerializationHolder_GetRealObject_m7963,
	MethodBase__ctor_m7964,
	MethodBase_GetMethodFromHandleNoGenericCheck_m7965,
	MethodBase_GetMethodFromIntPtr_m7966,
	MethodBase_GetMethodFromHandle_m7967,
	MethodBase_GetMethodFromHandleInternalType_m7968,
	MethodBase_GetParameterCount_m7969,
	MethodBase_Invoke_m7970,
	MethodBase_get_CallingConvention_m7971,
	MethodBase_get_IsPublic_m7972,
	MethodBase_get_IsStatic_m7973,
	MethodBase_get_IsVirtual_m7974,
	MethodBase_get_IsAbstract_m7975,
	MethodBase_get_next_table_index_m7976,
	MethodBase_GetGenericArguments_m7977,
	MethodBase_get_ContainsGenericParameters_m7978,
	MethodBase_get_IsGenericMethodDefinition_m7979,
	MethodBase_get_IsGenericMethod_m7980,
	MethodInfo__ctor_m7981,
	MethodInfo_get_MemberType_m7982,
	MethodInfo_get_ReturnType_m7983,
	MethodInfo_MakeGenericMethod_m7984,
	MethodInfo_GetGenericArguments_m7985,
	MethodInfo_get_IsGenericMethod_m7986,
	MethodInfo_get_IsGenericMethodDefinition_m7987,
	MethodInfo_get_ContainsGenericParameters_m7988,
	Missing__ctor_m7989,
	Missing__cctor_m7990,
	Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m7991,
	Module__ctor_m7992,
	Module__cctor_m7993,
	Module_get_Assembly_m7994,
	Module_get_ScopeName_m7995,
	Module_GetCustomAttributes_m7996,
	Module_GetObjectData_m7997,
	Module_InternalGetTypes_m7998,
	Module_GetTypes_m7999,
	Module_IsDefined_m8000,
	Module_IsResource_m8001,
	Module_ToString_m8002,
	Module_filter_by_type_name_m8003,
	Module_filter_by_type_name_ignore_case_m8004,
	MonoEventInfo_get_event_info_m8005,
	MonoEventInfo_GetEventInfo_m8006,
	MonoEvent__ctor_m8007,
	MonoEvent_get_Attributes_m8008,
	MonoEvent_GetAddMethod_m8009,
	MonoEvent_get_DeclaringType_m8010,
	MonoEvent_get_ReflectedType_m8011,
	MonoEvent_get_Name_m8012,
	MonoEvent_ToString_m8013,
	MonoEvent_IsDefined_m8014,
	MonoEvent_GetCustomAttributes_m8015,
	MonoEvent_GetCustomAttributes_m8016,
	MonoEvent_GetObjectData_m8017,
	MonoField__ctor_m8018,
	MonoField_get_Attributes_m8019,
	MonoField_get_FieldHandle_m8020,
	MonoField_get_FieldType_m8021,
	MonoField_GetParentType_m8022,
	MonoField_get_ReflectedType_m8023,
	MonoField_get_DeclaringType_m8024,
	MonoField_get_Name_m8025,
	MonoField_IsDefined_m8026,
	MonoField_GetCustomAttributes_m8027,
	MonoField_GetCustomAttributes_m8028,
	MonoField_GetFieldOffset_m8029,
	MonoField_GetValueInternal_m8030,
	MonoField_GetValue_m8031,
	MonoField_ToString_m8032,
	MonoField_SetValueInternal_m8033,
	MonoField_SetValue_m8034,
	MonoField_GetObjectData_m8035,
	MonoField_CheckGeneric_m8036,
	MonoGenericMethod__ctor_m8037,
	MonoGenericMethod_get_ReflectedType_m8038,
	MonoGenericCMethod__ctor_m8039,
	MonoGenericCMethod_get_ReflectedType_m8040,
	MonoMethodInfo_get_method_info_m8041,
	MonoMethodInfo_GetMethodInfo_m8042,
	MonoMethodInfo_GetDeclaringType_m8043,
	MonoMethodInfo_GetReturnType_m8044,
	MonoMethodInfo_GetAttributes_m8045,
	MonoMethodInfo_GetCallingConvention_m8046,
	MonoMethodInfo_get_parameter_info_m8047,
	MonoMethodInfo_GetParametersInfo_m8048,
	MonoMethod__ctor_m8049,
	MonoMethod_get_name_m8050,
	MonoMethod_get_base_definition_m8051,
	MonoMethod_GetBaseDefinition_m8052,
	MonoMethod_get_ReturnType_m8053,
	MonoMethod_GetParameters_m8054,
	MonoMethod_InternalInvoke_m8055,
	MonoMethod_Invoke_m8056,
	MonoMethod_get_MethodHandle_m8057,
	MonoMethod_get_Attributes_m8058,
	MonoMethod_get_CallingConvention_m8059,
	MonoMethod_get_ReflectedType_m8060,
	MonoMethod_get_DeclaringType_m8061,
	MonoMethod_get_Name_m8062,
	MonoMethod_IsDefined_m8063,
	MonoMethod_GetCustomAttributes_m8064,
	MonoMethod_GetCustomAttributes_m8065,
	MonoMethod_GetDllImportAttribute_m8066,
	MonoMethod_GetPseudoCustomAttributes_m8067,
	MonoMethod_ShouldPrintFullName_m8068,
	MonoMethod_ToString_m8069,
	MonoMethod_GetObjectData_m8070,
	MonoMethod_MakeGenericMethod_m8071,
	MonoMethod_MakeGenericMethod_impl_m8072,
	MonoMethod_GetGenericArguments_m8073,
	MonoMethod_get_IsGenericMethodDefinition_m8074,
	MonoMethod_get_IsGenericMethod_m8075,
	MonoMethod_get_ContainsGenericParameters_m8076,
	MonoCMethod__ctor_m8077,
	MonoCMethod_GetParameters_m8078,
	MonoCMethod_InternalInvoke_m8079,
	MonoCMethod_Invoke_m8080,
	MonoCMethod_Invoke_m8081,
	MonoCMethod_get_MethodHandle_m8082,
	MonoCMethod_get_Attributes_m8083,
	MonoCMethod_get_CallingConvention_m8084,
	MonoCMethod_get_ReflectedType_m8085,
	MonoCMethod_get_DeclaringType_m8086,
	MonoCMethod_get_Name_m8087,
	MonoCMethod_IsDefined_m8088,
	MonoCMethod_GetCustomAttributes_m8089,
	MonoCMethod_GetCustomAttributes_m8090,
	MonoCMethod_ToString_m8091,
	MonoCMethod_GetObjectData_m8092,
	MonoPropertyInfo_get_property_info_m8093,
	MonoPropertyInfo_GetTypeModifiers_m8094,
	GetterAdapter__ctor_m8095,
	GetterAdapter_Invoke_m8096,
	GetterAdapter_BeginInvoke_m8097,
	GetterAdapter_EndInvoke_m8098,
	MonoProperty__ctor_m8099,
	MonoProperty_CachePropertyInfo_m8100,
	MonoProperty_get_Attributes_m8101,
	MonoProperty_get_CanRead_m8102,
	MonoProperty_get_CanWrite_m8103,
	MonoProperty_get_PropertyType_m8104,
	MonoProperty_get_ReflectedType_m8105,
	MonoProperty_get_DeclaringType_m8106,
	MonoProperty_get_Name_m8107,
	MonoProperty_GetAccessors_m8108,
	MonoProperty_GetGetMethod_m8109,
	MonoProperty_GetIndexParameters_m8110,
	MonoProperty_GetSetMethod_m8111,
	MonoProperty_IsDefined_m8112,
	MonoProperty_GetCustomAttributes_m8113,
	MonoProperty_GetCustomAttributes_m8114,
	MonoProperty_CreateGetterDelegate_m8115,
	MonoProperty_GetValue_m8116,
	MonoProperty_GetValue_m8117,
	MonoProperty_SetValue_m8118,
	MonoProperty_ToString_m8119,
	MonoProperty_GetOptionalCustomModifiers_m8120,
	MonoProperty_GetRequiredCustomModifiers_m8121,
	MonoProperty_GetObjectData_m8122,
	ParameterInfo__ctor_m8123,
	ParameterInfo__ctor_m8124,
	ParameterInfo__ctor_m8125,
	ParameterInfo_ToString_m8126,
	ParameterInfo_get_ParameterType_m8127,
	ParameterInfo_get_Attributes_m8128,
	ParameterInfo_get_IsIn_m8129,
	ParameterInfo_get_IsOptional_m8130,
	ParameterInfo_get_IsOut_m8131,
	ParameterInfo_get_IsRetval_m8132,
	ParameterInfo_get_Member_m8133,
	ParameterInfo_get_Name_m8134,
	ParameterInfo_get_Position_m8135,
	ParameterInfo_GetCustomAttributes_m8136,
	ParameterInfo_IsDefined_m8137,
	ParameterInfo_GetPseudoCustomAttributes_m8138,
	Pointer__ctor_m8139,
	Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m8140,
	PropertyInfo__ctor_m8141,
	PropertyInfo_get_MemberType_m8142,
	PropertyInfo_GetValue_m8143,
	PropertyInfo_SetValue_m8144,
	PropertyInfo_GetOptionalCustomModifiers_m8145,
	PropertyInfo_GetRequiredCustomModifiers_m8146,
	StrongNameKeyPair__ctor_m8147,
	StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m8148,
	StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m8149,
	TargetException__ctor_m8150,
	TargetException__ctor_m8151,
	TargetException__ctor_m8152,
	TargetInvocationException__ctor_m8153,
	TargetInvocationException__ctor_m8154,
	TargetParameterCountException__ctor_m8155,
	TargetParameterCountException__ctor_m8156,
	TargetParameterCountException__ctor_m8157,
	NeutralResourcesLanguageAttribute__ctor_m8158,
	SatelliteContractVersionAttribute__ctor_m8159,
	CompilationRelaxationsAttribute__ctor_m8160,
	DefaultDependencyAttribute__ctor_m8161,
	StringFreezingAttribute__ctor_m8162,
	CriticalFinalizerObject__ctor_m8163,
	CriticalFinalizerObject_Finalize_m8164,
	ReliabilityContractAttribute__ctor_m8165,
	ClassInterfaceAttribute__ctor_m8166,
	ComDefaultInterfaceAttribute__ctor_m8167,
	DispIdAttribute__ctor_m8168,
	GCHandle__ctor_m8169,
	GCHandle_get_IsAllocated_m8170,
	GCHandle_get_Target_m8171,
	GCHandle_Alloc_m8172,
	GCHandle_Free_m8173,
	GCHandle_GetTarget_m8174,
	GCHandle_GetTargetHandle_m8175,
	GCHandle_FreeHandle_m8176,
	GCHandle_Equals_m8177,
	GCHandle_GetHashCode_m8178,
	InterfaceTypeAttribute__ctor_m8179,
	Marshal__cctor_m8180,
	Marshal_copy_from_unmanaged_m8181,
	Marshal_Copy_m8182,
	Marshal_Copy_m8183,
	MarshalDirectiveException__ctor_m8184,
	MarshalDirectiveException__ctor_m8185,
	PreserveSigAttribute__ctor_m8186,
	SafeHandle__ctor_m8187,
	SafeHandle_Close_m8188,
	SafeHandle_DangerousAddRef_m8189,
	SafeHandle_DangerousGetHandle_m8190,
	SafeHandle_DangerousRelease_m8191,
	SafeHandle_Dispose_m8192,
	SafeHandle_Dispose_m8193,
	SafeHandle_SetHandle_m8194,
	SafeHandle_Finalize_m8195,
	TypeLibImportClassAttribute__ctor_m8196,
	TypeLibVersionAttribute__ctor_m8197,
	ActivationServices_get_ConstructionActivator_m8198,
	ActivationServices_CreateProxyFromAttributes_m8199,
	ActivationServices_CreateConstructionCall_m8200,
	ActivationServices_AllocateUninitializedClassInstance_m8201,
	ActivationServices_EnableProxyActivation_m8202,
	AppDomainLevelActivator__ctor_m8203,
	ConstructionLevelActivator__ctor_m8204,
	ContextLevelActivator__ctor_m8205,
	UrlAttribute_get_UrlValue_m8206,
	UrlAttribute_Equals_m8207,
	UrlAttribute_GetHashCode_m8208,
	UrlAttribute_GetPropertiesForNewContext_m8209,
	UrlAttribute_IsContextOK_m8210,
	ChannelInfo__ctor_m8211,
	ChannelInfo_get_ChannelData_m8212,
	ChannelServices__cctor_m8213,
	ChannelServices_CreateClientChannelSinkChain_m8214,
	ChannelServices_CreateClientChannelSinkChain_m8215,
	ChannelServices_RegisterChannel_m8216,
	ChannelServices_RegisterChannel_m8217,
	ChannelServices_RegisterChannelConfig_m8218,
	ChannelServices_CreateProvider_m8219,
	ChannelServices_GetCurrentChannelInfo_m8220,
	CrossAppDomainData__ctor_m8221,
	CrossAppDomainData_get_DomainID_m8222,
	CrossAppDomainData_get_ProcessID_m8223,
	CrossAppDomainChannel__ctor_m8224,
	CrossAppDomainChannel__cctor_m8225,
	CrossAppDomainChannel_RegisterCrossAppDomainChannel_m8226,
	CrossAppDomainChannel_get_ChannelName_m8227,
	CrossAppDomainChannel_get_ChannelPriority_m8228,
	CrossAppDomainChannel_get_ChannelData_m8229,
	CrossAppDomainChannel_StartListening_m8230,
	CrossAppDomainChannel_CreateMessageSink_m8231,
	CrossAppDomainSink__ctor_m8232,
	CrossAppDomainSink__cctor_m8233,
	CrossAppDomainSink_GetSink_m8234,
	CrossAppDomainSink_get_TargetDomainId_m8235,
	SinkProviderData__ctor_m8236,
	SinkProviderData_get_Children_m8237,
	SinkProviderData_get_Properties_m8238,
	Context__ctor_m8239,
	Context__cctor_m8240,
	Context_Finalize_m8241,
	Context_get_DefaultContext_m8242,
	Context_get_ContextID_m8243,
	Context_get_ContextProperties_m8244,
	Context_get_IsDefaultContext_m8245,
	Context_get_NeedsContextSink_m8246,
	Context_RegisterDynamicProperty_m8247,
	Context_UnregisterDynamicProperty_m8248,
	Context_GetDynamicPropertyCollection_m8249,
	Context_NotifyGlobalDynamicSinks_m8250,
	Context_get_HasGlobalDynamicSinks_m8251,
	Context_NotifyDynamicSinks_m8252,
	Context_get_HasDynamicSinks_m8253,
	Context_get_HasExitSinks_m8254,
	Context_GetProperty_m8255,
	Context_SetProperty_m8256,
	Context_Freeze_m8257,
	Context_ToString_m8258,
	Context_GetServerContextSinkChain_m8259,
	Context_GetClientContextSinkChain_m8260,
	Context_CreateServerObjectSinkChain_m8261,
	Context_CreateEnvoySink_m8262,
	Context_SwitchToContext_m8263,
	Context_CreateNewContext_m8264,
	Context_DoCallBack_m8265,
	Context_AllocateDataSlot_m8266,
	Context_AllocateNamedDataSlot_m8267,
	Context_FreeNamedDataSlot_m8268,
	Context_GetData_m8269,
	Context_GetNamedDataSlot_m8270,
	Context_SetData_m8271,
	DynamicPropertyReg__ctor_m8272,
	DynamicPropertyCollection__ctor_m8273,
	DynamicPropertyCollection_get_HasProperties_m8274,
	DynamicPropertyCollection_RegisterDynamicProperty_m8275,
	DynamicPropertyCollection_UnregisterDynamicProperty_m8276,
	DynamicPropertyCollection_NotifyMessage_m8277,
	DynamicPropertyCollection_FindProperty_m8278,
	ContextCallbackObject__ctor_m8279,
	ContextCallbackObject_DoCallBack_m8280,
	ContextAttribute__ctor_m8281,
	ContextAttribute_get_Name_m8282,
	ContextAttribute_Equals_m8283,
	ContextAttribute_Freeze_m8284,
	ContextAttribute_GetHashCode_m8285,
	ContextAttribute_GetPropertiesForNewContext_m8286,
	ContextAttribute_IsContextOK_m8287,
	ContextAttribute_IsNewContextOK_m8288,
	CrossContextChannel__ctor_m8289,
	SynchronizationAttribute__ctor_m8290,
	SynchronizationAttribute__ctor_m8291,
	SynchronizationAttribute_set_Locked_m8292,
	SynchronizationAttribute_ReleaseLock_m8293,
	SynchronizationAttribute_GetPropertiesForNewContext_m8294,
	SynchronizationAttribute_GetClientContextSink_m8295,
	SynchronizationAttribute_GetServerContextSink_m8296,
	SynchronizationAttribute_IsContextOK_m8297,
	SynchronizationAttribute_ExitContext_m8298,
	SynchronizationAttribute_EnterContext_m8299,
	SynchronizedClientContextSink__ctor_m8300,
	SynchronizedServerContextSink__ctor_m8301,
	LeaseManager__ctor_m8302,
	LeaseManager_SetPollTime_m8303,
	LeaseSink__ctor_m8304,
	LifetimeServices__cctor_m8305,
	LifetimeServices_set_LeaseManagerPollTime_m8306,
	LifetimeServices_set_LeaseTime_m8307,
	LifetimeServices_set_RenewOnCallTime_m8308,
	LifetimeServices_set_SponsorshipTimeout_m8309,
	ArgInfo__ctor_m8310,
	ArgInfo_GetInOutArgs_m8311,
	AsyncResult__ctor_m8312,
	AsyncResult_get_AsyncState_m8313,
	AsyncResult_get_AsyncWaitHandle_m8314,
	AsyncResult_get_CompletedSynchronously_m8315,
	AsyncResult_get_IsCompleted_m8316,
	AsyncResult_get_EndInvokeCalled_m8317,
	AsyncResult_set_EndInvokeCalled_m8318,
	AsyncResult_get_AsyncDelegate_m8319,
	AsyncResult_get_NextSink_m8320,
	AsyncResult_AsyncProcessMessage_m8321,
	AsyncResult_GetReplyMessage_m8322,
	AsyncResult_SetMessageCtrl_m8323,
	AsyncResult_SetCompletedSynchronously_m8324,
	AsyncResult_EndInvoke_m8325,
	AsyncResult_SyncProcessMessage_m8326,
	AsyncResult_get_CallMessage_m8327,
	AsyncResult_set_CallMessage_m8328,
	ClientContextTerminatorSink__ctor_m8329,
	ConstructionCall__ctor_m8330,
	ConstructionCall__ctor_m8331,
	ConstructionCall_InitDictionary_m8332,
	ConstructionCall_set_IsContextOk_m8333,
	ConstructionCall_get_ActivationType_m8334,
	ConstructionCall_get_ActivationTypeName_m8335,
	ConstructionCall_get_Activator_m8336,
	ConstructionCall_set_Activator_m8337,
	ConstructionCall_get_CallSiteActivationAttributes_m8338,
	ConstructionCall_SetActivationAttributes_m8339,
	ConstructionCall_get_ContextProperties_m8340,
	ConstructionCall_InitMethodProperty_m8341,
	ConstructionCall_GetObjectData_m8342,
	ConstructionCall_get_Properties_m8343,
	ConstructionCallDictionary__ctor_m8344,
	ConstructionCallDictionary__cctor_m8345,
	ConstructionCallDictionary_GetMethodProperty_m8346,
	ConstructionCallDictionary_SetMethodProperty_m8347,
	EnvoyTerminatorSink__ctor_m8348,
	EnvoyTerminatorSink__cctor_m8349,
	Header__ctor_m8350,
	Header__ctor_m8351,
	Header__ctor_m8352,
	LogicalCallContext__ctor_m8353,
	LogicalCallContext__ctor_m8354,
	LogicalCallContext_GetObjectData_m8355,
	LogicalCallContext_SetData_m8356,
	CallContextRemotingData__ctor_m8357,
	MethodCall__ctor_m8358,
	MethodCall__ctor_m8359,
	MethodCall__ctor_m8360,
	MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8361,
	MethodCall_InitMethodProperty_m8362,
	MethodCall_GetObjectData_m8363,
	MethodCall_get_Args_m8364,
	MethodCall_get_LogicalCallContext_m8365,
	MethodCall_get_MethodBase_m8366,
	MethodCall_get_MethodName_m8367,
	MethodCall_get_MethodSignature_m8368,
	MethodCall_get_Properties_m8369,
	MethodCall_InitDictionary_m8370,
	MethodCall_get_TypeName_m8371,
	MethodCall_get_Uri_m8372,
	MethodCall_set_Uri_m8373,
	MethodCall_Init_m8374,
	MethodCall_ResolveMethod_m8375,
	MethodCall_CastTo_m8376,
	MethodCall_GetTypeNameFromAssemblyQualifiedName_m8377,
	MethodCall_get_GenericArguments_m8378,
	MethodCallDictionary__ctor_m8379,
	MethodCallDictionary__cctor_m8380,
	DictionaryEnumerator__ctor_m8381,
	DictionaryEnumerator_get_Current_m8382,
	DictionaryEnumerator_MoveNext_m8383,
	DictionaryEnumerator_get_Entry_m8384,
	DictionaryEnumerator_get_Key_m8385,
	DictionaryEnumerator_get_Value_m8386,
	MethodDictionary__ctor_m8387,
	MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m8388,
	MethodDictionary_set_MethodKeys_m8389,
	MethodDictionary_AllocInternalProperties_m8390,
	MethodDictionary_GetInternalProperties_m8391,
	MethodDictionary_IsOverridenKey_m8392,
	MethodDictionary_get_Item_m8393,
	MethodDictionary_set_Item_m8394,
	MethodDictionary_GetMethodProperty_m8395,
	MethodDictionary_SetMethodProperty_m8396,
	MethodDictionary_get_Values_m8397,
	MethodDictionary_Add_m8398,
	MethodDictionary_Contains_m8399,
	MethodDictionary_Remove_m8400,
	MethodDictionary_get_Count_m8401,
	MethodDictionary_get_IsSynchronized_m8402,
	MethodDictionary_get_SyncRoot_m8403,
	MethodDictionary_CopyTo_m8404,
	MethodDictionary_GetEnumerator_m8405,
	MethodReturnDictionary__ctor_m8406,
	MethodReturnDictionary__cctor_m8407,
	MonoMethodMessage_get_Args_m8408,
	MonoMethodMessage_get_LogicalCallContext_m8409,
	MonoMethodMessage_get_MethodBase_m8410,
	MonoMethodMessage_get_MethodName_m8411,
	MonoMethodMessage_get_MethodSignature_m8412,
	MonoMethodMessage_get_TypeName_m8413,
	MonoMethodMessage_get_Uri_m8414,
	MonoMethodMessage_set_Uri_m8415,
	MonoMethodMessage_get_Exception_m8416,
	MonoMethodMessage_get_OutArgCount_m8417,
	MonoMethodMessage_get_OutArgs_m8418,
	MonoMethodMessage_get_ReturnValue_m8419,
	RemotingSurrogate__ctor_m8420,
	RemotingSurrogate_SetObjectData_m8421,
	ObjRefSurrogate__ctor_m8422,
	ObjRefSurrogate_SetObjectData_m8423,
	RemotingSurrogateSelector__ctor_m8424,
	RemotingSurrogateSelector__cctor_m8425,
	RemotingSurrogateSelector_GetSurrogate_m8426,
	ReturnMessage__ctor_m8427,
	ReturnMessage__ctor_m8428,
	ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8429,
	ReturnMessage_get_Args_m8430,
	ReturnMessage_get_LogicalCallContext_m8431,
	ReturnMessage_get_MethodBase_m8432,
	ReturnMessage_get_MethodName_m8433,
	ReturnMessage_get_MethodSignature_m8434,
	ReturnMessage_get_Properties_m8435,
	ReturnMessage_get_TypeName_m8436,
	ReturnMessage_get_Uri_m8437,
	ReturnMessage_set_Uri_m8438,
	ReturnMessage_get_Exception_m8439,
	ReturnMessage_get_OutArgs_m8440,
	ReturnMessage_get_ReturnValue_m8441,
	ServerContextTerminatorSink__ctor_m8442,
	ServerObjectTerminatorSink__ctor_m8443,
	StackBuilderSink__ctor_m8444,
	SoapAttribute__ctor_m8445,
	SoapAttribute_get_UseAttribute_m8446,
	SoapAttribute_get_XmlNamespace_m8447,
	SoapAttribute_SetReflectionObject_m8448,
	SoapFieldAttribute__ctor_m8449,
	SoapFieldAttribute_get_XmlElementName_m8450,
	SoapFieldAttribute_IsInteropXmlElement_m8451,
	SoapFieldAttribute_SetReflectionObject_m8452,
	SoapMethodAttribute__ctor_m8453,
	SoapMethodAttribute_get_UseAttribute_m8454,
	SoapMethodAttribute_get_XmlNamespace_m8455,
	SoapMethodAttribute_SetReflectionObject_m8456,
	SoapParameterAttribute__ctor_m8457,
	SoapTypeAttribute__ctor_m8458,
	SoapTypeAttribute_get_UseAttribute_m8459,
	SoapTypeAttribute_get_XmlElementName_m8460,
	SoapTypeAttribute_get_XmlNamespace_m8461,
	SoapTypeAttribute_get_XmlTypeName_m8462,
	SoapTypeAttribute_get_XmlTypeNamespace_m8463,
	SoapTypeAttribute_get_IsInteropXmlElement_m8464,
	SoapTypeAttribute_get_IsInteropXmlType_m8465,
	SoapTypeAttribute_SetReflectionObject_m8466,
	ProxyAttribute_CreateInstance_m8467,
	ProxyAttribute_CreateProxy_m8468,
	ProxyAttribute_GetPropertiesForNewContext_m8469,
	ProxyAttribute_IsContextOK_m8470,
	RealProxy__ctor_m8471,
	RealProxy__ctor_m8472,
	RealProxy__ctor_m8473,
	RealProxy_InternalGetProxyType_m8474,
	RealProxy_GetProxiedType_m8475,
	RealProxy_get_ObjectIdentity_m8476,
	RealProxy_InternalGetTransparentProxy_m8477,
	RealProxy_GetTransparentProxy_m8478,
	RealProxy_SetTargetDomain_m8479,
	RemotingProxy__ctor_m8480,
	RemotingProxy__ctor_m8481,
	RemotingProxy__cctor_m8482,
	RemotingProxy_get_TypeName_m8483,
	RemotingProxy_Finalize_m8484,
	TrackingServices__cctor_m8485,
	TrackingServices_NotifyUnmarshaledObject_m8486,
	ActivatedClientTypeEntry__ctor_m8487,
	ActivatedClientTypeEntry_get_ApplicationUrl_m8488,
	ActivatedClientTypeEntry_get_ContextAttributes_m8489,
	ActivatedClientTypeEntry_get_ObjectType_m8490,
	ActivatedClientTypeEntry_ToString_m8491,
	ActivatedServiceTypeEntry__ctor_m8492,
	ActivatedServiceTypeEntry_get_ObjectType_m8493,
	ActivatedServiceTypeEntry_ToString_m8494,
	EnvoyInfo__ctor_m8495,
	EnvoyInfo_get_EnvoySinks_m8496,
	Identity__ctor_m8497,
	Identity_get_ChannelSink_m8498,
	Identity_set_ChannelSink_m8499,
	Identity_get_ObjectUri_m8500,
	Identity_get_Disposed_m8501,
	Identity_set_Disposed_m8502,
	Identity_get_ClientDynamicProperties_m8503,
	Identity_get_ServerDynamicProperties_m8504,
	ClientIdentity__ctor_m8505,
	ClientIdentity_get_ClientProxy_m8506,
	ClientIdentity_set_ClientProxy_m8507,
	ClientIdentity_CreateObjRef_m8508,
	ClientIdentity_get_TargetUri_m8509,
	InternalRemotingServices__cctor_m8510,
	InternalRemotingServices_GetCachedSoapAttribute_m8511,
	ObjRef__ctor_m8512,
	ObjRef__ctor_m8513,
	ObjRef__cctor_m8514,
	ObjRef_get_IsReferenceToWellKnow_m8515,
	ObjRef_get_ChannelInfo_m8516,
	ObjRef_get_EnvoyInfo_m8517,
	ObjRef_set_EnvoyInfo_m8518,
	ObjRef_get_TypeInfo_m8519,
	ObjRef_set_TypeInfo_m8520,
	ObjRef_get_URI_m8521,
	ObjRef_set_URI_m8522,
	ObjRef_GetObjectData_m8523,
	ObjRef_GetRealObject_m8524,
	ObjRef_UpdateChannelInfo_m8525,
	ObjRef_get_ServerType_m8526,
	RemotingConfiguration__cctor_m8527,
	RemotingConfiguration_get_ApplicationName_m8528,
	RemotingConfiguration_set_ApplicationName_m8529,
	RemotingConfiguration_get_ProcessId_m8530,
	RemotingConfiguration_LoadDefaultDelayedChannels_m8531,
	RemotingConfiguration_IsRemotelyActivatedClientType_m8532,
	RemotingConfiguration_RegisterActivatedClientType_m8533,
	RemotingConfiguration_RegisterActivatedServiceType_m8534,
	RemotingConfiguration_RegisterWellKnownClientType_m8535,
	RemotingConfiguration_RegisterWellKnownServiceType_m8536,
	RemotingConfiguration_RegisterChannelTemplate_m8537,
	RemotingConfiguration_RegisterClientProviderTemplate_m8538,
	RemotingConfiguration_RegisterServerProviderTemplate_m8539,
	RemotingConfiguration_RegisterChannels_m8540,
	RemotingConfiguration_RegisterTypes_m8541,
	RemotingConfiguration_SetCustomErrorsMode_m8542,
	ConfigHandler__ctor_m8543,
	ConfigHandler_ValidatePath_m8544,
	ConfigHandler_CheckPath_m8545,
	ConfigHandler_OnStartParsing_m8546,
	ConfigHandler_OnProcessingInstruction_m8547,
	ConfigHandler_OnIgnorableWhitespace_m8548,
	ConfigHandler_OnStartElement_m8549,
	ConfigHandler_ParseElement_m8550,
	ConfigHandler_OnEndElement_m8551,
	ConfigHandler_ReadCustomProviderData_m8552,
	ConfigHandler_ReadLifetine_m8553,
	ConfigHandler_ParseTime_m8554,
	ConfigHandler_ReadChannel_m8555,
	ConfigHandler_ReadProvider_m8556,
	ConfigHandler_ReadClientActivated_m8557,
	ConfigHandler_ReadServiceActivated_m8558,
	ConfigHandler_ReadClientWellKnown_m8559,
	ConfigHandler_ReadServiceWellKnown_m8560,
	ConfigHandler_ReadInteropXml_m8561,
	ConfigHandler_ReadPreload_m8562,
	ConfigHandler_GetNotNull_m8563,
	ConfigHandler_ExtractAssembly_m8564,
	ConfigHandler_OnChars_m8565,
	ConfigHandler_OnEndParsing_m8566,
	ChannelData__ctor_m8567,
	ChannelData_get_ServerProviders_m8568,
	ChannelData_get_ClientProviders_m8569,
	ChannelData_get_CustomProperties_m8570,
	ChannelData_CopyFrom_m8571,
	ProviderData__ctor_m8572,
	ProviderData_CopyFrom_m8573,
	FormatterData__ctor_m8574,
	RemotingException__ctor_m8575,
	RemotingException__ctor_m8576,
	RemotingException__ctor_m8577,
	RemotingException__ctor_m8578,
	RemotingServices__cctor_m8579,
	RemotingServices_GetVirtualMethod_m8580,
	RemotingServices_IsTransparentProxy_m8581,
	RemotingServices_GetServerTypeForUri_m8582,
	RemotingServices_Unmarshal_m8583,
	RemotingServices_Unmarshal_m8584,
	RemotingServices_GetRealProxy_m8585,
	RemotingServices_GetMethodBaseFromMethodMessage_m8586,
	RemotingServices_GetMethodBaseFromName_m8587,
	RemotingServices_FindInterfaceMethod_m8588,
	RemotingServices_CreateClientProxy_m8589,
	RemotingServices_CreateClientProxy_m8590,
	RemotingServices_CreateClientProxyForContextBound_m8591,
	RemotingServices_GetIdentityForUri_m8592,
	RemotingServices_RemoveAppNameFromUri_m8593,
	RemotingServices_GetOrCreateClientIdentity_m8594,
	RemotingServices_GetClientChannelSinkChain_m8595,
	RemotingServices_CreateWellKnownServerIdentity_m8596,
	RemotingServices_RegisterServerIdentity_m8597,
	RemotingServices_GetProxyForRemoteObject_m8598,
	RemotingServices_GetRemoteObject_m8599,
	RemotingServices_RegisterInternalChannels_m8600,
	RemotingServices_DisposeIdentity_m8601,
	RemotingServices_GetNormalizedUri_m8602,
	ServerIdentity__ctor_m8603,
	ServerIdentity_get_ObjectType_m8604,
	ServerIdentity_CreateObjRef_m8605,
	ClientActivatedIdentity_GetServerObject_m8606,
	SingletonIdentity__ctor_m8607,
	SingleCallIdentity__ctor_m8608,
	TypeInfo__ctor_m8609,
	SoapServices__cctor_m8610,
	SoapServices_get_XmlNsForClrTypeWithAssembly_m8611,
	SoapServices_get_XmlNsForClrTypeWithNs_m8612,
	SoapServices_get_XmlNsForClrTypeWithNsAndAssembly_m8613,
	SoapServices_CodeXmlNamespaceForClrTypeNamespace_m8614,
	SoapServices_GetNameKey_m8615,
	SoapServices_GetAssemblyName_m8616,
	SoapServices_GetXmlElementForInteropType_m8617,
	SoapServices_GetXmlNamespaceForMethodCall_m8618,
	SoapServices_GetXmlNamespaceForMethodResponse_m8619,
	SoapServices_GetXmlTypeForInteropType_m8620,
	SoapServices_PreLoad_m8621,
	SoapServices_PreLoad_m8622,
	SoapServices_RegisterInteropXmlElement_m8623,
	SoapServices_RegisterInteropXmlType_m8624,
	SoapServices_EncodeNs_m8625,
	TypeEntry__ctor_m8626,
	TypeEntry_get_AssemblyName_m8627,
	TypeEntry_set_AssemblyName_m8628,
	TypeEntry_get_TypeName_m8629,
	TypeEntry_set_TypeName_m8630,
	TypeInfo__ctor_m8631,
	TypeInfo_get_TypeName_m8632,
	WellKnownClientTypeEntry__ctor_m8633,
	WellKnownClientTypeEntry_get_ApplicationUrl_m8634,
	WellKnownClientTypeEntry_get_ObjectType_m8635,
	WellKnownClientTypeEntry_get_ObjectUrl_m8636,
	WellKnownClientTypeEntry_ToString_m8637,
	WellKnownServiceTypeEntry__ctor_m8638,
	WellKnownServiceTypeEntry_get_Mode_m8639,
	WellKnownServiceTypeEntry_get_ObjectType_m8640,
	WellKnownServiceTypeEntry_get_ObjectUri_m8641,
	WellKnownServiceTypeEntry_ToString_m8642,
	BinaryCommon__cctor_m8643,
	BinaryCommon_IsPrimitive_m8644,
	BinaryCommon_GetTypeFromCode_m8645,
	BinaryCommon_SwapBytes_m8646,
	BinaryFormatter__ctor_m8647,
	BinaryFormatter__ctor_m8648,
	BinaryFormatter_get_DefaultSurrogateSelector_m8649,
	BinaryFormatter_set_AssemblyFormat_m8650,
	BinaryFormatter_get_Binder_m8651,
	BinaryFormatter_get_Context_m8652,
	BinaryFormatter_get_SurrogateSelector_m8653,
	BinaryFormatter_get_FilterLevel_m8654,
	BinaryFormatter_Deserialize_m8655,
	BinaryFormatter_NoCheckDeserialize_m8656,
	BinaryFormatter_ReadBinaryHeader_m8657,
	MessageFormatter_ReadMethodCall_m8658,
	MessageFormatter_ReadMethodResponse_m8659,
	TypeMetadata__ctor_m8660,
	ArrayNullFiller__ctor_m8661,
	ObjectReader__ctor_m8662,
	ObjectReader_ReadObjectGraph_m8663,
	ObjectReader_ReadObjectGraph_m8664,
	ObjectReader_ReadNextObject_m8665,
	ObjectReader_ReadNextObject_m8666,
	ObjectReader_get_CurrentObject_m8667,
	ObjectReader_ReadObject_m8668,
	ObjectReader_ReadAssembly_m8669,
	ObjectReader_ReadObjectInstance_m8670,
	ObjectReader_ReadRefTypeObjectInstance_m8671,
	ObjectReader_ReadObjectContent_m8672,
	ObjectReader_RegisterObject_m8673,
	ObjectReader_ReadStringIntance_m8674,
	ObjectReader_ReadGenericArray_m8675,
	ObjectReader_ReadBoxedPrimitiveTypeValue_m8676,
	ObjectReader_ReadArrayOfPrimitiveType_m8677,
	ObjectReader_BlockRead_m8678,
	ObjectReader_ReadArrayOfObject_m8679,
	ObjectReader_ReadArrayOfString_m8680,
	ObjectReader_ReadSimpleArray_m8681,
	ObjectReader_ReadTypeMetadata_m8682,
	ObjectReader_ReadValue_m8683,
	ObjectReader_SetObjectValue_m8684,
	ObjectReader_RecordFixup_m8685,
	ObjectReader_GetDeserializationType_m8686,
	ObjectReader_ReadType_m8687,
	ObjectReader_ReadPrimitiveTypeValue_m8688,
	FormatterConverter__ctor_m8689,
	FormatterConverter_Convert_m8690,
	FormatterConverter_ToBoolean_m8691,
	FormatterConverter_ToInt16_m8692,
	FormatterConverter_ToInt32_m8693,
	FormatterConverter_ToInt64_m8694,
	FormatterConverter_ToString_m8695,
	FormatterServices_GetUninitializedObject_m8696,
	FormatterServices_GetSafeUninitializedObject_m8697,
	ObjectManager__ctor_m8698,
	ObjectManager_DoFixups_m8699,
	ObjectManager_GetObjectRecord_m8700,
	ObjectManager_GetObject_m8701,
	ObjectManager_RaiseDeserializationEvent_m8702,
	ObjectManager_RaiseOnDeserializingEvent_m8703,
	ObjectManager_RaiseOnDeserializedEvent_m8704,
	ObjectManager_AddFixup_m8705,
	ObjectManager_RecordArrayElementFixup_m8706,
	ObjectManager_RecordArrayElementFixup_m8707,
	ObjectManager_RecordDelayedFixup_m8708,
	ObjectManager_RecordFixup_m8709,
	ObjectManager_RegisterObjectInternal_m8710,
	ObjectManager_RegisterObject_m8711,
	BaseFixupRecord__ctor_m8712,
	BaseFixupRecord_DoFixup_m8713,
	ArrayFixupRecord__ctor_m8714,
	ArrayFixupRecord_FixupImpl_m8715,
	MultiArrayFixupRecord__ctor_m8716,
	MultiArrayFixupRecord_FixupImpl_m8717,
	FixupRecord__ctor_m8718,
	FixupRecord_FixupImpl_m8719,
	DelayedFixupRecord__ctor_m8720,
	DelayedFixupRecord_FixupImpl_m8721,
	ObjectRecord__ctor_m8722,
	ObjectRecord_SetMemberValue_m8723,
	ObjectRecord_SetArrayValue_m8724,
	ObjectRecord_SetMemberValue_m8725,
	ObjectRecord_get_IsInstanceReady_m8726,
	ObjectRecord_get_IsUnsolvedObjectReference_m8727,
	ObjectRecord_get_IsRegistered_m8728,
	ObjectRecord_DoFixups_m8729,
	ObjectRecord_RemoveFixup_m8730,
	ObjectRecord_UnchainFixup_m8731,
	ObjectRecord_ChainFixup_m8732,
	ObjectRecord_LoadData_m8733,
	ObjectRecord_get_HasPendingFixups_m8734,
	SerializationBinder__ctor_m8735,
	CallbackHandler__ctor_m8736,
	CallbackHandler_Invoke_m8737,
	CallbackHandler_BeginInvoke_m8738,
	CallbackHandler_EndInvoke_m8739,
	SerializationCallbacks__ctor_m8740,
	SerializationCallbacks__cctor_m8741,
	SerializationCallbacks_get_HasDeserializedCallbacks_m8742,
	SerializationCallbacks_GetMethodsByAttribute_m8743,
	SerializationCallbacks_Invoke_m8744,
	SerializationCallbacks_RaiseOnDeserializing_m8745,
	SerializationCallbacks_RaiseOnDeserialized_m8746,
	SerializationCallbacks_GetSerializationCallbacks_m8747,
	SerializationEntry__ctor_m8748,
	SerializationEntry_get_Name_m8749,
	SerializationEntry_get_Value_m8750,
	SerializationException__ctor_m8751,
	SerializationException__ctor_m5356,
	SerializationException__ctor_m8752,
	SerializationInfo__ctor_m8753,
	SerializationInfo_AddValue_m5352,
	SerializationInfo_GetValue_m5355,
	SerializationInfo_SetType_m8754,
	SerializationInfo_GetEnumerator_m8755,
	SerializationInfo_AddValue_m8756,
	SerializationInfo_AddValue_m5354,
	SerializationInfo_AddValue_m5353,
	SerializationInfo_AddValue_m8757,
	SerializationInfo_AddValue_m8758,
	SerializationInfo_AddValue_m5363,
	SerializationInfo_AddValue_m8759,
	SerializationInfo_AddValue_m4388,
	SerializationInfo_GetBoolean_m5357,
	SerializationInfo_GetInt16_m8760,
	SerializationInfo_GetInt32_m5362,
	SerializationInfo_GetInt64_m5361,
	SerializationInfo_GetString_m5360,
	SerializationInfoEnumerator__ctor_m8761,
	SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m8762,
	SerializationInfoEnumerator_get_Current_m8763,
	SerializationInfoEnumerator_get_Name_m8764,
	SerializationInfoEnumerator_get_Value_m8765,
	SerializationInfoEnumerator_MoveNext_m8766,
	StreamingContext__ctor_m8767,
	StreamingContext__ctor_m8768,
	StreamingContext_get_State_m8769,
	StreamingContext_Equals_m8770,
	StreamingContext_GetHashCode_m8771,
	X509Certificate__ctor_m8772,
	X509Certificate__ctor_m4446,
	X509Certificate__ctor_m5385,
	X509Certificate__ctor_m8773,
	X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m8774,
	X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m8775,
	X509Certificate_tostr_m8776,
	X509Certificate_Equals_m8777,
	X509Certificate_GetCertHash_m8778,
	X509Certificate_GetCertHashString_m5387,
	X509Certificate_GetEffectiveDateString_m8779,
	X509Certificate_GetExpirationDateString_m8780,
	X509Certificate_GetHashCode_m8781,
	X509Certificate_GetIssuerName_m8782,
	X509Certificate_GetName_m8783,
	X509Certificate_GetPublicKey_m8784,
	X509Certificate_GetRawCertData_m8785,
	X509Certificate_ToString_m8786,
	X509Certificate_ToString_m5390,
	X509Certificate_get_Issuer_m5392,
	X509Certificate_get_Subject_m5391,
	X509Certificate_Equals_m8787,
	X509Certificate_Import_m5388,
	X509Certificate_Reset_m5389,
	AsymmetricAlgorithm__ctor_m8788,
	AsymmetricAlgorithm_System_IDisposable_Dispose_m8789,
	AsymmetricAlgorithm_get_KeySize_m4364,
	AsymmetricAlgorithm_set_KeySize_m4363,
	AsymmetricAlgorithm_Clear_m4453,
	AsymmetricAlgorithm_GetNamedParam_m8790,
	AsymmetricKeyExchangeFormatter__ctor_m8791,
	AsymmetricSignatureDeformatter__ctor_m4439,
	AsymmetricSignatureFormatter__ctor_m4440,
	Base64Constants__cctor_m8792,
	CryptoConfig__cctor_m8793,
	CryptoConfig_Initialize_m8794,
	CryptoConfig_CreateFromName_m4354,
	CryptoConfig_CreateFromName_m5397,
	CryptoConfig_MapNameToOID_m4355,
	CryptoConfig_EncodeOID_m4347,
	CryptoConfig_EncodeLongNumber_m8795,
	CryptographicException__ctor_m8796,
	CryptographicException__ctor_m3448,
	CryptographicException__ctor_m4376,
	CryptographicException__ctor_m3456,
	CryptographicException__ctor_m8797,
	CryptographicUnexpectedOperationException__ctor_m8798,
	CryptographicUnexpectedOperationException__ctor_m4406,
	CryptographicUnexpectedOperationException__ctor_m8799,
	CspParameters__ctor_m4358,
	CspParameters__ctor_m8800,
	CspParameters__ctor_m8801,
	CspParameters__ctor_m8802,
	CspParameters_get_Flags_m8803,
	CspParameters_set_Flags_m4359,
	DES__ctor_m8804,
	DES__cctor_m8805,
	DES_Create_m4407,
	DES_Create_m8806,
	DES_IsWeakKey_m8807,
	DES_IsSemiWeakKey_m8808,
	DES_get_Key_m8809,
	DES_set_Key_m8810,
	DESTransform__ctor_m8811,
	DESTransform__cctor_m8812,
	DESTransform_CipherFunct_m8813,
	DESTransform_Permutation_m8814,
	DESTransform_BSwap_m8815,
	DESTransform_SetKey_m8816,
	DESTransform_ProcessBlock_m8817,
	DESTransform_ECB_m8818,
	DESTransform_GetStrongKey_m8819,
	DESCryptoServiceProvider__ctor_m8820,
	DESCryptoServiceProvider_CreateDecryptor_m8821,
	DESCryptoServiceProvider_CreateEncryptor_m8822,
	DESCryptoServiceProvider_GenerateIV_m8823,
	DESCryptoServiceProvider_GenerateKey_m8824,
	DSA__ctor_m8825,
	DSA_Create_m4361,
	DSA_Create_m8826,
	DSA_ZeroizePrivateKey_m8827,
	DSA_FromXmlString_m8828,
	DSA_ToXmlString_m8829,
	DSACryptoServiceProvider__ctor_m8830,
	DSACryptoServiceProvider__ctor_m4378,
	DSACryptoServiceProvider__ctor_m8831,
	DSACryptoServiceProvider__cctor_m8832,
	DSACryptoServiceProvider_Finalize_m8833,
	DSACryptoServiceProvider_get_KeySize_m8834,
	DSACryptoServiceProvider_get_PublicOnly_m5379,
	DSACryptoServiceProvider_ExportParameters_m8835,
	DSACryptoServiceProvider_ImportParameters_m8836,
	DSACryptoServiceProvider_CreateSignature_m8837,
	DSACryptoServiceProvider_VerifySignature_m8838,
	DSACryptoServiceProvider_Dispose_m8839,
	DSACryptoServiceProvider_OnKeyGenerated_m8840,
	DSASignatureDeformatter__ctor_m8841,
	DSASignatureDeformatter__ctor_m4386,
	DSASignatureDeformatter_SetHashAlgorithm_m8842,
	DSASignatureDeformatter_SetKey_m8843,
	DSASignatureDeformatter_VerifySignature_m8844,
	DSASignatureFormatter__ctor_m8845,
	DSASignatureFormatter_CreateSignature_m8846,
	DSASignatureFormatter_SetHashAlgorithm_m8847,
	DSASignatureFormatter_SetKey_m8848,
	HMAC__ctor_m8849,
	HMAC_get_BlockSizeValue_m8850,
	HMAC_set_BlockSizeValue_m8851,
	HMAC_set_HashName_m8852,
	HMAC_get_Key_m8853,
	HMAC_set_Key_m8854,
	HMAC_get_Block_m8855,
	HMAC_KeySetup_m8856,
	HMAC_Dispose_m8857,
	HMAC_HashCore_m8858,
	HMAC_HashFinal_m8859,
	HMAC_Initialize_m8860,
	HMAC_Create_m4373,
	HMAC_Create_m8861,
	HMACMD5__ctor_m8862,
	HMACMD5__ctor_m8863,
	HMACRIPEMD160__ctor_m8864,
	HMACRIPEMD160__ctor_m8865,
	HMACSHA1__ctor_m8866,
	HMACSHA1__ctor_m8867,
	HMACSHA256__ctor_m8868,
	HMACSHA256__ctor_m8869,
	HMACSHA384__ctor_m8870,
	HMACSHA384__ctor_m8871,
	HMACSHA384__cctor_m8872,
	HMACSHA384_set_ProduceLegacyHmacValues_m8873,
	HMACSHA512__ctor_m8874,
	HMACSHA512__ctor_m8875,
	HMACSHA512__cctor_m8876,
	HMACSHA512_set_ProduceLegacyHmacValues_m8877,
	HashAlgorithm__ctor_m4353,
	HashAlgorithm_System_IDisposable_Dispose_m8878,
	HashAlgorithm_get_CanReuseTransform_m8879,
	HashAlgorithm_ComputeHash_m4393,
	HashAlgorithm_ComputeHash_m4366,
	HashAlgorithm_Create_m4365,
	HashAlgorithm_get_Hash_m8880,
	HashAlgorithm_get_HashSize_m8881,
	HashAlgorithm_Dispose_m8882,
	HashAlgorithm_TransformBlock_m8883,
	HashAlgorithm_TransformFinalBlock_m8884,
	KeySizes__ctor_m3458,
	KeySizes_get_MaxSize_m8885,
	KeySizes_get_MinSize_m8886,
	KeySizes_get_SkipSize_m8887,
	KeySizes_IsLegal_m8888,
	KeySizes_IsLegalKeySize_m8889,
	KeyedHashAlgorithm__ctor_m4405,
	KeyedHashAlgorithm_Finalize_m8890,
	KeyedHashAlgorithm_get_Key_m8891,
	KeyedHashAlgorithm_set_Key_m8892,
	KeyedHashAlgorithm_Dispose_m8893,
	KeyedHashAlgorithm_ZeroizeKey_m8894,
	MACTripleDES__ctor_m8895,
	MACTripleDES_Setup_m8896,
	MACTripleDES_Finalize_m8897,
	MACTripleDES_Dispose_m8898,
	MACTripleDES_Initialize_m8899,
	MACTripleDES_HashCore_m8900,
	MACTripleDES_HashFinal_m8901,
	MD5__ctor_m8902,
	MD5_Create_m4379,
	MD5_Create_m8903,
	MD5CryptoServiceProvider__ctor_m8904,
	MD5CryptoServiceProvider__cctor_m8905,
	MD5CryptoServiceProvider_Finalize_m8906,
	MD5CryptoServiceProvider_Dispose_m8907,
	MD5CryptoServiceProvider_HashCore_m8908,
	MD5CryptoServiceProvider_HashFinal_m8909,
	MD5CryptoServiceProvider_Initialize_m8910,
	MD5CryptoServiceProvider_ProcessBlock_m8911,
	MD5CryptoServiceProvider_ProcessFinalBlock_m8912,
	MD5CryptoServiceProvider_AddLength_m8913,
	RC2__ctor_m8914,
	RC2_Create_m4408,
	RC2_Create_m8915,
	RC2_get_EffectiveKeySize_m8916,
	RC2_get_KeySize_m8917,
	RC2_set_KeySize_m8918,
	RC2CryptoServiceProvider__ctor_m8919,
	RC2CryptoServiceProvider_get_EffectiveKeySize_m8920,
	RC2CryptoServiceProvider_CreateDecryptor_m8921,
	RC2CryptoServiceProvider_CreateEncryptor_m8922,
	RC2CryptoServiceProvider_GenerateIV_m8923,
	RC2CryptoServiceProvider_GenerateKey_m8924,
	RC2Transform__ctor_m8925,
	RC2Transform__cctor_m8926,
	RC2Transform_ECB_m8927,
	RIPEMD160__ctor_m8928,
	RIPEMD160Managed__ctor_m8929,
	RIPEMD160Managed_Initialize_m8930,
	RIPEMD160Managed_HashCore_m8931,
	RIPEMD160Managed_HashFinal_m8932,
	RIPEMD160Managed_Finalize_m8933,
	RIPEMD160Managed_ProcessBlock_m8934,
	RIPEMD160Managed_Compress_m8935,
	RIPEMD160Managed_CompressFinal_m8936,
	RIPEMD160Managed_ROL_m8937,
	RIPEMD160Managed_F_m8938,
	RIPEMD160Managed_G_m8939,
	RIPEMD160Managed_H_m8940,
	RIPEMD160Managed_I_m8941,
	RIPEMD160Managed_J_m8942,
	RIPEMD160Managed_FF_m8943,
	RIPEMD160Managed_GG_m8944,
	RIPEMD160Managed_HH_m8945,
	RIPEMD160Managed_II_m8946,
	RIPEMD160Managed_JJ_m8947,
	RIPEMD160Managed_FFF_m8948,
	RIPEMD160Managed_GGG_m8949,
	RIPEMD160Managed_HHH_m8950,
	RIPEMD160Managed_III_m8951,
	RIPEMD160Managed_JJJ_m8952,
	RNGCryptoServiceProvider__ctor_m8953,
	RNGCryptoServiceProvider__cctor_m8954,
	RNGCryptoServiceProvider_Check_m8955,
	RNGCryptoServiceProvider_RngOpen_m8956,
	RNGCryptoServiceProvider_RngInitialize_m8957,
	RNGCryptoServiceProvider_RngGetBytes_m8958,
	RNGCryptoServiceProvider_RngClose_m8959,
	RNGCryptoServiceProvider_GetBytes_m8960,
	RNGCryptoServiceProvider_GetNonZeroBytes_m8961,
	RNGCryptoServiceProvider_Finalize_m8962,
	RSA__ctor_m4362,
	RSA_Create_m4357,
	RSA_Create_m8963,
	RSA_ZeroizePrivateKey_m8964,
	RSA_FromXmlString_m8965,
	RSA_ToXmlString_m8966,
	RSACryptoServiceProvider__ctor_m8967,
	RSACryptoServiceProvider__ctor_m4360,
	RSACryptoServiceProvider__ctor_m4382,
	RSACryptoServiceProvider__cctor_m8968,
	RSACryptoServiceProvider_Common_m8969,
	RSACryptoServiceProvider_Finalize_m8970,
	RSACryptoServiceProvider_get_KeySize_m8971,
	RSACryptoServiceProvider_get_PublicOnly_m5378,
	RSACryptoServiceProvider_DecryptValue_m8972,
	RSACryptoServiceProvider_EncryptValue_m8973,
	RSACryptoServiceProvider_ExportParameters_m8974,
	RSACryptoServiceProvider_ImportParameters_m8975,
	RSACryptoServiceProvider_Dispose_m8976,
	RSACryptoServiceProvider_OnKeyGenerated_m8977,
	RSAPKCS1KeyExchangeFormatter__ctor_m4452,
	RSAPKCS1KeyExchangeFormatter_CreateKeyExchange_m8978,
	RSAPKCS1KeyExchangeFormatter_SetRSAKey_m8979,
	RSAPKCS1SignatureDeformatter__ctor_m8980,
	RSAPKCS1SignatureDeformatter__ctor_m4387,
	RSAPKCS1SignatureDeformatter_SetHashAlgorithm_m8981,
	RSAPKCS1SignatureDeformatter_SetKey_m8982,
	RSAPKCS1SignatureDeformatter_VerifySignature_m8983,
	RSAPKCS1SignatureFormatter__ctor_m8984,
	RSAPKCS1SignatureFormatter_CreateSignature_m8985,
	RSAPKCS1SignatureFormatter_SetHashAlgorithm_m8986,
	RSAPKCS1SignatureFormatter_SetKey_m8987,
	RandomNumberGenerator__ctor_m8988,
	RandomNumberGenerator_Create_m3447,
	RandomNumberGenerator_Create_m8989,
	Rijndael__ctor_m8990,
	Rijndael_Create_m4410,
	Rijndael_Create_m8991,
	RijndaelManaged__ctor_m8992,
	RijndaelManaged_GenerateIV_m8993,
	RijndaelManaged_GenerateKey_m8994,
	RijndaelManaged_CreateDecryptor_m8995,
	RijndaelManaged_CreateEncryptor_m8996,
	RijndaelTransform__ctor_m8997,
	RijndaelTransform__cctor_m8998,
	RijndaelTransform_Clear_m8999,
	RijndaelTransform_ECB_m9000,
	RijndaelTransform_SubByte_m9001,
	RijndaelTransform_Encrypt128_m9002,
	RijndaelTransform_Encrypt192_m9003,
	RijndaelTransform_Encrypt256_m9004,
	RijndaelTransform_Decrypt128_m9005,
	RijndaelTransform_Decrypt192_m9006,
	RijndaelTransform_Decrypt256_m9007,
	RijndaelManagedTransform__ctor_m9008,
	RijndaelManagedTransform_System_IDisposable_Dispose_m9009,
	RijndaelManagedTransform_get_CanReuseTransform_m9010,
	RijndaelManagedTransform_TransformBlock_m9011,
	RijndaelManagedTransform_TransformFinalBlock_m9012,
	SHA1__ctor_m9013,
	SHA1_Create_m4380,
	SHA1_Create_m9014,
	SHA1Internal__ctor_m9015,
	SHA1Internal_HashCore_m9016,
	SHA1Internal_HashFinal_m9017,
	SHA1Internal_Initialize_m9018,
	SHA1Internal_ProcessBlock_m9019,
	SHA1Internal_InitialiseBuff_m9020,
	SHA1Internal_FillBuff_m9021,
	SHA1Internal_ProcessFinalBlock_m9022,
	SHA1Internal_AddLength_m9023,
	SHA1CryptoServiceProvider__ctor_m9024,
	SHA1CryptoServiceProvider_Finalize_m9025,
	SHA1CryptoServiceProvider_Dispose_m9026,
	SHA1CryptoServiceProvider_HashCore_m9027,
	SHA1CryptoServiceProvider_HashFinal_m9028,
	SHA1CryptoServiceProvider_Initialize_m9029,
	SHA1Managed__ctor_m9030,
	SHA1Managed_HashCore_m9031,
	SHA1Managed_HashFinal_m9032,
	SHA1Managed_Initialize_m9033,
	SHA256__ctor_m9034,
	SHA256_Create_m4381,
	SHA256_Create_m9035,
	SHA256Managed__ctor_m9036,
	SHA256Managed_HashCore_m9037,
	SHA256Managed_HashFinal_m9038,
	SHA256Managed_Initialize_m9039,
	SHA256Managed_ProcessBlock_m9040,
	SHA256Managed_ProcessFinalBlock_m9041,
	SHA256Managed_AddLength_m9042,
	SHA384__ctor_m9043,
	SHA384Managed__ctor_m9044,
	SHA384Managed_Initialize_m9045,
	SHA384Managed_Initialize_m9046,
	SHA384Managed_HashCore_m9047,
	SHA384Managed_HashFinal_m9048,
	SHA384Managed_update_m9049,
	SHA384Managed_processWord_m9050,
	SHA384Managed_unpackWord_m9051,
	SHA384Managed_adjustByteCounts_m9052,
	SHA384Managed_processLength_m9053,
	SHA384Managed_processBlock_m9054,
	SHA512__ctor_m9055,
	SHA512Managed__ctor_m9056,
	SHA512Managed_Initialize_m9057,
	SHA512Managed_Initialize_m9058,
	SHA512Managed_HashCore_m9059,
	SHA512Managed_HashFinal_m9060,
	SHA512Managed_update_m9061,
	SHA512Managed_processWord_m9062,
	SHA512Managed_unpackWord_m9063,
	SHA512Managed_adjustByteCounts_m9064,
	SHA512Managed_processLength_m9065,
	SHA512Managed_processBlock_m9066,
	SHA512Managed_rotateRight_m9067,
	SHA512Managed_Ch_m9068,
	SHA512Managed_Maj_m9069,
	SHA512Managed_Sum0_m9070,
	SHA512Managed_Sum1_m9071,
	SHA512Managed_Sigma0_m9072,
	SHA512Managed_Sigma1_m9073,
	SHAConstants__cctor_m9074,
	SignatureDescription__ctor_m9075,
	SignatureDescription_set_DeformatterAlgorithm_m9076,
	SignatureDescription_set_DigestAlgorithm_m9077,
	SignatureDescription_set_FormatterAlgorithm_m9078,
	SignatureDescription_set_KeyAlgorithm_m9079,
	DSASignatureDescription__ctor_m9080,
	RSAPKCS1SHA1SignatureDescription__ctor_m9081,
	SymmetricAlgorithm__ctor_m3457,
	SymmetricAlgorithm_System_IDisposable_Dispose_m9082,
	SymmetricAlgorithm_Finalize_m4351,
	SymmetricAlgorithm_Clear_m4372,
	SymmetricAlgorithm_Dispose_m3465,
	SymmetricAlgorithm_get_BlockSize_m9083,
	SymmetricAlgorithm_set_BlockSize_m9084,
	SymmetricAlgorithm_get_FeedbackSize_m9085,
	SymmetricAlgorithm_get_IV_m3459,
	SymmetricAlgorithm_set_IV_m3460,
	SymmetricAlgorithm_get_Key_m3461,
	SymmetricAlgorithm_set_Key_m3462,
	SymmetricAlgorithm_get_KeySize_m3463,
	SymmetricAlgorithm_set_KeySize_m3464,
	SymmetricAlgorithm_get_LegalKeySizes_m9086,
	SymmetricAlgorithm_get_Mode_m9087,
	SymmetricAlgorithm_set_Mode_m9088,
	SymmetricAlgorithm_get_Padding_m9089,
	SymmetricAlgorithm_set_Padding_m9090,
	SymmetricAlgorithm_CreateDecryptor_m9091,
	SymmetricAlgorithm_CreateEncryptor_m9092,
	SymmetricAlgorithm_Create_m4371,
	ToBase64Transform_System_IDisposable_Dispose_m9093,
	ToBase64Transform_Finalize_m9094,
	ToBase64Transform_get_CanReuseTransform_m9095,
	ToBase64Transform_get_InputBlockSize_m9096,
	ToBase64Transform_get_OutputBlockSize_m9097,
	ToBase64Transform_Dispose_m9098,
	ToBase64Transform_TransformBlock_m9099,
	ToBase64Transform_InternalTransformBlock_m9100,
	ToBase64Transform_TransformFinalBlock_m9101,
	ToBase64Transform_InternalTransformFinalBlock_m9102,
	TripleDES__ctor_m9103,
	TripleDES_get_Key_m9104,
	TripleDES_set_Key_m9105,
	TripleDES_IsWeakKey_m9106,
	TripleDES_Create_m4409,
	TripleDES_Create_m9107,
	TripleDESCryptoServiceProvider__ctor_m9108,
	TripleDESCryptoServiceProvider_GenerateIV_m9109,
	TripleDESCryptoServiceProvider_GenerateKey_m9110,
	TripleDESCryptoServiceProvider_CreateDecryptor_m9111,
	TripleDESCryptoServiceProvider_CreateEncryptor_m9112,
	TripleDESTransform__ctor_m9113,
	TripleDESTransform_ECB_m9114,
	TripleDESTransform_GetStrongKey_m9115,
	SecurityPermission__ctor_m9116,
	SecurityPermission_set_Flags_m9117,
	SecurityPermission_IsUnrestricted_m9118,
	SecurityPermission_IsSubsetOf_m9119,
	SecurityPermission_ToXml_m9120,
	SecurityPermission_IsEmpty_m9121,
	SecurityPermission_Cast_m9122,
	StrongNamePublicKeyBlob_Equals_m9123,
	StrongNamePublicKeyBlob_GetHashCode_m9124,
	StrongNamePublicKeyBlob_ToString_m9125,
	ApplicationTrust__ctor_m9126,
	EvidenceEnumerator__ctor_m9127,
	EvidenceEnumerator_MoveNext_m9128,
	EvidenceEnumerator_get_Current_m9129,
	Evidence__ctor_m9130,
	Evidence_get_Count_m9131,
	Evidence_get_IsSynchronized_m9132,
	Evidence_get_SyncRoot_m9133,
	Evidence_get_HostEvidenceList_m9134,
	Evidence_get_AssemblyEvidenceList_m9135,
	Evidence_CopyTo_m9136,
	Evidence_Equals_m9137,
	Evidence_GetEnumerator_m9138,
	Evidence_GetHashCode_m9139,
	Hash__ctor_m9140,
	Hash__ctor_m9141,
	Hash_GetObjectData_m9142,
	Hash_ToString_m9143,
	Hash_GetData_m9144,
	StrongName_get_Name_m9145,
	StrongName_get_PublicKey_m9146,
	StrongName_get_Version_m9147,
	StrongName_Equals_m9148,
	StrongName_GetHashCode_m9149,
	StrongName_ToString_m9150,
	WindowsIdentity__ctor_m9151,
	WindowsIdentity__cctor_m9152,
	WindowsIdentity_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m9153,
	WindowsIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m9154,
	WindowsIdentity_Dispose_m9155,
	WindowsIdentity_GetCurrentToken_m9156,
	WindowsIdentity_GetTokenName_m9157,
	CodeAccessPermission__ctor_m9158,
	CodeAccessPermission_Equals_m9159,
	CodeAccessPermission_GetHashCode_m9160,
	CodeAccessPermission_ToString_m9161,
	CodeAccessPermission_Element_m9162,
	CodeAccessPermission_ThrowInvalidPermission_m9163,
	PermissionSet__ctor_m9164,
	PermissionSet__ctor_m9165,
	PermissionSet_set_DeclarativeSecurity_m9166,
	PermissionSet_CreateFromBinaryFormat_m9167,
	SecurityContext__ctor_m9168,
	SecurityContext__ctor_m9169,
	SecurityContext_Capture_m9170,
	SecurityContext_get_FlowSuppressed_m9171,
	SecurityContext_get_CompressedStack_m9172,
	SecurityAttribute__ctor_m9173,
	SecurityAttribute_get_Name_m9174,
	SecurityAttribute_get_Value_m9175,
	SecurityElement__ctor_m9176,
	SecurityElement__ctor_m9177,
	SecurityElement__cctor_m9178,
	SecurityElement_get_Children_m9179,
	SecurityElement_get_Tag_m9180,
	SecurityElement_set_Text_m9181,
	SecurityElement_AddAttribute_m9182,
	SecurityElement_AddChild_m9183,
	SecurityElement_Escape_m9184,
	SecurityElement_Unescape_m9185,
	SecurityElement_IsValidAttributeName_m9186,
	SecurityElement_IsValidAttributeValue_m9187,
	SecurityElement_IsValidTag_m9188,
	SecurityElement_IsValidText_m9189,
	SecurityElement_SearchForChildByTag_m9190,
	SecurityElement_ToString_m9191,
	SecurityElement_ToXml_m9192,
	SecurityElement_GetAttribute_m9193,
	SecurityException__ctor_m9194,
	SecurityException__ctor_m9195,
	SecurityException__ctor_m9196,
	SecurityException_get_Demanded_m9197,
	SecurityException_get_FirstPermissionThatFailed_m9198,
	SecurityException_get_PermissionState_m9199,
	SecurityException_get_PermissionType_m9200,
	SecurityException_get_GrantedSet_m9201,
	SecurityException_get_RefusedSet_m9202,
	SecurityException_GetObjectData_m9203,
	SecurityException_ToString_m9204,
	SecurityFrame__ctor_m9205,
	SecurityFrame__GetSecurityStack_m9206,
	SecurityFrame_InitFromRuntimeFrame_m9207,
	SecurityFrame_get_Assembly_m9208,
	SecurityFrame_get_Domain_m9209,
	SecurityFrame_ToString_m9210,
	SecurityFrame_GetStack_m9211,
	SecurityManager__cctor_m9212,
	SecurityManager_get_SecurityEnabled_m9213,
	SecurityManager_Decode_m9214,
	SecurityManager_Decode_m9215,
	SecuritySafeCriticalAttribute__ctor_m9216,
	SuppressUnmanagedCodeSecurityAttribute__ctor_m9217,
	UnverifiableCodeAttribute__ctor_m9218,
	ASCIIEncoding__ctor_m9219,
	ASCIIEncoding_GetByteCount_m9220,
	ASCIIEncoding_GetByteCount_m9221,
	ASCIIEncoding_GetBytes_m9222,
	ASCIIEncoding_GetBytes_m9223,
	ASCIIEncoding_GetBytes_m9224,
	ASCIIEncoding_GetBytes_m9225,
	ASCIIEncoding_GetCharCount_m9226,
	ASCIIEncoding_GetChars_m9227,
	ASCIIEncoding_GetChars_m9228,
	ASCIIEncoding_GetMaxByteCount_m9229,
	ASCIIEncoding_GetMaxCharCount_m9230,
	ASCIIEncoding_GetString_m9231,
	ASCIIEncoding_GetBytes_m9232,
	ASCIIEncoding_GetByteCount_m9233,
	ASCIIEncoding_GetDecoder_m9234,
	Decoder__ctor_m9235,
	Decoder_set_Fallback_m9236,
	Decoder_get_FallbackBuffer_m9237,
	DecoderExceptionFallback__ctor_m9238,
	DecoderExceptionFallback_CreateFallbackBuffer_m9239,
	DecoderExceptionFallback_Equals_m9240,
	DecoderExceptionFallback_GetHashCode_m9241,
	DecoderExceptionFallbackBuffer__ctor_m9242,
	DecoderExceptionFallbackBuffer_get_Remaining_m9243,
	DecoderExceptionFallbackBuffer_Fallback_m9244,
	DecoderExceptionFallbackBuffer_GetNextChar_m9245,
	DecoderFallback__ctor_m9246,
	DecoderFallback__cctor_m9247,
	DecoderFallback_get_ExceptionFallback_m9248,
	DecoderFallback_get_ReplacementFallback_m9249,
	DecoderFallback_get_StandardSafeFallback_m9250,
	DecoderFallbackBuffer__ctor_m9251,
	DecoderFallbackBuffer_Reset_m9252,
	DecoderFallbackException__ctor_m9253,
	DecoderFallbackException__ctor_m9254,
	DecoderFallbackException__ctor_m9255,
	DecoderReplacementFallback__ctor_m9256,
	DecoderReplacementFallback__ctor_m9257,
	DecoderReplacementFallback_get_DefaultString_m9258,
	DecoderReplacementFallback_CreateFallbackBuffer_m9259,
	DecoderReplacementFallback_Equals_m9260,
	DecoderReplacementFallback_GetHashCode_m9261,
	DecoderReplacementFallbackBuffer__ctor_m9262,
	DecoderReplacementFallbackBuffer_get_Remaining_m9263,
	DecoderReplacementFallbackBuffer_Fallback_m9264,
	DecoderReplacementFallbackBuffer_GetNextChar_m9265,
	DecoderReplacementFallbackBuffer_Reset_m9266,
	EncoderExceptionFallback__ctor_m9267,
	EncoderExceptionFallback_CreateFallbackBuffer_m9268,
	EncoderExceptionFallback_Equals_m9269,
	EncoderExceptionFallback_GetHashCode_m9270,
	EncoderExceptionFallbackBuffer__ctor_m9271,
	EncoderExceptionFallbackBuffer_get_Remaining_m9272,
	EncoderExceptionFallbackBuffer_Fallback_m9273,
	EncoderExceptionFallbackBuffer_Fallback_m9274,
	EncoderExceptionFallbackBuffer_GetNextChar_m9275,
	EncoderFallback__ctor_m9276,
	EncoderFallback__cctor_m9277,
	EncoderFallback_get_ExceptionFallback_m9278,
	EncoderFallback_get_ReplacementFallback_m9279,
	EncoderFallback_get_StandardSafeFallback_m9280,
	EncoderFallbackBuffer__ctor_m9281,
	EncoderFallbackException__ctor_m9282,
	EncoderFallbackException__ctor_m9283,
	EncoderFallbackException__ctor_m9284,
	EncoderFallbackException__ctor_m9285,
	EncoderReplacementFallback__ctor_m9286,
	EncoderReplacementFallback__ctor_m9287,
	EncoderReplacementFallback_get_DefaultString_m9288,
	EncoderReplacementFallback_CreateFallbackBuffer_m9289,
	EncoderReplacementFallback_Equals_m9290,
	EncoderReplacementFallback_GetHashCode_m9291,
	EncoderReplacementFallbackBuffer__ctor_m9292,
	EncoderReplacementFallbackBuffer_get_Remaining_m9293,
	EncoderReplacementFallbackBuffer_Fallback_m9294,
	EncoderReplacementFallbackBuffer_Fallback_m9295,
	EncoderReplacementFallbackBuffer_Fallback_m9296,
	EncoderReplacementFallbackBuffer_GetNextChar_m9297,
	ForwardingDecoder__ctor_m9298,
	ForwardingDecoder_GetChars_m9299,
	Encoding__ctor_m9300,
	Encoding__ctor_m9301,
	Encoding__cctor_m9302,
	Encoding___m9303,
	Encoding_get_IsReadOnly_m9304,
	Encoding_get_DecoderFallback_m9305,
	Encoding_set_DecoderFallback_m9306,
	Encoding_get_EncoderFallback_m9307,
	Encoding_SetFallbackInternal_m9308,
	Encoding_Equals_m9309,
	Encoding_GetByteCount_m9310,
	Encoding_GetByteCount_m9311,
	Encoding_GetBytes_m9312,
	Encoding_GetBytes_m9313,
	Encoding_GetBytes_m9314,
	Encoding_GetBytes_m9315,
	Encoding_GetChars_m9316,
	Encoding_GetDecoder_m9317,
	Encoding_InvokeI18N_m9318,
	Encoding_GetEncoding_m9319,
	Encoding_GetEncoding_m3271,
	Encoding_GetHashCode_m9320,
	Encoding_GetPreamble_m9321,
	Encoding_GetString_m9322,
	Encoding_GetString_m9323,
	Encoding_get_HeaderName_m9324,
	Encoding_get_WebName_m9325,
	Encoding_get_ASCII_m3266,
	Encoding_get_BigEndianUnicode_m4368,
	Encoding_InternalCodePage_m9326,
	Encoding_get_Default_m9327,
	Encoding_get_ISOLatin1_m9328,
	Encoding_get_UTF7_m4374,
	Encoding_get_UTF8_m3272,
	Encoding_get_UTF8Unmarked_m9329,
	Encoding_get_UTF8UnmarkedUnsafe_m9330,
	Encoding_get_Unicode_m9331,
	Encoding_get_UTF32_m9332,
	Encoding_get_BigEndianUTF32_m9333,
	Encoding_GetByteCount_m9334,
	Encoding_GetBytes_m9335,
	Latin1Encoding__ctor_m9336,
	Latin1Encoding_GetByteCount_m9337,
	Latin1Encoding_GetByteCount_m9338,
	Latin1Encoding_GetBytes_m9339,
	Latin1Encoding_GetBytes_m9340,
	Latin1Encoding_GetBytes_m9341,
	Latin1Encoding_GetBytes_m9342,
	Latin1Encoding_GetCharCount_m9343,
	Latin1Encoding_GetChars_m9344,
	Latin1Encoding_GetMaxByteCount_m9345,
	Latin1Encoding_GetMaxCharCount_m9346,
	Latin1Encoding_GetString_m9347,
	Latin1Encoding_GetString_m9348,
	Latin1Encoding_get_HeaderName_m9349,
	Latin1Encoding_get_WebName_m9350,
	StringBuilder__ctor_m9351,
	StringBuilder__ctor_m9352,
	StringBuilder__ctor_m1477,
	StringBuilder__ctor_m3326,
	StringBuilder__ctor_m1539,
	StringBuilder__ctor_m5358,
	StringBuilder__ctor_m9353,
	StringBuilder_System_Runtime_Serialization_ISerializable_GetObjectData_m9354,
	StringBuilder_get_Capacity_m9355,
	StringBuilder_set_Capacity_m9356,
	StringBuilder_get_Length_m4403,
	StringBuilder_set_Length_m5425,
	StringBuilder_get_Chars_m9357,
	StringBuilder_set_Chars_m9358,
	StringBuilder_ToString_m1480,
	StringBuilder_ToString_m9359,
	StringBuilder_Remove_m9360,
	StringBuilder_Replace_m9361,
	StringBuilder_Replace_m9362,
	StringBuilder_Append_m3330,
	StringBuilder_Append_m5384,
	StringBuilder_Append_m5368,
	StringBuilder_Append_m5359,
	StringBuilder_Append_m1781,
	StringBuilder_Append_m9363,
	StringBuilder_Append_m9364,
	StringBuilder_Append_m5400,
	StringBuilder_AppendLine_m1479,
	StringBuilder_AppendLine_m1478,
	StringBuilder_AppendFormat_m4345,
	StringBuilder_AppendFormat_m9365,
	StringBuilder_AppendFormat_m4344,
	StringBuilder_AppendFormat_m4343,
	StringBuilder_AppendFormat_m5393,
	StringBuilder_Insert_m9366,
	StringBuilder_Insert_m9367,
	StringBuilder_Insert_m9368,
	StringBuilder_InternalEnsureCapacity_m9369,
	UTF32Decoder__ctor_m9370,
	UTF32Decoder_GetChars_m9371,
	UTF32Encoding__ctor_m9372,
	UTF32Encoding__ctor_m9373,
	UTF32Encoding__ctor_m9374,
	UTF32Encoding_GetByteCount_m9375,
	UTF32Encoding_GetBytes_m9376,
	UTF32Encoding_GetCharCount_m9377,
	UTF32Encoding_GetChars_m9378,
	UTF32Encoding_GetMaxByteCount_m9379,
	UTF32Encoding_GetMaxCharCount_m9380,
	UTF32Encoding_GetDecoder_m9381,
	UTF32Encoding_GetPreamble_m9382,
	UTF32Encoding_Equals_m9383,
	UTF32Encoding_GetHashCode_m9384,
	UTF32Encoding_GetByteCount_m9385,
	UTF32Encoding_GetByteCount_m9386,
	UTF32Encoding_GetBytes_m9387,
	UTF32Encoding_GetBytes_m9388,
	UTF32Encoding_GetString_m9389,
	UTF7Decoder__ctor_m9390,
	UTF7Decoder_GetChars_m9391,
	UTF7Encoding__ctor_m9392,
	UTF7Encoding__ctor_m9393,
	UTF7Encoding__cctor_m9394,
	UTF7Encoding_GetHashCode_m9395,
	UTF7Encoding_Equals_m9396,
	UTF7Encoding_InternalGetByteCount_m9397,
	UTF7Encoding_GetByteCount_m9398,
	UTF7Encoding_InternalGetBytes_m9399,
	UTF7Encoding_GetBytes_m9400,
	UTF7Encoding_InternalGetCharCount_m9401,
	UTF7Encoding_GetCharCount_m9402,
	UTF7Encoding_InternalGetChars_m9403,
	UTF7Encoding_GetChars_m9404,
	UTF7Encoding_GetMaxByteCount_m9405,
	UTF7Encoding_GetMaxCharCount_m9406,
	UTF7Encoding_GetDecoder_m9407,
	UTF7Encoding_GetByteCount_m9408,
	UTF7Encoding_GetByteCount_m9409,
	UTF7Encoding_GetBytes_m9410,
	UTF7Encoding_GetBytes_m9411,
	UTF7Encoding_GetString_m9412,
	UTF8Decoder__ctor_m9413,
	UTF8Decoder_GetChars_m9414,
	UTF8Encoding__ctor_m9415,
	UTF8Encoding__ctor_m9416,
	UTF8Encoding__ctor_m9417,
	UTF8Encoding_InternalGetByteCount_m9418,
	UTF8Encoding_InternalGetByteCount_m9419,
	UTF8Encoding_GetByteCount_m9420,
	UTF8Encoding_GetByteCount_m9421,
	UTF8Encoding_InternalGetBytes_m9422,
	UTF8Encoding_InternalGetBytes_m9423,
	UTF8Encoding_GetBytes_m9424,
	UTF8Encoding_GetBytes_m9425,
	UTF8Encoding_GetBytes_m9426,
	UTF8Encoding_InternalGetCharCount_m9427,
	UTF8Encoding_InternalGetCharCount_m9428,
	UTF8Encoding_Fallback_m9429,
	UTF8Encoding_Fallback_m9430,
	UTF8Encoding_GetCharCount_m9431,
	UTF8Encoding_InternalGetChars_m9432,
	UTF8Encoding_InternalGetChars_m9433,
	UTF8Encoding_GetChars_m9434,
	UTF8Encoding_GetMaxByteCount_m9435,
	UTF8Encoding_GetMaxCharCount_m9436,
	UTF8Encoding_GetDecoder_m9437,
	UTF8Encoding_GetPreamble_m9438,
	UTF8Encoding_Equals_m9439,
	UTF8Encoding_GetHashCode_m9440,
	UTF8Encoding_GetByteCount_m9441,
	UTF8Encoding_GetString_m9442,
	UnicodeDecoder__ctor_m9443,
	UnicodeDecoder_GetChars_m9444,
	UnicodeEncoding__ctor_m9445,
	UnicodeEncoding__ctor_m9446,
	UnicodeEncoding__ctor_m9447,
	UnicodeEncoding_GetByteCount_m9448,
	UnicodeEncoding_GetByteCount_m9449,
	UnicodeEncoding_GetByteCount_m9450,
	UnicodeEncoding_GetBytes_m9451,
	UnicodeEncoding_GetBytes_m9452,
	UnicodeEncoding_GetBytes_m9453,
	UnicodeEncoding_GetBytesInternal_m9454,
	UnicodeEncoding_GetCharCount_m9455,
	UnicodeEncoding_GetChars_m9456,
	UnicodeEncoding_GetString_m9457,
	UnicodeEncoding_GetCharsInternal_m9458,
	UnicodeEncoding_GetMaxByteCount_m9459,
	UnicodeEncoding_GetMaxCharCount_m9460,
	UnicodeEncoding_GetDecoder_m9461,
	UnicodeEncoding_GetPreamble_m9462,
	UnicodeEncoding_Equals_m9463,
	UnicodeEncoding_GetHashCode_m9464,
	UnicodeEncoding_CopyChars_m9465,
	CompressedStack__ctor_m9466,
	CompressedStack__ctor_m9467,
	CompressedStack_CreateCopy_m9468,
	CompressedStack_Capture_m9469,
	CompressedStack_GetObjectData_m9470,
	CompressedStack_IsEmpty_m9471,
	EventWaitHandle__ctor_m9472,
	EventWaitHandle_IsManualReset_m9473,
	EventWaitHandle_Reset_m4436,
	EventWaitHandle_Set_m4434,
	ExecutionContext__ctor_m9474,
	ExecutionContext__ctor_m9475,
	ExecutionContext__ctor_m9476,
	ExecutionContext_Capture_m9477,
	ExecutionContext_GetObjectData_m9478,
	ExecutionContext_get_SecurityContext_m9479,
	ExecutionContext_set_SecurityContext_m9480,
	ExecutionContext_get_FlowSuppressed_m9481,
	ExecutionContext_IsFlowSuppressed_m9482,
	Interlocked_CompareExchange_m9483,
	ManualResetEvent__ctor_m4433,
	Monitor_Enter_m4417,
	Monitor_Exit_m4419,
	Monitor_Monitor_pulse_m9484,
	Monitor_Monitor_test_synchronised_m9485,
	Monitor_Pulse_m9486,
	Monitor_Monitor_wait_m9487,
	Monitor_Wait_m9488,
	Mutex__ctor_m9489,
	Mutex_CreateMutex_internal_m9490,
	Mutex_ReleaseMutex_internal_m9491,
	Mutex_ReleaseMutex_m9492,
	NativeEventCalls_CreateEvent_internal_m9493,
	NativeEventCalls_SetEvent_internal_m9494,
	NativeEventCalls_ResetEvent_internal_m9495,
	NativeEventCalls_CloseEvent_internal_m9496,
	SynchronizationLockException__ctor_m9497,
	SynchronizationLockException__ctor_m9498,
	SynchronizationLockException__ctor_m9499,
	Thread__ctor_m9500,
	Thread__cctor_m9501,
	Thread_get_CurrentContext_m9502,
	Thread_CurrentThread_internal_m9503,
	Thread_get_CurrentThread_m9504,
	Thread_FreeLocalSlotValues_m9505,
	Thread_GetDomainID_m9506,
	Thread_Thread_internal_m9507,
	Thread_Thread_init_m9508,
	Thread_GetCachedCurrentCulture_m9509,
	Thread_GetSerializedCurrentCulture_m9510,
	Thread_SetCachedCurrentCulture_m9511,
	Thread_GetCachedCurrentUICulture_m9512,
	Thread_GetSerializedCurrentUICulture_m9513,
	Thread_SetCachedCurrentUICulture_m9514,
	Thread_get_CurrentCulture_m9515,
	Thread_get_CurrentUICulture_m9516,
	Thread_set_IsBackground_m9517,
	Thread_SetName_internal_m9518,
	Thread_set_Name_m9519,
	Thread_Start_m9520,
	Thread_Thread_free_internal_m9521,
	Thread_Finalize_m9522,
	Thread_SetState_m9523,
	Thread_ClrState_m9524,
	Thread_GetNewManagedId_m9525,
	Thread_GetNewManagedId_internal_m9526,
	Thread_get_ExecutionContext_m9527,
	Thread_get_ManagedThreadId_m9528,
	Thread_GetHashCode_m9529,
	Thread_GetCompressedStack_m9530,
	ThreadAbortException__ctor_m9531,
	ThreadAbortException__ctor_m9532,
	ThreadInterruptedException__ctor_m9533,
	ThreadInterruptedException__ctor_m9534,
	ThreadPool_QueueUserWorkItem_m9535,
	ThreadStateException__ctor_m9536,
	ThreadStateException__ctor_m9537,
	TimerComparer__ctor_m9538,
	TimerComparer_Compare_m9539,
	Scheduler__ctor_m9540,
	Scheduler__cctor_m9541,
	Scheduler_get_Instance_m9542,
	Scheduler_Remove_m9543,
	Scheduler_Change_m9544,
	Scheduler_Add_m9545,
	Scheduler_InternalRemove_m9546,
	Scheduler_SchedulerThread_m9547,
	Scheduler_ShrinkIfNeeded_m9548,
	Timer__cctor_m9549,
	Timer_Change_m9550,
	Timer_Dispose_m9551,
	Timer_Change_m9552,
	WaitHandle__ctor_m9553,
	WaitHandle__cctor_m9554,
	WaitHandle_System_IDisposable_Dispose_m9555,
	WaitHandle_get_Handle_m9556,
	WaitHandle_set_Handle_m9557,
	WaitHandle_WaitOne_internal_m9558,
	WaitHandle_Dispose_m9559,
	WaitHandle_WaitOne_m9560,
	WaitHandle_WaitOne_m9561,
	WaitHandle_CheckDisposed_m9562,
	WaitHandle_Finalize_m9563,
	AccessViolationException__ctor_m9564,
	AccessViolationException__ctor_m9565,
	ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m9566,
	ActivationContext_Finalize_m9567,
	ActivationContext_Dispose_m9568,
	ActivationContext_Dispose_m9569,
	Activator_CreateInstance_m9570,
	Activator_CreateInstance_m9571,
	Activator_CreateInstance_m9572,
	Activator_CreateInstance_m9573,
	Activator_CreateInstance_m5377,
	Activator_CheckType_m9574,
	Activator_CheckAbstractType_m9575,
	Activator_CreateInstanceInternal_m9576,
	AppDomain_add_UnhandledException_m3233,
	AppDomain_remove_UnhandledException_m9577,
	AppDomain_getFriendlyName_m9578,
	AppDomain_getCurDomain_m9579,
	AppDomain_get_CurrentDomain_m3231,
	AppDomain_LoadAssembly_m9580,
	AppDomain_Load_m9581,
	AppDomain_Load_m9582,
	AppDomain_InternalSetContext_m9583,
	AppDomain_InternalGetContext_m9584,
	AppDomain_InternalGetDefaultContext_m9585,
	AppDomain_InternalGetProcessGuid_m9586,
	AppDomain_GetProcessGuid_m9587,
	AppDomain_ToString_m9588,
	AppDomain_DoTypeResolve_m9589,
	AppDomainSetup__ctor_m9590,
	ApplicationException__ctor_m9591,
	ApplicationException__ctor_m9592,
	ApplicationException__ctor_m9593,
	ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m9594,
	ApplicationIdentity_ToString_m9595,
	ArgumentException__ctor_m9596,
	ArgumentException__ctor_m1919,
	ArgumentException__ctor_m5395,
	ArgumentException__ctor_m3454,
	ArgumentException__ctor_m9597,
	ArgumentException__ctor_m9598,
	ArgumentException_get_ParamName_m9599,
	ArgumentException_get_Message_m9600,
	ArgumentException_GetObjectData_m9601,
	ArgumentNullException__ctor_m9602,
	ArgumentNullException__ctor_m3324,
	ArgumentNullException__ctor_m5346,
	ArgumentNullException__ctor_m9603,
	ArgumentOutOfRangeException__ctor_m5399,
	ArgumentOutOfRangeException__ctor_m4338,
	ArgumentOutOfRangeException__ctor_m3331,
	ArgumentOutOfRangeException__ctor_m9604,
	ArgumentOutOfRangeException__ctor_m9605,
	ArgumentOutOfRangeException_get_Message_m9606,
	ArgumentOutOfRangeException_GetObjectData_m9607,
	ArithmeticException__ctor_m9608,
	ArithmeticException__ctor_m4337,
	ArithmeticException__ctor_m9609,
	ArrayTypeMismatchException__ctor_m9610,
	ArrayTypeMismatchException__ctor_m9611,
	ArrayTypeMismatchException__ctor_m9612,
	BitConverter__cctor_m9613,
	BitConverter_AmILittleEndian_m9614,
	BitConverter_DoubleWordsAreSwapped_m9615,
	BitConverter_DoubleToInt64Bits_m9616,
	BitConverter_GetBytes_m9617,
	BitConverter_GetBytes_m9618,
	BitConverter_PutBytes_m9619,
	BitConverter_ToInt64_m9620,
	BitConverter_ToString_m4418,
	BitConverter_ToString_m9621,
	Buffer_ByteLength_m9622,
	Buffer_BlockCopy_m3450,
	Buffer_ByteLengthInternal_m9623,
	Buffer_BlockCopyInternal_m9624,
	CharEnumerator__ctor_m9625,
	CharEnumerator_System_Collections_IEnumerator_get_Current_m9626,
	CharEnumerator_System_IDisposable_Dispose_m9627,
	CharEnumerator_get_Current_m9628,
	CharEnumerator_MoveNext_m9629,
	Console__cctor_m9630,
	Console_SetEncodings_m9631,
	Console_get_Error_m5414,
	Console_Open_m9632,
	Console_OpenStandardError_m9633,
	Console_OpenStandardInput_m9634,
	Console_OpenStandardOutput_m9635,
	ContextBoundObject__ctor_m9636,
	Convert__cctor_m9637,
	Convert_InternalFromBase64String_m9638,
	Convert_FromBase64String_m4389,
	Convert_ToBase64String_m3304,
	Convert_ToBase64String_m9639,
	Convert_ToBoolean_m9640,
	Convert_ToBoolean_m9641,
	Convert_ToBoolean_m9642,
	Convert_ToBoolean_m9643,
	Convert_ToBoolean_m9644,
	Convert_ToBoolean_m9645,
	Convert_ToBoolean_m9646,
	Convert_ToBoolean_m9647,
	Convert_ToBoolean_m9648,
	Convert_ToBoolean_m9649,
	Convert_ToBoolean_m9650,
	Convert_ToBoolean_m9651,
	Convert_ToBoolean_m3301,
	Convert_ToBoolean_m9652,
	Convert_ToByte_m9653,
	Convert_ToByte_m9654,
	Convert_ToByte_m9655,
	Convert_ToByte_m9656,
	Convert_ToByte_m9657,
	Convert_ToByte_m9658,
	Convert_ToByte_m9659,
	Convert_ToByte_m9660,
	Convert_ToByte_m9661,
	Convert_ToByte_m9662,
	Convert_ToByte_m9663,
	Convert_ToByte_m9664,
	Convert_ToByte_m9665,
	Convert_ToByte_m9666,
	Convert_ToByte_m9667,
	Convert_ToChar_m4394,
	Convert_ToChar_m9668,
	Convert_ToChar_m9669,
	Convert_ToChar_m9670,
	Convert_ToChar_m9671,
	Convert_ToChar_m9672,
	Convert_ToChar_m9673,
	Convert_ToChar_m9674,
	Convert_ToChar_m9675,
	Convert_ToChar_m9676,
	Convert_ToChar_m9677,
	Convert_ToDateTime_m9678,
	Convert_ToDateTime_m9679,
	Convert_ToDateTime_m9680,
	Convert_ToDateTime_m9681,
	Convert_ToDateTime_m9682,
	Convert_ToDateTime_m9683,
	Convert_ToDateTime_m9684,
	Convert_ToDateTime_m9685,
	Convert_ToDateTime_m9686,
	Convert_ToDateTime_m9687,
	Convert_ToDecimal_m9688,
	Convert_ToDecimal_m9689,
	Convert_ToDecimal_m9690,
	Convert_ToDecimal_m9691,
	Convert_ToDecimal_m9692,
	Convert_ToDecimal_m9693,
	Convert_ToDecimal_m9694,
	Convert_ToDecimal_m9695,
	Convert_ToDecimal_m9696,
	Convert_ToDecimal_m9697,
	Convert_ToDecimal_m9698,
	Convert_ToDecimal_m9699,
	Convert_ToDecimal_m9700,
	Convert_ToDouble_m9701,
	Convert_ToDouble_m9702,
	Convert_ToDouble_m9703,
	Convert_ToDouble_m9704,
	Convert_ToDouble_m9705,
	Convert_ToDouble_m9706,
	Convert_ToDouble_m9707,
	Convert_ToDouble_m9708,
	Convert_ToDouble_m9709,
	Convert_ToDouble_m9710,
	Convert_ToDouble_m9711,
	Convert_ToDouble_m9712,
	Convert_ToDouble_m9713,
	Convert_ToDouble_m3341,
	Convert_ToInt16_m9714,
	Convert_ToInt16_m9715,
	Convert_ToInt16_m9716,
	Convert_ToInt16_m9717,
	Convert_ToInt16_m9718,
	Convert_ToInt16_m9719,
	Convert_ToInt16_m9720,
	Convert_ToInt16_m9721,
	Convert_ToInt16_m9722,
	Convert_ToInt16_m9723,
	Convert_ToInt16_m4349,
	Convert_ToInt16_m9724,
	Convert_ToInt16_m9725,
	Convert_ToInt16_m9726,
	Convert_ToInt16_m9727,
	Convert_ToInt16_m9728,
	Convert_ToInt32_m9729,
	Convert_ToInt32_m9730,
	Convert_ToInt32_m9731,
	Convert_ToInt32_m9732,
	Convert_ToInt32_m9733,
	Convert_ToInt32_m9734,
	Convert_ToInt32_m9735,
	Convert_ToInt32_m9736,
	Convert_ToInt32_m9737,
	Convert_ToInt32_m9738,
	Convert_ToInt32_m9739,
	Convert_ToInt32_m9740,
	Convert_ToInt32_m9741,
	Convert_ToInt32_m3298,
	Convert_ToInt32_m4402,
	Convert_ToInt64_m9742,
	Convert_ToInt64_m9743,
	Convert_ToInt64_m9744,
	Convert_ToInt64_m9745,
	Convert_ToInt64_m9746,
	Convert_ToInt64_m9747,
	Convert_ToInt64_m9748,
	Convert_ToInt64_m9749,
	Convert_ToInt64_m9750,
	Convert_ToInt64_m9751,
	Convert_ToInt64_m9752,
	Convert_ToInt64_m9753,
	Convert_ToInt64_m9754,
	Convert_ToInt64_m9755,
	Convert_ToInt64_m9756,
	Convert_ToInt64_m9757,
	Convert_ToInt64_m9758,
	Convert_ToSByte_m9759,
	Convert_ToSByte_m9760,
	Convert_ToSByte_m9761,
	Convert_ToSByte_m9762,
	Convert_ToSByte_m9763,
	Convert_ToSByte_m9764,
	Convert_ToSByte_m9765,
	Convert_ToSByte_m9766,
	Convert_ToSByte_m9767,
	Convert_ToSByte_m9768,
	Convert_ToSByte_m9769,
	Convert_ToSByte_m9770,
	Convert_ToSByte_m9771,
	Convert_ToSByte_m9772,
	Convert_ToSingle_m9773,
	Convert_ToSingle_m9774,
	Convert_ToSingle_m9775,
	Convert_ToSingle_m9776,
	Convert_ToSingle_m9777,
	Convert_ToSingle_m9778,
	Convert_ToSingle_m9779,
	Convert_ToSingle_m9780,
	Convert_ToSingle_m9781,
	Convert_ToSingle_m9782,
	Convert_ToSingle_m9783,
	Convert_ToSingle_m9784,
	Convert_ToSingle_m9785,
	Convert_ToSingle_m9786,
	Convert_ToString_m9787,
	Convert_ToString_m9788,
	Convert_ToUInt16_m9789,
	Convert_ToUInt16_m9790,
	Convert_ToUInt16_m9791,
	Convert_ToUInt16_m9792,
	Convert_ToUInt16_m9793,
	Convert_ToUInt16_m9794,
	Convert_ToUInt16_m9795,
	Convert_ToUInt16_m9796,
	Convert_ToUInt16_m9797,
	Convert_ToUInt16_m9798,
	Convert_ToUInt16_m9799,
	Convert_ToUInt16_m9800,
	Convert_ToUInt16_m9801,
	Convert_ToUInt16_m3299,
	Convert_ToUInt16_m9802,
	Convert_ToUInt32_m3259,
	Convert_ToUInt32_m9803,
	Convert_ToUInt32_m9804,
	Convert_ToUInt32_m9805,
	Convert_ToUInt32_m9806,
	Convert_ToUInt32_m9807,
	Convert_ToUInt32_m9808,
	Convert_ToUInt32_m9809,
	Convert_ToUInt32_m9810,
	Convert_ToUInt32_m9811,
	Convert_ToUInt32_m9812,
	Convert_ToUInt32_m9813,
	Convert_ToUInt32_m9814,
	Convert_ToUInt32_m3258,
	Convert_ToUInt32_m9815,
	Convert_ToUInt64_m9816,
	Convert_ToUInt64_m9817,
	Convert_ToUInt64_m9818,
	Convert_ToUInt64_m9819,
	Convert_ToUInt64_m9820,
	Convert_ToUInt64_m9821,
	Convert_ToUInt64_m9822,
	Convert_ToUInt64_m9823,
	Convert_ToUInt64_m9824,
	Convert_ToUInt64_m9825,
	Convert_ToUInt64_m9826,
	Convert_ToUInt64_m9827,
	Convert_ToUInt64_m9828,
	Convert_ToUInt64_m3300,
	Convert_ToUInt64_m9829,
	Convert_ChangeType_m9830,
	Convert_ToType_m9831,
	DBNull__ctor_m9832,
	DBNull__ctor_m9833,
	DBNull__cctor_m9834,
	DBNull_System_IConvertible_ToBoolean_m9835,
	DBNull_System_IConvertible_ToByte_m9836,
	DBNull_System_IConvertible_ToChar_m9837,
	DBNull_System_IConvertible_ToDateTime_m9838,
	DBNull_System_IConvertible_ToDecimal_m9839,
	DBNull_System_IConvertible_ToDouble_m9840,
	DBNull_System_IConvertible_ToInt16_m9841,
	DBNull_System_IConvertible_ToInt32_m9842,
	DBNull_System_IConvertible_ToInt64_m9843,
	DBNull_System_IConvertible_ToSByte_m9844,
	DBNull_System_IConvertible_ToSingle_m9845,
	DBNull_System_IConvertible_ToType_m9846,
	DBNull_System_IConvertible_ToUInt16_m9847,
	DBNull_System_IConvertible_ToUInt32_m9848,
	DBNull_System_IConvertible_ToUInt64_m9849,
	DBNull_GetObjectData_m9850,
	DBNull_ToString_m9851,
	DBNull_ToString_m9852,
	DateTime__ctor_m9853,
	DateTime__ctor_m9854,
	DateTime__ctor_m3367,
	DateTime__ctor_m9855,
	DateTime__ctor_m9856,
	DateTime__cctor_m9857,
	DateTime_System_IConvertible_ToBoolean_m9858,
	DateTime_System_IConvertible_ToByte_m9859,
	DateTime_System_IConvertible_ToChar_m9860,
	DateTime_System_IConvertible_ToDateTime_m9861,
	DateTime_System_IConvertible_ToDecimal_m9862,
	DateTime_System_IConvertible_ToDouble_m9863,
	DateTime_System_IConvertible_ToInt16_m9864,
	DateTime_System_IConvertible_ToInt32_m9865,
	DateTime_System_IConvertible_ToInt64_m9866,
	DateTime_System_IConvertible_ToSByte_m9867,
	DateTime_System_IConvertible_ToSingle_m9868,
	DateTime_System_IConvertible_ToType_m9869,
	DateTime_System_IConvertible_ToUInt16_m9870,
	DateTime_System_IConvertible_ToUInt32_m9871,
	DateTime_System_IConvertible_ToUInt64_m9872,
	DateTime_AbsoluteDays_m9873,
	DateTime_FromTicks_m9874,
	DateTime_get_Month_m9875,
	DateTime_get_Day_m9876,
	DateTime_get_DayOfWeek_m9877,
	DateTime_get_Hour_m9878,
	DateTime_get_Minute_m9879,
	DateTime_get_Second_m9880,
	DateTime_GetTimeMonotonic_m9881,
	DateTime_GetNow_m9882,
	DateTime_get_Now_m3248,
	DateTime_get_Ticks_m4420,
	DateTime_get_Today_m9883,
	DateTime_get_UtcNow_m4383,
	DateTime_get_Year_m9884,
	DateTime_get_Kind_m9885,
	DateTime_Add_m9886,
	DateTime_AddTicks_m9887,
	DateTime_AddMilliseconds_m5369,
	DateTime_AddSeconds_m3368,
	DateTime_Compare_m9888,
	DateTime_CompareTo_m9889,
	DateTime_CompareTo_m9890,
	DateTime_Equals_m9891,
	DateTime_FromBinary_m9892,
	DateTime_SpecifyKind_m9893,
	DateTime_DaysInMonth_m9894,
	DateTime_Equals_m9895,
	DateTime_CheckDateTimeKind_m9896,
	DateTime_GetHashCode_m9897,
	DateTime_IsLeapYear_m9898,
	DateTime_Parse_m9899,
	DateTime_Parse_m9900,
	DateTime_CoreParse_m9901,
	DateTime_YearMonthDayFormats_m9902,
	DateTime__ParseNumber_m9903,
	DateTime__ParseEnum_m9904,
	DateTime__ParseString_m9905,
	DateTime__ParseAmPm_m9906,
	DateTime__ParseTimeSeparator_m9907,
	DateTime__ParseDateSeparator_m9908,
	DateTime_IsLetter_m9909,
	DateTime__DoParse_m9910,
	DateTime_ParseExact_m4350,
	DateTime_ParseExact_m9911,
	DateTime_CheckStyle_m9912,
	DateTime_ParseExact_m9913,
	DateTime_Subtract_m9914,
	DateTime_ToString_m9915,
	DateTime_ToString_m9916,
	DateTime_ToString_m3353,
	DateTime_ToLocalTime_m5386,
	DateTime_ToUniversalTime_m3352,
	DateTime_op_Addition_m9917,
	DateTime_op_Equality_m9918,
	DateTime_op_GreaterThan_m4384,
	DateTime_op_GreaterThanOrEqual_m5370,
	DateTime_op_Inequality_m9919,
	DateTime_op_LessThan_m5394,
	DateTime_op_LessThanOrEqual_m4385,
	DateTime_op_Subtraction_m9920,
	DateTimeOffset__ctor_m9921,
	DateTimeOffset__ctor_m9922,
	DateTimeOffset__ctor_m9923,
	DateTimeOffset__ctor_m9924,
	DateTimeOffset__cctor_m9925,
	DateTimeOffset_System_IComparable_CompareTo_m9926,
	DateTimeOffset_System_Runtime_Serialization_ISerializable_GetObjectData_m9927,
	DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m9928,
	DateTimeOffset_CompareTo_m9929,
	DateTimeOffset_Equals_m9930,
	DateTimeOffset_Equals_m9931,
	DateTimeOffset_GetHashCode_m9932,
	DateTimeOffset_ToString_m9933,
	DateTimeOffset_ToString_m3355,
	DateTimeOffset_ToUniversalTime_m3354,
	DateTimeOffset_get_DateTime_m9934,
	DateTimeOffset_get_Offset_m9935,
	DateTimeOffset_get_UtcDateTime_m9936,
	DateTimeUtils_CountRepeat_m9937,
	DateTimeUtils_ZeroPad_m9938,
	DateTimeUtils_ParseQuotedString_m9939,
	DateTimeUtils_GetStandardPattern_m9940,
	DateTimeUtils_GetStandardPattern_m9941,
	DateTimeUtils_ToString_m9942,
	DateTimeUtils_ToString_m9943,
	DelegateEntry__ctor_m9944,
	DelegateEntry_DeserializeDelegate_m9945,
	DelegateSerializationHolder__ctor_m9946,
	DelegateSerializationHolder_GetDelegateData_m9947,
	DelegateSerializationHolder_GetObjectData_m9948,
	DelegateSerializationHolder_GetRealObject_m9949,
	DivideByZeroException__ctor_m9950,
	DivideByZeroException__ctor_m9951,
	DllNotFoundException__ctor_m9952,
	DllNotFoundException__ctor_m9953,
	EntryPointNotFoundException__ctor_m9954,
	EntryPointNotFoundException__ctor_m9955,
	SByteComparer__ctor_m9956,
	SByteComparer_Compare_m9957,
	SByteComparer_Compare_m9958,
	ShortComparer__ctor_m9959,
	ShortComparer_Compare_m9960,
	ShortComparer_Compare_m9961,
	IntComparer__ctor_m9962,
	IntComparer_Compare_m9963,
	IntComparer_Compare_m9964,
	LongComparer__ctor_m9965,
	LongComparer_Compare_m9966,
	LongComparer_Compare_m9967,
	MonoEnumInfo__ctor_m9968,
	MonoEnumInfo__cctor_m9969,
	MonoEnumInfo_get_enum_info_m9970,
	MonoEnumInfo_get_Cache_m9971,
	MonoEnumInfo_GetInfo_m9972,
	Environment_get_SocketSecurityEnabled_m9973,
	Environment_get_NewLine_m4342,
	Environment_get_Platform_m9974,
	Environment_GetOSVersionString_m9975,
	Environment_get_OSVersion_m9976,
	Environment_get_TickCount_m3305,
	Environment_internalGetEnvironmentVariable_m9977,
	Environment_GetEnvironmentVariable_m4415,
	Environment_GetWindowsFolderPath_m9978,
	Environment_GetFolderPath_m4400,
	Environment_ReadXdgUserDir_m9979,
	Environment_InternalGetFolderPath_m9980,
	Environment_get_IsRunningOnWindows_m9981,
	Environment_GetMachineConfigPath_m9982,
	Environment_internalGetHome_m9983,
	EventArgs__ctor_m9984,
	EventArgs__cctor_m9985,
	ExecutionEngineException__ctor_m9986,
	ExecutionEngineException__ctor_m9987,
	FieldAccessException__ctor_m9988,
	FieldAccessException__ctor_m9989,
	FieldAccessException__ctor_m9990,
	FlagsAttribute__ctor_m9991,
	FormatException__ctor_m9992,
	FormatException__ctor_m3297,
	FormatException__ctor_m5427,
	GC_SuppressFinalize_m3451,
	Guid__ctor_m9993,
	Guid__ctor_m9994,
	Guid__cctor_m9995,
	Guid_CheckNull_m9996,
	Guid_CheckLength_m9997,
	Guid_CheckArray_m9998,
	Guid_Compare_m9999,
	Guid_CompareTo_m10000,
	Guid_Equals_m10001,
	Guid_CompareTo_m10002,
	Guid_Equals_m10003,
	Guid_GetHashCode_m10004,
	Guid_ToHex_m10005,
	Guid_NewGuid_m10006,
	Guid_AppendInt_m10007,
	Guid_AppendShort_m10008,
	Guid_AppendByte_m10009,
	Guid_BaseToString_m10010,
	Guid_ToString_m10011,
	Guid_ToString_m3356,
	Guid_ToString_m10012,
	IndexOutOfRangeException__ctor_m10013,
	IndexOutOfRangeException__ctor_m3261,
	IndexOutOfRangeException__ctor_m10014,
	InvalidCastException__ctor_m10015,
	InvalidCastException__ctor_m10016,
	InvalidCastException__ctor_m10017,
	InvalidOperationException__ctor_m5349,
	InvalidOperationException__ctor_m4412,
	InvalidOperationException__ctor_m10018,
	InvalidOperationException__ctor_m10019,
	LocalDataStoreSlot__ctor_m10020,
	LocalDataStoreSlot__cctor_m10021,
	LocalDataStoreSlot_Finalize_m10022,
	Math_Abs_m10023,
	Math_Abs_m10024,
	Math_Abs_m10025,
	Math_Ceiling_m10026,
	Math_Floor_m10027,
	Math_Log_m3264,
	Math_Max_m4356,
	Math_Min_m3449,
	Math_Round_m10028,
	Math_Round_m10029,
	Math_Sin_m10030,
	Math_Cos_m10031,
	Math_Log_m10032,
	Math_Pow_m10033,
	Math_Sqrt_m10034,
	MemberAccessException__ctor_m10035,
	MemberAccessException__ctor_m10036,
	MemberAccessException__ctor_m10037,
	MethodAccessException__ctor_m10038,
	MethodAccessException__ctor_m10039,
	MissingFieldException__ctor_m10040,
	MissingFieldException__ctor_m10041,
	MissingFieldException__ctor_m10042,
	MissingFieldException_get_Message_m10043,
	MissingMemberException__ctor_m10044,
	MissingMemberException__ctor_m10045,
	MissingMemberException__ctor_m10046,
	MissingMemberException__ctor_m10047,
	MissingMemberException_GetObjectData_m10048,
	MissingMemberException_get_Message_m10049,
	MissingMethodException__ctor_m10050,
	MissingMethodException__ctor_m10051,
	MissingMethodException__ctor_m10052,
	MissingMethodException__ctor_m10053,
	MissingMethodException_get_Message_m10054,
	MonoAsyncCall__ctor_m10055,
	AttributeInfo__ctor_m10056,
	AttributeInfo_get_Usage_m10057,
	AttributeInfo_get_InheritanceLevel_m10058,
	MonoCustomAttrs__cctor_m10059,
	MonoCustomAttrs_IsUserCattrProvider_m10060,
	MonoCustomAttrs_GetCustomAttributesInternal_m10061,
	MonoCustomAttrs_GetPseudoCustomAttributes_m10062,
	MonoCustomAttrs_GetCustomAttributesBase_m10063,
	MonoCustomAttrs_GetCustomAttribute_m10064,
	MonoCustomAttrs_GetCustomAttributes_m10065,
	MonoCustomAttrs_GetCustomAttributes_m10066,
	MonoCustomAttrs_IsDefined_m10067,
	MonoCustomAttrs_IsDefinedInternal_m10068,
	MonoCustomAttrs_GetBasePropertyDefinition_m10069,
	MonoCustomAttrs_GetBase_m10070,
	MonoCustomAttrs_RetrieveAttributeUsage_m10071,
	MonoTouchAOTHelper__cctor_m10072,
	MonoTypeInfo__ctor_m10073,
	MonoType_get_attributes_m10074,
	MonoType_GetDefaultConstructor_m10075,
	MonoType_GetAttributeFlagsImpl_m10076,
	MonoType_GetConstructorImpl_m10077,
	MonoType_GetConstructors_internal_m10078,
	MonoType_GetConstructors_m10079,
	MonoType_InternalGetEvent_m10080,
	MonoType_GetEvent_m10081,
	MonoType_GetField_m10082,
	MonoType_GetFields_internal_m10083,
	MonoType_GetFields_m10084,
	MonoType_GetInterfaces_m10085,
	MonoType_GetMethodsByName_m10086,
	MonoType_GetMethods_m10087,
	MonoType_GetMethodImpl_m10088,
	MonoType_GetPropertiesByName_m10089,
	MonoType_GetProperties_m10090,
	MonoType_GetPropertyImpl_m10091,
	MonoType_HasElementTypeImpl_m10092,
	MonoType_IsArrayImpl_m10093,
	MonoType_IsByRefImpl_m10094,
	MonoType_IsPointerImpl_m10095,
	MonoType_IsPrimitiveImpl_m10096,
	MonoType_IsSubclassOf_m10097,
	MonoType_InvokeMember_m10098,
	MonoType_GetElementType_m10099,
	MonoType_get_UnderlyingSystemType_m10100,
	MonoType_get_Assembly_m10101,
	MonoType_get_AssemblyQualifiedName_m10102,
	MonoType_getFullName_m10103,
	MonoType_get_BaseType_m10104,
	MonoType_get_FullName_m10105,
	MonoType_IsDefined_m10106,
	MonoType_GetCustomAttributes_m10107,
	MonoType_GetCustomAttributes_m10108,
	MonoType_get_MemberType_m10109,
	MonoType_get_Name_m10110,
	MonoType_get_Namespace_m10111,
	MonoType_get_Module_m10112,
	MonoType_get_DeclaringType_m10113,
	MonoType_get_ReflectedType_m10114,
	MonoType_get_TypeHandle_m10115,
	MonoType_GetObjectData_m10116,
	MonoType_ToString_m10117,
	MonoType_GetGenericArguments_m10118,
	MonoType_get_ContainsGenericParameters_m10119,
	MonoType_get_IsGenericParameter_m10120,
	MonoType_GetGenericTypeDefinition_m10121,
	MonoType_CheckMethodSecurity_m10122,
	MonoType_ReorderParamArrayArguments_m10123,
	MulticastNotSupportedException__ctor_m10124,
	MulticastNotSupportedException__ctor_m10125,
	MulticastNotSupportedException__ctor_m10126,
	NonSerializedAttribute__ctor_m10127,
	NotImplementedException__ctor_m10128,
	NotImplementedException__ctor_m3453,
	NotImplementedException__ctor_m10129,
	NotSupportedException__ctor_m208,
	NotSupportedException__ctor_m4339,
	NotSupportedException__ctor_m10130,
	NullReferenceException__ctor_m10131,
	NullReferenceException__ctor_m3229,
	NullReferenceException__ctor_m10132,
	CustomInfo__ctor_m10133,
	CustomInfo_GetActiveSection_m10134,
	CustomInfo_Parse_m10135,
	CustomInfo_Format_m10136,
	NumberFormatter__ctor_m10137,
	NumberFormatter__cctor_m10138,
	NumberFormatter_GetFormatterTables_m10139,
	NumberFormatter_GetTenPowerOf_m10140,
	NumberFormatter_InitDecHexDigits_m10141,
	NumberFormatter_InitDecHexDigits_m10142,
	NumberFormatter_InitDecHexDigits_m10143,
	NumberFormatter_FastToDecHex_m10144,
	NumberFormatter_ToDecHex_m10145,
	NumberFormatter_FastDecHexLen_m10146,
	NumberFormatter_DecHexLen_m10147,
	NumberFormatter_DecHexLen_m10148,
	NumberFormatter_ScaleOrder_m10149,
	NumberFormatter_InitialFloatingPrecision_m10150,
	NumberFormatter_ParsePrecision_m10151,
	NumberFormatter_Init_m10152,
	NumberFormatter_InitHex_m10153,
	NumberFormatter_Init_m10154,
	NumberFormatter_Init_m10155,
	NumberFormatter_Init_m10156,
	NumberFormatter_Init_m10157,
	NumberFormatter_Init_m10158,
	NumberFormatter_Init_m10159,
	NumberFormatter_ResetCharBuf_m10160,
	NumberFormatter_Resize_m10161,
	NumberFormatter_Append_m10162,
	NumberFormatter_Append_m10163,
	NumberFormatter_Append_m10164,
	NumberFormatter_GetNumberFormatInstance_m10165,
	NumberFormatter_set_CurrentCulture_m10166,
	NumberFormatter_get_IntegerDigits_m10167,
	NumberFormatter_get_DecimalDigits_m10168,
	NumberFormatter_get_IsFloatingSource_m10169,
	NumberFormatter_get_IsZero_m10170,
	NumberFormatter_get_IsZeroInteger_m10171,
	NumberFormatter_RoundPos_m10172,
	NumberFormatter_RoundDecimal_m10173,
	NumberFormatter_RoundBits_m10174,
	NumberFormatter_RemoveTrailingZeros_m10175,
	NumberFormatter_AddOneToDecHex_m10176,
	NumberFormatter_AddOneToDecHex_m10177,
	NumberFormatter_CountTrailingZeros_m10178,
	NumberFormatter_CountTrailingZeros_m10179,
	NumberFormatter_GetInstance_m10180,
	NumberFormatter_Release_m10181,
	NumberFormatter_SetThreadCurrentCulture_m10182,
	NumberFormatter_NumberToString_m10183,
	NumberFormatter_NumberToString_m10184,
	NumberFormatter_NumberToString_m10185,
	NumberFormatter_NumberToString_m10186,
	NumberFormatter_NumberToString_m10187,
	NumberFormatter_NumberToString_m10188,
	NumberFormatter_NumberToString_m10189,
	NumberFormatter_NumberToString_m10190,
	NumberFormatter_NumberToString_m10191,
	NumberFormatter_NumberToString_m10192,
	NumberFormatter_NumberToString_m10193,
	NumberFormatter_NumberToString_m10194,
	NumberFormatter_NumberToString_m10195,
	NumberFormatter_NumberToString_m10196,
	NumberFormatter_NumberToString_m10197,
	NumberFormatter_NumberToString_m10198,
	NumberFormatter_NumberToString_m10199,
	NumberFormatter_FastIntegerToString_m10200,
	NumberFormatter_IntegerToString_m10201,
	NumberFormatter_NumberToString_m10202,
	NumberFormatter_FormatCurrency_m10203,
	NumberFormatter_FormatDecimal_m10204,
	NumberFormatter_FormatHexadecimal_m10205,
	NumberFormatter_FormatFixedPoint_m10206,
	NumberFormatter_FormatRoundtrip_m10207,
	NumberFormatter_FormatRoundtrip_m10208,
	NumberFormatter_FormatGeneral_m10209,
	NumberFormatter_FormatNumber_m10210,
	NumberFormatter_FormatPercent_m10211,
	NumberFormatter_FormatExponential_m10212,
	NumberFormatter_FormatExponential_m10213,
	NumberFormatter_FormatCustom_m10214,
	NumberFormatter_ZeroTrimEnd_m10215,
	NumberFormatter_IsZeroOnly_m10216,
	NumberFormatter_AppendNonNegativeNumber_m10217,
	NumberFormatter_AppendIntegerString_m10218,
	NumberFormatter_AppendIntegerString_m10219,
	NumberFormatter_AppendDecimalString_m10220,
	NumberFormatter_AppendDecimalString_m10221,
	NumberFormatter_AppendIntegerStringWithGroupSeparator_m10222,
	NumberFormatter_AppendExponent_m10223,
	NumberFormatter_AppendOneDigit_m10224,
	NumberFormatter_FastAppendDigits_m10225,
	NumberFormatter_AppendDigits_m10226,
	NumberFormatter_AppendDigits_m10227,
	NumberFormatter_Multiply10_m10228,
	NumberFormatter_Divide10_m10229,
	NumberFormatter_GetClone_m10230,
	ObjectDisposedException__ctor_m3455,
	ObjectDisposedException__ctor_m10231,
	ObjectDisposedException__ctor_m10232,
	ObjectDisposedException_get_Message_m10233,
	ObjectDisposedException_GetObjectData_m10234,
	OperatingSystem__ctor_m10235,
	OperatingSystem_get_Platform_m10236,
	OperatingSystem_GetObjectData_m10237,
	OperatingSystem_ToString_m10238,
	OutOfMemoryException__ctor_m10239,
	OutOfMemoryException__ctor_m10240,
	OverflowException__ctor_m10241,
	OverflowException__ctor_m10242,
	OverflowException__ctor_m10243,
	Random__ctor_m10244,
	Random__ctor_m3306,
	RankException__ctor_m10245,
	RankException__ctor_m10246,
	RankException__ctor_m10247,
	ResolveEventArgs__ctor_m10248,
	RuntimeMethodHandle__ctor_m10249,
	RuntimeMethodHandle__ctor_m10250,
	RuntimeMethodHandle_get_Value_m10251,
	RuntimeMethodHandle_GetObjectData_m10252,
	RuntimeMethodHandle_Equals_m10253,
	RuntimeMethodHandle_GetHashCode_m10254,
	StringComparer__ctor_m10255,
	StringComparer__cctor_m10256,
	StringComparer_get_InvariantCultureIgnoreCase_m5373,
	StringComparer_get_OrdinalIgnoreCase_m3254,
	StringComparer_Compare_m10257,
	StringComparer_Equals_m10258,
	StringComparer_GetHashCode_m10259,
	CultureAwareComparer__ctor_m10260,
	CultureAwareComparer_Compare_m10261,
	CultureAwareComparer_Equals_m10262,
	CultureAwareComparer_GetHashCode_m10263,
	OrdinalComparer__ctor_m10264,
	OrdinalComparer_Compare_m10265,
	OrdinalComparer_Equals_m10266,
	OrdinalComparer_GetHashCode_m10267,
	SystemException__ctor_m10268,
	SystemException__ctor_m5401,
	SystemException__ctor_m10269,
	SystemException__ctor_m10270,
	ThreadStaticAttribute__ctor_m10271,
	TimeSpan__ctor_m10272,
	TimeSpan__ctor_m10273,
	TimeSpan__ctor_m10274,
	TimeSpan__cctor_m10275,
	TimeSpan_CalculateTicks_m10276,
	TimeSpan_get_Days_m10277,
	TimeSpan_get_Hours_m10278,
	TimeSpan_get_Milliseconds_m10279,
	TimeSpan_get_Minutes_m10280,
	TimeSpan_get_Seconds_m10281,
	TimeSpan_get_Ticks_m10282,
	TimeSpan_get_TotalDays_m10283,
	TimeSpan_get_TotalHours_m10284,
	TimeSpan_get_TotalMilliseconds_m10285,
	TimeSpan_get_TotalMinutes_m10286,
	TimeSpan_get_TotalSeconds_m10287,
	TimeSpan_Add_m10288,
	TimeSpan_Compare_m10289,
	TimeSpan_CompareTo_m10290,
	TimeSpan_CompareTo_m10291,
	TimeSpan_Equals_m10292,
	TimeSpan_Duration_m10293,
	TimeSpan_Equals_m10294,
	TimeSpan_FromDays_m10295,
	TimeSpan_FromHours_m10296,
	TimeSpan_FromMinutes_m10297,
	TimeSpan_FromSeconds_m10298,
	TimeSpan_FromMilliseconds_m10299,
	TimeSpan_From_m10300,
	TimeSpan_GetHashCode_m10301,
	TimeSpan_Negate_m10302,
	TimeSpan_Subtract_m10303,
	TimeSpan_ToString_m10304,
	TimeSpan_op_Addition_m10305,
	TimeSpan_op_Equality_m10306,
	TimeSpan_op_GreaterThan_m10307,
	TimeSpan_op_GreaterThanOrEqual_m10308,
	TimeSpan_op_Inequality_m10309,
	TimeSpan_op_LessThan_m10310,
	TimeSpan_op_LessThanOrEqual_m10311,
	TimeSpan_op_Subtraction_m10312,
	TimeZone__ctor_m10313,
	TimeZone__cctor_m10314,
	TimeZone_get_CurrentTimeZone_m10315,
	TimeZone_IsDaylightSavingTime_m10316,
	TimeZone_IsDaylightSavingTime_m10317,
	TimeZone_ToLocalTime_m10318,
	TimeZone_ToUniversalTime_m10319,
	TimeZone_GetLocalTimeDiff_m10320,
	TimeZone_GetLocalTimeDiff_m10321,
	CurrentSystemTimeZone__ctor_m10322,
	CurrentSystemTimeZone__ctor_m10323,
	CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m10324,
	CurrentSystemTimeZone_GetTimeZoneData_m10325,
	CurrentSystemTimeZone_GetDaylightChanges_m10326,
	CurrentSystemTimeZone_GetUtcOffset_m10327,
	CurrentSystemTimeZone_OnDeserialization_m10328,
	CurrentSystemTimeZone_GetDaylightTimeFromData_m10329,
	TypeInitializationException__ctor_m10330,
	TypeInitializationException_GetObjectData_m10331,
	TypeLoadException__ctor_m10332,
	TypeLoadException__ctor_m10333,
	TypeLoadException__ctor_m10334,
	TypeLoadException_get_Message_m10335,
	TypeLoadException_GetObjectData_m10336,
	UnauthorizedAccessException__ctor_m10337,
	UnauthorizedAccessException__ctor_m10338,
	UnauthorizedAccessException__ctor_m10339,
	UnhandledExceptionEventArgs__ctor_m10340,
	UnhandledExceptionEventArgs_get_ExceptionObject_m3234,
	UnhandledExceptionEventArgs_get_IsTerminating_m10341,
	UnitySerializationHolder__ctor_m10342,
	UnitySerializationHolder_GetTypeData_m10343,
	UnitySerializationHolder_GetDBNullData_m10344,
	UnitySerializationHolder_GetModuleData_m10345,
	UnitySerializationHolder_GetObjectData_m10346,
	UnitySerializationHolder_GetRealObject_m10347,
	Version__ctor_m10348,
	Version__ctor_m10349,
	Version__ctor_m5364,
	Version__ctor_m10350,
	Version_CheckedSet_m10351,
	Version_get_Build_m10352,
	Version_get_Major_m10353,
	Version_get_Minor_m10354,
	Version_get_Revision_m10355,
	Version_CompareTo_m10356,
	Version_Equals_m10357,
	Version_CompareTo_m10358,
	Version_Equals_m10359,
	Version_GetHashCode_m10360,
	Version_ToString_m10361,
	Version_CreateFromString_m10362,
	Version_op_Equality_m10363,
	Version_op_Inequality_m10364,
	WeakReference__ctor_m10365,
	WeakReference__ctor_m10366,
	WeakReference__ctor_m10367,
	WeakReference__ctor_m10368,
	WeakReference_AllocateHandle_m10369,
	WeakReference_get_Target_m10370,
	WeakReference_get_TrackResurrection_m10371,
	WeakReference_Finalize_m10372,
	WeakReference_GetObjectData_m10373,
	PrimalityTest__ctor_m10374,
	PrimalityTest_Invoke_m10375,
	PrimalityTest_BeginInvoke_m10376,
	PrimalityTest_EndInvoke_m10377,
	MemberFilter__ctor_m10378,
	MemberFilter_Invoke_m10379,
	MemberFilter_BeginInvoke_m10380,
	MemberFilter_EndInvoke_m10381,
	TypeFilter__ctor_m10382,
	TypeFilter_Invoke_m10383,
	TypeFilter_BeginInvoke_m10384,
	TypeFilter_EndInvoke_m10385,
	CrossContextDelegate__ctor_m10386,
	CrossContextDelegate_Invoke_m10387,
	CrossContextDelegate_BeginInvoke_m10388,
	CrossContextDelegate_EndInvoke_m10389,
	HeaderHandler__ctor_m10390,
	HeaderHandler_Invoke_m10391,
	HeaderHandler_BeginInvoke_m10392,
	HeaderHandler_EndInvoke_m10393,
	ThreadStart__ctor_m10394,
	ThreadStart_Invoke_m10395,
	ThreadStart_BeginInvoke_m10396,
	ThreadStart_EndInvoke_m10397,
	TimerCallback__ctor_m10398,
	TimerCallback_Invoke_m10399,
	TimerCallback_BeginInvoke_m10400,
	TimerCallback_EndInvoke_m10401,
	WaitCallback__ctor_m10402,
	WaitCallback_Invoke_m10403,
	WaitCallback_BeginInvoke_m10404,
	WaitCallback_EndInvoke_m10405,
	AppDomainInitializer__ctor_m10406,
	AppDomainInitializer_Invoke_m10407,
	AppDomainInitializer_BeginInvoke_m10408,
	AppDomainInitializer_EndInvoke_m10409,
	AssemblyLoadEventHandler__ctor_m10410,
	AssemblyLoadEventHandler_Invoke_m10411,
	AssemblyLoadEventHandler_BeginInvoke_m10412,
	AssemblyLoadEventHandler_EndInvoke_m10413,
	EventHandler__ctor_m10414,
	EventHandler_Invoke_m10415,
	EventHandler_BeginInvoke_m10416,
	EventHandler_EndInvoke_m10417,
	ResolveEventHandler__ctor_m10418,
	ResolveEventHandler_Invoke_m10419,
	ResolveEventHandler_BeginInvoke_m10420,
	ResolveEventHandler_EndInvoke_m10421,
	UnhandledExceptionEventHandler__ctor_m3232,
	UnhandledExceptionEventHandler_Invoke_m10422,
	UnhandledExceptionEventHandler_BeginInvoke_m10423,
	UnhandledExceptionEventHandler_EndInvoke_m10424,
};
