﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// Player/<ExecuteAfterTime>c__IteratorE
struct  U3CExecuteAfterTimeU3Ec__IteratorE_t42  : public Object_t
{
	// System.Single Player/<ExecuteAfterTime>c__IteratorE::time
	float ___time_0;
	// System.Int32 Player/<ExecuteAfterTime>c__IteratorE::$PC
	int32_t ___U24PC_1;
	// System.Object Player/<ExecuteAfterTime>c__IteratorE::$current
	Object_t * ___U24current_2;
	// System.Single Player/<ExecuteAfterTime>c__IteratorE::<$>time
	float ___U3CU24U3Etime_3;
};
