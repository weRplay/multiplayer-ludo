﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m15699(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2169 *, uint64_t, NetworkAccessToken_t504 *, const MethodInfo*))KeyValuePair_2__ctor_m15604_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_Key()
#define KeyValuePair_2_get_Key_m15700(__this, method) (( uint64_t (*) (KeyValuePair_2_t2169 *, const MethodInfo*))KeyValuePair_2_get_Key_m15605_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m15701(__this, ___value, method) (( void (*) (KeyValuePair_2_t2169 *, uint64_t, const MethodInfo*))KeyValuePair_2_set_Key_m15606_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::get_Value()
#define KeyValuePair_2_get_Value_m15702(__this, method) (( NetworkAccessToken_t504 * (*) (KeyValuePair_2_t2169 *, const MethodInfo*))KeyValuePair_2_get_Value_m15607_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m15703(__this, ___value, method) (( void (*) (KeyValuePair_2_t2169 *, NetworkAccessToken_t504 *, const MethodInfo*))KeyValuePair_2_set_Value_m15608_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Networking.Types.NetworkID,UnityEngine.Networking.Types.NetworkAccessToken>::ToString()
#define KeyValuePair_2_ToString_m15704(__this, method) (( String_t* (*) (KeyValuePair_2_t2169 *, const MethodInfo*))KeyValuePair_2_ToString_m15609_gshared)(__this, method)
