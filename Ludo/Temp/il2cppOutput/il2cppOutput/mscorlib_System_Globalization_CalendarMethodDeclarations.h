﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Globalization.Calendar
struct Calendar_t1219;
// System.String[]
struct StringU5BU5D_t16;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Globalization.Calendar::.ctor()
extern "C" void Calendar__ctor_m7087 (Calendar_t1219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Globalization.Calendar::CheckReadOnly()
extern "C" void Calendar_CheckReadOnly_m7088 (Calendar_t1219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.String[] System.Globalization.Calendar::get_EraNames()
extern "C" StringU5BU5D_t16* Calendar_get_EraNames_m7089 (Calendar_t1219 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
