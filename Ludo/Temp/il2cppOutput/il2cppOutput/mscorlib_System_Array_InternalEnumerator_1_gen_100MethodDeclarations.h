﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_100.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17724_gshared (InternalEnumerator_1_t2369 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17724(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2369 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17724_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17725_gshared (InternalEnumerator_1_t2369 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17725(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2369 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17725_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17726_gshared (InternalEnumerator_1_t2369 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17726(__this, method) (( void (*) (InternalEnumerator_1_t2369 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17726_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17727_gshared (InternalEnumerator_1_t2369 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17727(__this, method) (( bool (*) (InternalEnumerator_1_t2369 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17727_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.Serialization.Formatters.Binary.TypeTag>::get_Current()
extern "C" uint8_t InternalEnumerator_1_get_Current_m17728_gshared (InternalEnumerator_1_t2369 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17728(__this, method) (( uint8_t (*) (InternalEnumerator_1_t2369 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17728_gshared)(__this, method)
