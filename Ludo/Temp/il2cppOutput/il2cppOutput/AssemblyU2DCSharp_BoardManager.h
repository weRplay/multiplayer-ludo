﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t3;
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t4;
// UnityEngine.GameObject
struct GameObject_t5;
// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t6;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// BoardManager
struct  BoardManager_t1  : public MonoBehaviour_t2
{
	// System.Int32 BoardManager::columns
	int32_t ___columns_2;
	// System.Int32 BoardManager::rows
	int32_t ___rows_3;
	// UnityEngine.Transform BoardManager::boardHolder
	Transform_t3 * ___boardHolder_4;
	// UnityEngine.GameObject[] BoardManager::floorTiles
	GameObjectU5BU5D_t4* ___floorTiles_5;
	// UnityEngine.GameObject[] BoardManager::outerWallTiles
	GameObjectU5BU5D_t4* ___outerWallTiles_6;
	// UnityEngine.GameObject BoardManager::Player
	GameObject_t5 * ___Player_7;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> BoardManager::BluePath
	List_1_t6 * ___BluePath_8;
	// System.Collections.Generic.List`1<UnityEngine.Vector3> BoardManager::RedPath
	List_1_t6 * ___RedPath_9;
};
