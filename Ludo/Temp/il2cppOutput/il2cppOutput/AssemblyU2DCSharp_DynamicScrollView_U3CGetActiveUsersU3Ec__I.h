﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t8;
// System.String
struct String_t;
// UnityEngine.WWW
struct WWW_t9;
// System.String[]
struct StringU5BU5D_t16;
// System.Object
struct Object_t;
// DynamicScrollView
struct DynamicScrollView_t14;

#include "mscorlib_System_Object.h"

// DynamicScrollView/<GetActiveUsers>c__Iterator2
struct  U3CGetActiveUsersU3Ec__Iterator2_t15  : public Object_t
{
	// UnityEngine.WWWForm DynamicScrollView/<GetActiveUsers>c__Iterator2::<form>__0
	WWWForm_t8 * ___U3CformU3E__0_0;
	// System.String DynamicScrollView/<GetActiveUsers>c__Iterator2::<username>__1
	String_t* ___U3CusernameU3E__1_1;
	// UnityEngine.WWW DynamicScrollView/<GetActiveUsers>c__Iterator2::<w>__2
	WWW_t9 * ___U3CwU3E__2_2;
	// System.String DynamicScrollView/<GetActiveUsers>c__Iterator2::<response>__3
	String_t* ___U3CresponseU3E__3_3;
	// System.String[] DynamicScrollView/<GetActiveUsers>c__Iterator2::<responseSplit>__4
	StringU5BU5D_t16* ___U3CresponseSplitU3E__4_4;
	// System.String DynamicScrollView/<GetActiveUsers>c__Iterator2::<playersName>__5
	String_t* ___U3CplayersNameU3E__5_5;
	// System.String DynamicScrollView/<GetActiveUsers>c__Iterator2::<matchDetails>__6
	String_t* ___U3CmatchDetailsU3E__6_6;
	// System.String[] DynamicScrollView/<GetActiveUsers>c__Iterator2::<details>__7
	StringU5BU5D_t16* ___U3CdetailsU3E__7_7;
	// System.String DynamicScrollView/<GetActiveUsers>c__Iterator2::<matchDetails>__8
	String_t* ___U3CmatchDetailsU3E__8_8;
	// System.String[] DynamicScrollView/<GetActiveUsers>c__Iterator2::<details>__9
	StringU5BU5D_t16* ___U3CdetailsU3E__9_9;
	// System.Int32 DynamicScrollView/<GetActiveUsers>c__Iterator2::$PC
	int32_t ___U24PC_10;
	// System.Object DynamicScrollView/<GetActiveUsers>c__Iterator2::$current
	Object_t * ___U24current_11;
	// DynamicScrollView DynamicScrollView/<GetActiveUsers>c__Iterator2::<>f__this
	DynamicScrollView_t14 * ___U3CU3Ef__this_12;
};
