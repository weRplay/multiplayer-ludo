﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_10MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m12533(__this, ___object, ___method, method) (( void (*) (Transform_1_t1924 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m12519_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m12534(__this, ___key, ___value, method) (( DictionaryEntry_t1068  (*) (Transform_1_t1924 *, Font_t142 *, List_1_t312 *, const MethodInfo*))Transform_1_Invoke_m12520_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m12535(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t1924 *, Font_t142 *, List_1_t312 *, AsyncCallback_t181 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m12521_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m12536(__this, ___result, method) (( DictionaryEntry_t1068  (*) (Transform_1_t1924 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m12522_gshared)(__this, ___result, method)
