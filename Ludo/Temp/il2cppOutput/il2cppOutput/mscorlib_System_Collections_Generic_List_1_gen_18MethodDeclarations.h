﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Object>
struct List_1_t344;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2022;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t51;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Object>
struct ICollection_1_t621;
// System.Collections.Generic.IEnumerable`1<System.Object>
struct IEnumerable_1_t346;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Object>
struct ReadOnlyCollection_1_t1803;
// System.Object[]
struct ObjectU5BU5D_t60;
// System.Predicate`1<System.Object>
struct Predicate_1_t1808;
// System.Comparison`1<System.Object>
struct Comparison_1_t1811;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3.h"

// System.Void System.Collections.Generic.List`1<System.Object>::.ctor()
extern "C" void List_1__ctor_m3317_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1__ctor_m3317(__this, method) (( void (*) (List_1_t344 *, const MethodInfo*))List_1__ctor_m3317_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.ctor(System.Int32)
extern "C" void List_1__ctor_m10626_gshared (List_1_t344 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m10626(__this, ___capacity, method) (( void (*) (List_1_t344 *, int32_t, const MethodInfo*))List_1__ctor_m10626_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Object>::.cctor()
extern "C" void List_1__cctor_m10628_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m10628(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m10628_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m10630_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m10630(__this, method) (( Object_t* (*) (List_1_t344 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m10630_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m10632_gshared (List_1_t344 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m10632(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t344 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m10632_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m10634_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m10634(__this, method) (( Object_t * (*) (List_1_t344 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m10634_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m10636_gshared (List_1_t344 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m10636(__this, ___item, method) (( int32_t (*) (List_1_t344 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m10636_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m10638_gshared (List_1_t344 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m10638(__this, ___item, method) (( bool (*) (List_1_t344 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m10638_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m10640_gshared (List_1_t344 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m10640(__this, ___item, method) (( int32_t (*) (List_1_t344 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m10640_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m10642_gshared (List_1_t344 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m10642(__this, ___index, ___item, method) (( void (*) (List_1_t344 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m10642_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m10644_gshared (List_1_t344 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m10644(__this, ___item, method) (( void (*) (List_1_t344 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m10644_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m10646_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m10646(__this, method) (( bool (*) (List_1_t344 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m10646_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m10648_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m10648(__this, method) (( bool (*) (List_1_t344 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m10648_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m10650_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m10650(__this, method) (( Object_t * (*) (List_1_t344 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m10650_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m10652_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m10652(__this, method) (( bool (*) (List_1_t344 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m10652_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m10654_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m10654(__this, method) (( bool (*) (List_1_t344 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m10654_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Object>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m10656_gshared (List_1_t344 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m10656(__this, ___index, method) (( Object_t * (*) (List_1_t344 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m10656_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m10658_gshared (List_1_t344 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m10658(__this, ___index, ___value, method) (( void (*) (List_1_t344 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m10658_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Add(T)
extern "C" void List_1_Add_m10660_gshared (List_1_t344 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Add_m10660(__this, ___item, method) (( void (*) (List_1_t344 *, Object_t *, const MethodInfo*))List_1_Add_m10660_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m10662_gshared (List_1_t344 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m10662(__this, ___newCount, method) (( void (*) (List_1_t344 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m10662_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m10664_gshared (List_1_t344 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m10664(__this, ___collection, method) (( void (*) (List_1_t344 *, Object_t*, const MethodInfo*))List_1_AddCollection_m10664_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m10666_gshared (List_1_t344 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m10666(__this, ___enumerable, method) (( void (*) (List_1_t344 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m10666_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Object>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m10668_gshared (List_1_t344 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m10668(__this, ___collection, method) (( void (*) (List_1_t344 *, Object_t*, const MethodInfo*))List_1_AddRange_m10668_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Object>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1803 * List_1_AsReadOnly_m10670_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m10670(__this, method) (( ReadOnlyCollection_1_t1803 * (*) (List_1_t344 *, const MethodInfo*))List_1_AsReadOnly_m10670_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Clear()
extern "C" void List_1_Clear_m10672_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_Clear_m10672(__this, method) (( void (*) (List_1_t344 *, const MethodInfo*))List_1_Clear_m10672_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Contains(T)
extern "C" bool List_1_Contains_m10674_gshared (List_1_t344 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Contains_m10674(__this, ___item, method) (( bool (*) (List_1_t344 *, Object_t *, const MethodInfo*))List_1_Contains_m10674_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m10676_gshared (List_1_t344 * __this, ObjectU5BU5D_t60* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m10676(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t344 *, ObjectU5BU5D_t60*, int32_t, const MethodInfo*))List_1_CopyTo_m10676_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Object>::Find(System.Predicate`1<T>)
extern "C" Object_t * List_1_Find_m10678_gshared (List_1_t344 * __this, Predicate_1_t1808 * ___match, const MethodInfo* method);
#define List_1_Find_m10678(__this, ___match, method) (( Object_t * (*) (List_1_t344 *, Predicate_1_t1808 *, const MethodInfo*))List_1_Find_m10678_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m10680_gshared (Object_t * __this /* static, unused */, Predicate_1_t1808 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m10680(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1808 *, const MethodInfo*))List_1_CheckMatch_m10680_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m10682_gshared (List_1_t344 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1808 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m10682(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t344 *, int32_t, int32_t, Predicate_1_t1808 *, const MethodInfo*))List_1_GetIndex_m10682_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Object>::GetEnumerator()
extern "C" Enumerator_t1802  List_1_GetEnumerator_m10684_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m10684(__this, method) (( Enumerator_t1802  (*) (List_1_t344 *, const MethodInfo*))List_1_GetEnumerator_m10684_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m10686_gshared (List_1_t344 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_IndexOf_m10686(__this, ___item, method) (( int32_t (*) (List_1_t344 *, Object_t *, const MethodInfo*))List_1_IndexOf_m10686_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m10688_gshared (List_1_t344 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m10688(__this, ___start, ___delta, method) (( void (*) (List_1_t344 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m10688_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m10690_gshared (List_1_t344 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m10690(__this, ___index, method) (( void (*) (List_1_t344 *, int32_t, const MethodInfo*))List_1_CheckIndex_m10690_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m10692_gshared (List_1_t344 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_Insert_m10692(__this, ___index, ___item, method) (( void (*) (List_1_t344 *, int32_t, Object_t *, const MethodInfo*))List_1_Insert_m10692_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Object>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m10694_gshared (List_1_t344 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m10694(__this, ___collection, method) (( void (*) (List_1_t344 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m10694_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Object>::Remove(T)
extern "C" bool List_1_Remove_m10696_gshared (List_1_t344 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_Remove_m10696(__this, ___item, method) (( bool (*) (List_1_t344 *, Object_t *, const MethodInfo*))List_1_Remove_m10696_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m10698_gshared (List_1_t344 * __this, Predicate_1_t1808 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m10698(__this, ___match, method) (( int32_t (*) (List_1_t344 *, Predicate_1_t1808 *, const MethodInfo*))List_1_RemoveAll_m10698_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Object>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m10700_gshared (List_1_t344 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m10700(__this, ___index, method) (( void (*) (List_1_t344 *, int32_t, const MethodInfo*))List_1_RemoveAt_m10700_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Reverse()
extern "C" void List_1_Reverse_m10702_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_Reverse_m10702(__this, method) (( void (*) (List_1_t344 *, const MethodInfo*))List_1_Reverse_m10702_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort()
extern "C" void List_1_Sort_m10704_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_Sort_m10704(__this, method) (( void (*) (List_1_t344 *, const MethodInfo*))List_1_Sort_m10704_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m10706_gshared (List_1_t344 * __this, Comparison_1_t1811 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m10706(__this, ___comparison, method) (( void (*) (List_1_t344 *, Comparison_1_t1811 *, const MethodInfo*))List_1_Sort_m10706_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Object>::ToArray()
extern "C" ObjectU5BU5D_t60* List_1_ToArray_m10708_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_ToArray_m10708(__this, method) (( ObjectU5BU5D_t60* (*) (List_1_t344 *, const MethodInfo*))List_1_ToArray_m10708_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::TrimExcess()
extern "C" void List_1_TrimExcess_m10710_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m10710(__this, method) (( void (*) (List_1_t344 *, const MethodInfo*))List_1_TrimExcess_m10710_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m10712_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m10712(__this, method) (( int32_t (*) (List_1_t344 *, const MethodInfo*))List_1_get_Capacity_m10712_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m10714_gshared (List_1_t344 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m10714(__this, ___value, method) (( void (*) (List_1_t344 *, int32_t, const MethodInfo*))List_1_set_Capacity_m10714_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Object>::get_Count()
extern "C" int32_t List_1_get_Count_m10716_gshared (List_1_t344 * __this, const MethodInfo* method);
#define List_1_get_Count_m10716(__this, method) (( int32_t (*) (List_1_t344 *, const MethodInfo*))List_1_get_Count_m10716_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * List_1_get_Item_m10718_gshared (List_1_t344 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m10718(__this, ___index, method) (( Object_t * (*) (List_1_t344 *, int32_t, const MethodInfo*))List_1_get_Item_m10718_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Object>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m10720_gshared (List_1_t344 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_set_Item_m10720(__this, ___index, ___value, method) (( void (*) (List_1_t344 *, int32_t, Object_t *, const MethodInfo*))List_1_set_Item_m10720_gshared)(__this, ___index, ___value, method)
