﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t8;
// UnityEngine.WWW
struct WWW_t9;
// UnityEngine.GameObject
struct GameObject_t5;
// System.Object
struct Object_t;
// GameManager
struct GameManager_t22;

#include "mscorlib_System_Object.h"

// GameManager/<AutomaticMovePlayer2>c__Iterator7
struct  U3CAutomaticMovePlayer2U3Ec__Iterator7_t25  : public Object_t
{
	// UnityEngine.WWWForm GameManager/<AutomaticMovePlayer2>c__Iterator7::<form>__0
	WWWForm_t8 * ___U3CformU3E__0_0;
	// UnityEngine.WWW GameManager/<AutomaticMovePlayer2>c__Iterator7::<w>__1
	WWW_t9 * ___U3CwU3E__1_1;
	// UnityEngine.GameObject GameManager/<AutomaticMovePlayer2>c__Iterator7::<toInstantiate>__2
	GameObject_t5 * ___U3CtoInstantiateU3E__2_2;
	// System.Int32 GameManager/<AutomaticMovePlayer2>c__Iterator7::$PC
	int32_t ___U24PC_3;
	// System.Object GameManager/<AutomaticMovePlayer2>c__Iterator7::$current
	Object_t * ___U24current_4;
	// GameManager GameManager/<AutomaticMovePlayer2>c__Iterator7::<>f__this
	GameManager_t22 * ___U3CU3Ef__this_5;
};
