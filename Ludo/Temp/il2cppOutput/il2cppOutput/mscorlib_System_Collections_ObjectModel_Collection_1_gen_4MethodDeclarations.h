﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>
struct Collection_1_t2108;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t51;
// System.Object
struct Object_t;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t612;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t2444;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t332;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void Collection_1__ctor_m15007_gshared (Collection_1_t2108 * __this, const MethodInfo* method);
#define Collection_1__ctor_m15007(__this, method) (( void (*) (Collection_1_t2108 *, const MethodInfo*))Collection_1__ctor_m15007_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15008_gshared (Collection_1_t2108 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15008(__this, method) (( bool (*) (Collection_1_t2108 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15008_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15009_gshared (Collection_1_t2108 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m15009(__this, ___array, ___index, method) (( void (*) (Collection_1_t2108 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m15009_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m15010_gshared (Collection_1_t2108 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m15010(__this, method) (( Object_t * (*) (Collection_1_t2108 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m15010_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m15011_gshared (Collection_1_t2108 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m15011(__this, ___value, method) (( int32_t (*) (Collection_1_t2108 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m15011_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m15012_gshared (Collection_1_t2108 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m15012(__this, ___value, method) (( bool (*) (Collection_1_t2108 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m15012_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m15013_gshared (Collection_1_t2108 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m15013(__this, ___value, method) (( int32_t (*) (Collection_1_t2108 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m15013_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m15014_gshared (Collection_1_t2108 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m15014(__this, ___index, ___value, method) (( void (*) (Collection_1_t2108 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m15014_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m15015_gshared (Collection_1_t2108 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m15015(__this, ___value, method) (( void (*) (Collection_1_t2108 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m15015_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m15016_gshared (Collection_1_t2108 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m15016(__this, method) (( bool (*) (Collection_1_t2108 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m15016_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m15017_gshared (Collection_1_t2108 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m15017(__this, method) (( Object_t * (*) (Collection_1_t2108 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m15017_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m15018_gshared (Collection_1_t2108 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m15018(__this, method) (( bool (*) (Collection_1_t2108 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m15018_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m15019_gshared (Collection_1_t2108 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m15019(__this, method) (( bool (*) (Collection_1_t2108 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m15019_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m15020_gshared (Collection_1_t2108 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m15020(__this, ___index, method) (( Object_t * (*) (Collection_1_t2108 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m15020_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m15021_gshared (Collection_1_t2108 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m15021(__this, ___index, ___value, method) (( void (*) (Collection_1_t2108 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m15021_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void Collection_1_Add_m15022_gshared (Collection_1_t2108 * __this, UILineInfo_t331  ___item, const MethodInfo* method);
#define Collection_1_Add_m15022(__this, ___item, method) (( void (*) (Collection_1_t2108 *, UILineInfo_t331 , const MethodInfo*))Collection_1_Add_m15022_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Clear()
extern "C" void Collection_1_Clear_m15023_gshared (Collection_1_t2108 * __this, const MethodInfo* method);
#define Collection_1_Clear_m15023(__this, method) (( void (*) (Collection_1_t2108 *, const MethodInfo*))Collection_1_Clear_m15023_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m15024_gshared (Collection_1_t2108 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m15024(__this, method) (( void (*) (Collection_1_t2108 *, const MethodInfo*))Collection_1_ClearItems_m15024_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m15025_gshared (Collection_1_t2108 * __this, UILineInfo_t331  ___item, const MethodInfo* method);
#define Collection_1_Contains_m15025(__this, ___item, method) (( bool (*) (Collection_1_t2108 *, UILineInfo_t331 , const MethodInfo*))Collection_1_Contains_m15025_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m15026_gshared (Collection_1_t2108 * __this, UILineInfoU5BU5D_t612* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m15026(__this, ___array, ___index, method) (( void (*) (Collection_1_t2108 *, UILineInfoU5BU5D_t612*, int32_t, const MethodInfo*))Collection_1_CopyTo_m15026_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m15027_gshared (Collection_1_t2108 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m15027(__this, method) (( Object_t* (*) (Collection_1_t2108 *, const MethodInfo*))Collection_1_GetEnumerator_m15027_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m15028_gshared (Collection_1_t2108 * __this, UILineInfo_t331  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m15028(__this, ___item, method) (( int32_t (*) (Collection_1_t2108 *, UILineInfo_t331 , const MethodInfo*))Collection_1_IndexOf_m15028_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m15029_gshared (Collection_1_t2108 * __this, int32_t ___index, UILineInfo_t331  ___item, const MethodInfo* method);
#define Collection_1_Insert_m15029(__this, ___index, ___item, method) (( void (*) (Collection_1_t2108 *, int32_t, UILineInfo_t331 , const MethodInfo*))Collection_1_Insert_m15029_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m15030_gshared (Collection_1_t2108 * __this, int32_t ___index, UILineInfo_t331  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m15030(__this, ___index, ___item, method) (( void (*) (Collection_1_t2108 *, int32_t, UILineInfo_t331 , const MethodInfo*))Collection_1_InsertItem_m15030_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m15031_gshared (Collection_1_t2108 * __this, UILineInfo_t331  ___item, const MethodInfo* method);
#define Collection_1_Remove_m15031(__this, ___item, method) (( bool (*) (Collection_1_t2108 *, UILineInfo_t331 , const MethodInfo*))Collection_1_Remove_m15031_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m15032_gshared (Collection_1_t2108 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m15032(__this, ___index, method) (( void (*) (Collection_1_t2108 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m15032_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m15033_gshared (Collection_1_t2108 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m15033(__this, ___index, method) (( void (*) (Collection_1_t2108 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m15033_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m15034_gshared (Collection_1_t2108 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m15034(__this, method) (( int32_t (*) (Collection_1_t2108 *, const MethodInfo*))Collection_1_get_Count_m15034_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t331  Collection_1_get_Item_m15035_gshared (Collection_1_t2108 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m15035(__this, ___index, method) (( UILineInfo_t331  (*) (Collection_1_t2108 *, int32_t, const MethodInfo*))Collection_1_get_Item_m15035_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m15036_gshared (Collection_1_t2108 * __this, int32_t ___index, UILineInfo_t331  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m15036(__this, ___index, ___value, method) (( void (*) (Collection_1_t2108 *, int32_t, UILineInfo_t331 , const MethodInfo*))Collection_1_set_Item_m15036_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m15037_gshared (Collection_1_t2108 * __this, int32_t ___index, UILineInfo_t331  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m15037(__this, ___index, ___item, method) (( void (*) (Collection_1_t2108 *, int32_t, UILineInfo_t331 , const MethodInfo*))Collection_1_SetItem_m15037_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m15038_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m15038(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m15038_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ConvertItem(System.Object)
extern "C" UILineInfo_t331  Collection_1_ConvertItem_m15039_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m15039(__this /* static, unused */, ___item, method) (( UILineInfo_t331  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m15039_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m15040_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m15040(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m15040_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m15041_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m15041(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m15041_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m15042_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m15042(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m15042_gshared)(__this /* static, unused */, ___list, method)
