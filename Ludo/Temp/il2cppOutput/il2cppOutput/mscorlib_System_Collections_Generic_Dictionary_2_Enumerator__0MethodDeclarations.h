﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m14466(__this, ___dictionary, method) (( void (*) (Enumerator_t649 *, Dictionary_2_t607 *, const MethodInfo*))Enumerator__ctor_m12484_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14467(__this, method) (( Object_t * (*) (Enumerator_t649 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12485_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14468(__this, method) (( DictionaryEntry_t1068  (*) (Enumerator_t649 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m12486_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14469(__this, method) (( Object_t * (*) (Enumerator_t649 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m12487_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14470(__this, method) (( Object_t * (*) (Enumerator_t649 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m12488_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::MoveNext()
#define Enumerator_MoveNext_m3277(__this, method) (( bool (*) (Enumerator_t649 *, const MethodInfo*))Enumerator_MoveNext_m12489_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::get_Current()
#define Enumerator_get_Current_m3274(__this, method) (( KeyValuePair_2_t648  (*) (Enumerator_t649 *, const MethodInfo*))Enumerator_get_Current_m12490_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m14471(__this, method) (( String_t* (*) (Enumerator_t649 *, const MethodInfo*))Enumerator_get_CurrentKey_m12491_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m14472(__this, method) (( String_t* (*) (Enumerator_t649 *, const MethodInfo*))Enumerator_get_CurrentValue_m12492_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::VerifyState()
#define Enumerator_VerifyState_m14473(__this, method) (( void (*) (Enumerator_t649 *, const MethodInfo*))Enumerator_VerifyState_m12493_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m14474(__this, method) (( void (*) (Enumerator_t649 *, const MethodInfo*))Enumerator_VerifyCurrent_m12494_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.String>::Dispose()
#define Enumerator_Dispose_m14475(__this, method) (( void (*) (Enumerator_t649 *, const MethodInfo*))Enumerator_Dispose_m12495_gshared)(__this, method)
