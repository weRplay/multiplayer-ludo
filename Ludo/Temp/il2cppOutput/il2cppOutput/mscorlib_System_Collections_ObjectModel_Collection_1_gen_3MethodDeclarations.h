﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>
struct Collection_1_t2099;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t51;
// System.Object
struct Object_t;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t611;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t2441;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t334;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void Collection_1__ctor_m14865_gshared (Collection_1_t2099 * __this, const MethodInfo* method);
#define Collection_1__ctor_m14865(__this, method) (( void (*) (Collection_1_t2099 *, const MethodInfo*))Collection_1__ctor_m14865_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14866_gshared (Collection_1_t2099 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14866(__this, method) (( bool (*) (Collection_1_t2099 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14866_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m14867_gshared (Collection_1_t2099 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m14867(__this, ___array, ___index, method) (( void (*) (Collection_1_t2099 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m14867_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m14868_gshared (Collection_1_t2099 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m14868(__this, method) (( Object_t * (*) (Collection_1_t2099 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m14868_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m14869_gshared (Collection_1_t2099 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m14869(__this, ___value, method) (( int32_t (*) (Collection_1_t2099 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m14869_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m14870_gshared (Collection_1_t2099 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m14870(__this, ___value, method) (( bool (*) (Collection_1_t2099 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m14870_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m14871_gshared (Collection_1_t2099 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m14871(__this, ___value, method) (( int32_t (*) (Collection_1_t2099 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m14871_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m14872_gshared (Collection_1_t2099 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m14872(__this, ___index, ___value, method) (( void (*) (Collection_1_t2099 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m14872_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m14873_gshared (Collection_1_t2099 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m14873(__this, ___value, method) (( void (*) (Collection_1_t2099 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m14873_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m14874_gshared (Collection_1_t2099 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m14874(__this, method) (( bool (*) (Collection_1_t2099 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m14874_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m14875_gshared (Collection_1_t2099 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m14875(__this, method) (( Object_t * (*) (Collection_1_t2099 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m14875_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m14876_gshared (Collection_1_t2099 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m14876(__this, method) (( bool (*) (Collection_1_t2099 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m14876_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m14877_gshared (Collection_1_t2099 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m14877(__this, method) (( bool (*) (Collection_1_t2099 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m14877_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m14878_gshared (Collection_1_t2099 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m14878(__this, ___index, method) (( Object_t * (*) (Collection_1_t2099 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m14878_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m14879_gshared (Collection_1_t2099 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m14879(__this, ___index, ___value, method) (( void (*) (Collection_1_t2099 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m14879_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void Collection_1_Add_m14880_gshared (Collection_1_t2099 * __this, UICharInfo_t333  ___item, const MethodInfo* method);
#define Collection_1_Add_m14880(__this, ___item, method) (( void (*) (Collection_1_t2099 *, UICharInfo_t333 , const MethodInfo*))Collection_1_Add_m14880_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Clear()
extern "C" void Collection_1_Clear_m14881_gshared (Collection_1_t2099 * __this, const MethodInfo* method);
#define Collection_1_Clear_m14881(__this, method) (( void (*) (Collection_1_t2099 *, const MethodInfo*))Collection_1_Clear_m14881_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m14882_gshared (Collection_1_t2099 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m14882(__this, method) (( void (*) (Collection_1_t2099 *, const MethodInfo*))Collection_1_ClearItems_m14882_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m14883_gshared (Collection_1_t2099 * __this, UICharInfo_t333  ___item, const MethodInfo* method);
#define Collection_1_Contains_m14883(__this, ___item, method) (( bool (*) (Collection_1_t2099 *, UICharInfo_t333 , const MethodInfo*))Collection_1_Contains_m14883_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m14884_gshared (Collection_1_t2099 * __this, UICharInfoU5BU5D_t611* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m14884(__this, ___array, ___index, method) (( void (*) (Collection_1_t2099 *, UICharInfoU5BU5D_t611*, int32_t, const MethodInfo*))Collection_1_CopyTo_m14884_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m14885_gshared (Collection_1_t2099 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m14885(__this, method) (( Object_t* (*) (Collection_1_t2099 *, const MethodInfo*))Collection_1_GetEnumerator_m14885_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m14886_gshared (Collection_1_t2099 * __this, UICharInfo_t333  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m14886(__this, ___item, method) (( int32_t (*) (Collection_1_t2099 *, UICharInfo_t333 , const MethodInfo*))Collection_1_IndexOf_m14886_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m14887_gshared (Collection_1_t2099 * __this, int32_t ___index, UICharInfo_t333  ___item, const MethodInfo* method);
#define Collection_1_Insert_m14887(__this, ___index, ___item, method) (( void (*) (Collection_1_t2099 *, int32_t, UICharInfo_t333 , const MethodInfo*))Collection_1_Insert_m14887_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m14888_gshared (Collection_1_t2099 * __this, int32_t ___index, UICharInfo_t333  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m14888(__this, ___index, ___item, method) (( void (*) (Collection_1_t2099 *, int32_t, UICharInfo_t333 , const MethodInfo*))Collection_1_InsertItem_m14888_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m14889_gshared (Collection_1_t2099 * __this, UICharInfo_t333  ___item, const MethodInfo* method);
#define Collection_1_Remove_m14889(__this, ___item, method) (( bool (*) (Collection_1_t2099 *, UICharInfo_t333 , const MethodInfo*))Collection_1_Remove_m14889_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m14890_gshared (Collection_1_t2099 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m14890(__this, ___index, method) (( void (*) (Collection_1_t2099 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m14890_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m14891_gshared (Collection_1_t2099 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m14891(__this, ___index, method) (( void (*) (Collection_1_t2099 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m14891_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m14892_gshared (Collection_1_t2099 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m14892(__this, method) (( int32_t (*) (Collection_1_t2099 *, const MethodInfo*))Collection_1_get_Count_m14892_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t333  Collection_1_get_Item_m14893_gshared (Collection_1_t2099 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m14893(__this, ___index, method) (( UICharInfo_t333  (*) (Collection_1_t2099 *, int32_t, const MethodInfo*))Collection_1_get_Item_m14893_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m14894_gshared (Collection_1_t2099 * __this, int32_t ___index, UICharInfo_t333  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m14894(__this, ___index, ___value, method) (( void (*) (Collection_1_t2099 *, int32_t, UICharInfo_t333 , const MethodInfo*))Collection_1_set_Item_m14894_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m14895_gshared (Collection_1_t2099 * __this, int32_t ___index, UICharInfo_t333  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m14895(__this, ___index, ___item, method) (( void (*) (Collection_1_t2099 *, int32_t, UICharInfo_t333 , const MethodInfo*))Collection_1_SetItem_m14895_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m14896_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m14896(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m14896_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ConvertItem(System.Object)
extern "C" UICharInfo_t333  Collection_1_ConvertItem_m14897_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m14897(__this /* static, unused */, ___item, method) (( UICharInfo_t333  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m14897_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m14898_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m14898(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m14898_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m14899_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m14899(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m14899_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m14900_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m14900(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m14900_gshared)(__this /* static, unused */, ___list, method)
