﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__12MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m15302(__this, ___dictionary, method) (( void (*) (Enumerator_t2136 *, Dictionary_2_t488 *, const MethodInfo*))Enumerator__ctor_m15200_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m15303(__this, method) (( Object_t * (*) (Enumerator_t2136 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15201_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15304(__this, method) (( DictionaryEntry_t1068  (*) (Enumerator_t2136 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m15202_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15305(__this, method) (( Object_t * (*) (Enumerator_t2136 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m15203_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15306(__this, method) (( Object_t * (*) (Enumerator_t2136 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m15204_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::MoveNext()
#define Enumerator_MoveNext_m15307(__this, method) (( bool (*) (Enumerator_t2136 *, const MethodInfo*))Enumerator_MoveNext_m15205_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_Current()
#define Enumerator_get_Current_m15308(__this, method) (( KeyValuePair_2_t2133  (*) (Enumerator_t2136 *, const MethodInfo*))Enumerator_get_Current_m15206_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m15309(__this, method) (( String_t* (*) (Enumerator_t2136 *, const MethodInfo*))Enumerator_get_CurrentKey_m15207_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m15310(__this, method) (( int64_t (*) (Enumerator_t2136 *, const MethodInfo*))Enumerator_get_CurrentValue_m15208_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::VerifyState()
#define Enumerator_VerifyState_m15311(__this, method) (( void (*) (Enumerator_t2136 *, const MethodInfo*))Enumerator_VerifyState_m15209_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m15312(__this, method) (( void (*) (Enumerator_t2136 *, const MethodInfo*))Enumerator_VerifyCurrent_m15210_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int64>::Dispose()
#define Enumerator_Dispose_m15313(__this, method) (( void (*) (Enumerator_t2136 *, const MethodInfo*))Enumerator_Dispose_m15211_gshared)(__this, method)
