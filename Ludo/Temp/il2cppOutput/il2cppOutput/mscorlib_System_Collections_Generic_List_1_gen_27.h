﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Networking.Match.MatchDesc[]
struct MatchDescU5BU5D_t2143;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>
struct  List_1_t499  : public Object_t
{
	// T[] System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::_items
	MatchDescU5BU5D_t2143* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::_version
	int32_t ____version_3;
};
struct List_1_t499_StaticFields{
	// T[] System.Collections.Generic.List`1<UnityEngine.Networking.Match.MatchDesc>::EmptyArray
	MatchDescU5BU5D_t2143* ___EmptyArray_4;
};
