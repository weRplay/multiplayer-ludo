﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Object
struct Object_t;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t108;
// UnityEngine.UI.ILayoutElement
struct ILayoutElement_t293;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t376;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t435;
// System.Collections.Specialized.ListDictionary/DictionaryNode
struct DictionaryNode_t917;
// System.Net.IPAddress
struct IPAddress_t946;
// System.Net.IPv6Address
struct IPv6Address_t948;
// System.UriFormatException
struct UriFormatException_t1058;
// System.Object[]
struct ObjectU5BU5D_t60;
// System.Exception
struct Exception_t301;
// System.MulticastDelegate
struct MulticastDelegate_t179;
// Mono.Globalization.Unicode.Contraction[]
struct ContractionU5BU5D_t1138;
// Mono.Globalization.Unicode.Level2Map[]
struct Level2MapU5BU5D_t1139;
// Mono.Globalization.Unicode.CodePointIndexer
struct CodePointIndexer_t1122;
// Mono.Globalization.Unicode.Contraction
struct Contraction_t1125;
// System.Reflection.MethodBase
struct MethodBase_t696;
// System.Reflection.Module
struct Module_t1289;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t1438;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t1700;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t631;
// System.Text.StringBuilder
struct StringBuilder_t296;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t1592;
// System.Char[]
struct CharU5BU5D_t58;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t1583;
// System.Int64[]
struct Int64U5BU5D_t1728;
// System.String[]
struct StringU5BU5D_t16;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t208;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t1835;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t185;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t611;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t612;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_Boolean.h"
#include "mscorlib_System_Single.h"
#include "mscorlib_System_Int32.h"
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "mscorlib_System_SByte.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirection.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Inp.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData.h"
#include "UnityEngine_UnityEngine_Touch.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Fra.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneInputModul.h"
#include "UnityEngine_UnityEngine_LayerMask.h"
#include "UnityEngine_UnityEngine_RaycastHit.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock.h"
#include "UnityEngine_UnityEngine_FontStyle.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_BlockingObjec.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Type.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethod.h"
#include "UnityEngine_UnityEngine_Vector4.h"
#include "UnityEngine_UnityEngine_UIVertex.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_LineType.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputType.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_CharacterValidation.h"
#include "mscorlib_System_Char.h"
#include "mscorlib_System_Int16.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_EditState.h"
#include "mscorlib_System_IntPtr.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementType.h"
#include "UnityEngine_UnityEngine_Bounds.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionState.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
#include "UnityEngine_UI_UnityEngine_UI_LayoutRebuilder.h"
#include "UnityEngine_UnityEngine_Color32.h"
#include "mscorlib_System_Double.h"
#include "mscorlib_System_Int64.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
#include "UnityEngine_UnityEngine_BoneWeight.h"
#include "mscorlib_System_DateTime.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstruc.h"
#include "UnityEngine_UnityEngine_EventType.h"
#include "UnityEngine_UnityEngine_EventModifiers.h"
#include "UnityEngine_UnityEngine_KeyCode.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_Matrix4x4.h"
#include "UnityEngine_UnityEngine_Ray.h"
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2.h"
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
#include "UnityEngine_UnityEngine_RenderBuffer.h"
#include "UnityEngine_UnityEngine_TouchPhase.h"
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
#include "UnityEngine_UnityEngine_RenderMode.h"
#include "UnityEngine_UnityEngine_Networking_Types_SourceID.h"
#include "UnityEngine_UnityEngine_Networking_Types_AppID.h"
#include "mscorlib_System_UInt16.h"
#include "mscorlib_System_UInt64.h"
#include "mscorlib_System_UInt32.h"
#include "UnityEngine_UnityEngine_Networking_Types_NetworkID.h"
#include "UnityEngine_UnityEngine_Networking_Types_NodeID.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
#include "Mono_Security_Mono_Math_BigInteger_Sign.h"
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"
#include "mscorlib_System_Byte.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"
#include "mscorlib_System_Security_Cryptography_RSAParameters.h"
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFlags.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLevel.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescription.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlgorithmType.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgorithmType.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAlgorithmTy.h"
#include "mscorlib_System_Security_Cryptography_CipherMode.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityProtocolTyp.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityCompression.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeState.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentType.h"
#include "System_System_Collections_Specialized_ListDictionary_Diction.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "System_System_ComponentModel_EditorBrowsableState.h"
#include "System_System_Net_IPAddress.h"
#include "System_System_Net_Sockets_AddressFamily.h"
#include "System_System_Net_IPv6Address.h"
#include "System_System_Net_SecurityProtocolType.h"
#include "System_System_Security_Cryptography_AsnDecodeStatus.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_1.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Rev.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Rev_0.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Ver.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Key_0.h"
#include "System_System_Text_RegularExpressions_RegexOptions.h"
#include "System_System_Text_RegularExpressions_Category.h"
#include "System_System_Text_RegularExpressions_OpFlags.h"
#include "System_System_Text_RegularExpressions_Interval.h"
#include "System_System_Text_RegularExpressions_Position.h"
#include "System_System_UriHostNameType.h"
#include "System_System_UriFormatException.h"
#include "mscorlib_System_Decimal.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_TypeCode.h"
#include "mscorlib_System_Globalization_UnicodeCategory.h"
#include "mscorlib_System_UIntPtr.h"
#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Reflection_TypeAttributes.h"
#include "mscorlib_System_Reflection_MemberTypes.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_RuntimeFieldHandle.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer.h"
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ExtenderT.h"
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_Context.h"
#include "mscorlib_Mono_Globalization_Unicode_Contraction.h"
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"
#include "mscorlib_Mono_Math_BigInteger_Sign.h"
#include "mscorlib_System_Reflection_MethodBase.h"
#include "mscorlib_System_DayOfWeek.h"
#include "mscorlib_System_TimeSpan.h"
#include "mscorlib_System_IO_MonoIOError.h"
#include "mscorlib_System_IO_FileAttributes.h"
#include "mscorlib_System_IO_MonoFileType.h"
#include "mscorlib_System_IO_MonoIOStat.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_Reflection_MethodAttributes.h"
#include "mscorlib_System_Reflection_Emit_MethodToken.h"
#include "mscorlib_System_Reflection_FieldAttributes.h"
#include "mscorlib_System_Reflection_Emit_OpCode.h"
#include "mscorlib_System_Reflection_Emit_StackBehaviour.h"
#include "mscorlib_System_Reflection_PropertyAttributes.h"
#include "mscorlib_System_Reflection_Module.h"
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"
#include "mscorlib_System_Reflection_EventAttributes.h"
#include "mscorlib_System_Reflection_MonoEventInfo.h"
#include "mscorlib_System_Reflection_MonoMethodInfo.h"
#include "mscorlib_System_Reflection_MonoPropertyInfo.h"
#include "mscorlib_System_Reflection_ParameterAttributes.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
#include "mscorlib_System_Runtime_Serialization_SerializationEntry.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContextStates.h"
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"
#include "mscorlib_System_Security_Cryptography_PaddingMode.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "mscorlib_System_Text_EncoderFallbackBuffer.h"
#include "mscorlib_System_Text_DecoderFallbackBuffer.h"
#include "mscorlib_System_DateTimeKind.h"
#include "mscorlib_System_DateTimeOffset.h"
#include "mscorlib_System_Nullable_1_gen.h"
#include "mscorlib_System_MonoEnumInfo.h"
#include "mscorlib_System_PlatformID.h"
#include "mscorlib_System_Guid.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__4.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_6.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_9.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_3.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_5.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__2.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_4.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"
#include "UnityEngine.UI_ArrayTypes.h"
#include "mscorlib_System_Collections_Generic_Link.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
#include "UnityEngine_UnityEngine_Keyframe.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_14.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_16.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"
#include "mscorlib_System_Reflection_ParameterModifier.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_23.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_ClientCer.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_25.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_27.h"
#include "System_System_Text_RegularExpressions_Mark.h"
#include "System_System_Uri_UriScheme.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"
#include "mscorlib_System_Collections_Hashtable_Slot.h"
#include "mscorlib_System_Collections_SortedList_Slot.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelData.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixup.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_7.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_1.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_4.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_7.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_14.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_25.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_26.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__12.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_16.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_18.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__14.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_19.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_21.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__17.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_23.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_25.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__19.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_26.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_28.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__21.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_29.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_31.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__23.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_32.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_34.h"

void* RuntimeInvoker_Void_t1089 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, const MethodInfo* method);
	((Func)method->method)(obj, method);
	return NULL;
}

void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, method);
	return ret;
}

void* RuntimeInvoker_Boolean_t298 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Single_t61_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t298_Int32_t56_Int32_t56_RaycastHit2DU26_t2723 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, RaycastHit2D_t52 * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (RaycastHit2D_t52 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t35  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_RaycastResult_t103_RaycastResult_t103 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t103  p1, RaycastResult_t103  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t103 *)args[0]), *((RaycastResult_t103 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Vector2_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t59  (*Func)(void* obj, const MethodInfo* method);
	Vector2_t59  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Vector2_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t59  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t59 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_MoveDirection_t100 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastResult_t103 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t103  (*Func)(void* obj, const MethodInfo* method);
	RaycastResult_t103  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_RaycastResult_t103 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResult_t103  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastResult_t103 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t35  (*Func)(void* obj, const MethodInfo* method);
	Vector3_t35  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t35  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t35 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_InputButton_t106 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastResult_t103_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t103  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RaycastResult_t103  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MoveDirection_t100_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MoveDirection_t100_Single_t61_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t298_Int32_t56_PointerEventDataU26_t2724_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, PointerEventData_t108 ** p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (PointerEventData_t108 **)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Touch_t282_BooleanU26_t2725_BooleanU26_t2725 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Touch_t282  p1, bool* p2, bool* p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Touch_t282 *)args[0]), (bool*)args[1], (bool*)args[2], method);
	return ret;
}

void* RuntimeInvoker_FramePressState_t107_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t298_Vector2_t59_Vector2_t59_Single_t61_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t59  p1, Vector2_t59  p2, float p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), *((Vector2_t59 *)args[1]), *((float*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_InputMode_t117 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_LayerMask_t37 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t37  (*Func)(void* obj, const MethodInfo* method);
	LayerMask_t37  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_LayerMask_t37 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LayerMask_t37  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LayerMask_t37 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_RaycastHit_t283_RaycastHit_t283 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t283  p1, RaycastHit_t283  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t283 *)args[0]), *((RaycastHit_t283 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t128 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t128  (*Func)(void* obj, const MethodInfo* method);
	Color_t128  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Color_t128 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t128  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t128 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_ColorTweenMode_t124 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ColorBlock_t140 (const MethodInfo* method, void* obj, void** args)
{
	typedef ColorBlock_t140  (*Func)(void* obj, const MethodInfo* method);
	ColorBlock_t140  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FontStyle_t415 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextAnchor_t348 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HorizontalWrapMode_t476 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VerticalWrapMode_t477 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Vector2_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t59  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t59_Vector2_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t59  (*Func)(void* obj, Vector2_t59  p1, const MethodInfo* method);
	Vector2_t59  ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t184 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t184  (*Func)(void* obj, const MethodInfo* method);
	Rect_t184  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Color_t128_Single_t61_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t128  p1, float p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t128 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Color_t128_Single_t61_SByte_t666_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t128  p1, float p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t128 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Color_t128_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t128  (*Func)(void* obj, float p1, const MethodInfo* method);
	Color_t128  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Single_t61_Single_t61_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_BlockingObjects_t154 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Vector2_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Vector2_t59  p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Vector2_t59 *)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Type_t160 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FillMethod_t161 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t287_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t287  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Vector4_t287  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_UIVertex_t191_Vector2_t59_Vector2_t59_Vector2_t59_Vector2_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, UIVertex_t191  p2, Vector2_t59  p3, Vector2_t59  p4, Vector2_t59  p5, Vector2_t59  p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((UIVertex_t191 *)args[1]), *((Vector2_t59 *)args[2]), *((Vector2_t59 *)args[3]), *((Vector2_t59 *)args[4]), *((Vector2_t59 *)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Vector4_t287_Vector4_t287_Rect_t184 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t287  (*Func)(void* obj, Vector4_t287  p1, Rect_t184  p2, const MethodInfo* method);
	Vector4_t287  ret = ((Func)method->method)(obj, *((Vector4_t287 *)args[0]), *((Rect_t184 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Object_t_Single_t61_SByte_t666_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, int8_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Single_t61_Single_t61_SByte_t666_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Vector2_t59_Vector2_t59_Rect_t184 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t59  (*Func)(void* obj, Vector2_t59  p1, Rect_t184  p2, const MethodInfo* method);
	Vector2_t59  ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), *((Rect_t184 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ContentType_t170 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LineType_t173 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_InputType_t171 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TouchScreenKeyboardType_t327 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CharacterValidation_t172 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t326 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Vector2_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t59  p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Vector2_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t59  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_EditState_t177_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Int32_t56_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32U26_t2726_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t* p3, int32_t* p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Vector2_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t59  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t59 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Single_t61_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t326_Object_t_Int32_t56_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56_Int16_t667_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Char_t326_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Mode_t193 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Navigation_t194 (const MethodInfo* method, void* obj, void** args)
{
	typedef Navigation_t194  (*Func)(void* obj, const MethodInfo* method);
	Navigation_t194  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Rect_t184 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t184  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t184 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Direction_t197 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Single_t61_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Axis_t200 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MovementType_t204 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Single_t61_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Single_t61_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Bounds_t207 (const MethodInfo* method, void* obj, void** args)
{
	typedef Bounds_t207  (*Func)(void* obj, const MethodInfo* method);
	Bounds_t207  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Navigation_t194 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Navigation_t194  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Navigation_t194 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Transition_t209 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_ColorBlock_t140 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorBlock_t140  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ColorBlock_t140 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_SpriteState_t211 (const MethodInfo* method, void* obj, void** args)
{
	typedef SpriteState_t211  (*Func)(void* obj, const MethodInfo* method);
	SpriteState_t211  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_SpriteState_t211 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SpriteState_t211  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((SpriteState_t211 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_SelectionState_t210 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t35_Object_t_Vector2_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t35  (*Func)(void* obj, Object_t * p1, Vector2_t59  p2, const MethodInfo* method);
	Vector3_t35  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t59 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Color_t128_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t128  p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t128 *)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_ColorU26_t2727_Color_t128 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t128 * p1, Color_t128  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Color_t128 *)args[0], *((Color_t128 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Direction_t215 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Axis_t217 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_TextGenerationSettings_t290_Vector2_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t290  (*Func)(void* obj, Vector2_t59  p1, const MethodInfo* method);
	TextGenerationSettings_t290  ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t59_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t59  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector2_t59  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AspectMode_t229 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Single_t61_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ScaleMode_t231 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ScreenMatchMode_t232 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Unit_t233 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FitMode_t235 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Corner_t237 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Axis_t238 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Constraint_t239 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Int32_t56_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Single_t61_Single_t61_Single_t61_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_LayoutRebuilder_t246 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LayoutRebuilder_t246  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LayoutRebuilder_t246 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Object_t_Object_t_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Object_t_Object_t_Single_t61_ILayoutElementU26_t2728 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, Object_t ** p4, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), (Object_t **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Color32_t295_Int32_t56_Int32_t56_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color32_t295  p2, int32_t p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color32_t295 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_LayerMask_t37 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LayerMask_t37  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LayerMask_t37 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LayerMask_t37_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t37  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LayerMask_t37  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int64_t662_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_GcAchievementDescriptionData_t545_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementDescriptionData_t545  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementDescriptionData_t545 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_GcUserProfileData_t544_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcUserProfileData_t544  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcUserProfileData_t544 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Double_t661_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int64_t662_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_UserProfileU5BU5DU26_t2729_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t376** p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t376**)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_UserProfileU5BU5DU26_t2729_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t376** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t376**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_GcScoreData_t547 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcScoreData_t547  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcScoreData_t547 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_BoneWeight_t381_BoneWeight_t381 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, BoneWeight_t381  p1, BoneWeight_t381  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((BoneWeight_t381 *)args[0]), *((BoneWeight_t381 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Int32_t56_SByte_t666_SByte_t666_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, IntPtr_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((IntPtr_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Color_t128_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t128  (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	Color_t128  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Vector3U26_t2730 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t35 * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t35 *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Color_t128_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t128  p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t128 *)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_DateTime_t396 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t396  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t396 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Object_t_Int32_t56_Single_t61_Single_t61_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, float p5, float p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t56_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t56_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Rect_t184_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t184  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Rect_t184  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Rect_t184 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t184  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t184 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_RectU26_t2731 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t184 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Rect_t184 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Single_t61_Single_t61_Single_t61_Single_t61_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_ColorU26_t2727 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t128 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t128 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Rect_t184_Rect_t184 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t184  (*Func)(void* obj, Rect_t184  p1, const MethodInfo* method);
	Rect_t184  ret = ((Func)method->method)(obj, *((Rect_t184 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t184_Object_t_RectU26_t2731 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t184  (*Func)(void* obj, Object_t * p1, Rect_t184 * p2, const MethodInfo* method);
	Rect_t184  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Rect_t184 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t59_Rect_t184_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t59  (*Func)(void* obj, Rect_t184  p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Vector2_t59  ret = ((Func)method->method)(obj, *((Rect_t184 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_IntPtr_t_Rect_t184_Object_t_Int32_t56_Vector2U26_t2732 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t184  p2, Object_t * p3, int32_t p4, Vector2_t59 * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((Rect_t184 *)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Vector2_t59 *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_IntPtr_t_RectU26_t2731_Object_t_Int32_t56_Vector2U26_t2732 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Rect_t184 * p2, Object_t * p3, int32_t p4, Vector2_t59 * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Rect_t184 *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Vector2_t59 *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Vector2_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t59  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector2_t59  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_IntPtr_t_Object_t_Vector2U26_t2732 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, Vector2_t59 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], (Vector2_t59 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Single_t61_Object_t_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_IntPtr_t_Object_t_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, IntPtr_t p1, Object_t * p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_SByte_t666_SByte_t666_SByte_t666_SByte_t666_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_TouchScreenKeyboard_InternalConstructorHelperArgumentsU26_t2733_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TouchScreenKeyboard_InternalConstructorHelperArguments_t416 * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (TouchScreenKeyboard_InternalConstructorHelperArguments_t416 *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56_SByte_t666_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56_SByte_t666_SByte_t666_SByte_t666_SByte_t666_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return ret;
}

void* RuntimeInvoker_EventType_t419 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Vector2U26_t2732 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t59 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t59 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_EventModifiers_t420 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyCode_t418 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Vector2_t59_Vector2_t59_Vector2_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t59  (*Func)(void* obj, Vector2_t59  p1, Vector2_t59  p2, const MethodInfo* method);
	Vector2_t59  ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), *((Vector2_t59 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Vector2_t59_Vector2_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t59  p1, Vector2_t59  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), *((Vector2_t59 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Vector2_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t59  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t59_Vector2_t59_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t59  (*Func)(void* obj, Vector2_t59  p1, float p2, const MethodInfo* method);
	Vector2_t59  ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Vector2_t59_Vector2_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t59  p1, Vector2_t59  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), *((Vector2_t59 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t59_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t59  (*Func)(void* obj, Vector3_t35  p1, const MethodInfo* method);
	Vector2_t59  ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t35_Vector2_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t35  (*Func)(void* obj, Vector2_t59  p1, const MethodInfo* method);
	Vector3_t35  ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Single_t61_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Vector3_t35_Vector3_t35_Vector3_t35_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t35  (*Func)(void* obj, Vector3_t35  p1, Vector3_t35  p2, float p3, const MethodInfo* method);
	Vector3_t35  ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), *((Vector3_t35 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t35_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t35  (*Func)(void* obj, Vector3_t35  p1, const MethodInfo* method);
	Vector3_t35  ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Vector3_t35_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t35  p1, Vector3_t35  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), *((Vector3_t35 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t35  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t35_Vector3_t35_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t35  (*Func)(void* obj, Vector3_t35  p1, Vector3_t35  p2, const MethodInfo* method);
	Vector3_t35  ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), *((Vector3_t35 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t35_Vector3_t35_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t35  (*Func)(void* obj, Vector3_t35  p1, float p2, const MethodInfo* method);
	Vector3_t35  ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Vector3_t35_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t35  p1, Vector3_t35  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), *((Vector3_t35 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Single_t61_Single_t61_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Color_t128_Color_t128_Color_t128_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t128  (*Func)(void* obj, Color_t128  p1, Color_t128  p2, float p3, const MethodInfo* method);
	Color_t128  ret = ((Func)method->method)(obj, *((Color_t128 *)args[0]), *((Color_t128 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t128_Color_t128_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t128  (*Func)(void* obj, Color_t128  p1, float p2, const MethodInfo* method);
	Color_t128  ret = ((Func)method->method)(obj, *((Color_t128 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t287_Color_t128 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t287  (*Func)(void* obj, Color_t128  p1, const MethodInfo* method);
	Vector4_t287  ret = ((Func)method->method)(obj, *((Color_t128 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_SByte_t666_SByte_t666_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Color32_t295_Color_t128 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t295  (*Func)(void* obj, Color_t128  p1, const MethodInfo* method);
	Color32_t295  ret = ((Func)method->method)(obj, *((Color_t128 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t128_Color32_t295 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t128  (*Func)(void* obj, Color32_t295  p1, const MethodInfo* method);
	Color_t128  ret = ((Func)method->method)(obj, *((Color32_t295 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t53  (*Func)(void* obj, const MethodInfo* method);
	Quaternion_t53  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Quaternion_t53_Quaternion_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Quaternion_t53  p1, Quaternion_t53  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Quaternion_t53 *)args[0]), *((Quaternion_t53 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t53_Quaternion_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t53  (*Func)(void* obj, Quaternion_t53  p1, const MethodInfo* method);
	Quaternion_t53  ret = ((Func)method->method)(obj, *((Quaternion_t53 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t53_QuaternionU26_t2734 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t53  (*Func)(void* obj, Quaternion_t53 * p1, const MethodInfo* method);
	Quaternion_t53  ret = ((Func)method->method)(obj, (Quaternion_t53 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t35_Quaternion_t53_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t35  (*Func)(void* obj, Quaternion_t53  p1, Vector3_t35  p2, const MethodInfo* method);
	Vector3_t35  ret = ((Func)method->method)(obj, *((Quaternion_t53 *)args[0]), *((Vector3_t35 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Quaternion_t53_Quaternion_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Quaternion_t53  p1, Quaternion_t53  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Quaternion_t53 *)args[0]), *((Quaternion_t53 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t35  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Rect_t184_Rect_t184 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t184  p1, Rect_t184  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t184 *)args[0]), *((Rect_t184 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t339_Matrix4x4_t339 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t339  (*Func)(void* obj, Matrix4x4_t339  p1, const MethodInfo* method);
	Matrix4x4_t339  ret = ((Func)method->method)(obj, *((Matrix4x4_t339 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t339_Matrix4x4U26_t2735 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t339  (*Func)(void* obj, Matrix4x4_t339 * p1, const MethodInfo* method);
	Matrix4x4_t339  ret = ((Func)method->method)(obj, (Matrix4x4_t339 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Matrix4x4_t339_Matrix4x4U26_t2735 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t339  p1, Matrix4x4_t339 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t339 *)args[0]), (Matrix4x4_t339 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Matrix4x4U26_t2735_Matrix4x4U26_t2735 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t339 * p1, Matrix4x4_t339 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Matrix4x4_t339 *)args[0], (Matrix4x4_t339 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t339 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t339  (*Func)(void* obj, const MethodInfo* method);
	Matrix4x4_t339  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t287_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t287  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector4_t287  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Vector4_t287 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector4_t287  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector4_t287 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t339_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t339  (*Func)(void* obj, Vector3_t35  p1, const MethodInfo* method);
	Matrix4x4_t339  ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Vector3_t35_Quaternion_t53_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t35  p1, Quaternion_t53  p2, Vector3_t35  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t35 *)args[0]), *((Quaternion_t53 *)args[1]), *((Vector3_t35 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t339_Vector3_t35_Quaternion_t53_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t339  (*Func)(void* obj, Vector3_t35  p1, Quaternion_t53  p2, Vector3_t35  p3, const MethodInfo* method);
	Matrix4x4_t339  ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), *((Quaternion_t53 *)args[1]), *((Vector3_t35 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t339_Vector3U26_t2730_QuaternionU26_t2734_Vector3U26_t2730 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t339  (*Func)(void* obj, Vector3_t35 * p1, Quaternion_t53 * p2, Vector3_t35 * p3, const MethodInfo* method);
	Matrix4x4_t339  ret = ((Func)method->method)(obj, (Vector3_t35 *)args[0], (Quaternion_t53 *)args[1], (Vector3_t35 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t339_Single_t61_Single_t61_Single_t61_Single_t61_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t339  (*Func)(void* obj, float p1, float p2, float p3, float p4, float p5, float p6, const MethodInfo* method);
	Matrix4x4_t339  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t339_Single_t61_Single_t61_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t339  (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	Matrix4x4_t339  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t339_Matrix4x4_t339_Matrix4x4_t339 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t339  (*Func)(void* obj, Matrix4x4_t339  p1, Matrix4x4_t339  p2, const MethodInfo* method);
	Matrix4x4_t339  ret = ((Func)method->method)(obj, *((Matrix4x4_t339 *)args[0]), *((Matrix4x4_t339 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t287_Matrix4x4_t339_Vector4_t287 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t287  (*Func)(void* obj, Matrix4x4_t339  p1, Vector4_t287  p2, const MethodInfo* method);
	Vector4_t287  ret = ((Func)method->method)(obj, *((Matrix4x4_t339 *)args[0]), *((Vector4_t287 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Matrix4x4_t339_Matrix4x4_t339 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t339  p1, Matrix4x4_t339  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t339 *)args[0]), *((Matrix4x4_t339 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Vector3_t35_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t35  p1, Vector3_t35  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t35 *)args[0]), *((Vector3_t35 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Bounds_t207 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Bounds_t207  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Bounds_t207 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_Bounds_t207 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t207  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t207 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Bounds_t207_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t207  p1, Vector3_t35  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t207 *)args[0]), *((Vector3_t35 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_BoundsU26_t2736_Vector3U26_t2730 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t207 * p1, Vector3_t35 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Bounds_t207 *)args[0], (Vector3_t35 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Bounds_t207_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t207  p1, Vector3_t35  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Bounds_t207 *)args[0]), *((Vector3_t35 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_BoundsU26_t2736_Vector3U26_t2730 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t207 * p1, Vector3_t35 * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Bounds_t207 *)args[0], (Vector3_t35 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_RayU26_t2737_BoundsU26_t2736_SingleU26_t2738 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t306 * p1, Bounds_t207 * p2, float* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Ray_t306 *)args[0], (Bounds_t207 *)args[1], (float*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Ray_t306 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t306  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t306 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Ray_t306_SingleU26_t2738 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t306  p1, float* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t306 *)args[0]), (float*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t35_BoundsU26_t2736_Vector3U26_t2730 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t35  (*Func)(void* obj, Bounds_t207 * p1, Vector3_t35 * p2, const MethodInfo* method);
	Vector3_t35  ret = ((Func)method->method)(obj, (Bounds_t207 *)args[0], (Vector3_t35 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Bounds_t207_Bounds_t207 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t207  p1, Bounds_t207  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t207 *)args[0]), *((Bounds_t207 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Vector4_t287_Vector4_t287 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t287  p1, Vector4_t287  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t287 *)args[0]), *((Vector4_t287 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Vector4_t287 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t287  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t287 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t287 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t287  (*Func)(void* obj, const MethodInfo* method);
	Vector4_t287  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t287_Vector4_t287_Vector4_t287 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t287  (*Func)(void* obj, Vector4_t287  p1, Vector4_t287  p2, const MethodInfo* method);
	Vector4_t287  ret = ((Func)method->method)(obj, *((Vector4_t287 *)args[0]), *((Vector4_t287 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t287_Vector4_t287_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t287  (*Func)(void* obj, Vector4_t287  p1, float p2, const MethodInfo* method);
	Vector4_t287  ret = ((Func)method->method)(obj, *((Vector4_t287 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Vector4_t287_Vector4_t287 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector4_t287  p1, Vector4_t287  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector4_t287 *)args[0]), *((Vector4_t287 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t35_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t35  (*Func)(void* obj, float p1, const MethodInfo* method);
	Vector3_t35  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Single_t61_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Single_t61_Single_t61_SingleU26_t2738_Single_t61_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float* p3, float p4, float p5, float p6, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_RectU26_t2731 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t184 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t184 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_SphericalHarmonicsL2U26_t2739 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SphericalHarmonicsL2_t431 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (SphericalHarmonicsL2_t431 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Color_t128_SphericalHarmonicsL2U26_t2739 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t128  p1, SphericalHarmonicsL2_t431 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t128 *)args[0]), (SphericalHarmonicsL2_t431 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_ColorU26_t2727_SphericalHarmonicsL2U26_t2739 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t128 * p1, SphericalHarmonicsL2_t431 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t128 *)args[0], (SphericalHarmonicsL2_t431 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Vector3_t35_Color_t128_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t35  p1, Color_t128  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t35 *)args[0]), *((Color_t128 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Vector3_t35_Color_t128_SphericalHarmonicsL2U26_t2739 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t35  p1, Color_t128  p2, SphericalHarmonicsL2_t431 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t35 *)args[0]), *((Color_t128 *)args[1]), (SphericalHarmonicsL2_t431 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Vector3U26_t2730_ColorU26_t2727_SphericalHarmonicsL2U26_t2739 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t35 * p1, Color_t128 * p2, SphericalHarmonicsL2_t431 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t35 *)args[0], (Color_t128 *)args[1], (SphericalHarmonicsL2_t431 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_SphericalHarmonicsL2_t431_SphericalHarmonicsL2_t431_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t431  (*Func)(void* obj, SphericalHarmonicsL2_t431  p1, float p2, const MethodInfo* method);
	SphericalHarmonicsL2_t431  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t431 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SphericalHarmonicsL2_t431_Single_t61_SphericalHarmonicsL2_t431 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t431  (*Func)(void* obj, float p1, SphericalHarmonicsL2_t431  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t431  ret = ((Func)method->method)(obj, *((float*)args[0]), *((SphericalHarmonicsL2_t431 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SphericalHarmonicsL2_t431_SphericalHarmonicsL2_t431_SphericalHarmonicsL2_t431 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t431  (*Func)(void* obj, SphericalHarmonicsL2_t431  p1, SphericalHarmonicsL2_t431  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t431  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t431 *)args[0]), *((SphericalHarmonicsL2_t431 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_SphericalHarmonicsL2_t431_SphericalHarmonicsL2_t431 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SphericalHarmonicsL2_t431  p1, SphericalHarmonicsL2_t431  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t431 *)args[0]), *((SphericalHarmonicsL2_t431 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Vector4U26_t2740 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4_t287 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector4_t287 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Vector4_t287_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t287  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector4_t287  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Vector2U26_t2732 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t59 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t59 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_SByte_t666_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t666_SByte_t666_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t298_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimePlatform_t362 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Int32_t56_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t56_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_CameraClearFlags_t550 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t35_Object_t_Vector3U26_t2730 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t35  (*Func)(void* obj, Object_t * p1, Vector3_t35 * p2, const MethodInfo* method);
	Vector3_t35  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t35 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t306_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t306  (*Func)(void* obj, Vector3_t35  p1, const MethodInfo* method);
	Ray_t306  ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t306_Object_t_Vector3U26_t2730 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t306  (*Func)(void* obj, Object_t * p1, Vector3_t35 * p2, const MethodInfo* method);
	Ray_t306  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t35 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Ray_t306_Single_t61_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t306  p1, float p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t306 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_RayU26_t2737_Single_t61_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Ray_t306 * p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t306 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_RenderBuffer_t549 (const MethodInfo* method, void* obj, void** args)
{
	typedef RenderBuffer_t549  (*Func)(void* obj, const MethodInfo* method);
	RenderBuffer_t549  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_IntPtr_t_Int32U26_t2726_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_IntPtr_t_RenderBufferU26_t2741_RenderBufferU26_t2741 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, RenderBuffer_t549 * p2, RenderBuffer_t549 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (RenderBuffer_t549 *)args[1], (RenderBuffer_t549 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_IntPtr_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_IntPtr_t_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_IntPtr_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Int32_t56_Int32_t56_Int32U26_t2726_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t* p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_TouchPhase_t446 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Vector3U26_t2730 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t35 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t35 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Touch_t282_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Touch_t282  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Touch_t282  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Vector3_t35_Quaternion_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t35  p2, Quaternion_t53  p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t35 *)args[1]), *((Quaternion_t53 *)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Vector3U26_t2730_QuaternionU26_t2734 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t35 * p2, Quaternion_t53 * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t35 *)args[1], (Quaternion_t53 *)args[2], method);
	return ret;
}

void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t666_SByte_t666_SByte_t666_SByte_t666_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_QuaternionU26_t2734 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t53 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Quaternion_t53 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Quaternion_t53 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t53  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Quaternion_t53 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Matrix4x4U26_t2735 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t339 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Matrix4x4_t339 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Vector3_t35_Vector3_t35_RaycastHitU26_t2742_Single_t61_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t35  p1, Vector3_t35  p2, RaycastHit_t283 * p3, float p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), *((Vector3_t35 *)args[1]), (RaycastHit_t283 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Vector3U26_t2730_Vector3U26_t2730_RaycastHitU26_t2742_Single_t61_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t35 * p1, Vector3_t35 * p2, RaycastHit_t283 * p3, float p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Vector3_t35 *)args[0], (Vector3_t35 *)args[1], (RaycastHit_t283 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Ray_t306_RaycastHitU26_t2742_Single_t61_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t306  p1, RaycastHit_t283 * p2, float p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t306 *)args[0]), (RaycastHit_t283 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector3_t35_Vector3_t35_Single_t61_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t35  p1, Vector3_t35  p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), *((Vector3_t35 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Vector3U26_t2730_Vector3U26_t2730_Single_t61_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t35 * p1, Vector3_t35 * p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector3_t35 *)args[0], (Vector3_t35 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Vector2_t59_Vector2_t59_Int32_t56_Single_t61_Single_t61_RaycastHit2DU26_t2723 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t59  p1, Vector2_t59  p2, int32_t p3, float p4, float p5, RaycastHit2D_t52 * p6, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t59 *)args[0]), *((Vector2_t59 *)args[1]), *((int32_t*)args[2]), *((float*)args[3]), *((float*)args[4]), (RaycastHit2D_t52 *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Vector2U26_t2732_Vector2U26_t2732_Int32_t56_Single_t61_Single_t61_RaycastHit2DU26_t2723 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t59 * p1, Vector2_t59 * p2, int32_t p3, float p4, float p5, RaycastHit2D_t52 * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t59 *)args[0], (Vector2_t59 *)args[1], *((int32_t*)args[2]), *((float*)args[3]), *((float*)args[4]), (RaycastHit2D_t52 *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_RaycastHit2D_t52_Vector2_t59_Vector2_t59_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t52  (*Func)(void* obj, Vector2_t59  p1, Vector2_t59  p2, int32_t p3, const MethodInfo* method);
	RaycastHit2D_t52  ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), *((Vector2_t59 *)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastHit2D_t52_Vector2_t59_Vector2_t59_Int32_t56_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t52  (*Func)(void* obj, Vector2_t59  p1, Vector2_t59  p2, int32_t p3, float p4, float p5, const MethodInfo* method);
	RaycastHit2D_t52  ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), *((Vector2_t59 *)args[1]), *((int32_t*)args[2]), *((float*)args[3]), *((float*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Vector2_t59_Vector2_t59_Single_t61_Int32_t56_Single_t61_Single_t61_RaycastHit2DU26_t2723 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t59  p1, Vector2_t59  p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t52 * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t59 *)args[0]), *((Vector2_t59 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t52 *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Vector2U26_t2732_Vector2U26_t2732_Single_t61_Int32_t56_Single_t61_Single_t61_RaycastHit2DU26_t2723 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t59 * p1, Vector2_t59 * p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t52 * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t59 *)args[0], (Vector2_t59 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t52 *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_RaycastHit2D_t52_Vector2_t59_Vector2_t59_Single_t61_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t52  (*Func)(void* obj, Vector2_t59  p1, Vector2_t59  p2, float p3, int32_t p4, const MethodInfo* method);
	RaycastHit2D_t52  ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), *((Vector2_t59 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastHit2D_t52_Vector2_t59_Vector2_t59_Single_t61_Int32_t56_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t52  (*Func)(void* obj, Vector2_t59  p1, Vector2_t59  p2, float p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	RaycastHit2D_t52  ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), *((Vector2_t59 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector2_t59_Vector2_t59_Single_t61_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t59  p1, Vector2_t59  p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), *((Vector2_t59 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Vector2U26_t2732_Vector2U26_t2732_Single_t61_Int32_t56_Single_t61_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t59 * p1, Vector2_t59 * p2, float p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector2_t59 *)args[0], (Vector2_t59 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_SByte_t666_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_SendMessageOptions_t361 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AnimatorStateInfo_t467 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorStateInfo_t467  (*Func)(void* obj, const MethodInfo* method);
	AnimatorStateInfo_t467  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AnimatorClipInfo_t468 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorClipInfo_t468  (*Func)(void* obj, const MethodInfo* method);
	AnimatorClipInfo_t468  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Object_t_Color_t128_Int32_t56_Single_t61_Single_t61_Int32_t56_SByte_t666_SByte_t666_Int32_t56_Int32_t56_Int32_t56_Int32_t56_SByte_t666_Int32_t56_Vector2_t59_Vector2_t59_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t128  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, Vector2_t59  p16, Vector2_t59  p17, int8_t p18, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t128 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((Vector2_t59 *)args[15]), *((Vector2_t59 *)args[16]), *((int8_t*)args[17]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Object_t_Color_t128_Int32_t56_Single_t61_Single_t61_Int32_t56_SByte_t666_SByte_t666_Int32_t56_Int32_t56_Int32_t56_Int32_t56_SByte_t666_Int32_t56_Single_t61_Single_t61_Single_t61_Single_t61_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t128  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, float p16, float p17, float p18, float p19, int8_t p20, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t128 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((float*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((int8_t*)args[19]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Object_t_Object_t_ColorU26_t2727_Int32_t56_Single_t61_Single_t61_Int32_t56_SByte_t666_SByte_t666_Int32_t56_Int32_t56_Int32_t56_Int32_t56_SByte_t666_Int32_t56_Single_t61_Single_t61_Single_t61_Single_t61_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Color_t128 * p4, int32_t p5, float p6, float p7, int32_t p8, int8_t p9, int8_t p10, int32_t p11, int32_t p12, int32_t p13, int32_t p14, int8_t p15, int32_t p16, float p17, float p18, float p19, float p20, int8_t p21, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Color_t128 *)args[3], *((int32_t*)args[4]), *((float*)args[5]), *((float*)args[6]), *((int32_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int32_t*)args[13]), *((int8_t*)args[14]), *((int32_t*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((float*)args[19]), *((int8_t*)args[20]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextGenerationSettings_t290_TextGenerationSettings_t290 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t290  (*Func)(void* obj, TextGenerationSettings_t290  p1, const MethodInfo* method);
	TextGenerationSettings_t290  ret = ((Func)method->method)(obj, *((TextGenerationSettings_t290 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Object_t_TextGenerationSettings_t290 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t290  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t290 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_TextGenerationSettings_t290 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t290  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t290 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RenderMode_t482 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_ColorU26_t2727 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t128 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Color_t128 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_Object_t_Vector2_t59_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t59  p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t59 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Vector2U26_t2732_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t59 * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t59 *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t59_Vector2_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t59  (*Func)(void* obj, Vector2_t59  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Vector2_t59  ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Vector2_t59_Object_t_Object_t_Vector2U26_t2732 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t59  p1, Object_t * p2, Object_t * p3, Vector2_t59 * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t59 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Vector2_t59 *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Vector2U26_t2732_Object_t_Object_t_Vector2U26_t2732 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t59 * p1, Object_t * p2, Object_t * p3, Vector2_t59 * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t59 *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Vector2_t59 *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Rect_t184_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t184  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Rect_t184  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Vector2_t59_Object_t_Vector3U26_t2730 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t59  p2, Object_t * p3, Vector3_t35 * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t59 *)args[1]), (Object_t *)args[2], (Vector3_t35 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Vector2_t59_Object_t_Vector2U26_t2732 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t59  p2, Object_t * p3, Vector2_t59 * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t59 *)args[1]), (Object_t *)args[2], (Vector2_t59 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t306_Object_t_Vector2_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t306  (*Func)(void* obj, Object_t * p1, Vector2_t59  p2, const MethodInfo* method);
	Ray_t306  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t59 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_SourceID_t501 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AppID_t500 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t652_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t665_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t654 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_NetworkID_t502 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_UInt64_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint64_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_NodeID_t503 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_UInt16_t652 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_UInt64_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56_SByte_t666_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_UInt64_t665_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_UInt64_t665_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_UInt64_t665_UInt16_t652_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, uint16_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), *((uint16_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t56_Int32_t56_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t298_Object_t_ObjectU26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_KeyValuePair_2_t618 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t618  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t618 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_KeyValuePair_2_t618 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t618  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t618 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32U26_t2726_BooleanU26_t2725 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t* p2, bool* p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (bool*)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_SByte_t666_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_UserState_t565 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Double_t661_SByte_t666_SByte_t666_DateTime_t396 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int8_t p3, int8_t p4, DateTime_t396  p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((DateTime_t396 *)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, double p1, const MethodInfo* method);
	((Func)method->method)(obj, *((double*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_DateTime_t396 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t396  (*Func)(void* obj, const MethodInfo* method);
	DateTime_t396  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t666_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int64_t662_Object_t_DateTime_t396_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, DateTime_t396  p4, Object_t * p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((DateTime_t396 *)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_UserScope_t566 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Range_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef Range_t559  (*Func)(void* obj, const MethodInfo* method);
	Range_t559  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Range_t559 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Range_t559  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Range_t559 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_TimeScope_t567 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_HitInfo_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, HitInfo_t561  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((HitInfo_t561 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_HitInfo_t561_HitInfo_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t561  p1, HitInfo_t561  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t561 *)args[0]), *((HitInfo_t561 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_HitInfo_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t561  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t561 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_StringU26_t2744_StringU26_t2744 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_StreamingContext_t632 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, StreamingContext_t632  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t632 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_AnimatorStateInfo_t467_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, AnimatorStateInfo_t467  p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((AnimatorStateInfo_t467 *)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_Color_t128_Color_t128 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t128  p1, Color_t128  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color_t128 *)args[0]), *((Color_t128 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_TextGenerationSettings_t290 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TextGenerationSettings_t290  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TextGenerationSettings_t290 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PersistentListenerMode_t581 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_SByte_t666_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_SByte_t666_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_UInt32_t654_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_UInt32_t654_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Sign_t732_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, int32_t p9, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), *((int32_t*)args[8]), method);
	return NULL;
}

void* RuntimeInvoker_ConfidenceFactor_t736 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_SByte_t666_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Byte_t647 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32U26_t2726_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32U26_t2726_ByteU26_t2745_Int32U26_t2726_ByteU5BU5DU26_t2746 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, uint8_t* p3, int32_t* p4, ByteU5BU5D_t435** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint8_t*)args[2], (int32_t*)args[3], (ByteU5BU5D_t435**)args[4], method);
	return NULL;
}

void* RuntimeInvoker_DateTime_t396_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t396  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DateTime_t396  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_Object_t_Object_t_Object_t_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_DSAParameters_t871 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DSAParameters_t871  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((DSAParameters_t871 *)args[1]), method);
	return ret;
}

void* RuntimeInvoker_RSAParameters_t842_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t842  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	RSAParameters_t842  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_RSAParameters_t842 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RSAParameters_t842  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RSAParameters_t842 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_DSAParameters_t871_BooleanU26_t2725 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t871  (*Func)(void* obj, bool* p1, const MethodInfo* method);
	DSAParameters_t871  ret = ((Func)method->method)(obj, (bool*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t666_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_DateTime_t396 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t396  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t396 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t773 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Byte_t647 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Byte_t647_Byte_t647 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_AlertLevel_t792 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AlertDescription_t793 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Byte_t647 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Int16_t667_Object_t_Int32_t56_Int32_t56_Int32_t56_SByte_t666_SByte_t666_SByte_t666_SByte_t666_Int16_t667_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return NULL;
}

void* RuntimeInvoker_CipherAlgorithmType_t795 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HashAlgorithmType_t814 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ExchangeAlgorithmType_t812 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CipherMode_t724 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_ByteU5BU5DU26_t2746_ByteU5BU5DU26_t2746 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ByteU5BU5D_t435** p2, ByteU5BU5D_t435** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ByteU5BU5D_t435**)args[1], (ByteU5BU5D_t435**)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t647_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t56_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t667_Object_t_Int32_t56_Int32_t56_Int32_t56_SByte_t666_SByte_t666_SByte_t666_SByte_t666_Int16_t667_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_SecurityProtocolType_t829 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SecurityCompressionType_t828 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HandshakeType_t845 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HandshakeState_t813 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SecurityProtocolType_t829_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t647_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Byte_t647_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Byte_t647_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t647_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_SByte_t666_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56_Int32_t56_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t662_Int64_t662_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_Int32_t56_Int32_t56_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Byte_t647_Byte_t647_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_RSAParameters_t842 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t842  (*Func)(void* obj, const MethodInfo* method);
	RSAParameters_t842  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Byte_t647 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Byte_t647_Byte_t647 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, uint8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), *((uint8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Byte_t647_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_ContentType_t807 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t2747 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DictionaryNode_t917 ** p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (DictionaryNode_t917 **)args[1], method);
	return ret;
}

void* RuntimeInvoker_DictionaryEntry_t1068 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1068  (*Func)(void* obj, const MethodInfo* method);
	DictionaryEntry_t1068  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_EditorBrowsableState_t929 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Object_t_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t667_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_IPAddressU26_t2748 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPAddress_t946 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPAddress_t946 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AddressFamily_t934 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_IPv6AddressU26_t2749 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPv6Address_t948 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPv6Address_t948 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t652_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SecurityProtocolType_t949 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_SByte_t666_SByte_t666_Int32_t56_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_AsnDecodeStatus_t990_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t56_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_X509ChainStatusFlags_t978_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t978_Object_t_Int32_t56_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t978_Object_t_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t978 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32U26_t2726_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_X509RevocationFlag_t985 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509RevocationMode_t986 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509VerificationFlags_t989 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509KeyUsageFlags_t983 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509KeyUsageFlags_t983_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Byte_t647_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t647_Int16_t667_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t56_Int32_t56_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_RegexOptions_t1004 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Category_t1011_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_UInt16_t652_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int32_t56_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int16_t667_SByte_t666_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_UInt16_t652_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int16_t667_Int16_t667_SByte_t666_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int16_t667_Object_t_SByte_t666_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_SByte_t666_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_SByte_t666_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_UInt16_t652_UInt16_t652_UInt16_t652 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_OpFlags_t1006_SByte_t666_SByte_t666_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_UInt16_t652_UInt16_t652 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_Int32_t56_Int32U26_t2726_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int32_t56_Int32U26_t2726_Int32U26_t2726_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int32U26_t2726_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_UInt16_t652_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int32_t56_Int32_t56_SByte_t666_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Int32U26_t2726_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_SByte_t666_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Interval_t1026 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t1026  (*Func)(void* obj, const MethodInfo* method);
	Interval_t1026  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Interval_t1026 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Interval_t1026  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Interval_t1026 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Interval_t1026 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Interval_t1026  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Interval_t1026 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Interval_t1026_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t1026  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Interval_t1026  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Double_t661_Interval_t1026 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Interval_t1026  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Interval_t1026 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Interval_t1026_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Interval_t1026  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Interval_t1026 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Double_t661_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32U26_t2726_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32U26_t2726_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_RegexOptionsU26_t2750 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_RegexOptionsU26_t2750_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_Int32U26_t2726_Int32U26_t2726_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Category_t1011 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Int16_t667_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t326_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32U26_t2726_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32U26_t2726_Int32U26_t2726_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_UInt16_t652_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int16_t667_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_UInt16_t652 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((uint16_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Position_t1007 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UriHostNameType_t1059_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_StringU26_t2744 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	((Func)method->method)(obj, (String_t**)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t666_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Char_t326_Object_t_Int32U26_t2726_CharU26_t2751 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t* p2, uint16_t* p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint16_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_UriFormatExceptionU26_t2752 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, UriFormatException_t1058 ** p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (UriFormatException_t1058 **)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t56_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t298_Object_t_Object_t_ObjectU5BU5DU26_t2753 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, ObjectU5BU5D_t60** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ObjectU5BU5D_t60**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_ObjectU5BU5DU26_t2753 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t60** p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t60**)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t298_Object_t_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t647_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t664_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t664  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Decimal_t664  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t667_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t662_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t666_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t652_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t654_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t665_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_SByte_t666_Object_t_Int32_t56_ExceptionU26_t2754 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int32_t p3, Exception_t301 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Exception_t301 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_SByte_t666_Int32U26_t2726_ExceptionU26_t2754 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int32_t* p3, Exception_t301 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int32_t*)args[2], (Exception_t301 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int32_t56_SByte_t666_ExceptionU26_t2754 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, Exception_t301 ** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (Exception_t301 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int32U26_t2726_Object_t_SByte_t666_SByte_t666_ExceptionU26_t2754 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int8_t p3, int8_t p4, Exception_t301 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), (Exception_t301 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32U26_t2726_Object_t_Object_t_BooleanU26_t2725_BooleanU26_t2725 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, bool* p5, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], (bool*)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32U26_t2726_Object_t_Object_t_BooleanU26_t2725 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_Int32U26_t2726_Object_t_Int32U26_t2726_SByte_t666_ExceptionU26_t2754 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int32_t* p3, int8_t p4, Exception_t301 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (int32_t*)args[2], *((int8_t*)args[3]), (Exception_t301 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int32U26_t2726_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int16_t667_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_SByte_t666_Int32U26_t2726_ExceptionU26_t2754 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, Exception_t301 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], (Exception_t301 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeCode_t1692 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_SByte_t666_Int64U26_t2755_ExceptionU26_t2754 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int64_t* p3, Exception_t301 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int64_t*)args[2], (Exception_t301 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t662_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_SByte_t666_Int64U26_t2755_ExceptionU26_t2754 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int64_t* p5, Exception_t301 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int64_t*)args[4], (Exception_t301 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t662_Object_t_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int64U26_t2755 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_Int64U26_t2755 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int64_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int64_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_SByte_t666_UInt32U26_t2756_ExceptionU26_t2754 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, uint32_t* p3, Exception_t301 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (uint32_t*)args[2], (Exception_t301 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_SByte_t666_UInt32U26_t2756_ExceptionU26_t2754 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint32_t* p5, Exception_t301 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint32_t*)args[4], (Exception_t301 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t654_Object_t_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t654_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_UInt32U26_t2756 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_UInt32U26_t2756 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t665_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_SByte_t666_UInt64U26_t2757_ExceptionU26_t2754 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint64_t* p5, Exception_t301 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint64_t*)args[4], (Exception_t301 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t665_Object_t_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_UInt64U26_t2757 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t647_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t647_Object_t_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_ByteU26_t2745 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_ByteU26_t2745 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint8_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint8_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_SByte_t666_SByteU26_t2758_ExceptionU26_t2754 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t* p3, Exception_t301 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int8_t*)args[2], (Exception_t301 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t666_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t666_Object_t_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_SByteU26_t2758 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_SByte_t666_Int16U26_t2759_ExceptionU26_t2754 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int16_t* p3, Exception_t301 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int16_t*)args[2], (Exception_t301 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t667_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t667_Object_t_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int16U26_t2759 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t652_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t652_Object_t_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_UInt16U26_t2760 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_UInt16U26_t2760 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint16_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint16_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_ByteU2AU26_t2761_ByteU2AU26_t2761_DoubleU2AU26_t2762_UInt16U2AU26_t2763_UInt16U2AU26_t2763_UInt16U2AU26_t2763_UInt16U2AU26_t2763 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t** p1, uint8_t** p2, double** p3, uint16_t** p4, uint16_t** p5, uint16_t** p6, uint16_t** p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint8_t**)args[0], (uint8_t**)args[1], (double**)args[2], (uint16_t**)args[3], (uint16_t**)args[4], (uint16_t**)args[5], (uint16_t**)args[6], method);
	return NULL;
}

void* RuntimeInvoker_UnicodeCategory_t1234_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t326_Int16_t667_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int16_t667_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Char_t326_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Object_t_SByte_t666_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Object_t_Int32_t56_Int32_t56_SByte_t666_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Int16_t667_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t56_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t667_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32U26_t2726_Int32U26_t2726_Int32U26_t2726_BooleanU26_t2725_StringU26_t2744 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t* p3, int32_t* p4, bool* p5, String_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (int32_t*)args[2], (int32_t*)args[3], (bool*)args[4], (String_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t667_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, double p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t661_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t661_Object_t_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_SByte_t666_DoubleU26_t2764_ExceptionU26_t2754 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, double* p5, Exception_t301 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (double*)args[4], (Exception_t301 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_DoubleU26_t2764 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, double* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (double*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_DoubleU26_t2764 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, double* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (double*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Int32_t56_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Decimal_t664  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Decimal_t664 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Decimal_t664_Decimal_t664_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t664  (*Func)(void* obj, Decimal_t664  p1, Decimal_t664  p2, const MethodInfo* method);
	Decimal_t664  ret = ((Func)method->method)(obj, *((Decimal_t664 *)args[0]), *((Decimal_t664 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t665_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Decimal_t664  p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((Decimal_t664 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t662_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Decimal_t664  p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((Decimal_t664 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Decimal_t664_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t664  p1, Decimal_t664  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t664 *)args[0]), *((Decimal_t664 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t664_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t664  (*Func)(void* obj, Decimal_t664  p1, const MethodInfo* method);
	Decimal_t664  ret = ((Func)method->method)(obj, *((Decimal_t664 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Decimal_t664_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t664  p1, Decimal_t664  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t664 *)args[0]), *((Decimal_t664 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t664  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t664 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t664  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t664 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t664_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t664  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Decimal_t664  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56_Object_t_Int32U26_t2726_BooleanU26_t2725_BooleanU26_t2725_Int32U26_t2726_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, bool* p5, bool* p6, int32_t* p7, int8_t p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], (bool*)args[4], (bool*)args[5], (int32_t*)args[6], *((int8_t*)args[7]), method);
	return ret;
}

void* RuntimeInvoker_Decimal_t664_Object_t_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t664  (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Decimal_t664  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_DecimalU26_t2765_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Decimal_t664 * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Decimal_t664 *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_DecimalU26_t2765_UInt64U26_t2757 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t664 * p1, uint64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t664 *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_DecimalU26_t2765_Int64U26_t2755 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t664 * p1, int64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t664 *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_DecimalU26_t2765_DecimalU26_t2765 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t664 * p1, Decimal_t664 * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t664 *)args[0], (Decimal_t664 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_DecimalU26_t2765_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t664 * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t664 *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_DecimalU26_t2765_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t664 * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t664 *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t661_DecimalU26_t2765 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t664 * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Decimal_t664 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_DecimalU26_t2765_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t664 * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Decimal_t664 *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_DecimalU26_t2765_DecimalU26_t2765_DecimalU26_t2765 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t664 * p1, Decimal_t664 * p2, Decimal_t664 * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t664 *)args[0], (Decimal_t664 *)args[1], (Decimal_t664 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t647_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Decimal_t664  p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((Decimal_t664 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t666_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Decimal_t664  p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((Decimal_t664 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t667_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Decimal_t664  p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((Decimal_t664 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t652_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Decimal_t664  p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((Decimal_t664 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t654_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Decimal_t664  p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((Decimal_t664 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t664_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t664  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Decimal_t664  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t664_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t664  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Decimal_t664  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t664_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t664  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Decimal_t664  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t664_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t664  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Decimal_t664  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t664_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t664  (*Func)(void* obj, float p1, const MethodInfo* method);
	Decimal_t664  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t664_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t664  (*Func)(void* obj, double p1, const MethodInfo* method);
	Decimal_t664  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Decimal_t664  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Decimal_t664 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t661_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t664  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Decimal_t664 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_UInt64_t665_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t654_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t2766 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, MulticastDelegate_t179 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (MulticastDelegate_t179 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t56_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t56_Object_t_Object_t_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t665_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Int64_t662_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int64_t662_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int64_t662_Int64_t662_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int64_t662_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int64_t662_Int64_t662_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, int64_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), *((int64_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Int64_t662_Object_t_Int64_t662_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, int64_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), *((int64_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Object_t_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Int32_t56_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_TypeAttributes_t1353 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MemberTypes_t1332 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeTypeHandle_t1090 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1090  (*Func)(void* obj, const MethodInfo* method);
	RuntimeTypeHandle_t1090  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_TypeCode_t1692_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_RuntimeTypeHandle_t1090 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeTypeHandle_t1090  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeTypeHandle_t1090 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_RuntimeTypeHandle_t1090_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1090  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RuntimeTypeHandle_t1090  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56_Object_t_Int32_t56_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t56_Object_t_Int32_t56_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t56_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_SByte_t666_SByte_t666_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_RuntimeFieldHandle_t1092 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, RuntimeFieldHandle_t1092  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((RuntimeFieldHandle_t1092 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_IntPtr_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Int32_t56_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_ContractionU5BU5DU26_t2767_Level2MapU5BU5DU26_t2768 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, ContractionU5BU5D_t1138** p3, Level2MapU5BU5D_t1139** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ContractionU5BU5D_t1138**)args[2], (Level2MapU5BU5D_t1139**)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_CodePointIndexerU26_t2769_ByteU2AU26_t2761_ByteU2AU26_t2761_CodePointIndexerU26_t2769_ByteU2AU26_t2761 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, CodePointIndexer_t1122 ** p2, uint8_t** p3, uint8_t** p4, CodePointIndexer_t1122 ** p5, uint8_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (CodePointIndexer_t1122 **)args[1], (uint8_t**)args[2], (uint8_t**)args[3], (CodePointIndexer_t1122 **)args[4], (uint8_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Byte_t647_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int32_t56_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t647_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ExtenderType_t1135_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_Int32_t56_BooleanU26_t2725_BooleanU26_t2725_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_Int32_t56_BooleanU26_t2725_BooleanU26_t2725_SByte_t666_SByte_t666_ContextU26_t2770 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, int8_t p10, Context_t1132 * p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), *((int8_t*)args[9]), (Context_t1132 *)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Object_t_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Object_t_Int32_t56_Int32_t56_SByte_t666_ContextU26_t2770 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int8_t p5, Context_t1132 * p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), (Context_t1132 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Object_t_Int32_t56_Int32_t56_BooleanU26_t2725 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, bool* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (bool*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Object_t_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int16_t667_Int32_t56_SByte_t666_ContextU26_t2770 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int16_t p5, int32_t p6, int8_t p7, Context_t1132 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int16_t*)args[4]), *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1132 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Object_t_Int32_t56_Int32_t56_Object_t_ContextU26_t2770 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, Context_t1132 * p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (Context_t1132 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Int32_t56_Object_t_Int32_t56_SByte_t666_ContextU26_t2770 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, Context_t1132 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1132 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32U26_t2726_Int32_t56_Int32_t56_Object_t_SByte_t666_ContextU26_t2770 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, Context_t1132 * p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), (Context_t1132 *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32U26_t2726_Int32_t56_Int32_t56_Object_t_SByte_t666_Int32_t56_ContractionU26_t2771_ContextU26_t2770 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, int32_t p7, Contraction_t1125 ** p8, Context_t1132 * p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), (Contraction_t1125 **)args[7], (Context_t1132 *)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32U26_t2726_Int32_t56_Int32_t56_Int32_t56_Object_t_SByte_t666_ContextU26_t2770 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, Context_t1132 * p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), (Context_t1132 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32U26_t2726_Int32_t56_Int32_t56_Int32_t56_Object_t_SByte_t666_Int32_t56_ContractionU26_t2771_ContextU26_t2770 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, int32_t p8, Contraction_t1125 ** p9, Context_t1132 * p10, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), *((int32_t*)args[7]), (Contraction_t1125 **)args[8], (Context_t1132 *)args[9], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Object_t_Object_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_SByte_t666_SByte_t666_SByte_t666_SByte_t666_SByte_t666_SByte_t666_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_SByte_t666_ByteU5BU5DU26_t2746_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, ByteU5BU5D_t435** p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (ByteU5BU5D_t435**)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ConfidenceFactor_t1144 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Sign_t1146_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DSAParameters_t871_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t871  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DSAParameters_t871  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_DSAParameters_t871 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DSAParameters_t871  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DSAParameters_t871 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int16_t667_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t661_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t667_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Single_t61_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Single_t61_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Single_t61_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_Int32_t56_SByte_t666_MethodBaseU26_t2772_Int32U26_t2726_Int32U26_t2726_StringU26_t2744_Int32U26_t2726_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, MethodBase_t696 ** p3, int32_t* p4, int32_t* p5, String_t** p6, int32_t* p7, int32_t* p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (MethodBase_t696 **)args[2], (int32_t*)args[3], (int32_t*)args[4], (String_t**)args[5], (int32_t*)args[6], (int32_t*)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t56_DateTime_t396 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t396  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t396 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DayOfWeek_t1642_DateTime_t396 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t396  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t396 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Int32U26_t2726_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DayOfWeek_t1642_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32U26_t2726_Int32U26_t2726_Int32U26_t2726_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t* p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], (int32_t*)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_DateTime_t396_DateTime_t396_TimeSpan_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t396  p1, DateTime_t396  p2, TimeSpan_t977  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t396 *)args[0]), *((DateTime_t396 *)args[1]), *((TimeSpan_t977 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t977  (*Func)(void* obj, const MethodInfo* method);
	TimeSpan_t977  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t664  (*Func)(void* obj, const MethodInfo* method);
	Decimal_t664  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t652 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_IntPtr_t_Int32_t56_SByte_t666_Int32_t56_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56_SByte_t666_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_IntPtr_t_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_Object_t_MonoIOErrorU26_t2773 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t56_Int32_t56_MonoIOErrorU26_t2773 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_MonoIOErrorU26_t2773 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_FileAttributes_t1244_Object_t_MonoIOErrorU26_t2773 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MonoFileType_t1253_IntPtr_t_MonoIOErrorU26_t2773 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_MonoIOStatU26_t2774_MonoIOErrorU26_t2773 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, MonoIOStat_t1252 * p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (MonoIOStat_t1252 *)args[1], (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Object_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56_MonoIOErrorU26_t2773 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_IntPtr_t_MonoIOErrorU26_t2773 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_IntPtr_t_Object_t_Int32_t56_Int32_t56_MonoIOErrorU26_t2773 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t662_IntPtr_t_Int64_t662_Int32_t56_MonoIOErrorU26_t2773 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t662_IntPtr_t_MonoIOErrorU26_t2773 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_IntPtr_t_Int64_t662_MonoIOErrorU26_t2773 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_CallingConventions_t1327 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeMethodHandle_t1684 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeMethodHandle_t1684  (*Func)(void* obj, const MethodInfo* method);
	RuntimeMethodHandle_t1684  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodAttributes_t1333 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodToken_t1292 (const MethodInfo* method, void* obj, void** args)
{
	typedef MethodToken_t1292  (*Func)(void* obj, const MethodInfo* method);
	MethodToken_t1292  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FieldAttributes_t1330 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeFieldHandle_t1092 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeFieldHandle_t1092  (*Func)(void* obj, const MethodInfo* method);
	RuntimeFieldHandle_t1092  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Int32_t56_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_OpCode_t1296 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1296  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1296 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_OpCode_t1296_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1296  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1296 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_StackBehaviour_t1301 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PropertyAttributes_t1349 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Int32_t56_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t56_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t56_Int32_t56_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56_SByte_t666_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t2726_ModuleU26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t* p2, Module_t1289 ** p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (Module_t1289 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_AssemblyNameFlags_t1321 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t56_Object_t_ObjectU5BU5DU26_t2753_Object_t_Object_t_Object_t_ObjectU26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, ObjectU5BU5D_t60** p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t ** p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (ObjectU5BU5D_t60**)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t **)args[6], method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_ObjectU5BU5DU26_t2753_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t60** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t60**)args[0], (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t56_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_ObjectU5BU5DU26_t2753_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t60** p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t60**)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t56_Object_t_Object_t_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_EventAttributes_t1328 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_RuntimeFieldHandle_t1092 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeFieldHandle_t1092  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeFieldHandle_t1092 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_Object_t_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_StreamingContext_t632 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t632  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t632 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_RuntimeMethodHandle_t1684 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeMethodHandle_t1684  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeMethodHandle_t1684 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_MonoEventInfoU26_t2776 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEventInfo_t1337 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEventInfo_t1337 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_MonoEventInfo_t1337_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoEventInfo_t1337  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	MonoEventInfo_t1337  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_IntPtr_t_MonoMethodInfoU26_t2777 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, MonoMethodInfo_t1341 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (MonoMethodInfo_t1341 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_MonoMethodInfo_t1341_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoMethodInfo_t1341  (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	MonoMethodInfo_t1341  ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodAttributes_t1333_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CallingConventions_t1327_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t2754 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Exception_t301 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Exception_t301 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_MonoPropertyInfoU26_t2778_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoPropertyInfo_t1342 * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoPropertyInfo_t1342 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_ParameterAttributes_t1345 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_GCHandle_t1374_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef GCHandle_t1374  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	GCHandle_t1374  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_IntPtr_t_Int32_t56_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_IntPtr_t_Object_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_BooleanU26_t2725 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, const MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t2744 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, String_t** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (String_t**)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t2744 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, String_t** p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (String_t**)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_SByte_t666_Object_t_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_TimeSpan_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TimeSpan_t977  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TimeSpan_t977 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_SByte_t666_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t632_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t632  p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t632 *)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t632_ISurrogateSelectorU26_t2779 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, StreamingContext_t632  p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t632 *)args[1]), (Object_t **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t977_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t977  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	TimeSpan_t977  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_StringU26_t2744 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (String_t**)args[0], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t298_Object_t_StringU26_t2744_StringU26_t2744 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WellKnownObjectMode_t1480 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_StreamingContext_t632 (const MethodInfo* method, void* obj, void** args)
{
	typedef StreamingContext_t632  (*Func)(void* obj, const MethodInfo* method);
	StreamingContext_t632  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeFilterLevel_t1496 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_BooleanU26_t2725 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t647_Object_t_SByte_t666_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Byte_t647_Object_t_SByte_t666_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_SByte_t666_ObjectU26_t2743_HeaderU5BU5DU26_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t ** p3, HeaderU5BU5D_t1700** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t **)args[2], (HeaderU5BU5D_t1700**)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Byte_t647_Object_t_SByte_t666_ObjectU26_t2743_HeaderU5BU5DU26_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t ** p4, HeaderU5BU5D_t1700** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t **)args[3], (HeaderU5BU5D_t1700**)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_Byte_t647_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Byte_t647_Object_t_Int64U26_t2755_ObjectU26_t2743_SerializationInfoU26_t2781 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int64_t* p3, Object_t ** p4, SerializationInfo_t631 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], (SerializationInfo_t631 **)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_SByte_t666_SByte_t666_Int64U26_t2755_ObjectU26_t2743_SerializationInfoU26_t2781 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int64_t* p4, Object_t ** p5, SerializationInfo_t631 ** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (int64_t*)args[3], (Object_t **)args[4], (SerializationInfo_t631 **)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int64U26_t2755_ObjectU26_t2743_SerializationInfoU26_t2781 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, SerializationInfo_t631 ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], (SerializationInfo_t631 **)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Int64_t662_ObjectU26_t2743_SerializationInfoU26_t2781 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t ** p4, SerializationInfo_t631 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t **)args[3], (SerializationInfo_t631 **)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int64_t662_Object_t_Object_t_Int64_t662_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int64U26_t2755_ObjectU26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Int64U26_t2755_ObjectU26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t* p3, Object_t ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Int64_t662_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int64_t662_Int64_t662_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int64_t662_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Byte_t647 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Int64_t662_Int32_t56_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int64_t662_Object_t_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int64_t662_Object_t_Int64_t662_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_SByte_t666_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Object_t_StreamingContext_t632 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t632  p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t632 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_StreamingContext_t632 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t632  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t632 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_StreamingContext_t632 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StreamingContext_t632  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((StreamingContext_t632 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_StreamingContext_t632_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t632  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t632 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_DateTime_t396 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DateTime_t396  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((DateTime_t396 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_SerializationEntry_t1513 (const MethodInfo* method, void* obj, void** args)
{
	typedef SerializationEntry_t1513  (*Func)(void* obj, const MethodInfo* method);
	SerializationEntry_t1513  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_StreamingContextStates_t1516 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CspProviderFlags_t1520 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t654_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int64_t662_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_UInt32_t654_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_UInt32U26_t2756_Int32_t56_UInt32U26_t2756_Int32_t56_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint32_t* p1, int32_t p2, uint32_t* p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint32_t*)args[0], *((int32_t*)args[1]), (uint32_t*)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int64_t662_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UInt64_t665_Int64_t662_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t665_Int64_t662_Int64_t662_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t665_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PaddingMode_t727 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_StringBuilderU26_t2782_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StringBuilder_t296 ** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (StringBuilder_t296 **)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_EncoderFallbackBufferU26_t2783_CharU5BU5DU26_t2784 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, EncoderFallbackBuffer_t1592 ** p6, CharU5BU5D_t58** p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (EncoderFallbackBuffer_t1592 **)args[5], (CharU5BU5D_t58**)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_DecoderFallbackBufferU26_t2785 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, DecoderFallbackBuffer_t1583 ** p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (DecoderFallbackBuffer_t1583 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int16_t667_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int16_t667_Int16_t667_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int16_t667_Int16_t667_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t56_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_SByte_t666_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_SByte_t666_Int32_t56_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_SByte_t666_Int32U26_t2726_BooleanU26_t2725_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, int32_t* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_CharU26_t2751_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t* p4, int8_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (uint16_t*)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_CharU26_t2751_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, uint16_t* p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (uint16_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_CharU26_t2751_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint16_t* p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint16_t*)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Object_t_Int32_t56_CharU26_t2751_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint16_t* p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint16_t*)args[4], *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Object_t_DecoderFallbackBufferU26_t2785_ByteU5BU5DU26_t2746_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, DecoderFallbackBuffer_t1583 ** p7, ByteU5BU5D_t435** p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (DecoderFallbackBuffer_t1583 **)args[6], (ByteU5BU5D_t435**)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Int32_t56_Object_t_DecoderFallbackBufferU26_t2785_ByteU5BU5DU26_t2746_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, DecoderFallbackBuffer_t1583 ** p6, ByteU5BU5D_t435** p7, int8_t p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (DecoderFallbackBuffer_t1583 **)args[5], (ByteU5BU5D_t435**)args[6], *((int8_t*)args[7]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_DecoderFallbackBufferU26_t2785_ByteU5BU5DU26_t2746_Object_t_Int64_t662_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t1583 ** p2, ByteU5BU5D_t435** p3, Object_t * p4, int64_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t1583 **)args[1], (ByteU5BU5D_t435**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_DecoderFallbackBufferU26_t2785_ByteU5BU5DU26_t2746_Object_t_Int64_t662_Int32_t56_Object_t_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t1583 ** p2, ByteU5BU5D_t435** p3, Object_t * p4, int64_t p5, int32_t p6, Object_t * p7, int32_t* p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t1583 **)args[1], (ByteU5BU5D_t435**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], (int32_t*)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_UInt32U26_t2756_UInt32U26_t2756_Object_t_DecoderFallbackBufferU26_t2785_ByteU5BU5DU26_t2746_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint32_t* p6, uint32_t* p7, Object_t * p8, DecoderFallbackBuffer_t1583 ** p9, ByteU5BU5D_t435** p10, int8_t p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint32_t*)args[5], (uint32_t*)args[6], (Object_t *)args[7], (DecoderFallbackBuffer_t1583 **)args[8], (ByteU5BU5D_t435**)args[9], *((int8_t*)args[10]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Object_t_Int32_t56_UInt32U26_t2756_UInt32U26_t2756_Object_t_DecoderFallbackBufferU26_t2785_ByteU5BU5DU26_t2746_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint32_t* p5, uint32_t* p6, Object_t * p7, DecoderFallbackBuffer_t1583 ** p8, ByteU5BU5D_t435** p9, int8_t p10, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint32_t*)args[4], (uint32_t*)args[5], (Object_t *)args[6], (DecoderFallbackBuffer_t1583 **)args[7], (ByteU5BU5D_t435**)args[8], *((int8_t*)args[9]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_SByte_t666_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_SByte_t666_Object_t_BooleanU26_t2725 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, Object_t * p2, bool* p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (bool*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_SByte_t666_SByte_t666_Object_t_BooleanU26_t2725 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, int8_t p2, Object_t * p3, bool* p4, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], (bool*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_TimeSpan_t977_TimeSpan_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t977  p1, TimeSpan_t977  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t977 *)args[0]), *((TimeSpan_t977 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int64_t662_Int64_t662_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, int64_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_IntPtr_t_Int32_t56_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t662_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Int64_t662_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Byte_t647_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t647_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t647_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t647_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t326_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t326_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t326_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t326_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t396_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t396  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DateTime_t396  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t396_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t396  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	DateTime_t396  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t396_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t396  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DateTime_t396  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t396_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t396  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	DateTime_t396  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t396_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t396  (*Func)(void* obj, float p1, const MethodInfo* method);
	DateTime_t396  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t396_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t396  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DateTime_t396  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t661_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t661_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t661_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, float p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t661_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t661_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t661_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t667_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t667_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t667_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t667_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t667_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t662_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t662_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t662_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t662_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t666_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t666_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t666_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t666_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t666_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, double p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t61_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t652_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t652_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t652_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t652_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t652_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t654_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t654_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t654_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t654_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t654_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t665_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t665_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t665_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t665_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t665_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_SByte_t666_TimeSpan_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, TimeSpan_t977  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((TimeSpan_t977 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int64_t662_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_DayOfWeek_t1642 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTimeKind_t1640 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t396_TimeSpan_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t396  (*Func)(void* obj, TimeSpan_t977  p1, const MethodInfo* method);
	DateTime_t396  ret = ((Func)method->method)(obj, *((TimeSpan_t977 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t396_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t396  (*Func)(void* obj, double p1, const MethodInfo* method);
	DateTime_t396  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_DateTime_t396_DateTime_t396 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t396  p1, DateTime_t396  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t396 *)args[0]), *((DateTime_t396 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t396_DateTime_t396_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t396  (*Func)(void* obj, DateTime_t396  p1, int32_t p2, const MethodInfo* method);
	DateTime_t396  ret = ((Func)method->method)(obj, *((DateTime_t396 *)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t396_Object_t_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t396  (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	DateTime_t396  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Object_t_Int32_t56_DateTimeU26_t2786_DateTimeOffsetU26_t2787_SByte_t666_ExceptionU26_t2754 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, DateTime_t396 * p4, DateTimeOffset_t679 * p5, int8_t p6, Exception_t301 ** p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (DateTime_t396 *)args[3], (DateTimeOffset_t679 *)args[4], *((int8_t*)args[5]), (Exception_t301 **)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t666_ExceptionU26_t2754 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Exception_t301 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Exception_t301 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Int32_t56_SByte_t666_SByte_t666_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, int32_t* p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Object_t_Object_t_SByte_t666_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, int8_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Int32_t56_Object_t_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Int32_t56_Object_t_SByte_t666_Int32U26_t2726_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, int32_t* p6, int32_t* p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_SByte_t666_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Object_t_Object_t_SByte_t666_DateTimeU26_t2786_DateTimeOffsetU26_t2787_Object_t_Int32_t56_SByte_t666_BooleanU26_t2725_BooleanU26_t2725 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, DateTime_t396 * p5, DateTimeOffset_t679 * p6, Object_t * p7, int32_t p8, int8_t p9, bool* p10, bool* p11, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), (DateTime_t396 *)args[4], (DateTimeOffset_t679 *)args[5], (Object_t *)args[6], *((int32_t*)args[7]), *((int8_t*)args[8]), (bool*)args[9], (bool*)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t396_Object_t_Object_t_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t396  (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	DateTime_t396  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_Object_t_Object_t_Int32_t56_DateTimeU26_t2786_SByte_t666_BooleanU26_t2725_SByte_t666_ExceptionU26_t2754 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, DateTime_t396 * p5, int8_t p6, bool* p7, int8_t p8, Exception_t301 ** p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (DateTime_t396 *)args[4], *((int8_t*)args[5]), (bool*)args[6], *((int8_t*)args[7]), (Exception_t301 **)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t396_DateTime_t396_TimeSpan_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t396  (*Func)(void* obj, DateTime_t396  p1, TimeSpan_t977  p2, const MethodInfo* method);
	DateTime_t396  ret = ((Func)method->method)(obj, *((DateTime_t396 *)args[0]), *((TimeSpan_t977 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_DateTime_t396_DateTime_t396 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t396  p1, DateTime_t396  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t396 *)args[0]), *((DateTime_t396 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_DateTime_t396_TimeSpan_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t396  p1, TimeSpan_t977  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t396 *)args[0]), *((TimeSpan_t977 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int64_t662_TimeSpan_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, TimeSpan_t977  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((TimeSpan_t977 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_DateTimeOffset_t679 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t679  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t679 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_DateTimeOffset_t679 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t679  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t679 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTimeOffset_t679 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTimeOffset_t679  (*Func)(void* obj, const MethodInfo* method);
	DateTimeOffset_t679  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t667_Object_t_BooleanU26_t2725_BooleanU26_t2725 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t667_Object_t_BooleanU26_t2725_BooleanU26_t2725_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_DateTime_t396_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t396  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t396 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_DateTime_t396_Nullable_1_t1741_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t396  p1, Nullable_1_t1741  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t396 *)args[0]), *((Nullable_1_t1741 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_MonoEnumInfo_t1653 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, MonoEnumInfo_t1653  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((MonoEnumInfo_t1653 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_MonoEnumInfoU26_t2788 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEnumInfo_t1653 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEnumInfo_t1653 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Int16_t667_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Int64_t662_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PlatformID_t1681 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Int16_t667_Int16_t667_SByte_t666_SByte_t666_SByte_t666_SByte_t666_SByte_t666_SByte_t666_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, int16_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int8_t p10, int8_t p11, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), *((int16_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int8_t*)args[10]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Guid_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t680  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t680 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Guid_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t680  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t680 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Guid_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef Guid_t680  (*Func)(void* obj, const MethodInfo* method);
	Guid_t680  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t666_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Double_t661_Double_t661_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, double p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), *((double*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeAttributes_t1353_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_UInt64U2AU26_t2789_Int32U2AU26_t2790_CharU2AU26_t2791_CharU2AU26_t2791_Int64U2AU26_t2792_Int32U2AU26_t2790 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t** p1, int32_t** p2, uint16_t** p3, uint16_t** p4, int64_t** p5, int32_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (uint64_t**)args[0], (int32_t**)args[1], (uint16_t**)args[2], (uint16_t**)args[3], (int64_t**)args[4], (int32_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Double_t661_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Decimal_t664  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t664 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t666_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int16_t667_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int64_t662_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Single_t61_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Double_t661_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Decimal_t664_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Decimal_t664  p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t664 *)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Single_t61_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Double_t661_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Object_t_BooleanU26_t2725_SByte_t666_Int32U26_t2726_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, int8_t p3, int32_t* p4, int32_t* p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], *((int8_t*)args[2]), (int32_t*)args[3], (int32_t*)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t56_Int32_t56_Object_t_SByte_t666_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Int64_t662_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t977_TimeSpan_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t977  (*Func)(void* obj, TimeSpan_t977  p1, const MethodInfo* method);
	TimeSpan_t977  ret = ((Func)method->method)(obj, *((TimeSpan_t977 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_TimeSpan_t977_TimeSpan_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t977  p1, TimeSpan_t977  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t977 *)args[0]), *((TimeSpan_t977 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_TimeSpan_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t977  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t977 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_TimeSpan_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t977  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t977 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t977_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t977  (*Func)(void* obj, double p1, const MethodInfo* method);
	TimeSpan_t977  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t977_Double_t661_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t977  (*Func)(void* obj, double p1, int64_t p2, const MethodInfo* method);
	TimeSpan_t977  ret = ((Func)method->method)(obj, *((double*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t977_TimeSpan_t977_TimeSpan_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t977  (*Func)(void* obj, TimeSpan_t977  p1, TimeSpan_t977  p2, const MethodInfo* method);
	TimeSpan_t977  ret = ((Func)method->method)(obj, *((TimeSpan_t977 *)args[0]), *((TimeSpan_t977 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t977_DateTime_t396 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t977  (*Func)(void* obj, DateTime_t396  p1, const MethodInfo* method);
	TimeSpan_t977  ret = ((Func)method->method)(obj, *((DateTime_t396 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_DateTime_t396_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t396  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t396 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t396_DateTime_t396 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t396  (*Func)(void* obj, DateTime_t396  p1, const MethodInfo* method);
	DateTime_t396  ret = ((Func)method->method)(obj, *((DateTime_t396 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t977_DateTime_t396_TimeSpan_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t977  (*Func)(void* obj, DateTime_t396  p1, TimeSpan_t977  p2, const MethodInfo* method);
	TimeSpan_t977  ret = ((Func)method->method)(obj, *((DateTime_t396 *)args[0]), *((TimeSpan_t977 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int32_t56_Int64U5BU5DU26_t2793_StringU5BU5DU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Int64U5BU5D_t1728** p2, StringU5BU5D_t16** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Int64U5BU5D_t1728**)args[1], (StringU5BU5D_t16**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_ObjectU26_t2743_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t ** p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_ObjectU26_t2743_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t ** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_KeyValuePair_2_t1934 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1934  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1934 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_KeyValuePair_2_t1934 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1934  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1934 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t1828 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1828  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1828  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_ObjectU26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_ObjectU5BU5DU26_t2753_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t60** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t60**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_ObjectU5BU5DU26_t2753_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t60** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t60**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t1934_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1934  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t1934  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1938 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1938  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1938  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1068_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1068  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t1068  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1934 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1934  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1934  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1937 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1937  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1937  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1941 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1941  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1941  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Int32_t56_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1802 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1802  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1802  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1885 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1885  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1885  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1882 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1882  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1882  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1877 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1877  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1877  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_ColorTween_t127 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorTween_t127  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ColorTween_t127 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_TypeU26_t2795_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_BooleanU26_t2725_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, bool* p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (bool*)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_FillMethodU26_t2796_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_SingleU26_t2738_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float* p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_ContentTypeU26_t2797_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_LineTypeU26_t2798_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_InputTypeU26_t2799_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_TouchScreenKeyboardTypeU26_t2800_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_CharacterValidationU26_t2801_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_CharU26_t2751_Int16_t667 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t* p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (uint16_t*)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_DirectionU26_t2802_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_NavigationU26_t2803_Navigation_t194 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Navigation_t194 * p1, Navigation_t194  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Navigation_t194 *)args[0], *((Navigation_t194 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_TransitionU26_t2804_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_ColorBlockU26_t2805_ColorBlock_t140 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ColorBlock_t140 * p1, ColorBlock_t140  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (ColorBlock_t140 *)args[0], *((ColorBlock_t140 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_SpriteStateU26_t2806_SpriteState_t211 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SpriteState_t211 * p1, SpriteState_t211  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (SpriteState_t211 *)args[0], *((SpriteState_t211 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_DirectionU26_t2807_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_AspectModeU26_t2808_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_FitModeU26_t2809_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_CornerU26_t2810_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_AxisU26_t2811_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Vector2U26_t2732_Vector2_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t59 * p1, Vector2_t59  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t59 *)args[0], *((Vector2_t59 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_ConstraintU26_t2812_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32U26_t2726_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_SingleU26_t2738_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float* p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_BooleanU26_t2725_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_TextAnchorU26_t2813_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Object_t_Object_t_Single_t61 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Vector3_t35_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t35  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector3_t35  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t35  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector3_t35  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector3_t35 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Vector3U5BU5DU26_t2814_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3U5BU5D_t208** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3U5BU5D_t208**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Vector3U5BU5DU26_t2814_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3U5BU5D_t208** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3U5BU5D_t208**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Object_t_Vector3_t35_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Vector3_t35  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t35 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Double_t661 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((double*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Vector3_t35_Vector3_t35_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t35  p1, Vector3_t35  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), *((Vector3_t35 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastResult_t103_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t103  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastResult_t103  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_RaycastResult_t103 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t103  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t103 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_RaycastResult_t103 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t103  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t103 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_RaycastResult_t103 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastResult_t103  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastResult_t103 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_RaycastResultU5BU5DU26_t2815_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t1835** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t1835**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_RaycastResultU5BU5DU26_t2815_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t1835** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t1835**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Object_t_RaycastResult_t103_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, RaycastResult_t103  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((RaycastResult_t103 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_RaycastResult_t103_RaycastResult_t103_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t103  p1, RaycastResult_t103  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t103 *)args[0]), *((RaycastResult_t103 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1877_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1877  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1877  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_KeyValuePair_2_t1877 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1877  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1877 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_KeyValuePair_2_t1877 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1877  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1877 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_KeyValuePair_2_t1877 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1877  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1877 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_KeyValuePair_2_t1877 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1877  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1877 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Link_t1188_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t1188  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Link_t1188  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Link_t1188 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Link_t1188  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Link_t1188 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_Link_t1188 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Link_t1188  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Link_t1188 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Link_t1188 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Link_t1188  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Link_t1188 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Link_t1188 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Link_t1188  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Link_t1188 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_DictionaryEntry_t1068_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1068  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DictionaryEntry_t1068  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_DictionaryEntry_t1068 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DictionaryEntry_t1068  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DictionaryEntry_t1068 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_DictionaryEntry_t1068 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DictionaryEntry_t1068  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DictionaryEntry_t1068 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_DictionaryEntry_t1068 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DictionaryEntry_t1068  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DictionaryEntry_t1068 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_DictionaryEntry_t1068 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DictionaryEntry_t1068  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DictionaryEntry_t1068 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_RaycastHit2D_t52_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t52  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastHit2D_t52  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_RaycastHit2D_t52 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit2D_t52  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit2D_t52 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_RaycastHit2D_t52 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit2D_t52  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit2D_t52 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_RaycastHit2D_t52 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit2D_t52  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit2D_t52 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_RaycastHit2D_t52 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit2D_t52  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit2D_t52 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_RaycastHit_t283_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t283  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastHit_t283  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_RaycastHit_t283 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit_t283  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit_t283 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_RaycastHit_t283 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit_t283  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit_t283 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_RaycastHit_t283 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t283  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t283 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_RaycastHit_t283 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit_t283  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit_t283 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t1908_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1908  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1908  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_KeyValuePair_2_t1908 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1908  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1908 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_KeyValuePair_2_t1908 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1908  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1908 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_KeyValuePair_2_t1908 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1908  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1908 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_KeyValuePair_2_t1908 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1908  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1908 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t1934_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1934  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1934  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_KeyValuePair_2_t1934 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1934  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1934 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_KeyValuePair_2_t1934 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1934  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1934 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_UIVertexU5BU5DU26_t2816_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t185** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t185**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_UIVertexU5BU5DU26_t2816_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t185** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t185**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Object_t_UIVertex_t191_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UIVertex_t191  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UIVertex_t191 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_UIVertex_t191_UIVertex_t191_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t191  p1, UIVertex_t191  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t191 *)args[0]), *((UIVertex_t191 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Vector2_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t59  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Vector2_t59 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector2_t59  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector2_t59 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ContentType_t170_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UILineInfo_t331_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t331  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UILineInfo_t331  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_UILineInfo_t331 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfo_t331  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UILineInfo_t331 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_UILineInfo_t331 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t331  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t331 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_UILineInfo_t331 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t331  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t331 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_UILineInfo_t331 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UILineInfo_t331  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UILineInfo_t331 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UICharInfo_t333_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t333  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UICharInfo_t333  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_UICharInfo_t333 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfo_t333  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UICharInfo_t333 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_UICharInfo_t333 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t333  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t333 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_UICharInfo_t333 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t333  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t333 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_UICharInfo_t333 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UICharInfo_t333  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UICharInfo_t333 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_GcAchievementData_t546_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t546  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcAchievementData_t546  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_GcAchievementData_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementData_t546  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementData_t546 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_GcAchievementData_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcAchievementData_t546  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcAchievementData_t546 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_GcAchievementData_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcAchievementData_t546  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcAchievementData_t546 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_GcAchievementData_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcAchievementData_t546  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcAchievementData_t546 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_GcScoreData_t547_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t547  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcScoreData_t547  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_GcScoreData_t547 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcScoreData_t547  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcScoreData_t547 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_GcScoreData_t547 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcScoreData_t547  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcScoreData_t547 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_GcScoreData_t547 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcScoreData_t547  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcScoreData_t547 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Keyframe_t469_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t469  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Keyframe_t469  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Keyframe_t469 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Keyframe_t469  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Keyframe_t469 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_Keyframe_t469 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Keyframe_t469  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Keyframe_t469 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Keyframe_t469 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Keyframe_t469  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Keyframe_t469 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Keyframe_t469 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Keyframe_t469  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Keyframe_t469 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_UICharInfoU5BU5DU26_t2817_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t611** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t611**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_UICharInfoU5BU5DU26_t2817_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t611** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t611**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Object_t_UICharInfo_t333_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UICharInfo_t333  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UICharInfo_t333 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_UICharInfo_t333_UICharInfo_t333_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t333  p1, UICharInfo_t333  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t333 *)args[0]), *((UICharInfo_t333 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_UILineInfoU5BU5DU26_t2818_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t612** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t612**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_UILineInfoU5BU5DU26_t2818_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t612** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t612**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Object_t_UILineInfo_t331_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UILineInfo_t331  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UILineInfo_t331 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_UILineInfo_t331_UILineInfo_t331_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t331  p1, UILineInfo_t331  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t331 *)args[0]), *((UILineInfo_t331 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2118_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2118  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2118  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_KeyValuePair_2_t2118 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2118  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2118 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_KeyValuePair_2_t2118 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2118  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2118 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_KeyValuePair_2_t2118 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2118  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2118 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_KeyValuePair_2_t2118 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2118  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2118 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t2155_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2155  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2155  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_KeyValuePair_2_t2155 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2155  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2155 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_KeyValuePair_2_t2155 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2155  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2155 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_KeyValuePair_2_t2155 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2155  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2155 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_KeyValuePair_2_t2155 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2155  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2155 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_NetworkID_t502_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_UInt64_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint64_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_UInt64_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, uint64_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_UInt64_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, uint64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((uint64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t2175_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2175  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2175  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_KeyValuePair_2_t2175 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2175  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2175 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_KeyValuePair_2_t2175 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2175  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2175 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_KeyValuePair_2_t2175 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2175  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2175 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_KeyValuePair_2_t2175 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2175  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2175 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ParameterModifier_t1346_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t1346  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ParameterModifier_t1346  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_ParameterModifier_t1346 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ParameterModifier_t1346  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ParameterModifier_t1346 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_ParameterModifier_t1346 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ParameterModifier_t1346  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ParameterModifier_t1346 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_ParameterModifier_t1346 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ParameterModifier_t1346  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ParameterModifier_t1346 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_ParameterModifier_t1346 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ParameterModifier_t1346  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ParameterModifier_t1346 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_HitInfo_t561_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t561  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	HitInfo_t561  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_HitInfo_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HitInfo_t561  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((HitInfo_t561 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_HitInfo_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HitInfo_t561  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HitInfo_t561 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2238_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2238  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2238  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_KeyValuePair_2_t2238 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2238  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2238 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_KeyValuePair_2_t2238 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2238  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2238 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_KeyValuePair_2_t2238 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2238  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2238 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_KeyValuePair_2_t2238 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2238  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2238 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TextEditOp_t579_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ClientCertificateType_t844_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2289_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2289  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2289  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_KeyValuePair_2_t2289 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2289  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2289 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_KeyValuePair_2_t2289 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2289  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2289 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_KeyValuePair_2_t2289 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2289  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2289 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_KeyValuePair_2_t2289 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2289  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2289 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_X509ChainStatus_t974_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t974  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	X509ChainStatus_t974  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_X509ChainStatus_t974 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, X509ChainStatus_t974  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((X509ChainStatus_t974 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_X509ChainStatus_t974 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, X509ChainStatus_t974  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((X509ChainStatus_t974 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_X509ChainStatus_t974 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, X509ChainStatus_t974  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((X509ChainStatus_t974 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_X509ChainStatus_t974 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, X509ChainStatus_t974  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((X509ChainStatus_t974 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t2312_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2312  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2312  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_KeyValuePair_2_t2312 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2312  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2312 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_KeyValuePair_2_t2312 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2312  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2312 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_KeyValuePair_2_t2312 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2312  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2312 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_KeyValuePair_2_t2312 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2312  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2312 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Mark_t1019_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t1019  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Mark_t1019  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Mark_t1019 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Mark_t1019  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Mark_t1019 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_Mark_t1019 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Mark_t1019  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Mark_t1019 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Mark_t1019 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Mark_t1019  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Mark_t1019 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Mark_t1019 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Mark_t1019  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Mark_t1019 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UriScheme_t1056_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t1056  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UriScheme_t1056  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_UriScheme_t1056 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UriScheme_t1056  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UriScheme_t1056 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_UriScheme_t1056 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UriScheme_t1056  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UriScheme_t1056 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_UriScheme_t1056 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UriScheme_t1056  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UriScheme_t1056 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_UriScheme_t1056 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UriScheme_t1056  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UriScheme_t1056 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TableRange_t1121_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1121  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TableRange_t1121  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_TableRange_t1121 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TableRange_t1121  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TableRange_t1121 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_TableRange_t1121 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TableRange_t1121  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TableRange_t1121 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_TableRange_t1121 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TableRange_t1121  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TableRange_t1121 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_TableRange_t1121 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TableRange_t1121  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TableRange_t1121 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Slot_t1198_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1198  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1198  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Slot_t1198 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1198  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1198 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_Slot_t1198 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1198  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1198 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Slot_t1198 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1198  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1198 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Slot_t1198 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1198  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1198 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Slot_t1205_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1205  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1205  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Slot_t1205 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1205  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1205 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_Slot_t1205 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1205  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1205 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Slot_t1205 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1205  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1205 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Slot_t1205 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1205  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1205 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ILTokenInfo_t1283_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t1283  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ILTokenInfo_t1283  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_ILTokenInfo_t1283 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ILTokenInfo_t1283  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ILTokenInfo_t1283 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_ILTokenInfo_t1283 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ILTokenInfo_t1283  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ILTokenInfo_t1283 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_ILTokenInfo_t1283 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ILTokenInfo_t1283  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ILTokenInfo_t1283 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_ILTokenInfo_t1283 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ILTokenInfo_t1283  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ILTokenInfo_t1283 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_LabelData_t1285_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t1285  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelData_t1285  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_LabelData_t1285 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelData_t1285  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelData_t1285 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_LabelData_t1285 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelData_t1285  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelData_t1285 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_LabelData_t1285 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelData_t1285  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelData_t1285 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_LabelData_t1285 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelData_t1285  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelData_t1285 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_LabelFixup_t1284_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t1284  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelFixup_t1284  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_LabelFixup_t1284 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelFixup_t1284  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelFixup_t1284 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_LabelFixup_t1284 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelFixup_t1284  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelFixup_t1284 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_LabelFixup_t1284 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelFixup_t1284  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelFixup_t1284 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_LabelFixup_t1284 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelFixup_t1284  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelFixup_t1284 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_DateTime_t396 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DateTime_t396  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DateTime_t396 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t664  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Decimal_t664 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Decimal_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Decimal_t664  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Decimal_t664 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t977_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t977  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TimeSpan_t977  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_TimeSpan_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TimeSpan_t977  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TimeSpan_t977 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TypeTag_t1484_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Byte_t647 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Byte_t647 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_Byte_t647 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Vector3_t35_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t35  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector3_t35  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1785 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1785  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1785  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector3_t35_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t35  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t56_Vector3_t35_Vector3_t35 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t35  p1, Vector3_t35  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), *((Vector3_t35 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector3_t35_Vector3_t35_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t35  p1, Vector3_t35  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t35 *)args[0]), *((Vector3_t35 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_RaycastResult_t103_RaycastResult_t103_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t103  p1, RaycastResult_t103  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t103 *)args[0]), *((RaycastResult_t103 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t1837 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1837  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1837  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_RaycastResult_t103_RaycastResult_t103 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t103  p1, RaycastResult_t103  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t103 *)args[0]), *((RaycastResult_t103 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_RaycastResult_t103_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t103  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t103 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_KeyValuePair_2_t1877_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1877  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t1877  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int32_t56_ObjectU26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1068_Int32_t56_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1068  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t1068  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Link_t1188 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t1188  (*Func)(void* obj, const MethodInfo* method);
	Link_t1188  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1881 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1881  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1881  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1068_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1068  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DictionaryEntry_t1068  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1877_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1877  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1877  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastHit2D_t52 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t52  (*Func)(void* obj, const MethodInfo* method);
	RaycastHit2D_t52  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_RaycastHit_t283_RaycastHit_t283_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastHit_t283  p1, RaycastHit_t283  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastHit_t283 *)args[0]), *((RaycastHit_t283 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_RaycastHit_t283 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t283  (*Func)(void* obj, const MethodInfo* method);
	RaycastHit_t283  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Color_t128_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color_t128  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color_t128 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_KeyValuePair_2_t1908_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1908  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t1908  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1912 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1912  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1912  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1068_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1068  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t1068  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1908 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1908  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1908  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1911 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1911  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1911  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1915 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1915  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1915  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1908_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1908  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1908  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1934_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1934  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1934  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_UIVertex_t191 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertex_t191  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UIVertex_t191 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t298_UIVertex_t191 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t191  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t191 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIVertex_t191_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t191  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIVertex_t191  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1952 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1952  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1952  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_UIVertex_t191 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t191  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t191 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Int32_t56_UIVertex_t191 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UIVertex_t191  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UIVertex_t191 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UIVertex_t191_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t191  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIVertex_t191  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIVertex_t191 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t191  (*Func)(void* obj, const MethodInfo* method);
	UIVertex_t191  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_UIVertex_t191_UIVertex_t191 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t191  p1, UIVertex_t191  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t191 *)args[0]), *((UIVertex_t191 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UIVertex_t191_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t191  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t191 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t56_UIVertex_t191_UIVertex_t191 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t191  p1, UIVertex_t191  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t191 *)args[0]), *((UIVertex_t191 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UIVertex_t191_UIVertex_t191_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t191  p1, UIVertex_t191  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t191 *)args[0]), *((UIVertex_t191 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_ColorTween_t127 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, ColorTween_t127  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((ColorTween_t127 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_UILineInfo_t331 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t331  (*Func)(void* obj, const MethodInfo* method);
	UILineInfo_t331  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UICharInfo_t333 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t333  (*Func)(void* obj, const MethodInfo* method);
	UICharInfo_t333  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Single_t61_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Vector2_t59_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t59  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t59 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_GcAchievementData_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t546  (*Func)(void* obj, const MethodInfo* method);
	GcAchievementData_t546  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_GcScoreData_t547 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t547  (*Func)(void* obj, const MethodInfo* method);
	GcScoreData_t547  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Keyframe_t469 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t469  (*Func)(void* obj, const MethodInfo* method);
	Keyframe_t469  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UICharInfo_t333_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t333  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UICharInfo_t333  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2097 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2097  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2097  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_UICharInfo_t333_UICharInfo_t333 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t333  p1, UICharInfo_t333  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t333 *)args[0]), *((UICharInfo_t333 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UICharInfo_t333_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t333  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t333 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t56_UICharInfo_t333_UICharInfo_t333 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t333  p1, UICharInfo_t333  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t333 *)args[0]), *((UICharInfo_t333 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UICharInfo_t333_UICharInfo_t333_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t333  p1, UICharInfo_t333  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t333 *)args[0]), *((UICharInfo_t333 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_UILineInfo_t331_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t331  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UILineInfo_t331  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2106 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2106  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2106  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_UILineInfo_t331_UILineInfo_t331 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t331  p1, UILineInfo_t331  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t331 *)args[0]), *((UILineInfo_t331 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UILineInfo_t331_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t331  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t331 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t56_UILineInfo_t331_UILineInfo_t331 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t331  p1, UILineInfo_t331  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t331 *)args[0]), *((UILineInfo_t331 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UILineInfo_t331_UILineInfo_t331_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t331  p1, UILineInfo_t331  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t331 *)args[0]), *((UILineInfo_t331 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_KeyValuePair_2_t2118_Object_t_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2118  (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	KeyValuePair_2_t2118  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t662_Object_t_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2123 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2123  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2123  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1068_Object_t_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1068  (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	DictionaryEntry_t1068  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2118 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2118  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2118  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2122 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2122  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2122  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int64_t662_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t2126 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2126  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2126  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2118_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2118  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2118  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int64_t662_Int64_t662 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_UInt64_t665_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t2155_UInt64_t665_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2155  (*Func)(void* obj, uint64_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t2155  ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_NetworkID_t502_UInt64_t665_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, uint64_t p1, Object_t * p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_UInt64_t665_ObjectU26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint64_t p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_NetworkID_t502_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2160 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2160  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2160  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1068_UInt64_t665_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1068  (*Func)(void* obj, uint64_t p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t1068  ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2155 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2155  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2155  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2159 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2159  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2159  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UInt64_t665_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint64_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t2163 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2163  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2163  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2155_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2155  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2155  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_UInt64_t665_UInt64_t665 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint64_t p1, uint64_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint64_t*)args[0]), *((uint64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2175 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2175  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2175  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1089_Object_t_KeyValuePair_2_t1934 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t1934  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t1934 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t2175_Object_t_KeyValuePair_2_t1934 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2175  (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t1934  p2, const MethodInfo* method);
	KeyValuePair_2_t2175  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t1934 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t1934 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t1934  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t1934 *)args[1]), method);
	return ret;
}

void* RuntimeInvoker_KeyValuePair_2_t1934_Object_t_KeyValuePair_2_t1934 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1934  (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t1934  p2, const MethodInfo* method);
	KeyValuePair_2_t1934  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t1934 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_KeyValuePair_2U26_t2819 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t1934 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (KeyValuePair_2_t1934 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2204 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2204  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2204  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1068_Object_t_KeyValuePair_2_t1934 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1068  (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t1934  p2, const MethodInfo* method);
	DictionaryEntry_t1068  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t1934 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2203 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2203  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2203  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t1934_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, KeyValuePair_2_t1934  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((KeyValuePair_2_t1934 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t2207 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2207  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2207  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2175_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2175  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2175  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_KeyValuePair_2_t1934_KeyValuePair_2_t1934 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1934  p1, KeyValuePair_2_t1934  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1934 *)args[0]), *((KeyValuePair_2_t1934 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ParameterModifier_t1346 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t1346  (*Func)(void* obj, const MethodInfo* method);
	ParameterModifier_t1346  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HitInfo_t561 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t561  (*Func)(void* obj, const MethodInfo* method);
	HitInfo_t561  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextEditOp_t579_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2238_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2238  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t2238  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextEditOp_t579_Object_t_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_TextEditOpU26_t2820 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2243 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2243  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2243  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2238 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2238  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2238  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextEditOp_t579 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2242 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2242  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2242  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2246 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2246  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2246  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2238_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2238  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2238  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ClientCertificateType_t844 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2289_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2289  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	KeyValuePair_2_t2289  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Object_t_BooleanU26_t2725 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2294 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2294  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2294  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1068_Object_t_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1068  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	DictionaryEntry_t1068  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2289 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2289  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2289  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2293 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2293  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2293  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t666_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t2297 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2297  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2297  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2289_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2289  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2289  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_SByte_t666_SByte_t666 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatus_t974 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t974  (*Func)(void* obj, const MethodInfo* method);
	X509ChainStatus_t974  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2312_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2312  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t2312  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Int32_t56_Int32U26_t2726 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2316 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2316  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2316  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1068_Int32_t56_Int32_t56 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1068  (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t1068  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2312 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2312  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2312  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2315 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2315  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2315  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2319 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2319  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2319  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2312_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2312  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2312  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Mark_t1019 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t1019  (*Func)(void* obj, const MethodInfo* method);
	Mark_t1019  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UriScheme_t1056 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t1056  (*Func)(void* obj, const MethodInfo* method);
	UriScheme_t1056  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TableRange_t1121 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1121  (*Func)(void* obj, const MethodInfo* method);
	TableRange_t1121  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Slot_t1198 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1198  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1198  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Slot_t1205 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1205  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1205  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ILTokenInfo_t1283 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t1283  (*Func)(void* obj, const MethodInfo* method);
	ILTokenInfo_t1283  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LabelData_t1285 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t1285  (*Func)(void* obj, const MethodInfo* method);
	LabelData_t1285  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LabelFixup_t1284 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t1284  (*Func)(void* obj, const MethodInfo* method);
	LabelFixup_t1284  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeTag_t1484 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_DateTimeOffset_t679_DateTimeOffset_t679 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t679  p1, DateTimeOffset_t679  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t679 *)args[0]), *((DateTimeOffset_t679 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_DateTimeOffset_t679_DateTimeOffset_t679 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t679  p1, DateTimeOffset_t679  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t679 *)args[0]), *((DateTimeOffset_t679 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Nullable_1_t1741 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t1741  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t1741 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t56_Guid_t680_Guid_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t680  p1, Guid_t680  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t680 *)args[0]), *((Guid_t680 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t298_Guid_t680_Guid_t680 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t680  p1, Guid_t680  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t680 *)args[0]), *((Guid_t680 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

extern const InvokerMethod g_Il2CppInvokerPointers[1469] = 
{
	RuntimeInvoker_Void_t1089,
	RuntimeInvoker_Object_t,
	RuntimeInvoker_Boolean_t298,
	RuntimeInvoker_Void_t1089_Object_t,
	RuntimeInvoker_Object_t_Single_t61_Single_t61_Single_t61,
	RuntimeInvoker_Void_t1089_Int32_t56,
	RuntimeInvoker_Object_t_Object_t,
	RuntimeInvoker_Boolean_t298_Int32_t56_Int32_t56_RaycastHit2DU26_t2723,
	RuntimeInvoker_Object_t_Vector3_t35,
	RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Object_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_Single_t61_Single_t61,
	RuntimeInvoker_Object_t_Single_t61,
	RuntimeInvoker_Void_t1089_SByte_t666,
	RuntimeInvoker_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Object_t,
	RuntimeInvoker_Int32_t56_RaycastResult_t103_RaycastResult_t103,
	RuntimeInvoker_Boolean_t298_Int32_t56,
	RuntimeInvoker_Void_t1089_Int32_t56_Object_t,
	RuntimeInvoker_Vector2_t59,
	RuntimeInvoker_Void_t1089_Vector2_t59,
	RuntimeInvoker_MoveDirection_t100,
	RuntimeInvoker_RaycastResult_t103,
	RuntimeInvoker_Void_t1089_RaycastResult_t103,
	RuntimeInvoker_Vector3_t35,
	RuntimeInvoker_Void_t1089_Vector3_t35,
	RuntimeInvoker_Single_t61,
	RuntimeInvoker_Void_t1089_Single_t61,
	RuntimeInvoker_InputButton_t106,
	RuntimeInvoker_RaycastResult_t103_Object_t,
	RuntimeInvoker_MoveDirection_t100_Single_t61_Single_t61,
	RuntimeInvoker_MoveDirection_t100_Single_t61_Single_t61_Single_t61,
	RuntimeInvoker_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t298_Int32_t56_PointerEventDataU26_t2724_SByte_t666,
	RuntimeInvoker_Object_t_Touch_t282_BooleanU26_t2725_BooleanU26_t2725,
	RuntimeInvoker_FramePressState_t107_Int32_t56,
	RuntimeInvoker_Object_t_Int32_t56,
	RuntimeInvoker_Boolean_t298_Vector2_t59_Vector2_t59_Single_t61_SByte_t666,
	RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Object_t,
	RuntimeInvoker_InputMode_t117,
	RuntimeInvoker_Void_t1089_Object_t_SByte_t666_SByte_t666,
	RuntimeInvoker_LayerMask_t37,
	RuntimeInvoker_Void_t1089_LayerMask_t37,
	RuntimeInvoker_Int32_t56_RaycastHit_t283_RaycastHit_t283,
	RuntimeInvoker_Color_t128,
	RuntimeInvoker_Void_t1089_Color_t128,
	RuntimeInvoker_ColorTweenMode_t124,
	RuntimeInvoker_Boolean_t298_Object_t,
	RuntimeInvoker_Int32_t56_Object_t,
	RuntimeInvoker_Int32_t56_Object_t_Object_t,
	RuntimeInvoker_ColorBlock_t140,
	RuntimeInvoker_FontStyle_t415,
	RuntimeInvoker_TextAnchor_t348,
	RuntimeInvoker_HorizontalWrapMode_t476,
	RuntimeInvoker_VerticalWrapMode_t477,
	RuntimeInvoker_Boolean_t298_Vector2_t59_Object_t,
	RuntimeInvoker_Vector2_t59_Vector2_t59,
	RuntimeInvoker_Rect_t184,
	RuntimeInvoker_Void_t1089_Color_t128_Single_t61_SByte_t666_SByte_t666,
	RuntimeInvoker_Void_t1089_Color_t128_Single_t61_SByte_t666_SByte_t666_SByte_t666,
	RuntimeInvoker_Color_t128_Single_t61,
	RuntimeInvoker_Void_t1089_Single_t61_Single_t61_SByte_t666,
	RuntimeInvoker_BlockingObjects_t154,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Vector2_t59_Object_t,
	RuntimeInvoker_Type_t160,
	RuntimeInvoker_FillMethod_t161,
	RuntimeInvoker_Vector4_t287_SByte_t666,
	RuntimeInvoker_Void_t1089_Object_t_SByte_t666,
	RuntimeInvoker_Void_t1089_Object_t_UIVertex_t191_Vector2_t59_Vector2_t59_Vector2_t59_Vector2_t59,
	RuntimeInvoker_Vector4_t287_Vector4_t287_Rect_t184,
	RuntimeInvoker_Boolean_t298_Object_t_Object_t_Single_t61_SByte_t666_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Single_t61_Single_t61_SByte_t666_Int32_t56,
	RuntimeInvoker_Vector2_t59_Vector2_t59_Rect_t184,
	RuntimeInvoker_ContentType_t170,
	RuntimeInvoker_LineType_t173,
	RuntimeInvoker_InputType_t171,
	RuntimeInvoker_TouchScreenKeyboardType_t327,
	RuntimeInvoker_CharacterValidation_t172,
	RuntimeInvoker_Char_t326,
	RuntimeInvoker_Void_t1089_Int16_t667,
	RuntimeInvoker_Void_t1089_Int32U26_t2726,
	RuntimeInvoker_Int32_t56_Vector2_t59_Object_t,
	RuntimeInvoker_Int32_t56_Vector2_t59,
	RuntimeInvoker_EditState_t177_Object_t,
	RuntimeInvoker_Boolean_t298_Int16_t667,
	RuntimeInvoker_Void_t1089_SByte_t666_SByte_t666,
	RuntimeInvoker_Int32_t56_Int32_t56_Object_t,
	RuntimeInvoker_Int32_t56_Int32_t56_SByte_t666,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32U26_t2726_Int32U26_t2726,
	RuntimeInvoker_Void_t1089_Object_t_Vector2_t59,
	RuntimeInvoker_Single_t61_Int32_t56_Object_t,
	RuntimeInvoker_Char_t326_Object_t_Int32_t56_Int16_t667,
	RuntimeInvoker_Void_t1089_Int32_t56_SByte_t666,
	RuntimeInvoker_Void_t1089_Object_t_IntPtr_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t56_Int16_t667_Object_t_Object_t,
	RuntimeInvoker_Char_t326_Object_t,
	RuntimeInvoker_Mode_t193,
	RuntimeInvoker_Navigation_t194,
	RuntimeInvoker_Void_t1089_Rect_t184,
	RuntimeInvoker_Direction_t197,
	RuntimeInvoker_Void_t1089_Single_t61_SByte_t666,
	RuntimeInvoker_Axis_t200,
	RuntimeInvoker_MovementType_t204,
	RuntimeInvoker_Void_t1089_Single_t61_Int32_t56,
	RuntimeInvoker_Single_t61_Single_t61_Single_t61,
	RuntimeInvoker_Bounds_t207,
	RuntimeInvoker_Void_t1089_Navigation_t194,
	RuntimeInvoker_Transition_t209,
	RuntimeInvoker_Void_t1089_ColorBlock_t140,
	RuntimeInvoker_SpriteState_t211,
	RuntimeInvoker_Void_t1089_SpriteState_t211,
	RuntimeInvoker_SelectionState_t210,
	RuntimeInvoker_Vector3_t35_Object_t_Vector2_t59,
	RuntimeInvoker_Void_t1089_Color_t128_SByte_t666,
	RuntimeInvoker_Boolean_t298_ColorU26_t2727_Color_t128,
	RuntimeInvoker_Direction_t215,
	RuntimeInvoker_Single_t61_Single_t61,
	RuntimeInvoker_Axis_t217,
	RuntimeInvoker_Object_t_Object_t_Int32_t56,
	RuntimeInvoker_TextGenerationSettings_t290_Vector2_t59,
	RuntimeInvoker_Vector2_t59_Int32_t56,
	RuntimeInvoker_AspectMode_t229,
	RuntimeInvoker_Single_t61_Single_t61_Int32_t56,
	RuntimeInvoker_ScaleMode_t231,
	RuntimeInvoker_ScreenMatchMode_t232,
	RuntimeInvoker_Unit_t233,
	RuntimeInvoker_FitMode_t235,
	RuntimeInvoker_Corner_t237,
	RuntimeInvoker_Axis_t238,
	RuntimeInvoker_Constraint_t239,
	RuntimeInvoker_Single_t61_Int32_t56,
	RuntimeInvoker_Single_t61_Int32_t56_Single_t61,
	RuntimeInvoker_Void_t1089_Single_t61_Single_t61_Single_t61_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Single_t61_Single_t61,
	RuntimeInvoker_Boolean_t298_LayoutRebuilder_t246,
	RuntimeInvoker_Single_t61_Object_t_Int32_t56,
	RuntimeInvoker_Single_t61_Object_t,
	RuntimeInvoker_Single_t61_Object_t_Object_t_Single_t61,
	RuntimeInvoker_Single_t61_Object_t_Object_t_Single_t61_ILayoutElementU26_t2728,
	RuntimeInvoker_Void_t1089_Object_t_Color32_t295_Int32_t56_Int32_t56_Single_t61_Single_t61,
	RuntimeInvoker_Int32_t56_LayerMask_t37,
	RuntimeInvoker_LayerMask_t37_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Double_t661,
	RuntimeInvoker_Void_t1089_Int64_t662_Object_t,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56,
	RuntimeInvoker_Void_t1089_GcAchievementDescriptionData_t545_Int32_t56,
	RuntimeInvoker_Void_t1089_GcUserProfileData_t544_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Double_t661_Object_t,
	RuntimeInvoker_Void_t1089_Int64_t662_Object_t_Object_t,
	RuntimeInvoker_Void_t1089_UserProfileU5BU5DU26_t2729_Object_t_Int32_t56,
	RuntimeInvoker_Void_t1089_UserProfileU5BU5DU26_t2729_Int32_t56,
	RuntimeInvoker_Void_t1089_GcScoreData_t547,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Object_t,
	RuntimeInvoker_Boolean_t298_BoneWeight_t381_BoneWeight_t381,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Int32_t56_SByte_t666_SByte_t666_IntPtr_t,
	RuntimeInvoker_Color_t128_Single_t61_Single_t61,
	RuntimeInvoker_Object_t_Object_t_Vector3U26_t2730,
	RuntimeInvoker_Void_t1089_Color_t128_Single_t61,
	RuntimeInvoker_Void_t1089_DateTime_t396,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Object_t_Int32_t56_Single_t61_Single_t61_Object_t,
	RuntimeInvoker_Object_t_Int32_t56_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t56_SByte_t666,
	RuntimeInvoker_Void_t1089_Int32_t56_Object_t_Object_t,
	RuntimeInvoker_Rect_t184_Int32_t56,
	RuntimeInvoker_Void_t1089_Int32_t56_Rect_t184,
	RuntimeInvoker_Void_t1089_Int32_t56_RectU26_t2731,
	RuntimeInvoker_Void_t1089_Single_t61_Single_t61_Single_t61_Single_t61_Object_t,
	RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_ColorU26_t2727,
	RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Rect_t184_Rect_t184,
	RuntimeInvoker_Rect_t184_Object_t_RectU26_t2731,
	RuntimeInvoker_IntPtr_t_Int32_t56,
	RuntimeInvoker_Single_t61_IntPtr_t,
	RuntimeInvoker_Vector2_t59_Rect_t184_Object_t_Int32_t56,
	RuntimeInvoker_Void_t1089_IntPtr_t_Rect_t184_Object_t_Int32_t56_Vector2U26_t2732,
	RuntimeInvoker_Void_t1089_IntPtr_t_RectU26_t2731_Object_t_Int32_t56_Vector2U26_t2732,
	RuntimeInvoker_Vector2_t59_Object_t,
	RuntimeInvoker_Void_t1089_IntPtr_t_Object_t_Vector2U26_t2732,
	RuntimeInvoker_Single_t61_Object_t_Single_t61,
	RuntimeInvoker_Single_t61_IntPtr_t_Object_t_Single_t61,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_SByte_t666_SByte_t666_SByte_t666_SByte_t666_Object_t,
	RuntimeInvoker_Void_t1089_TouchScreenKeyboard_InternalConstructorHelperArgumentsU26_t2733_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t56_SByte_t666_SByte_t666_SByte_t666,
	RuntimeInvoker_Object_t_Object_t_Int32_t56_SByte_t666_SByte_t666,
	RuntimeInvoker_Object_t_Object_t_Int32_t56_SByte_t666_SByte_t666_SByte_t666_SByte_t666_Object_t,
	RuntimeInvoker_EventType_t419,
	RuntimeInvoker_Void_t1089_Vector2U26_t2732,
	RuntimeInvoker_EventModifiers_t420,
	RuntimeInvoker_KeyCode_t418,
	RuntimeInvoker_Void_t1089_IntPtr_t,
	RuntimeInvoker_Void_t1089_Int32_t56_Single_t61,
	RuntimeInvoker_Vector2_t59_Vector2_t59_Vector2_t59,
	RuntimeInvoker_Single_t61_Vector2_t59_Vector2_t59,
	RuntimeInvoker_Single_t61_Vector2_t59,
	RuntimeInvoker_Vector2_t59_Vector2_t59_Single_t61,
	RuntimeInvoker_Boolean_t298_Vector2_t59_Vector2_t59,
	RuntimeInvoker_Vector2_t59_Vector3_t35,
	RuntimeInvoker_Vector3_t35_Vector2_t59,
	RuntimeInvoker_Void_t1089_Single_t61_Single_t61_Single_t61,
	RuntimeInvoker_Vector3_t35_Vector3_t35_Vector3_t35_Single_t61,
	RuntimeInvoker_Vector3_t35_Vector3_t35,
	RuntimeInvoker_Single_t61_Vector3_t35_Vector3_t35,
	RuntimeInvoker_Single_t61_Vector3_t35,
	RuntimeInvoker_Vector3_t35_Vector3_t35_Vector3_t35,
	RuntimeInvoker_Vector3_t35_Vector3_t35_Single_t61,
	RuntimeInvoker_Boolean_t298_Vector3_t35_Vector3_t35,
	RuntimeInvoker_Void_t1089_Single_t61_Single_t61_Single_t61_Single_t61,
	RuntimeInvoker_Color_t128_Color_t128_Color_t128_Single_t61,
	RuntimeInvoker_Color_t128_Color_t128_Single_t61,
	RuntimeInvoker_Vector4_t287_Color_t128,
	RuntimeInvoker_Void_t1089_SByte_t666_SByte_t666_SByte_t666_SByte_t666,
	RuntimeInvoker_Color32_t295_Color_t128,
	RuntimeInvoker_Color_t128_Color32_t295,
	RuntimeInvoker_Quaternion_t53,
	RuntimeInvoker_Single_t61_Quaternion_t53_Quaternion_t53,
	RuntimeInvoker_Quaternion_t53_Quaternion_t53,
	RuntimeInvoker_Quaternion_t53_QuaternionU26_t2734,
	RuntimeInvoker_Vector3_t35_Quaternion_t53_Vector3_t35,
	RuntimeInvoker_Boolean_t298_Quaternion_t53_Quaternion_t53,
	RuntimeInvoker_Boolean_t298_Vector3_t35,
	RuntimeInvoker_Boolean_t298_Rect_t184_Rect_t184,
	RuntimeInvoker_Single_t61_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Single_t61,
	RuntimeInvoker_Matrix4x4_t339_Matrix4x4_t339,
	RuntimeInvoker_Matrix4x4_t339_Matrix4x4U26_t2735,
	RuntimeInvoker_Boolean_t298_Matrix4x4_t339_Matrix4x4U26_t2735,
	RuntimeInvoker_Boolean_t298_Matrix4x4U26_t2735_Matrix4x4U26_t2735,
	RuntimeInvoker_Matrix4x4_t339,
	RuntimeInvoker_Vector4_t287_Int32_t56,
	RuntimeInvoker_Void_t1089_Int32_t56_Vector4_t287,
	RuntimeInvoker_Matrix4x4_t339_Vector3_t35,
	RuntimeInvoker_Void_t1089_Vector3_t35_Quaternion_t53_Vector3_t35,
	RuntimeInvoker_Matrix4x4_t339_Vector3_t35_Quaternion_t53_Vector3_t35,
	RuntimeInvoker_Matrix4x4_t339_Vector3U26_t2730_QuaternionU26_t2734_Vector3U26_t2730,
	RuntimeInvoker_Matrix4x4_t339_Single_t61_Single_t61_Single_t61_Single_t61_Single_t61_Single_t61,
	RuntimeInvoker_Matrix4x4_t339_Single_t61_Single_t61_Single_t61_Single_t61,
	RuntimeInvoker_Matrix4x4_t339_Matrix4x4_t339_Matrix4x4_t339,
	RuntimeInvoker_Vector4_t287_Matrix4x4_t339_Vector4_t287,
	RuntimeInvoker_Boolean_t298_Matrix4x4_t339_Matrix4x4_t339,
	RuntimeInvoker_Void_t1089_Vector3_t35_Vector3_t35,
	RuntimeInvoker_Void_t1089_Bounds_t207,
	RuntimeInvoker_Boolean_t298_Bounds_t207,
	RuntimeInvoker_Boolean_t298_Bounds_t207_Vector3_t35,
	RuntimeInvoker_Boolean_t298_BoundsU26_t2736_Vector3U26_t2730,
	RuntimeInvoker_Single_t61_Bounds_t207_Vector3_t35,
	RuntimeInvoker_Single_t61_BoundsU26_t2736_Vector3U26_t2730,
	RuntimeInvoker_Boolean_t298_RayU26_t2737_BoundsU26_t2736_SingleU26_t2738,
	RuntimeInvoker_Boolean_t298_Ray_t306,
	RuntimeInvoker_Boolean_t298_Ray_t306_SingleU26_t2738,
	RuntimeInvoker_Vector3_t35_BoundsU26_t2736_Vector3U26_t2730,
	RuntimeInvoker_Boolean_t298_Bounds_t207_Bounds_t207,
	RuntimeInvoker_Single_t61_Vector4_t287_Vector4_t287,
	RuntimeInvoker_Single_t61_Vector4_t287,
	RuntimeInvoker_Vector4_t287,
	RuntimeInvoker_Vector4_t287_Vector4_t287_Vector4_t287,
	RuntimeInvoker_Vector4_t287_Vector4_t287_Single_t61,
	RuntimeInvoker_Boolean_t298_Vector4_t287_Vector4_t287,
	RuntimeInvoker_Vector3_t35_Single_t61,
	RuntimeInvoker_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_Single_t61,
	RuntimeInvoker_Single_t61_Single_t61_Single_t61_Single_t61,
	RuntimeInvoker_Int32_t56_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Boolean_t298_Single_t61_Single_t61,
	RuntimeInvoker_Single_t61_Single_t61_Single_t61_SingleU26_t2738_Single_t61_Single_t61_Single_t61,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Int32_t56,
	RuntimeInvoker_Void_t1089_RectU26_t2731,
	RuntimeInvoker_Void_t1089_Int32_t56_Single_t61_Single_t61,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1089_Object_t_Single_t61,
	RuntimeInvoker_Void_t1089_SphericalHarmonicsL2U26_t2739,
	RuntimeInvoker_Void_t1089_Color_t128_SphericalHarmonicsL2U26_t2739,
	RuntimeInvoker_Void_t1089_ColorU26_t2727_SphericalHarmonicsL2U26_t2739,
	RuntimeInvoker_Void_t1089_Vector3_t35_Color_t128_Single_t61,
	RuntimeInvoker_Void_t1089_Vector3_t35_Color_t128_SphericalHarmonicsL2U26_t2739,
	RuntimeInvoker_Void_t1089_Vector3U26_t2730_ColorU26_t2727_SphericalHarmonicsL2U26_t2739,
	RuntimeInvoker_SphericalHarmonicsL2_t431_SphericalHarmonicsL2_t431_Single_t61,
	RuntimeInvoker_SphericalHarmonicsL2_t431_Single_t61_SphericalHarmonicsL2_t431,
	RuntimeInvoker_SphericalHarmonicsL2_t431_SphericalHarmonicsL2_t431_SphericalHarmonicsL2_t431,
	RuntimeInvoker_Boolean_t298_SphericalHarmonicsL2_t431_SphericalHarmonicsL2_t431,
	RuntimeInvoker_Void_t1089_Vector4U26_t2740,
	RuntimeInvoker_Vector4_t287_Object_t,
	RuntimeInvoker_Void_t1089_Object_t_Vector2U26_t2732,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_SByte_t666_Object_t,
	RuntimeInvoker_Object_t_Object_t_SByte_t666_SByte_t666_Object_t_SByte_t666,
	RuntimeInvoker_Boolean_t298_Object_t_SByte_t666,
	RuntimeInvoker_Boolean_t298_Object_t_Object_t,
	RuntimeInvoker_RuntimePlatform_t362,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Int32_t56_SByte_t666,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t56_Object_t_Object_t,
	RuntimeInvoker_CameraClearFlags_t550,
	RuntimeInvoker_Vector3_t35_Object_t_Vector3U26_t2730,
	RuntimeInvoker_Ray_t306_Vector3_t35,
	RuntimeInvoker_Ray_t306_Object_t_Vector3U26_t2730,
	RuntimeInvoker_Object_t_Ray_t306_Single_t61_Int32_t56,
	RuntimeInvoker_Object_t_Object_t_RayU26_t2737_Single_t61_Int32_t56,
	RuntimeInvoker_RenderBuffer_t549,
	RuntimeInvoker_Void_t1089_IntPtr_t_Int32U26_t2726_Int32U26_t2726,
	RuntimeInvoker_Void_t1089_IntPtr_t_RenderBufferU26_t2741_RenderBufferU26_t2741,
	RuntimeInvoker_Void_t1089_IntPtr_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_IntPtr_t_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_IntPtr_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_Int32_t56_Int32_t56_Int32U26_t2726_Int32U26_t2726,
	RuntimeInvoker_Void_t1089_Object_t_Single_t61_Single_t61,
	RuntimeInvoker_TouchPhase_t446,
	RuntimeInvoker_Void_t1089_Vector3U26_t2730,
	RuntimeInvoker_Touch_t282_Int32_t56,
	RuntimeInvoker_Object_t_Object_t_Vector3_t35_Quaternion_t53,
	RuntimeInvoker_Object_t_Object_t_Vector3U26_t2730_QuaternionU26_t2734,
	RuntimeInvoker_IntPtr_t,
	RuntimeInvoker_Object_t_Object_t_SByte_t666_SByte_t666_SByte_t666_SByte_t666_Object_t,
	RuntimeInvoker_Void_t1089_QuaternionU26_t2734,
	RuntimeInvoker_Void_t1089_Quaternion_t53,
	RuntimeInvoker_Void_t1089_Matrix4x4U26_t2735,
	RuntimeInvoker_Boolean_t298_Object_t_Int32_t56,
	RuntimeInvoker_Boolean_t298_Vector3_t35_Vector3_t35_RaycastHitU26_t2742_Single_t61_Int32_t56,
	RuntimeInvoker_Boolean_t298_Vector3U26_t2730_Vector3U26_t2730_RaycastHitU26_t2742_Single_t61_Int32_t56,
	RuntimeInvoker_Boolean_t298_Ray_t306_RaycastHitU26_t2742_Single_t61_Int32_t56,
	RuntimeInvoker_Object_t_Vector3_t35_Vector3_t35_Single_t61_Int32_t56,
	RuntimeInvoker_Object_t_Vector3U26_t2730_Vector3U26_t2730_Single_t61_Int32_t56,
	RuntimeInvoker_Void_t1089_Vector2_t59_Vector2_t59_Int32_t56_Single_t61_Single_t61_RaycastHit2DU26_t2723,
	RuntimeInvoker_Void_t1089_Vector2U26_t2732_Vector2U26_t2732_Int32_t56_Single_t61_Single_t61_RaycastHit2DU26_t2723,
	RuntimeInvoker_RaycastHit2D_t52_Vector2_t59_Vector2_t59_Int32_t56,
	RuntimeInvoker_RaycastHit2D_t52_Vector2_t59_Vector2_t59_Int32_t56_Single_t61_Single_t61,
	RuntimeInvoker_Void_t1089_Vector2_t59_Vector2_t59_Single_t61_Int32_t56_Single_t61_Single_t61_RaycastHit2DU26_t2723,
	RuntimeInvoker_Void_t1089_Vector2U26_t2732_Vector2U26_t2732_Single_t61_Int32_t56_Single_t61_Single_t61_RaycastHit2DU26_t2723,
	RuntimeInvoker_RaycastHit2D_t52_Vector2_t59_Vector2_t59_Single_t61_Int32_t56,
	RuntimeInvoker_RaycastHit2D_t52_Vector2_t59_Vector2_t59_Single_t61_Int32_t56_Single_t61_Single_t61,
	RuntimeInvoker_Object_t_Vector2_t59_Vector2_t59_Single_t61_Int32_t56,
	RuntimeInvoker_Object_t_Vector2U26_t2732_Vector2U26_t2732_Single_t61_Int32_t56_Single_t61_Single_t61,
	RuntimeInvoker_Object_t_SByte_t666_Object_t_Object_t,
	RuntimeInvoker_SendMessageOptions_t361,
	RuntimeInvoker_AnimatorStateInfo_t467,
	RuntimeInvoker_AnimatorClipInfo_t468,
	RuntimeInvoker_Boolean_t298_Object_t_Object_t_Color_t128_Int32_t56_Single_t61_Single_t61_Int32_t56_SByte_t666_SByte_t666_Int32_t56_Int32_t56_Int32_t56_Int32_t56_SByte_t666_Int32_t56_Vector2_t59_Vector2_t59_SByte_t666,
	RuntimeInvoker_Boolean_t298_Object_t_Object_t_Color_t128_Int32_t56_Single_t61_Single_t61_Int32_t56_SByte_t666_SByte_t666_Int32_t56_Int32_t56_Int32_t56_Int32_t56_SByte_t666_Int32_t56_Single_t61_Single_t61_Single_t61_Single_t61_SByte_t666,
	RuntimeInvoker_Boolean_t298_Object_t_Object_t_Object_t_ColorU26_t2727_Int32_t56_Single_t61_Single_t61_Int32_t56_SByte_t666_SByte_t666_Int32_t56_Int32_t56_Int32_t56_Int32_t56_SByte_t666_Int32_t56_Single_t61_Single_t61_Single_t61_Single_t61_SByte_t666,
	RuntimeInvoker_TextGenerationSettings_t290_TextGenerationSettings_t290,
	RuntimeInvoker_Single_t61_Object_t_TextGenerationSettings_t290,
	RuntimeInvoker_Boolean_t298_Object_t_TextGenerationSettings_t290,
	RuntimeInvoker_RenderMode_t482,
	RuntimeInvoker_Void_t1089_Object_t_ColorU26_t2727,
	RuntimeInvoker_Boolean_t298_Object_t_Vector2_t59_Object_t,
	RuntimeInvoker_Boolean_t298_Object_t_Vector2U26_t2732_Object_t,
	RuntimeInvoker_Vector2_t59_Vector2_t59_Object_t_Object_t,
	RuntimeInvoker_Void_t1089_Vector2_t59_Object_t_Object_t_Vector2U26_t2732,
	RuntimeInvoker_Void_t1089_Vector2U26_t2732_Object_t_Object_t_Vector2U26_t2732,
	RuntimeInvoker_Rect_t184_Object_t_Object_t,
	RuntimeInvoker_Boolean_t298_Object_t_Vector2_t59_Object_t_Vector3U26_t2730,
	RuntimeInvoker_Boolean_t298_Object_t_Vector2_t59_Object_t_Vector2U26_t2732,
	RuntimeInvoker_Ray_t306_Object_t_Vector2_t59,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_SByte_t666_SByte_t666,
	RuntimeInvoker_SourceID_t501,
	RuntimeInvoker_AppID_t500,
	RuntimeInvoker_Int32_t56_Object_t_Object_t_Object_t,
	RuntimeInvoker_UInt16_t652_Object_t_Object_t_Object_t,
	RuntimeInvoker_UInt64_t665_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t298_Object_t_Object_t_Object_t,
	RuntimeInvoker_UInt32_t654,
	RuntimeInvoker_NetworkID_t502,
	RuntimeInvoker_Void_t1089_UInt64_t665,
	RuntimeInvoker_NodeID_t503,
	RuntimeInvoker_Void_t1089_UInt16_t652,
	RuntimeInvoker_Object_t_UInt64_t665,
	RuntimeInvoker_Object_t_Object_t_Int32_t56_SByte_t666_Object_t_Object_t,
	RuntimeInvoker_Object_t_UInt64_t665_Object_t_Object_t,
	RuntimeInvoker_Object_t_UInt64_t665_Object_t,
	RuntimeInvoker_Object_t_UInt64_t665_UInt16_t652_Object_t,
	RuntimeInvoker_Object_t_Int32_t56_Int32_t56_Object_t_Object_t,
	RuntimeInvoker_Boolean_t298_Object_t_ObjectU26_t2743,
	RuntimeInvoker_Void_t1089_KeyValuePair_2_t618,
	RuntimeInvoker_Boolean_t298_KeyValuePair_2_t618,
	RuntimeInvoker_Object_t_Object_t_Int32U26_t2726_BooleanU26_t2725,
	RuntimeInvoker_Void_t1089_Object_t_Int32U26_t2726,
	RuntimeInvoker_Int32_t56_Object_t_Int32U26_t2726,
	RuntimeInvoker_Boolean_t298_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_SByte_t666_Int32_t56_Object_t,
	RuntimeInvoker_UserState_t565,
	RuntimeInvoker_Void_t1089_Object_t_Double_t661_SByte_t666_SByte_t666_DateTime_t396,
	RuntimeInvoker_Double_t661,
	RuntimeInvoker_Void_t1089_Double_t661,
	RuntimeInvoker_DateTime_t396,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t666_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Int64_t662,
	RuntimeInvoker_Void_t1089_Object_t_Int64_t662_Object_t_DateTime_t396_Object_t_Int32_t56,
	RuntimeInvoker_Int64_t662,
	RuntimeInvoker_Void_t1089_Int64_t662,
	RuntimeInvoker_UserScope_t566,
	RuntimeInvoker_Range_t559,
	RuntimeInvoker_Void_t1089_Range_t559,
	RuntimeInvoker_TimeScope_t567,
	RuntimeInvoker_Void_t1089_Int32_t56_HitInfo_t561,
	RuntimeInvoker_Boolean_t298_HitInfo_t561_HitInfo_t561,
	RuntimeInvoker_Boolean_t298_HitInfo_t561,
	RuntimeInvoker_Void_t1089_Object_t_StringU26_t2744_StringU26_t2744,
	RuntimeInvoker_Object_t_Object_t_SByte_t666,
	RuntimeInvoker_Void_t1089_Object_t_StreamingContext_t632,
	RuntimeInvoker_Void_t1089_Object_t_AnimatorStateInfo_t467_Int32_t56,
	RuntimeInvoker_Boolean_t298_Color_t128_Color_t128,
	RuntimeInvoker_Boolean_t298_TextGenerationSettings_t290,
	RuntimeInvoker_PersistentListenerMode_t581,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t56_Object_t,
	RuntimeInvoker_Void_t1089_Object_t_SByte_t666_Object_t,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56,
	RuntimeInvoker_Object_t_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_SByte_t666_Object_t_Object_t,
	RuntimeInvoker_UInt32_t654_Int32_t56,
	RuntimeInvoker_Object_t_Int32_t56_Object_t,
	RuntimeInvoker_UInt32_t654_Object_t_Int32_t56,
	RuntimeInvoker_Sign_t732_Object_t_Object_t,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_ConfidenceFactor_t736,
	RuntimeInvoker_Void_t1089_SByte_t666_Object_t,
	RuntimeInvoker_Byte_t647,
	RuntimeInvoker_Void_t1089_Object_t_Int32U26_t2726_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Int32U26_t2726_ByteU26_t2745_Int32U26_t2726_ByteU5BU5DU26_t2746,
	RuntimeInvoker_DateTime_t396_Object_t,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_Int32_t56,
	RuntimeInvoker_Boolean_t298_Object_t_Object_t_Object_t_Object_t_SByte_t666,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t56,
	RuntimeInvoker_Object_t_Object_t_DSAParameters_t871,
	RuntimeInvoker_RSAParameters_t842_SByte_t666,
	RuntimeInvoker_Void_t1089_RSAParameters_t842,
	RuntimeInvoker_Object_t_SByte_t666,
	RuntimeInvoker_DSAParameters_t871_BooleanU26_t2725,
	RuntimeInvoker_Object_t_Object_t_SByte_t666_Object_t_SByte_t666,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_SByte_t666,
	RuntimeInvoker_Boolean_t298_DateTime_t396,
	RuntimeInvoker_X509ChainStatusFlags_t773,
	RuntimeInvoker_Void_t1089_Byte_t647,
	RuntimeInvoker_Void_t1089_Byte_t647_Byte_t647,
	RuntimeInvoker_AlertLevel_t792,
	RuntimeInvoker_AlertDescription_t793,
	RuntimeInvoker_Object_t_Byte_t647,
	RuntimeInvoker_Void_t1089_Int16_t667_Object_t_Int32_t56_Int32_t56_Int32_t56_SByte_t666_SByte_t666_SByte_t666_SByte_t666_Int16_t667_SByte_t666_SByte_t666,
	RuntimeInvoker_CipherAlgorithmType_t795,
	RuntimeInvoker_HashAlgorithmType_t814,
	RuntimeInvoker_ExchangeAlgorithmType_t812,
	RuntimeInvoker_CipherMode_t724,
	RuntimeInvoker_Int16_t667,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int16_t667,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int64_t662,
	RuntimeInvoker_Void_t1089_Object_t_ByteU5BU5DU26_t2746_ByteU5BU5DU26_t2746,
	RuntimeInvoker_Object_t_Byte_t647_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t56,
	RuntimeInvoker_Object_t_Int16_t667,
	RuntimeInvoker_Int32_t56_Int16_t667,
	RuntimeInvoker_Object_t_Int16_t667_Object_t_Int32_t56_Int32_t56_Int32_t56_SByte_t666_SByte_t666_SByte_t666_SByte_t666_Int16_t667_SByte_t666_SByte_t666,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Object_t_Object_t,
	RuntimeInvoker_SecurityProtocolType_t829,
	RuntimeInvoker_SecurityCompressionType_t828,
	RuntimeInvoker_HandshakeType_t845,
	RuntimeInvoker_HandshakeState_t813,
	RuntimeInvoker_UInt64_t665,
	RuntimeInvoker_SecurityProtocolType_t829_Int16_t667,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t647_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t647_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1089_Byte_t647_Object_t,
	RuntimeInvoker_Object_t_Byte_t647_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_SByte_t666_Int32_t56,
	RuntimeInvoker_Object_t_Object_t_Int32_t56_Int32_t56_Object_t_Object_t,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Int64_t662_Int64_t662_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_Int32_t56_Int32_t56_SByte_t666_SByte_t666,
	RuntimeInvoker_Void_t1089_Byte_t647_Byte_t647_Object_t,
	RuntimeInvoker_RSAParameters_t842,
	RuntimeInvoker_Void_t1089_Object_t_Byte_t647,
	RuntimeInvoker_Void_t1089_Object_t_Byte_t647_Byte_t647,
	RuntimeInvoker_Void_t1089_Object_t_Byte_t647_Object_t,
	RuntimeInvoker_ContentType_t807,
	RuntimeInvoker_Object_t_Object_t_Int32_t56_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t2747,
	RuntimeInvoker_DictionaryEntry_t1068,
	RuntimeInvoker_EditorBrowsableState_t929,
	RuntimeInvoker_Boolean_t298_Object_t_Object_t_Object_t_Int32_t56,
	RuntimeInvoker_Int16_t667_Int16_t667,
	RuntimeInvoker_Boolean_t298_Object_t_IPAddressU26_t2748,
	RuntimeInvoker_AddressFamily_t934,
	RuntimeInvoker_Object_t_Int64_t662,
	RuntimeInvoker_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Boolean_t298_Object_t_Int32U26_t2726,
	RuntimeInvoker_Boolean_t298_Object_t_IPv6AddressU26_t2749,
	RuntimeInvoker_UInt16_t652_Int16_t667,
	RuntimeInvoker_SecurityProtocolType_t949,
	RuntimeInvoker_Void_t1089_SByte_t666_SByte_t666_Int32_t56_SByte_t666,
	RuntimeInvoker_AsnDecodeStatus_t990_Object_t,
	RuntimeInvoker_Object_t_Int32_t56_Object_t_SByte_t666,
	RuntimeInvoker_X509ChainStatusFlags_t978_Object_t,
	RuntimeInvoker_X509ChainStatusFlags_t978_Object_t_Int32_t56_SByte_t666,
	RuntimeInvoker_X509ChainStatusFlags_t978_Object_t_Object_t_SByte_t666,
	RuntimeInvoker_X509ChainStatusFlags_t978,
	RuntimeInvoker_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Int32U26_t2726_Int32_t56_Int32_t56,
	RuntimeInvoker_X509RevocationFlag_t985,
	RuntimeInvoker_X509RevocationMode_t986,
	RuntimeInvoker_X509VerificationFlags_t989,
	RuntimeInvoker_X509KeyUsageFlags_t983,
	RuntimeInvoker_X509KeyUsageFlags_t983_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_SByte_t666,
	RuntimeInvoker_Byte_t647_Int16_t667,
	RuntimeInvoker_Byte_t647_Int16_t667_Int16_t667,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t56_Int32_t56_SByte_t666,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_RegexOptions_t1004,
	RuntimeInvoker_Category_t1011_Object_t,
	RuntimeInvoker_Boolean_t298_UInt16_t652_Int16_t667,
	RuntimeInvoker_Boolean_t298_Int32_t56_Int16_t667,
	RuntimeInvoker_Void_t1089_Int16_t667_SByte_t666_SByte_t666_SByte_t666,
	RuntimeInvoker_Void_t1089_UInt16_t652_SByte_t666_SByte_t666,
	RuntimeInvoker_Void_t1089_Int16_t667_Int16_t667_SByte_t666_SByte_t666_SByte_t666,
	RuntimeInvoker_Void_t1089_Int16_t667_Object_t_SByte_t666_SByte_t666_SByte_t666,
	RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_SByte_t666_Object_t,
	RuntimeInvoker_Void_t1089_Int32_t56_SByte_t666_SByte_t666,
	RuntimeInvoker_Void_t1089_SByte_t666_Int32_t56_Object_t,
	RuntimeInvoker_UInt16_t652_UInt16_t652_UInt16_t652,
	RuntimeInvoker_OpFlags_t1006_SByte_t666_SByte_t666_SByte_t666_SByte_t666,
	RuntimeInvoker_Void_t1089_UInt16_t652_UInt16_t652,
	RuntimeInvoker_Boolean_t298_Int32_t56_Int32U26_t2726_Int32_t56,
	RuntimeInvoker_Boolean_t298_Int32_t56_Int32U26_t2726_Int32U26_t2726_SByte_t666,
	RuntimeInvoker_Boolean_t298_Int32U26_t2726_Int32_t56,
	RuntimeInvoker_Boolean_t298_UInt16_t652_Int32_t56,
	RuntimeInvoker_Boolean_t298_Int32_t56_Int32_t56_SByte_t666_Int32_t56,
	RuntimeInvoker_Void_t1089_Int32_t56_Int32U26_t2726_Int32U26_t2726,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_SByte_t666_Int32_t56,
	RuntimeInvoker_Interval_t1026,
	RuntimeInvoker_Boolean_t298_Interval_t1026,
	RuntimeInvoker_Void_t1089_Interval_t1026,
	RuntimeInvoker_Interval_t1026_Int32_t56,
	RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Object_t_Object_t,
	RuntimeInvoker_Double_t661_Interval_t1026,
	RuntimeInvoker_Object_t_Interval_t1026_Object_t_Object_t,
	RuntimeInvoker_Double_t661_Object_t,
	RuntimeInvoker_Int32_t56_Object_t_Int32U26_t2726_Int32_t56,
	RuntimeInvoker_Int32_t56_Object_t_Int32U26_t2726_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Object_t_Object_t_Int32U26_t2726,
	RuntimeInvoker_Object_t_RegexOptionsU26_t2750,
	RuntimeInvoker_Void_t1089_RegexOptionsU26_t2750_SByte_t666,
	RuntimeInvoker_Boolean_t298_Int32U26_t2726_Int32U26_t2726_Int32_t56,
	RuntimeInvoker_Category_t1011,
	RuntimeInvoker_Int32_t56_Int16_t667_Int32_t56_Int32_t56,
	RuntimeInvoker_Char_t326_Int16_t667,
	RuntimeInvoker_Int32_t56_Int32U26_t2726,
	RuntimeInvoker_Void_t1089_Int32U26_t2726_Int32U26_t2726,
	RuntimeInvoker_Void_t1089_Int32U26_t2726_Int32U26_t2726_Int32_t56,
	RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_SByte_t666,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_SByte_t666_SByte_t666,
	RuntimeInvoker_Void_t1089_UInt16_t652_SByte_t666,
	RuntimeInvoker_Void_t1089_Int16_t667_Int16_t667,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Object_t_SByte_t666,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_UInt16_t652,
	RuntimeInvoker_Position_t1007,
	RuntimeInvoker_UriHostNameType_t1059_Object_t,
	RuntimeInvoker_Void_t1089_StringU26_t2744,
	RuntimeInvoker_Object_t_Object_t_SByte_t666_SByte_t666_SByte_t666,
	RuntimeInvoker_Char_t326_Object_t_Int32U26_t2726_CharU26_t2751,
	RuntimeInvoker_Void_t1089_Object_t_UriFormatExceptionU26_t2752,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t56_Object_t_Object_t,
	RuntimeInvoker_Boolean_t298_Object_t_Object_t_ObjectU5BU5DU26_t2753,
	RuntimeInvoker_Int32_t56_Object_t_ObjectU5BU5DU26_t2753,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t666,
	RuntimeInvoker_Boolean_t298_Object_t_Object_t_SByte_t666,
	RuntimeInvoker_Byte_t647_Object_t,
	RuntimeInvoker_Decimal_t664_Object_t,
	RuntimeInvoker_Int16_t667_Object_t,
	RuntimeInvoker_Int64_t662_Object_t,
	RuntimeInvoker_SByte_t666_Object_t,
	RuntimeInvoker_UInt16_t652_Object_t,
	RuntimeInvoker_UInt32_t654_Object_t,
	RuntimeInvoker_UInt64_t665_Object_t,
	RuntimeInvoker_Boolean_t298_SByte_t666_Object_t_Int32_t56_ExceptionU26_t2754,
	RuntimeInvoker_Boolean_t298_Object_t_SByte_t666_Int32U26_t2726_ExceptionU26_t2754,
	RuntimeInvoker_Boolean_t298_Int32_t56_SByte_t666_ExceptionU26_t2754,
	RuntimeInvoker_Boolean_t298_Int32U26_t2726_Object_t_SByte_t666_SByte_t666_ExceptionU26_t2754,
	RuntimeInvoker_Void_t1089_Int32U26_t2726_Object_t_Object_t_BooleanU26_t2725_BooleanU26_t2725,
	RuntimeInvoker_Void_t1089_Int32U26_t2726_Object_t_Object_t_BooleanU26_t2725,
	RuntimeInvoker_Boolean_t298_Int32U26_t2726_Object_t_Int32U26_t2726_SByte_t666_ExceptionU26_t2754,
	RuntimeInvoker_Boolean_t298_Int32U26_t2726_Object_t_Object_t,
	RuntimeInvoker_Boolean_t298_Int16_t667_SByte_t666,
	RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_SByte_t666_Int32U26_t2726_ExceptionU26_t2754,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Object_t,
	RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_Int32U26_t2726,
	RuntimeInvoker_TypeCode_t1692,
	RuntimeInvoker_Int32_t56_Int64_t662,
	RuntimeInvoker_Boolean_t298_Int64_t662,
	RuntimeInvoker_Boolean_t298_Object_t_SByte_t666_Int64U26_t2755_ExceptionU26_t2754,
	RuntimeInvoker_Int64_t662_Object_t_Object_t,
	RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_SByte_t666_Int64U26_t2755_ExceptionU26_t2754,
	RuntimeInvoker_Int64_t662_Object_t_Int32_t56_Object_t,
	RuntimeInvoker_Boolean_t298_Object_t_Int64U26_t2755,
	RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_Int64U26_t2755,
	RuntimeInvoker_Boolean_t298_Object_t_SByte_t666_UInt32U26_t2756_ExceptionU26_t2754,
	RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_SByte_t666_UInt32U26_t2756_ExceptionU26_t2754,
	RuntimeInvoker_UInt32_t654_Object_t_Int32_t56_Object_t,
	RuntimeInvoker_UInt32_t654_Object_t_Object_t,
	RuntimeInvoker_Boolean_t298_Object_t_UInt32U26_t2756,
	RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_UInt32U26_t2756,
	RuntimeInvoker_UInt64_t665_Object_t_Object_t,
	RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_SByte_t666_UInt64U26_t2757_ExceptionU26_t2754,
	RuntimeInvoker_UInt64_t665_Object_t_Int32_t56_Object_t,
	RuntimeInvoker_Boolean_t298_Object_t_UInt64U26_t2757,
	RuntimeInvoker_Int32_t56_SByte_t666,
	RuntimeInvoker_Boolean_t298_SByte_t666,
	RuntimeInvoker_Byte_t647_Object_t_Object_t,
	RuntimeInvoker_Byte_t647_Object_t_Int32_t56_Object_t,
	RuntimeInvoker_Boolean_t298_Object_t_ByteU26_t2745,
	RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_ByteU26_t2745,
	RuntimeInvoker_Boolean_t298_Object_t_SByte_t666_SByteU26_t2758_ExceptionU26_t2754,
	RuntimeInvoker_SByte_t666_Object_t_Object_t,
	RuntimeInvoker_SByte_t666_Object_t_Int32_t56_Object_t,
	RuntimeInvoker_Boolean_t298_Object_t_SByteU26_t2758,
	RuntimeInvoker_Boolean_t298_Object_t_SByte_t666_Int16U26_t2759_ExceptionU26_t2754,
	RuntimeInvoker_Int16_t667_Object_t_Object_t,
	RuntimeInvoker_Int16_t667_Object_t_Int32_t56_Object_t,
	RuntimeInvoker_Boolean_t298_Object_t_Int16U26_t2759,
	RuntimeInvoker_UInt16_t652_Object_t_Object_t,
	RuntimeInvoker_UInt16_t652_Object_t_Int32_t56_Object_t,
	RuntimeInvoker_Boolean_t298_Object_t_UInt16U26_t2760,
	RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_UInt16U26_t2760,
	RuntimeInvoker_Void_t1089_ByteU2AU26_t2761_ByteU2AU26_t2761_DoubleU2AU26_t2762_UInt16U2AU26_t2763_UInt16U2AU26_t2763_UInt16U2AU26_t2763_UInt16U2AU26_t2763,
	RuntimeInvoker_UnicodeCategory_t1234_Int16_t667,
	RuntimeInvoker_Char_t326_Int16_t667_Object_t,
	RuntimeInvoker_Void_t1089_Int16_t667_Int32_t56,
	RuntimeInvoker_Char_t326_Int32_t56,
	RuntimeInvoker_Void_t1089_Int32_t56_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Object_t,
	RuntimeInvoker_Int32_t56_Object_t_Object_t_SByte_t666,
	RuntimeInvoker_Int32_t56_Object_t_Object_t_SByte_t666_Object_t,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Object_t_Int32_t56_Int32_t56_SByte_t666_Object_t,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_Int16_t667_Int32_t56,
	RuntimeInvoker_Object_t_Int32_t56_Int16_t667,
	RuntimeInvoker_Object_t_Int16_t667_Int16_t667,
	RuntimeInvoker_Void_t1089_Object_t_Int32U26_t2726_Int32U26_t2726_Int32U26_t2726_BooleanU26_t2725_StringU26_t2744,
	RuntimeInvoker_Void_t1089_Int32_t56_Int16_t667,
	RuntimeInvoker_Object_t_Object_t_Int32_t56_Int32_t56_Object_t,
	RuntimeInvoker_Object_t_Int16_t667_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Boolean_t298_Single_t61,
	RuntimeInvoker_Single_t61_Object_t_Object_t,
	RuntimeInvoker_Int32_t56_Double_t661,
	RuntimeInvoker_Boolean_t298_Double_t661,
	RuntimeInvoker_Double_t661_Object_t_Object_t,
	RuntimeInvoker_Double_t661_Object_t_Int32_t56_Object_t,
	RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_SByte_t666_DoubleU26_t2764_ExceptionU26_t2754,
	RuntimeInvoker_Boolean_t298_Object_t_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Boolean_t298_Object_t_DoubleU26_t2764,
	RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_DoubleU26_t2764,
	RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Int32_t56_SByte_t666_SByte_t666,
	RuntimeInvoker_Object_t_Decimal_t664,
	RuntimeInvoker_Decimal_t664_Decimal_t664_Decimal_t664,
	RuntimeInvoker_UInt64_t665_Decimal_t664,
	RuntimeInvoker_Int64_t662_Decimal_t664,
	RuntimeInvoker_Boolean_t298_Decimal_t664_Decimal_t664,
	RuntimeInvoker_Decimal_t664_Decimal_t664,
	RuntimeInvoker_Int32_t56_Decimal_t664_Decimal_t664,
	RuntimeInvoker_Int32_t56_Decimal_t664,
	RuntimeInvoker_Boolean_t298_Decimal_t664,
	RuntimeInvoker_Decimal_t664_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t56_Object_t_Int32U26_t2726_BooleanU26_t2725_BooleanU26_t2725_Int32U26_t2726_SByte_t666,
	RuntimeInvoker_Decimal_t664_Object_t_Int32_t56_Object_t,
	RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_DecimalU26_t2765_SByte_t666,
	RuntimeInvoker_Int32_t56_DecimalU26_t2765_UInt64U26_t2757,
	RuntimeInvoker_Int32_t56_DecimalU26_t2765_Int64U26_t2755,
	RuntimeInvoker_Int32_t56_DecimalU26_t2765_DecimalU26_t2765,
	RuntimeInvoker_Int32_t56_DecimalU26_t2765_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_DecimalU26_t2765_Int32_t56,
	RuntimeInvoker_Double_t661_DecimalU26_t2765,
	RuntimeInvoker_Void_t1089_DecimalU26_t2765_Int32_t56,
	RuntimeInvoker_Int32_t56_DecimalU26_t2765_DecimalU26_t2765_DecimalU26_t2765,
	RuntimeInvoker_Byte_t647_Decimal_t664,
	RuntimeInvoker_SByte_t666_Decimal_t664,
	RuntimeInvoker_Int16_t667_Decimal_t664,
	RuntimeInvoker_UInt16_t652_Decimal_t664,
	RuntimeInvoker_UInt32_t654_Decimal_t664,
	RuntimeInvoker_Decimal_t664_SByte_t666,
	RuntimeInvoker_Decimal_t664_Int16_t667,
	RuntimeInvoker_Decimal_t664_Int32_t56,
	RuntimeInvoker_Decimal_t664_Int64_t662,
	RuntimeInvoker_Decimal_t664_Single_t61,
	RuntimeInvoker_Decimal_t664_Double_t661,
	RuntimeInvoker_Single_t61_Decimal_t664,
	RuntimeInvoker_Double_t661_Decimal_t664,
	RuntimeInvoker_Boolean_t298_IntPtr_t_IntPtr_t,
	RuntimeInvoker_IntPtr_t_Int64_t662,
	RuntimeInvoker_IntPtr_t_Object_t,
	RuntimeInvoker_Int32_t56_IntPtr_t,
	RuntimeInvoker_Object_t_IntPtr_t,
	RuntimeInvoker_UInt64_t665_IntPtr_t,
	RuntimeInvoker_UInt32_t654_IntPtr_t,
	RuntimeInvoker_UIntPtr_t_Int64_t662,
	RuntimeInvoker_UIntPtr_t_Object_t,
	RuntimeInvoker_UIntPtr_t_Int32_t56,
	RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t2766,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t666,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t56_SByte_t666_SByte_t666,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t666_SByte_t666,
	RuntimeInvoker_Int32_t56_Object_t_Object_t_Object_t_SByte_t666,
	RuntimeInvoker_UInt64_t665_Object_t_Int32_t56,
	RuntimeInvoker_Object_t_Object_t_Int16_t667,
	RuntimeInvoker_Object_t_Object_t_Int64_t662,
	RuntimeInvoker_Int64_t662_Int32_t56,
	RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Object_t_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Object_t_Int64_t662_Int64_t662,
	RuntimeInvoker_Object_t_Int64_t662_Int64_t662_Int64_t662,
	RuntimeInvoker_Void_t1089_Object_t_Int64_t662_Int64_t662,
	RuntimeInvoker_Void_t1089_Object_t_Int64_t662_Int64_t662_Int64_t662,
	RuntimeInvoker_Object_t_Object_t_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Object_t,
	RuntimeInvoker_Void_t1089_Object_t_Int64_t662_Object_t_Int64_t662_Int64_t662,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Int64_t662,
	RuntimeInvoker_Int32_t56_Object_t_Object_t_Int32_t56,
	RuntimeInvoker_Int32_t56_Object_t_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Object_t,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Int32_t56_Int32_t56_Object_t,
	RuntimeInvoker_TypeAttributes_t1353,
	RuntimeInvoker_MemberTypes_t1332,
	RuntimeInvoker_RuntimeTypeHandle_t1090,
	RuntimeInvoker_Object_t_Object_t_SByte_t666_SByte_t666,
	RuntimeInvoker_TypeCode_t1692_Object_t,
	RuntimeInvoker_Object_t_RuntimeTypeHandle_t1090,
	RuntimeInvoker_RuntimeTypeHandle_t1090_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t56_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t56_Object_t_Int32_t56_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t56_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t56_Object_t_Int32_t56_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t56_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t56_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1089_SByte_t666_SByte_t666_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_RuntimeFieldHandle_t1092,
	RuntimeInvoker_Void_t1089_IntPtr_t_SByte_t666,
	RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Int32_t56_SByte_t666,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_ContractionU5BU5DU26_t2767_Level2MapU5BU5DU26_t2768,
	RuntimeInvoker_Void_t1089_Object_t_CodePointIndexerU26_t2769_ByteU2AU26_t2761_ByteU2AU26_t2761_CodePointIndexerU26_t2769_ByteU2AU26_t2761,
	RuntimeInvoker_Byte_t647_Int32_t56,
	RuntimeInvoker_Boolean_t298_Int32_t56_SByte_t666,
	RuntimeInvoker_Byte_t647_Int32_t56_Int32_t56,
	RuntimeInvoker_Boolean_t298_Int32_t56_Int32_t56,
	RuntimeInvoker_ExtenderType_t1135_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56,
	RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Object_t_Int32_t56,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_Int32_t56_BooleanU26_t2725_BooleanU26_t2725_SByte_t666,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_Int32_t56_BooleanU26_t2725_BooleanU26_t2725_SByte_t666_SByte_t666_ContextU26_t2770,
	RuntimeInvoker_Int32_t56_SByte_t666_SByte_t666,
	RuntimeInvoker_Boolean_t298_Object_t_Object_t_Int32_t56,
	RuntimeInvoker_Boolean_t298_Object_t_Object_t_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Boolean_t298_Object_t_Object_t_Int32_t56_Int32_t56_SByte_t666_ContextU26_t2770,
	RuntimeInvoker_Int32_t56_Object_t_Object_t_Int32_t56_Int32_t56_BooleanU26_t2725,
	RuntimeInvoker_Int32_t56_Object_t_Object_t_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int16_t667_Int32_t56_SByte_t666_ContextU26_t2770,
	RuntimeInvoker_Int32_t56_Object_t_Object_t_Int32_t56_Int32_t56_Object_t_ContextU26_t2770,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Int32_t56_Object_t_Int32_t56_SByte_t666_ContextU26_t2770,
	RuntimeInvoker_Boolean_t298_Object_t_Int32U26_t2726_Int32_t56_Int32_t56_Object_t_SByte_t666_ContextU26_t2770,
	RuntimeInvoker_Boolean_t298_Object_t_Int32U26_t2726_Int32_t56_Int32_t56_Object_t_SByte_t666_Int32_t56_ContractionU26_t2771_ContextU26_t2770,
	RuntimeInvoker_Boolean_t298_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_SByte_t666,
	RuntimeInvoker_Boolean_t298_Object_t_Int32U26_t2726_Int32_t56_Int32_t56_Int32_t56_Object_t_SByte_t666_ContextU26_t2770,
	RuntimeInvoker_Boolean_t298_Object_t_Int32U26_t2726_Int32_t56_Int32_t56_Int32_t56_Object_t_SByte_t666_Int32_t56_ContractionU26_t2771_ContextU26_t2770,
	RuntimeInvoker_Void_t1089_Int32_t56_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t666,
	RuntimeInvoker_Void_t1089_Int32_t56_Object_t_Int32_t56,
	RuntimeInvoker_Void_t1089_Int32_t56_Object_t_Object_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Object_t_SByte_t666,
	RuntimeInvoker_Void_t1089_SByte_t666_SByte_t666_SByte_t666_SByte_t666_SByte_t666_SByte_t666_SByte_t666_SByte_t666,
	RuntimeInvoker_Void_t1089_SByte_t666_ByteU5BU5DU26_t2746_Int32U26_t2726,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_SByte_t666,
	RuntimeInvoker_ConfidenceFactor_t1144,
	RuntimeInvoker_Sign_t1146_Object_t_Object_t,
	RuntimeInvoker_DSAParameters_t871_SByte_t666,
	RuntimeInvoker_Void_t1089_DSAParameters_t871,
	RuntimeInvoker_Int16_t667_Object_t_Int32_t56,
	RuntimeInvoker_Double_t661_Object_t_Int32_t56,
	RuntimeInvoker_Object_t_Int16_t667_SByte_t666,
	RuntimeInvoker_Void_t1089_Int32_t56_Single_t61_Object_t_Object_t,
	RuntimeInvoker_Void_t1089_Object_t_Single_t61_Object_t_Object_t,
	RuntimeInvoker_Void_t1089_Int32_t56_Single_t61_Object_t,
	RuntimeInvoker_Boolean_t298_Int32_t56_SByte_t666_MethodBaseU26_t2772_Int32U26_t2726_Int32U26_t2726_StringU26_t2744_Int32U26_t2726_Int32U26_t2726,
	RuntimeInvoker_Object_t_Object_t_Int32_t56_SByte_t666,
	RuntimeInvoker_Int32_t56_DateTime_t396,
	RuntimeInvoker_DayOfWeek_t1642_DateTime_t396,
	RuntimeInvoker_Int32_t56_Int32U26_t2726_Int32_t56_Int32_t56,
	RuntimeInvoker_DayOfWeek_t1642_Int32_t56,
	RuntimeInvoker_Void_t1089_Int32U26_t2726_Int32U26_t2726_Int32U26_t2726_Int32_t56,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_SByte_t666,
	RuntimeInvoker_Void_t1089_DateTime_t396_DateTime_t396_TimeSpan_t977,
	RuntimeInvoker_TimeSpan_t977,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Object_t_SByte_t666,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Int32U26_t2726,
	RuntimeInvoker_Decimal_t664,
	RuntimeInvoker_SByte_t666,
	RuntimeInvoker_UInt16_t652,
	RuntimeInvoker_Void_t1089_IntPtr_t_Int32_t56_SByte_t666_Int32_t56_SByte_t666_SByte_t666,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56_SByte_t666_SByte_t666,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56_SByte_t666_Int32_t56,
	RuntimeInvoker_Int32_t56_IntPtr_t_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_SByte_t666_SByte_t666,
	RuntimeInvoker_Boolean_t298_Object_t_MonoIOErrorU26_t2773,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t56_Int32_t56_MonoIOErrorU26_t2773,
	RuntimeInvoker_Object_t_MonoIOErrorU26_t2773,
	RuntimeInvoker_FileAttributes_t1244_Object_t_MonoIOErrorU26_t2773,
	RuntimeInvoker_MonoFileType_t1253_IntPtr_t_MonoIOErrorU26_t2773,
	RuntimeInvoker_Boolean_t298_Object_t_MonoIOStatU26_t2774_MonoIOErrorU26_t2773,
	RuntimeInvoker_IntPtr_t_Object_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56_MonoIOErrorU26_t2773,
	RuntimeInvoker_Boolean_t298_IntPtr_t_MonoIOErrorU26_t2773,
	RuntimeInvoker_Int32_t56_IntPtr_t_Object_t_Int32_t56_Int32_t56_MonoIOErrorU26_t2773,
	RuntimeInvoker_Int64_t662_IntPtr_t_Int64_t662_Int32_t56_MonoIOErrorU26_t2773,
	RuntimeInvoker_Int64_t662_IntPtr_t_MonoIOErrorU26_t2773,
	RuntimeInvoker_Boolean_t298_IntPtr_t_Int64_t662_MonoIOErrorU26_t2773,
	RuntimeInvoker_Void_t1089_Object_t_Int32_t56_Int32_t56_Object_t_Object_t_Object_t,
	RuntimeInvoker_CallingConventions_t1327,
	RuntimeInvoker_RuntimeMethodHandle_t1684,
	RuntimeInvoker_MethodAttributes_t1333,
	RuntimeInvoker_MethodToken_t1292,
	RuntimeInvoker_FieldAttributes_t1330,
	RuntimeInvoker_RuntimeFieldHandle_t1092,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Int32_t56_Object_t_Object_t,
	RuntimeInvoker_Void_t1089_OpCode_t1296,
	RuntimeInvoker_Void_t1089_OpCode_t1296_Object_t,
	RuntimeInvoker_StackBehaviour_t1301,
	RuntimeInvoker_PropertyAttributes_t1349,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Int32_t56_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t56_Int32_t56_Object_t,
	RuntimeInvoker_Object_t_Int32_t56_Int32_t56_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t56_SByte_t666_Object_t,
	RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t2726_ModuleU26_t2775,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t666_SByte_t666,
	RuntimeInvoker_AssemblyNameFlags_t1321,
	RuntimeInvoker_Object_t_Int32_t56_Object_t_ObjectU5BU5DU26_t2753_Object_t_Object_t_Object_t_ObjectU26_t2743,
	RuntimeInvoker_Void_t1089_ObjectU5BU5DU26_t2753_Object_t,
	RuntimeInvoker_Object_t_Int32_t56_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1089_Object_t_ObjectU5BU5DU26_t2753_Object_t,
	RuntimeInvoker_Object_t_Int32_t56_Object_t_Object_t_Object_t_SByte_t666,
	RuntimeInvoker_EventAttributes_t1328,
	RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Object_t_RuntimeFieldHandle_t1092,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_Object_t_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_Object_t_Int32_t56_Object_t,
	RuntimeInvoker_Object_t_StreamingContext_t632,
	RuntimeInvoker_Object_t_RuntimeMethodHandle_t1684,
	RuntimeInvoker_Void_t1089_Object_t_MonoEventInfoU26_t2776,
	RuntimeInvoker_MonoEventInfo_t1337_Object_t,
	RuntimeInvoker_Void_t1089_IntPtr_t_MonoMethodInfoU26_t2777,
	RuntimeInvoker_MonoMethodInfo_t1341_IntPtr_t,
	RuntimeInvoker_MethodAttributes_t1333_IntPtr_t,
	RuntimeInvoker_CallingConventions_t1327_IntPtr_t,
	RuntimeInvoker_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t2754,
	RuntimeInvoker_Void_t1089_Object_t_MonoPropertyInfoU26_t2778_Int32_t56,
	RuntimeInvoker_ParameterAttributes_t1345,
	RuntimeInvoker_GCHandle_t1374_Object_t_Int32_t56,
	RuntimeInvoker_Void_t1089_IntPtr_t_Int32_t56_Object_t_Int32_t56,
	RuntimeInvoker_Void_t1089_IntPtr_t_Object_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_BooleanU26_t2725,
	RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t2744,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t2744,
	RuntimeInvoker_Void_t1089_SByte_t666_Object_t_SByte_t666_SByte_t666,
	RuntimeInvoker_Void_t1089_TimeSpan_t977,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_SByte_t666_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t632_Object_t,
	RuntimeInvoker_Object_t_Object_t_StreamingContext_t632_ISurrogateSelectorU26_t2779,
	RuntimeInvoker_Void_t1089_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_TimeSpan_t977_Object_t,
	RuntimeInvoker_Object_t_StringU26_t2744,
	RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t2743,
	RuntimeInvoker_Boolean_t298_Object_t_StringU26_t2744_StringU26_t2744,
	RuntimeInvoker_WellKnownObjectMode_t1480,
	RuntimeInvoker_StreamingContext_t632,
	RuntimeInvoker_TypeFilterLevel_t1496,
	RuntimeInvoker_Void_t1089_Object_t_BooleanU26_t2725,
	RuntimeInvoker_Object_t_Byte_t647_Object_t_SByte_t666_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t647_Object_t_SByte_t666_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1089_Object_t_SByte_t666_ObjectU26_t2743_HeaderU5BU5DU26_t2780,
	RuntimeInvoker_Void_t1089_Byte_t647_Object_t_SByte_t666_ObjectU26_t2743_HeaderU5BU5DU26_t2780,
	RuntimeInvoker_Boolean_t298_Byte_t647_Object_t,
	RuntimeInvoker_Void_t1089_Byte_t647_Object_t_Int64U26_t2755_ObjectU26_t2743_SerializationInfoU26_t2781,
	RuntimeInvoker_Void_t1089_Object_t_SByte_t666_SByte_t666_Int64U26_t2755_ObjectU26_t2743_SerializationInfoU26_t2781,
	RuntimeInvoker_Void_t1089_Object_t_Int64U26_t2755_ObjectU26_t2743_SerializationInfoU26_t2781,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Int64_t662_ObjectU26_t2743_SerializationInfoU26_t2781,
	RuntimeInvoker_Void_t1089_Int64_t662_Object_t_Object_t_Int64_t662_Object_t_Object_t,
	RuntimeInvoker_Void_t1089_Object_t_Int64U26_t2755_ObjectU26_t2743,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Int64U26_t2755_ObjectU26_t2743,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Int64_t662_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1089_Int64_t662_Int64_t662_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int64_t662_Object_t,
	RuntimeInvoker_Object_t_Object_t_Byte_t647,
	RuntimeInvoker_Void_t1089_Int64_t662_Int32_t56_Int64_t662,
	RuntimeInvoker_Void_t1089_Int64_t662_Object_t_Int64_t662,
	RuntimeInvoker_Void_t1089_Object_t_Int64_t662_Object_t_Int64_t662_Object_t_Object_t,
	RuntimeInvoker_Boolean_t298_SByte_t666_Object_t_SByte_t666,
	RuntimeInvoker_Boolean_t298_Object_t_Object_t_StreamingContext_t632,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_StreamingContext_t632,
	RuntimeInvoker_Void_t1089_StreamingContext_t632,
	RuntimeInvoker_Object_t_StreamingContext_t632_Object_t_Object_t,
	RuntimeInvoker_Void_t1089_Object_t_Int16_t667,
	RuntimeInvoker_Void_t1089_Object_t_DateTime_t396,
	RuntimeInvoker_SerializationEntry_t1513,
	RuntimeInvoker_StreamingContextStates_t1516,
	RuntimeInvoker_CspProviderFlags_t1520,
	RuntimeInvoker_UInt32_t654_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Object_t_SByte_t666,
	RuntimeInvoker_Void_t1089_Int64_t662_Object_t_Int32_t56,
	RuntimeInvoker_UInt32_t654_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_UInt32U26_t2756_Int32_t56_UInt32U26_t2756_Int32_t56_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t,
	RuntimeInvoker_Void_t1089_Int64_t662_Int64_t662,
	RuntimeInvoker_UInt64_t665_Int64_t662_Int32_t56,
	RuntimeInvoker_UInt64_t665_Int64_t662_Int64_t662_Int64_t662,
	RuntimeInvoker_UInt64_t665_Int64_t662,
	RuntimeInvoker_PaddingMode_t727,
	RuntimeInvoker_Void_t1089_StringBuilderU26_t2782_Int32_t56,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t56,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_EncoderFallbackBufferU26_t2783_CharU5BU5DU26_t2784,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_DecoderFallbackBufferU26_t2785,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Object_t_Int32_t56,
	RuntimeInvoker_Boolean_t298_Int16_t667_Int32_t56,
	RuntimeInvoker_Boolean_t298_Int16_t667_Int16_t667_Int32_t56,
	RuntimeInvoker_Void_t1089_Int16_t667_Int16_t667_Int32_t56,
	RuntimeInvoker_Object_t_Int32U26_t2726,
	RuntimeInvoker_Object_t_Int32_t56_Object_t_Int32_t56,
	RuntimeInvoker_Void_t1089_SByte_t666_SByte_t666_SByte_t666,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_SByte_t666_Int32_t56_SByte_t666_SByte_t666,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_SByte_t666_Int32U26_t2726_BooleanU26_t2725_SByte_t666,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_Int32U26_t2726,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_CharU26_t2751_SByte_t666,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_CharU26_t2751_SByte_t666,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_CharU26_t2751_SByte_t666,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Object_t_Int32_t56_CharU26_t2751_SByte_t666,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Object_t_DecoderFallbackBufferU26_t2785_ByteU5BU5DU26_t2746_SByte_t666,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Int32_t56_Object_t_DecoderFallbackBufferU26_t2785_ByteU5BU5DU26_t2746_SByte_t666,
	RuntimeInvoker_Int32_t56_Object_t_DecoderFallbackBufferU26_t2785_ByteU5BU5DU26_t2746_Object_t_Int64_t662_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_DecoderFallbackBufferU26_t2785_ByteU5BU5DU26_t2746_Object_t_Int64_t662_Int32_t56_Object_t_Int32U26_t2726,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Object_t_Int32_t56_UInt32U26_t2756_UInt32U26_t2756_Object_t_DecoderFallbackBufferU26_t2785_ByteU5BU5DU26_t2746_SByte_t666,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Object_t_Int32_t56_UInt32U26_t2756_UInt32U26_t2756_Object_t_DecoderFallbackBufferU26_t2785_ByteU5BU5DU26_t2746_SByte_t666,
	RuntimeInvoker_Void_t1089_SByte_t666_Int32_t56,
	RuntimeInvoker_IntPtr_t_SByte_t666_Object_t_BooleanU26_t2725,
	RuntimeInvoker_Boolean_t298_IntPtr_t,
	RuntimeInvoker_IntPtr_t_SByte_t666_SByte_t666_Object_t_BooleanU26_t2725,
	RuntimeInvoker_Boolean_t298_TimeSpan_t977_TimeSpan_t977,
	RuntimeInvoker_Boolean_t298_Int64_t662_Int64_t662_SByte_t666,
	RuntimeInvoker_Boolean_t298_IntPtr_t_Int32_t56_SByte_t666,
	RuntimeInvoker_Int64_t662_Double_t661,
	RuntimeInvoker_Object_t_Double_t661,
	RuntimeInvoker_Int64_t662_Object_t_Int32_t56,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t56_Int32_t56,
	RuntimeInvoker_Byte_t647_SByte_t666,
	RuntimeInvoker_Byte_t647_Double_t661,
	RuntimeInvoker_Byte_t647_Single_t61,
	RuntimeInvoker_Byte_t647_Int64_t662,
	RuntimeInvoker_Char_t326_SByte_t666,
	RuntimeInvoker_Char_t326_Int64_t662,
	RuntimeInvoker_Char_t326_Single_t61,
	RuntimeInvoker_Char_t326_Object_t_Object_t,
	RuntimeInvoker_DateTime_t396_Object_t_Object_t,
	RuntimeInvoker_DateTime_t396_Int16_t667,
	RuntimeInvoker_DateTime_t396_Int32_t56,
	RuntimeInvoker_DateTime_t396_Int64_t662,
	RuntimeInvoker_DateTime_t396_Single_t61,
	RuntimeInvoker_DateTime_t396_SByte_t666,
	RuntimeInvoker_Double_t661_SByte_t666,
	RuntimeInvoker_Double_t661_Double_t661,
	RuntimeInvoker_Double_t661_Single_t61,
	RuntimeInvoker_Double_t661_Int32_t56,
	RuntimeInvoker_Double_t661_Int64_t662,
	RuntimeInvoker_Double_t661_Int16_t667,
	RuntimeInvoker_Int16_t667_SByte_t666,
	RuntimeInvoker_Int16_t667_Double_t661,
	RuntimeInvoker_Int16_t667_Single_t61,
	RuntimeInvoker_Int16_t667_Int32_t56,
	RuntimeInvoker_Int16_t667_Int64_t662,
	RuntimeInvoker_Int64_t662_SByte_t666,
	RuntimeInvoker_Int64_t662_Int16_t667,
	RuntimeInvoker_Int64_t662_Single_t61,
	RuntimeInvoker_Int64_t662_Int64_t662,
	RuntimeInvoker_SByte_t666_SByte_t666,
	RuntimeInvoker_SByte_t666_Int16_t667,
	RuntimeInvoker_SByte_t666_Double_t661,
	RuntimeInvoker_SByte_t666_Single_t61,
	RuntimeInvoker_SByte_t666_Int32_t56,
	RuntimeInvoker_SByte_t666_Int64_t662,
	RuntimeInvoker_Single_t61_SByte_t666,
	RuntimeInvoker_Single_t61_Double_t661,
	RuntimeInvoker_Single_t61_Int64_t662,
	RuntimeInvoker_Single_t61_Int16_t667,
	RuntimeInvoker_UInt16_t652_SByte_t666,
	RuntimeInvoker_UInt16_t652_Double_t661,
	RuntimeInvoker_UInt16_t652_Single_t61,
	RuntimeInvoker_UInt16_t652_Int32_t56,
	RuntimeInvoker_UInt16_t652_Int64_t662,
	RuntimeInvoker_UInt32_t654_SByte_t666,
	RuntimeInvoker_UInt32_t654_Int16_t667,
	RuntimeInvoker_UInt32_t654_Double_t661,
	RuntimeInvoker_UInt32_t654_Single_t61,
	RuntimeInvoker_UInt32_t654_Int64_t662,
	RuntimeInvoker_UInt64_t665_SByte_t666,
	RuntimeInvoker_UInt64_t665_Int16_t667,
	RuntimeInvoker_UInt64_t665_Double_t661,
	RuntimeInvoker_UInt64_t665_Single_t61,
	RuntimeInvoker_UInt64_t665_Int32_t56,
	RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_SByte_t666_TimeSpan_t977,
	RuntimeInvoker_Void_t1089_Int64_t662_Int32_t56,
	RuntimeInvoker_DayOfWeek_t1642,
	RuntimeInvoker_DateTimeKind_t1640,
	RuntimeInvoker_DateTime_t396_TimeSpan_t977,
	RuntimeInvoker_DateTime_t396_Double_t661,
	RuntimeInvoker_Int32_t56_DateTime_t396_DateTime_t396,
	RuntimeInvoker_DateTime_t396_DateTime_t396_Int32_t56,
	RuntimeInvoker_DateTime_t396_Object_t_Object_t_Int32_t56,
	RuntimeInvoker_Boolean_t298_Object_t_Object_t_Int32_t56_DateTimeU26_t2786_DateTimeOffsetU26_t2787_SByte_t666_ExceptionU26_t2754,
	RuntimeInvoker_Object_t_Object_t_SByte_t666_ExceptionU26_t2754,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Int32_t56_SByte_t666_SByte_t666_Int32U26_t2726,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Object_t_Object_t_SByte_t666_Int32U26_t2726,
	RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Int32_t56_Object_t_Int32U26_t2726,
	RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Int32_t56_Object_t_SByte_t666_Int32U26_t2726_Int32U26_t2726,
	RuntimeInvoker_Boolean_t298_Object_t_Int32_t56_Object_t_SByte_t666_Int32U26_t2726,
	RuntimeInvoker_Boolean_t298_Object_t_Object_t_Object_t_SByte_t666_DateTimeU26_t2786_DateTimeOffsetU26_t2787_Object_t_Int32_t56_SByte_t666_BooleanU26_t2725_BooleanU26_t2725,
	RuntimeInvoker_DateTime_t396_Object_t_Object_t_Object_t_Int32_t56,
	RuntimeInvoker_Boolean_t298_Object_t_Object_t_Object_t_Int32_t56_DateTimeU26_t2786_SByte_t666_BooleanU26_t2725_SByte_t666_ExceptionU26_t2754,
	RuntimeInvoker_DateTime_t396_DateTime_t396_TimeSpan_t977,
	RuntimeInvoker_Boolean_t298_DateTime_t396_DateTime_t396,
	RuntimeInvoker_Void_t1089_DateTime_t396_TimeSpan_t977,
	RuntimeInvoker_Void_t1089_Int64_t662_TimeSpan_t977,
	RuntimeInvoker_Int32_t56_DateTimeOffset_t679,
	RuntimeInvoker_Boolean_t298_DateTimeOffset_t679,
	RuntimeInvoker_DateTimeOffset_t679,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int16_t667,
	RuntimeInvoker_Object_t_Int16_t667_Object_t_BooleanU26_t2725_BooleanU26_t2725,
	RuntimeInvoker_Object_t_Int16_t667_Object_t_BooleanU26_t2725_BooleanU26_t2725_SByte_t666,
	RuntimeInvoker_Object_t_DateTime_t396_Object_t_Object_t,
	RuntimeInvoker_Object_t_DateTime_t396_Nullable_1_t1741_Object_t_Object_t,
	RuntimeInvoker_Void_t1089_MonoEnumInfo_t1653,
	RuntimeInvoker_Void_t1089_Object_t_MonoEnumInfoU26_t2788,
	RuntimeInvoker_Int32_t56_Int16_t667_Int16_t667,
	RuntimeInvoker_Int32_t56_Int64_t662_Int64_t662,
	RuntimeInvoker_PlatformID_t1681,
	RuntimeInvoker_Void_t1089_Int32_t56_Int16_t667_Int16_t667_SByte_t666_SByte_t666_SByte_t666_SByte_t666_SByte_t666_SByte_t666_SByte_t666_SByte_t666,
	RuntimeInvoker_Int32_t56_Guid_t680,
	RuntimeInvoker_Boolean_t298_Guid_t680,
	RuntimeInvoker_Guid_t680,
	RuntimeInvoker_Object_t_SByte_t666_SByte_t666_SByte_t666,
	RuntimeInvoker_Double_t661_Double_t661_Double_t661,
	RuntimeInvoker_TypeAttributes_t1353_Object_t,
	RuntimeInvoker_Object_t_SByte_t666_SByte_t666,
	RuntimeInvoker_Void_t1089_UInt64U2AU26_t2789_Int32U2AU26_t2790_CharU2AU26_t2791_CharU2AU26_t2791_Int64U2AU26_t2792_Int32U2AU26_t2790,
	RuntimeInvoker_Void_t1089_Int32_t56_Int64_t662,
	RuntimeInvoker_Void_t1089_Object_t_Double_t661_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Decimal_t664,
	RuntimeInvoker_Object_t_Object_t_SByte_t666_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int16_t667_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t56_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int64_t662_Object_t,
	RuntimeInvoker_Object_t_Object_t_Single_t61_Object_t,
	RuntimeInvoker_Object_t_Object_t_Double_t661_Object_t,
	RuntimeInvoker_Object_t_Object_t_Decimal_t664_Object_t,
	RuntimeInvoker_Object_t_Single_t61_Object_t,
	RuntimeInvoker_Object_t_Double_t661_Object_t,
	RuntimeInvoker_Void_t1089_Object_t_BooleanU26_t2725_SByte_t666_Int32U26_t2726_Int32U26_t2726,
	RuntimeInvoker_Object_t_Object_t_Int32_t56_Int32_t56_Object_t_SByte_t666_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1089_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_Int64_t662_Int32_t56_Int32_t56_Int32_t56_Int32_t56_Int32_t56,
	RuntimeInvoker_TimeSpan_t977_TimeSpan_t977,
	RuntimeInvoker_Int32_t56_TimeSpan_t977_TimeSpan_t977,
	RuntimeInvoker_Int32_t56_TimeSpan_t977,
	RuntimeInvoker_Boolean_t298_TimeSpan_t977,
	RuntimeInvoker_TimeSpan_t977_Double_t661,
	RuntimeInvoker_TimeSpan_t977_Double_t661_Int64_t662,
	RuntimeInvoker_TimeSpan_t977_TimeSpan_t977_TimeSpan_t977,
	RuntimeInvoker_TimeSpan_t977_DateTime_t396,
	RuntimeInvoker_Boolean_t298_DateTime_t396_Object_t,
	RuntimeInvoker_DateTime_t396_DateTime_t396,
	RuntimeInvoker_TimeSpan_t977_DateTime_t396_TimeSpan_t977,
	RuntimeInvoker_Boolean_t298_Int32_t56_Int64U5BU5DU26_t2793_StringU5BU5DU26_t2794,
	RuntimeInvoker_Boolean_t298_ObjectU26_t2743_Object_t,
	RuntimeInvoker_Void_t1089_ObjectU26_t2743_Object_t,
	RuntimeInvoker_Void_t1089_KeyValuePair_2_t1934,
	RuntimeInvoker_Boolean_t298_KeyValuePair_2_t1934,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t1828,
	RuntimeInvoker_Void_t1089_Int32_t56_ObjectU26_t2743,
	RuntimeInvoker_Void_t1089_ObjectU5BU5DU26_t2753_Int32_t56,
	RuntimeInvoker_Void_t1089_ObjectU5BU5DU26_t2753_Int32_t56_Int32_t56,
	RuntimeInvoker_KeyValuePair_2_t1934_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t1938,
	RuntimeInvoker_DictionaryEntry_t1068_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1934,
	RuntimeInvoker_Enumerator_t1937,
	RuntimeInvoker_Enumerator_t1941,
	RuntimeInvoker_Int32_t56_Int32_t56_Int32_t56_Object_t,
	RuntimeInvoker_Enumerator_t1802,
	RuntimeInvoker_Enumerator_t1885,
	RuntimeInvoker_Enumerator_t1882,
	RuntimeInvoker_KeyValuePair_2_t1877,
	RuntimeInvoker_Void_t1089_ColorTween_t127,
	RuntimeInvoker_Boolean_t298_TypeU26_t2795_Int32_t56,
	RuntimeInvoker_Boolean_t298_BooleanU26_t2725_SByte_t666,
	RuntimeInvoker_Boolean_t298_FillMethodU26_t2796_Int32_t56,
	RuntimeInvoker_Boolean_t298_SingleU26_t2738_Single_t61,
	RuntimeInvoker_Boolean_t298_ContentTypeU26_t2797_Int32_t56,
	RuntimeInvoker_Boolean_t298_LineTypeU26_t2798_Int32_t56,
	RuntimeInvoker_Boolean_t298_InputTypeU26_t2799_Int32_t56,
	RuntimeInvoker_Boolean_t298_TouchScreenKeyboardTypeU26_t2800_Int32_t56,
	RuntimeInvoker_Boolean_t298_CharacterValidationU26_t2801_Int32_t56,
	RuntimeInvoker_Boolean_t298_CharU26_t2751_Int16_t667,
	RuntimeInvoker_Boolean_t298_DirectionU26_t2802_Int32_t56,
	RuntimeInvoker_Boolean_t298_NavigationU26_t2803_Navigation_t194,
	RuntimeInvoker_Boolean_t298_TransitionU26_t2804_Int32_t56,
	RuntimeInvoker_Boolean_t298_ColorBlockU26_t2805_ColorBlock_t140,
	RuntimeInvoker_Boolean_t298_SpriteStateU26_t2806_SpriteState_t211,
	RuntimeInvoker_Boolean_t298_DirectionU26_t2807_Int32_t56,
	RuntimeInvoker_Boolean_t298_AspectModeU26_t2808_Int32_t56,
	RuntimeInvoker_Boolean_t298_FitModeU26_t2809_Int32_t56,
	RuntimeInvoker_Void_t1089_CornerU26_t2810_Int32_t56,
	RuntimeInvoker_Void_t1089_AxisU26_t2811_Int32_t56,
	RuntimeInvoker_Void_t1089_Vector2U26_t2732_Vector2_t59,
	RuntimeInvoker_Void_t1089_ConstraintU26_t2812_Int32_t56,
	RuntimeInvoker_Void_t1089_Int32U26_t2726_Int32_t56,
	RuntimeInvoker_Void_t1089_SingleU26_t2738_Single_t61,
	RuntimeInvoker_Void_t1089_BooleanU26_t2725_SByte_t666,
	RuntimeInvoker_Void_t1089_TextAnchorU26_t2813_Int32_t56,
	RuntimeInvoker_Void_t1089_Object_t_Object_t_Single_t61,
	RuntimeInvoker_Vector3_t35_Int32_t56,
	RuntimeInvoker_Int32_t56_Vector3_t35,
	RuntimeInvoker_Void_t1089_Int32_t56_Vector3_t35,
	RuntimeInvoker_Void_t1089_Vector3U5BU5DU26_t2814_Int32_t56,
	RuntimeInvoker_Void_t1089_Vector3U5BU5DU26_t2814_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_Object_t_Vector3_t35_Int32_t56_Int32_t56,
	RuntimeInvoker_Void_t1089_Int32_t56_Double_t661,
	RuntimeInvoker_Int32_t56_Vector3_t35_Vector3_t35_Object_t,
	RuntimeInvoker_RaycastResult_t103_Int32_t56,
	RuntimeInvoker_Boolean_t298_RaycastResult_t103,
	RuntimeInvoker_Int32_t56_RaycastResult_t103,
	RuntimeInvoker_Void_t1089_Int32_t56_RaycastResult_t103,
	RuntimeInvoker_Void_t1089_RaycastResultU5BU5DU26_t2815_Int32_t56,
	RuntimeInvoker_Void_t1089_RaycastResultU5BU5DU26_t2815_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_Object_t_RaycastResult_t103_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_RaycastResult_t103_RaycastResult_t103_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1877_Int32_t56,
	RuntimeInvoker_Void_t1089_KeyValuePair_2_t1877,
	RuntimeInvoker_Boolean_t298_KeyValuePair_2_t1877,
	RuntimeInvoker_Int32_t56_KeyValuePair_2_t1877,
	RuntimeInvoker_Void_t1089_Int32_t56_KeyValuePair_2_t1877,
	RuntimeInvoker_Link_t1188_Int32_t56,
	RuntimeInvoker_Void_t1089_Link_t1188,
	RuntimeInvoker_Boolean_t298_Link_t1188,
	RuntimeInvoker_Int32_t56_Link_t1188,
	RuntimeInvoker_Void_t1089_Int32_t56_Link_t1188,
	RuntimeInvoker_DictionaryEntry_t1068_Int32_t56,
	RuntimeInvoker_Void_t1089_DictionaryEntry_t1068,
	RuntimeInvoker_Boolean_t298_DictionaryEntry_t1068,
	RuntimeInvoker_Int32_t56_DictionaryEntry_t1068,
	RuntimeInvoker_Void_t1089_Int32_t56_DictionaryEntry_t1068,
	RuntimeInvoker_RaycastHit2D_t52_Int32_t56,
	RuntimeInvoker_Void_t1089_RaycastHit2D_t52,
	RuntimeInvoker_Boolean_t298_RaycastHit2D_t52,
	RuntimeInvoker_Int32_t56_RaycastHit2D_t52,
	RuntimeInvoker_Void_t1089_Int32_t56_RaycastHit2D_t52,
	RuntimeInvoker_RaycastHit_t283_Int32_t56,
	RuntimeInvoker_Void_t1089_RaycastHit_t283,
	RuntimeInvoker_Boolean_t298_RaycastHit_t283,
	RuntimeInvoker_Int32_t56_RaycastHit_t283,
	RuntimeInvoker_Void_t1089_Int32_t56_RaycastHit_t283,
	RuntimeInvoker_KeyValuePair_2_t1908_Int32_t56,
	RuntimeInvoker_Void_t1089_KeyValuePair_2_t1908,
	RuntimeInvoker_Boolean_t298_KeyValuePair_2_t1908,
	RuntimeInvoker_Int32_t56_KeyValuePair_2_t1908,
	RuntimeInvoker_Void_t1089_Int32_t56_KeyValuePair_2_t1908,
	RuntimeInvoker_KeyValuePair_2_t1934_Int32_t56,
	RuntimeInvoker_Int32_t56_KeyValuePair_2_t1934,
	RuntimeInvoker_Void_t1089_Int32_t56_KeyValuePair_2_t1934,
	RuntimeInvoker_Void_t1089_UIVertexU5BU5DU26_t2816_Int32_t56,
	RuntimeInvoker_Void_t1089_UIVertexU5BU5DU26_t2816_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_Object_t_UIVertex_t191_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_UIVertex_t191_UIVertex_t191_Object_t,
	RuntimeInvoker_Boolean_t298_Vector2_t59,
	RuntimeInvoker_Void_t1089_Int32_t56_Vector2_t59,
	RuntimeInvoker_ContentType_t170_Int32_t56,
	RuntimeInvoker_UILineInfo_t331_Int32_t56,
	RuntimeInvoker_Void_t1089_UILineInfo_t331,
	RuntimeInvoker_Boolean_t298_UILineInfo_t331,
	RuntimeInvoker_Int32_t56_UILineInfo_t331,
	RuntimeInvoker_Void_t1089_Int32_t56_UILineInfo_t331,
	RuntimeInvoker_UICharInfo_t333_Int32_t56,
	RuntimeInvoker_Void_t1089_UICharInfo_t333,
	RuntimeInvoker_Boolean_t298_UICharInfo_t333,
	RuntimeInvoker_Int32_t56_UICharInfo_t333,
	RuntimeInvoker_Void_t1089_Int32_t56_UICharInfo_t333,
	RuntimeInvoker_GcAchievementData_t546_Int32_t56,
	RuntimeInvoker_Void_t1089_GcAchievementData_t546,
	RuntimeInvoker_Boolean_t298_GcAchievementData_t546,
	RuntimeInvoker_Int32_t56_GcAchievementData_t546,
	RuntimeInvoker_Void_t1089_Int32_t56_GcAchievementData_t546,
	RuntimeInvoker_GcScoreData_t547_Int32_t56,
	RuntimeInvoker_Boolean_t298_GcScoreData_t547,
	RuntimeInvoker_Int32_t56_GcScoreData_t547,
	RuntimeInvoker_Void_t1089_Int32_t56_GcScoreData_t547,
	RuntimeInvoker_Void_t1089_Int32_t56_IntPtr_t,
	RuntimeInvoker_Keyframe_t469_Int32_t56,
	RuntimeInvoker_Void_t1089_Keyframe_t469,
	RuntimeInvoker_Boolean_t298_Keyframe_t469,
	RuntimeInvoker_Int32_t56_Keyframe_t469,
	RuntimeInvoker_Void_t1089_Int32_t56_Keyframe_t469,
	RuntimeInvoker_Void_t1089_UICharInfoU5BU5DU26_t2817_Int32_t56,
	RuntimeInvoker_Void_t1089_UICharInfoU5BU5DU26_t2817_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_Object_t_UICharInfo_t333_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_UICharInfo_t333_UICharInfo_t333_Object_t,
	RuntimeInvoker_Void_t1089_UILineInfoU5BU5DU26_t2818_Int32_t56,
	RuntimeInvoker_Void_t1089_UILineInfoU5BU5DU26_t2818_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_Object_t_UILineInfo_t331_Int32_t56_Int32_t56,
	RuntimeInvoker_Int32_t56_UILineInfo_t331_UILineInfo_t331_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2118_Int32_t56,
	RuntimeInvoker_Void_t1089_KeyValuePair_2_t2118,
	RuntimeInvoker_Boolean_t298_KeyValuePair_2_t2118,
	RuntimeInvoker_Int32_t56_KeyValuePair_2_t2118,
	RuntimeInvoker_Void_t1089_Int32_t56_KeyValuePair_2_t2118,
	RuntimeInvoker_KeyValuePair_2_t2155_Int32_t56,
	RuntimeInvoker_Void_t1089_KeyValuePair_2_t2155,
	RuntimeInvoker_Boolean_t298_KeyValuePair_2_t2155,
	RuntimeInvoker_Int32_t56_KeyValuePair_2_t2155,
	RuntimeInvoker_Void_t1089_Int32_t56_KeyValuePair_2_t2155,
	RuntimeInvoker_NetworkID_t502_Int32_t56,
	RuntimeInvoker_Boolean_t298_UInt64_t665,
	RuntimeInvoker_Int32_t56_UInt64_t665,
	RuntimeInvoker_Void_t1089_Int32_t56_UInt64_t665,
	RuntimeInvoker_KeyValuePair_2_t2175_Int32_t56,
	RuntimeInvoker_Void_t1089_KeyValuePair_2_t2175,
	RuntimeInvoker_Boolean_t298_KeyValuePair_2_t2175,
	RuntimeInvoker_Int32_t56_KeyValuePair_2_t2175,
	RuntimeInvoker_Void_t1089_Int32_t56_KeyValuePair_2_t2175,
	RuntimeInvoker_ParameterModifier_t1346_Int32_t56,
	RuntimeInvoker_Void_t1089_ParameterModifier_t1346,
	RuntimeInvoker_Boolean_t298_ParameterModifier_t1346,
	RuntimeInvoker_Int32_t56_ParameterModifier_t1346,
	RuntimeInvoker_Void_t1089_Int32_t56_ParameterModifier_t1346,
	RuntimeInvoker_HitInfo_t561_Int32_t56,
	RuntimeInvoker_Void_t1089_HitInfo_t561,
	RuntimeInvoker_Int32_t56_HitInfo_t561,
	RuntimeInvoker_KeyValuePair_2_t2238_Int32_t56,
	RuntimeInvoker_Void_t1089_KeyValuePair_2_t2238,
	RuntimeInvoker_Boolean_t298_KeyValuePair_2_t2238,
	RuntimeInvoker_Int32_t56_KeyValuePair_2_t2238,
	RuntimeInvoker_Void_t1089_Int32_t56_KeyValuePair_2_t2238,
	RuntimeInvoker_TextEditOp_t579_Int32_t56,
	RuntimeInvoker_ClientCertificateType_t844_Int32_t56,
	RuntimeInvoker_KeyValuePair_2_t2289_Int32_t56,
	RuntimeInvoker_Void_t1089_KeyValuePair_2_t2289,
	RuntimeInvoker_Boolean_t298_KeyValuePair_2_t2289,
	RuntimeInvoker_Int32_t56_KeyValuePair_2_t2289,
	RuntimeInvoker_Void_t1089_Int32_t56_KeyValuePair_2_t2289,
	RuntimeInvoker_X509ChainStatus_t974_Int32_t56,
	RuntimeInvoker_Void_t1089_X509ChainStatus_t974,
	RuntimeInvoker_Boolean_t298_X509ChainStatus_t974,
	RuntimeInvoker_Int32_t56_X509ChainStatus_t974,
	RuntimeInvoker_Void_t1089_Int32_t56_X509ChainStatus_t974,
	RuntimeInvoker_KeyValuePair_2_t2312_Int32_t56,
	RuntimeInvoker_Void_t1089_KeyValuePair_2_t2312,
	RuntimeInvoker_Boolean_t298_KeyValuePair_2_t2312,
	RuntimeInvoker_Int32_t56_KeyValuePair_2_t2312,
	RuntimeInvoker_Void_t1089_Int32_t56_KeyValuePair_2_t2312,
	RuntimeInvoker_Int32_t56_Object_t_Int32_t56_Int32_t56_Int32_t56_Object_t,
	RuntimeInvoker_Mark_t1019_Int32_t56,
	RuntimeInvoker_Void_t1089_Mark_t1019,
	RuntimeInvoker_Boolean_t298_Mark_t1019,
	RuntimeInvoker_Int32_t56_Mark_t1019,
	RuntimeInvoker_Void_t1089_Int32_t56_Mark_t1019,
	RuntimeInvoker_UriScheme_t1056_Int32_t56,
	RuntimeInvoker_Void_t1089_UriScheme_t1056,
	RuntimeInvoker_Boolean_t298_UriScheme_t1056,
	RuntimeInvoker_Int32_t56_UriScheme_t1056,
	RuntimeInvoker_Void_t1089_Int32_t56_UriScheme_t1056,
	RuntimeInvoker_TableRange_t1121_Int32_t56,
	RuntimeInvoker_Void_t1089_TableRange_t1121,
	RuntimeInvoker_Boolean_t298_TableRange_t1121,
	RuntimeInvoker_Int32_t56_TableRange_t1121,
	RuntimeInvoker_Void_t1089_Int32_t56_TableRange_t1121,
	RuntimeInvoker_Slot_t1198_Int32_t56,
	RuntimeInvoker_Void_t1089_Slot_t1198,
	RuntimeInvoker_Boolean_t298_Slot_t1198,
	RuntimeInvoker_Int32_t56_Slot_t1198,
	RuntimeInvoker_Void_t1089_Int32_t56_Slot_t1198,
	RuntimeInvoker_Slot_t1205_Int32_t56,
	RuntimeInvoker_Void_t1089_Slot_t1205,
	RuntimeInvoker_Boolean_t298_Slot_t1205,
	RuntimeInvoker_Int32_t56_Slot_t1205,
	RuntimeInvoker_Void_t1089_Int32_t56_Slot_t1205,
	RuntimeInvoker_ILTokenInfo_t1283_Int32_t56,
	RuntimeInvoker_Void_t1089_ILTokenInfo_t1283,
	RuntimeInvoker_Boolean_t298_ILTokenInfo_t1283,
	RuntimeInvoker_Int32_t56_ILTokenInfo_t1283,
	RuntimeInvoker_Void_t1089_Int32_t56_ILTokenInfo_t1283,
	RuntimeInvoker_LabelData_t1285_Int32_t56,
	RuntimeInvoker_Void_t1089_LabelData_t1285,
	RuntimeInvoker_Boolean_t298_LabelData_t1285,
	RuntimeInvoker_Int32_t56_LabelData_t1285,
	RuntimeInvoker_Void_t1089_Int32_t56_LabelData_t1285,
	RuntimeInvoker_LabelFixup_t1284_Int32_t56,
	RuntimeInvoker_Void_t1089_LabelFixup_t1284,
	RuntimeInvoker_Boolean_t298_LabelFixup_t1284,
	RuntimeInvoker_Int32_t56_LabelFixup_t1284,
	RuntimeInvoker_Void_t1089_Int32_t56_LabelFixup_t1284,
	RuntimeInvoker_Void_t1089_Int32_t56_DateTime_t396,
	RuntimeInvoker_Void_t1089_Decimal_t664,
	RuntimeInvoker_Void_t1089_Int32_t56_Decimal_t664,
	RuntimeInvoker_TimeSpan_t977_Int32_t56,
	RuntimeInvoker_Void_t1089_Int32_t56_TimeSpan_t977,
	RuntimeInvoker_TypeTag_t1484_Int32_t56,
	RuntimeInvoker_Boolean_t298_Byte_t647,
	RuntimeInvoker_Int32_t56_Byte_t647,
	RuntimeInvoker_Void_t1089_Int32_t56_Byte_t647,
	RuntimeInvoker_Vector3_t35_Object_t,
	RuntimeInvoker_Enumerator_t1785,
	RuntimeInvoker_Object_t_Vector3_t35_Object_t_Object_t,
	RuntimeInvoker_Int32_t56_Vector3_t35_Vector3_t35,
	RuntimeInvoker_Object_t_Vector3_t35_Vector3_t35_Object_t_Object_t,
	RuntimeInvoker_Object_t_RaycastResult_t103_RaycastResult_t103_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t1837,
	RuntimeInvoker_Boolean_t298_RaycastResult_t103_RaycastResult_t103,
	RuntimeInvoker_Object_t_RaycastResult_t103_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1877_Int32_t56_Object_t,
	RuntimeInvoker_Boolean_t298_Int32_t56_ObjectU26_t2743,
	RuntimeInvoker_DictionaryEntry_t1068_Int32_t56_Object_t,
	RuntimeInvoker_Link_t1188,
	RuntimeInvoker_Enumerator_t1881,
	RuntimeInvoker_DictionaryEntry_t1068_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1877_Object_t,
	RuntimeInvoker_RaycastHit2D_t52,
	RuntimeInvoker_Object_t_RaycastHit_t283_RaycastHit_t283_Object_t_Object_t,
	RuntimeInvoker_RaycastHit_t283,
	RuntimeInvoker_Object_t_Color_t128_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1908_Object_t_Int32_t56,
	RuntimeInvoker_Enumerator_t1912,
	RuntimeInvoker_DictionaryEntry_t1068_Object_t_Int32_t56,
	RuntimeInvoker_KeyValuePair_2_t1908,
	RuntimeInvoker_Enumerator_t1911,
	RuntimeInvoker_Enumerator_t1915,
	RuntimeInvoker_KeyValuePair_2_t1908_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1934_Object_t,
	RuntimeInvoker_Void_t1089_UIVertex_t191,
	RuntimeInvoker_Boolean_t298_UIVertex_t191,
	RuntimeInvoker_UIVertex_t191_Object_t,
	RuntimeInvoker_Enumerator_t1952,
	RuntimeInvoker_Int32_t56_UIVertex_t191,
	RuntimeInvoker_Void_t1089_Int32_t56_UIVertex_t191,
	RuntimeInvoker_UIVertex_t191_Int32_t56,
	RuntimeInvoker_UIVertex_t191,
	RuntimeInvoker_Boolean_t298_UIVertex_t191_UIVertex_t191,
	RuntimeInvoker_Object_t_UIVertex_t191_Object_t_Object_t,
	RuntimeInvoker_Int32_t56_UIVertex_t191_UIVertex_t191,
	RuntimeInvoker_Object_t_UIVertex_t191_UIVertex_t191_Object_t_Object_t,
	RuntimeInvoker_Object_t_ColorTween_t127,
	RuntimeInvoker_UILineInfo_t331,
	RuntimeInvoker_UICharInfo_t333,
	RuntimeInvoker_Object_t_Single_t61_Object_t_Object_t,
	RuntimeInvoker_Object_t_Vector2_t59_Object_t_Object_t,
	RuntimeInvoker_GcAchievementData_t546,
	RuntimeInvoker_GcScoreData_t547,
	RuntimeInvoker_Keyframe_t469,
	RuntimeInvoker_UICharInfo_t333_Object_t,
	RuntimeInvoker_Enumerator_t2097,
	RuntimeInvoker_Boolean_t298_UICharInfo_t333_UICharInfo_t333,
	RuntimeInvoker_Object_t_UICharInfo_t333_Object_t_Object_t,
	RuntimeInvoker_Int32_t56_UICharInfo_t333_UICharInfo_t333,
	RuntimeInvoker_Object_t_UICharInfo_t333_UICharInfo_t333_Object_t_Object_t,
	RuntimeInvoker_UILineInfo_t331_Object_t,
	RuntimeInvoker_Enumerator_t2106,
	RuntimeInvoker_Boolean_t298_UILineInfo_t331_UILineInfo_t331,
	RuntimeInvoker_Object_t_UILineInfo_t331_Object_t_Object_t,
	RuntimeInvoker_Int32_t56_UILineInfo_t331_UILineInfo_t331,
	RuntimeInvoker_Object_t_UILineInfo_t331_UILineInfo_t331_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2118_Object_t_Int64_t662,
	RuntimeInvoker_Int64_t662_Object_t_Int64_t662,
	RuntimeInvoker_Enumerator_t2123,
	RuntimeInvoker_DictionaryEntry_t1068_Object_t_Int64_t662,
	RuntimeInvoker_KeyValuePair_2_t2118,
	RuntimeInvoker_Enumerator_t2122,
	RuntimeInvoker_Object_t_Object_t_Int64_t662_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2126,
	RuntimeInvoker_KeyValuePair_2_t2118_Object_t,
	RuntimeInvoker_Boolean_t298_Int64_t662_Int64_t662,
	RuntimeInvoker_Void_t1089_UInt64_t665_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2155_UInt64_t665_Object_t,
	RuntimeInvoker_NetworkID_t502_UInt64_t665_Object_t,
	RuntimeInvoker_Boolean_t298_UInt64_t665_ObjectU26_t2743,
	RuntimeInvoker_NetworkID_t502_Object_t,
	RuntimeInvoker_Enumerator_t2160,
	RuntimeInvoker_DictionaryEntry_t1068_UInt64_t665_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2155,
	RuntimeInvoker_Enumerator_t2159,
	RuntimeInvoker_Object_t_UInt64_t665_Object_t_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2163,
	RuntimeInvoker_KeyValuePair_2_t2155_Object_t,
	RuntimeInvoker_Boolean_t298_UInt64_t665_UInt64_t665,
	RuntimeInvoker_KeyValuePair_2_t2175,
	RuntimeInvoker_Void_t1089_Object_t_KeyValuePair_2_t1934,
	RuntimeInvoker_KeyValuePair_2_t2175_Object_t_KeyValuePair_2_t1934,
	RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t1934,
	RuntimeInvoker_KeyValuePair_2_t1934_Object_t_KeyValuePair_2_t1934,
	RuntimeInvoker_Boolean_t298_Object_t_KeyValuePair_2U26_t2819,
	RuntimeInvoker_Enumerator_t2204,
	RuntimeInvoker_DictionaryEntry_t1068_Object_t_KeyValuePair_2_t1934,
	RuntimeInvoker_Enumerator_t2203,
	RuntimeInvoker_Object_t_Object_t_KeyValuePair_2_t1934_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2207,
	RuntimeInvoker_KeyValuePair_2_t2175_Object_t,
	RuntimeInvoker_Boolean_t298_KeyValuePair_2_t1934_KeyValuePair_2_t1934,
	RuntimeInvoker_ParameterModifier_t1346,
	RuntimeInvoker_HitInfo_t561,
	RuntimeInvoker_TextEditOp_t579_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2238_Object_t_Int32_t56,
	RuntimeInvoker_TextEditOp_t579_Object_t_Int32_t56,
	RuntimeInvoker_Boolean_t298_Object_t_TextEditOpU26_t2820,
	RuntimeInvoker_Enumerator_t2243,
	RuntimeInvoker_KeyValuePair_2_t2238,
	RuntimeInvoker_TextEditOp_t579,
	RuntimeInvoker_Enumerator_t2242,
	RuntimeInvoker_Enumerator_t2246,
	RuntimeInvoker_KeyValuePair_2_t2238_Object_t,
	RuntimeInvoker_ClientCertificateType_t844,
	RuntimeInvoker_KeyValuePair_2_t2289_Object_t_SByte_t666,
	RuntimeInvoker_Boolean_t298_Object_t_BooleanU26_t2725,
	RuntimeInvoker_Enumerator_t2294,
	RuntimeInvoker_DictionaryEntry_t1068_Object_t_SByte_t666,
	RuntimeInvoker_KeyValuePair_2_t2289,
	RuntimeInvoker_Enumerator_t2293,
	RuntimeInvoker_Object_t_Object_t_SByte_t666_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2297,
	RuntimeInvoker_KeyValuePair_2_t2289_Object_t,
	RuntimeInvoker_Boolean_t298_SByte_t666_SByte_t666,
	RuntimeInvoker_X509ChainStatus_t974,
	RuntimeInvoker_KeyValuePair_2_t2312_Int32_t56_Int32_t56,
	RuntimeInvoker_Boolean_t298_Int32_t56_Int32U26_t2726,
	RuntimeInvoker_Enumerator_t2316,
	RuntimeInvoker_DictionaryEntry_t1068_Int32_t56_Int32_t56,
	RuntimeInvoker_KeyValuePair_2_t2312,
	RuntimeInvoker_Enumerator_t2315,
	RuntimeInvoker_Enumerator_t2319,
	RuntimeInvoker_KeyValuePair_2_t2312_Object_t,
	RuntimeInvoker_Mark_t1019,
	RuntimeInvoker_UriScheme_t1056,
	RuntimeInvoker_TableRange_t1121,
	RuntimeInvoker_Slot_t1198,
	RuntimeInvoker_Slot_t1205,
	RuntimeInvoker_ILTokenInfo_t1283,
	RuntimeInvoker_LabelData_t1285,
	RuntimeInvoker_LabelFixup_t1284,
	RuntimeInvoker_TypeTag_t1484,
	RuntimeInvoker_Int32_t56_DateTimeOffset_t679_DateTimeOffset_t679,
	RuntimeInvoker_Boolean_t298_DateTimeOffset_t679_DateTimeOffset_t679,
	RuntimeInvoker_Boolean_t298_Nullable_1_t1741,
	RuntimeInvoker_Int32_t56_Guid_t680_Guid_t680,
	RuntimeInvoker_Boolean_t298_Guid_t680_Guid_t680,
};
