﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void U24ArrayTypeU243132_t1707_marshal(const U24ArrayTypeU243132_t1707& unmarshaled, U24ArrayTypeU243132_t1707_marshaled& marshaled);
extern "C" void U24ArrayTypeU243132_t1707_marshal_back(const U24ArrayTypeU243132_t1707_marshaled& marshaled, U24ArrayTypeU243132_t1707& unmarshaled);
extern "C" void U24ArrayTypeU243132_t1707_marshal_cleanup(U24ArrayTypeU243132_t1707_marshaled& marshaled);
