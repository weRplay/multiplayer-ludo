﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_23MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m16805(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2252 *, Event_t189 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m16710_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Key()
#define KeyValuePair_2_get_Key_m16806(__this, method) (( Event_t189 * (*) (KeyValuePair_2_t2252 *, const MethodInfo*))KeyValuePair_2_get_Key_m16711_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m16807(__this, ___value, method) (( void (*) (KeyValuePair_2_t2252 *, Event_t189 *, const MethodInfo*))KeyValuePair_2_set_Key_m16712_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Value()
#define KeyValuePair_2_get_Value_m16808(__this, method) (( int32_t (*) (KeyValuePair_2_t2252 *, const MethodInfo*))KeyValuePair_2_get_Value_m16713_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m16809(__this, ___value, method) (( void (*) (KeyValuePair_2_t2252 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m16714_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToString()
#define KeyValuePair_2_ToString_m16810(__this, method) (( String_t* (*) (KeyValuePair_2_t2252 *, const MethodInfo*))KeyValuePair_2_ToString_m16715_gshared)(__this, method)
