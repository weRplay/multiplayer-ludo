﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void U24ArrayTypeU24128_t1722_marshal(const U24ArrayTypeU24128_t1722& unmarshaled, U24ArrayTypeU24128_t1722_marshaled& marshaled);
extern "C" void U24ArrayTypeU24128_t1722_marshal_back(const U24ArrayTypeU24128_t1722_marshaled& marshaled, U24ArrayTypeU24128_t1722& unmarshaled);
extern "C" void U24ArrayTypeU24128_t1722_marshal_cleanup(U24ArrayTypeU24128_t1722_marshaled& marshaled);
