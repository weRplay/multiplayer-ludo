﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_69.h"

// System.Void System.Array/InternalEnumerator`1<System.Int16>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17531_gshared (InternalEnumerator_1_t2329 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17531(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2329 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17531_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17532_gshared (InternalEnumerator_1_t2329 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17532(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2329 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17532_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int16>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17533_gshared (InternalEnumerator_1_t2329 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17533(__this, method) (( void (*) (InternalEnumerator_1_t2329 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17533_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int16>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17534_gshared (InternalEnumerator_1_t2329 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17534(__this, method) (( bool (*) (InternalEnumerator_1_t2329 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17534_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int16>::get_Current()
extern "C" int16_t InternalEnumerator_1_get_Current_m17535_gshared (InternalEnumerator_1_t2329 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17535(__this, method) (( int16_t (*) (InternalEnumerator_1_t2329 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17535_gshared)(__this, method)
