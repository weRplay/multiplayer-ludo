﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Reflection.Assembly
struct Assembly_t1076;
// System.Object
struct Object_t;
// System.ResolveEventArgs
struct ResolveEventArgs_t1683;
// System.IAsyncResult
struct IAsyncResult_t180;
// System.AsyncCallback
struct AsyncCallback_t181;

#include "mscorlib_System_MulticastDelegate.h"

// System.ResolveEventHandler
struct  ResolveEventHandler_t1628  : public MulticastDelegate_t179
{
};
