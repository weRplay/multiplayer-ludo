﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameManager/<AutomaticMovePlayer1>c__Iterator6
struct U3CAutomaticMovePlayer1U3Ec__Iterator6_t24;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void GameManager/<AutomaticMovePlayer1>c__Iterator6::.ctor()
extern "C" void U3CAutomaticMovePlayer1U3Ec__Iterator6__ctor_m58 (U3CAutomaticMovePlayer1U3Ec__Iterator6_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager/<AutomaticMovePlayer1>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CAutomaticMovePlayer1U3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m59 (U3CAutomaticMovePlayer1U3Ec__Iterator6_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager/<AutomaticMovePlayer1>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CAutomaticMovePlayer1U3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m60 (U3CAutomaticMovePlayer1U3Ec__Iterator6_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager/<AutomaticMovePlayer1>c__Iterator6::MoveNext()
extern "C" bool U3CAutomaticMovePlayer1U3Ec__Iterator6_MoveNext_m61 (U3CAutomaticMovePlayer1U3Ec__Iterator6_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager/<AutomaticMovePlayer1>c__Iterator6::Dispose()
extern "C" void U3CAutomaticMovePlayer1U3Ec__Iterator6_Dispose_m62 (U3CAutomaticMovePlayer1U3Ec__Iterator6_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager/<AutomaticMovePlayer1>c__Iterator6::Reset()
extern "C" void U3CAutomaticMovePlayer1U3Ec__Iterator6_Reset_m63 (U3CAutomaticMovePlayer1U3Ec__Iterator6_t24 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
