﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Player/<ExecuteAfterTime>c__IteratorE
struct U3CExecuteAfterTimeU3Ec__IteratorE_t42;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Player/<ExecuteAfterTime>c__IteratorE::.ctor()
extern "C" void U3CExecuteAfterTimeU3Ec__IteratorE__ctor_m144 (U3CExecuteAfterTimeU3Ec__IteratorE_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Player/<ExecuteAfterTime>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CExecuteAfterTimeU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m145 (U3CExecuteAfterTimeU3Ec__IteratorE_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Player/<ExecuteAfterTime>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CExecuteAfterTimeU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m146 (U3CExecuteAfterTimeU3Ec__IteratorE_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Player/<ExecuteAfterTime>c__IteratorE::MoveNext()
extern "C" bool U3CExecuteAfterTimeU3Ec__IteratorE_MoveNext_m147 (U3CExecuteAfterTimeU3Ec__IteratorE_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player/<ExecuteAfterTime>c__IteratorE::Dispose()
extern "C" void U3CExecuteAfterTimeU3Ec__IteratorE_Dispose_m148 (U3CExecuteAfterTimeU3Ec__IteratorE_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Player/<ExecuteAfterTime>c__IteratorE::Reset()
extern "C" void U3CExecuteAfterTimeU3Ec__IteratorE_Reset_m149 (U3CExecuteAfterTimeU3Ec__IteratorE_t42 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
