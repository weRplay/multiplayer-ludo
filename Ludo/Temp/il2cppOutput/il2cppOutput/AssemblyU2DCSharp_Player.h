﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "AssemblyU2DCSharp_MovingObject.h"

// Player
struct  Player_t11  : public MovingObject_t36
{
	// System.Int32 Player::wallDamage
	int32_t ___wallDamage_7;
};
