﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m13102(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1976 *, Canvas_t148 *, IndexedSet_1_t324 *, const MethodInfo*))KeyValuePair_2__ctor_m12459_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Key()
#define KeyValuePair_2_get_Key_m13103(__this, method) (( Canvas_t148 * (*) (KeyValuePair_2_t1976 *, const MethodInfo*))KeyValuePair_2_get_Key_m12460_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m13104(__this, ___value, method) (( void (*) (KeyValuePair_2_t1976 *, Canvas_t148 *, const MethodInfo*))KeyValuePair_2_set_Key_m12461_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Value()
#define KeyValuePair_2_get_Value_m13105(__this, method) (( IndexedSet_1_t324 * (*) (KeyValuePair_2_t1976 *, const MethodInfo*))KeyValuePair_2_get_Value_m12462_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m13106(__this, ___value, method) (( void (*) (KeyValuePair_2_t1976 *, IndexedSet_1_t324 *, const MethodInfo*))KeyValuePair_2_set_Value_m12463_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::ToString()
#define KeyValuePair_2_ToString_m13107(__this, method) (( String_t* (*) (KeyValuePair_2_t1976 *, const MethodInfo*))KeyValuePair_2_ToString_m12464_gshared)(__this, method)
