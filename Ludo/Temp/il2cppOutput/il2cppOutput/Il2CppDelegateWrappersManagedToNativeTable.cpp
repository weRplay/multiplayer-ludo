﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void pinvoke_delegate_wrapper_OnValidateInput_t178 ();
extern "C" void pinvoke_delegate_wrapper_WindowFunction_t392 ();
extern "C" void pinvoke_delegate_wrapper_SkinChangedDelegate_t411 ();
extern "C" void pinvoke_delegate_wrapper_ReapplyDrivenProperties_t351 ();
extern "C" void pinvoke_delegate_wrapper_LogCallback_t439 ();
extern "C" void pinvoke_delegate_wrapper_CameraCallback_t441 ();
extern "C" void pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t443 ();
extern "C" void pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t457 ();
extern "C" void pinvoke_delegate_wrapper_PCMReaderCallback_t459 ();
extern "C" void pinvoke_delegate_wrapper_PCMSetPositionCallback_t461 ();
extern "C" void pinvoke_delegate_wrapper_FontTextureRebuildCallback_t479 ();
extern "C" void pinvoke_delegate_wrapper_WillRenderCanvases_t311 ();
extern "C" void pinvoke_delegate_wrapper_GetDelegate_t520 ();
extern "C" void pinvoke_delegate_wrapper_SetDelegate_t521 ();
extern "C" void pinvoke_delegate_wrapper_ConstructorDelegate_t522 ();
extern "C" void pinvoke_delegate_wrapper_UnityAction_t149 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t758 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t857 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback_t833 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback2_t834 ();
extern "C" void pinvoke_delegate_wrapper_CertificateSelectionCallback_t817 ();
extern "C" void pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t818 ();
extern "C" void pinvoke_delegate_wrapper_MatchAppendEvaluator_t992 ();
extern "C" void pinvoke_delegate_wrapper_CostDelegate_t1029 ();
extern "C" void pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t898 ();
extern "C" void pinvoke_delegate_wrapper_MatchEvaluator_t1063 ();
extern "C" void pinvoke_delegate_wrapper_Swapper_t1088 ();
extern "C" void pinvoke_delegate_wrapper_AsyncCallback_t181 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1153 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1161 ();
extern "C" void pinvoke_delegate_wrapper_ReadDelegate_t1249 ();
extern "C" void pinvoke_delegate_wrapper_WriteDelegate_t1250 ();
extern "C" void pinvoke_delegate_wrapper_AddEventAdapter_t1329 ();
extern "C" void pinvoke_delegate_wrapper_GetterAdapter_t1344 ();
extern "C" void pinvoke_delegate_wrapper_CallbackHandler_t1511 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t1697 ();
extern "C" void pinvoke_delegate_wrapper_MemberFilter_t1091 ();
extern "C" void pinvoke_delegate_wrapper_TypeFilter_t1336 ();
extern "C" void pinvoke_delegate_wrapper_CrossContextDelegate_t1698 ();
extern "C" void pinvoke_delegate_wrapper_HeaderHandler_t1699 ();
extern "C" void pinvoke_delegate_wrapper_ThreadStart_t1701 ();
extern "C" void pinvoke_delegate_wrapper_TimerCallback_t1621 ();
extern "C" void pinvoke_delegate_wrapper_WaitCallback_t1702 ();
extern "C" void pinvoke_delegate_wrapper_AppDomainInitializer_t1631 ();
extern "C" void pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t1627 ();
extern "C" void pinvoke_delegate_wrapper_EventHandler_t1629 ();
extern "C" void pinvoke_delegate_wrapper_ResolveEventHandler_t1628 ();
extern "C" void pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t635 ();
extern const methodPointerType g_DelegateWrappersManagedToNative[48] = 
{
	pinvoke_delegate_wrapper_OnValidateInput_t178,
	pinvoke_delegate_wrapper_WindowFunction_t392,
	pinvoke_delegate_wrapper_SkinChangedDelegate_t411,
	pinvoke_delegate_wrapper_ReapplyDrivenProperties_t351,
	pinvoke_delegate_wrapper_LogCallback_t439,
	pinvoke_delegate_wrapper_CameraCallback_t441,
	pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t443,
	pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t457,
	pinvoke_delegate_wrapper_PCMReaderCallback_t459,
	pinvoke_delegate_wrapper_PCMSetPositionCallback_t461,
	pinvoke_delegate_wrapper_FontTextureRebuildCallback_t479,
	pinvoke_delegate_wrapper_WillRenderCanvases_t311,
	pinvoke_delegate_wrapper_GetDelegate_t520,
	pinvoke_delegate_wrapper_SetDelegate_t521,
	pinvoke_delegate_wrapper_ConstructorDelegate_t522,
	pinvoke_delegate_wrapper_UnityAction_t149,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t758,
	pinvoke_delegate_wrapper_PrimalityTest_t857,
	pinvoke_delegate_wrapper_CertificateValidationCallback_t833,
	pinvoke_delegate_wrapper_CertificateValidationCallback2_t834,
	pinvoke_delegate_wrapper_CertificateSelectionCallback_t817,
	pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t818,
	pinvoke_delegate_wrapper_MatchAppendEvaluator_t992,
	pinvoke_delegate_wrapper_CostDelegate_t1029,
	pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t898,
	pinvoke_delegate_wrapper_MatchEvaluator_t1063,
	pinvoke_delegate_wrapper_Swapper_t1088,
	pinvoke_delegate_wrapper_AsyncCallback_t181,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1153,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1161,
	pinvoke_delegate_wrapper_ReadDelegate_t1249,
	pinvoke_delegate_wrapper_WriteDelegate_t1250,
	pinvoke_delegate_wrapper_AddEventAdapter_t1329,
	pinvoke_delegate_wrapper_GetterAdapter_t1344,
	pinvoke_delegate_wrapper_CallbackHandler_t1511,
	pinvoke_delegate_wrapper_PrimalityTest_t1697,
	pinvoke_delegate_wrapper_MemberFilter_t1091,
	pinvoke_delegate_wrapper_TypeFilter_t1336,
	pinvoke_delegate_wrapper_CrossContextDelegate_t1698,
	pinvoke_delegate_wrapper_HeaderHandler_t1699,
	pinvoke_delegate_wrapper_ThreadStart_t1701,
	pinvoke_delegate_wrapper_TimerCallback_t1621,
	pinvoke_delegate_wrapper_WaitCallback_t1702,
	pinvoke_delegate_wrapper_AppDomainInitializer_t1631,
	pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t1627,
	pinvoke_delegate_wrapper_EventHandler_t1629,
	pinvoke_delegate_wrapper_ResolveEventHandler_t1628,
	pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t635,
};
