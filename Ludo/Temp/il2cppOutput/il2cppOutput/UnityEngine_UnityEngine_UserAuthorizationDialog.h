﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture
struct Texture_t196;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Rect.h"

// UnityEngine.UserAuthorizationDialog
struct  UserAuthorizationDialog_t592  : public MonoBehaviour_t2
{
	// UnityEngine.Rect UnityEngine.UserAuthorizationDialog::windowRect
	Rect_t184  ___windowRect_4;
	// UnityEngine.Texture UnityEngine.UserAuthorizationDialog::warningIcon
	Texture_t196 * ___warningIcon_5;
};
