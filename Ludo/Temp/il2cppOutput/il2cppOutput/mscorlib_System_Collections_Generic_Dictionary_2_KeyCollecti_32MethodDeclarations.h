﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Int32>
struct Dictionary_2_t1000;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_32.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m17449_gshared (Enumerator_t2315 * __this, Dictionary_2_t1000 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m17449(__this, ___host, method) (( void (*) (Enumerator_t2315 *, Dictionary_2_t1000 *, const MethodInfo*))Enumerator__ctor_m17449_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17450_gshared (Enumerator_t2315 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17450(__this, method) (( Object_t * (*) (Enumerator_t2315 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17450_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m17451_gshared (Enumerator_t2315 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17451(__this, method) (( void (*) (Enumerator_t2315 *, const MethodInfo*))Enumerator_Dispose_m17451_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17452_gshared (Enumerator_t2315 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17452(__this, method) (( bool (*) (Enumerator_t2315 *, const MethodInfo*))Enumerator_MoveNext_m17452_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m17453_gshared (Enumerator_t2315 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17453(__this, method) (( int32_t (*) (Enumerator_t2315 *, const MethodInfo*))Enumerator_get_Current_m17453_gshared)(__this, method)
