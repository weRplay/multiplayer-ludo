﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Object
struct Object_t;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_6.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::.ctor(TKey,TValue)
extern "C" void KeyValuePair_2__ctor_m15874_gshared (KeyValuePair_2_t2175 * __this, Object_t * ___key, KeyValuePair_2_t1934  ___value, const MethodInfo* method);
#define KeyValuePair_2__ctor_m15874(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2175 *, Object_t *, KeyValuePair_2_t1934 , const MethodInfo*))KeyValuePair_2__ctor_m15874_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Key()
extern "C" Object_t * KeyValuePair_2_get_Key_m15875_gshared (KeyValuePair_2_t2175 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Key_m15875(__this, method) (( Object_t * (*) (KeyValuePair_2_t2175 *, const MethodInfo*))KeyValuePair_2_get_Key_m15875_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Key(TKey)
extern "C" void KeyValuePair_2_set_Key_m15876_gshared (KeyValuePair_2_t2175 * __this, Object_t * ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Key_m15876(__this, ___value, method) (( void (*) (KeyValuePair_2_t2175 *, Object_t *, const MethodInfo*))KeyValuePair_2_set_Key_m15876_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::get_Value()
extern "C" KeyValuePair_2_t1934  KeyValuePair_2_get_Value_m15877_gshared (KeyValuePair_2_t2175 * __this, const MethodInfo* method);
#define KeyValuePair_2_get_Value_m15877(__this, method) (( KeyValuePair_2_t1934  (*) (KeyValuePair_2_t2175 *, const MethodInfo*))KeyValuePair_2_get_Value_m15877_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::set_Value(TValue)
extern "C" void KeyValuePair_2_set_Value_m15878_gshared (KeyValuePair_2_t2175 * __this, KeyValuePair_2_t1934  ___value, const MethodInfo* method);
#define KeyValuePair_2_set_Value_m15878(__this, ___value, method) (( void (*) (KeyValuePair_2_t2175 *, KeyValuePair_2_t1934 , const MethodInfo*))KeyValuePair_2_set_Value_m15878_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Object,System.Collections.Generic.KeyValuePair`2<System.Object,System.Object>>::ToString()
extern "C" String_t* KeyValuePair_2_ToString_m15879_gshared (KeyValuePair_2_t2175 * __this, const MethodInfo* method);
#define KeyValuePair_2_ToString_m15879(__this, method) (( String_t* (*) (KeyValuePair_2_t2175 *, const MethodInfo*))KeyValuePair_2_ToString_m15879_gshared)(__this, method)
