﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen_4MethodDeclarations.h"

// System.Void System.Comparison`1<UnityEngine.Events.BaseInvokableCall>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m17064(__this, ___object, ___method, method) (( void (*) (Comparison_1_t2275 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m10811_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.Events.BaseInvokableCall>::Invoke(T,T)
#define Comparison_1_Invoke_m17065(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t2275 *, BaseInvokableCall_t583 *, BaseInvokableCall_t583 *, const MethodInfo*))Comparison_1_Invoke_m10812_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.Events.BaseInvokableCall>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m17066(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t2275 *, BaseInvokableCall_t583 *, BaseInvokableCall_t583 *, AsyncCallback_t181 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m10813_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.Events.BaseInvokableCall>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m17067(__this, ___result, method) (( int32_t (*) (Comparison_1_t2275 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m10814_gshared)(__this, ___result, method)
