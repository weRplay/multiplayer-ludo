﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>
struct Collection_1_t1788;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t51;
// System.Object
struct Object_t;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t208;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector3>
struct IEnumerator_1_t2392;
// System.Collections.Generic.IList`1<UnityEngine.Vector3>
struct IList_1_t1787;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::.ctor()
extern "C" void Collection_1__ctor_m10542_gshared (Collection_1_t1788 * __this, const MethodInfo* method);
#define Collection_1__ctor_m10542(__this, method) (( void (*) (Collection_1_t1788 *, const MethodInfo*))Collection_1__ctor_m10542_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m10543_gshared (Collection_1_t1788 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m10543(__this, method) (( bool (*) (Collection_1_t1788 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m10543_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m10544_gshared (Collection_1_t1788 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m10544(__this, ___array, ___index, method) (( void (*) (Collection_1_t1788 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m10544_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m10545_gshared (Collection_1_t1788 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m10545(__this, method) (( Object_t * (*) (Collection_1_t1788 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m10545_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m10546_gshared (Collection_1_t1788 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m10546(__this, ___value, method) (( int32_t (*) (Collection_1_t1788 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m10546_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m10547_gshared (Collection_1_t1788 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m10547(__this, ___value, method) (( bool (*) (Collection_1_t1788 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m10547_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m10548_gshared (Collection_1_t1788 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m10548(__this, ___value, method) (( int32_t (*) (Collection_1_t1788 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m10548_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m10549_gshared (Collection_1_t1788 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m10549(__this, ___index, ___value, method) (( void (*) (Collection_1_t1788 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m10549_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m10550_gshared (Collection_1_t1788 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m10550(__this, ___value, method) (( void (*) (Collection_1_t1788 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m10550_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m10551_gshared (Collection_1_t1788 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m10551(__this, method) (( bool (*) (Collection_1_t1788 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m10551_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m10552_gshared (Collection_1_t1788 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m10552(__this, method) (( Object_t * (*) (Collection_1_t1788 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m10552_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m10553_gshared (Collection_1_t1788 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m10553(__this, method) (( bool (*) (Collection_1_t1788 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m10553_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m10554_gshared (Collection_1_t1788 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m10554(__this, method) (( bool (*) (Collection_1_t1788 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m10554_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m10555_gshared (Collection_1_t1788 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m10555(__this, ___index, method) (( Object_t * (*) (Collection_1_t1788 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m10555_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m10556_gshared (Collection_1_t1788 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m10556(__this, ___index, ___value, method) (( void (*) (Collection_1_t1788 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m10556_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Add(T)
extern "C" void Collection_1_Add_m10557_gshared (Collection_1_t1788 * __this, Vector3_t35  ___item, const MethodInfo* method);
#define Collection_1_Add_m10557(__this, ___item, method) (( void (*) (Collection_1_t1788 *, Vector3_t35 , const MethodInfo*))Collection_1_Add_m10557_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Clear()
extern "C" void Collection_1_Clear_m10558_gshared (Collection_1_t1788 * __this, const MethodInfo* method);
#define Collection_1_Clear_m10558(__this, method) (( void (*) (Collection_1_t1788 *, const MethodInfo*))Collection_1_Clear_m10558_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::ClearItems()
extern "C" void Collection_1_ClearItems_m10559_gshared (Collection_1_t1788 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m10559(__this, method) (( void (*) (Collection_1_t1788 *, const MethodInfo*))Collection_1_ClearItems_m10559_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Contains(T)
extern "C" bool Collection_1_Contains_m10560_gshared (Collection_1_t1788 * __this, Vector3_t35  ___item, const MethodInfo* method);
#define Collection_1_Contains_m10560(__this, ___item, method) (( bool (*) (Collection_1_t1788 *, Vector3_t35 , const MethodInfo*))Collection_1_Contains_m10560_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m10561_gshared (Collection_1_t1788 * __this, Vector3U5BU5D_t208* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m10561(__this, ___array, ___index, method) (( void (*) (Collection_1_t1788 *, Vector3U5BU5D_t208*, int32_t, const MethodInfo*))Collection_1_CopyTo_m10561_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m10562_gshared (Collection_1_t1788 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m10562(__this, method) (( Object_t* (*) (Collection_1_t1788 *, const MethodInfo*))Collection_1_GetEnumerator_m10562_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m10563_gshared (Collection_1_t1788 * __this, Vector3_t35  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m10563(__this, ___item, method) (( int32_t (*) (Collection_1_t1788 *, Vector3_t35 , const MethodInfo*))Collection_1_IndexOf_m10563_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m10564_gshared (Collection_1_t1788 * __this, int32_t ___index, Vector3_t35  ___item, const MethodInfo* method);
#define Collection_1_Insert_m10564(__this, ___index, ___item, method) (( void (*) (Collection_1_t1788 *, int32_t, Vector3_t35 , const MethodInfo*))Collection_1_Insert_m10564_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m10565_gshared (Collection_1_t1788 * __this, int32_t ___index, Vector3_t35  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m10565(__this, ___index, ___item, method) (( void (*) (Collection_1_t1788 *, int32_t, Vector3_t35 , const MethodInfo*))Collection_1_InsertItem_m10565_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::Remove(T)
extern "C" bool Collection_1_Remove_m10566_gshared (Collection_1_t1788 * __this, Vector3_t35  ___item, const MethodInfo* method);
#define Collection_1_Remove_m10566(__this, ___item, method) (( bool (*) (Collection_1_t1788 *, Vector3_t35 , const MethodInfo*))Collection_1_Remove_m10566_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m10567_gshared (Collection_1_t1788 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m10567(__this, ___index, method) (( void (*) (Collection_1_t1788 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m10567_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m10568_gshared (Collection_1_t1788 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m10568(__this, ___index, method) (( void (*) (Collection_1_t1788 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m10568_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::get_Count()
extern "C" int32_t Collection_1_get_Count_m10569_gshared (Collection_1_t1788 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m10569(__this, method) (( int32_t (*) (Collection_1_t1788 *, const MethodInfo*))Collection_1_get_Count_m10569_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::get_Item(System.Int32)
extern "C" Vector3_t35  Collection_1_get_Item_m10570_gshared (Collection_1_t1788 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m10570(__this, ___index, method) (( Vector3_t35  (*) (Collection_1_t1788 *, int32_t, const MethodInfo*))Collection_1_get_Item_m10570_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m10571_gshared (Collection_1_t1788 * __this, int32_t ___index, Vector3_t35  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m10571(__this, ___index, ___value, method) (( void (*) (Collection_1_t1788 *, int32_t, Vector3_t35 , const MethodInfo*))Collection_1_set_Item_m10571_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m10572_gshared (Collection_1_t1788 * __this, int32_t ___index, Vector3_t35  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m10572(__this, ___index, ___item, method) (( void (*) (Collection_1_t1788 *, int32_t, Vector3_t35 , const MethodInfo*))Collection_1_SetItem_m10572_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m10573_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m10573(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m10573_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::ConvertItem(System.Object)
extern "C" Vector3_t35  Collection_1_ConvertItem_m10574_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m10574(__this /* static, unused */, ___item, method) (( Vector3_t35  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m10574_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m10575_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m10575(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m10575_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m10576_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m10576(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m10576_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector3>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m10577_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m10577(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m10577_gshared)(__this /* static, unused */, ___list, method)
