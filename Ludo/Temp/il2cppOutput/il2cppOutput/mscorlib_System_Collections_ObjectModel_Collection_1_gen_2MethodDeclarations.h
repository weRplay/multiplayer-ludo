﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>
struct Collection_1_t1955;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t51;
// System.Object
struct Object_t;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t185;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t2417;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t341;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::.ctor()
extern "C" void Collection_1__ctor_m12743_gshared (Collection_1_t1955 * __this, const MethodInfo* method);
#define Collection_1__ctor_m12743(__this, method) (( void (*) (Collection_1_t1955 *, const MethodInfo*))Collection_1__ctor_m12743_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12744_gshared (Collection_1_t1955 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12744(__this, method) (( bool (*) (Collection_1_t1955 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12744_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m12745_gshared (Collection_1_t1955 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m12745(__this, ___array, ___index, method) (( void (*) (Collection_1_t1955 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m12745_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m12746_gshared (Collection_1_t1955 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m12746(__this, method) (( Object_t * (*) (Collection_1_t1955 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m12746_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m12747_gshared (Collection_1_t1955 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m12747(__this, ___value, method) (( int32_t (*) (Collection_1_t1955 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m12747_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m12748_gshared (Collection_1_t1955 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m12748(__this, ___value, method) (( bool (*) (Collection_1_t1955 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m12748_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m12749_gshared (Collection_1_t1955 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m12749(__this, ___value, method) (( int32_t (*) (Collection_1_t1955 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m12749_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m12750_gshared (Collection_1_t1955 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m12750(__this, ___index, ___value, method) (( void (*) (Collection_1_t1955 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m12750_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m12751_gshared (Collection_1_t1955 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m12751(__this, ___value, method) (( void (*) (Collection_1_t1955 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m12751_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m12752_gshared (Collection_1_t1955 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m12752(__this, method) (( bool (*) (Collection_1_t1955 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m12752_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m12753_gshared (Collection_1_t1955 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m12753(__this, method) (( Object_t * (*) (Collection_1_t1955 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m12753_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m12754_gshared (Collection_1_t1955 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m12754(__this, method) (( bool (*) (Collection_1_t1955 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m12754_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m12755_gshared (Collection_1_t1955 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m12755(__this, method) (( bool (*) (Collection_1_t1955 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m12755_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m12756_gshared (Collection_1_t1955 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m12756(__this, ___index, method) (( Object_t * (*) (Collection_1_t1955 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m12756_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m12757_gshared (Collection_1_t1955 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m12757(__this, ___index, ___value, method) (( void (*) (Collection_1_t1955 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m12757_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Add(T)
extern "C" void Collection_1_Add_m12758_gshared (Collection_1_t1955 * __this, UIVertex_t191  ___item, const MethodInfo* method);
#define Collection_1_Add_m12758(__this, ___item, method) (( void (*) (Collection_1_t1955 *, UIVertex_t191 , const MethodInfo*))Collection_1_Add_m12758_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Clear()
extern "C" void Collection_1_Clear_m12759_gshared (Collection_1_t1955 * __this, const MethodInfo* method);
#define Collection_1_Clear_m12759(__this, method) (( void (*) (Collection_1_t1955 *, const MethodInfo*))Collection_1_Clear_m12759_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ClearItems()
extern "C" void Collection_1_ClearItems_m12760_gshared (Collection_1_t1955 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m12760(__this, method) (( void (*) (Collection_1_t1955 *, const MethodInfo*))Collection_1_ClearItems_m12760_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool Collection_1_Contains_m12761_gshared (Collection_1_t1955 * __this, UIVertex_t191  ___item, const MethodInfo* method);
#define Collection_1_Contains_m12761(__this, ___item, method) (( bool (*) (Collection_1_t1955 *, UIVertex_t191 , const MethodInfo*))Collection_1_Contains_m12761_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m12762_gshared (Collection_1_t1955 * __this, UIVertexU5BU5D_t185* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m12762(__this, ___array, ___index, method) (( void (*) (Collection_1_t1955 *, UIVertexU5BU5D_t185*, int32_t, const MethodInfo*))Collection_1_CopyTo_m12762_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m12763_gshared (Collection_1_t1955 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m12763(__this, method) (( Object_t* (*) (Collection_1_t1955 *, const MethodInfo*))Collection_1_GetEnumerator_m12763_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m12764_gshared (Collection_1_t1955 * __this, UIVertex_t191  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m12764(__this, ___item, method) (( int32_t (*) (Collection_1_t1955 *, UIVertex_t191 , const MethodInfo*))Collection_1_IndexOf_m12764_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m12765_gshared (Collection_1_t1955 * __this, int32_t ___index, UIVertex_t191  ___item, const MethodInfo* method);
#define Collection_1_Insert_m12765(__this, ___index, ___item, method) (( void (*) (Collection_1_t1955 *, int32_t, UIVertex_t191 , const MethodInfo*))Collection_1_Insert_m12765_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m12766_gshared (Collection_1_t1955 * __this, int32_t ___index, UIVertex_t191  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m12766(__this, ___index, ___item, method) (( void (*) (Collection_1_t1955 *, int32_t, UIVertex_t191 , const MethodInfo*))Collection_1_InsertItem_m12766_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool Collection_1_Remove_m12767_gshared (Collection_1_t1955 * __this, UIVertex_t191  ___item, const MethodInfo* method);
#define Collection_1_Remove_m12767(__this, ___item, method) (( bool (*) (Collection_1_t1955 *, UIVertex_t191 , const MethodInfo*))Collection_1_Remove_m12767_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m12768_gshared (Collection_1_t1955 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m12768(__this, ___index, method) (( void (*) (Collection_1_t1955 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m12768_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m12769_gshared (Collection_1_t1955 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m12769(__this, ___index, method) (( void (*) (Collection_1_t1955 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m12769_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t Collection_1_get_Count_m12770_gshared (Collection_1_t1955 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m12770(__this, method) (( int32_t (*) (Collection_1_t1955 *, const MethodInfo*))Collection_1_get_Count_m12770_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t191  Collection_1_get_Item_m12771_gshared (Collection_1_t1955 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m12771(__this, ___index, method) (( UIVertex_t191  (*) (Collection_1_t1955 *, int32_t, const MethodInfo*))Collection_1_get_Item_m12771_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m12772_gshared (Collection_1_t1955 * __this, int32_t ___index, UIVertex_t191  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m12772(__this, ___index, ___value, method) (( void (*) (Collection_1_t1955 *, int32_t, UIVertex_t191 , const MethodInfo*))Collection_1_set_Item_m12772_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m12773_gshared (Collection_1_t1955 * __this, int32_t ___index, UIVertex_t191  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m12773(__this, ___index, ___item, method) (( void (*) (Collection_1_t1955 *, int32_t, UIVertex_t191 , const MethodInfo*))Collection_1_SetItem_m12773_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m12774_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m12774(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m12774_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ConvertItem(System.Object)
extern "C" UIVertex_t191  Collection_1_ConvertItem_m12775_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m12775(__this /* static, unused */, ___item, method) (( UIVertex_t191  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m12775_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m12776_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m12776(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m12776_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m12777_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m12777(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m12777_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m12778_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m12778(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m12778_gshared)(__this /* static, unused */, ___list, method)
