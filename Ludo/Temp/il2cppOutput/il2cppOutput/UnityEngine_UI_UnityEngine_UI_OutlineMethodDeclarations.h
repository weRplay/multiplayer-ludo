﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Outline
struct Outline_t260;
// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t187;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Outline::.ctor()
extern "C" void Outline__ctor_m1452 (Outline_t260 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Outline::ModifyVertices(System.Collections.Generic.List`1<UnityEngine.UIVertex>)
extern "C" void Outline_ModifyVertices_m1453 (Outline_t260 * __this, List_1_t187 * ___verts, const MethodInfo* method) IL2CPP_METHOD_ATTR;
