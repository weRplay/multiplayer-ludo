﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameManager/<ResetBoard>c__Iterator3
struct U3CResetBoardU3Ec__Iterator3_t20;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void GameManager/<ResetBoard>c__Iterator3::.ctor()
extern "C" void U3CResetBoardU3Ec__Iterator3__ctor_m40 (U3CResetBoardU3Ec__Iterator3_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager/<ResetBoard>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CResetBoardU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m41 (U3CResetBoardU3Ec__Iterator3_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager/<ResetBoard>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CResetBoardU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m42 (U3CResetBoardU3Ec__Iterator3_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager/<ResetBoard>c__Iterator3::MoveNext()
extern "C" bool U3CResetBoardU3Ec__Iterator3_MoveNext_m43 (U3CResetBoardU3Ec__Iterator3_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager/<ResetBoard>c__Iterator3::Dispose()
extern "C" void U3CResetBoardU3Ec__Iterator3_Dispose_m44 (U3CResetBoardU3Ec__Iterator3_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager/<ResetBoard>c__Iterator3::Reset()
extern "C" void U3CResetBoardU3Ec__Iterator3_Reset_m45 (U3CResetBoardU3Ec__Iterator3_t20 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
