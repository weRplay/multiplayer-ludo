﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_18MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m16410(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2213 *, String_t*, KeyValuePair_2_t678 , const MethodInfo*))KeyValuePair_2__ctor_m15874_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::get_Key()
#define KeyValuePair_2_get_Key_m16411(__this, method) (( String_t* (*) (KeyValuePair_2_t2213 *, const MethodInfo*))KeyValuePair_2_get_Key_m15875_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m16412(__this, ___value, method) (( void (*) (KeyValuePair_2_t2213 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m15876_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::get_Value()
#define KeyValuePair_2_get_Value_m16413(__this, method) (( KeyValuePair_2_t678  (*) (KeyValuePair_2_t2213 *, const MethodInfo*))KeyValuePair_2_get_Value_m15877_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m16414(__this, ___value, method) (( void (*) (KeyValuePair_2_t2213 *, KeyValuePair_2_t678 , const MethodInfo*))KeyValuePair_2_set_Value_m15878_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::ToString()
#define KeyValuePair_2_ToString_m16415(__this, method) (( String_t* (*) (KeyValuePair_2_t2213 *, const MethodInfo*))KeyValuePair_2_ToString_m15879_gshared)(__this, method)
