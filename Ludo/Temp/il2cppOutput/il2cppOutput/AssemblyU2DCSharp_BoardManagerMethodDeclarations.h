﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// BoardManager
struct BoardManager_t1;

#include "codegen/il2cpp-codegen.h"

// System.Void BoardManager::.ctor()
extern "C" void BoardManager__ctor_m0 (BoardManager_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BoardManager::Start()
extern "C" void BoardManager_Start_m1 (BoardManager_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BoardManager::InitializeRedPath()
extern "C" void BoardManager_InitializeRedPath_m2 (BoardManager_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BoardManager::InitializeBluePath()
extern "C" void BoardManager_InitializeBluePath_m3 (BoardManager_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BoardManager::BoardSetup()
extern "C" void BoardManager_BoardSetup_m4 (BoardManager_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void BoardManager::SetupScene()
extern "C" void BoardManager_SetupScene_m5 (BoardManager_t1 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
