﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1<UnityEngine.TextEditor/TextEditOp>
struct EqualityComparer_1_t2250;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.TextEditor/TextEditOp>::.ctor()
extern "C" void EqualityComparer_1__ctor_m16793_gshared (EqualityComparer_1_t2250 * __this, const MethodInfo* method);
#define EqualityComparer_1__ctor_m16793(__this, method) (( void (*) (EqualityComparer_1_t2250 *, const MethodInfo*))EqualityComparer_1__ctor_m16793_gshared)(__this, method)
// System.Void System.Collections.Generic.EqualityComparer`1<UnityEngine.TextEditor/TextEditOp>::.cctor()
extern "C" void EqualityComparer_1__cctor_m16794_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1__cctor_m16794(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1__cctor_m16794_gshared)(__this /* static, unused */, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1<UnityEngine.TextEditor/TextEditOp>::System.Collections.IEqualityComparer.GetHashCode(System.Object)
extern "C" int32_t EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16795_gshared (EqualityComparer_1_t2250 * __this, Object_t * ___obj, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16795(__this, ___obj, method) (( int32_t (*) (EqualityComparer_1_t2250 *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_GetHashCode_m16795_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1<UnityEngine.TextEditor/TextEditOp>::System.Collections.IEqualityComparer.Equals(System.Object,System.Object)
extern "C" bool EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16796_gshared (EqualityComparer_1_t2250 * __this, Object_t * ___x, Object_t * ___y, const MethodInfo* method);
#define EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16796(__this, ___x, ___y, method) (( bool (*) (EqualityComparer_1_t2250 *, Object_t *, Object_t *, const MethodInfo*))EqualityComparer_1_System_Collections_IEqualityComparer_Equals_m16796_gshared)(__this, ___x, ___y, method)
// System.Collections.Generic.EqualityComparer`1<T> System.Collections.Generic.EqualityComparer`1<UnityEngine.TextEditor/TextEditOp>::get_Default()
extern "C" EqualityComparer_1_t2250 * EqualityComparer_1_get_Default_m16797_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define EqualityComparer_1_get_Default_m16797(__this /* static, unused */, method) (( EqualityComparer_1_t2250 * (*) (Object_t * /* static, unused */, const MethodInfo*))EqualityComparer_1_get_Default_m16797_gshared)(__this /* static, unused */, method)
