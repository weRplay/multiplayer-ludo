﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t187;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_14.h"
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m12675_gshared (Enumerator_t1952 * __this, List_1_t187 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m12675(__this, ___l, method) (( void (*) (Enumerator_t1952 *, List_1_t187 *, const MethodInfo*))Enumerator__ctor_m12675_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m12676_gshared (Enumerator_t1952 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m12676(__this, method) (( Object_t * (*) (Enumerator_t1952 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12676_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::Dispose()
extern "C" void Enumerator_Dispose_m12677_gshared (Enumerator_t1952 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m12677(__this, method) (( void (*) (Enumerator_t1952 *, const MethodInfo*))Enumerator_Dispose_m12677_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::VerifyState()
extern "C" void Enumerator_VerifyState_m12678_gshared (Enumerator_t1952 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m12678(__this, method) (( void (*) (Enumerator_t1952 *, const MethodInfo*))Enumerator_VerifyState_m12678_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::MoveNext()
extern "C" bool Enumerator_MoveNext_m12679_gshared (Enumerator_t1952 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m12679(__this, method) (( bool (*) (Enumerator_t1952 *, const MethodInfo*))Enumerator_MoveNext_m12679_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UIVertex>::get_Current()
extern "C" UIVertex_t191  Enumerator_get_Current_m12680_gshared (Enumerator_t1952 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m12680(__this, method) (( UIVertex_t191  (*) (Enumerator_t1952 *, const MethodInfo*))Enumerator_get_Current_m12680_gshared)(__this, method)
