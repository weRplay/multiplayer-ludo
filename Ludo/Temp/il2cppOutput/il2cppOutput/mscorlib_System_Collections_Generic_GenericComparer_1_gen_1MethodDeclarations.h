﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.Guid>
struct GenericComparer_1_t1778;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Guid.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.Guid>::.ctor()
extern "C" void GenericComparer_1__ctor_m10438_gshared (GenericComparer_1_t1778 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m10438(__this, method) (( void (*) (GenericComparer_1_t1778 *, const MethodInfo*))GenericComparer_1__ctor_m10438_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.Guid>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m17864_gshared (GenericComparer_1_t1778 * __this, Guid_t680  ___x, Guid_t680  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m17864(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t1778 *, Guid_t680 , Guid_t680 , const MethodInfo*))GenericComparer_1_Compare_m17864_gshared)(__this, ___x, ___y, method)
