﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Menu
struct Menu_t32;
// System.Collections.IEnumerator
struct IEnumerator_t51;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Menu::.ctor()
extern "C" void Menu__ctor_m116 (Menu_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Menu::Start()
extern "C" void Menu_Start_m117 (Menu_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Menu::CreateUser()
extern "C" void Menu_CreateUser_m118 (Menu_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Menu::PlayGame()
extern "C" void Menu_PlayGame_m119 (Menu_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Menu::CreateUniquePlayer(System.String)
extern "C" Object_t * Menu_CreateUniquePlayer_m120 (Menu_t32 * __this, String_t* ___username, const MethodInfo* method) IL2CPP_METHOD_ATTR;
