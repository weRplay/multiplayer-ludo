﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t8;
// UnityEngine.WWW
struct WWW_t9;
// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// Player/<SendPlayer1Position>c__IteratorC
struct  U3CSendPlayer1PositionU3Ec__IteratorC_t40  : public Object_t
{
	// UnityEngine.WWWForm Player/<SendPlayer1Position>c__IteratorC::<form>__0
	WWWForm_t8 * ___U3CformU3E__0_0;
	// System.Int32 Player/<SendPlayer1Position>c__IteratorC::a
	int32_t ___a_1;
	// System.Int32 Player/<SendPlayer1Position>c__IteratorC::b
	int32_t ___b_2;
	// System.Int32 Player/<SendPlayer1Position>c__IteratorC::diceNumber
	int32_t ___diceNumber_3;
	// System.Int32 Player/<SendPlayer1Position>c__IteratorC::playerPosition
	int32_t ___playerPosition_4;
	// UnityEngine.WWW Player/<SendPlayer1Position>c__IteratorC::<w>__1
	WWW_t9 * ___U3CwU3E__1_5;
	// System.Int32 Player/<SendPlayer1Position>c__IteratorC::$PC
	int32_t ___U24PC_6;
	// System.Object Player/<SendPlayer1Position>c__IteratorC::$current
	Object_t * ___U24current_7;
	// System.Int32 Player/<SendPlayer1Position>c__IteratorC::<$>a
	int32_t ___U3CU24U3Ea_8;
	// System.Int32 Player/<SendPlayer1Position>c__IteratorC::<$>b
	int32_t ___U3CU24U3Eb_9;
	// System.Int32 Player/<SendPlayer1Position>c__IteratorC::<$>diceNumber
	int32_t ___U3CU24U3EdiceNumber_10;
	// System.Int32 Player/<SendPlayer1Position>c__IteratorC::<$>playerPosition
	int32_t ___U3CU24U3EplayerPosition_11;
};
