﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Networking.Match.CreateMatchResponse
struct CreateMatchResponse_t489;
// System.IAsyncResult
struct IAsyncResult_t180;
// System.AsyncCallback
struct AsyncCallback_t181;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Void.h"

// UnityEngine.Networking.Match.NetworkMatch/ResponseDelegate`1<UnityEngine.Networking.Match.CreateMatchResponse>
struct  ResponseDelegate_1_t614  : public MulticastDelegate_t179
{
};
