﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Type[]
struct TypeU5BU5D_t516;

#include "mscorlib_System_Object.h"

// System.Collections.Generic.List`1<System.Type>
struct  List_1_t691  : public Object_t
{
	// T[] System.Collections.Generic.List`1<System.Type>::_items
	TypeU5BU5D_t516* ____items_1;
	// System.Int32 System.Collections.Generic.List`1<System.Type>::_size
	int32_t ____size_2;
	// System.Int32 System.Collections.Generic.List`1<System.Type>::_version
	int32_t ____version_3;
};
struct List_1_t691_StaticFields{
	// T[] System.Collections.Generic.List`1<System.Type>::EmptyArray
	TypeU5BU5D_t516* ___EmptyArray_4;
};
