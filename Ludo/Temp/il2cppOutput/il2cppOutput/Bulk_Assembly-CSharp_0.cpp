﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// BoardManager
struct BoardManager_t1;
// CountdownTimer/<AutomaticTurn>c__Iterator0
struct U3CAutomaticTurnU3Ec__Iterator0_t7;
// System.Object
struct Object_t;
// CountdownTimer
struct CountdownTimer_t10;
// Player
struct Player_t11;
// PlayerTwo
struct PlayerTwo_t12;
// UnityEngine.UI.Text
struct Text_t47;
// System.Collections.IEnumerator
struct IEnumerator_t51;
// DynamicScrollView/<MoveTowardsTarget>c__Iterator1
struct U3CMoveTowardsTargetU3Ec__Iterator1_t13;
// DynamicScrollView/<GetActiveUsers>c__Iterator2
struct U3CGetActiveUsersU3Ec__Iterator2_t15;
// DynamicScrollView
struct DynamicScrollView_t14;
// System.String
struct String_t;
// UnityEngine.GameObject
struct GameObject_t5;
// GameManager/<ResetBoard>c__Iterator3
struct U3CResetBoardU3Ec__Iterator3_t20;
// GameManager/<GetPlayer1Turn>c__Iterator4
struct U3CGetPlayer1TurnU3Ec__Iterator4_t21;
// GameManager/<GetPlayer2Turn>c__Iterator5
struct U3CGetPlayer2TurnU3Ec__Iterator5_t23;
// GameManager/<AutomaticMovePlayer1>c__Iterator6
struct U3CAutomaticMovePlayer1U3Ec__Iterator6_t24;
// GameManager/<AutomaticMovePlayer2>c__Iterator7
struct U3CAutomaticMovePlayer2U3Ec__Iterator7_t25;
// GameManager
struct GameManager_t22;
// GameMenu/<SetUserOnline>c__Iterator8
struct U3CSetUserOnlineU3Ec__Iterator8_t27;
// GameMenu/<GetActiveUsers>c__Iterator9
struct U3CGetActiveUsersU3Ec__Iterator9_t28;
// GameMenu
struct GameMenu_t29;
// Loader
struct Loader_t30;
// Menu/<CreateUniquePlayer>c__IteratorA
struct U3CCreateUniquePlayerU3Ec__IteratorA_t31;
// Menu
struct Menu_t32;
// MovingObject/<SmoothMovement>c__IteratorB
struct U3CSmoothMovementU3Ec__IteratorB_t34;
// MovingObject
struct MovingObject_t36;
// UnityEngine.BoxCollider2D
struct BoxCollider2D_t38;
// UnityEngine.Rigidbody2D
struct Rigidbody2D_t39;
// Player/<SendPlayer1Position>c__IteratorC
struct U3CSendPlayer1PositionU3Ec__IteratorC_t40;
// Player/<GetPlayer1Position>c__IteratorD
struct U3CGetPlayer1PositionU3Ec__IteratorD_t41;
// Player/<ExecuteAfterTime>c__IteratorE
struct U3CExecuteAfterTimeU3Ec__IteratorE_t42;
// PlayerTwo/<SendPlayer2Position>c__IteratorF
struct U3CSendPlayer2PositionU3Ec__IteratorF_t43;
// PlayerTwo/<GetPlayer2Position>c__Iterator10
struct U3CGetPlayer2PositionU3Ec__Iterator10_t44;
// ScrollItem/<CreateGame>c__Iterator11
struct U3CCreateGameU3Ec__Iterator11_t45;
// ScrollItem
struct ScrollItem_t46;
// Wall
struct Wall_t48;
// UnityEngine.SpriteRenderer
struct SpriteRenderer_t50;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
#include "AssemblyU2DCSharp_U3CModuleU3EMethodDeclarations.h"
#include "AssemblyU2DCSharp_BoardManager.h"
#include "AssemblyU2DCSharp_BoardManagerMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
#include "mscorlib_System_Int32.h"
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "mscorlib_System_Single.h"
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "mscorlib_System_String.h"
#include "UnityEngine_UnityEngine_Transform.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "AssemblyU2DCSharp_CountdownTimer_U3CAutomaticTurnU3Ec__Itera.h"
#include "AssemblyU2DCSharp_CountdownTimer_U3CAutomaticTurnU3Ec__IteraMethodDeclarations.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_Boolean.h"
#include "UnityEngine_UnityEngine_WWWFormMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PlayerPrefsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WWWMethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayerMethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayerTwoMethodDeclarations.h"
#include "mscorlib_System_UInt32.h"
#include "UnityEngine_UnityEngine_WWWForm.h"
#include "UnityEngine_UnityEngine_WWW.h"
#include "AssemblyU2DCSharp_CountdownTimer.h"
#include "AssemblyU2DCSharp_CountdownTimerMethodDeclarations.h"
#include "AssemblyU2DCSharp_Player.h"
#include "UnityEngine_UnityEngine_Coroutine.h"
#include "AssemblyU2DCSharp_PlayerTwo.h"
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
#include "mscorlib_System_NotSupportedException.h"
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text.h"
#include "UnityEngine_UI_UnityEngine_UI_TextMethodDeclarations.h"
#include "AssemblyU2DCSharp_GameManager.h"
#include "AssemblyU2DCSharp_GameManagerMethodDeclarations.h"
#include "AssemblyU2DCSharp_DynamicScrollView_U3CMoveTowardsTargetU3Ec.h"
#include "AssemblyU2DCSharp_DynamicScrollView_U3CMoveTowardsTargetU3EcMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRectMethodDeclarations.h"
#include "AssemblyU2DCSharp_DynamicScrollView.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect.h"
#include "AssemblyU2DCSharp_DynamicScrollView_U3CGetActiveUsersU3Ec__I.h"
#include "AssemblyU2DCSharp_DynamicScrollView_U3CGetActiveUsersU3Ec__IMethodDeclarations.h"
#include "AssemblyU2DCSharp_DynamicScrollViewMethodDeclarations.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Char.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroupMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UnityEngine_RectTransform.h"
#include "AssemblyU2DCSharp_GameManager_U3CResetBoardU3Ec__Iterator3.h"
#include "AssemblyU2DCSharp_GameManager_U3CResetBoardU3Ec__Iterator3MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
#include "AssemblyU2DCSharp_GameManager_U3CGetPlayer1TurnU3Ec__Iterato.h"
#include "AssemblyU2DCSharp_GameManager_U3CGetPlayer1TurnU3Ec__IteratoMethodDeclarations.h"
#include "mscorlib_System_SingleMethodDeclarations.h"
#include "mscorlib_System_Int32MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameManager_U3CGetPlayer2TurnU3Ec__Iterato.h"
#include "AssemblyU2DCSharp_GameManager_U3CGetPlayer2TurnU3Ec__IteratoMethodDeclarations.h"
#include "AssemblyU2DCSharp_GameManager_U3CAutomaticMovePlayer1U3Ec__I.h"
#include "AssemblyU2DCSharp_GameManager_U3CAutomaticMovePlayer1U3Ec__IMethodDeclarations.h"
#include "AssemblyU2DCSharp_GameManager_U3CAutomaticMovePlayer2U3Ec__I.h"
#include "AssemblyU2DCSharp_GameManager_U3CAutomaticMovePlayer2U3Ec__IMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "UnityEngine_UnityEngine_BehaviourMethodDeclarations.h"
#include "AssemblyU2DCSharp_GameMenu_U3CSetUserOnlineU3Ec__Iterator8.h"
#include "AssemblyU2DCSharp_GameMenu_U3CSetUserOnlineU3Ec__Iterator8MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameMenu_U3CGetActiveUsersU3Ec__Iterator9.h"
#include "AssemblyU2DCSharp_GameMenu_U3CGetActiveUsersU3Ec__Iterator9MethodDeclarations.h"
#include "AssemblyU2DCSharp_GameMenu.h"
#include "AssemblyU2DCSharp_GameMenuMethodDeclarations.h"
#include "AssemblyU2DCSharp_Loader.h"
#include "AssemblyU2DCSharp_LoaderMethodDeclarations.h"
#include "AssemblyU2DCSharp_Menu_U3CCreateUniquePlayerU3Ec__IteratorA.h"
#include "AssemblyU2DCSharp_Menu_U3CCreateUniquePlayerU3Ec__IteratorAMethodDeclarations.h"
#include "AssemblyU2DCSharp_Menu.h"
#include "AssemblyU2DCSharp_MenuMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_InputFieldMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField.h"
#include "AssemblyU2DCSharp_MovingObject_U3CSmoothMovementU3Ec__Iterat.h"
#include "AssemblyU2DCSharp_MovingObject_U3CSmoothMovementU3Ec__IteratMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody2DMethodDeclarations.h"
#include "AssemblyU2DCSharp_MovingObject.h"
#include "UnityEngine_UnityEngine_Rigidbody2D.h"
#include "AssemblyU2DCSharp_MovingObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_BoxCollider2D.h"
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
#include "UnityEngine_UnityEngine_LayerMaskMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Physics2DMethodDeclarations.h"
#include "UnityEngine_UnityEngine_LayerMask.h"
#include "AssemblyU2DCSharp_Player_U3CSendPlayer1PositionU3Ec__Iterato.h"
#include "AssemblyU2DCSharp_Player_U3CSendPlayer1PositionU3Ec__IteratoMethodDeclarations.h"
#include "AssemblyU2DCSharp_Player_U3CGetPlayer1PositionU3Ec__Iterator.h"
#include "AssemblyU2DCSharp_Player_U3CGetPlayer1PositionU3Ec__IteratorMethodDeclarations.h"
#include "AssemblyU2DCSharp_Player_U3CExecuteAfterTimeU3Ec__IteratorE.h"
#include "AssemblyU2DCSharp_Player_U3CExecuteAfterTimeU3Ec__IteratorEMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSecondsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSeconds.h"
#include "AssemblyU2DCSharp_PlayerTwo_U3CSendPlayer2PositionU3Ec__Iter.h"
#include "AssemblyU2DCSharp_PlayerTwo_U3CSendPlayer2PositionU3Ec__IterMethodDeclarations.h"
#include "AssemblyU2DCSharp_PlayerTwo_U3CGetPlayer2PositionU3Ec__Itera.h"
#include "AssemblyU2DCSharp_PlayerTwo_U3CGetPlayer2PositionU3Ec__IteraMethodDeclarations.h"
#include "AssemblyU2DCSharp_ScrollItem_U3CCreateGameU3Ec__Iterator11.h"
#include "AssemblyU2DCSharp_ScrollItem_U3CCreateGameU3Ec__Iterator11MethodDeclarations.h"
#include "AssemblyU2DCSharp_ScrollItem.h"
#include "AssemblyU2DCSharp_ScrollItemMethodDeclarations.h"
#include "AssemblyU2DCSharp_Wall.h"
#include "AssemblyU2DCSharp_WallMethodDeclarations.h"
#include "UnityEngine_UnityEngine_SpriteRenderer.h"
#include "UnityEngine_UnityEngine_SpriteRendererMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Sprite.h"

// !!0 UnityEngine.GameObject::GetComponent<System.Object>()
extern "C" Object_t * GameObject_GetComponent_TisObject_t_m274_gshared (GameObject_t5 * __this, const MethodInfo* method);
#define GameObject_GetComponent_TisObject_t_m274(__this, method) (( Object_t * (*) (GameObject_t5 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m274_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<Player>()
#define GameObject_GetComponent_TisPlayer_t11_m210(__this, method) (( Player_t11 * (*) (GameObject_t5 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m274_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<PlayerTwo>()
#define GameObject_GetComponent_TisPlayerTwo_t12_m211(__this, method) (( PlayerTwo_t12 * (*) (GameObject_t5 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m274_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<UnityEngine.UI.Text>()
#define GameObject_GetComponent_TisText_t47_m213(__this, method) (( Text_t47 * (*) (GameObject_t5 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m274_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C" Object_t * Object_Instantiate_TisObject_t_m275_gshared (Object_t * __this /* static, unused */, Object_t * p0, const MethodInfo* method);
#define Object_Instantiate_TisObject_t_m275(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Object_Instantiate_TisObject_t_m275_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.GameObject>(!!0)
#define Object_Instantiate_TisGameObject_t5_m233(__this /* static, unused */, p0, method) (( GameObject_t5 * (*) (Object_t * /* static, unused */, GameObject_t5 *, const MethodInfo*))Object_Instantiate_TisObject_t_m275_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m276_gshared (Component_t64 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m276(__this, method) (( Object_t * (*) (Component_t64 *, const MethodInfo*))Component_GetComponent_TisObject_t_m276_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<BoardManager>()
#define Component_GetComponent_TisBoardManager_t1_m248(__this, method) (( BoardManager_t1 * (*) (Component_t64 *, const MethodInfo*))Component_GetComponent_TisObject_t_m276_gshared)(__this, method)
// !!0 UnityEngine.GameObject::GetComponent<CountdownTimer>()
#define GameObject_GetComponent_TisCountdownTimer_t10_m249(__this, method) (( CountdownTimer_t10 * (*) (GameObject_t5 *, const MethodInfo*))GameObject_GetComponent_TisObject_t_m274_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.BoxCollider2D>()
#define Component_GetComponent_TisBoxCollider2D_t38_m264(__this, method) (( BoxCollider2D_t38 * (*) (Component_t64 *, const MethodInfo*))Component_GetComponent_TisObject_t_m276_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody2D>()
#define Component_GetComponent_TisRigidbody2D_t39_m265(__this, method) (( Rigidbody2D_t39 * (*) (Component_t64 *, const MethodInfo*))Component_GetComponent_TisObject_t_m276_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.SpriteRenderer>()
#define Component_GetComponent_TisSpriteRenderer_t50_m272(__this, method) (( SpriteRenderer_t50 * (*) (Component_t64 *, const MethodInfo*))Component_GetComponent_TisObject_t_m276_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void BoardManager::.ctor()
extern "C" void BoardManager__ctor_m0 (BoardManager_t1 * __this, const MethodInfo* method)
{
	{
		__this->___columns_2 = ((int32_t)15);
		__this->___rows_3 = ((int32_t)15);
		MonoBehaviour__ctor_m191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void BoardManager::Start()
extern "C" void BoardManager_Start_m1 (BoardManager_t1 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void BoardManager::InitializeRedPath()
extern "C" void BoardManager_InitializeRedPath_m2 (BoardManager_t1 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	{
		List_1_t6 * L_0 = (__this->___RedPath_9);
		Vector3_t35  L_1 = {0};
		Vector3__ctor_m192(&L_1, (0.0f), (9.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_0, L_1);
		V_0 = 8;
		V_1 = 1;
		V_2 = 1;
		goto IL_004e;
	}

IL_002a:
	{
		List_1_t6 * L_2 = (__this->___RedPath_9);
		int32_t L_3 = V_1;
		Vector3_t35  L_4 = {0};
		Vector3__ctor_m192(&L_4, (((float)L_3)), (8.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_2, L_4);
		int32_t L_5 = V_1;
		V_1 = ((int32_t)((int32_t)L_5+(int32_t)1));
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_7 = V_2;
		if ((((int32_t)L_7) < ((int32_t)6)))
		{
			goto IL_002a;
		}
	}
	{
		int32_t L_8 = V_0;
		V_0 = ((int32_t)((int32_t)L_8+(int32_t)1));
		V_3 = 6;
		goto IL_0081;
	}

IL_0060:
	{
		List_1_t6 * L_9 = (__this->___RedPath_9);
		int32_t L_10 = V_1;
		int32_t L_11 = V_0;
		Vector3_t35  L_12 = {0};
		Vector3__ctor_m192(&L_12, (((float)L_10)), (((float)L_11)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_9);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_9, L_12);
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
		int32_t L_14 = V_3;
		V_3 = ((int32_t)((int32_t)L_14+(int32_t)1));
	}

IL_0081:
	{
		int32_t L_15 = V_3;
		if ((((int32_t)L_15) < ((int32_t)((int32_t)12))))
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16-(int32_t)1));
		int32_t L_17 = V_1;
		V_1 = ((int32_t)((int32_t)L_17+(int32_t)1));
		List_1_t6 * L_18 = (__this->___RedPath_9);
		int32_t L_19 = V_1;
		int32_t L_20 = V_0;
		Vector3_t35  L_21 = {0};
		Vector3__ctor_m192(&L_21, (((float)L_19)), (((float)L_20)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_18);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_18, L_21);
		int32_t L_22 = V_1;
		V_1 = ((int32_t)((int32_t)L_22+(int32_t)1));
		V_4 = ((int32_t)13);
		goto IL_00da;
	}

IL_00b7:
	{
		List_1_t6 * L_23 = (__this->___RedPath_9);
		int32_t L_24 = V_1;
		int32_t L_25 = V_0;
		Vector3_t35  L_26 = {0};
		Vector3__ctor_m192(&L_26, (((float)L_24)), (((float)L_25)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_23);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_23, L_26);
		int32_t L_27 = V_0;
		V_0 = ((int32_t)((int32_t)L_27-(int32_t)1));
		int32_t L_28 = V_4;
		V_4 = ((int32_t)((int32_t)L_28+(int32_t)1));
	}

IL_00da:
	{
		int32_t L_29 = V_4;
		if ((((int32_t)L_29) < ((int32_t)((int32_t)19))))
		{
			goto IL_00b7;
		}
	}
	{
		int32_t L_30 = V_1;
		V_1 = ((int32_t)((int32_t)L_30+(int32_t)1));
		V_5 = ((int32_t)19);
		goto IL_0113;
	}

IL_00f0:
	{
		List_1_t6 * L_31 = (__this->___RedPath_9);
		int32_t L_32 = V_1;
		int32_t L_33 = V_0;
		Vector3_t35  L_34 = {0};
		Vector3__ctor_m192(&L_34, (((float)L_32)), (((float)L_33)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_31);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_31, L_34);
		int32_t L_35 = V_1;
		V_1 = ((int32_t)((int32_t)L_35+(int32_t)1));
		int32_t L_36 = V_5;
		V_5 = ((int32_t)((int32_t)L_36+(int32_t)1));
	}

IL_0113:
	{
		int32_t L_37 = V_5;
		if ((((int32_t)L_37) < ((int32_t)((int32_t)25))))
		{
			goto IL_00f0;
		}
	}
	{
		int32_t L_38 = V_0;
		V_0 = ((int32_t)((int32_t)L_38-(int32_t)1));
		int32_t L_39 = V_1;
		V_1 = ((int32_t)((int32_t)L_39-(int32_t)1));
		List_1_t6 * L_40 = (__this->___RedPath_9);
		int32_t L_41 = V_1;
		int32_t L_42 = V_0;
		Vector3_t35  L_43 = {0};
		Vector3__ctor_m192(&L_43, (((float)L_41)), (((float)L_42)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_40);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_40, L_43);
		int32_t L_44 = V_0;
		V_0 = ((int32_t)((int32_t)L_44-(int32_t)1));
		V_6 = ((int32_t)26);
		goto IL_016d;
	}

IL_014a:
	{
		List_1_t6 * L_45 = (__this->___RedPath_9);
		int32_t L_46 = V_1;
		int32_t L_47 = V_0;
		Vector3_t35  L_48 = {0};
		Vector3__ctor_m192(&L_48, (((float)L_46)), (((float)L_47)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_45);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_45, L_48);
		int32_t L_49 = V_1;
		V_1 = ((int32_t)((int32_t)L_49-(int32_t)1));
		int32_t L_50 = V_6;
		V_6 = ((int32_t)((int32_t)L_50+(int32_t)1));
	}

IL_016d:
	{
		int32_t L_51 = V_6;
		if ((((int32_t)L_51) < ((int32_t)((int32_t)32))))
		{
			goto IL_014a;
		}
	}
	{
		V_7 = ((int32_t)32);
		goto IL_01a2;
	}

IL_017f:
	{
		int32_t L_52 = V_0;
		V_0 = ((int32_t)((int32_t)L_52-(int32_t)1));
		List_1_t6 * L_53 = (__this->___RedPath_9);
		int32_t L_54 = V_1;
		int32_t L_55 = V_0;
		Vector3_t35  L_56 = {0};
		Vector3__ctor_m192(&L_56, (((float)L_54)), (((float)L_55)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_53);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_53, L_56);
		int32_t L_57 = V_7;
		V_7 = ((int32_t)((int32_t)L_57+(int32_t)1));
	}

IL_01a2:
	{
		int32_t L_58 = V_7;
		if ((((int32_t)L_58) < ((int32_t)((int32_t)38))))
		{
			goto IL_017f;
		}
	}
	{
		int32_t L_59 = V_1;
		V_1 = ((int32_t)((int32_t)L_59-(int32_t)1));
		List_1_t6 * L_60 = (__this->___RedPath_9);
		int32_t L_61 = V_1;
		int32_t L_62 = V_0;
		Vector3_t35  L_63 = {0};
		Vector3__ctor_m192(&L_63, (((float)L_61)), (((float)L_62)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_60);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_60, L_63);
		int32_t L_64 = V_1;
		V_1 = ((int32_t)((int32_t)L_64-(int32_t)1));
		V_8 = ((int32_t)39);
		goto IL_01f8;
	}

IL_01d5:
	{
		List_1_t6 * L_65 = (__this->___RedPath_9);
		int32_t L_66 = V_1;
		int32_t L_67 = V_0;
		Vector3_t35  L_68 = {0};
		Vector3__ctor_m192(&L_68, (((float)L_66)), (((float)L_67)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_65);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_65, L_68);
		int32_t L_69 = V_0;
		V_0 = ((int32_t)((int32_t)L_69+(int32_t)1));
		int32_t L_70 = V_8;
		V_8 = ((int32_t)((int32_t)L_70+(int32_t)1));
	}

IL_01f8:
	{
		int32_t L_71 = V_8;
		if ((((int32_t)L_71) < ((int32_t)((int32_t)45))))
		{
			goto IL_01d5;
		}
	}
	{
		V_9 = ((int32_t)45);
		goto IL_022d;
	}

IL_020a:
	{
		int32_t L_72 = V_1;
		V_1 = ((int32_t)((int32_t)L_72-(int32_t)1));
		List_1_t6 * L_73 = (__this->___RedPath_9);
		int32_t L_74 = V_1;
		int32_t L_75 = V_0;
		Vector3_t35  L_76 = {0};
		Vector3__ctor_m192(&L_76, (((float)L_74)), (((float)L_75)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_73);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_73, L_76);
		int32_t L_77 = V_9;
		V_9 = ((int32_t)((int32_t)L_77+(int32_t)1));
	}

IL_022d:
	{
		int32_t L_78 = V_9;
		if ((((int32_t)L_78) < ((int32_t)((int32_t)51))))
		{
			goto IL_020a;
		}
	}
	{
		int32_t L_79 = V_0;
		V_0 = ((int32_t)((int32_t)L_79+(int32_t)1));
		V_10 = ((int32_t)51);
		goto IL_0266;
	}

IL_0243:
	{
		List_1_t6 * L_80 = (__this->___RedPath_9);
		int32_t L_81 = V_1;
		int32_t L_82 = V_0;
		Vector3_t35  L_83 = {0};
		Vector3__ctor_m192(&L_83, (((float)L_81)), (((float)L_82)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_80);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_80, L_83);
		int32_t L_84 = V_1;
		V_1 = ((int32_t)((int32_t)L_84+(int32_t)1));
		int32_t L_85 = V_10;
		V_10 = ((int32_t)((int32_t)L_85+(int32_t)1));
	}

IL_0266:
	{
		int32_t L_86 = V_10;
		if ((((int32_t)L_86) < ((int32_t)((int32_t)58))))
		{
			goto IL_0243;
		}
	}
	{
		return;
	}
}
// System.Void BoardManager::InitializeBluePath()
extern "C" void BoardManager_InitializeBluePath_m3 (BoardManager_t1 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = 0;
	int32_t V_4 = 0;
	int32_t V_5 = 0;
	int32_t V_6 = 0;
	int32_t V_7 = 0;
	int32_t V_8 = 0;
	int32_t V_9 = 0;
	int32_t V_10 = 0;
	{
		List_1_t6 * L_0 = (__this->___BluePath_8);
		Vector3_t35  L_1 = {0};
		Vector3__ctor_m192(&L_1, (5.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_0, L_1);
		V_0 = 1;
		V_1 = 0;
		V_2 = 1;
		goto IL_004e;
	}

IL_002a:
	{
		List_1_t6 * L_2 = (__this->___BluePath_8);
		int32_t L_3 = V_0;
		Vector3_t35  L_4 = {0};
		Vector3__ctor_m192(&L_4, (6.0f), (((float)L_3)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_2, L_4);
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
		int32_t L_6 = V_2;
		V_2 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_004e:
	{
		int32_t L_7 = V_2;
		if ((((int32_t)L_7) < ((int32_t)6)))
		{
			goto IL_002a;
		}
	}
	{
		V_1 = 5;
		V_3 = 6;
		goto IL_007f;
	}

IL_005e:
	{
		List_1_t6 * L_8 = (__this->___BluePath_8);
		int32_t L_9 = V_1;
		int32_t L_10 = V_0;
		Vector3_t35  L_11 = {0};
		Vector3__ctor_m192(&L_11, (((float)L_9)), (((float)L_10)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_8);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_8, L_11);
		int32_t L_12 = V_1;
		V_1 = ((int32_t)((int32_t)L_12-(int32_t)1));
		int32_t L_13 = V_3;
		V_3 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_007f:
	{
		int32_t L_14 = V_3;
		if ((((int32_t)L_14) < ((int32_t)((int32_t)12))))
		{
			goto IL_005e;
		}
	}
	{
		int32_t L_15 = V_1;
		V_1 = ((int32_t)((int32_t)L_15+(int32_t)1));
		int32_t L_16 = V_0;
		V_0 = ((int32_t)((int32_t)L_16+(int32_t)1));
		List_1_t6 * L_17 = (__this->___BluePath_8);
		int32_t L_18 = V_1;
		int32_t L_19 = V_0;
		Vector3_t35  L_20 = {0};
		Vector3__ctor_m192(&L_20, (((float)L_18)), (((float)L_19)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_17);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_17, L_20);
		int32_t L_21 = V_0;
		V_0 = ((int32_t)((int32_t)L_21+(int32_t)1));
		V_4 = ((int32_t)13);
		goto IL_00d8;
	}

IL_00b5:
	{
		List_1_t6 * L_22 = (__this->___BluePath_8);
		int32_t L_23 = V_1;
		int32_t L_24 = V_0;
		Vector3_t35  L_25 = {0};
		Vector3__ctor_m192(&L_25, (((float)L_23)), (((float)L_24)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_22);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_22, L_25);
		int32_t L_26 = V_1;
		V_1 = ((int32_t)((int32_t)L_26+(int32_t)1));
		int32_t L_27 = V_4;
		V_4 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_00d8:
	{
		int32_t L_28 = V_4;
		if ((((int32_t)L_28) < ((int32_t)((int32_t)19))))
		{
			goto IL_00b5;
		}
	}
	{
		int32_t L_29 = V_0;
		V_0 = ((int32_t)((int32_t)L_29+(int32_t)1));
		V_5 = ((int32_t)19);
		goto IL_0111;
	}

IL_00ee:
	{
		List_1_t6 * L_30 = (__this->___BluePath_8);
		int32_t L_31 = V_1;
		int32_t L_32 = V_0;
		Vector3_t35  L_33 = {0};
		Vector3__ctor_m192(&L_33, (((float)L_31)), (((float)L_32)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_30);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_30, L_33);
		int32_t L_34 = V_0;
		V_0 = ((int32_t)((int32_t)L_34+(int32_t)1));
		int32_t L_35 = V_5;
		V_5 = ((int32_t)((int32_t)L_35+(int32_t)1));
	}

IL_0111:
	{
		int32_t L_36 = V_5;
		if ((((int32_t)L_36) < ((int32_t)((int32_t)25))))
		{
			goto IL_00ee;
		}
	}
	{
		int32_t L_37 = V_1;
		V_1 = ((int32_t)((int32_t)L_37+(int32_t)1));
		int32_t L_38 = V_0;
		V_0 = ((int32_t)((int32_t)L_38-(int32_t)1));
		List_1_t6 * L_39 = (__this->___BluePath_8);
		int32_t L_40 = V_1;
		int32_t L_41 = V_0;
		Vector3_t35  L_42 = {0};
		Vector3__ctor_m192(&L_42, (((float)L_40)), (((float)L_41)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_39);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_39, L_42);
		int32_t L_43 = V_1;
		V_1 = ((int32_t)((int32_t)L_43+(int32_t)1));
		V_6 = ((int32_t)26);
		goto IL_016b;
	}

IL_0148:
	{
		List_1_t6 * L_44 = (__this->___BluePath_8);
		int32_t L_45 = V_1;
		int32_t L_46 = V_0;
		Vector3_t35  L_47 = {0};
		Vector3__ctor_m192(&L_47, (((float)L_45)), (((float)L_46)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_44);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_44, L_47);
		int32_t L_48 = V_0;
		V_0 = ((int32_t)((int32_t)L_48-(int32_t)1));
		int32_t L_49 = V_6;
		V_6 = ((int32_t)((int32_t)L_49+(int32_t)1));
	}

IL_016b:
	{
		int32_t L_50 = V_6;
		if ((((int32_t)L_50) < ((int32_t)((int32_t)32))))
		{
			goto IL_0148;
		}
	}
	{
		V_7 = ((int32_t)32);
		goto IL_01a0;
	}

IL_017d:
	{
		int32_t L_51 = V_1;
		V_1 = ((int32_t)((int32_t)L_51+(int32_t)1));
		List_1_t6 * L_52 = (__this->___BluePath_8);
		int32_t L_53 = V_1;
		int32_t L_54 = V_0;
		Vector3_t35  L_55 = {0};
		Vector3__ctor_m192(&L_55, (((float)L_53)), (((float)L_54)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_52);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_52, L_55);
		int32_t L_56 = V_7;
		V_7 = ((int32_t)((int32_t)L_56+(int32_t)1));
	}

IL_01a0:
	{
		int32_t L_57 = V_7;
		if ((((int32_t)L_57) < ((int32_t)((int32_t)38))))
		{
			goto IL_017d;
		}
	}
	{
		int32_t L_58 = V_0;
		V_0 = ((int32_t)((int32_t)L_58-(int32_t)1));
		List_1_t6 * L_59 = (__this->___BluePath_8);
		int32_t L_60 = V_1;
		int32_t L_61 = V_0;
		Vector3_t35  L_62 = {0};
		Vector3__ctor_m192(&L_62, (((float)L_60)), (((float)L_61)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_59);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_59, L_62);
		int32_t L_63 = V_0;
		V_0 = ((int32_t)((int32_t)L_63-(int32_t)1));
		V_8 = ((int32_t)39);
		goto IL_01f6;
	}

IL_01d3:
	{
		List_1_t6 * L_64 = (__this->___BluePath_8);
		int32_t L_65 = V_1;
		int32_t L_66 = V_0;
		Vector3_t35  L_67 = {0};
		Vector3__ctor_m192(&L_67, (((float)L_65)), (((float)L_66)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_64);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_64, L_67);
		int32_t L_68 = V_1;
		V_1 = ((int32_t)((int32_t)L_68-(int32_t)1));
		int32_t L_69 = V_8;
		V_8 = ((int32_t)((int32_t)L_69+(int32_t)1));
	}

IL_01f6:
	{
		int32_t L_70 = V_8;
		if ((((int32_t)L_70) < ((int32_t)((int32_t)45))))
		{
			goto IL_01d3;
		}
	}
	{
		V_9 = ((int32_t)45);
		goto IL_022b;
	}

IL_0208:
	{
		int32_t L_71 = V_0;
		V_0 = ((int32_t)((int32_t)L_71-(int32_t)1));
		List_1_t6 * L_72 = (__this->___BluePath_8);
		int32_t L_73 = V_1;
		int32_t L_74 = V_0;
		Vector3_t35  L_75 = {0};
		Vector3__ctor_m192(&L_75, (((float)L_73)), (((float)L_74)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_72);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_72, L_75);
		int32_t L_76 = V_9;
		V_9 = ((int32_t)((int32_t)L_76+(int32_t)1));
	}

IL_022b:
	{
		int32_t L_77 = V_9;
		if ((((int32_t)L_77) < ((int32_t)((int32_t)51))))
		{
			goto IL_0208;
		}
	}
	{
		int32_t L_78 = V_1;
		V_1 = ((int32_t)((int32_t)L_78-(int32_t)1));
		List_1_t6 * L_79 = (__this->___BluePath_8);
		int32_t L_80 = V_1;
		int32_t L_81 = V_0;
		Vector3_t35  L_82 = {0};
		Vector3__ctor_m192(&L_82, (((float)L_80)), (((float)L_81)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_79);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_79, L_82);
		V_10 = ((int32_t)52);
		goto IL_027d;
	}

IL_025a:
	{
		int32_t L_83 = V_0;
		V_0 = ((int32_t)((int32_t)L_83+(int32_t)1));
		List_1_t6 * L_84 = (__this->___BluePath_8);
		int32_t L_85 = V_1;
		int32_t L_86 = V_0;
		Vector3_t35  L_87 = {0};
		Vector3__ctor_m192(&L_87, (((float)L_85)), (((float)L_86)), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_84);
		VirtActionInvoker1< Vector3_t35  >::Invoke(22 /* System.Void System.Collections.Generic.List`1<UnityEngine.Vector3>::Add(!0) */, L_84, L_87);
		int32_t L_88 = V_10;
		V_10 = ((int32_t)((int32_t)L_88+(int32_t)1));
	}

IL_027d:
	{
		int32_t L_89 = V_10;
		if ((((int32_t)L_89) < ((int32_t)((int32_t)58))))
		{
			goto IL_025a;
		}
	}
	{
		return;
	}
}
// System.Void BoardManager::BoardSetup()
extern TypeInfo* GameObject_t5_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral0;
extern "C" void BoardManager_BoardSetup_m4 (BoardManager_t1 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		_stringLiteral0 = il2cpp_codegen_string_literal_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	int32_t V_1 = 0;
	GameObject_t5 * V_2 = {0};
	GameObject_t5 * V_3 = {0};
	{
		GameObject_t5 * L_0 = (GameObject_t5 *)il2cpp_codegen_object_new (GameObject_t5_il2cpp_TypeInfo_var);
		GameObject__ctor_m193(L_0, _stringLiteral0, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t3 * L_1 = GameObject_get_transform_m194(L_0, /*hidden argument*/NULL);
		__this->___boardHolder_4 = L_1;
		V_0 = (-1);
		goto IL_0252;
	}

IL_001c:
	{
		V_1 = (-1);
		goto IL_0240;
	}

IL_0023:
	{
		GameObjectU5BU5D_t4* L_2 = (__this->___outerWallTiles_6);
		GameObjectU5BU5D_t4* L_3 = (__this->___outerWallTiles_6);
		NullCheck(L_3);
		int32_t L_4 = Random_Range_m195(NULL /*static, unused*/, 0, (((int32_t)(((Array_t *)L_3)->max_length))), /*hidden argument*/NULL);
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_4);
		int32_t L_5 = L_4;
		V_2 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_2, L_5, sizeof(GameObject_t5 *)));
		int32_t L_6 = V_0;
		if ((((int32_t)L_6) == ((int32_t)(-1))))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_7 = V_0;
		int32_t L_8 = (__this->___columns_2);
		if ((((int32_t)L_7) == ((int32_t)L_8)))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_9 = V_1;
		if ((((int32_t)L_9) == ((int32_t)(-1))))
		{
			goto IL_005f;
		}
	}
	{
		int32_t L_10 = V_1;
		int32_t L_11 = (__this->___rows_3);
		if ((!(((uint32_t)L_10) == ((uint32_t)L_11))))
		{
			goto IL_0075;
		}
	}

IL_005f:
	{
		GameObjectU5BU5D_t4* L_12 = (__this->___outerWallTiles_6);
		GameObjectU5BU5D_t4* L_13 = (__this->___outerWallTiles_6);
		NullCheck(L_13);
		int32_t L_14 = Random_Range_m195(NULL /*static, unused*/, 0, (((int32_t)(((Array_t *)L_13)->max_length))), /*hidden argument*/NULL);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, L_14);
		int32_t L_15 = L_14;
		V_2 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_12, L_15, sizeof(GameObject_t5 *)));
	}

IL_0075:
	{
		int32_t L_16 = V_0;
		if ((((int32_t)L_16) <= ((int32_t)5)))
		{
			goto IL_0142;
		}
	}
	{
		int32_t L_17 = V_0;
		if ((((int32_t)L_17) >= ((int32_t)((int32_t)9))))
		{
			goto IL_0142;
		}
	}
	{
		int32_t L_18 = V_1;
		if ((((int32_t)L_18) <= ((int32_t)(-1))))
		{
			goto IL_00e0;
		}
	}
	{
		int32_t L_19 = V_1;
		if ((((int32_t)L_19) >= ((int32_t)6)))
		{
			goto IL_00e0;
		}
	}
	{
		GameObjectU5BU5D_t4* L_20 = (__this->___floorTiles_5);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 3);
		int32_t L_21 = 3;
		V_2 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_20, L_21, sizeof(GameObject_t5 *)));
		int32_t L_22 = V_0;
		if ((!(((uint32_t)L_22) == ((uint32_t)6))))
		{
			goto IL_00b2;
		}
	}
	{
		int32_t L_23 = V_1;
		if ((!(((uint32_t)L_23) == ((uint32_t)1))))
		{
			goto IL_00b2;
		}
	}
	{
		GameObjectU5BU5D_t4* L_24 = (__this->___floorTiles_5);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 1);
		int32_t L_25 = 1;
		V_2 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_24, L_25, sizeof(GameObject_t5 *)));
	}

IL_00b2:
	{
		int32_t L_26 = V_0;
		if ((!(((uint32_t)L_26) == ((uint32_t)8))))
		{
			goto IL_00c9;
		}
	}
	{
		int32_t L_27 = V_1;
		if ((!(((uint32_t)L_27) == ((uint32_t)2))))
		{
			goto IL_00c9;
		}
	}
	{
		GameObjectU5BU5D_t4* L_28 = (__this->___floorTiles_5);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 2);
		int32_t L_29 = 2;
		V_2 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_28, L_29, sizeof(GameObject_t5 *)));
	}

IL_00c9:
	{
		int32_t L_30 = V_0;
		if ((!(((uint32_t)L_30) == ((uint32_t)7))))
		{
			goto IL_00e0;
		}
	}
	{
		int32_t L_31 = V_1;
		if ((((int32_t)L_31) <= ((int32_t)0)))
		{
			goto IL_00e0;
		}
	}
	{
		GameObjectU5BU5D_t4* L_32 = (__this->___floorTiles_5);
		NullCheck(L_32);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_32, 2);
		int32_t L_33 = 2;
		V_2 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_32, L_33, sizeof(GameObject_t5 *)));
	}

IL_00e0:
	{
		int32_t L_34 = V_1;
		if ((((int32_t)L_34) <= ((int32_t)8)))
		{
			goto IL_0142;
		}
	}
	{
		int32_t L_35 = V_1;
		if ((((int32_t)L_35) >= ((int32_t)((int32_t)15))))
		{
			goto IL_0142;
		}
	}
	{
		GameObjectU5BU5D_t4* L_36 = (__this->___floorTiles_5);
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 3);
		int32_t L_37 = 3;
		V_2 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_36, L_37, sizeof(GameObject_t5 *)));
		int32_t L_38 = V_0;
		if ((!(((uint32_t)L_38) == ((uint32_t)8))))
		{
			goto IL_0110;
		}
	}
	{
		int32_t L_39 = V_1;
		if ((!(((uint32_t)L_39) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_0110;
		}
	}
	{
		GameObjectU5BU5D_t4* L_40 = (__this->___floorTiles_5);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 6);
		int32_t L_41 = 6;
		V_2 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_40, L_41, sizeof(GameObject_t5 *)));
	}

IL_0110:
	{
		int32_t L_42 = V_0;
		if ((!(((uint32_t)L_42) == ((uint32_t)6))))
		{
			goto IL_0129;
		}
	}
	{
		int32_t L_43 = V_1;
		if ((!(((uint32_t)L_43) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_0129;
		}
	}
	{
		GameObjectU5BU5D_t4* L_44 = (__this->___floorTiles_5);
		NullCheck(L_44);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_44, ((int32_t)9));
		int32_t L_45 = ((int32_t)9);
		V_2 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_44, L_45, sizeof(GameObject_t5 *)));
	}

IL_0129:
	{
		int32_t L_46 = V_0;
		if ((!(((uint32_t)L_46) == ((uint32_t)7))))
		{
			goto IL_0142;
		}
	}
	{
		int32_t L_47 = V_1;
		if ((((int32_t)L_47) >= ((int32_t)((int32_t)14))))
		{
			goto IL_0142;
		}
	}
	{
		GameObjectU5BU5D_t4* L_48 = (__this->___floorTiles_5);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, ((int32_t)9));
		int32_t L_49 = ((int32_t)9);
		V_2 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_48, L_49, sizeof(GameObject_t5 *)));
	}

IL_0142:
	{
		int32_t L_50 = V_1;
		if ((((int32_t)L_50) <= ((int32_t)5)))
		{
			goto IL_020c;
		}
	}
	{
		int32_t L_51 = V_1;
		if ((((int32_t)L_51) >= ((int32_t)((int32_t)9))))
		{
			goto IL_020c;
		}
	}
	{
		int32_t L_52 = V_0;
		if ((((int32_t)L_52) <= ((int32_t)(-1))))
		{
			goto IL_01ad;
		}
	}
	{
		int32_t L_53 = V_0;
		if ((((int32_t)L_53) >= ((int32_t)6)))
		{
			goto IL_01ad;
		}
	}
	{
		GameObjectU5BU5D_t4* L_54 = (__this->___floorTiles_5);
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, 3);
		int32_t L_55 = 3;
		V_2 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_54, L_55, sizeof(GameObject_t5 *)));
		int32_t L_56 = V_1;
		if ((!(((uint32_t)L_56) == ((uint32_t)6))))
		{
			goto IL_017f;
		}
	}
	{
		int32_t L_57 = V_0;
		if ((!(((uint32_t)L_57) == ((uint32_t)2))))
		{
			goto IL_017f;
		}
	}
	{
		GameObjectU5BU5D_t4* L_58 = (__this->___floorTiles_5);
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, 5);
		int32_t L_59 = 5;
		V_2 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_58, L_59, sizeof(GameObject_t5 *)));
	}

IL_017f:
	{
		int32_t L_60 = V_1;
		if ((!(((uint32_t)L_60) == ((uint32_t)8))))
		{
			goto IL_0196;
		}
	}
	{
		int32_t L_61 = V_0;
		if ((!(((uint32_t)L_61) == ((uint32_t)1))))
		{
			goto IL_0196;
		}
	}
	{
		GameObjectU5BU5D_t4* L_62 = (__this->___floorTiles_5);
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, 4);
		int32_t L_63 = 4;
		V_2 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_62, L_63, sizeof(GameObject_t5 *)));
	}

IL_0196:
	{
		int32_t L_64 = V_1;
		if ((!(((uint32_t)L_64) == ((uint32_t)7))))
		{
			goto IL_01ad;
		}
	}
	{
		int32_t L_65 = V_0;
		if ((((int32_t)L_65) <= ((int32_t)0)))
		{
			goto IL_01ad;
		}
	}
	{
		GameObjectU5BU5D_t4* L_66 = (__this->___floorTiles_5);
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, 5);
		int32_t L_67 = 5;
		V_2 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_66, L_67, sizeof(GameObject_t5 *)));
	}

IL_01ad:
	{
		int32_t L_68 = V_0;
		if ((((int32_t)L_68) <= ((int32_t)8)))
		{
			goto IL_020c;
		}
	}
	{
		int32_t L_69 = V_0;
		if ((((int32_t)L_69) >= ((int32_t)((int32_t)15))))
		{
			goto IL_020c;
		}
	}
	{
		GameObjectU5BU5D_t4* L_70 = (__this->___floorTiles_5);
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, 3);
		int32_t L_71 = 3;
		V_2 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_70, L_71, sizeof(GameObject_t5 *)));
		int32_t L_72 = V_1;
		if ((!(((uint32_t)L_72) == ((uint32_t)8))))
		{
			goto IL_01dd;
		}
	}
	{
		int32_t L_73 = V_0;
		if ((!(((uint32_t)L_73) == ((uint32_t)((int32_t)12)))))
		{
			goto IL_01dd;
		}
	}
	{
		GameObjectU5BU5D_t4* L_74 = (__this->___floorTiles_5);
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, 8);
		int32_t L_75 = 8;
		V_2 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_74, L_75, sizeof(GameObject_t5 *)));
	}

IL_01dd:
	{
		int32_t L_76 = V_1;
		if ((!(((uint32_t)L_76) == ((uint32_t)6))))
		{
			goto IL_01f5;
		}
	}
	{
		int32_t L_77 = V_0;
		if ((!(((uint32_t)L_77) == ((uint32_t)((int32_t)13)))))
		{
			goto IL_01f5;
		}
	}
	{
		GameObjectU5BU5D_t4* L_78 = (__this->___floorTiles_5);
		NullCheck(L_78);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_78, 0);
		int32_t L_79 = 0;
		V_2 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_78, L_79, sizeof(GameObject_t5 *)));
	}

IL_01f5:
	{
		int32_t L_80 = V_1;
		if ((!(((uint32_t)L_80) == ((uint32_t)7))))
		{
			goto IL_020c;
		}
	}
	{
		int32_t L_81 = V_0;
		if ((((int32_t)L_81) <= ((int32_t)8)))
		{
			goto IL_020c;
		}
	}
	{
		GameObjectU5BU5D_t4* L_82 = (__this->___floorTiles_5);
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, 8);
		int32_t L_83 = 8;
		V_2 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_82, L_83, sizeof(GameObject_t5 *)));
	}

IL_020c:
	{
		GameObject_t5 * L_84 = V_2;
		int32_t L_85 = V_0;
		int32_t L_86 = V_1;
		Vector3_t35  L_87 = {0};
		Vector3__ctor_m192(&L_87, (((float)L_85)), (((float)L_86)), (0.0f), /*hidden argument*/NULL);
		Quaternion_t53  L_88 = Quaternion_get_identity_m196(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t54 * L_89 = Object_Instantiate_m197(NULL /*static, unused*/, L_84, L_87, L_88, /*hidden argument*/NULL);
		V_3 = ((GameObject_t5 *)IsInstSealed(L_89, GameObject_t5_il2cpp_TypeInfo_var));
		GameObject_t5 * L_90 = V_3;
		NullCheck(L_90);
		Transform_t3 * L_91 = GameObject_get_transform_m194(L_90, /*hidden argument*/NULL);
		Transform_t3 * L_92 = (__this->___boardHolder_4);
		NullCheck(L_91);
		Transform_SetParent_m198(L_91, L_92, /*hidden argument*/NULL);
		int32_t L_93 = V_1;
		V_1 = ((int32_t)((int32_t)L_93+(int32_t)1));
	}

IL_0240:
	{
		int32_t L_94 = V_1;
		int32_t L_95 = (__this->___rows_3);
		if ((((int32_t)L_94) < ((int32_t)((int32_t)((int32_t)L_95+(int32_t)1)))))
		{
			goto IL_0023;
		}
	}
	{
		int32_t L_96 = V_0;
		V_0 = ((int32_t)((int32_t)L_96+(int32_t)1));
	}

IL_0252:
	{
		int32_t L_97 = V_0;
		int32_t L_98 = (__this->___columns_2);
		if ((((int32_t)L_97) < ((int32_t)((int32_t)((int32_t)L_98+(int32_t)1)))))
		{
			goto IL_001c;
		}
	}
	{
		return;
	}
}
// System.Void BoardManager::SetupScene()
extern "C" void BoardManager_SetupScene_m5 (BoardManager_t1 * __this, const MethodInfo* method)
{
	{
		BoardManager_BoardSetup_m4(__this, /*hidden argument*/NULL);
		BoardManager_InitializeBluePath_m3(__this, /*hidden argument*/NULL);
		BoardManager_InitializeRedPath_m2(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CountdownTimer/<AutomaticTurn>c__Iterator0::.ctor()
extern "C" void U3CAutomaticTurnU3Ec__Iterator0__ctor_m6 (U3CAutomaticTurnU3Ec__Iterator0_t7 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object CountdownTimer/<AutomaticTurn>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CAutomaticTurnU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m7 (U3CAutomaticTurnU3Ec__Iterator0_t7 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object CountdownTimer/<AutomaticTurn>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CAutomaticTurnU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m8 (U3CAutomaticTurnU3Ec__Iterator0_t7 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean CountdownTimer/<AutomaticTurn>c__Iterator0::MoveNext()
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern TypeInfo* CountdownTimer_t10_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral4;
extern Il2CppCodeGenString* _stringLiteral5;
extern Il2CppCodeGenString* _stringLiteral6;
extern Il2CppCodeGenString* _stringLiteral7;
extern "C" bool U3CAutomaticTurnU3Ec__Iterator0_MoveNext_m9 (U3CAutomaticTurnU3Ec__Iterator0_t7 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		CountdownTimer_t10_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral4 = il2cpp_codegen_string_literal_from_index(4);
		_stringLiteral5 = il2cpp_codegen_string_literal_from_index(5);
		_stringLiteral6 = il2cpp_codegen_string_literal_from_index(6);
		_stringLiteral7 = il2cpp_codegen_string_literal_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00ee;
		}
	}
	{
		goto IL_013b;
	}

IL_0021:
	{
		WWWForm_t8 * L_2 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_2, /*hidden argument*/NULL);
		__this->___U3CformU3E__0_0 = L_2;
		String_t* L_3 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral1, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		WWWForm_t8 * L_4 = (__this->___U3CformU3E__0_0);
		String_t* L_5 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral1, /*hidden argument*/NULL);
		NullCheck(L_4);
		WWWForm_AddField_m203(L_4, _stringLiteral2, L_5, /*hidden argument*/NULL);
		String_t* L_6 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_7 = String_op_Equality_m204(NULL /*static, unused*/, L_6, _stringLiteral4, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_008d;
		}
	}
	{
		WWWForm_t8 * L_8 = (__this->___U3CformU3E__0_0);
		String_t* L_9 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral3, /*hidden argument*/NULL);
		NullCheck(L_8);
		WWWForm_AddField_m203(L_8, _stringLiteral3, L_9, /*hidden argument*/NULL);
		goto IL_00c0;
	}

IL_008d:
	{
		String_t* L_10 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m204(NULL /*static, unused*/, L_10, _stringLiteral5, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00c0;
		}
	}
	{
		WWWForm_t8 * L_12 = (__this->___U3CformU3E__0_0);
		String_t* L_13 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral3, /*hidden argument*/NULL);
		NullCheck(L_12);
		WWWForm_AddField_m203(L_12, _stringLiteral3, L_13, /*hidden argument*/NULL);
	}

IL_00c0:
	{
		WWWForm_t8 * L_14 = (__this->___U3CformU3E__0_0);
		WWW_t9 * L_15 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_15, _stringLiteral6, L_14, /*hidden argument*/NULL);
		__this->___U3CwU3E__1_1 = L_15;
		WWW_t9 * L_16 = (__this->___U3CwU3E__1_1);
		__this->___U24current_3 = L_16;
		__this->___U24PC_2 = 1;
		goto IL_013d;
	}

IL_00ee:
	{
		WWW_t9 * L_17 = (__this->___U3CwU3E__1_1);
		NullCheck(L_17);
		String_t* L_18 = WWW_get_text_m206(L_17, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_19 = String_op_Equality_m204(NULL /*static, unused*/, L_18, _stringLiteral7, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_0134;
		}
	}
	{
		CountdownTimer_t10 * L_20 = (__this->___U3CU3Ef__this_4);
		IL2CPP_RUNTIME_CLASS_INIT(CountdownTimer_t10_il2cpp_TypeInfo_var);
		Player_t11 * L_21 = ((CountdownTimer_t10_StaticFields*)CountdownTimer_t10_il2cpp_TypeInfo_var->static_fields)->___playerScript_6;
		NullCheck(L_21);
		Object_t * L_22 = Player_GetPlayer1Position_m156(L_21, /*hidden argument*/NULL);
		NullCheck(L_20);
		MonoBehaviour_StartCoroutine_m207(L_20, L_22, /*hidden argument*/NULL);
		CountdownTimer_t10 * L_23 = (__this->___U3CU3Ef__this_4);
		PlayerTwo_t12 * L_24 = ((CountdownTimer_t10_StaticFields*)CountdownTimer_t10_il2cpp_TypeInfo_var->static_fields)->___playerTwoScript_7;
		NullCheck(L_24);
		Object_t * L_25 = PlayerTwo_GetPlayer2Position_m177(L_24, /*hidden argument*/NULL);
		NullCheck(L_23);
		MonoBehaviour_StartCoroutine_m207(L_23, L_25, /*hidden argument*/NULL);
	}

IL_0134:
	{
		__this->___U24PC_2 = (-1);
	}

IL_013b:
	{
		return 0;
	}

IL_013d:
	{
		return 1;
	}
	// Dead block : IL_013f: ldloc.1
}
// System.Void CountdownTimer/<AutomaticTurn>c__Iterator0::Dispose()
extern "C" void U3CAutomaticTurnU3Ec__Iterator0_Dispose_m10 (U3CAutomaticTurnU3Ec__Iterator0_t7 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void CountdownTimer/<AutomaticTurn>c__Iterator0::Reset()
extern TypeInfo* NotSupportedException_t55_il2cpp_TypeInfo_var;
extern "C" void U3CAutomaticTurnU3Ec__Iterator0_Reset_m11 (U3CAutomaticTurnU3Ec__Iterator0_t7 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t55 * L_0 = (NotSupportedException_t55 *)il2cpp_codegen_object_new (NotSupportedException_t55_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m208(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void CountdownTimer::.ctor()
extern "C" void CountdownTimer__ctor_m12 (CountdownTimer_t10 * __this, const MethodInfo* method)
{
	{
		__this->___CountdownNumber_8 = ((int32_t)10);
		MonoBehaviour__ctor_m191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void CountdownTimer::.cctor()
extern TypeInfo* CountdownTimer_t10_il2cpp_TypeInfo_var;
extern "C" void CountdownTimer__cctor_m13 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CountdownTimer_t10_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		((CountdownTimer_t10_StaticFields*)CountdownTimer_t10_il2cpp_TypeInfo_var->static_fields)->___timeLeft_4 = (10.0f);
		return;
	}
}
// System.Void CountdownTimer::Start()
extern TypeInfo* CountdownTimer_t10_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayer_t11_m210_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayerTwo_t12_m211_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral5;
extern Il2CppCodeGenString* _stringLiteral8;
extern "C" void CountdownTimer_Start_m14 (CountdownTimer_t10 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CountdownTimer_t10_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		GameObject_GetComponent_TisPlayer_t11_m210_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483649);
		GameObject_GetComponent_TisPlayerTwo_t12_m211_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483650);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral5 = il2cpp_codegen_string_literal_from_index(5);
		_stringLiteral8 = il2cpp_codegen_string_literal_from_index(8);
		s_Il2CppMethodIntialized = true;
	}
	{
		GameObject_t5 * L_0 = GameObject_Find_m209(NULL /*static, unused*/, _stringLiteral3, /*hidden argument*/NULL);
		NullCheck(L_0);
		Player_t11 * L_1 = GameObject_GetComponent_TisPlayer_t11_m210(L_0, /*hidden argument*/GameObject_GetComponent_TisPlayer_t11_m210_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(CountdownTimer_t10_il2cpp_TypeInfo_var);
		((CountdownTimer_t10_StaticFields*)CountdownTimer_t10_il2cpp_TypeInfo_var->static_fields)->___playerScript_6 = L_1;
		GameObject_t5 * L_2 = GameObject_Find_m209(NULL /*static, unused*/, _stringLiteral5, /*hidden argument*/NULL);
		NullCheck(L_2);
		PlayerTwo_t12 * L_3 = GameObject_GetComponent_TisPlayerTwo_t12_m211(L_2, /*hidden argument*/GameObject_GetComponent_TisPlayerTwo_t12_m211_MethodInfo_var);
		((CountdownTimer_t10_StaticFields*)CountdownTimer_t10_il2cpp_TypeInfo_var->static_fields)->___playerTwoScript_7 = L_3;
		GameObject_t5 * L_4 = GameObject_Find_m209(NULL /*static, unused*/, _stringLiteral8, /*hidden argument*/NULL);
		__this->___myTimeLeftText_2 = L_4;
		return;
	}
}
// System.Void CountdownTimer::Update()
extern TypeInfo* Int32_t56_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t47_m213_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral9;
extern "C" void CountdownTimer_Update_m15 (CountdownTimer_t10 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Int32_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		GameObject_GetComponent_TisText_t47_m213_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		_stringLiteral9 = il2cpp_codegen_string_literal_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = Time_get_deltaTime_m212(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		GameObject_t5 * L_1 = (__this->___myTimeLeftText_2);
		NullCheck(L_1);
		Text_t47 * L_2 = GameObject_GetComponent_TisText_t47_m213(L_1, /*hidden argument*/GameObject_GetComponent_TisText_t47_m213_MethodInfo_var);
		int32_t L_3 = (__this->___CountdownNumber_8);
		int32_t L_4 = L_3;
		Object_t * L_5 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m214(NULL /*static, unused*/, _stringLiteral9, L_5, /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_6);
		return;
	}
}
// System.Void CountdownTimer::ShiftTimer()
extern TypeInfo* GameManager_t22_il2cpp_TypeInfo_var;
extern TypeInfo* CountdownTimer_t10_il2cpp_TypeInfo_var;
extern "C" void CountdownTimer_ShiftTimer_m16 (CountdownTimer_t10 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameManager_t22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		CountdownTimer_t10_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playersTurn_6 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(CountdownTimer_t10_il2cpp_TypeInfo_var);
		((CountdownTimer_t10_StaticFields*)CountdownTimer_t10_il2cpp_TypeInfo_var->static_fields)->___timeLeft_4 = (10.0f);
		((CountdownTimer_t10_StaticFields*)CountdownTimer_t10_il2cpp_TypeInfo_var->static_fields)->___showTimer_5 = 0;
		return;
	}
}
// System.Collections.IEnumerator CountdownTimer::AutomaticTurn()
extern TypeInfo* U3CAutomaticTurnU3Ec__Iterator0_t7_il2cpp_TypeInfo_var;
extern "C" Object_t * CountdownTimer_AutomaticTurn_m17 (CountdownTimer_t10 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CAutomaticTurnU3Ec__Iterator0_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(12);
		s_Il2CppMethodIntialized = true;
	}
	U3CAutomaticTurnU3Ec__Iterator0_t7 * V_0 = {0};
	{
		U3CAutomaticTurnU3Ec__Iterator0_t7 * L_0 = (U3CAutomaticTurnU3Ec__Iterator0_t7 *)il2cpp_codegen_object_new (U3CAutomaticTurnU3Ec__Iterator0_t7_il2cpp_TypeInfo_var);
		U3CAutomaticTurnU3Ec__Iterator0__ctor_m6(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAutomaticTurnU3Ec__Iterator0_t7 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_4 = __this;
		U3CAutomaticTurnU3Ec__Iterator0_t7 * L_2 = V_0;
		return L_2;
	}
}
// System.Void DynamicScrollView/<MoveTowardsTarget>c__Iterator1::.ctor()
extern "C" void U3CMoveTowardsTargetU3Ec__Iterator1__ctor_m18 (U3CMoveTowardsTargetU3Ec__Iterator1_t13 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object DynamicScrollView/<MoveTowardsTarget>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CMoveTowardsTargetU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m19 (U3CMoveTowardsTargetU3Ec__Iterator1_t13 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_6);
		return L_0;
	}
}
// System.Object DynamicScrollView/<MoveTowardsTarget>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CMoveTowardsTargetU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m20 (U3CMoveTowardsTargetU3Ec__Iterator1_t13 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_6);
		return L_0;
	}
}
// System.Boolean DynamicScrollView/<MoveTowardsTarget>c__Iterator1::MoveNext()
extern TypeInfo* Mathf_t57_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t56_il2cpp_TypeInfo_var;
extern "C" bool U3CMoveTowardsTargetU3Ec__Iterator1_MoveNext_m21 (U3CMoveTowardsTargetU3Ec__Iterator1_t13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t57_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(13);
		Int32_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_5);
		V_0 = L_0;
		__this->___U24PC_5 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_009b;
		}
	}
	{
		goto IL_00b2;
	}

IL_0021:
	{
		__this->___U3CiU3E__0_0 = (0.0f);
		float L_2 = (__this->___time_1);
		__this->___U3CrateU3E__1_2 = ((float)((float)(1.0f)/(float)L_2));
		goto IL_009b;
	}

IL_0043:
	{
		float L_3 = (__this->___U3CiU3E__0_0);
		float L_4 = (__this->___U3CrateU3E__1_2);
		float L_5 = Time_get_deltaTime_m212(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___U3CiU3E__0_0 = ((float)((float)L_3+(float)((float)((float)L_4*(float)L_5))));
		DynamicScrollView_t14 * L_6 = (__this->___U3CU3Ef__this_10);
		NullCheck(L_6);
		ScrollRect_t19 * L_7 = (L_6->___scrollRect_7);
		float L_8 = (__this->___from_3);
		float L_9 = (__this->___target_4);
		float L_10 = (__this->___U3CiU3E__0_0);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t57_il2cpp_TypeInfo_var);
		float L_11 = Mathf_Lerp_m215(NULL /*static, unused*/, L_8, L_9, L_10, /*hidden argument*/NULL);
		NullCheck(L_7);
		ScrollRect_set_verticalNormalizedPosition_m216(L_7, L_11, /*hidden argument*/NULL);
		int32_t L_12 = 0;
		Object_t * L_13 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_12);
		__this->___U24current_6 = L_13;
		__this->___U24PC_5 = 1;
		goto IL_00b4;
	}

IL_009b:
	{
		float L_14 = (__this->___U3CiU3E__0_0);
		if ((((float)L_14) < ((float)(1.0f))))
		{
			goto IL_0043;
		}
	}
	{
		__this->___U24PC_5 = (-1);
	}

IL_00b2:
	{
		return 0;
	}

IL_00b4:
	{
		return 1;
	}
	// Dead block : IL_00b6: ldloc.1
}
// System.Void DynamicScrollView/<MoveTowardsTarget>c__Iterator1::Dispose()
extern "C" void U3CMoveTowardsTargetU3Ec__Iterator1_Dispose_m22 (U3CMoveTowardsTargetU3Ec__Iterator1_t13 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_5 = (-1);
		return;
	}
}
// System.Void DynamicScrollView/<MoveTowardsTarget>c__Iterator1::Reset()
extern TypeInfo* NotSupportedException_t55_il2cpp_TypeInfo_var;
extern "C" void U3CMoveTowardsTargetU3Ec__Iterator1_Reset_m23 (U3CMoveTowardsTargetU3Ec__Iterator1_t13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t55 * L_0 = (NotSupportedException_t55 *)il2cpp_codegen_object_new (NotSupportedException_t55_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m208(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void DynamicScrollView/<GetActiveUsers>c__Iterator2::.ctor()
extern "C" void U3CGetActiveUsersU3Ec__Iterator2__ctor_m24 (U3CGetActiveUsersU3Ec__Iterator2_t15 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object DynamicScrollView/<GetActiveUsers>c__Iterator2::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CGetActiveUsersU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m25 (U3CGetActiveUsersU3Ec__Iterator2_t15 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_11);
		return L_0;
	}
}
// System.Object DynamicScrollView/<GetActiveUsers>c__Iterator2::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetActiveUsersU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m26 (U3CGetActiveUsersU3Ec__Iterator2_t15 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_11);
		return L_0;
	}
}
// System.Boolean DynamicScrollView/<GetActiveUsers>c__Iterator2::MoveNext()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t58_il2cpp_TypeInfo_var;
extern TypeInfo* DynamicScrollView_t14_il2cpp_TypeInfo_var;
extern TypeInfo* StringU5BU5D_t16_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t47_m213_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral10;
extern Il2CppCodeGenString* _stringLiteral11;
extern Il2CppCodeGenString* _stringLiteral12;
extern Il2CppCodeGenString* _stringLiteral13;
extern Il2CppCodeGenString* _stringLiteral14;
extern Il2CppCodeGenString* _stringLiteral15;
extern Il2CppCodeGenString* _stringLiteral16;
extern Il2CppCodeGenString* _stringLiteral17;
extern Il2CppCodeGenString* _stringLiteral18;
extern Il2CppCodeGenString* _stringLiteral19;
extern Il2CppCodeGenString* _stringLiteral20;
extern Il2CppCodeGenString* _stringLiteral21;
extern Il2CppCodeGenString* _stringLiteral5;
extern Il2CppCodeGenString* _stringLiteral22;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral24;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral25;
extern Il2CppCodeGenString* _stringLiteral26;
extern Il2CppCodeGenString* _stringLiteral4;
extern "C" bool U3CGetActiveUsersU3Ec__Iterator2_MoveNext_m27 (U3CGetActiveUsersU3Ec__Iterator2_t15 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		CharU5BU5D_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		DynamicScrollView_t14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		StringU5BU5D_t16_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		GameObject_GetComponent_TisText_t47_m213_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		_stringLiteral10 = il2cpp_codegen_string_literal_from_index(10);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		_stringLiteral12 = il2cpp_codegen_string_literal_from_index(12);
		_stringLiteral13 = il2cpp_codegen_string_literal_from_index(13);
		_stringLiteral14 = il2cpp_codegen_string_literal_from_index(14);
		_stringLiteral15 = il2cpp_codegen_string_literal_from_index(15);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		_stringLiteral17 = il2cpp_codegen_string_literal_from_index(17);
		_stringLiteral18 = il2cpp_codegen_string_literal_from_index(18);
		_stringLiteral19 = il2cpp_codegen_string_literal_from_index(19);
		_stringLiteral20 = il2cpp_codegen_string_literal_from_index(20);
		_stringLiteral21 = il2cpp_codegen_string_literal_from_index(21);
		_stringLiteral5 = il2cpp_codegen_string_literal_from_index(5);
		_stringLiteral22 = il2cpp_codegen_string_literal_from_index(22);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral24 = il2cpp_codegen_string_literal_from_index(24);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral25 = il2cpp_codegen_string_literal_from_index(25);
		_stringLiteral26 = il2cpp_codegen_string_literal_from_index(26);
		_stringLiteral4 = il2cpp_codegen_string_literal_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_10);
		V_0 = L_0;
		__this->___U24PC_10 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0099;
		}
	}
	{
		goto IL_0414;
	}

IL_0021:
	{
		String_t* L_2 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral11, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_3 = String_Concat_m217(NULL /*static, unused*/, _stringLiteral10, L_2, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		WWWForm_t8 * L_4 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_4, /*hidden argument*/NULL);
		__this->___U3CformU3E__0_0 = L_4;
		String_t* L_5 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral11, /*hidden argument*/NULL);
		__this->___U3CusernameU3E__1_1 = L_5;
		WWWForm_t8 * L_6 = (__this->___U3CformU3E__0_0);
		String_t* L_7 = (__this->___U3CusernameU3E__1_1);
		NullCheck(L_6);
		WWWForm_AddField_m203(L_6, _stringLiteral12, L_7, /*hidden argument*/NULL);
		WWWForm_t8 * L_8 = (__this->___U3CformU3E__0_0);
		WWW_t9 * L_9 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_9, _stringLiteral13, L_8, /*hidden argument*/NULL);
		__this->___U3CwU3E__2_2 = L_9;
		WWW_t9 * L_10 = (__this->___U3CwU3E__2_2);
		__this->___U24current_11 = L_10;
		__this->___U24PC_10 = 1;
		goto IL_0416;
	}

IL_0099:
	{
		WWW_t9 * L_11 = (__this->___U3CwU3E__2_2);
		NullCheck(L_11);
		String_t* L_12 = WWW_get_error_m218(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_03fd;
		}
	}
	{
		WWW_t9 * L_13 = (__this->___U3CwU3E__2_2);
		NullCheck(L_13);
		String_t* L_14 = WWW_get_text_m206(L_13, /*hidden argument*/NULL);
		__this->___U3CresponseU3E__3_3 = L_14;
		String_t* L_15 = (__this->___U3CresponseU3E__3_3);
		CharU5BU5D_t58* L_16 = ((CharU5BU5D_t58*)SZArrayNew(CharU5BU5D_t58_il2cpp_TypeInfo_var, 1));
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_16, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)37);
		NullCheck(L_15);
		StringU5BU5D_t16* L_17 = String_Split_m219(L_15, L_16, /*hidden argument*/NULL);
		__this->___U3CresponseSplitU3E__4_4 = L_17;
		String_t* L_18 = (__this->___U3CresponseU3E__3_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_19 = String_Concat_m217(NULL /*static, unused*/, _stringLiteral14, L_18, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		StringU5BU5D_t16* L_20 = (__this->___U3CresponseSplitU3E__4_4);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		int32_t L_21 = 0;
		bool L_22 = String_op_Equality_m204(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_20, L_21, sizeof(String_t*))), _stringLiteral15, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_01c3;
		}
	}
	{
		StringU5BU5D_t16* L_23 = (__this->___U3CresponseSplitU3E__4_4);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 0);
		int32_t L_24 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_25 = String_Concat_m217(NULL /*static, unused*/, _stringLiteral16, (*(String_t**)(String_t**)SZArrayLdElema(L_23, L_24, sizeof(String_t*))), /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_25, /*hidden argument*/NULL);
		StringU5BU5D_t16* L_26 = (__this->___U3CresponseSplitU3E__4_4);
		NullCheck(L_26);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_26, 1);
		int32_t L_27 = 1;
		__this->___U3CplayersNameU3E__5_5 = (*(String_t**)(String_t**)SZArrayLdElema(L_26, L_27, sizeof(String_t*)));
		StringU5BU5D_t16* L_28 = (__this->___U3CresponseSplitU3E__4_4);
		NullCheck(L_28);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_28, 1);
		int32_t L_29 = 1;
		String_t* L_30 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		bool L_31 = String_op_Equality_m204(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_28, L_29, sizeof(String_t*))), L_30, /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_0166;
		}
	}
	{
		GameObject_t5 * L_32 = GameObject_Find_m209(NULL /*static, unused*/, _stringLiteral17, /*hidden argument*/NULL);
		((DynamicScrollView_t14_StaticFields*)DynamicScrollView_t14_il2cpp_TypeInfo_var->static_fields)->___heading_3 = L_32;
		GameObject_t5 * L_33 = ((DynamicScrollView_t14_StaticFields*)DynamicScrollView_t14_il2cpp_TypeInfo_var->static_fields)->___heading_3;
		NullCheck(L_33);
		Text_t47 * L_34 = GameObject_GetComponent_TisText_t47_m213(L_33, /*hidden argument*/GameObject_GetComponent_TisText_t47_m213_MethodInfo_var);
		NullCheck(L_34);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_34, _stringLiteral18);
		goto IL_01be;
	}

IL_0166:
	{
		String_t* L_35 = (__this->___U3CplayersNameU3E__5_5);
		CharU5BU5D_t58* L_36 = ((CharU5BU5D_t58*)SZArrayNew(CharU5BU5D_t58_il2cpp_TypeInfo_var, 1));
		NullCheck(L_36);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_36, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_36, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)45);
		NullCheck(L_35);
		StringU5BU5D_t16* L_37 = String_Split_m219(L_35, L_36, /*hidden argument*/NULL);
		((DynamicScrollView_t14_StaticFields*)DynamicScrollView_t14_il2cpp_TypeInfo_var->static_fields)->___players_8 = L_37;
		DynamicScrollView_t14 * L_38 = (__this->___U3CU3Ef__this_12);
		NullCheck(L_38);
		DynamicScrollView_InitializeList_m34(L_38, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m220(NULL /*static, unused*/, _stringLiteral19, _stringLiteral20, /*hidden argument*/NULL);
		GameObject_t5 * L_39 = GameObject_Find_m209(NULL /*static, unused*/, _stringLiteral17, /*hidden argument*/NULL);
		((DynamicScrollView_t14_StaticFields*)DynamicScrollView_t14_il2cpp_TypeInfo_var->static_fields)->___heading_3 = L_39;
		GameObject_t5 * L_40 = ((DynamicScrollView_t14_StaticFields*)DynamicScrollView_t14_il2cpp_TypeInfo_var->static_fields)->___heading_3;
		NullCheck(L_40);
		Text_t47 * L_41 = GameObject_GetComponent_TisText_t47_m213(L_40, /*hidden argument*/GameObject_GetComponent_TisText_t47_m213_MethodInfo_var);
		NullCheck(L_41);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_41, _stringLiteral21);
	}

IL_01be:
	{
		goto IL_03f8;
	}

IL_01c3:
	{
		StringU5BU5D_t16* L_42 = (__this->___U3CresponseSplitU3E__4_4);
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 0);
		int32_t L_43 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_44 = String_op_Equality_m204(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_42, L_43, sizeof(String_t*))), _stringLiteral5, /*hidden argument*/NULL);
		if (!L_44)
		{
			goto IL_02de;
		}
	}
	{
		StringU5BU5D_t16* L_45 = (__this->___U3CresponseSplitU3E__4_4);
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 0);
		int32_t L_46 = 0;
		StringU5BU5D_t16* L_47 = (__this->___U3CresponseSplitU3E__4_4);
		NullCheck(L_47);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_47, 1);
		int32_t L_48 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_49 = String_Concat_m221(NULL /*static, unused*/, _stringLiteral22, (*(String_t**)(String_t**)SZArrayLdElema(L_45, L_46, sizeof(String_t*))), _stringLiteral23, (*(String_t**)(String_t**)SZArrayLdElema(L_47, L_48, sizeof(String_t*))), /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_49, /*hidden argument*/NULL);
		DynamicScrollView_t14 * L_50 = (__this->___U3CU3Ef__this_12);
		StringU5BU5D_t16* L_51 = (__this->___U3CresponseSplitU3E__4_4);
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, 1);
		int32_t L_52 = 1;
		NullCheck(L_50);
		DynamicScrollView_InitializeListForGames_m35(L_50, (*(String_t**)(String_t**)SZArrayLdElema(L_51, L_52, sizeof(String_t*))), /*hidden argument*/NULL);
		StringU5BU5D_t16* L_53 = (__this->___U3CresponseSplitU3E__4_4);
		NullCheck(L_53);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_53, 2);
		int32_t L_54 = 2;
		__this->___U3CmatchDetailsU3E__6_6 = (*(String_t**)(String_t**)SZArrayLdElema(L_53, L_54, sizeof(String_t*)));
		String_t* L_55 = (__this->___U3CmatchDetailsU3E__6_6);
		CharU5BU5D_t58* L_56 = ((CharU5BU5D_t58*)SZArrayNew(CharU5BU5D_t58_il2cpp_TypeInfo_var, 1));
		NullCheck(L_56);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_56, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_56, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)45);
		NullCheck(L_55);
		StringU5BU5D_t16* L_57 = String_Split_m219(L_55, L_56, /*hidden argument*/NULL);
		__this->___U3CdetailsU3E__7_7 = L_57;
		PlayerPrefs_SetString_m220(NULL /*static, unused*/, _stringLiteral19, _stringLiteral24, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m220(NULL /*static, unused*/, _stringLiteral3, _stringLiteral5, /*hidden argument*/NULL);
		StringU5BU5D_t16* L_58 = (__this->___U3CdetailsU3E__7_7);
		NullCheck(L_58);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_58, ((int32_t)10));
		int32_t L_59 = ((int32_t)10);
		PlayerPrefs_SetString_m220(NULL /*static, unused*/, _stringLiteral1, (*(String_t**)(String_t**)SZArrayLdElema(L_58, L_59, sizeof(String_t*))), /*hidden argument*/NULL);
		StringU5BU5D_t16* L_60 = ((StringU5BU5D_t16*)SZArrayNew(StringU5BU5D_t16_il2cpp_TypeInfo_var, 6));
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, 0);
		ArrayElementTypeCheck (L_60, _stringLiteral25);
		*((String_t**)(String_t**)SZArrayLdElema(L_60, 0, sizeof(String_t*))) = (String_t*)_stringLiteral25;
		StringU5BU5D_t16* L_61 = L_60;
		StringU5BU5D_t16* L_62 = (__this->___U3CresponseSplitU3E__4_4);
		NullCheck(L_62);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_62, 0);
		int32_t L_63 = 0;
		NullCheck(L_61);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_61, 1);
		ArrayElementTypeCheck (L_61, (*(String_t**)(String_t**)SZArrayLdElema(L_62, L_63, sizeof(String_t*))));
		*((String_t**)(String_t**)SZArrayLdElema(L_61, 1, sizeof(String_t*))) = (String_t*)(*(String_t**)(String_t**)SZArrayLdElema(L_62, L_63, sizeof(String_t*)));
		StringU5BU5D_t16* L_64 = L_61;
		NullCheck(L_64);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_64, 2);
		ArrayElementTypeCheck (L_64, _stringLiteral23);
		*((String_t**)(String_t**)SZArrayLdElema(L_64, 2, sizeof(String_t*))) = (String_t*)_stringLiteral23;
		StringU5BU5D_t16* L_65 = L_64;
		StringU5BU5D_t16* L_66 = (__this->___U3CresponseSplitU3E__4_4);
		NullCheck(L_66);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_66, 1);
		int32_t L_67 = 1;
		NullCheck(L_65);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_65, 3);
		ArrayElementTypeCheck (L_65, (*(String_t**)(String_t**)SZArrayLdElema(L_66, L_67, sizeof(String_t*))));
		*((String_t**)(String_t**)SZArrayLdElema(L_65, 3, sizeof(String_t*))) = (String_t*)(*(String_t**)(String_t**)SZArrayLdElema(L_66, L_67, sizeof(String_t*)));
		StringU5BU5D_t16* L_68 = L_65;
		NullCheck(L_68);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_68, 4);
		ArrayElementTypeCheck (L_68, _stringLiteral23);
		*((String_t**)(String_t**)SZArrayLdElema(L_68, 4, sizeof(String_t*))) = (String_t*)_stringLiteral23;
		StringU5BU5D_t16* L_69 = L_68;
		StringU5BU5D_t16* L_70 = (__this->___U3CdetailsU3E__7_7);
		NullCheck(L_70);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_70, ((int32_t)10));
		int32_t L_71 = ((int32_t)10);
		NullCheck(L_69);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_69, 5);
		ArrayElementTypeCheck (L_69, (*(String_t**)(String_t**)SZArrayLdElema(L_70, L_71, sizeof(String_t*))));
		*((String_t**)(String_t**)SZArrayLdElema(L_69, 5, sizeof(String_t*))) = (String_t*)(*(String_t**)(String_t**)SZArrayLdElema(L_70, L_71, sizeof(String_t*)));
		String_t* L_72 = String_Concat_m222(NULL /*static, unused*/, L_69, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_72, /*hidden argument*/NULL);
		GameObject_t5 * L_73 = GameObject_Find_m209(NULL /*static, unused*/, _stringLiteral17, /*hidden argument*/NULL);
		((DynamicScrollView_t14_StaticFields*)DynamicScrollView_t14_il2cpp_TypeInfo_var->static_fields)->___heading_3 = L_73;
		GameObject_t5 * L_74 = ((DynamicScrollView_t14_StaticFields*)DynamicScrollView_t14_il2cpp_TypeInfo_var->static_fields)->___heading_3;
		NullCheck(L_74);
		Text_t47 * L_75 = GameObject_GetComponent_TisText_t47_m213(L_74, /*hidden argument*/GameObject_GetComponent_TisText_t47_m213_MethodInfo_var);
		NullCheck(L_75);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_75, _stringLiteral26);
		goto IL_03f8;
	}

IL_02de:
	{
		StringU5BU5D_t16* L_76 = (__this->___U3CresponseSplitU3E__4_4);
		NullCheck(L_76);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_76, 0);
		int32_t L_77 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_78 = String_op_Equality_m204(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_76, L_77, sizeof(String_t*))), _stringLiteral4, /*hidden argument*/NULL);
		if (!L_78)
		{
			goto IL_03d5;
		}
	}
	{
		DynamicScrollView_t14 * L_79 = (__this->___U3CU3Ef__this_12);
		StringU5BU5D_t16* L_80 = (__this->___U3CresponseSplitU3E__4_4);
		NullCheck(L_80);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_80, 1);
		int32_t L_81 = 1;
		NullCheck(L_79);
		DynamicScrollView_InitializeListForGames_m35(L_79, (*(String_t**)(String_t**)SZArrayLdElema(L_80, L_81, sizeof(String_t*))), /*hidden argument*/NULL);
		StringU5BU5D_t16* L_82 = (__this->___U3CresponseSplitU3E__4_4);
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, 2);
		int32_t L_83 = 2;
		__this->___U3CmatchDetailsU3E__8_8 = (*(String_t**)(String_t**)SZArrayLdElema(L_82, L_83, sizeof(String_t*)));
		String_t* L_84 = (__this->___U3CmatchDetailsU3E__8_8);
		CharU5BU5D_t58* L_85 = ((CharU5BU5D_t58*)SZArrayNew(CharU5BU5D_t58_il2cpp_TypeInfo_var, 1));
		NullCheck(L_85);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_85, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_85, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)45);
		NullCheck(L_84);
		StringU5BU5D_t16* L_86 = String_Split_m219(L_84, L_85, /*hidden argument*/NULL);
		__this->___U3CdetailsU3E__9_9 = L_86;
		PlayerPrefs_SetString_m220(NULL /*static, unused*/, _stringLiteral19, _stringLiteral24, /*hidden argument*/NULL);
		PlayerPrefs_SetString_m220(NULL /*static, unused*/, _stringLiteral3, _stringLiteral4, /*hidden argument*/NULL);
		StringU5BU5D_t16* L_87 = (__this->___U3CdetailsU3E__9_9);
		NullCheck(L_87);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_87, ((int32_t)10));
		int32_t L_88 = ((int32_t)10);
		PlayerPrefs_SetString_m220(NULL /*static, unused*/, _stringLiteral1, (*(String_t**)(String_t**)SZArrayLdElema(L_87, L_88, sizeof(String_t*))), /*hidden argument*/NULL);
		StringU5BU5D_t16* L_89 = ((StringU5BU5D_t16*)SZArrayNew(StringU5BU5D_t16_il2cpp_TypeInfo_var, 6));
		NullCheck(L_89);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_89, 0);
		ArrayElementTypeCheck (L_89, _stringLiteral25);
		*((String_t**)(String_t**)SZArrayLdElema(L_89, 0, sizeof(String_t*))) = (String_t*)_stringLiteral25;
		StringU5BU5D_t16* L_90 = L_89;
		StringU5BU5D_t16* L_91 = (__this->___U3CresponseSplitU3E__4_4);
		NullCheck(L_91);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_91, 0);
		int32_t L_92 = 0;
		NullCheck(L_90);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_90, 1);
		ArrayElementTypeCheck (L_90, (*(String_t**)(String_t**)SZArrayLdElema(L_91, L_92, sizeof(String_t*))));
		*((String_t**)(String_t**)SZArrayLdElema(L_90, 1, sizeof(String_t*))) = (String_t*)(*(String_t**)(String_t**)SZArrayLdElema(L_91, L_92, sizeof(String_t*)));
		StringU5BU5D_t16* L_93 = L_90;
		NullCheck(L_93);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_93, 2);
		ArrayElementTypeCheck (L_93, _stringLiteral23);
		*((String_t**)(String_t**)SZArrayLdElema(L_93, 2, sizeof(String_t*))) = (String_t*)_stringLiteral23;
		StringU5BU5D_t16* L_94 = L_93;
		StringU5BU5D_t16* L_95 = (__this->___U3CresponseSplitU3E__4_4);
		NullCheck(L_95);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_95, 1);
		int32_t L_96 = 1;
		NullCheck(L_94);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_94, 3);
		ArrayElementTypeCheck (L_94, (*(String_t**)(String_t**)SZArrayLdElema(L_95, L_96, sizeof(String_t*))));
		*((String_t**)(String_t**)SZArrayLdElema(L_94, 3, sizeof(String_t*))) = (String_t*)(*(String_t**)(String_t**)SZArrayLdElema(L_95, L_96, sizeof(String_t*)));
		StringU5BU5D_t16* L_97 = L_94;
		NullCheck(L_97);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_97, 4);
		ArrayElementTypeCheck (L_97, _stringLiteral23);
		*((String_t**)(String_t**)SZArrayLdElema(L_97, 4, sizeof(String_t*))) = (String_t*)_stringLiteral23;
		StringU5BU5D_t16* L_98 = L_97;
		StringU5BU5D_t16* L_99 = (__this->___U3CdetailsU3E__9_9);
		NullCheck(L_99);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_99, ((int32_t)10));
		int32_t L_100 = ((int32_t)10);
		NullCheck(L_98);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_98, 5);
		ArrayElementTypeCheck (L_98, (*(String_t**)(String_t**)SZArrayLdElema(L_99, L_100, sizeof(String_t*))));
		*((String_t**)(String_t**)SZArrayLdElema(L_98, 5, sizeof(String_t*))) = (String_t*)(*(String_t**)(String_t**)SZArrayLdElema(L_99, L_100, sizeof(String_t*)));
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_101 = String_Concat_m222(NULL /*static, unused*/, L_98, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_101, /*hidden argument*/NULL);
		GameObject_t5 * L_102 = GameObject_Find_m209(NULL /*static, unused*/, _stringLiteral17, /*hidden argument*/NULL);
		((DynamicScrollView_t14_StaticFields*)DynamicScrollView_t14_il2cpp_TypeInfo_var->static_fields)->___heading_3 = L_102;
		GameObject_t5 * L_103 = ((DynamicScrollView_t14_StaticFields*)DynamicScrollView_t14_il2cpp_TypeInfo_var->static_fields)->___heading_3;
		NullCheck(L_103);
		Text_t47 * L_104 = GameObject_GetComponent_TisText_t47_m213(L_103, /*hidden argument*/GameObject_GetComponent_TisText_t47_m213_MethodInfo_var);
		NullCheck(L_104);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_104, _stringLiteral26);
		goto IL_03f8;
	}

IL_03d5:
	{
		GameObject_t5 * L_105 = GameObject_Find_m209(NULL /*static, unused*/, _stringLiteral17, /*hidden argument*/NULL);
		((DynamicScrollView_t14_StaticFields*)DynamicScrollView_t14_il2cpp_TypeInfo_var->static_fields)->___heading_3 = L_105;
		GameObject_t5 * L_106 = ((DynamicScrollView_t14_StaticFields*)DynamicScrollView_t14_il2cpp_TypeInfo_var->static_fields)->___heading_3;
		NullCheck(L_106);
		Text_t47 * L_107 = GameObject_GetComponent_TisText_t47_m213(L_106, /*hidden argument*/GameObject_GetComponent_TisText_t47_m213_MethodInfo_var);
		NullCheck(L_107);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_107, _stringLiteral18);
	}

IL_03f8:
	{
		goto IL_040d;
	}

IL_03fd:
	{
		WWW_t9 * L_108 = (__this->___U3CwU3E__2_2);
		NullCheck(L_108);
		String_t* L_109 = WWW_get_error_m218(L_108, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_109, /*hidden argument*/NULL);
	}

IL_040d:
	{
		__this->___U24PC_10 = (-1);
	}

IL_0414:
	{
		return 0;
	}

IL_0416:
	{
		return 1;
	}
	// Dead block : IL_0418: ldloc.1
}
// System.Void DynamicScrollView/<GetActiveUsers>c__Iterator2::Dispose()
extern "C" void U3CGetActiveUsersU3Ec__Iterator2_Dispose_m28 (U3CGetActiveUsersU3Ec__Iterator2_t15 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_10 = (-1);
		return;
	}
}
// System.Void DynamicScrollView/<GetActiveUsers>c__Iterator2::Reset()
extern TypeInfo* NotSupportedException_t55_il2cpp_TypeInfo_var;
extern "C" void U3CGetActiveUsersU3Ec__Iterator2_Reset_m29 (U3CGetActiveUsersU3Ec__Iterator2_t15 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t55 * L_0 = (NotSupportedException_t55 *)il2cpp_codegen_object_new (NotSupportedException_t55_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m208(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void DynamicScrollView::.ctor()
extern "C" void DynamicScrollView__ctor_m30 (DynamicScrollView_t14 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DynamicScrollView::OnEnable()
extern Il2CppCodeGenString* _stringLiteral27;
extern "C" void DynamicScrollView_OnEnable_m31 (DynamicScrollView_t14 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral27 = il2cpp_codegen_string_literal_from_index(27);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_StartCoroutine_m223(__this, _stringLiteral27, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DynamicScrollView::ClearOldElement()
extern "C" void DynamicScrollView_ClearOldElement_m32 (DynamicScrollView_t14 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_0026;
	}

IL_0007:
	{
		GridLayoutGroup_t17 * L_0 = (__this->___gridLayout_5);
		NullCheck(L_0);
		Transform_t3 * L_1 = Component_get_transform_m224(L_0, /*hidden argument*/NULL);
		int32_t L_2 = V_0;
		NullCheck(L_1);
		Transform_t3 * L_3 = Transform_GetChild_m225(L_1, L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		GameObject_t5 * L_4 = Component_get_gameObject_m226(L_3, /*hidden argument*/NULL);
		Object_Destroy_m227(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_0;
		V_0 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_0026:
	{
		int32_t L_6 = V_0;
		GridLayoutGroup_t17 * L_7 = (__this->___gridLayout_5);
		NullCheck(L_7);
		Transform_t3 * L_8 = Component_get_transform_m224(L_7, /*hidden argument*/NULL);
		NullCheck(L_8);
		int32_t L_9 = Transform_get_childCount_m228(L_8, /*hidden argument*/NULL);
		if ((((int32_t)L_6) < ((int32_t)L_9)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void DynamicScrollView::SetContentHeight()
extern "C" void DynamicScrollView_SetContentHeight_m33 (DynamicScrollView_t14 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector2_t59  V_1 = {0};
	Vector2_t59  V_2 = {0};
	{
		GridLayoutGroup_t17 * L_0 = (__this->___gridLayout_5);
		NullCheck(L_0);
		Transform_t3 * L_1 = Component_get_transform_m224(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		int32_t L_2 = Transform_get_childCount_m228(L_1, /*hidden argument*/NULL);
		GridLayoutGroup_t17 * L_3 = (__this->___gridLayout_5);
		NullCheck(L_3);
		Vector2_t59  L_4 = GridLayoutGroup_get_cellSize_m229(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = ((&V_1)->___y_2);
		GridLayoutGroup_t17 * L_6 = (__this->___gridLayout_5);
		NullCheck(L_6);
		Transform_t3 * L_7 = Component_get_transform_m224(L_6, /*hidden argument*/NULL);
		NullCheck(L_7);
		int32_t L_8 = Transform_get_childCount_m228(L_7, /*hidden argument*/NULL);
		GridLayoutGroup_t17 * L_9 = (__this->___gridLayout_5);
		NullCheck(L_9);
		Vector2_t59  L_10 = GridLayoutGroup_get_spacing_m230(L_9, /*hidden argument*/NULL);
		V_2 = L_10;
		float L_11 = ((&V_2)->___y_2);
		V_0 = ((float)((float)((float)((float)(((float)L_2))*(float)L_5))+(float)((float)((float)(((float)((int32_t)((int32_t)L_8-(int32_t)1))))*(float)L_11))));
		RectTransform_t18 * L_12 = (__this->___scrollContent_6);
		float L_13 = V_0;
		Vector2_t59  L_14 = {0};
		Vector2__ctor_m231(&L_14, (400.0f), L_13, /*hidden argument*/NULL);
		NullCheck(L_12);
		RectTransform_set_sizeDelta_m232(L_12, L_14, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DynamicScrollView::InitializeList()
extern TypeInfo* DynamicScrollView_t14_il2cpp_TypeInfo_var;
extern "C" void DynamicScrollView_InitializeList_m34 (DynamicScrollView_t14 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		DynamicScrollView_t14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		DynamicScrollView_ClearOldElement_m32(__this, /*hidden argument*/NULL);
		V_0 = 0;
		goto IL_001e;
	}

IL_000d:
	{
		StringU5BU5D_t16* L_0 = ((DynamicScrollView_t14_StaticFields*)DynamicScrollView_t14_il2cpp_TypeInfo_var->static_fields)->___players_8;
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		DynamicScrollView_InitializeNewItem_m36(__this, (*(String_t**)(String_t**)SZArrayLdElema(L_0, L_2, sizeof(String_t*))), /*hidden argument*/NULL);
		int32_t L_3 = V_0;
		V_0 = ((int32_t)((int32_t)L_3+(int32_t)1));
	}

IL_001e:
	{
		int32_t L_4 = V_0;
		StringU5BU5D_t16* L_5 = ((DynamicScrollView_t14_StaticFields*)DynamicScrollView_t14_il2cpp_TypeInfo_var->static_fields)->___players_8;
		NullCheck(L_5);
		if ((((int32_t)L_4) < ((int32_t)((int32_t)((int32_t)(((int32_t)(((Array_t *)L_5)->max_length)))-(int32_t)1)))))
		{
			goto IL_000d;
		}
	}
	{
		DynamicScrollView_SetContentHeight_m33(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DynamicScrollView::InitializeListForGames(System.String)
extern "C" void DynamicScrollView_InitializeListForGames_m35 (DynamicScrollView_t14 * __this, String_t* ___Game, const MethodInfo* method)
{
	{
		DynamicScrollView_ClearOldElement_m32(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___Game;
		DynamicScrollView_InitializeNewItem_m36(__this, L_0, /*hidden argument*/NULL);
		DynamicScrollView_SetContentHeight_m33(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void DynamicScrollView::InitializeNewItem(System.String)
extern const MethodInfo* Object_Instantiate_TisGameObject_t5_m233_MethodInfo_var;
extern "C" void DynamicScrollView_InitializeNewItem_m36 (DynamicScrollView_t14 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_Instantiate_TisGameObject_t5_m233_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483652);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t5 * V_0 = {0};
	{
		GameObject_t5 * L_0 = (__this->___item_4);
		GameObject_t5 * L_1 = Object_Instantiate_TisGameObject_t5_m233(NULL /*static, unused*/, L_0, /*hidden argument*/Object_Instantiate_TisGameObject_t5_m233_MethodInfo_var);
		V_0 = L_1;
		GameObject_t5 * L_2 = V_0;
		String_t* L_3 = ___name;
		NullCheck(L_2);
		Object_set_name_m234(L_2, L_3, /*hidden argument*/NULL);
		GameObject_t5 * L_4 = V_0;
		NullCheck(L_4);
		Transform_t3 * L_5 = GameObject_get_transform_m194(L_4, /*hidden argument*/NULL);
		GridLayoutGroup_t17 * L_6 = (__this->___gridLayout_5);
		NullCheck(L_6);
		Transform_t3 * L_7 = Component_get_transform_m224(L_6, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_parent_m235(L_5, L_7, /*hidden argument*/NULL);
		GameObject_t5 * L_8 = V_0;
		NullCheck(L_8);
		Transform_t3 * L_9 = GameObject_get_transform_m194(L_8, /*hidden argument*/NULL);
		Vector3_t35  L_10 = Vector3_get_one_m236(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_9);
		Transform_set_localScale_m237(L_9, L_10, /*hidden argument*/NULL);
		GameObject_t5 * L_11 = V_0;
		NullCheck(L_11);
		GameObject_SetActive_m238(L_11, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator DynamicScrollView::MoveTowardsTarget(System.Single,System.Single,System.Single)
extern TypeInfo* U3CMoveTowardsTargetU3Ec__Iterator1_t13_il2cpp_TypeInfo_var;
extern "C" Object_t * DynamicScrollView_MoveTowardsTarget_m37 (DynamicScrollView_t14 * __this, float ___time, float ___from, float ___target, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CMoveTowardsTargetU3Ec__Iterator1_t13_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		s_Il2CppMethodIntialized = true;
	}
	U3CMoveTowardsTargetU3Ec__Iterator1_t13 * V_0 = {0};
	{
		U3CMoveTowardsTargetU3Ec__Iterator1_t13 * L_0 = (U3CMoveTowardsTargetU3Ec__Iterator1_t13 *)il2cpp_codegen_object_new (U3CMoveTowardsTargetU3Ec__Iterator1_t13_il2cpp_TypeInfo_var);
		U3CMoveTowardsTargetU3Ec__Iterator1__ctor_m18(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CMoveTowardsTargetU3Ec__Iterator1_t13 * L_1 = V_0;
		float L_2 = ___time;
		NullCheck(L_1);
		L_1->___time_1 = L_2;
		U3CMoveTowardsTargetU3Ec__Iterator1_t13 * L_3 = V_0;
		float L_4 = ___from;
		NullCheck(L_3);
		L_3->___from_3 = L_4;
		U3CMoveTowardsTargetU3Ec__Iterator1_t13 * L_5 = V_0;
		float L_6 = ___target;
		NullCheck(L_5);
		L_5->___target_4 = L_6;
		U3CMoveTowardsTargetU3Ec__Iterator1_t13 * L_7 = V_0;
		float L_8 = ___time;
		NullCheck(L_7);
		L_7->___U3CU24U3Etime_7 = L_8;
		U3CMoveTowardsTargetU3Ec__Iterator1_t13 * L_9 = V_0;
		float L_10 = ___from;
		NullCheck(L_9);
		L_9->___U3CU24U3Efrom_8 = L_10;
		U3CMoveTowardsTargetU3Ec__Iterator1_t13 * L_11 = V_0;
		float L_12 = ___target;
		NullCheck(L_11);
		L_11->___U3CU24U3Etarget_9 = L_12;
		U3CMoveTowardsTargetU3Ec__Iterator1_t13 * L_13 = V_0;
		NullCheck(L_13);
		L_13->___U3CU3Ef__this_10 = __this;
		U3CMoveTowardsTargetU3Ec__Iterator1_t13 * L_14 = V_0;
		return L_14;
	}
}
// System.Collections.IEnumerator DynamicScrollView::GetActiveUsers()
extern TypeInfo* U3CGetActiveUsersU3Ec__Iterator2_t15_il2cpp_TypeInfo_var;
extern "C" Object_t * DynamicScrollView_GetActiveUsers_m38 (DynamicScrollView_t14 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGetActiveUsersU3Ec__Iterator2_t15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetActiveUsersU3Ec__Iterator2_t15 * V_0 = {0};
	{
		U3CGetActiveUsersU3Ec__Iterator2_t15 * L_0 = (U3CGetActiveUsersU3Ec__Iterator2_t15 *)il2cpp_codegen_object_new (U3CGetActiveUsersU3Ec__Iterator2_t15_il2cpp_TypeInfo_var);
		U3CGetActiveUsersU3Ec__Iterator2__ctor_m24(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetActiveUsersU3Ec__Iterator2_t15 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_12 = __this;
		U3CGetActiveUsersU3Ec__Iterator2_t15 * L_2 = V_0;
		return L_2;
	}
}
// System.Void DynamicScrollView::AddNewElement()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t56_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral28;
extern "C" void DynamicScrollView_AddNewElement_m39 (DynamicScrollView_t14 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		Int32_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		_stringLiteral28 = il2cpp_codegen_string_literal_from_index(28);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_0 = ((String_t_StaticFields*)String_t_il2cpp_TypeInfo_var->static_fields)->___Empty_2;
		GridLayoutGroup_t17 * L_1 = (__this->___gridLayout_5);
		NullCheck(L_1);
		Transform_t3 * L_2 = Component_get_transform_m224(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		int32_t L_3 = Transform_get_childCount_m228(L_2, /*hidden argument*/NULL);
		int32_t L_4 = ((int32_t)((int32_t)L_3+(int32_t)1));
		Object_t * L_5 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_4);
		String_t* L_6 = String_Concat_m239(NULL /*static, unused*/, L_0, L_5, _stringLiteral28, /*hidden argument*/NULL);
		DynamicScrollView_InitializeNewItem_m36(__this, L_6, /*hidden argument*/NULL);
		DynamicScrollView_SetContentHeight_m33(__this, /*hidden argument*/NULL);
		ScrollRect_t19 * L_7 = (__this->___scrollRect_7);
		NullCheck(L_7);
		float L_8 = ScrollRect_get_verticalNormalizedPosition_m240(L_7, /*hidden argument*/NULL);
		Object_t * L_9 = DynamicScrollView_MoveTowardsTarget_m37(__this, (0.2f), L_8, (0.0f), /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m207(__this, L_9, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager/<ResetBoard>c__Iterator3::.ctor()
extern "C" void U3CResetBoardU3Ec__Iterator3__ctor_m40 (U3CResetBoardU3Ec__Iterator3_t20 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object GameManager/<ResetBoard>c__Iterator3::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CResetBoardU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m41 (U3CResetBoardU3Ec__Iterator3_t20 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object GameManager/<ResetBoard>c__Iterator3::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CResetBoardU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m42 (U3CResetBoardU3Ec__Iterator3_t20 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean GameManager/<ResetBoard>c__Iterator3::MoveNext()
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameManager_t22_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral29;
extern Il2CppCodeGenString* _stringLiteral7;
extern Il2CppCodeGenString* _stringLiteral30;
extern Il2CppCodeGenString* _stringLiteral17;
extern "C" bool U3CResetBoardU3Ec__Iterator3_MoveNext_m43 (U3CResetBoardU3Ec__Iterator3_t20 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		GameManager_t22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral29 = il2cpp_codegen_string_literal_from_index(29);
		_stringLiteral7 = il2cpp_codegen_string_literal_from_index(7);
		_stringLiteral30 = il2cpp_codegen_string_literal_from_index(30);
		_stringLiteral17 = il2cpp_codegen_string_literal_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0083;
		}
	}
	{
		goto IL_00be;
	}

IL_0021:
	{
		WWWForm_t8 * L_2 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_2, /*hidden argument*/NULL);
		__this->___U3CformU3E__0_0 = L_2;
		String_t* L_3 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral1, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		WWWForm_t8 * L_4 = (__this->___U3CformU3E__0_0);
		String_t* L_5 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral1, /*hidden argument*/NULL);
		NullCheck(L_4);
		WWWForm_AddField_m203(L_4, _stringLiteral2, L_5, /*hidden argument*/NULL);
		WWWForm_t8 * L_6 = (__this->___U3CformU3E__0_0);
		WWW_t9 * L_7 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_7, _stringLiteral29, L_6, /*hidden argument*/NULL);
		__this->___U3CwU3E__1_1 = L_7;
		WWW_t9 * L_8 = (__this->___U3CwU3E__1_1);
		__this->___U24current_3 = L_8;
		__this->___U24PC_2 = 1;
		goto IL_00c0;
	}

IL_0083:
	{
		WWW_t9 * L_9 = (__this->___U3CwU3E__1_1);
		NullCheck(L_9);
		String_t* L_10 = WWW_get_text_m206(L_9, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_11 = String_op_Equality_m204(NULL /*static, unused*/, L_10, _stringLiteral7, /*hidden argument*/NULL);
		if (!L_11)
		{
			goto IL_00b7;
		}
	}
	{
		Debug_Log_m202(NULL /*static, unused*/, _stringLiteral30, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___gameOverBool_8 = 0;
		Application_LoadLevel_m241(NULL /*static, unused*/, _stringLiteral17, /*hidden argument*/NULL);
	}

IL_00b7:
	{
		__this->___U24PC_2 = (-1);
	}

IL_00be:
	{
		return 0;
	}

IL_00c0:
	{
		return 1;
	}
	// Dead block : IL_00c2: ldloc.1
}
// System.Void GameManager/<ResetBoard>c__Iterator3::Dispose()
extern "C" void U3CResetBoardU3Ec__Iterator3_Dispose_m44 (U3CResetBoardU3Ec__Iterator3_t20 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void GameManager/<ResetBoard>c__Iterator3::Reset()
extern TypeInfo* NotSupportedException_t55_il2cpp_TypeInfo_var;
extern "C" void U3CResetBoardU3Ec__Iterator3_Reset_m45 (U3CResetBoardU3Ec__Iterator3_t20 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t55 * L_0 = (NotSupportedException_t55 *)il2cpp_codegen_object_new (NotSupportedException_t55_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m208(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void GameManager/<GetPlayer1Turn>c__Iterator4::.ctor()
extern "C" void U3CGetPlayer1TurnU3Ec__Iterator4__ctor_m46 (U3CGetPlayer1TurnU3Ec__Iterator4_t21 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object GameManager/<GetPlayer1Turn>c__Iterator4::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CGetPlayer1TurnU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m47 (U3CGetPlayer1TurnU3Ec__Iterator4_t21 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_22);
		return L_0;
	}
}
// System.Object GameManager/<GetPlayer1Turn>c__Iterator4::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetPlayer1TurnU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m48 (U3CGetPlayer1TurnU3Ec__Iterator4_t21 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_22);
		return L_0;
	}
}
// System.Boolean GameManager/<GetPlayer1Turn>c__Iterator4::MoveNext()
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t58_il2cpp_TypeInfo_var;
extern TypeInfo* GameManager_t22_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t56_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t5_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t47_m213_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral31;
extern Il2CppCodeGenString* _stringLiteral32;
extern Il2CppCodeGenString* _stringLiteral33;
extern Il2CppCodeGenString* _stringLiteral34;
extern Il2CppCodeGenString* _stringLiteral35;
extern Il2CppCodeGenString* _stringLiteral36;
extern Il2CppCodeGenString* _stringLiteral37;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral38;
extern Il2CppCodeGenString* _stringLiteral39;
extern Il2CppCodeGenString* _stringLiteral40;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral42;
extern "C" bool U3CGetPlayer1TurnU3Ec__Iterator4_MoveNext_m49 (U3CGetPlayer1TurnU3Ec__Iterator4_t21 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		CharU5BU5D_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GameManager_t22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Int32_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		GameObject_t5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		GameObject_GetComponent_TisText_t47_m213_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral31 = il2cpp_codegen_string_literal_from_index(31);
		_stringLiteral32 = il2cpp_codegen_string_literal_from_index(32);
		_stringLiteral33 = il2cpp_codegen_string_literal_from_index(33);
		_stringLiteral34 = il2cpp_codegen_string_literal_from_index(34);
		_stringLiteral35 = il2cpp_codegen_string_literal_from_index(35);
		_stringLiteral36 = il2cpp_codegen_string_literal_from_index(36);
		_stringLiteral37 = il2cpp_codegen_string_literal_from_index(37);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral38 = il2cpp_codegen_string_literal_from_index(38);
		_stringLiteral39 = il2cpp_codegen_string_literal_from_index(39);
		_stringLiteral40 = il2cpp_codegen_string_literal_from_index(40);
		_stringLiteral41 = il2cpp_codegen_string_literal_from_index(41);
		_stringLiteral42 = il2cpp_codegen_string_literal_from_index(42);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_21);
		V_0 = L_0;
		__this->___U24PC_21 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0074;
		}
	}
	{
		goto IL_05e2;
	}

IL_0021:
	{
		WWWForm_t8 * L_2 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_2, /*hidden argument*/NULL);
		__this->___U3CformU3E__0_0 = L_2;
		WWWForm_t8 * L_3 = (__this->___U3CformU3E__0_0);
		String_t* L_4 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral1, /*hidden argument*/NULL);
		NullCheck(L_3);
		WWWForm_AddField_m203(L_3, _stringLiteral2, L_4, /*hidden argument*/NULL);
		WWWForm_t8 * L_5 = (__this->___U3CformU3E__0_0);
		WWW_t9 * L_6 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_6, _stringLiteral31, L_5, /*hidden argument*/NULL);
		__this->___U3CwU3E__1_1 = L_6;
		WWW_t9 * L_7 = (__this->___U3CwU3E__1_1);
		__this->___U24current_22 = L_7;
		__this->___U24PC_21 = 1;
		goto IL_05e4;
	}

IL_0074:
	{
		WWW_t9 * L_8 = (__this->___U3CwU3E__1_1);
		NullCheck(L_8);
		String_t* L_9 = WWW_get_text_m206(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m217(NULL /*static, unused*/, _stringLiteral32, L_9, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		WWW_t9 * L_11 = (__this->___U3CwU3E__1_1);
		NullCheck(L_11);
		String_t* L_12 = WWW_get_text_m206(L_11, /*hidden argument*/NULL);
		__this->___U3CJsonStringU3E__2_2 = L_12;
		String_t* L_13 = (__this->___U3CJsonStringU3E__2_2);
		CharU5BU5D_t58* L_14 = ((CharU5BU5D_t58*)SZArrayNew(CharU5BU5D_t58_il2cpp_TypeInfo_var, 1));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_14, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)45);
		NullCheck(L_13);
		StringU5BU5D_t16* L_15 = String_Split_m219(L_13, L_14, /*hidden argument*/NULL);
		__this->___U3CarrayU3E__3_3 = L_15;
		StringU5BU5D_t16* L_16 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 0);
		int32_t L_17 = 0;
		bool L_18 = String_op_Equality_m204(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_16, L_17, sizeof(String_t*))), _stringLiteral33, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_033e;
		}
	}
	{
		StringU5BU5D_t16* L_19 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 7);
		int32_t L_20 = 7;
		__this->___U3CResetBoolU3E__4_4 = (*(String_t**)(String_t**)SZArrayLdElema(L_19, L_20, sizeof(String_t*)));
		String_t* L_21 = (__this->___U3CResetBoolU3E__4_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_op_Equality_m204(NULL /*static, unused*/, L_21, _stringLiteral33, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0101;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		CountdownTimer_t10 * L_23 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		NullCheck(L_23);
		L_23->___CountdownNumber_8 = ((int32_t)10);
	}

IL_0101:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		CountdownTimer_t10 * L_24 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		CountdownTimer_t10 * L_25 = L_24;
		NullCheck(L_25);
		int32_t L_26 = (L_25->___CountdownNumber_8);
		NullCheck(L_25);
		L_25->___CountdownNumber_8 = ((int32_t)((int32_t)L_26-(int32_t)1));
		CountdownTimer_t10 * L_27 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		NullCheck(L_27);
		int32_t L_28 = (L_27->___CountdownNumber_8);
		int32_t L_29 = L_28;
		Object_t * L_30 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_29);
		Debug_Log_m202(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		CountdownTimer_t10 * L_31 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		NullCheck(L_31);
		int32_t L_32 = (L_31->___CountdownNumber_8);
		if (L_32)
		{
			goto IL_0159;
		}
	}
	{
		GameManager_t22 * L_33 = (__this->___U3CU3Ef__this_23);
		GameManager_t22 * L_34 = (__this->___U3CU3Ef__this_23);
		NullCheck(L_34);
		Object_t * L_35 = GameManager_AutomaticMovePlayer1_m88(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		MonoBehaviour_StartCoroutine_m207(L_33, L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		CountdownTimer_t10 * L_36 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		NullCheck(L_36);
		L_36->___CountdownNumber_8 = ((int32_t)10);
	}

IL_0159:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		GameObject_t5 * L_37 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___turnText_19;
		NullCheck(L_37);
		Text_t47 * L_38 = GameObject_GetComponent_TisText_t47_m213(L_37, /*hidden argument*/GameObject_GetComponent_TisText_t47_m213_MethodInfo_var);
		NullCheck(L_38);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_38, _stringLiteral34);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playersTurn_6 = 1;
		GameManager_t22 * L_39 = (__this->___U3CU3Ef__this_23);
		NullCheck(L_39);
		MonoBehaviour_InvokeRepeating_m242(L_39, _stringLiteral35, (0.0f), (0.3f), /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___MoveDice_11 = 1;
		StringU5BU5D_t16* L_40 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 1);
		int32_t L_41 = 1;
		StringU5BU5D_t16* L_42 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 2);
		int32_t L_43 = 2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m221(NULL /*static, unused*/, _stringLiteral36, (*(String_t**)(String_t**)SZArrayLdElema(L_40, L_41, sizeof(String_t*))), _stringLiteral37, (*(String_t**)(String_t**)SZArrayLdElema(L_42, L_43, sizeof(String_t*))), /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		StringU5BU5D_t16* L_45 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 1);
		int32_t L_46 = 1;
		float L_47 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_45, L_46, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CxU3E__5_5 = L_47;
		StringU5BU5D_t16* L_48 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, 2);
		int32_t L_49 = 2;
		float L_50 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_48, L_49, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CyU3E__6_6 = L_50;
		StringU5BU5D_t16* L_51 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, 3);
		int32_t L_52 = 3;
		int32_t L_53 = Int32_Parse_m244(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_51, L_52, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CdnU3E__7_7 = L_53;
		StringU5BU5D_t16* L_54 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, 4);
		int32_t L_55 = 4;
		float L_56 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_54, L_55, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CPlayerxU3E__8_8 = L_56;
		StringU5BU5D_t16* L_57 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, 5);
		int32_t L_58 = 5;
		float L_59 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_57, L_58, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CPlayeryU3E__9_9 = L_59;
		StringU5BU5D_t16* L_60 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, 6);
		int32_t L_61 = 6;
		int32_t L_62 = Int32_Parse_m244(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_60, L_61, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CpU3E__10_10 = L_62;
		int32_t L_63 = (__this->___U3CpU3E__10_10);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22 = L_63;
		GameObject_t5 * L_64 = GameObject_Find_m209(NULL /*static, unused*/, _stringLiteral3, /*hidden argument*/NULL);
		NullCheck(L_64);
		Transform_t3 * L_65 = GameObject_get_transform_m194(L_64, /*hidden argument*/NULL);
		float L_66 = (__this->___U3CPlayerxU3E__8_8);
		float L_67 = (__this->___U3CPlayeryU3E__9_9);
		Vector3_t35  L_68 = {0};
		Vector3__ctor_m192(&L_68, L_66, L_67, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_65);
		Transform_set_position_m245(L_65, L_68, /*hidden argument*/NULL);
		int32_t L_69 = (__this->___U3CpU3E__10_10);
		PlayerPrefs_SetInt_m246(NULL /*static, unused*/, _stringLiteral38, L_69, /*hidden argument*/NULL);
		int32_t L_70 = (__this->___U3CdnU3E__7_7);
		if ((((int32_t)L_70) <= ((int32_t)0)))
		{
			goto IL_0286;
		}
	}
	{
		int32_t L_71 = (__this->___U3CdnU3E__7_7);
		if ((((int32_t)L_71) <= ((int32_t)6)))
		{
			goto IL_02d6;
		}
	}

IL_0286:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		GameObject_t5 * L_72 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstanceB_16;
		Object_Destroy_m227(NULL /*static, unused*/, L_72, /*hidden argument*/NULL);
		GameManager_t22 * L_73 = (__this->___U3CU3Ef__this_23);
		NullCheck(L_73);
		GameObjectU5BU5D_t4* L_74 = (L_73->___dices_14);
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, 1);
		int32_t L_75 = 1;
		__this->___U3CtoInstantiateU3E__11_11 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_74, L_75, sizeof(GameObject_t5 *)));
		GameObject_t5 * L_76 = (__this->___U3CtoInstantiateU3E__11_11);
		Vector3_t35  L_77 = {0};
		Vector3__ctor_m192(&L_77, (-10.0f), (15.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t53  L_78 = Quaternion_get_identity_m196(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t54 * L_79 = Object_Instantiate_m197(NULL /*static, unused*/, L_76, L_77, L_78, /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstanceB_16 = ((GameObject_t5 *)IsInstSealed(L_79, GameObject_t5_il2cpp_TypeInfo_var));
		goto IL_0328;
	}

IL_02d6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		GameObject_t5 * L_80 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstanceB_16;
		Object_Destroy_m227(NULL /*static, unused*/, L_80, /*hidden argument*/NULL);
		GameManager_t22 * L_81 = (__this->___U3CU3Ef__this_23);
		NullCheck(L_81);
		GameObjectU5BU5D_t4* L_82 = (L_81->___dices_14);
		int32_t L_83 = (__this->___U3CdnU3E__7_7);
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, ((int32_t)((int32_t)L_83-(int32_t)1)));
		int32_t L_84 = ((int32_t)((int32_t)L_83-(int32_t)1));
		__this->___U3CtoInstantiateU3E__12_12 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_82, L_84, sizeof(GameObject_t5 *)));
		GameObject_t5 * L_85 = (__this->___U3CtoInstantiateU3E__12_12);
		Vector3_t35  L_86 = {0};
		Vector3__ctor_m192(&L_86, (-10.0f), (15.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t53  L_87 = Quaternion_get_identity_m196(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t54 * L_88 = Object_Instantiate_m197(NULL /*static, unused*/, L_85, L_86, L_87, /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstanceB_16 = ((GameObject_t5 *)IsInstSealed(L_88, GameObject_t5_il2cpp_TypeInfo_var));
	}

IL_0328:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		PlayerTwo_t12 * L_89 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerTwoScript_4;
		float L_90 = (__this->___U3CxU3E__5_5);
		float L_91 = (__this->___U3CyU3E__6_6);
		NullCheck(L_89);
		PlayerTwo_ChangeTransform_m176(L_89, L_90, L_91, /*hidden argument*/NULL);
	}

IL_033e:
	{
		StringU5BU5D_t16* L_92 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_92);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_92, 0);
		int32_t L_93 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_94 = String_op_Equality_m204(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_92, L_93, sizeof(String_t*))), _stringLiteral39, /*hidden argument*/NULL);
		if (!L_94)
		{
			goto IL_05db;
		}
	}
	{
		GameManager_t22 * L_95 = (__this->___U3CU3Ef__this_23);
		NullCheck(L_95);
		GameObject_t5 * L_96 = (L_95->___rollDiceButton_18);
		NullCheck(L_96);
		GameObject_SetActive_m238(L_96, 0, /*hidden argument*/NULL);
		StringU5BU5D_t16* L_97 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_97);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_97, 7);
		int32_t L_98 = 7;
		__this->___U3CResetBoolU3E__13_13 = (*(String_t**)(String_t**)SZArrayLdElema(L_97, L_98, sizeof(String_t*)));
		String_t* L_99 = (__this->___U3CResetBoolU3E__13_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_100 = String_op_Equality_m204(NULL /*static, unused*/, L_99, _stringLiteral33, /*hidden argument*/NULL);
		if (!L_100)
		{
			goto IL_0395;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		CountdownTimer_t10 * L_101 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		NullCheck(L_101);
		L_101->___CountdownNumber_8 = ((int32_t)10);
	}

IL_0395:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		CountdownTimer_t10 * L_102 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		CountdownTimer_t10 * L_103 = L_102;
		NullCheck(L_103);
		int32_t L_104 = (L_103->___CountdownNumber_8);
		NullCheck(L_103);
		L_103->___CountdownNumber_8 = ((int32_t)((int32_t)L_104-(int32_t)1));
		CountdownTimer_t10 * L_105 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		NullCheck(L_105);
		int32_t L_106 = (L_105->___CountdownNumber_8);
		int32_t L_107 = L_106;
		Object_t * L_108 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_107);
		Debug_Log_m202(NULL /*static, unused*/, L_108, /*hidden argument*/NULL);
		CountdownTimer_t10 * L_109 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		NullCheck(L_109);
		int32_t L_110 = (L_109->___CountdownNumber_8);
		if (L_110)
		{
			goto IL_03ed;
		}
	}
	{
		GameManager_t22 * L_111 = (__this->___U3CU3Ef__this_23);
		GameManager_t22 * L_112 = (__this->___U3CU3Ef__this_23);
		NullCheck(L_112);
		Object_t * L_113 = GameManager_AutomaticMovePlayer2_m89(L_112, /*hidden argument*/NULL);
		NullCheck(L_111);
		MonoBehaviour_StartCoroutine_m207(L_111, L_113, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		CountdownTimer_t10 * L_114 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		NullCheck(L_114);
		L_114->___CountdownNumber_8 = ((int32_t)10);
	}

IL_03ed:
	{
		StringU5BU5D_t16* L_115 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_115);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_115, 1);
		int32_t L_116 = 1;
		int32_t L_117 = Int32_Parse_m244(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_115, L_116, sizeof(String_t*))), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_117) == ((uint32_t)6))))
		{
			goto IL_0481;
		}
	}
	{
		StringU5BU5D_t16* L_118 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_118);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_118, 2);
		int32_t L_119 = 2;
		int32_t L_120 = Int32_Parse_m244(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_118, L_119, sizeof(String_t*))), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_120) == ((uint32_t)7))))
		{
			goto IL_0481;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___gameOverBool_8 = 1;
		GameObject_t5 * L_121 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___turnText_19;
		NullCheck(L_121);
		Text_t47 * L_122 = GameObject_GetComponent_TisText_t47_m213(L_121, /*hidden argument*/GameObject_GetComponent_TisText_t47_m213_MethodInfo_var);
		NullCheck(L_122);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_122, _stringLiteral40);
		StringU5BU5D_t16* L_123 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_123);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_123, 1);
		int32_t L_124 = 1;
		float L_125 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_123, L_124, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CxU3E__14_14 = L_125;
		StringU5BU5D_t16* L_126 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_126);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_126, 2);
		int32_t L_127 = 2;
		float L_128 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_126, L_127, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CyU3E__15_15 = L_128;
		StringU5BU5D_t16* L_129 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_129);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_129, 3);
		int32_t L_130 = 3;
		int32_t L_131 = Int32_Parse_m244(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_129, L_130, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CdnU3E__16_16 = L_131;
		PlayerTwo_t12 * L_132 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerTwoScript_4;
		float L_133 = (__this->___U3CxU3E__14_14);
		float L_134 = (__this->___U3CyU3E__15_15);
		NullCheck(L_132);
		PlayerTwo_ChangeTransform_m176(L_132, L_133, L_134, /*hidden argument*/NULL);
		goto IL_056c;
	}

IL_0481:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		GameObject_t5 * L_135 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___turnText_19;
		NullCheck(L_135);
		Text_t47 * L_136 = GameObject_GetComponent_TisText_t47_m213(L_135, /*hidden argument*/GameObject_GetComponent_TisText_t47_m213_MethodInfo_var);
		NullCheck(L_136);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_136, _stringLiteral41);
		StringU5BU5D_t16* L_137 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_137);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_137, 4);
		int32_t L_138 = 4;
		float L_139 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_137, L_138, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CPlayerxU3E__17_17 = L_139;
		StringU5BU5D_t16* L_140 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_140);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_140, 5);
		int32_t L_141 = 5;
		float L_142 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_140, L_141, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CPlayeryU3E__18_18 = L_142;
		StringU5BU5D_t16* L_143 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_143);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_143, 6);
		int32_t L_144 = 6;
		int32_t L_145 = Int32_Parse_m244(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_143, L_144, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CpU3E__19_19 = L_145;
		int32_t L_146 = (__this->___U3CpU3E__19_19);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22 = L_146;
		GameObject_t5 * L_147 = GameObject_Find_m209(NULL /*static, unused*/, _stringLiteral3, /*hidden argument*/NULL);
		NullCheck(L_147);
		Transform_t3 * L_148 = GameObject_get_transform_m194(L_147, /*hidden argument*/NULL);
		float L_149 = (__this->___U3CPlayerxU3E__17_17);
		float L_150 = (__this->___U3CPlayeryU3E__18_18);
		Vector3_t35  L_151 = {0};
		Vector3__ctor_m192(&L_151, L_149, L_150, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_148);
		Transform_set_position_m245(L_148, L_151, /*hidden argument*/NULL);
		int32_t L_152 = (__this->___U3CpU3E__19_19);
		PlayerPrefs_SetInt_m246(NULL /*static, unused*/, _stringLiteral38, L_152, /*hidden argument*/NULL);
		float L_153 = (__this->___U3CPlayerxU3E__17_17);
		if ((!(((float)L_153) == ((float)(7.0f)))))
		{
			goto IL_0552;
		}
	}
	{
		float L_154 = (__this->___U3CPlayeryU3E__18_18);
		if ((!(((float)L_154) == ((float)(6.0f)))))
		{
			goto IL_0552;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___gameOverBool_8 = 1;
		GameObject_t5 * L_155 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___turnText_19;
		NullCheck(L_155);
		Text_t47 * L_156 = GameObject_GetComponent_TisText_t47_m213(L_155, /*hidden argument*/GameObject_GetComponent_TisText_t47_m213_MethodInfo_var);
		NullCheck(L_156);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_156, _stringLiteral40);
		goto IL_056c;
	}

IL_0552:
	{
		GameManager_t22 * L_157 = (__this->___U3CU3Ef__this_23);
		NullCheck(L_157);
		MonoBehaviour_InvokeRepeating_m242(L_157, _stringLiteral42, (1.0f), (0.3f), /*hidden argument*/NULL);
	}

IL_056c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		GameObject_t5 * L_158 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstance_15;
		Object_Destroy_m227(NULL /*static, unused*/, L_158, /*hidden argument*/NULL);
		StringU5BU5D_t16* L_159 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_159);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_159, 8);
		int32_t L_160 = 8;
		int32_t L_161 = Int32_Parse_m244(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_159, L_160, sizeof(String_t*))), /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21 = L_161;
		GameManager_t22 * L_162 = (__this->___U3CU3Ef__this_23);
		NullCheck(L_162);
		GameObjectU5BU5D_t4* L_163 = (L_162->___dices_14);
		int32_t L_164 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		NullCheck(L_163);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_163, ((int32_t)((int32_t)L_164-(int32_t)1)));
		int32_t L_165 = ((int32_t)((int32_t)L_164-(int32_t)1));
		__this->___U3CtoInstantiateU3E__20_20 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_163, L_165, sizeof(GameObject_t5 *)));
		GameObject_t5 * L_166 = (__this->___U3CtoInstantiateU3E__20_20);
		Vector3_t35  L_167 = {0};
		Vector3__ctor_m192(&L_167, (21.0f), (15.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t53  L_168 = Quaternion_get_identity_m196(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t54 * L_169 = Object_Instantiate_m197(NULL /*static, unused*/, L_166, L_167, L_168, /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstance_15 = ((GameObject_t5 *)IsInstSealed(L_169, GameObject_t5_il2cpp_TypeInfo_var));
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___callCoroutineBool_7 = 1;
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playersTurn_6 = 0;
	}

IL_05db:
	{
		__this->___U24PC_21 = (-1);
	}

IL_05e2:
	{
		return 0;
	}

IL_05e4:
	{
		return 1;
	}
	// Dead block : IL_05e6: ldloc.1
}
// System.Void GameManager/<GetPlayer1Turn>c__Iterator4::Dispose()
extern "C" void U3CGetPlayer1TurnU3Ec__Iterator4_Dispose_m50 (U3CGetPlayer1TurnU3Ec__Iterator4_t21 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_21 = (-1);
		return;
	}
}
// System.Void GameManager/<GetPlayer1Turn>c__Iterator4::Reset()
extern TypeInfo* NotSupportedException_t55_il2cpp_TypeInfo_var;
extern "C" void U3CGetPlayer1TurnU3Ec__Iterator4_Reset_m51 (U3CGetPlayer1TurnU3Ec__Iterator4_t21 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t55 * L_0 = (NotSupportedException_t55 *)il2cpp_codegen_object_new (NotSupportedException_t55_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m208(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void GameManager/<GetPlayer2Turn>c__Iterator5::.ctor()
extern "C" void U3CGetPlayer2TurnU3Ec__Iterator5__ctor_m52 (U3CGetPlayer2TurnU3Ec__Iterator5_t23 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object GameManager/<GetPlayer2Turn>c__Iterator5::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CGetPlayer2TurnU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m53 (U3CGetPlayer2TurnU3Ec__Iterator5_t23 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_21);
		return L_0;
	}
}
// System.Object GameManager/<GetPlayer2Turn>c__Iterator5::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetPlayer2TurnU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m54 (U3CGetPlayer2TurnU3Ec__Iterator5_t23 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_21);
		return L_0;
	}
}
// System.Boolean GameManager/<GetPlayer2Turn>c__Iterator5::MoveNext()
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t58_il2cpp_TypeInfo_var;
extern TypeInfo* GameManager_t22_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t56_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t5_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t47_m213_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral43;
extern Il2CppCodeGenString* _stringLiteral44;
extern Il2CppCodeGenString* _stringLiteral33;
extern Il2CppCodeGenString* _stringLiteral34;
extern Il2CppCodeGenString* _stringLiteral35;
extern Il2CppCodeGenString* _stringLiteral45;
extern Il2CppCodeGenString* _stringLiteral37;
extern Il2CppCodeGenString* _stringLiteral5;
extern Il2CppCodeGenString* _stringLiteral38;
extern Il2CppCodeGenString* _stringLiteral39;
extern Il2CppCodeGenString* _stringLiteral40;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral42;
extern "C" bool U3CGetPlayer2TurnU3Ec__Iterator5_MoveNext_m55 (U3CGetPlayer2TurnU3Ec__Iterator5_t23 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		CharU5BU5D_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GameManager_t22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Int32_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		GameObject_t5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		GameObject_GetComponent_TisText_t47_m213_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral43 = il2cpp_codegen_string_literal_from_index(43);
		_stringLiteral44 = il2cpp_codegen_string_literal_from_index(44);
		_stringLiteral33 = il2cpp_codegen_string_literal_from_index(33);
		_stringLiteral34 = il2cpp_codegen_string_literal_from_index(34);
		_stringLiteral35 = il2cpp_codegen_string_literal_from_index(35);
		_stringLiteral45 = il2cpp_codegen_string_literal_from_index(45);
		_stringLiteral37 = il2cpp_codegen_string_literal_from_index(37);
		_stringLiteral5 = il2cpp_codegen_string_literal_from_index(5);
		_stringLiteral38 = il2cpp_codegen_string_literal_from_index(38);
		_stringLiteral39 = il2cpp_codegen_string_literal_from_index(39);
		_stringLiteral40 = il2cpp_codegen_string_literal_from_index(40);
		_stringLiteral41 = il2cpp_codegen_string_literal_from_index(41);
		_stringLiteral42 = il2cpp_codegen_string_literal_from_index(42);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_20);
		V_0 = L_0;
		__this->___U24PC_20 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0074;
		}
	}
	{
		goto IL_05fb;
	}

IL_0021:
	{
		WWWForm_t8 * L_2 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_2, /*hidden argument*/NULL);
		__this->___U3CformU3E__0_0 = L_2;
		WWWForm_t8 * L_3 = (__this->___U3CformU3E__0_0);
		String_t* L_4 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral1, /*hidden argument*/NULL);
		NullCheck(L_3);
		WWWForm_AddField_m203(L_3, _stringLiteral2, L_4, /*hidden argument*/NULL);
		WWWForm_t8 * L_5 = (__this->___U3CformU3E__0_0);
		WWW_t9 * L_6 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_6, _stringLiteral43, L_5, /*hidden argument*/NULL);
		__this->___U3CwU3E__1_1 = L_6;
		WWW_t9 * L_7 = (__this->___U3CwU3E__1_1);
		__this->___U24current_21 = L_7;
		__this->___U24PC_20 = 1;
		goto IL_05fd;
	}

IL_0074:
	{
		WWW_t9 * L_8 = (__this->___U3CwU3E__1_1);
		NullCheck(L_8);
		String_t* L_9 = WWW_get_text_m206(L_8, /*hidden argument*/NULL);
		__this->___U3CJsonStringU3E__2_2 = L_9;
		WWW_t9 * L_10 = (__this->___U3CwU3E__1_1);
		NullCheck(L_10);
		String_t* L_11 = WWW_get_text_m206(L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_12 = String_Concat_m217(NULL /*static, unused*/, _stringLiteral44, L_11, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		String_t* L_13 = (__this->___U3CJsonStringU3E__2_2);
		CharU5BU5D_t58* L_14 = ((CharU5BU5D_t58*)SZArrayNew(CharU5BU5D_t58_il2cpp_TypeInfo_var, 1));
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_14, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)45);
		NullCheck(L_13);
		StringU5BU5D_t16* L_15 = String_Split_m219(L_13, L_14, /*hidden argument*/NULL);
		__this->___U3CarrayU3E__3_3 = L_15;
		StringU5BU5D_t16* L_16 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 0);
		int32_t L_17 = 0;
		bool L_18 = String_op_Equality_m204(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_16, L_17, sizeof(String_t*))), _stringLiteral33, /*hidden argument*/NULL);
		if (!L_18)
		{
			goto IL_033e;
		}
	}
	{
		StringU5BU5D_t16* L_19 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 7);
		int32_t L_20 = 7;
		__this->___U3CResetBoolU3E__4_4 = (*(String_t**)(String_t**)SZArrayLdElema(L_19, L_20, sizeof(String_t*)));
		String_t* L_21 = (__this->___U3CResetBoolU3E__4_4);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_22 = String_op_Equality_m204(NULL /*static, unused*/, L_21, _stringLiteral33, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0101;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		CountdownTimer_t10 * L_23 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		NullCheck(L_23);
		L_23->___CountdownNumber_8 = ((int32_t)10);
	}

IL_0101:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		CountdownTimer_t10 * L_24 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		CountdownTimer_t10 * L_25 = L_24;
		NullCheck(L_25);
		int32_t L_26 = (L_25->___CountdownNumber_8);
		NullCheck(L_25);
		L_25->___CountdownNumber_8 = ((int32_t)((int32_t)L_26-(int32_t)1));
		CountdownTimer_t10 * L_27 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		NullCheck(L_27);
		int32_t L_28 = (L_27->___CountdownNumber_8);
		int32_t L_29 = L_28;
		Object_t * L_30 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_29);
		Debug_Log_m202(NULL /*static, unused*/, L_30, /*hidden argument*/NULL);
		CountdownTimer_t10 * L_31 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		NullCheck(L_31);
		int32_t L_32 = (L_31->___CountdownNumber_8);
		if (L_32)
		{
			goto IL_0159;
		}
	}
	{
		GameManager_t22 * L_33 = (__this->___U3CU3Ef__this_22);
		GameManager_t22 * L_34 = (__this->___U3CU3Ef__this_22);
		NullCheck(L_34);
		Object_t * L_35 = GameManager_AutomaticMovePlayer2_m89(L_34, /*hidden argument*/NULL);
		NullCheck(L_33);
		MonoBehaviour_StartCoroutine_m207(L_33, L_35, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		CountdownTimer_t10 * L_36 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		NullCheck(L_36);
		L_36->___CountdownNumber_8 = ((int32_t)10);
	}

IL_0159:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		GameObject_t5 * L_37 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___turnText_19;
		NullCheck(L_37);
		Text_t47 * L_38 = GameObject_GetComponent_TisText_t47_m213(L_37, /*hidden argument*/GameObject_GetComponent_TisText_t47_m213_MethodInfo_var);
		NullCheck(L_38);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_38, _stringLiteral34);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playersTurn_6 = 1;
		GameManager_t22 * L_39 = (__this->___U3CU3Ef__this_22);
		NullCheck(L_39);
		MonoBehaviour_InvokeRepeating_m242(L_39, _stringLiteral35, (0.0f), (0.3f), /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___MoveDice_11 = 1;
		StringU5BU5D_t16* L_40 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, 1);
		int32_t L_41 = 1;
		StringU5BU5D_t16* L_42 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_42);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_42, 2);
		int32_t L_43 = 2;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_44 = String_Concat_m221(NULL /*static, unused*/, _stringLiteral45, (*(String_t**)(String_t**)SZArrayLdElema(L_40, L_41, sizeof(String_t*))), _stringLiteral37, (*(String_t**)(String_t**)SZArrayLdElema(L_42, L_43, sizeof(String_t*))), /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_44, /*hidden argument*/NULL);
		StringU5BU5D_t16* L_45 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_45);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_45, 1);
		int32_t L_46 = 1;
		float L_47 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_45, L_46, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CxU3E__5_5 = L_47;
		StringU5BU5D_t16* L_48 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_48);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_48, 2);
		int32_t L_49 = 2;
		float L_50 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_48, L_49, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CyU3E__6_6 = L_50;
		StringU5BU5D_t16* L_51 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_51);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_51, 3);
		int32_t L_52 = 3;
		int32_t L_53 = Int32_Parse_m244(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_51, L_52, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CdnU3E__7_7 = L_53;
		StringU5BU5D_t16* L_54 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_54);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_54, 4);
		int32_t L_55 = 4;
		float L_56 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_54, L_55, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CPlayerxU3E__8_8 = L_56;
		StringU5BU5D_t16* L_57 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_57);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_57, 5);
		int32_t L_58 = 5;
		float L_59 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_57, L_58, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CPlayeryU3E__9_9 = L_59;
		StringU5BU5D_t16* L_60 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_60);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_60, 6);
		int32_t L_61 = 6;
		int32_t L_62 = Int32_Parse_m244(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_60, L_61, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CpU3E__10_10 = L_62;
		int32_t L_63 = (__this->___U3CpU3E__10_10);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22 = L_63;
		GameObject_t5 * L_64 = GameObject_Find_m209(NULL /*static, unused*/, _stringLiteral5, /*hidden argument*/NULL);
		NullCheck(L_64);
		Transform_t3 * L_65 = GameObject_get_transform_m194(L_64, /*hidden argument*/NULL);
		float L_66 = (__this->___U3CPlayerxU3E__8_8);
		float L_67 = (__this->___U3CPlayeryU3E__9_9);
		Vector3_t35  L_68 = {0};
		Vector3__ctor_m192(&L_68, L_66, L_67, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_65);
		Transform_set_position_m245(L_65, L_68, /*hidden argument*/NULL);
		int32_t L_69 = (__this->___U3CpU3E__10_10);
		PlayerPrefs_SetInt_m246(NULL /*static, unused*/, _stringLiteral38, L_69, /*hidden argument*/NULL);
		int32_t L_70 = (__this->___U3CdnU3E__7_7);
		if ((((int32_t)L_70) <= ((int32_t)0)))
		{
			goto IL_0286;
		}
	}
	{
		int32_t L_71 = (__this->___U3CdnU3E__7_7);
		if ((((int32_t)L_71) <= ((int32_t)6)))
		{
			goto IL_02d6;
		}
	}

IL_0286:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		GameObject_t5 * L_72 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstanceB_16;
		Object_Destroy_m227(NULL /*static, unused*/, L_72, /*hidden argument*/NULL);
		GameManager_t22 * L_73 = (__this->___U3CU3Ef__this_22);
		NullCheck(L_73);
		GameObjectU5BU5D_t4* L_74 = (L_73->___dices_14);
		NullCheck(L_74);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_74, 1);
		int32_t L_75 = 1;
		__this->___U3CtoInstantiateU3E__11_11 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_74, L_75, sizeof(GameObject_t5 *)));
		GameObject_t5 * L_76 = (__this->___U3CtoInstantiateU3E__11_11);
		Vector3_t35  L_77 = {0};
		Vector3__ctor_m192(&L_77, (-10.0f), (15.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t53  L_78 = Quaternion_get_identity_m196(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t54 * L_79 = Object_Instantiate_m197(NULL /*static, unused*/, L_76, L_77, L_78, /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstanceB_16 = ((GameObject_t5 *)IsInstSealed(L_79, GameObject_t5_il2cpp_TypeInfo_var));
		goto IL_0328;
	}

IL_02d6:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		GameObject_t5 * L_80 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstanceB_16;
		Object_Destroy_m227(NULL /*static, unused*/, L_80, /*hidden argument*/NULL);
		GameManager_t22 * L_81 = (__this->___U3CU3Ef__this_22);
		NullCheck(L_81);
		GameObjectU5BU5D_t4* L_82 = (L_81->___dices_14);
		int32_t L_83 = (__this->___U3CdnU3E__7_7);
		NullCheck(L_82);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_82, ((int32_t)((int32_t)L_83-(int32_t)1)));
		int32_t L_84 = ((int32_t)((int32_t)L_83-(int32_t)1));
		__this->___U3CtoInstantiateU3E__12_12 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_82, L_84, sizeof(GameObject_t5 *)));
		GameObject_t5 * L_85 = (__this->___U3CtoInstantiateU3E__12_12);
		Vector3_t35  L_86 = {0};
		Vector3__ctor_m192(&L_86, (-10.0f), (15.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t53  L_87 = Quaternion_get_identity_m196(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t54 * L_88 = Object_Instantiate_m197(NULL /*static, unused*/, L_85, L_86, L_87, /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstanceB_16 = ((GameObject_t5 *)IsInstSealed(L_88, GameObject_t5_il2cpp_TypeInfo_var));
	}

IL_0328:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		Player_t11 * L_89 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerScript_3;
		float L_90 = (__this->___U3CxU3E__5_5);
		float L_91 = (__this->___U3CyU3E__6_6);
		NullCheck(L_89);
		Player_ChangeTransform_m155(L_89, L_90, L_91, /*hidden argument*/NULL);
	}

IL_033e:
	{
		StringU5BU5D_t16* L_92 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_92);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_92, 0);
		int32_t L_93 = 0;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_94 = String_op_Equality_m204(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_92, L_93, sizeof(String_t*))), _stringLiteral39, /*hidden argument*/NULL);
		if (!L_94)
		{
			goto IL_05f4;
		}
	}
	{
		GameManager_t22 * L_95 = (__this->___U3CU3Ef__this_22);
		NullCheck(L_95);
		GameObject_t5 * L_96 = (L_95->___rollDiceButton_18);
		NullCheck(L_96);
		GameObject_SetActive_m238(L_96, 0, /*hidden argument*/NULL);
		StringU5BU5D_t16* L_97 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_97);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_97, 7);
		int32_t L_98 = 7;
		__this->___U3CResetBoolU3E__13_13 = (*(String_t**)(String_t**)SZArrayLdElema(L_97, L_98, sizeof(String_t*)));
		String_t* L_99 = (__this->___U3CResetBoolU3E__13_13);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_100 = String_op_Equality_m204(NULL /*static, unused*/, L_99, _stringLiteral33, /*hidden argument*/NULL);
		if (!L_100)
		{
			goto IL_0395;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		CountdownTimer_t10 * L_101 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		NullCheck(L_101);
		L_101->___CountdownNumber_8 = ((int32_t)10);
	}

IL_0395:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		CountdownTimer_t10 * L_102 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		CountdownTimer_t10 * L_103 = L_102;
		NullCheck(L_103);
		int32_t L_104 = (L_103->___CountdownNumber_8);
		NullCheck(L_103);
		L_103->___CountdownNumber_8 = ((int32_t)((int32_t)L_104-(int32_t)1));
		CountdownTimer_t10 * L_105 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		NullCheck(L_105);
		int32_t L_106 = (L_105->___CountdownNumber_8);
		int32_t L_107 = L_106;
		Object_t * L_108 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_107);
		Debug_Log_m202(NULL /*static, unused*/, L_108, /*hidden argument*/NULL);
		CountdownTimer_t10 * L_109 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		NullCheck(L_109);
		int32_t L_110 = (L_109->___CountdownNumber_8);
		if (L_110)
		{
			goto IL_03ed;
		}
	}
	{
		GameManager_t22 * L_111 = (__this->___U3CU3Ef__this_22);
		GameManager_t22 * L_112 = (__this->___U3CU3Ef__this_22);
		NullCheck(L_112);
		Object_t * L_113 = GameManager_AutomaticMovePlayer1_m88(L_112, /*hidden argument*/NULL);
		NullCheck(L_111);
		MonoBehaviour_StartCoroutine_m207(L_111, L_113, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		CountdownTimer_t10 * L_114 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		NullCheck(L_114);
		L_114->___CountdownNumber_8 = ((int32_t)10);
	}

IL_03ed:
	{
		StringU5BU5D_t16* L_115 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_115);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_115, 1);
		int32_t L_116 = 1;
		int32_t L_117 = Int32_Parse_m244(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_115, L_116, sizeof(String_t*))), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_117) == ((uint32_t)7))))
		{
			goto IL_046e;
		}
	}
	{
		StringU5BU5D_t16* L_118 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_118);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_118, 2);
		int32_t L_119 = 2;
		int32_t L_120 = Int32_Parse_m244(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_118, L_119, sizeof(String_t*))), /*hidden argument*/NULL);
		if ((!(((uint32_t)L_120) == ((uint32_t)6))))
		{
			goto IL_046e;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___gameOverBool_8 = 1;
		GameObject_t5 * L_121 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___turnText_19;
		NullCheck(L_121);
		Text_t47 * L_122 = GameObject_GetComponent_TisText_t47_m213(L_121, /*hidden argument*/GameObject_GetComponent_TisText_t47_m213_MethodInfo_var);
		NullCheck(L_122);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_122, _stringLiteral40);
		StringU5BU5D_t16* L_123 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_123);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_123, 1);
		int32_t L_124 = 1;
		float L_125 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_123, L_124, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CxU3E__14_14 = L_125;
		StringU5BU5D_t16* L_126 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_126);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_126, 2);
		int32_t L_127 = 2;
		float L_128 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_126, L_127, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CyU3E__15_15 = L_128;
		Player_t11 * L_129 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerScript_3;
		float L_130 = (__this->___U3CxU3E__14_14);
		float L_131 = (__this->___U3CyU3E__15_15);
		NullCheck(L_129);
		Player_ChangeTransform_m155(L_129, L_130, L_131, /*hidden argument*/NULL);
		goto IL_0559;
	}

IL_046e:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		GameObject_t5 * L_132 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___turnText_19;
		NullCheck(L_132);
		Text_t47 * L_133 = GameObject_GetComponent_TisText_t47_m213(L_132, /*hidden argument*/GameObject_GetComponent_TisText_t47_m213_MethodInfo_var);
		NullCheck(L_133);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_133, _stringLiteral41);
		StringU5BU5D_t16* L_134 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_134);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_134, 4);
		int32_t L_135 = 4;
		float L_136 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_134, L_135, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CPlayerxU3E__16_16 = L_136;
		StringU5BU5D_t16* L_137 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_137);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_137, 5);
		int32_t L_138 = 5;
		float L_139 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_137, L_138, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CPlayeryU3E__17_17 = L_139;
		StringU5BU5D_t16* L_140 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_140);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_140, 6);
		int32_t L_141 = 6;
		int32_t L_142 = Int32_Parse_m244(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_140, L_141, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CpU3E__18_18 = L_142;
		int32_t L_143 = (__this->___U3CpU3E__18_18);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22 = L_143;
		GameObject_t5 * L_144 = GameObject_Find_m209(NULL /*static, unused*/, _stringLiteral5, /*hidden argument*/NULL);
		NullCheck(L_144);
		Transform_t3 * L_145 = GameObject_get_transform_m194(L_144, /*hidden argument*/NULL);
		float L_146 = (__this->___U3CPlayerxU3E__16_16);
		float L_147 = (__this->___U3CPlayeryU3E__17_17);
		Vector3_t35  L_148 = {0};
		Vector3__ctor_m192(&L_148, L_146, L_147, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_145);
		Transform_set_position_m245(L_145, L_148, /*hidden argument*/NULL);
		int32_t L_149 = (__this->___U3CpU3E__18_18);
		PlayerPrefs_SetInt_m246(NULL /*static, unused*/, _stringLiteral38, L_149, /*hidden argument*/NULL);
		float L_150 = (__this->___U3CPlayerxU3E__16_16);
		if ((!(((float)L_150) == ((float)(6.0f)))))
		{
			goto IL_053f;
		}
	}
	{
		float L_151 = (__this->___U3CPlayeryU3E__17_17);
		if ((!(((float)L_151) == ((float)(7.0f)))))
		{
			goto IL_053f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___gameOverBool_8 = 1;
		GameObject_t5 * L_152 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___turnText_19;
		NullCheck(L_152);
		Text_t47 * L_153 = GameObject_GetComponent_TisText_t47_m213(L_152, /*hidden argument*/GameObject_GetComponent_TisText_t47_m213_MethodInfo_var);
		NullCheck(L_153);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_153, _stringLiteral40);
		goto IL_0559;
	}

IL_053f:
	{
		GameManager_t22 * L_154 = (__this->___U3CU3Ef__this_22);
		NullCheck(L_154);
		MonoBehaviour_InvokeRepeating_m242(L_154, _stringLiteral42, (1.0f), (0.3f), /*hidden argument*/NULL);
	}

IL_0559:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		GameObject_t5 * L_155 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstance_15;
		Object_Destroy_m227(NULL /*static, unused*/, L_155, /*hidden argument*/NULL);
		StringU5BU5D_t16* L_156 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_156);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_156, 8);
		int32_t L_157 = 8;
		int32_t L_158 = Int32_Parse_m244(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_156, L_157, sizeof(String_t*))), /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21 = L_158;
		GameManager_t22 * L_159 = (__this->___U3CU3Ef__this_22);
		NullCheck(L_159);
		GameObjectU5BU5D_t4* L_160 = (L_159->___dices_14);
		int32_t L_161 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		NullCheck(L_160);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_160, ((int32_t)((int32_t)L_161-(int32_t)1)));
		int32_t L_162 = ((int32_t)((int32_t)L_161-(int32_t)1));
		__this->___U3CtoInstantiateU3E__19_19 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_160, L_162, sizeof(GameObject_t5 *)));
		GameObject_t5 * L_163 = (__this->___U3CtoInstantiateU3E__19_19);
		Vector3_t35  L_164 = {0};
		Vector3__ctor_m192(&L_164, (21.0f), (15.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t53  L_165 = Quaternion_get_identity_m196(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t54 * L_166 = Object_Instantiate_m197(NULL /*static, unused*/, L_163, L_164, L_165, /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstance_15 = ((GameObject_t5 *)IsInstSealed(L_166, GameObject_t5_il2cpp_TypeInfo_var));
		GameManager_t22 * L_167 = (__this->___U3CU3Ef__this_22);
		NullCheck(L_167);
		GameObject_t5 * L_168 = (L_167->___rollDiceButton_18);
		bool L_169 = Object_op_Equality_m247(NULL /*static, unused*/, L_168, (Object_t54 *)NULL, /*hidden argument*/NULL);
		if (!L_169)
		{
			goto IL_05d7;
		}
	}
	{
		goto IL_05e8;
	}

IL_05d7:
	{
		GameManager_t22 * L_170 = (__this->___U3CU3Ef__this_22);
		NullCheck(L_170);
		GameObject_t5 * L_171 = (L_170->___rollDiceButton_18);
		NullCheck(L_171);
		GameObject_SetActive_m238(L_171, 0, /*hidden argument*/NULL);
	}

IL_05e8:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___callCoroutineBool_7 = 1;
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playersTurn_6 = 0;
	}

IL_05f4:
	{
		__this->___U24PC_20 = (-1);
	}

IL_05fb:
	{
		return 0;
	}

IL_05fd:
	{
		return 1;
	}
	// Dead block : IL_05ff: ldloc.1
}
// System.Void GameManager/<GetPlayer2Turn>c__Iterator5::Dispose()
extern "C" void U3CGetPlayer2TurnU3Ec__Iterator5_Dispose_m56 (U3CGetPlayer2TurnU3Ec__Iterator5_t23 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_20 = (-1);
		return;
	}
}
// System.Void GameManager/<GetPlayer2Turn>c__Iterator5::Reset()
extern TypeInfo* NotSupportedException_t55_il2cpp_TypeInfo_var;
extern "C" void U3CGetPlayer2TurnU3Ec__Iterator5_Reset_m57 (U3CGetPlayer2TurnU3Ec__Iterator5_t23 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t55 * L_0 = (NotSupportedException_t55 *)il2cpp_codegen_object_new (NotSupportedException_t55_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m208(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void GameManager/<AutomaticMovePlayer1>c__Iterator6::.ctor()
extern "C" void U3CAutomaticMovePlayer1U3Ec__Iterator6__ctor_m58 (U3CAutomaticMovePlayer1U3Ec__Iterator6_t24 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object GameManager/<AutomaticMovePlayer1>c__Iterator6::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CAutomaticMovePlayer1U3Ec__Iterator6_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m59 (U3CAutomaticMovePlayer1U3Ec__Iterator6_t24 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Object GameManager/<AutomaticMovePlayer1>c__Iterator6::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CAutomaticMovePlayer1U3Ec__Iterator6_System_Collections_IEnumerator_get_Current_m60 (U3CAutomaticMovePlayer1U3Ec__Iterator6_t24 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_3);
		return L_0;
	}
}
// System.Boolean GameManager/<AutomaticMovePlayer1>c__Iterator6::MoveNext()
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral46;
extern Il2CppCodeGenString* _stringLiteral47;
extern "C" bool U3CAutomaticMovePlayer1U3Ec__Iterator6_MoveNext_m61 (U3CAutomaticMovePlayer1U3Ec__Iterator6_t24 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral46 = il2cpp_codegen_string_literal_from_index(46);
		_stringLiteral47 = il2cpp_codegen_string_literal_from_index(47);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_2);
		V_0 = L_0;
		__this->___U24PC_2 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0074;
		}
	}
	{
		goto IL_0095;
	}

IL_0021:
	{
		WWWForm_t8 * L_2 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_2, /*hidden argument*/NULL);
		__this->___U3CformU3E__0_0 = L_2;
		WWWForm_t8 * L_3 = (__this->___U3CformU3E__0_0);
		String_t* L_4 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral1, /*hidden argument*/NULL);
		NullCheck(L_3);
		WWWForm_AddField_m203(L_3, _stringLiteral2, L_4, /*hidden argument*/NULL);
		WWWForm_t8 * L_5 = (__this->___U3CformU3E__0_0);
		WWW_t9 * L_6 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_6, _stringLiteral46, L_5, /*hidden argument*/NULL);
		__this->___U3CwU3E__1_1 = L_6;
		WWW_t9 * L_7 = (__this->___U3CwU3E__1_1);
		__this->___U24current_3 = L_7;
		__this->___U24PC_2 = 1;
		goto IL_0097;
	}

IL_0074:
	{
		WWW_t9 * L_8 = (__this->___U3CwU3E__1_1);
		NullCheck(L_8);
		String_t* L_9 = WWW_get_text_m206(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m217(NULL /*static, unused*/, _stringLiteral47, L_9, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		__this->___U24PC_2 = (-1);
	}

IL_0095:
	{
		return 0;
	}

IL_0097:
	{
		return 1;
	}
	// Dead block : IL_0099: ldloc.1
}
// System.Void GameManager/<AutomaticMovePlayer1>c__Iterator6::Dispose()
extern "C" void U3CAutomaticMovePlayer1U3Ec__Iterator6_Dispose_m62 (U3CAutomaticMovePlayer1U3Ec__Iterator6_t24 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_2 = (-1);
		return;
	}
}
// System.Void GameManager/<AutomaticMovePlayer1>c__Iterator6::Reset()
extern TypeInfo* NotSupportedException_t55_il2cpp_TypeInfo_var;
extern "C" void U3CAutomaticMovePlayer1U3Ec__Iterator6_Reset_m63 (U3CAutomaticMovePlayer1U3Ec__Iterator6_t24 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t55 * L_0 = (NotSupportedException_t55 *)il2cpp_codegen_object_new (NotSupportedException_t55_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m208(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void GameManager/<AutomaticMovePlayer2>c__Iterator7::.ctor()
extern "C" void U3CAutomaticMovePlayer2U3Ec__Iterator7__ctor_m64 (U3CAutomaticMovePlayer2U3Ec__Iterator7_t25 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object GameManager/<AutomaticMovePlayer2>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CAutomaticMovePlayer2U3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m65 (U3CAutomaticMovePlayer2U3Ec__Iterator7_t25 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_4);
		return L_0;
	}
}
// System.Object GameManager/<AutomaticMovePlayer2>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CAutomaticMovePlayer2U3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m66 (U3CAutomaticMovePlayer2U3Ec__Iterator7_t25 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_4);
		return L_0;
	}
}
// System.Boolean GameManager/<AutomaticMovePlayer2>c__Iterator7::MoveNext()
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* GameManager_t22_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t5_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral48;
extern Il2CppCodeGenString* _stringLiteral47;
extern "C" bool U3CAutomaticMovePlayer2U3Ec__Iterator7_MoveNext_m67 (U3CAutomaticMovePlayer2U3Ec__Iterator7_t25 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		GameManager_t22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		GameObject_t5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral48 = il2cpp_codegen_string_literal_from_index(48);
		_stringLiteral47 = il2cpp_codegen_string_literal_from_index(47);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_3);
		V_0 = L_0;
		__this->___U24PC_3 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0074;
		}
	}
	{
		goto IL_00ec;
	}

IL_0021:
	{
		WWWForm_t8 * L_2 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_2, /*hidden argument*/NULL);
		__this->___U3CformU3E__0_0 = L_2;
		WWWForm_t8 * L_3 = (__this->___U3CformU3E__0_0);
		String_t* L_4 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral1, /*hidden argument*/NULL);
		NullCheck(L_3);
		WWWForm_AddField_m203(L_3, _stringLiteral2, L_4, /*hidden argument*/NULL);
		WWWForm_t8 * L_5 = (__this->___U3CformU3E__0_0);
		WWW_t9 * L_6 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_6, _stringLiteral48, L_5, /*hidden argument*/NULL);
		__this->___U3CwU3E__1_1 = L_6;
		WWW_t9 * L_7 = (__this->___U3CwU3E__1_1);
		__this->___U24current_4 = L_7;
		__this->___U24PC_3 = 1;
		goto IL_00ee;
	}

IL_0074:
	{
		WWW_t9 * L_8 = (__this->___U3CwU3E__1_1);
		NullCheck(L_8);
		String_t* L_9 = WWW_get_text_m206(L_8, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_10 = String_Concat_m217(NULL /*static, unused*/, _stringLiteral47, L_9, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		GameObject_t5 * L_11 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstance_15;
		Object_Destroy_m227(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21 = 1;
		GameManager_t22 * L_12 = (__this->___U3CU3Ef__this_5);
		NullCheck(L_12);
		GameObjectU5BU5D_t4* L_13 = (L_12->___dices_14);
		int32_t L_14 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, ((int32_t)((int32_t)L_14-(int32_t)1)));
		int32_t L_15 = ((int32_t)((int32_t)L_14-(int32_t)1));
		__this->___U3CtoInstantiateU3E__2_2 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_13, L_15, sizeof(GameObject_t5 *)));
		GameObject_t5 * L_16 = (__this->___U3CtoInstantiateU3E__2_2);
		Vector3_t35  L_17 = {0};
		Vector3__ctor_m192(&L_17, (21.0f), (15.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t53  L_18 = Quaternion_get_identity_m196(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t54 * L_19 = Object_Instantiate_m197(NULL /*static, unused*/, L_16, L_17, L_18, /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstance_15 = ((GameObject_t5 *)IsInstSealed(L_19, GameObject_t5_il2cpp_TypeInfo_var));
		__this->___U24PC_3 = (-1);
	}

IL_00ec:
	{
		return 0;
	}

IL_00ee:
	{
		return 1;
	}
	// Dead block : IL_00f0: ldloc.1
}
// System.Void GameManager/<AutomaticMovePlayer2>c__Iterator7::Dispose()
extern "C" void U3CAutomaticMovePlayer2U3Ec__Iterator7_Dispose_m68 (U3CAutomaticMovePlayer2U3Ec__Iterator7_t25 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_3 = (-1);
		return;
	}
}
// System.Void GameManager/<AutomaticMovePlayer2>c__Iterator7::Reset()
extern TypeInfo* NotSupportedException_t55_il2cpp_TypeInfo_var;
extern "C" void U3CAutomaticMovePlayer2U3Ec__Iterator7_Reset_m69 (U3CAutomaticMovePlayer2U3Ec__Iterator7_t25 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t55 * L_0 = (NotSupportedException_t55 *)il2cpp_codegen_object_new (NotSupportedException_t55_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m208(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void GameManager::.ctor()
extern "C" void GameManager__ctor_m70 (GameManager_t22 * __this, const MethodInfo* method)
{
	{
		__this->___turnDelay_12 = (0.1f);
		MonoBehaviour__ctor_m191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::.cctor()
extern TypeInfo* GameManager_t22_il2cpp_TypeInfo_var;
extern "C" void GameManager__cctor_m71 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameManager_t22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___callCoroutineBool_7 = 1;
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___ShowButtonBool_9 = 1;
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___MoveForward_10 = 1;
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___MoveDice_11 = 1;
		return;
	}
}
// System.Void GameManager::Awake()
extern TypeInfo* GameManager_t22_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisBoardManager_t1_m248_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayer_t11_m210_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisPlayerTwo_t12_m211_MethodInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisCountdownTimer_t10_m249_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral5;
extern Il2CppCodeGenString* _stringLiteral49;
extern Il2CppCodeGenString* _stringLiteral50;
extern Il2CppCodeGenString* _stringLiteral51;
extern Il2CppCodeGenString* _stringLiteral52;
extern "C" void GameManager_Awake_m72 (GameManager_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameManager_t22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Component_GetComponent_TisBoardManager_t1_m248_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483653);
		GameObject_GetComponent_TisPlayer_t11_m210_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483649);
		GameObject_GetComponent_TisPlayerTwo_t12_m211_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483650);
		GameObject_GetComponent_TisCountdownTimer_t10_m249_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483654);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral5 = il2cpp_codegen_string_literal_from_index(5);
		_stringLiteral49 = il2cpp_codegen_string_literal_from_index(49);
		_stringLiteral50 = il2cpp_codegen_string_literal_from_index(50);
		_stringLiteral51 = il2cpp_codegen_string_literal_from_index(51);
		_stringLiteral52 = il2cpp_codegen_string_literal_from_index(52);
		s_Il2CppMethodIntialized = true;
	}
	{
		BoardManager_t1 * L_0 = Component_GetComponent_TisBoardManager_t1_m248(__this, /*hidden argument*/Component_GetComponent_TisBoardManager_t1_m248_MethodInfo_var);
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___boardScript_2 = L_0;
		GameObject_t5 * L_1 = GameObject_Find_m209(NULL /*static, unused*/, _stringLiteral3, /*hidden argument*/NULL);
		NullCheck(L_1);
		Player_t11 * L_2 = GameObject_GetComponent_TisPlayer_t11_m210(L_1, /*hidden argument*/GameObject_GetComponent_TisPlayer_t11_m210_MethodInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerScript_3 = L_2;
		GameObject_t5 * L_3 = GameObject_Find_m209(NULL /*static, unused*/, _stringLiteral5, /*hidden argument*/NULL);
		NullCheck(L_3);
		PlayerTwo_t12 * L_4 = GameObject_GetComponent_TisPlayerTwo_t12_m211(L_3, /*hidden argument*/GameObject_GetComponent_TisPlayerTwo_t12_m211_MethodInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerTwoScript_4 = L_4;
		GameObject_t5 * L_5 = GameObject_Find_m209(NULL /*static, unused*/, _stringLiteral49, /*hidden argument*/NULL);
		NullCheck(L_5);
		CountdownTimer_t10 * L_6 = GameObject_GetComponent_TisCountdownTimer_t10_m249(L_5, /*hidden argument*/GameObject_GetComponent_TisCountdownTimer_t10_m249_MethodInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5 = L_6;
		GameObject_t5 * L_7 = GameObject_Find_m209(NULL /*static, unused*/, _stringLiteral50, /*hidden argument*/NULL);
		__this->___rollDiceButton_18 = L_7;
		GameObject_t5 * L_8 = GameObject_Find_m209(NULL /*static, unused*/, _stringLiteral51, /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___resetButton_17 = L_8;
		GameObject_t5 * L_9 = GameObject_Find_m209(NULL /*static, unused*/, _stringLiteral52, /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___turnText_19 = L_9;
		Player_t11 * L_10 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerScript_3;
		NullCheck(L_10);
		Object_t * L_11 = Player_GetPlayer1Position_m156(L_10, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m207(__this, L_11, /*hidden argument*/NULL);
		PlayerTwo_t12 * L_12 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerTwoScript_4;
		NullCheck(L_12);
		Object_t * L_13 = PlayerTwo_GetPlayer2Position_m177(L_12, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m207(__this, L_13, /*hidden argument*/NULL);
		GameManager_InitGame_m74(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::Start()
extern "C" void GameManager_Start_m73 (GameManager_t22 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void GameManager::InitGame()
extern TypeInfo* GameManager_t22_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral53;
extern "C" void GameManager_InitGame_m74 (GameManager_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameManager_t22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral53 = il2cpp_codegen_string_literal_from_index(53);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral1, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		GameObject_t5 * L_1 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___resetButton_17;
		NullCheck(L_1);
		GameObject_SetActive_m238(L_1, 0, /*hidden argument*/NULL);
		BoardManager_t1 * L_2 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___boardScript_2;
		NullCheck(L_2);
		BoardManager_SetupScene_m5(L_2, /*hidden argument*/NULL);
		CountdownTimer_t10 * L_3 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		NullCheck(L_3);
		L_3->___CountdownNumber_8 = ((int32_t)10);
		MonoBehaviour_InvokeRepeating_m242(__this, _stringLiteral53, (1.0f), (2.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::GameOver()
extern "C" void GameManager_GameOver_m75 (GameManager_t22 * __this, const MethodInfo* method)
{
	{
		Behaviour_set_enabled_m250(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::GetNewPosition(System.Int32)
extern TypeInfo* GameManager_t22_il2cpp_TypeInfo_var;
extern TypeInfo* Vector3_t35_il2cpp_TypeInfo_var;
extern "C" void GameManager_GetNewPosition_m76 (GameManager_t22 * __this, int32_t ___DiceRoll, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameManager_t22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Vector3_t35_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(21);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		BoardManager_t1 * L_0 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___boardScript_2;
		NullCheck(L_0);
		List_1_t6 * L_1 = (L_0->___BluePath_8);
		int32_t L_2 = ___DiceRoll;
		NullCheck(L_1);
		Vector3_t35  L_3 = (Vector3_t35 )VirtFuncInvoker1< Vector3_t35 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_1, L_2);
		Vector3_t35  L_4 = L_3;
		Object_t * L_5 = Box(Vector3_t35_il2cpp_TypeInfo_var, &L_4);
		Debug_Log_m202(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::RollDice()
extern TypeInfo* GameManager_t22_il2cpp_TypeInfo_var;
extern TypeInfo* Int32_t56_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t5_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* ObjectU5BU5D_t60_il2cpp_TypeInfo_var;
extern TypeInfo* Single_t61_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t47_m213_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral50;
extern Il2CppCodeGenString* _stringLiteral54;
extern Il2CppCodeGenString* _stringLiteral41;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral4;
extern Il2CppCodeGenString* _stringLiteral55;
extern Il2CppCodeGenString* _stringLiteral56;
extern Il2CppCodeGenString* _stringLiteral57;
extern Il2CppCodeGenString* _stringLiteral58;
extern Il2CppCodeGenString* _stringLiteral59;
extern Il2CppCodeGenString* _stringLiteral60;
extern Il2CppCodeGenString* _stringLiteral23;
extern Il2CppCodeGenString* _stringLiteral5;
extern Il2CppCodeGenString* _stringLiteral38;
extern "C" void GameManager_RollDice_m77 (GameManager_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameManager_t22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		Int32_t56_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		GameObject_t5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		ObjectU5BU5D_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Single_t61_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		GameObject_GetComponent_TisText_t47_m213_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		_stringLiteral50 = il2cpp_codegen_string_literal_from_index(50);
		_stringLiteral54 = il2cpp_codegen_string_literal_from_index(54);
		_stringLiteral41 = il2cpp_codegen_string_literal_from_index(41);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral4 = il2cpp_codegen_string_literal_from_index(4);
		_stringLiteral55 = il2cpp_codegen_string_literal_from_index(55);
		_stringLiteral56 = il2cpp_codegen_string_literal_from_index(56);
		_stringLiteral57 = il2cpp_codegen_string_literal_from_index(57);
		_stringLiteral58 = il2cpp_codegen_string_literal_from_index(58);
		_stringLiteral59 = il2cpp_codegen_string_literal_from_index(59);
		_stringLiteral60 = il2cpp_codegen_string_literal_from_index(60);
		_stringLiteral23 = il2cpp_codegen_string_literal_from_index(23);
		_stringLiteral5 = il2cpp_codegen_string_literal_from_index(5);
		_stringLiteral38 = il2cpp_codegen_string_literal_from_index(38);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t5 * V_0 = {0};
	Vector3_t35  V_1 = {0};
	Vector3_t35  V_2 = {0};
	Vector3_t35  V_3 = {0};
	Vector3_t35  V_4 = {0};
	Vector3_t35  V_5 = {0};
	Vector3_t35  V_6 = {0};
	Vector3_t35  V_7 = {0};
	Vector3_t35  V_8 = {0};
	Vector3_t35  V_9 = {0};
	Vector3_t35  V_10 = {0};
	Vector3_t35  V_11 = {0};
	Vector3_t35  V_12 = {0};
	Vector3_t35  V_13 = {0};
	Vector3_t35  V_14 = {0};
	Vector3_t35  V_15 = {0};
	Vector3_t35  V_16 = {0};
	Vector3_t35  V_17 = {0};
	Vector3_t35  V_18 = {0};
	Vector3_t35  V_19 = {0};
	Vector3_t35  V_20 = {0};
	Vector3_t35  V_21 = {0};
	Vector3_t35  V_22 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playersTurn_6 = 0;
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___MoveDice_11 = 0;
		GameObject_t5 * L_0 = (__this->___rollDiceButton_18);
		bool L_1 = Object_op_Equality_m247(NULL /*static, unused*/, L_0, (Object_t54 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_002d;
		}
	}
	{
		GameObject_t5 * L_2 = GameObject_Find_m209(NULL /*static, unused*/, _stringLiteral50, /*hidden argument*/NULL);
		__this->___rollDiceButton_18 = L_2;
	}

IL_002d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		int32_t L_3 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		PlayerPrefs_SetInt_m246(NULL /*static, unused*/, _stringLiteral54, L_3, /*hidden argument*/NULL);
		GameObject_t5 * L_4 = (__this->___rollDiceButton_18);
		NullCheck(L_4);
		GameObject_SetActive_m238(L_4, 0, /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___ShowButtonBool_9 = 0;
		Player_t11 * L_5 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerScript_3;
		NullCheck(L_5);
		Player_CallExecuteAfterTime_m157(L_5, /*hidden argument*/NULL);
		GameObject_t5 * L_6 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___turnText_19;
		NullCheck(L_6);
		Text_t47 * L_7 = GameObject_GetComponent_TisText_t47_m213(L_6, /*hidden argument*/GameObject_GetComponent_TisText_t47_m213_MethodInfo_var);
		NullCheck(L_7);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_7, _stringLiteral41);
		int32_t L_8 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		int32_t L_9 = L_8;
		Object_t * L_10 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_9);
		Debug_Log_m202(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		GameObject_t5 * L_11 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstance_15;
		Object_Destroy_m227(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
		GameObjectU5BU5D_t4* L_12 = (__this->___dices_14);
		int32_t L_13 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, ((int32_t)((int32_t)L_13-(int32_t)1)));
		int32_t L_14 = ((int32_t)((int32_t)L_13-(int32_t)1));
		V_0 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_12, L_14, sizeof(GameObject_t5 *)));
		GameObject_t5 * L_15 = V_0;
		Vector3_t35  L_16 = {0};
		Vector3__ctor_m192(&L_16, (21.0f), (15.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t53  L_17 = Quaternion_get_identity_m196(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t54 * L_18 = Object_Instantiate_m197(NULL /*static, unused*/, L_15, L_16, L_17, /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstance_15 = ((GameObject_t5 *)IsInstSealed(L_18, GameObject_t5_il2cpp_TypeInfo_var));
		String_t* L_19 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_20 = String_op_Equality_m204(NULL /*static, unused*/, L_19, _stringLiteral4, /*hidden argument*/NULL);
		if (!L_20)
		{
			goto IL_0437;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		int32_t L_21 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		if (L_21)
		{
			goto IL_01d3;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		BoardManager_t1 * L_22 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___boardScript_2;
		NullCheck(L_22);
		List_1_t6 * L_23 = (L_22->___BluePath_8);
		int32_t L_24 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		NullCheck(L_23);
		Vector3_t35  L_25 = (Vector3_t35 )VirtFuncInvoker1< Vector3_t35 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_23, L_24);
		V_1 = L_25;
		int32_t L_26 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		if ((!(((uint32_t)L_26) == ((uint32_t)6))))
		{
			goto IL_0173;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21 = 1;
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22 = 1;
		BoardManager_t1 * L_27 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___boardScript_2;
		NullCheck(L_27);
		List_1_t6 * L_28 = (L_27->___BluePath_8);
		NullCheck(L_28);
		Vector3_t35  L_29 = (Vector3_t35 )VirtFuncInvoker1< Vector3_t35 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_28, 1);
		V_2 = L_29;
		Vector3_t35  L_30 = V_2;
		Vector3_t35  L_31 = V_1;
		Vector3_t35  L_32 = Vector3_op_Subtraction_m251(NULL /*static, unused*/, L_30, L_31, /*hidden argument*/NULL);
		V_3 = L_32;
		int32_t L_33 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_34 = L_33;
		Object_t * L_35 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_34);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_36 = String_Concat_m214(NULL /*static, unused*/, _stringLiteral55, L_35, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_36, /*hidden argument*/NULL);
		Player_t11 * L_37 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerScript_3;
		float L_38 = ((&V_3)->___x_1);
		float L_39 = ((&V_3)->___y_2);
		float L_40 = ((&V_2)->___x_1);
		float L_41 = ((&V_2)->___y_2);
		int32_t L_42 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		NullCheck(L_37);
		Player_MoveThePlayer_m152(L_37, (((int32_t)L_38)), (((int32_t)L_39)), (((int32_t)L_40)), (((int32_t)L_41)), L_42, 6, /*hidden argument*/NULL);
		goto IL_01ce;
	}

IL_0173:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22 = 0;
		BoardManager_t1 * L_43 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___boardScript_2;
		NullCheck(L_43);
		List_1_t6 * L_44 = (L_43->___BluePath_8);
		int32_t L_45 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		NullCheck(L_44);
		Vector3_t35  L_46 = (Vector3_t35 )VirtFuncInvoker1< Vector3_t35 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_44, L_45);
		V_4 = L_46;
		Player_t11 * L_47 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerScript_3;
		float L_48 = ((&V_4)->___x_1);
		float L_49 = ((&V_4)->___y_2);
		int32_t L_50 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_51 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		NullCheck(L_47);
		Player_MoveThePlayer_m152(L_47, 0, 0, (((int32_t)L_48)), (((int32_t)L_49)), L_50, L_51, /*hidden argument*/NULL);
		int32_t L_52 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_53 = L_52;
		Object_t * L_54 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_53);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_55 = String_Concat_m214(NULL /*static, unused*/, _stringLiteral56, L_54, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_55, /*hidden argument*/NULL);
	}

IL_01ce:
	{
		goto IL_0432;
	}

IL_01d3:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		BoardManager_t1 * L_56 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___boardScript_2;
		NullCheck(L_56);
		List_1_t6 * L_57 = (L_56->___BluePath_8);
		int32_t L_58 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		NullCheck(L_57);
		Vector3_t35  L_59 = (Vector3_t35 )VirtFuncInvoker1< Vector3_t35 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_57, L_58);
		V_5 = L_59;
		int32_t L_60 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_61 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22 = ((int32_t)((int32_t)L_60+(int32_t)L_61));
		int32_t L_62 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		if ((((int32_t)L_62) <= ((int32_t)((int32_t)57))))
		{
			goto IL_026f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		int32_t L_63 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_64 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22 = ((int32_t)((int32_t)L_63-(int32_t)L_64));
		BoardManager_t1 * L_65 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___boardScript_2;
		NullCheck(L_65);
		List_1_t6 * L_66 = (L_65->___BluePath_8);
		int32_t L_67 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		NullCheck(L_66);
		Vector3_t35  L_68 = (Vector3_t35 )VirtFuncInvoker1< Vector3_t35 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_66, L_67);
		V_6 = L_68;
		Player_t11 * L_69 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerScript_3;
		float L_70 = ((&V_6)->___x_1);
		float L_71 = ((&V_6)->___y_2);
		int32_t L_72 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_73 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		NullCheck(L_69);
		Player_MoveThePlayer_m152(L_69, 0, 0, (((int32_t)L_70)), (((int32_t)L_71)), L_72, L_73, /*hidden argument*/NULL);
		int32_t L_74 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_75 = L_74;
		Object_t * L_76 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_75);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_77 = String_Concat_m214(NULL /*static, unused*/, _stringLiteral57, L_76, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_77, /*hidden argument*/NULL);
		goto IL_0432;
	}

IL_026f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		int32_t L_78 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		if ((!(((uint32_t)L_78) == ((uint32_t)((int32_t)57)))))
		{
			goto IL_037d;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		int32_t L_79 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		if ((!(((uint32_t)L_79) == ((uint32_t)6))))
		{
			goto IL_02f0;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		int32_t L_80 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_81 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22 = ((int32_t)((int32_t)L_80-(int32_t)L_81));
		BoardManager_t1 * L_82 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___boardScript_2;
		NullCheck(L_82);
		List_1_t6 * L_83 = (L_82->___BluePath_8);
		int32_t L_84 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		NullCheck(L_83);
		Vector3_t35  L_85 = (Vector3_t35 )VirtFuncInvoker1< Vector3_t35 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_83, L_84);
		V_7 = L_85;
		Player_t11 * L_86 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerScript_3;
		float L_87 = ((&V_7)->___x_1);
		float L_88 = ((&V_7)->___y_2);
		int32_t L_89 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_90 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		NullCheck(L_86);
		Player_MoveThePlayer_m152(L_86, 0, 0, (((int32_t)L_87)), (((int32_t)L_88)), L_89, L_90, /*hidden argument*/NULL);
		int32_t L_91 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_92 = L_91;
		Object_t * L_93 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_92);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_94 = String_Concat_m214(NULL /*static, unused*/, _stringLiteral57, L_93, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_94, /*hidden argument*/NULL);
		goto IL_0378;
	}

IL_02f0:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		BoardManager_t1 * L_95 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___boardScript_2;
		NullCheck(L_95);
		List_1_t6 * L_96 = (L_95->___BluePath_8);
		int32_t L_97 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		NullCheck(L_96);
		Vector3_t35  L_98 = (Vector3_t35 )VirtFuncInvoker1< Vector3_t35 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_96, L_97);
		V_8 = L_98;
		Vector3_t35  L_99 = V_8;
		Vector3_t35  L_100 = V_5;
		Vector3_t35  L_101 = Vector3_op_Subtraction_m251(NULL /*static, unused*/, L_99, L_100, /*hidden argument*/NULL);
		V_9 = L_101;
		int32_t L_102 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_103 = L_102;
		Object_t * L_104 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_103);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_105 = String_Concat_m214(NULL /*static, unused*/, _stringLiteral58, L_104, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_105, /*hidden argument*/NULL);
		Player_t11 * L_106 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerScript_3;
		float L_107 = ((&V_9)->___x_1);
		float L_108 = ((&V_9)->___y_2);
		float L_109 = ((&V_8)->___x_1);
		float L_110 = ((&V_8)->___y_2);
		int32_t L_111 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_112 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		NullCheck(L_106);
		Player_MoveThePlayer_m152(L_106, (((int32_t)L_107)), (((int32_t)L_108)), (((int32_t)L_109)), (((int32_t)L_110)), L_111, L_112, /*hidden argument*/NULL);
		GameObject_t5 * L_113 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___turnText_19;
		NullCheck(L_113);
		Text_t47 * L_114 = GameObject_GetComponent_TisText_t47_m213(L_113, /*hidden argument*/GameObject_GetComponent_TisText_t47_m213_MethodInfo_var);
		NullCheck(L_114);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_114, _stringLiteral59);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___gameOverBool_8 = 1;
	}

IL_0378:
	{
		goto IL_0432;
	}

IL_037d:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		BoardManager_t1 * L_115 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___boardScript_2;
		NullCheck(L_115);
		List_1_t6 * L_116 = (L_115->___BluePath_8);
		int32_t L_117 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		NullCheck(L_116);
		Vector3_t35  L_118 = (Vector3_t35 )VirtFuncInvoker1< Vector3_t35 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_116, L_117);
		V_10 = L_118;
		Vector3_t35  L_119 = V_10;
		Vector3_t35  L_120 = V_5;
		Vector3_t35  L_121 = Vector3_op_Subtraction_m251(NULL /*static, unused*/, L_119, L_120, /*hidden argument*/NULL);
		V_11 = L_121;
		ObjectU5BU5D_t60* L_122 = ((ObjectU5BU5D_t60*)SZArrayNew(ObjectU5BU5D_t60_il2cpp_TypeInfo_var, 7));
		NullCheck(L_122);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_122, 0);
		ArrayElementTypeCheck (L_122, _stringLiteral60);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_122, 0, sizeof(Object_t *))) = (Object_t *)_stringLiteral60;
		ObjectU5BU5D_t60* L_123 = L_122;
		int32_t L_124 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		int32_t L_125 = L_124;
		Object_t * L_126 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_125);
		NullCheck(L_123);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_123, 1);
		ArrayElementTypeCheck (L_123, L_126);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_123, 1, sizeof(Object_t *))) = (Object_t *)L_126;
		ObjectU5BU5D_t60* L_127 = L_123;
		NullCheck(L_127);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_127, 2);
		ArrayElementTypeCheck (L_127, _stringLiteral23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_127, 2, sizeof(Object_t *))) = (Object_t *)_stringLiteral23;
		ObjectU5BU5D_t60* L_128 = L_127;
		int32_t L_129 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_130 = L_129;
		Object_t * L_131 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_130);
		NullCheck(L_128);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_128, 3);
		ArrayElementTypeCheck (L_128, L_131);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_128, 3, sizeof(Object_t *))) = (Object_t *)L_131;
		ObjectU5BU5D_t60* L_132 = L_128;
		NullCheck(L_132);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_132, 4);
		ArrayElementTypeCheck (L_132, _stringLiteral23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_132, 4, sizeof(Object_t *))) = (Object_t *)_stringLiteral23;
		ObjectU5BU5D_t60* L_133 = L_132;
		float L_134 = ((&V_11)->___x_1);
		float L_135 = L_134;
		Object_t * L_136 = Box(Single_t61_il2cpp_TypeInfo_var, &L_135);
		NullCheck(L_133);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_133, 5);
		ArrayElementTypeCheck (L_133, L_136);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_133, 5, sizeof(Object_t *))) = (Object_t *)L_136;
		ObjectU5BU5D_t60* L_137 = L_133;
		float L_138 = ((&V_11)->___y_2);
		float L_139 = L_138;
		Object_t * L_140 = Box(Single_t61_il2cpp_TypeInfo_var, &L_139);
		NullCheck(L_137);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_137, 6);
		ArrayElementTypeCheck (L_137, L_140);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_137, 6, sizeof(Object_t *))) = (Object_t *)L_140;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_141 = String_Concat_m252(NULL /*static, unused*/, L_137, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_141, /*hidden argument*/NULL);
		Player_t11 * L_142 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerScript_3;
		float L_143 = ((&V_11)->___x_1);
		float L_144 = ((&V_11)->___y_2);
		float L_145 = ((&V_10)->___x_1);
		float L_146 = ((&V_10)->___y_2);
		int32_t L_147 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_148 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		NullCheck(L_142);
		Player_MoveThePlayer_m152(L_142, (((int32_t)L_143)), (((int32_t)L_144)), (((int32_t)L_145)), (((int32_t)L_146)), L_147, L_148, /*hidden argument*/NULL);
	}

IL_0432:
	{
		goto IL_07a9;
	}

IL_0437:
	{
		String_t* L_149 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_150 = String_op_Equality_m204(NULL /*static, unused*/, L_149, _stringLiteral5, /*hidden argument*/NULL);
		if (!L_150)
		{
			goto IL_07a9;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		int32_t L_151 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		if (L_151)
		{
			goto IL_0552;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		BoardManager_t1 * L_152 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___boardScript_2;
		NullCheck(L_152);
		List_1_t6 * L_153 = (L_152->___RedPath_9);
		int32_t L_154 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		NullCheck(L_153);
		Vector3_t35  L_155 = (Vector3_t35 )VirtFuncInvoker1< Vector3_t35 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_153, L_154);
		V_12 = L_155;
		int32_t L_156 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		if ((!(((uint32_t)L_156) == ((uint32_t)6))))
		{
			goto IL_04f2;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21 = 1;
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22 = 1;
		BoardManager_t1 * L_157 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___boardScript_2;
		NullCheck(L_157);
		List_1_t6 * L_158 = (L_157->___RedPath_9);
		NullCheck(L_158);
		Vector3_t35  L_159 = (Vector3_t35 )VirtFuncInvoker1< Vector3_t35 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_158, 1);
		V_13 = L_159;
		Vector3_t35  L_160 = V_13;
		Vector3_t35  L_161 = V_12;
		Vector3_t35  L_162 = Vector3_op_Subtraction_m251(NULL /*static, unused*/, L_160, L_161, /*hidden argument*/NULL);
		V_14 = L_162;
		int32_t L_163 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_164 = L_163;
		Object_t * L_165 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_164);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_166 = String_Concat_m214(NULL /*static, unused*/, _stringLiteral55, L_165, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_166, /*hidden argument*/NULL);
		PlayerTwo_t12 * L_167 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerTwoScript_4;
		float L_168 = ((&V_14)->___x_1);
		float L_169 = ((&V_14)->___y_2);
		float L_170 = ((&V_13)->___x_1);
		float L_171 = ((&V_13)->___y_2);
		int32_t L_172 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		NullCheck(L_167);
		PlayerTwo_MoveThePlayer_m173(L_167, (((int32_t)L_168)), (((int32_t)L_169)), (((int32_t)L_170)), (((int32_t)L_171)), L_172, 6, /*hidden argument*/NULL);
		goto IL_054d;
	}

IL_04f2:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22 = 0;
		BoardManager_t1 * L_173 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___boardScript_2;
		NullCheck(L_173);
		List_1_t6 * L_174 = (L_173->___RedPath_9);
		int32_t L_175 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		NullCheck(L_174);
		Vector3_t35  L_176 = (Vector3_t35 )VirtFuncInvoker1< Vector3_t35 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_174, L_175);
		V_15 = L_176;
		PlayerTwo_t12 * L_177 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerTwoScript_4;
		float L_178 = ((&V_15)->___x_1);
		float L_179 = ((&V_15)->___y_2);
		int32_t L_180 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_181 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		NullCheck(L_177);
		PlayerTwo_MoveThePlayer_m173(L_177, 0, 0, (((int32_t)L_178)), (((int32_t)L_179)), L_180, L_181, /*hidden argument*/NULL);
		int32_t L_182 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_183 = L_182;
		Object_t * L_184 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_183);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_185 = String_Concat_m214(NULL /*static, unused*/, _stringLiteral56, L_184, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_185, /*hidden argument*/NULL);
	}

IL_054d:
	{
		goto IL_07a9;
	}

IL_0552:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		BoardManager_t1 * L_186 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___boardScript_2;
		NullCheck(L_186);
		List_1_t6 * L_187 = (L_186->___RedPath_9);
		int32_t L_188 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		NullCheck(L_187);
		Vector3_t35  L_189 = (Vector3_t35 )VirtFuncInvoker1< Vector3_t35 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_187, L_188);
		V_16 = L_189;
		int32_t L_190 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_191 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22 = ((int32_t)((int32_t)L_190+(int32_t)L_191));
		int32_t L_192 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		if ((((int32_t)L_192) <= ((int32_t)((int32_t)57))))
		{
			goto IL_05ee;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		int32_t L_193 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_194 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22 = ((int32_t)((int32_t)L_193-(int32_t)L_194));
		BoardManager_t1 * L_195 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___boardScript_2;
		NullCheck(L_195);
		List_1_t6 * L_196 = (L_195->___RedPath_9);
		int32_t L_197 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		NullCheck(L_196);
		Vector3_t35  L_198 = (Vector3_t35 )VirtFuncInvoker1< Vector3_t35 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_196, L_197);
		V_17 = L_198;
		PlayerTwo_t12 * L_199 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerTwoScript_4;
		float L_200 = ((&V_17)->___x_1);
		float L_201 = ((&V_17)->___y_2);
		int32_t L_202 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_203 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		NullCheck(L_199);
		PlayerTwo_MoveThePlayer_m173(L_199, 0, 0, (((int32_t)L_200)), (((int32_t)L_201)), L_202, L_203, /*hidden argument*/NULL);
		int32_t L_204 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_205 = L_204;
		Object_t * L_206 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_205);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_207 = String_Concat_m214(NULL /*static, unused*/, _stringLiteral57, L_206, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_207, /*hidden argument*/NULL);
		goto IL_07a9;
	}

IL_05ee:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		int32_t L_208 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		if ((!(((uint32_t)L_208) == ((uint32_t)((int32_t)57)))))
		{
			goto IL_06fc;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		int32_t L_209 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		if ((!(((uint32_t)L_209) == ((uint32_t)6))))
		{
			goto IL_066f;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		int32_t L_210 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_211 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22 = ((int32_t)((int32_t)L_210-(int32_t)L_211));
		BoardManager_t1 * L_212 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___boardScript_2;
		NullCheck(L_212);
		List_1_t6 * L_213 = (L_212->___RedPath_9);
		int32_t L_214 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		NullCheck(L_213);
		Vector3_t35  L_215 = (Vector3_t35 )VirtFuncInvoker1< Vector3_t35 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_213, L_214);
		V_18 = L_215;
		PlayerTwo_t12 * L_216 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerTwoScript_4;
		float L_217 = ((&V_18)->___x_1);
		float L_218 = ((&V_18)->___y_2);
		int32_t L_219 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_220 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		NullCheck(L_216);
		PlayerTwo_MoveThePlayer_m173(L_216, 0, 0, (((int32_t)L_217)), (((int32_t)L_218)), L_219, L_220, /*hidden argument*/NULL);
		int32_t L_221 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_222 = L_221;
		Object_t * L_223 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_222);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_224 = String_Concat_m214(NULL /*static, unused*/, _stringLiteral57, L_223, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_224, /*hidden argument*/NULL);
		goto IL_06f7;
	}

IL_066f:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		BoardManager_t1 * L_225 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___boardScript_2;
		NullCheck(L_225);
		List_1_t6 * L_226 = (L_225->___RedPath_9);
		int32_t L_227 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		NullCheck(L_226);
		Vector3_t35  L_228 = (Vector3_t35 )VirtFuncInvoker1< Vector3_t35 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_226, L_227);
		V_19 = L_228;
		Vector3_t35  L_229 = V_19;
		Vector3_t35  L_230 = V_16;
		Vector3_t35  L_231 = Vector3_op_Subtraction_m251(NULL /*static, unused*/, L_229, L_230, /*hidden argument*/NULL);
		V_20 = L_231;
		int32_t L_232 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_233 = L_232;
		Object_t * L_234 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_233);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_235 = String_Concat_m214(NULL /*static, unused*/, _stringLiteral58, L_234, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_235, /*hidden argument*/NULL);
		PlayerTwo_t12 * L_236 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerTwoScript_4;
		float L_237 = ((&V_20)->___x_1);
		float L_238 = ((&V_20)->___y_2);
		float L_239 = ((&V_19)->___x_1);
		float L_240 = ((&V_19)->___y_2);
		int32_t L_241 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_242 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		NullCheck(L_236);
		PlayerTwo_MoveThePlayer_m173(L_236, (((int32_t)L_237)), (((int32_t)L_238)), (((int32_t)L_239)), (((int32_t)L_240)), L_241, L_242, /*hidden argument*/NULL);
		GameObject_t5 * L_243 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___turnText_19;
		NullCheck(L_243);
		Text_t47 * L_244 = GameObject_GetComponent_TisText_t47_m213(L_243, /*hidden argument*/GameObject_GetComponent_TisText_t47_m213_MethodInfo_var);
		NullCheck(L_244);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_244, _stringLiteral59);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___gameOverBool_8 = 1;
	}

IL_06f7:
	{
		goto IL_07a9;
	}

IL_06fc:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		BoardManager_t1 * L_245 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___boardScript_2;
		NullCheck(L_245);
		List_1_t6 * L_246 = (L_245->___RedPath_9);
		int32_t L_247 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		NullCheck(L_246);
		Vector3_t35  L_248 = (Vector3_t35 )VirtFuncInvoker1< Vector3_t35 , int32_t >::Invoke(31 /* !0 System.Collections.Generic.List`1<UnityEngine.Vector3>::get_Item(System.Int32) */, L_246, L_247);
		V_21 = L_248;
		Vector3_t35  L_249 = V_21;
		Vector3_t35  L_250 = V_16;
		Vector3_t35  L_251 = Vector3_op_Subtraction_m251(NULL /*static, unused*/, L_249, L_250, /*hidden argument*/NULL);
		V_22 = L_251;
		ObjectU5BU5D_t60* L_252 = ((ObjectU5BU5D_t60*)SZArrayNew(ObjectU5BU5D_t60_il2cpp_TypeInfo_var, 6));
		int32_t L_253 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		int32_t L_254 = L_253;
		Object_t * L_255 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_254);
		NullCheck(L_252);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_252, 0);
		ArrayElementTypeCheck (L_252, L_255);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_252, 0, sizeof(Object_t *))) = (Object_t *)L_255;
		ObjectU5BU5D_t60* L_256 = L_252;
		NullCheck(L_256);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_256, 1);
		ArrayElementTypeCheck (L_256, _stringLiteral23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_256, 1, sizeof(Object_t *))) = (Object_t *)_stringLiteral23;
		ObjectU5BU5D_t60* L_257 = L_256;
		int32_t L_258 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_259 = L_258;
		Object_t * L_260 = Box(Int32_t56_il2cpp_TypeInfo_var, &L_259);
		NullCheck(L_257);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_257, 2);
		ArrayElementTypeCheck (L_257, L_260);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_257, 2, sizeof(Object_t *))) = (Object_t *)L_260;
		ObjectU5BU5D_t60* L_261 = L_257;
		NullCheck(L_261);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_261, 3);
		ArrayElementTypeCheck (L_261, _stringLiteral23);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_261, 3, sizeof(Object_t *))) = (Object_t *)_stringLiteral23;
		ObjectU5BU5D_t60* L_262 = L_261;
		float L_263 = ((&V_22)->___x_1);
		float L_264 = L_263;
		Object_t * L_265 = Box(Single_t61_il2cpp_TypeInfo_var, &L_264);
		NullCheck(L_262);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_262, 4);
		ArrayElementTypeCheck (L_262, L_265);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_262, 4, sizeof(Object_t *))) = (Object_t *)L_265;
		ObjectU5BU5D_t60* L_266 = L_262;
		float L_267 = ((&V_22)->___y_2);
		float L_268 = L_267;
		Object_t * L_269 = Box(Single_t61_il2cpp_TypeInfo_var, &L_268);
		NullCheck(L_266);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_266, 5);
		ArrayElementTypeCheck (L_266, L_269);
		*((Object_t **)(Object_t **)SZArrayLdElema(L_266, 5, sizeof(Object_t *))) = (Object_t *)L_269;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_270 = String_Concat_m252(NULL /*static, unused*/, L_266, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_270, /*hidden argument*/NULL);
		PlayerTwo_t12 * L_271 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerTwoScript_4;
		float L_272 = ((&V_22)->___x_1);
		float L_273 = ((&V_22)->___y_2);
		float L_274 = ((&V_21)->___x_1);
		float L_275 = ((&V_21)->___y_2);
		int32_t L_276 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		int32_t L_277 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		NullCheck(L_271);
		PlayerTwo_MoveThePlayer_m173(L_271, (((int32_t)L_272)), (((int32_t)L_273)), (((int32_t)L_274)), (((int32_t)L_275)), L_276, L_277, /*hidden argument*/NULL);
	}

IL_07a9:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		int32_t L_278 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22;
		PlayerPrefs_SetInt_m246(NULL /*static, unused*/, _stringLiteral38, L_278, /*hidden argument*/NULL);
		CountdownTimer_t10 * L_279 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___countdownTimerScript_5;
		NullCheck(L_279);
		CountdownTimer_ShiftTimer_m16(L_279, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::Move_Dice()
extern TypeInfo* GameManager_t22_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t5_il2cpp_TypeInfo_var;
extern TypeInfo* CountdownTimer_t10_il2cpp_TypeInfo_var;
extern "C" void GameManager_Move_Dice_m78 (GameManager_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameManager_t22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		GameObject_t5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		CountdownTimer_t10_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t5 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		GameObject_t5 * L_0 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstance_15;
		Object_Destroy_m227(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_1 = Random_Range_m195(NULL /*static, unused*/, 1, 7, /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21 = L_1;
		GameObjectU5BU5D_t4* L_2 = (__this->___dices_14);
		int32_t L_3 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, ((int32_t)((int32_t)L_3-(int32_t)1)));
		int32_t L_4 = ((int32_t)((int32_t)L_3-(int32_t)1));
		V_0 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_2, L_4, sizeof(GameObject_t5 *)));
		GameObject_t5 * L_5 = V_0;
		Vector3_t35  L_6 = {0};
		Vector3__ctor_m192(&L_6, (21.0f), (15.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t53  L_7 = Quaternion_get_identity_m196(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t54 * L_8 = Object_Instantiate_m197(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstance_15 = ((GameObject_t5 *)IsInstSealed(L_8, GameObject_t5_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(CountdownTimer_t10_il2cpp_TypeInfo_var);
		((CountdownTimer_t10_StaticFields*)CountdownTimer_t10_il2cpp_TypeInfo_var->static_fields)->___showTimer_5 = 1;
		return;
	}
}
// System.Void GameManager::Move_OpponentsDice()
extern TypeInfo* GameManager_t22_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t5_il2cpp_TypeInfo_var;
extern TypeInfo* CountdownTimer_t10_il2cpp_TypeInfo_var;
extern "C" void GameManager_Move_OpponentsDice_m79 (GameManager_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameManager_t22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		GameObject_t5_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		CountdownTimer_t10_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		s_Il2CppMethodIntialized = true;
	}
	GameObject_t5 * V_0 = {0};
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		GameObject_t5 * L_0 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstanceB_16;
		Object_Destroy_m227(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		int32_t L_1 = Random_Range_m195(NULL /*static, unused*/, 1, 7, /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21 = L_1;
		GameObjectU5BU5D_t4* L_2 = (__this->___dices_14);
		int32_t L_3 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceNumber_21;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, ((int32_t)((int32_t)L_3-(int32_t)1)));
		int32_t L_4 = ((int32_t)((int32_t)L_3-(int32_t)1));
		V_0 = (*(GameObject_t5 **)(GameObject_t5 **)SZArrayLdElema(L_2, L_4, sizeof(GameObject_t5 *)));
		GameObject_t5 * L_5 = V_0;
		Vector3_t35  L_6 = {0};
		Vector3__ctor_m192(&L_6, (-10.0f), (15.0f), (0.0f), /*hidden argument*/NULL);
		Quaternion_t53  L_7 = Quaternion_get_identity_m196(NULL /*static, unused*/, /*hidden argument*/NULL);
		Object_t54 * L_8 = Object_Instantiate_m197(NULL /*static, unused*/, L_5, L_6, L_7, /*hidden argument*/NULL);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___diceInstanceB_16 = ((GameObject_t5 *)IsInstSealed(L_8, GameObject_t5_il2cpp_TypeInfo_var));
		IL2CPP_RUNTIME_CLASS_INIT(CountdownTimer_t10_il2cpp_TypeInfo_var);
		((CountdownTimer_t10_StaticFields*)CountdownTimer_t10_il2cpp_TypeInfo_var->static_fields)->___showTimer_5 = 1;
		return;
	}
}
// System.Void GameManager::Stop_Dice()
extern Il2CppCodeGenString* _stringLiteral35;
extern "C" void GameManager_Stop_Dice_m80 (GameManager_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral35 = il2cpp_codegen_string_literal_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_CancelInvoke_m253(__this, _stringLiteral35, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::Stop_OpponentsDice()
extern Il2CppCodeGenString* _stringLiteral42;
extern "C" void GameManager_Stop_OpponentsDice_m81 (GameManager_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral42 = il2cpp_codegen_string_literal_from_index(42);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_CancelInvoke_m253(__this, _stringLiteral42, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameManager::Update()
extern TypeInfo* GameManager_t22_il2cpp_TypeInfo_var;
extern const MethodInfo* GameObject_GetComponent_TisText_t47_m213_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral40;
extern Il2CppCodeGenString* _stringLiteral41;
extern "C" void GameManager_Update_m82 (GameManager_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameManager_t22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		GameObject_GetComponent_TisText_t47_m213_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		_stringLiteral40 = il2cpp_codegen_string_literal_from_index(40);
		_stringLiteral41 = il2cpp_codegen_string_literal_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		bool L_0 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playersTurn_6;
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		GameManager_Stop_OpponentsDice_m81(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		bool L_1 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___ShowButtonBool_9;
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		GameObject_t5 * L_2 = (__this->___rollDiceButton_18);
		NullCheck(L_2);
		GameObject_SetActive_m238(L_2, 1, /*hidden argument*/NULL);
	}

IL_0026:
	{
		goto IL_007f;
	}

IL_002b:
	{
		GameManager_Stop_Dice_m80(__this, /*hidden argument*/NULL);
		GameObject_t5 * L_3 = (__this->___rollDiceButton_18);
		NullCheck(L_3);
		GameObject_SetActive_m238(L_3, 0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		bool L_4 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___gameOverBool_8;
		if (!L_4)
		{
			goto IL_006b;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		GameObject_t5 * L_5 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___turnText_19;
		NullCheck(L_5);
		Text_t47 * L_6 = GameObject_GetComponent_TisText_t47_m213(L_5, /*hidden argument*/GameObject_GetComponent_TisText_t47_m213_MethodInfo_var);
		NullCheck(L_6);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_6, _stringLiteral40);
		GameObject_t5 * L_7 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___resetButton_17;
		NullCheck(L_7);
		GameObject_SetActive_m238(L_7, 1, /*hidden argument*/NULL);
		goto IL_007f;
	}

IL_006b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		GameObject_t5 * L_8 = ((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___turnText_19;
		NullCheck(L_8);
		Text_t47 * L_9 = GameObject_GetComponent_TisText_t47_m213(L_8, /*hidden argument*/GameObject_GetComponent_TisText_t47_m213_MethodInfo_var);
		NullCheck(L_9);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_9, _stringLiteral41);
	}

IL_007f:
	{
		return;
	}
}
// System.Void GameManager::ResetButtonPressed()
extern Il2CppCodeGenString* _stringLiteral61;
extern "C" void GameManager_ResetButtonPressed_m83 (GameManager_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral61 = il2cpp_codegen_string_literal_from_index(61);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_StartCoroutine_m223(__this, _stringLiteral61, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator GameManager::ResetBoard()
extern TypeInfo* U3CResetBoardU3Ec__Iterator3_t20_il2cpp_TypeInfo_var;
extern "C" Object_t * GameManager_ResetBoard_m84 (GameManager_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CResetBoardU3Ec__Iterator3_t20_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		s_Il2CppMethodIntialized = true;
	}
	U3CResetBoardU3Ec__Iterator3_t20 * V_0 = {0};
	{
		U3CResetBoardU3Ec__Iterator3_t20 * L_0 = (U3CResetBoardU3Ec__Iterator3_t20 *)il2cpp_codegen_object_new (U3CResetBoardU3Ec__Iterator3_t20_il2cpp_TypeInfo_var);
		U3CResetBoardU3Ec__Iterator3__ctor_m40(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CResetBoardU3Ec__Iterator3_t20 * L_1 = V_0;
		return L_1;
	}
}
// System.Void GameManager::CallGetPlayerTurn()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral62;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral4;
extern Il2CppCodeGenString* _stringLiteral63;
extern Il2CppCodeGenString* _stringLiteral5;
extern Il2CppCodeGenString* _stringLiteral64;
extern "C" void GameManager_CallGetPlayerTurn_m85 (GameManager_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral62 = il2cpp_codegen_string_literal_from_index(62);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral4 = il2cpp_codegen_string_literal_from_index(4);
		_stringLiteral63 = il2cpp_codegen_string_literal_from_index(63);
		_stringLiteral5 = il2cpp_codegen_string_literal_from_index(5);
		_stringLiteral64 = il2cpp_codegen_string_literal_from_index(64);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m202(NULL /*static, unused*/, _stringLiteral62, /*hidden argument*/NULL);
		String_t* L_0 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_1 = String_op_Equality_m204(NULL /*static, unused*/, L_0, _stringLiteral4, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0034;
		}
	}
	{
		MonoBehaviour_StartCoroutine_m223(__this, _stringLiteral63, /*hidden argument*/NULL);
		goto IL_0059;
	}

IL_0034:
	{
		String_t* L_2 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral3, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_3 = String_op_Equality_m204(NULL /*static, unused*/, L_2, _stringLiteral5, /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_0059;
		}
	}
	{
		MonoBehaviour_StartCoroutine_m223(__this, _stringLiteral64, /*hidden argument*/NULL);
	}

IL_0059:
	{
		return;
	}
}
// System.Collections.IEnumerator GameManager::GetPlayer1Turn()
extern TypeInfo* U3CGetPlayer1TurnU3Ec__Iterator4_t21_il2cpp_TypeInfo_var;
extern "C" Object_t * GameManager_GetPlayer1Turn_m86 (GameManager_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGetPlayer1TurnU3Ec__Iterator4_t21_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(25);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetPlayer1TurnU3Ec__Iterator4_t21 * V_0 = {0};
	{
		U3CGetPlayer1TurnU3Ec__Iterator4_t21 * L_0 = (U3CGetPlayer1TurnU3Ec__Iterator4_t21 *)il2cpp_codegen_object_new (U3CGetPlayer1TurnU3Ec__Iterator4_t21_il2cpp_TypeInfo_var);
		U3CGetPlayer1TurnU3Ec__Iterator4__ctor_m46(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetPlayer1TurnU3Ec__Iterator4_t21 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_23 = __this;
		U3CGetPlayer1TurnU3Ec__Iterator4_t21 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator GameManager::GetPlayer2Turn()
extern TypeInfo* U3CGetPlayer2TurnU3Ec__Iterator5_t23_il2cpp_TypeInfo_var;
extern "C" Object_t * GameManager_GetPlayer2Turn_m87 (GameManager_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGetPlayer2TurnU3Ec__Iterator5_t23_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetPlayer2TurnU3Ec__Iterator5_t23 * V_0 = {0};
	{
		U3CGetPlayer2TurnU3Ec__Iterator5_t23 * L_0 = (U3CGetPlayer2TurnU3Ec__Iterator5_t23 *)il2cpp_codegen_object_new (U3CGetPlayer2TurnU3Ec__Iterator5_t23_il2cpp_TypeInfo_var);
		U3CGetPlayer2TurnU3Ec__Iterator5__ctor_m52(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetPlayer2TurnU3Ec__Iterator5_t23 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_22 = __this;
		U3CGetPlayer2TurnU3Ec__Iterator5_t23 * L_2 = V_0;
		return L_2;
	}
}
// System.Collections.IEnumerator GameManager::AutomaticMovePlayer1()
extern TypeInfo* U3CAutomaticMovePlayer1U3Ec__Iterator6_t24_il2cpp_TypeInfo_var;
extern "C" Object_t * GameManager_AutomaticMovePlayer1_m88 (GameManager_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CAutomaticMovePlayer1U3Ec__Iterator6_t24_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(27);
		s_Il2CppMethodIntialized = true;
	}
	U3CAutomaticMovePlayer1U3Ec__Iterator6_t24 * V_0 = {0};
	{
		U3CAutomaticMovePlayer1U3Ec__Iterator6_t24 * L_0 = (U3CAutomaticMovePlayer1U3Ec__Iterator6_t24 *)il2cpp_codegen_object_new (U3CAutomaticMovePlayer1U3Ec__Iterator6_t24_il2cpp_TypeInfo_var);
		U3CAutomaticMovePlayer1U3Ec__Iterator6__ctor_m58(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAutomaticMovePlayer1U3Ec__Iterator6_t24 * L_1 = V_0;
		return L_1;
	}
}
// System.Collections.IEnumerator GameManager::AutomaticMovePlayer2()
extern TypeInfo* U3CAutomaticMovePlayer2U3Ec__Iterator7_t25_il2cpp_TypeInfo_var;
extern "C" Object_t * GameManager_AutomaticMovePlayer2_m89 (GameManager_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CAutomaticMovePlayer2U3Ec__Iterator7_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(28);
		s_Il2CppMethodIntialized = true;
	}
	U3CAutomaticMovePlayer2U3Ec__Iterator7_t25 * V_0 = {0};
	{
		U3CAutomaticMovePlayer2U3Ec__Iterator7_t25 * L_0 = (U3CAutomaticMovePlayer2U3Ec__Iterator7_t25 *)il2cpp_codegen_object_new (U3CAutomaticMovePlayer2U3Ec__Iterator7_t25_il2cpp_TypeInfo_var);
		U3CAutomaticMovePlayer2U3Ec__Iterator7__ctor_m64(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAutomaticMovePlayer2U3Ec__Iterator7_t25 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_5 = __this;
		U3CAutomaticMovePlayer2U3Ec__Iterator7_t25 * L_2 = V_0;
		return L_2;
	}
}
// System.Void GameMenu/<SetUserOnline>c__Iterator8::.ctor()
extern "C" void U3CSetUserOnlineU3Ec__Iterator8__ctor_m90 (U3CSetUserOnlineU3Ec__Iterator8_t27 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object GameMenu/<SetUserOnline>c__Iterator8::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CSetUserOnlineU3Ec__Iterator8_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m91 (U3CSetUserOnlineU3Ec__Iterator8_t27 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_4);
		return L_0;
	}
}
// System.Object GameMenu/<SetUserOnline>c__Iterator8::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CSetUserOnlineU3Ec__Iterator8_System_Collections_IEnumerator_get_Current_m92 (U3CSetUserOnlineU3Ec__Iterator8_t27 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_4);
		return L_0;
	}
}
// System.Boolean GameMenu/<SetUserOnline>c__Iterator8::MoveNext()
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral11;
extern Il2CppCodeGenString* _stringLiteral12;
extern Il2CppCodeGenString* _stringLiteral65;
extern "C" bool U3CSetUserOnlineU3Ec__Iterator8_MoveNext_m93 (U3CSetUserOnlineU3Ec__Iterator8_t27 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		_stringLiteral12 = il2cpp_codegen_string_literal_from_index(12);
		_stringLiteral65 = il2cpp_codegen_string_literal_from_index(65);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_3);
		V_0 = L_0;
		__this->___U24PC_3 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0080;
		}
	}
	{
		goto IL_00ac;
	}

IL_0021:
	{
		WWWForm_t8 * L_2 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_2, /*hidden argument*/NULL);
		__this->___U3CformU3E__0_0 = L_2;
		String_t* L_3 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral11, /*hidden argument*/NULL);
		__this->___U3CusernameU3E__1_1 = L_3;
		WWWForm_t8 * L_4 = (__this->___U3CformU3E__0_0);
		String_t* L_5 = (__this->___U3CusernameU3E__1_1);
		NullCheck(L_4);
		WWWForm_AddField_m203(L_4, _stringLiteral12, L_5, /*hidden argument*/NULL);
		WWWForm_t8 * L_6 = (__this->___U3CformU3E__0_0);
		WWW_t9 * L_7 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_7, _stringLiteral65, L_6, /*hidden argument*/NULL);
		__this->___U3CwU3E__2_2 = L_7;
		WWW_t9 * L_8 = (__this->___U3CwU3E__2_2);
		__this->___U24current_4 = L_8;
		__this->___U24PC_3 = 1;
		goto IL_00ae;
	}

IL_0080:
	{
		WWW_t9 * L_9 = (__this->___U3CwU3E__2_2);
		NullCheck(L_9);
		String_t* L_10 = WWW_get_error_m218(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_0095;
		}
	}
	{
		goto IL_00a5;
	}

IL_0095:
	{
		WWW_t9 * L_11 = (__this->___U3CwU3E__2_2);
		NullCheck(L_11);
		String_t* L_12 = WWW_get_error_m218(L_11, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
	}

IL_00a5:
	{
		__this->___U24PC_3 = (-1);
	}

IL_00ac:
	{
		return 0;
	}

IL_00ae:
	{
		return 1;
	}
	// Dead block : IL_00b0: ldloc.1
}
// System.Void GameMenu/<SetUserOnline>c__Iterator8::Dispose()
extern "C" void U3CSetUserOnlineU3Ec__Iterator8_Dispose_m94 (U3CSetUserOnlineU3Ec__Iterator8_t27 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_3 = (-1);
		return;
	}
}
// System.Void GameMenu/<SetUserOnline>c__Iterator8::Reset()
extern TypeInfo* NotSupportedException_t55_il2cpp_TypeInfo_var;
extern "C" void U3CSetUserOnlineU3Ec__Iterator8_Reset_m95 (U3CSetUserOnlineU3Ec__Iterator8_t27 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t55 * L_0 = (NotSupportedException_t55 *)il2cpp_codegen_object_new (NotSupportedException_t55_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m208(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void GameMenu/<GetActiveUsers>c__Iterator9::.ctor()
extern "C" void U3CGetActiveUsersU3Ec__Iterator9__ctor_m96 (U3CGetActiveUsersU3Ec__Iterator9_t28 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object GameMenu/<GetActiveUsers>c__Iterator9::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CGetActiveUsersU3Ec__Iterator9_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m97 (U3CGetActiveUsersU3Ec__Iterator9_t28 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_5);
		return L_0;
	}
}
// System.Object GameMenu/<GetActiveUsers>c__Iterator9::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetActiveUsersU3Ec__Iterator9_System_Collections_IEnumerator_get_Current_m98 (U3CGetActiveUsersU3Ec__Iterator9_t28 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_5);
		return L_0;
	}
}
// System.Boolean GameMenu/<GetActiveUsers>c__Iterator9::MoveNext()
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral11;
extern Il2CppCodeGenString* _stringLiteral12;
extern Il2CppCodeGenString* _stringLiteral66;
extern Il2CppCodeGenString* _stringLiteral67;
extern "C" bool U3CGetActiveUsersU3Ec__Iterator9_MoveNext_m99 (U3CGetActiveUsersU3Ec__Iterator9_t28 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		_stringLiteral12 = il2cpp_codegen_string_literal_from_index(12);
		_stringLiteral66 = il2cpp_codegen_string_literal_from_index(66);
		_stringLiteral67 = il2cpp_codegen_string_literal_from_index(67);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_4);
		V_0 = L_0;
		__this->___U24PC_4 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0080;
		}
	}
	{
		goto IL_00d2;
	}

IL_0021:
	{
		WWWForm_t8 * L_2 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_2, /*hidden argument*/NULL);
		__this->___U3CformU3E__0_0 = L_2;
		String_t* L_3 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral11, /*hidden argument*/NULL);
		__this->___U3CusernameU3E__1_1 = L_3;
		WWWForm_t8 * L_4 = (__this->___U3CformU3E__0_0);
		String_t* L_5 = (__this->___U3CusernameU3E__1_1);
		NullCheck(L_4);
		WWWForm_AddField_m203(L_4, _stringLiteral12, L_5, /*hidden argument*/NULL);
		WWWForm_t8 * L_6 = (__this->___U3CformU3E__0_0);
		WWW_t9 * L_7 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_7, _stringLiteral66, L_6, /*hidden argument*/NULL);
		__this->___U3CwU3E__2_2 = L_7;
		WWW_t9 * L_8 = (__this->___U3CwU3E__2_2);
		__this->___U24current_5 = L_8;
		__this->___U24PC_4 = 1;
		goto IL_00d4;
	}

IL_0080:
	{
		WWW_t9 * L_9 = (__this->___U3CwU3E__2_2);
		NullCheck(L_9);
		String_t* L_10 = WWW_get_error_m218(L_9, /*hidden argument*/NULL);
		if (L_10)
		{
			goto IL_00bb;
		}
	}
	{
		WWW_t9 * L_11 = (__this->___U3CwU3E__2_2);
		NullCheck(L_11);
		String_t* L_12 = WWW_get_text_m206(L_11, /*hidden argument*/NULL);
		__this->___U3CPlayersU3E__3_3 = L_12;
		String_t* L_13 = (__this->___U3CPlayersU3E__3_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m217(NULL /*static, unused*/, _stringLiteral67, L_13, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		goto IL_00cb;
	}

IL_00bb:
	{
		WWW_t9 * L_15 = (__this->___U3CwU3E__2_2);
		NullCheck(L_15);
		String_t* L_16 = WWW_get_error_m218(L_15, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
	}

IL_00cb:
	{
		__this->___U24PC_4 = (-1);
	}

IL_00d2:
	{
		return 0;
	}

IL_00d4:
	{
		return 1;
	}
	// Dead block : IL_00d6: ldloc.1
}
// System.Void GameMenu/<GetActiveUsers>c__Iterator9::Dispose()
extern "C" void U3CGetActiveUsersU3Ec__Iterator9_Dispose_m100 (U3CGetActiveUsersU3Ec__Iterator9_t28 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_4 = (-1);
		return;
	}
}
// System.Void GameMenu/<GetActiveUsers>c__Iterator9::Reset()
extern TypeInfo* NotSupportedException_t55_il2cpp_TypeInfo_var;
extern "C" void U3CGetActiveUsersU3Ec__Iterator9_Reset_m101 (U3CGetActiveUsersU3Ec__Iterator9_t28 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t55 * L_0 = (NotSupportedException_t55 *)il2cpp_codegen_object_new (NotSupportedException_t55_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m208(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void GameMenu::.ctor()
extern "C" void GameMenu__ctor_m102 (GameMenu_t29 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameMenu::Start()
extern Il2CppCodeGenString* _stringLiteral68;
extern "C" void GameMenu_Start_m103 (GameMenu_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral68 = il2cpp_codegen_string_literal_from_index(68);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_InvokeRepeating_m242(__this, _stringLiteral68, (1.0f), (10.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void GameMenu::CallSetActiveUsers()
extern Il2CppCodeGenString* _stringLiteral69;
extern "C" void GameMenu_CallSetActiveUsers_m104 (GameMenu_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral69 = il2cpp_codegen_string_literal_from_index(69);
		s_Il2CppMethodIntialized = true;
	}
	{
		MonoBehaviour_StartCoroutine_m223(__this, _stringLiteral69, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator GameMenu::SetUserOnline()
extern TypeInfo* U3CSetUserOnlineU3Ec__Iterator8_t27_il2cpp_TypeInfo_var;
extern "C" Object_t * GameMenu_SetUserOnline_m105 (GameMenu_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CSetUserOnlineU3Ec__Iterator8_t27_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(29);
		s_Il2CppMethodIntialized = true;
	}
	U3CSetUserOnlineU3Ec__Iterator8_t27 * V_0 = {0};
	{
		U3CSetUserOnlineU3Ec__Iterator8_t27 * L_0 = (U3CSetUserOnlineU3Ec__Iterator8_t27 *)il2cpp_codegen_object_new (U3CSetUserOnlineU3Ec__Iterator8_t27_il2cpp_TypeInfo_var);
		U3CSetUserOnlineU3Ec__Iterator8__ctor_m90(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSetUserOnlineU3Ec__Iterator8_t27 * L_1 = V_0;
		return L_1;
	}
}
// System.Collections.IEnumerator GameMenu::GetActiveUsers()
extern TypeInfo* U3CGetActiveUsersU3Ec__Iterator9_t28_il2cpp_TypeInfo_var;
extern "C" Object_t * GameMenu_GetActiveUsers_m106 (GameMenu_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGetActiveUsersU3Ec__Iterator9_t28_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetActiveUsersU3Ec__Iterator9_t28 * V_0 = {0};
	{
		U3CGetActiveUsersU3Ec__Iterator9_t28 * L_0 = (U3CGetActiveUsersU3Ec__Iterator9_t28 *)il2cpp_codegen_object_new (U3CGetActiveUsersU3Ec__Iterator9_t28_il2cpp_TypeInfo_var);
		U3CGetActiveUsersU3Ec__Iterator9__ctor_m96(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetActiveUsersU3Ec__Iterator9_t28 * L_1 = V_0;
		return L_1;
	}
}
// System.Void GameMenu::Refresh()
extern Il2CppCodeGenString* _stringLiteral17;
extern "C" void GameMenu_Refresh_m107 (GameMenu_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral17 = il2cpp_codegen_string_literal_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	{
		Application_LoadLevel_m241(NULL /*static, unused*/, _stringLiteral17, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Loader::.ctor()
extern "C" void Loader__ctor_m108 (Loader_t30 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Loader::Awake()
extern "C" void Loader_Awake_m109 (Loader_t30 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Menu/<CreateUniquePlayer>c__IteratorA::.ctor()
extern "C" void U3CCreateUniquePlayerU3Ec__IteratorA__ctor_m110 (U3CCreateUniquePlayerU3Ec__IteratorA_t31 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Menu/<CreateUniquePlayer>c__IteratorA::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CCreateUniquePlayerU3Ec__IteratorA_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m111 (U3CCreateUniquePlayerU3Ec__IteratorA_t31 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_5);
		return L_0;
	}
}
// System.Object Menu/<CreateUniquePlayer>c__IteratorA::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCreateUniquePlayerU3Ec__IteratorA_System_Collections_IEnumerator_get_Current_m112 (U3CCreateUniquePlayerU3Ec__IteratorA_t31 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_5);
		return L_0;
	}
}
// System.Boolean Menu/<CreateUniquePlayer>c__IteratorA::MoveNext()
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral12;
extern Il2CppCodeGenString* _stringLiteral70;
extern Il2CppCodeGenString* _stringLiteral67;
extern Il2CppCodeGenString* _stringLiteral11;
extern Il2CppCodeGenString* _stringLiteral17;
extern "C" bool U3CCreateUniquePlayerU3Ec__IteratorA_MoveNext_m113 (U3CCreateUniquePlayerU3Ec__IteratorA_t31 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral12 = il2cpp_codegen_string_literal_from_index(12);
		_stringLiteral70 = il2cpp_codegen_string_literal_from_index(70);
		_stringLiteral67 = il2cpp_codegen_string_literal_from_index(67);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		_stringLiteral17 = il2cpp_codegen_string_literal_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_4);
		V_0 = L_0;
		__this->___U24PC_4 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0070;
		}
	}
	{
		goto IL_00dc;
	}

IL_0021:
	{
		WWWForm_t8 * L_2 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_2, /*hidden argument*/NULL);
		__this->___U3CformU3E__0_0 = L_2;
		WWWForm_t8 * L_3 = (__this->___U3CformU3E__0_0);
		String_t* L_4 = (__this->___username_1);
		NullCheck(L_3);
		WWWForm_AddField_m203(L_3, _stringLiteral12, L_4, /*hidden argument*/NULL);
		WWWForm_t8 * L_5 = (__this->___U3CformU3E__0_0);
		WWW_t9 * L_6 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_6, _stringLiteral70, L_5, /*hidden argument*/NULL);
		__this->___U3CwU3E__1_2 = L_6;
		WWW_t9 * L_7 = (__this->___U3CwU3E__1_2);
		__this->___U24current_5 = L_7;
		__this->___U24PC_4 = 1;
		goto IL_00de;
	}

IL_0070:
	{
		WWW_t9 * L_8 = (__this->___U3CwU3E__1_2);
		NullCheck(L_8);
		String_t* L_9 = WWW_get_error_m218(L_8, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_00c5;
		}
	}
	{
		WWW_t9 * L_10 = (__this->___U3CwU3E__1_2);
		NullCheck(L_10);
		String_t* L_11 = WWW_get_text_m206(L_10, /*hidden argument*/NULL);
		__this->___U3CnewIDU3E__2_3 = L_11;
		String_t* L_12 = (__this->___U3CnewIDU3E__2_3);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_13 = String_Concat_m217(NULL /*static, unused*/, _stringLiteral67, L_12, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_13, /*hidden argument*/NULL);
		String_t* L_14 = (__this->___username_1);
		PlayerPrefs_SetString_m220(NULL /*static, unused*/, _stringLiteral11, L_14, /*hidden argument*/NULL);
		Application_LoadLevel_m241(NULL /*static, unused*/, _stringLiteral17, /*hidden argument*/NULL);
		goto IL_00d5;
	}

IL_00c5:
	{
		WWW_t9 * L_15 = (__this->___U3CwU3E__1_2);
		NullCheck(L_15);
		String_t* L_16 = WWW_get_error_m218(L_15, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
	}

IL_00d5:
	{
		__this->___U24PC_4 = (-1);
	}

IL_00dc:
	{
		return 0;
	}

IL_00de:
	{
		return 1;
	}
	// Dead block : IL_00e0: ldloc.1
}
// System.Void Menu/<CreateUniquePlayer>c__IteratorA::Dispose()
extern "C" void U3CCreateUniquePlayerU3Ec__IteratorA_Dispose_m114 (U3CCreateUniquePlayerU3Ec__IteratorA_t31 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_4 = (-1);
		return;
	}
}
// System.Void Menu/<CreateUniquePlayer>c__IteratorA::Reset()
extern TypeInfo* NotSupportedException_t55_il2cpp_TypeInfo_var;
extern "C" void U3CCreateUniquePlayerU3Ec__IteratorA_Reset_m115 (U3CCreateUniquePlayerU3Ec__IteratorA_t31 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t55 * L_0 = (NotSupportedException_t55 *)il2cpp_codegen_object_new (NotSupportedException_t55_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m208(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void Menu::.ctor()
extern "C" void Menu__ctor_m116 (Menu_t32 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Menu::Start()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral11;
extern Il2CppCodeGenString* _stringLiteral71;
extern Il2CppCodeGenString* _stringLiteral72;
extern Il2CppCodeGenString* _stringLiteral17;
extern Il2CppCodeGenString* _stringLiteral73;
extern "C" void Menu_Start_m117 (Menu_t32 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		_stringLiteral71 = il2cpp_codegen_string_literal_from_index(71);
		_stringLiteral72 = il2cpp_codegen_string_literal_from_index(72);
		_stringLiteral17 = il2cpp_codegen_string_literal_from_index(17);
		_stringLiteral73 = il2cpp_codegen_string_literal_from_index(73);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = PlayerPrefs_HasKey_m254(NULL /*static, unused*/, _stringLiteral11, /*hidden argument*/NULL);
		if (!L_0)
		{
			goto IL_0037;
		}
	}
	{
		String_t* L_1 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral72, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_2 = String_Concat_m217(NULL /*static, unused*/, _stringLiteral71, L_1, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Application_LoadLevel_m241(NULL /*static, unused*/, _stringLiteral17, /*hidden argument*/NULL);
		goto IL_0041;
	}

IL_0037:
	{
		Debug_Log_m202(NULL /*static, unused*/, _stringLiteral73, /*hidden argument*/NULL);
	}

IL_0041:
	{
		return;
	}
}
// System.Void Menu::CreateUser()
extern "C" void Menu_CreateUser_m118 (Menu_t32 * __this, const MethodInfo* method)
{
	{
		InputField_t33 * L_0 = (__this->___username_4);
		NullCheck(L_0);
		String_t* L_1 = InputField_get_text_m255(L_0, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		InputField_t33 * L_2 = (__this->___username_4);
		NullCheck(L_2);
		String_t* L_3 = InputField_get_text_m255(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		String_t* L_4 = String_ToString_m256(L_3, /*hidden argument*/NULL);
		Object_t * L_5 = Menu_CreateUniquePlayer_m120(__this, L_4, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m207(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Menu::PlayGame()
extern Il2CppCodeGenString* _stringLiteral74;
extern "C" void Menu_PlayGame_m119 (Menu_t32 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral74 = il2cpp_codegen_string_literal_from_index(74);
		s_Il2CppMethodIntialized = true;
	}
	{
		Application_LoadLevel_m241(NULL /*static, unused*/, _stringLiteral74, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Menu::CreateUniquePlayer(System.String)
extern TypeInfo* U3CCreateUniquePlayerU3Ec__IteratorA_t31_il2cpp_TypeInfo_var;
extern "C" Object_t * Menu_CreateUniquePlayer_m120 (Menu_t32 * __this, String_t* ___username, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CCreateUniquePlayerU3Ec__IteratorA_t31_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(31);
		s_Il2CppMethodIntialized = true;
	}
	U3CCreateUniquePlayerU3Ec__IteratorA_t31 * V_0 = {0};
	{
		U3CCreateUniquePlayerU3Ec__IteratorA_t31 * L_0 = (U3CCreateUniquePlayerU3Ec__IteratorA_t31 *)il2cpp_codegen_object_new (U3CCreateUniquePlayerU3Ec__IteratorA_t31_il2cpp_TypeInfo_var);
		U3CCreateUniquePlayerU3Ec__IteratorA__ctor_m110(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCreateUniquePlayerU3Ec__IteratorA_t31 * L_1 = V_0;
		String_t* L_2 = ___username;
		NullCheck(L_1);
		L_1->___username_1 = L_2;
		U3CCreateUniquePlayerU3Ec__IteratorA_t31 * L_3 = V_0;
		String_t* L_4 = ___username;
		NullCheck(L_3);
		L_3->___U3CU24U3Eusername_6 = L_4;
		U3CCreateUniquePlayerU3Ec__IteratorA_t31 * L_5 = V_0;
		return L_5;
	}
}
// System.Void MovingObject/<SmoothMovement>c__IteratorB::.ctor()
extern "C" void U3CSmoothMovementU3Ec__IteratorB__ctor_m121 (U3CSmoothMovementU3Ec__IteratorB_t34 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object MovingObject/<SmoothMovement>c__IteratorB::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CSmoothMovementU3Ec__IteratorB_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m122 (U3CSmoothMovementU3Ec__IteratorB_t34 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_4);
		return L_0;
	}
}
// System.Object MovingObject/<SmoothMovement>c__IteratorB::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CSmoothMovementU3Ec__IteratorB_System_Collections_IEnumerator_get_Current_m123 (U3CSmoothMovementU3Ec__IteratorB_t34 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_4);
		return L_0;
	}
}
// System.Boolean MovingObject/<SmoothMovement>c__IteratorB::MoveNext()
extern "C" bool U3CSmoothMovementU3Ec__IteratorB_MoveNext_m124 (U3CSmoothMovementU3Ec__IteratorB_t34 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	Vector3_t35  V_1 = {0};
	Vector3_t35  V_2 = {0};
	bool V_3 = false;
	{
		int32_t L_0 = (__this->___U24PC_3);
		V_0 = L_0;
		__this->___U24PC_3 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_00dd;
		}
	}
	{
		goto IL_00f4;
	}

IL_0021:
	{
		MovingObject_t36 * L_2 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_2);
		Transform_t3 * L_3 = Component_get_transform_m224(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t35  L_4 = Transform_get_position_m257(L_3, /*hidden argument*/NULL);
		Vector3_t35  L_5 = (__this->___end_0);
		Vector3_t35  L_6 = Vector3_op_Subtraction_m251(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		V_1 = L_6;
		float L_7 = Vector3_get_sqrMagnitude_m258((&V_1), /*hidden argument*/NULL);
		__this->___U3CsqrRemainingDistanceU3E__0_1 = L_7;
		goto IL_00dd;
	}

IL_004f:
	{
		MovingObject_t36 * L_8 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_8);
		Rigidbody2D_t39 * L_9 = (L_8->___rb2D_5);
		NullCheck(L_9);
		Vector2_t59  L_10 = Rigidbody2D_get_position_m259(L_9, /*hidden argument*/NULL);
		Vector3_t35  L_11 = Vector2_op_Implicit_m260(NULL /*static, unused*/, L_10, /*hidden argument*/NULL);
		Vector3_t35  L_12 = (__this->___end_0);
		MovingObject_t36 * L_13 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_13);
		float L_14 = (L_13->___inverseMoveTime_6);
		float L_15 = Time_get_deltaTime_m212(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t35  L_16 = Vector3_MoveTowards_m261(NULL /*static, unused*/, L_11, L_12, ((float)((float)L_14*(float)L_15)), /*hidden argument*/NULL);
		__this->___U3CnewPostionU3E__1_2 = L_16;
		MovingObject_t36 * L_17 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_17);
		Rigidbody2D_t39 * L_18 = (L_17->___rb2D_5);
		Vector3_t35  L_19 = (__this->___U3CnewPostionU3E__1_2);
		Vector2_t59  L_20 = Vector2_op_Implicit_m262(NULL /*static, unused*/, L_19, /*hidden argument*/NULL);
		NullCheck(L_18);
		Rigidbody2D_MovePosition_m263(L_18, L_20, /*hidden argument*/NULL);
		MovingObject_t36 * L_21 = (__this->___U3CU3Ef__this_6);
		NullCheck(L_21);
		Transform_t3 * L_22 = Component_get_transform_m224(L_21, /*hidden argument*/NULL);
		NullCheck(L_22);
		Vector3_t35  L_23 = Transform_get_position_m257(L_22, /*hidden argument*/NULL);
		Vector3_t35  L_24 = (__this->___end_0);
		Vector3_t35  L_25 = Vector3_op_Subtraction_m251(NULL /*static, unused*/, L_23, L_24, /*hidden argument*/NULL);
		V_2 = L_25;
		float L_26 = Vector3_get_sqrMagnitude_m258((&V_2), /*hidden argument*/NULL);
		__this->___U3CsqrRemainingDistanceU3E__0_1 = L_26;
		__this->___U24current_4 = NULL;
		__this->___U24PC_3 = 1;
		goto IL_00f6;
	}

IL_00dd:
	{
		float L_27 = (__this->___U3CsqrRemainingDistanceU3E__0_1);
		if ((((float)L_27) > ((float)(1.401298E-45f))))
		{
			goto IL_004f;
		}
	}
	{
		__this->___U24PC_3 = (-1);
	}

IL_00f4:
	{
		return 0;
	}

IL_00f6:
	{
		return 1;
	}
	// Dead block : IL_00f8: ldloc.3
}
// System.Void MovingObject/<SmoothMovement>c__IteratorB::Dispose()
extern "C" void U3CSmoothMovementU3Ec__IteratorB_Dispose_m125 (U3CSmoothMovementU3Ec__IteratorB_t34 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_3 = (-1);
		return;
	}
}
// System.Void MovingObject/<SmoothMovement>c__IteratorB::Reset()
extern TypeInfo* NotSupportedException_t55_il2cpp_TypeInfo_var;
extern "C" void U3CSmoothMovementU3Ec__IteratorB_Reset_m126 (U3CSmoothMovementU3Ec__IteratorB_t34 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t55 * L_0 = (NotSupportedException_t55 *)il2cpp_codegen_object_new (NotSupportedException_t55_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m208(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void MovingObject::.ctor()
extern "C" void MovingObject__ctor_m127 (MovingObject_t36 * __this, const MethodInfo* method)
{
	{
		__this->___moveTime_2 = (0.1f);
		MonoBehaviour__ctor_m191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void MovingObject::Start()
extern const MethodInfo* Component_GetComponent_TisBoxCollider2D_t38_m264_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody2D_t39_m265_MethodInfo_var;
extern "C" void MovingObject_Start_m128 (MovingObject_t36 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisBoxCollider2D_t38_m264_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483655);
		Component_GetComponent_TisRigidbody2D_t39_m265_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483656);
		s_Il2CppMethodIntialized = true;
	}
	{
		BoxCollider2D_t38 * L_0 = Component_GetComponent_TisBoxCollider2D_t38_m264(__this, /*hidden argument*/Component_GetComponent_TisBoxCollider2D_t38_m264_MethodInfo_var);
		__this->___boxCollider_4 = L_0;
		Rigidbody2D_t39 * L_1 = Component_GetComponent_TisRigidbody2D_t39_m265(__this, /*hidden argument*/Component_GetComponent_TisRigidbody2D_t39_m265_MethodInfo_var);
		__this->___rb2D_5 = L_1;
		float L_2 = (__this->___moveTime_2);
		__this->___inverseMoveTime_6 = ((float)((float)(1.0f)/(float)L_2));
		return;
	}
}
// System.Boolean MovingObject::Move(System.Int32,System.Int32,UnityEngine.RaycastHit2D&)
extern TypeInfo* Physics2D_t62_il2cpp_TypeInfo_var;
extern "C" bool MovingObject_Move_m129 (MovingObject_t36 * __this, int32_t ___xDir, int32_t ___yDir, RaycastHit2D_t52 * ___hit, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Physics2D_t62_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t59  V_0 = {0};
	Vector2_t59  V_1 = {0};
	{
		Transform_t3 * L_0 = Component_get_transform_m224(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t35  L_1 = Transform_get_position_m257(L_0, /*hidden argument*/NULL);
		Vector2_t59  L_2 = Vector2_op_Implicit_m262(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		Vector2_t59  L_3 = V_0;
		int32_t L_4 = ___xDir;
		int32_t L_5 = ___yDir;
		Vector2_t59  L_6 = {0};
		Vector2__ctor_m231(&L_6, (((float)L_4)), (((float)L_5)), /*hidden argument*/NULL);
		Vector2_t59  L_7 = Vector2_op_Addition_m266(NULL /*static, unused*/, L_3, L_6, /*hidden argument*/NULL);
		V_1 = L_7;
		Vector2_t59  L_8 = V_1;
		Vector3_t35  L_9 = Vector2_op_Implicit_m260(NULL /*static, unused*/, L_8, /*hidden argument*/NULL);
		Object_t * L_10 = MovingObject_SmoothMovement_m130(__this, L_9, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m207(__this, L_10, /*hidden argument*/NULL);
		RaycastHit2D_t52 * L_11 = ___hit;
		Vector2_t59  L_12 = V_0;
		Vector2_t59  L_13 = V_1;
		LayerMask_t37  L_14 = (__this->___blockingLayer_3);
		int32_t L_15 = LayerMask_op_Implicit_m267(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Physics2D_t62_il2cpp_TypeInfo_var);
		RaycastHit2D_t52  L_16 = Physics2D_Linecast_m268(NULL /*static, unused*/, L_12, L_13, L_15, /*hidden argument*/NULL);
		*L_11 = L_16;
		return 0;
	}
}
// System.Collections.IEnumerator MovingObject::SmoothMovement(UnityEngine.Vector3)
extern TypeInfo* U3CSmoothMovementU3Ec__IteratorB_t34_il2cpp_TypeInfo_var;
extern "C" Object_t * MovingObject_SmoothMovement_m130 (MovingObject_t36 * __this, Vector3_t35  ___end, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CSmoothMovementU3Ec__IteratorB_t34_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		s_Il2CppMethodIntialized = true;
	}
	U3CSmoothMovementU3Ec__IteratorB_t34 * V_0 = {0};
	{
		U3CSmoothMovementU3Ec__IteratorB_t34 * L_0 = (U3CSmoothMovementU3Ec__IteratorB_t34 *)il2cpp_codegen_object_new (U3CSmoothMovementU3Ec__IteratorB_t34_il2cpp_TypeInfo_var);
		U3CSmoothMovementU3Ec__IteratorB__ctor_m121(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSmoothMovementU3Ec__IteratorB_t34 * L_1 = V_0;
		Vector3_t35  L_2 = ___end;
		NullCheck(L_1);
		L_1->___end_0 = L_2;
		U3CSmoothMovementU3Ec__IteratorB_t34 * L_3 = V_0;
		Vector3_t35  L_4 = ___end;
		NullCheck(L_3);
		L_3->___U3CU24U3Eend_5 = L_4;
		U3CSmoothMovementU3Ec__IteratorB_t34 * L_5 = V_0;
		NullCheck(L_5);
		L_5->___U3CU3Ef__this_6 = __this;
		U3CSmoothMovementU3Ec__IteratorB_t34 * L_6 = V_0;
		return L_6;
	}
}
// System.Void MovingObject::AttemptMove(System.Int32,System.Int32)
extern "C" void MovingObject_AttemptMove_m131 (MovingObject_t36 * __this, int32_t ___xDir, int32_t ___yDir, const MethodInfo* method)
{
	RaycastHit2D_t52  V_0 = {0};
	bool V_1 = false;
	{
		int32_t L_0 = ___xDir;
		int32_t L_1 = ___yDir;
		bool L_2 = MovingObject_Move_m129(__this, L_0, L_1, (&V_0), /*hidden argument*/NULL);
		V_1 = L_2;
		return;
	}
}
// System.Void Player/<SendPlayer1Position>c__IteratorC::.ctor()
extern "C" void U3CSendPlayer1PositionU3Ec__IteratorC__ctor_m132 (U3CSendPlayer1PositionU3Ec__IteratorC_t40 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Player/<SendPlayer1Position>c__IteratorC::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CSendPlayer1PositionU3Ec__IteratorC_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m133 (U3CSendPlayer1PositionU3Ec__IteratorC_t40 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_7);
		return L_0;
	}
}
// System.Object Player/<SendPlayer1Position>c__IteratorC::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CSendPlayer1PositionU3Ec__IteratorC_System_Collections_IEnumerator_get_Current_m134 (U3CSendPlayer1PositionU3Ec__IteratorC_t40 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_7);
		return L_0;
	}
}
// System.Boolean Player/<SendPlayer1Position>c__IteratorC::MoveNext()
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral75;
extern Il2CppCodeGenString* _stringLiteral76;
extern Il2CppCodeGenString* _stringLiteral77;
extern Il2CppCodeGenString* _stringLiteral78;
extern Il2CppCodeGenString* _stringLiteral79;
extern Il2CppCodeGenString* _stringLiteral80;
extern Il2CppCodeGenString* _stringLiteral54;
extern Il2CppCodeGenString* _stringLiteral81;
extern Il2CppCodeGenString* _stringLiteral82;
extern "C" bool U3CSendPlayer1PositionU3Ec__IteratorC_MoveNext_m135 (U3CSendPlayer1PositionU3Ec__IteratorC_t40 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral75 = il2cpp_codegen_string_literal_from_index(75);
		_stringLiteral76 = il2cpp_codegen_string_literal_from_index(76);
		_stringLiteral77 = il2cpp_codegen_string_literal_from_index(77);
		_stringLiteral78 = il2cpp_codegen_string_literal_from_index(78);
		_stringLiteral79 = il2cpp_codegen_string_literal_from_index(79);
		_stringLiteral80 = il2cpp_codegen_string_literal_from_index(80);
		_stringLiteral54 = il2cpp_codegen_string_literal_from_index(54);
		_stringLiteral81 = il2cpp_codegen_string_literal_from_index(81);
		_stringLiteral82 = il2cpp_codegen_string_literal_from_index(82);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_6);
		V_0 = L_0;
		__this->___U24PC_6 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0105;
		}
	}
	{
		goto IL_0141;
	}

IL_0021:
	{
		WWWForm_t8 * L_2 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_2, /*hidden argument*/NULL);
		__this->___U3CformU3E__0_0 = L_2;
		String_t* L_3 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral1, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		WWWForm_t8 * L_4 = (__this->___U3CformU3E__0_0);
		String_t* L_5 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral1, /*hidden argument*/NULL);
		NullCheck(L_4);
		WWWForm_AddField_m203(L_4, _stringLiteral2, L_5, /*hidden argument*/NULL);
		WWWForm_t8 * L_6 = (__this->___U3CformU3E__0_0);
		NullCheck(L_6);
		WWWForm_AddField_m203(L_6, _stringLiteral75, _stringLiteral76, /*hidden argument*/NULL);
		WWWForm_t8 * L_7 = (__this->___U3CformU3E__0_0);
		NullCheck(L_7);
		WWWForm_AddField_m203(L_7, _stringLiteral77, _stringLiteral78, /*hidden argument*/NULL);
		WWWForm_t8 * L_8 = (__this->___U3CformU3E__0_0);
		int32_t L_9 = (__this->___a_1);
		NullCheck(L_8);
		WWWForm_AddField_m269(L_8, _stringLiteral79, L_9, /*hidden argument*/NULL);
		WWWForm_t8 * L_10 = (__this->___U3CformU3E__0_0);
		int32_t L_11 = (__this->___b_2);
		NullCheck(L_10);
		WWWForm_AddField_m269(L_10, _stringLiteral80, L_11, /*hidden argument*/NULL);
		WWWForm_t8 * L_12 = (__this->___U3CformU3E__0_0);
		int32_t L_13 = (__this->___diceNumber_3);
		NullCheck(L_12);
		WWWForm_AddField_m269(L_12, _stringLiteral54, L_13, /*hidden argument*/NULL);
		WWWForm_t8 * L_14 = (__this->___U3CformU3E__0_0);
		int32_t L_15 = (__this->___playerPosition_4);
		NullCheck(L_14);
		WWWForm_AddField_m269(L_14, _stringLiteral81, L_15, /*hidden argument*/NULL);
		WWWForm_t8 * L_16 = (__this->___U3CformU3E__0_0);
		WWW_t9 * L_17 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_17, _stringLiteral82, L_16, /*hidden argument*/NULL);
		__this->___U3CwU3E__1_5 = L_17;
		WWW_t9 * L_18 = (__this->___U3CwU3E__1_5);
		__this->___U24current_7 = L_18;
		__this->___U24PC_6 = 1;
		goto IL_0143;
	}

IL_0105:
	{
		WWW_t9 * L_19 = (__this->___U3CwU3E__1_5);
		NullCheck(L_19);
		String_t* L_20 = WWW_get_error_m218(L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_012a;
		}
	}
	{
		WWW_t9 * L_21 = (__this->___U3CwU3E__1_5);
		NullCheck(L_21);
		String_t* L_22 = WWW_get_text_m206(L_21, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		goto IL_013a;
	}

IL_012a:
	{
		WWW_t9 * L_23 = (__this->___U3CwU3E__1_5);
		NullCheck(L_23);
		String_t* L_24 = WWW_get_error_m218(L_23, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
	}

IL_013a:
	{
		__this->___U24PC_6 = (-1);
	}

IL_0141:
	{
		return 0;
	}

IL_0143:
	{
		return 1;
	}
	// Dead block : IL_0145: ldloc.1
}
// System.Void Player/<SendPlayer1Position>c__IteratorC::Dispose()
extern "C" void U3CSendPlayer1PositionU3Ec__IteratorC_Dispose_m136 (U3CSendPlayer1PositionU3Ec__IteratorC_t40 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_6 = (-1);
		return;
	}
}
// System.Void Player/<SendPlayer1Position>c__IteratorC::Reset()
extern TypeInfo* NotSupportedException_t55_il2cpp_TypeInfo_var;
extern "C" void U3CSendPlayer1PositionU3Ec__IteratorC_Reset_m137 (U3CSendPlayer1PositionU3Ec__IteratorC_t40 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t55 * L_0 = (NotSupportedException_t55 *)il2cpp_codegen_object_new (NotSupportedException_t55_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m208(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void Player/<GetPlayer1Position>c__IteratorD::.ctor()
extern "C" void U3CGetPlayer1PositionU3Ec__IteratorD__ctor_m138 (U3CGetPlayer1PositionU3Ec__IteratorD_t41 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Player/<GetPlayer1Position>c__IteratorD::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CGetPlayer1PositionU3Ec__IteratorD_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m139 (U3CGetPlayer1PositionU3Ec__IteratorD_t41 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_8);
		return L_0;
	}
}
// System.Object Player/<GetPlayer1Position>c__IteratorD::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetPlayer1PositionU3Ec__IteratorD_System_Collections_IEnumerator_get_Current_m140 (U3CGetPlayer1PositionU3Ec__IteratorD_t41 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_8);
		return L_0;
	}
}
// System.Boolean Player/<GetPlayer1Position>c__IteratorD::MoveNext()
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t58_il2cpp_TypeInfo_var;
extern TypeInfo* GameManager_t22_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral83;
extern Il2CppCodeGenString* _stringLiteral38;
extern Il2CppCodeGenString* _stringLiteral84;
extern Il2CppCodeGenString* _stringLiteral85;
extern "C" bool U3CGetPlayer1PositionU3Ec__IteratorD_MoveNext_m141 (U3CGetPlayer1PositionU3Ec__IteratorD_t41 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		CharU5BU5D_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GameManager_t22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral83 = il2cpp_codegen_string_literal_from_index(83);
		_stringLiteral38 = il2cpp_codegen_string_literal_from_index(38);
		_stringLiteral84 = il2cpp_codegen_string_literal_from_index(84);
		_stringLiteral85 = il2cpp_codegen_string_literal_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_7);
		V_0 = L_0;
		__this->___U24PC_7 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0074;
		}
	}
	{
		goto IL_0146;
	}

IL_0021:
	{
		WWWForm_t8 * L_2 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_2, /*hidden argument*/NULL);
		__this->___U3CformU3E__0_0 = L_2;
		WWWForm_t8 * L_3 = (__this->___U3CformU3E__0_0);
		String_t* L_4 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral1, /*hidden argument*/NULL);
		NullCheck(L_3);
		WWWForm_AddField_m203(L_3, _stringLiteral2, L_4, /*hidden argument*/NULL);
		WWWForm_t8 * L_5 = (__this->___U3CformU3E__0_0);
		WWW_t9 * L_6 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_6, _stringLiteral83, L_5, /*hidden argument*/NULL);
		__this->___U3CwU3E__1_1 = L_6;
		WWW_t9 * L_7 = (__this->___U3CwU3E__1_1);
		__this->___U24current_8 = L_7;
		__this->___U24PC_7 = 1;
		goto IL_0148;
	}

IL_0074:
	{
		WWW_t9 * L_8 = (__this->___U3CwU3E__1_1);
		NullCheck(L_8);
		String_t* L_9 = WWW_get_text_m206(L_8, /*hidden argument*/NULL);
		__this->___U3CJsonStringU3E__2_2 = L_9;
		String_t* L_10 = (__this->___U3CJsonStringU3E__2_2);
		CharU5BU5D_t58* L_11 = ((CharU5BU5D_t58*)SZArrayNew(CharU5BU5D_t58_il2cpp_TypeInfo_var, 1));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_11, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)45);
		NullCheck(L_10);
		StringU5BU5D_t16* L_12 = String_Split_m219(L_10, L_11, /*hidden argument*/NULL);
		__this->___U3CarrayU3E__3_3 = L_12;
		StringU5BU5D_t16* L_13 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		int32_t L_14 = 0;
		float L_15 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_13, L_14, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CxU3E__4_4 = L_15;
		StringU5BU5D_t16* L_16 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
		int32_t L_17 = 1;
		float L_18 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_16, L_17, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CyU3E__5_5 = L_18;
		StringU5BU5D_t16* L_19 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 2);
		int32_t L_20 = 2;
		int32_t L_21 = Int32_Parse_m244(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_19, L_20, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CpU3E__6_6 = L_21;
		int32_t L_22 = (__this->___U3CpU3E__6_6);
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22 = L_22;
		int32_t L_23 = (__this->___U3CpU3E__6_6);
		PlayerPrefs_SetInt_m246(NULL /*static, unused*/, _stringLiteral38, L_23, /*hidden argument*/NULL);
		Player_t11 * L_24 = (__this->___U3CU3Ef__this_9);
		NullCheck(L_24);
		Transform_t3 * L_25 = Component_get_transform_m224(L_24, /*hidden argument*/NULL);
		float L_26 = (__this->___U3CxU3E__4_4);
		float L_27 = (__this->___U3CyU3E__5_5);
		Vector3_t35  L_28 = {0};
		Vector3__ctor_m192(&L_28, L_26, L_27, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_position_m245(L_25, L_28, /*hidden argument*/NULL);
		StringU5BU5D_t16* L_29 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 0);
		int32_t L_30 = 0;
		StringU5BU5D_t16* L_31 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 1);
		int32_t L_32 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m221(NULL /*static, unused*/, _stringLiteral84, (*(String_t**)(String_t**)SZArrayLdElema(L_29, L_30, sizeof(String_t*))), _stringLiteral85, (*(String_t**)(String_t**)SZArrayLdElema(L_31, L_32, sizeof(String_t*))), /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		__this->___U24PC_7 = (-1);
	}

IL_0146:
	{
		return 0;
	}

IL_0148:
	{
		return 1;
	}
	// Dead block : IL_014a: ldloc.1
}
// System.Void Player/<GetPlayer1Position>c__IteratorD::Dispose()
extern "C" void U3CGetPlayer1PositionU3Ec__IteratorD_Dispose_m142 (U3CGetPlayer1PositionU3Ec__IteratorD_t41 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_7 = (-1);
		return;
	}
}
// System.Void Player/<GetPlayer1Position>c__IteratorD::Reset()
extern TypeInfo* NotSupportedException_t55_il2cpp_TypeInfo_var;
extern "C" void U3CGetPlayer1PositionU3Ec__IteratorD_Reset_m143 (U3CGetPlayer1PositionU3Ec__IteratorD_t41 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t55 * L_0 = (NotSupportedException_t55 *)il2cpp_codegen_object_new (NotSupportedException_t55_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m208(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void Player/<ExecuteAfterTime>c__IteratorE::.ctor()
extern "C" void U3CExecuteAfterTimeU3Ec__IteratorE__ctor_m144 (U3CExecuteAfterTimeU3Ec__IteratorE_t42 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Player/<ExecuteAfterTime>c__IteratorE::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CExecuteAfterTimeU3Ec__IteratorE_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m145 (U3CExecuteAfterTimeU3Ec__IteratorE_t42 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Object Player/<ExecuteAfterTime>c__IteratorE::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CExecuteAfterTimeU3Ec__IteratorE_System_Collections_IEnumerator_get_Current_m146 (U3CExecuteAfterTimeU3Ec__IteratorE_t42 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_2);
		return L_0;
	}
}
// System.Boolean Player/<ExecuteAfterTime>c__IteratorE::MoveNext()
extern TypeInfo* WaitForSeconds_t63_il2cpp_TypeInfo_var;
extern TypeInfo* GameManager_t22_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral86;
extern "C" bool U3CExecuteAfterTimeU3Ec__IteratorE_MoveNext_m147 (U3CExecuteAfterTimeU3Ec__IteratorE_t42 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(36);
		GameManager_t22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		_stringLiteral86 = il2cpp_codegen_string_literal_from_index(86);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_1);
		V_0 = L_0;
		__this->___U24PC_1 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_003e;
		}
	}
	{
		goto IL_0055;
	}

IL_0021:
	{
		float L_2 = (__this->___time_0);
		WaitForSeconds_t63 * L_3 = (WaitForSeconds_t63 *)il2cpp_codegen_object_new (WaitForSeconds_t63_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m270(L_3, L_2, /*hidden argument*/NULL);
		__this->___U24current_2 = L_3;
		__this->___U24PC_1 = 1;
		goto IL_0057;
	}

IL_003e:
	{
		Debug_Log_m202(NULL /*static, unused*/, _stringLiteral86, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___ShowButtonBool_9 = 1;
		__this->___U24PC_1 = (-1);
	}

IL_0055:
	{
		return 0;
	}

IL_0057:
	{
		return 1;
	}
	// Dead block : IL_0059: ldloc.1
}
// System.Void Player/<ExecuteAfterTime>c__IteratorE::Dispose()
extern "C" void U3CExecuteAfterTimeU3Ec__IteratorE_Dispose_m148 (U3CExecuteAfterTimeU3Ec__IteratorE_t42 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_1 = (-1);
		return;
	}
}
// System.Void Player/<ExecuteAfterTime>c__IteratorE::Reset()
extern TypeInfo* NotSupportedException_t55_il2cpp_TypeInfo_var;
extern "C" void U3CExecuteAfterTimeU3Ec__IteratorE_Reset_m149 (U3CExecuteAfterTimeU3Ec__IteratorE_t42 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t55 * L_0 = (NotSupportedException_t55 *)il2cpp_codegen_object_new (NotSupportedException_t55_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m208(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void Player::.ctor()
extern "C" void Player__ctor_m150 (Player_t11 * __this, const MethodInfo* method)
{
	{
		__this->___wallDamage_7 = 1;
		MovingObject__ctor_m127(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Player::Start()
extern "C" void Player_Start_m151 (Player_t11 * __this, const MethodInfo* method)
{
	{
		MovingObject_Start_m128(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Player::MoveThePlayer(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void Player_MoveThePlayer_m152 (Player_t11 * __this, int32_t ___x, int32_t ___y, int32_t ___a, int32_t ___b, int32_t ___playerPosition, int32_t ___diceNumber, const MethodInfo* method)
{
	{
		int32_t L_0 = ___x;
		int32_t L_1 = ___y;
		VirtActionInvoker2< int32_t, int32_t >::Invoke(5 /* System.Void Player::AttemptMove(System.Int32,System.Int32) */, __this, L_0, L_1);
		int32_t L_2 = ___a;
		int32_t L_3 = ___b;
		int32_t L_4 = ___playerPosition;
		int32_t L_5 = ___diceNumber;
		Object_t * L_6 = Player_SendPlayer1Position_m154(__this, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m207(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Player::AttemptMove(System.Int32,System.Int32)
extern TypeInfo* GameManager_t22_il2cpp_TypeInfo_var;
extern "C" void Player_AttemptMove_m153 (Player_t11 * __this, int32_t ___xDir, int32_t ___yDir, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameManager_t22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		int32_t L_0 = ___xDir;
		int32_t L_1 = ___yDir;
		MovingObject_AttemptMove_m131(__this, L_0, L_1, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playersTurn_6 = 0;
		return;
	}
}
// System.Collections.IEnumerator Player::SendPlayer1Position(System.Int32,System.Int32,System.Int32,System.Int32)
extern TypeInfo* U3CSendPlayer1PositionU3Ec__IteratorC_t40_il2cpp_TypeInfo_var;
extern "C" Object_t * Player_SendPlayer1Position_m154 (Player_t11 * __this, int32_t ___a, int32_t ___b, int32_t ___playerPosition, int32_t ___diceNumber, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CSendPlayer1PositionU3Ec__IteratorC_t40_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	U3CSendPlayer1PositionU3Ec__IteratorC_t40 * V_0 = {0};
	{
		U3CSendPlayer1PositionU3Ec__IteratorC_t40 * L_0 = (U3CSendPlayer1PositionU3Ec__IteratorC_t40 *)il2cpp_codegen_object_new (U3CSendPlayer1PositionU3Ec__IteratorC_t40_il2cpp_TypeInfo_var);
		U3CSendPlayer1PositionU3Ec__IteratorC__ctor_m132(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSendPlayer1PositionU3Ec__IteratorC_t40 * L_1 = V_0;
		int32_t L_2 = ___a;
		NullCheck(L_1);
		L_1->___a_1 = L_2;
		U3CSendPlayer1PositionU3Ec__IteratorC_t40 * L_3 = V_0;
		int32_t L_4 = ___b;
		NullCheck(L_3);
		L_3->___b_2 = L_4;
		U3CSendPlayer1PositionU3Ec__IteratorC_t40 * L_5 = V_0;
		int32_t L_6 = ___diceNumber;
		NullCheck(L_5);
		L_5->___diceNumber_3 = L_6;
		U3CSendPlayer1PositionU3Ec__IteratorC_t40 * L_7 = V_0;
		int32_t L_8 = ___playerPosition;
		NullCheck(L_7);
		L_7->___playerPosition_4 = L_8;
		U3CSendPlayer1PositionU3Ec__IteratorC_t40 * L_9 = V_0;
		int32_t L_10 = ___a;
		NullCheck(L_9);
		L_9->___U3CU24U3Ea_8 = L_10;
		U3CSendPlayer1PositionU3Ec__IteratorC_t40 * L_11 = V_0;
		int32_t L_12 = ___b;
		NullCheck(L_11);
		L_11->___U3CU24U3Eb_9 = L_12;
		U3CSendPlayer1PositionU3Ec__IteratorC_t40 * L_13 = V_0;
		int32_t L_14 = ___diceNumber;
		NullCheck(L_13);
		L_13->___U3CU24U3EdiceNumber_10 = L_14;
		U3CSendPlayer1PositionU3Ec__IteratorC_t40 * L_15 = V_0;
		int32_t L_16 = ___playerPosition;
		NullCheck(L_15);
		L_15->___U3CU24U3EplayerPosition_11 = L_16;
		U3CSendPlayer1PositionU3Ec__IteratorC_t40 * L_17 = V_0;
		return L_17;
	}
}
// System.Void Player::ChangeTransform(System.Single,System.Single)
extern Il2CppCodeGenString* _stringLiteral87;
extern "C" void Player_ChangeTransform_m155 (Player_t11 * __this, float ___x, float ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral87 = il2cpp_codegen_string_literal_from_index(87);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m202(NULL /*static, unused*/, _stringLiteral87, /*hidden argument*/NULL);
		Transform_t3 * L_0 = Component_get_transform_m224(__this, /*hidden argument*/NULL);
		float L_1 = ___x;
		float L_2 = ___y;
		Vector3_t35  L_3 = {0};
		Vector3__ctor_m192(&L_3, L_1, L_2, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m245(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Player::GetPlayer1Position()
extern TypeInfo* U3CGetPlayer1PositionU3Ec__IteratorD_t41_il2cpp_TypeInfo_var;
extern "C" Object_t * Player_GetPlayer1Position_m156 (Player_t11 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGetPlayer1PositionU3Ec__IteratorD_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetPlayer1PositionU3Ec__IteratorD_t41 * V_0 = {0};
	{
		U3CGetPlayer1PositionU3Ec__IteratorD_t41 * L_0 = (U3CGetPlayer1PositionU3Ec__IteratorD_t41 *)il2cpp_codegen_object_new (U3CGetPlayer1PositionU3Ec__IteratorD_t41_il2cpp_TypeInfo_var);
		U3CGetPlayer1PositionU3Ec__IteratorD__ctor_m138(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetPlayer1PositionU3Ec__IteratorD_t41 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_9 = __this;
		U3CGetPlayer1PositionU3Ec__IteratorD_t41 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Player::CallExecuteAfterTime()
extern Il2CppCodeGenString* _stringLiteral88;
extern "C" void Player_CallExecuteAfterTime_m157 (Player_t11 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral88 = il2cpp_codegen_string_literal_from_index(88);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m202(NULL /*static, unused*/, _stringLiteral88, /*hidden argument*/NULL);
		Object_t * L_0 = Player_ExecuteAfterTime_m158(__this, (3.0f), /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m207(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator Player::ExecuteAfterTime(System.Single)
extern TypeInfo* U3CExecuteAfterTimeU3Ec__IteratorE_t42_il2cpp_TypeInfo_var;
extern "C" Object_t * Player_ExecuteAfterTime_m158 (Player_t11 * __this, float ___time, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CExecuteAfterTimeU3Ec__IteratorE_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(39);
		s_Il2CppMethodIntialized = true;
	}
	U3CExecuteAfterTimeU3Ec__IteratorE_t42 * V_0 = {0};
	{
		U3CExecuteAfterTimeU3Ec__IteratorE_t42 * L_0 = (U3CExecuteAfterTimeU3Ec__IteratorE_t42 *)il2cpp_codegen_object_new (U3CExecuteAfterTimeU3Ec__IteratorE_t42_il2cpp_TypeInfo_var);
		U3CExecuteAfterTimeU3Ec__IteratorE__ctor_m144(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CExecuteAfterTimeU3Ec__IteratorE_t42 * L_1 = V_0;
		float L_2 = ___time;
		NullCheck(L_1);
		L_1->___time_0 = L_2;
		U3CExecuteAfterTimeU3Ec__IteratorE_t42 * L_3 = V_0;
		float L_4 = ___time;
		NullCheck(L_3);
		L_3->___U3CU24U3Etime_3 = L_4;
		U3CExecuteAfterTimeU3Ec__IteratorE_t42 * L_5 = V_0;
		return L_5;
	}
}
// System.Void PlayerTwo/<SendPlayer2Position>c__IteratorF::.ctor()
extern "C" void U3CSendPlayer2PositionU3Ec__IteratorF__ctor_m159 (U3CSendPlayer2PositionU3Ec__IteratorF_t43 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object PlayerTwo/<SendPlayer2Position>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CSendPlayer2PositionU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m160 (U3CSendPlayer2PositionU3Ec__IteratorF_t43 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_7);
		return L_0;
	}
}
// System.Object PlayerTwo/<SendPlayer2Position>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CSendPlayer2PositionU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m161 (U3CSendPlayer2PositionU3Ec__IteratorF_t43 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_7);
		return L_0;
	}
}
// System.Boolean PlayerTwo/<SendPlayer2Position>c__IteratorF::MoveNext()
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral75;
extern Il2CppCodeGenString* _stringLiteral76;
extern Il2CppCodeGenString* _stringLiteral77;
extern Il2CppCodeGenString* _stringLiteral78;
extern Il2CppCodeGenString* _stringLiteral79;
extern Il2CppCodeGenString* _stringLiteral80;
extern Il2CppCodeGenString* _stringLiteral54;
extern Il2CppCodeGenString* _stringLiteral81;
extern Il2CppCodeGenString* _stringLiteral89;
extern "C" bool U3CSendPlayer2PositionU3Ec__IteratorF_MoveNext_m162 (U3CSendPlayer2PositionU3Ec__IteratorF_t43 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral75 = il2cpp_codegen_string_literal_from_index(75);
		_stringLiteral76 = il2cpp_codegen_string_literal_from_index(76);
		_stringLiteral77 = il2cpp_codegen_string_literal_from_index(77);
		_stringLiteral78 = il2cpp_codegen_string_literal_from_index(78);
		_stringLiteral79 = il2cpp_codegen_string_literal_from_index(79);
		_stringLiteral80 = il2cpp_codegen_string_literal_from_index(80);
		_stringLiteral54 = il2cpp_codegen_string_literal_from_index(54);
		_stringLiteral81 = il2cpp_codegen_string_literal_from_index(81);
		_stringLiteral89 = il2cpp_codegen_string_literal_from_index(89);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_6);
		V_0 = L_0;
		__this->___U24PC_6 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0105;
		}
	}
	{
		goto IL_0141;
	}

IL_0021:
	{
		WWWForm_t8 * L_2 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_2, /*hidden argument*/NULL);
		__this->___U3CformU3E__0_0 = L_2;
		String_t* L_3 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral1, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		WWWForm_t8 * L_4 = (__this->___U3CformU3E__0_0);
		String_t* L_5 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral1, /*hidden argument*/NULL);
		NullCheck(L_4);
		WWWForm_AddField_m203(L_4, _stringLiteral2, L_5, /*hidden argument*/NULL);
		WWWForm_t8 * L_6 = (__this->___U3CformU3E__0_0);
		NullCheck(L_6);
		WWWForm_AddField_m203(L_6, _stringLiteral75, _stringLiteral76, /*hidden argument*/NULL);
		WWWForm_t8 * L_7 = (__this->___U3CformU3E__0_0);
		NullCheck(L_7);
		WWWForm_AddField_m203(L_7, _stringLiteral77, _stringLiteral78, /*hidden argument*/NULL);
		WWWForm_t8 * L_8 = (__this->___U3CformU3E__0_0);
		int32_t L_9 = (__this->___a_1);
		NullCheck(L_8);
		WWWForm_AddField_m269(L_8, _stringLiteral79, L_9, /*hidden argument*/NULL);
		WWWForm_t8 * L_10 = (__this->___U3CformU3E__0_0);
		int32_t L_11 = (__this->___b_2);
		NullCheck(L_10);
		WWWForm_AddField_m269(L_10, _stringLiteral80, L_11, /*hidden argument*/NULL);
		WWWForm_t8 * L_12 = (__this->___U3CformU3E__0_0);
		int32_t L_13 = (__this->___diceNumber_3);
		NullCheck(L_12);
		WWWForm_AddField_m269(L_12, _stringLiteral54, L_13, /*hidden argument*/NULL);
		WWWForm_t8 * L_14 = (__this->___U3CformU3E__0_0);
		int32_t L_15 = (__this->___playerPosition_4);
		NullCheck(L_14);
		WWWForm_AddField_m269(L_14, _stringLiteral81, L_15, /*hidden argument*/NULL);
		WWWForm_t8 * L_16 = (__this->___U3CformU3E__0_0);
		WWW_t9 * L_17 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_17, _stringLiteral89, L_16, /*hidden argument*/NULL);
		__this->___U3CwU3E__1_5 = L_17;
		WWW_t9 * L_18 = (__this->___U3CwU3E__1_5);
		__this->___U24current_7 = L_18;
		__this->___U24PC_6 = 1;
		goto IL_0143;
	}

IL_0105:
	{
		WWW_t9 * L_19 = (__this->___U3CwU3E__1_5);
		NullCheck(L_19);
		String_t* L_20 = WWW_get_error_m218(L_19, /*hidden argument*/NULL);
		if (L_20)
		{
			goto IL_012a;
		}
	}
	{
		WWW_t9 * L_21 = (__this->___U3CwU3E__1_5);
		NullCheck(L_21);
		String_t* L_22 = WWW_get_text_m206(L_21, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_22, /*hidden argument*/NULL);
		goto IL_013a;
	}

IL_012a:
	{
		WWW_t9 * L_23 = (__this->___U3CwU3E__1_5);
		NullCheck(L_23);
		String_t* L_24 = WWW_get_error_m218(L_23, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_24, /*hidden argument*/NULL);
	}

IL_013a:
	{
		__this->___U24PC_6 = (-1);
	}

IL_0141:
	{
		return 0;
	}

IL_0143:
	{
		return 1;
	}
	// Dead block : IL_0145: ldloc.1
}
// System.Void PlayerTwo/<SendPlayer2Position>c__IteratorF::Dispose()
extern "C" void U3CSendPlayer2PositionU3Ec__IteratorF_Dispose_m163 (U3CSendPlayer2PositionU3Ec__IteratorF_t43 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_6 = (-1);
		return;
	}
}
// System.Void PlayerTwo/<SendPlayer2Position>c__IteratorF::Reset()
extern TypeInfo* NotSupportedException_t55_il2cpp_TypeInfo_var;
extern "C" void U3CSendPlayer2PositionU3Ec__IteratorF_Reset_m164 (U3CSendPlayer2PositionU3Ec__IteratorF_t43 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t55 * L_0 = (NotSupportedException_t55 *)il2cpp_codegen_object_new (NotSupportedException_t55_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m208(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void PlayerTwo/<GetPlayer2Position>c__Iterator10::.ctor()
extern "C" void U3CGetPlayer2PositionU3Ec__Iterator10__ctor_m165 (U3CGetPlayer2PositionU3Ec__Iterator10_t44 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object PlayerTwo/<GetPlayer2Position>c__Iterator10::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CGetPlayer2PositionU3Ec__Iterator10_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m166 (U3CGetPlayer2PositionU3Ec__Iterator10_t44 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_8);
		return L_0;
	}
}
// System.Object PlayerTwo/<GetPlayer2Position>c__Iterator10::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetPlayer2PositionU3Ec__Iterator10_System_Collections_IEnumerator_get_Current_m167 (U3CGetPlayer2PositionU3Ec__Iterator10_t44 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_8);
		return L_0;
	}
}
// System.Boolean PlayerTwo/<GetPlayer2Position>c__Iterator10::MoveNext()
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t58_il2cpp_TypeInfo_var;
extern TypeInfo* GameManager_t22_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral2;
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral90;
extern Il2CppCodeGenString* _stringLiteral38;
extern Il2CppCodeGenString* _stringLiteral91;
extern Il2CppCodeGenString* _stringLiteral85;
extern "C" bool U3CGetPlayer2PositionU3Ec__Iterator10_MoveNext_m168 (U3CGetPlayer2PositionU3Ec__Iterator10_t44 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		CharU5BU5D_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		GameManager_t22_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(11);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral90 = il2cpp_codegen_string_literal_from_index(90);
		_stringLiteral38 = il2cpp_codegen_string_literal_from_index(38);
		_stringLiteral91 = il2cpp_codegen_string_literal_from_index(91);
		_stringLiteral85 = il2cpp_codegen_string_literal_from_index(85);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_7);
		V_0 = L_0;
		__this->___U24PC_7 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0074;
		}
	}
	{
		goto IL_0146;
	}

IL_0021:
	{
		WWWForm_t8 * L_2 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_2, /*hidden argument*/NULL);
		__this->___U3CformU3E__0_0 = L_2;
		WWWForm_t8 * L_3 = (__this->___U3CformU3E__0_0);
		String_t* L_4 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral1, /*hidden argument*/NULL);
		NullCheck(L_3);
		WWWForm_AddField_m203(L_3, _stringLiteral2, L_4, /*hidden argument*/NULL);
		WWWForm_t8 * L_5 = (__this->___U3CformU3E__0_0);
		WWW_t9 * L_6 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_6, _stringLiteral90, L_5, /*hidden argument*/NULL);
		__this->___U3CwU3E__1_1 = L_6;
		WWW_t9 * L_7 = (__this->___U3CwU3E__1_1);
		__this->___U24current_8 = L_7;
		__this->___U24PC_7 = 1;
		goto IL_0148;
	}

IL_0074:
	{
		WWW_t9 * L_8 = (__this->___U3CwU3E__1_1);
		NullCheck(L_8);
		String_t* L_9 = WWW_get_text_m206(L_8, /*hidden argument*/NULL);
		__this->___U3CJsonStringU3E__2_2 = L_9;
		String_t* L_10 = (__this->___U3CJsonStringU3E__2_2);
		CharU5BU5D_t58* L_11 = ((CharU5BU5D_t58*)SZArrayNew(CharU5BU5D_t58_il2cpp_TypeInfo_var, 1));
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_11, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)45);
		NullCheck(L_10);
		StringU5BU5D_t16* L_12 = String_Split_m219(L_10, L_11, /*hidden argument*/NULL);
		__this->___U3CarrayU3E__3_3 = L_12;
		StringU5BU5D_t16* L_13 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		int32_t L_14 = 0;
		float L_15 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_13, L_14, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CxU3E__4_4 = L_15;
		StringU5BU5D_t16* L_16 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, 1);
		int32_t L_17 = 1;
		float L_18 = Single_Parse_m243(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_16, L_17, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CyU3E__5_5 = L_18;
		StringU5BU5D_t16* L_19 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_19);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_19, 2);
		int32_t L_20 = 2;
		int32_t L_21 = Int32_Parse_m244(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_19, L_20, sizeof(String_t*))), /*hidden argument*/NULL);
		__this->___U3CpU3E__6_6 = L_21;
		int32_t L_22 = (__this->___U3CpU3E__6_6);
		IL2CPP_RUNTIME_CLASS_INIT(GameManager_t22_il2cpp_TypeInfo_var);
		((GameManager_t22_StaticFields*)GameManager_t22_il2cpp_TypeInfo_var->static_fields)->___playerPosition_22 = L_22;
		int32_t L_23 = (__this->___U3CpU3E__6_6);
		PlayerPrefs_SetInt_m246(NULL /*static, unused*/, _stringLiteral38, L_23, /*hidden argument*/NULL);
		PlayerTwo_t12 * L_24 = (__this->___U3CU3Ef__this_9);
		NullCheck(L_24);
		Transform_t3 * L_25 = Component_get_transform_m224(L_24, /*hidden argument*/NULL);
		float L_26 = (__this->___U3CxU3E__4_4);
		float L_27 = (__this->___U3CyU3E__5_5);
		Vector3_t35  L_28 = {0};
		Vector3__ctor_m192(&L_28, L_26, L_27, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_25);
		Transform_set_position_m245(L_25, L_28, /*hidden argument*/NULL);
		StringU5BU5D_t16* L_29 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_29);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_29, 0);
		int32_t L_30 = 0;
		StringU5BU5D_t16* L_31 = (__this->___U3CarrayU3E__3_3);
		NullCheck(L_31);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_31, 1);
		int32_t L_32 = 1;
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_33 = String_Concat_m221(NULL /*static, unused*/, _stringLiteral91, (*(String_t**)(String_t**)SZArrayLdElema(L_29, L_30, sizeof(String_t*))), _stringLiteral85, (*(String_t**)(String_t**)SZArrayLdElema(L_31, L_32, sizeof(String_t*))), /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_33, /*hidden argument*/NULL);
		__this->___U24PC_7 = (-1);
	}

IL_0146:
	{
		return 0;
	}

IL_0148:
	{
		return 1;
	}
	// Dead block : IL_014a: ldloc.1
}
// System.Void PlayerTwo/<GetPlayer2Position>c__Iterator10::Dispose()
extern "C" void U3CGetPlayer2PositionU3Ec__Iterator10_Dispose_m169 (U3CGetPlayer2PositionU3Ec__Iterator10_t44 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_7 = (-1);
		return;
	}
}
// System.Void PlayerTwo/<GetPlayer2Position>c__Iterator10::Reset()
extern TypeInfo* NotSupportedException_t55_il2cpp_TypeInfo_var;
extern "C" void U3CGetPlayer2PositionU3Ec__Iterator10_Reset_m170 (U3CGetPlayer2PositionU3Ec__Iterator10_t44 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t55 * L_0 = (NotSupportedException_t55 *)il2cpp_codegen_object_new (NotSupportedException_t55_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m208(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void PlayerTwo::.ctor()
extern "C" void PlayerTwo__ctor_m171 (PlayerTwo_t12 * __this, const MethodInfo* method)
{
	{
		MovingObject__ctor_m127(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerTwo::Start()
extern "C" void PlayerTwo_Start_m172 (PlayerTwo_t12 * __this, const MethodInfo* method)
{
	{
		MovingObject_Start_m128(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerTwo::MoveThePlayer(System.Int32,System.Int32,System.Int32,System.Int32,System.Int32,System.Int32)
extern "C" void PlayerTwo_MoveThePlayer_m173 (PlayerTwo_t12 * __this, int32_t ___x, int32_t ___y, int32_t ___a, int32_t ___b, int32_t ___playerPosition, int32_t ___diceNumber, const MethodInfo* method)
{
	{
		int32_t L_0 = ___x;
		int32_t L_1 = ___y;
		VirtActionInvoker2< int32_t, int32_t >::Invoke(5 /* System.Void PlayerTwo::AttemptMove(System.Int32,System.Int32) */, __this, L_0, L_1);
		int32_t L_2 = ___a;
		int32_t L_3 = ___b;
		int32_t L_4 = ___playerPosition;
		int32_t L_5 = ___diceNumber;
		Object_t * L_6 = PlayerTwo_SendPlayer2Position_m175(__this, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m207(__this, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void PlayerTwo::AttemptMove(System.Int32,System.Int32)
extern "C" void PlayerTwo_AttemptMove_m174 (PlayerTwo_t12 * __this, int32_t ___xDir, int32_t ___yDir, const MethodInfo* method)
{
	{
		int32_t L_0 = ___xDir;
		int32_t L_1 = ___yDir;
		MovingObject_AttemptMove_m131(__this, L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator PlayerTwo::SendPlayer2Position(System.Int32,System.Int32,System.Int32,System.Int32)
extern TypeInfo* U3CSendPlayer2PositionU3Ec__IteratorF_t43_il2cpp_TypeInfo_var;
extern "C" Object_t * PlayerTwo_SendPlayer2Position_m175 (PlayerTwo_t12 * __this, int32_t ___a, int32_t ___b, int32_t ___playerPosition, int32_t ___diceNumber, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CSendPlayer2PositionU3Ec__IteratorF_t43_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	U3CSendPlayer2PositionU3Ec__IteratorF_t43 * V_0 = {0};
	{
		U3CSendPlayer2PositionU3Ec__IteratorF_t43 * L_0 = (U3CSendPlayer2PositionU3Ec__IteratorF_t43 *)il2cpp_codegen_object_new (U3CSendPlayer2PositionU3Ec__IteratorF_t43_il2cpp_TypeInfo_var);
		U3CSendPlayer2PositionU3Ec__IteratorF__ctor_m159(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CSendPlayer2PositionU3Ec__IteratorF_t43 * L_1 = V_0;
		int32_t L_2 = ___a;
		NullCheck(L_1);
		L_1->___a_1 = L_2;
		U3CSendPlayer2PositionU3Ec__IteratorF_t43 * L_3 = V_0;
		int32_t L_4 = ___b;
		NullCheck(L_3);
		L_3->___b_2 = L_4;
		U3CSendPlayer2PositionU3Ec__IteratorF_t43 * L_5 = V_0;
		int32_t L_6 = ___diceNumber;
		NullCheck(L_5);
		L_5->___diceNumber_3 = L_6;
		U3CSendPlayer2PositionU3Ec__IteratorF_t43 * L_7 = V_0;
		int32_t L_8 = ___playerPosition;
		NullCheck(L_7);
		L_7->___playerPosition_4 = L_8;
		U3CSendPlayer2PositionU3Ec__IteratorF_t43 * L_9 = V_0;
		int32_t L_10 = ___a;
		NullCheck(L_9);
		L_9->___U3CU24U3Ea_8 = L_10;
		U3CSendPlayer2PositionU3Ec__IteratorF_t43 * L_11 = V_0;
		int32_t L_12 = ___b;
		NullCheck(L_11);
		L_11->___U3CU24U3Eb_9 = L_12;
		U3CSendPlayer2PositionU3Ec__IteratorF_t43 * L_13 = V_0;
		int32_t L_14 = ___diceNumber;
		NullCheck(L_13);
		L_13->___U3CU24U3EdiceNumber_10 = L_14;
		U3CSendPlayer2PositionU3Ec__IteratorF_t43 * L_15 = V_0;
		int32_t L_16 = ___playerPosition;
		NullCheck(L_15);
		L_15->___U3CU24U3EplayerPosition_11 = L_16;
		U3CSendPlayer2PositionU3Ec__IteratorF_t43 * L_17 = V_0;
		return L_17;
	}
}
// System.Void PlayerTwo::ChangeTransform(System.Single,System.Single)
extern Il2CppCodeGenString* _stringLiteral92;
extern "C" void PlayerTwo_ChangeTransform_m176 (PlayerTwo_t12 * __this, float ___x, float ___y, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral92 = il2cpp_codegen_string_literal_from_index(92);
		s_Il2CppMethodIntialized = true;
	}
	{
		Debug_Log_m202(NULL /*static, unused*/, _stringLiteral92, /*hidden argument*/NULL);
		Transform_t3 * L_0 = Component_get_transform_m224(__this, /*hidden argument*/NULL);
		float L_1 = ___x;
		float L_2 = ___y;
		Vector3_t35  L_3 = {0};
		Vector3__ctor_m192(&L_3, L_1, L_2, (0.0f), /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m245(L_0, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator PlayerTwo::GetPlayer2Position()
extern TypeInfo* U3CGetPlayer2PositionU3Ec__Iterator10_t44_il2cpp_TypeInfo_var;
extern "C" Object_t * PlayerTwo_GetPlayer2Position_m177 (PlayerTwo_t12 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CGetPlayer2PositionU3Ec__Iterator10_t44_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		s_Il2CppMethodIntialized = true;
	}
	U3CGetPlayer2PositionU3Ec__Iterator10_t44 * V_0 = {0};
	{
		U3CGetPlayer2PositionU3Ec__Iterator10_t44 * L_0 = (U3CGetPlayer2PositionU3Ec__Iterator10_t44 *)il2cpp_codegen_object_new (U3CGetPlayer2PositionU3Ec__Iterator10_t44_il2cpp_TypeInfo_var);
		U3CGetPlayer2PositionU3Ec__Iterator10__ctor_m165(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CGetPlayer2PositionU3Ec__Iterator10_t44 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_9 = __this;
		U3CGetPlayer2PositionU3Ec__Iterator10_t44 * L_2 = V_0;
		return L_2;
	}
}
// System.Void ScrollItem/<CreateGame>c__Iterator11::.ctor()
extern "C" void U3CCreateGameU3Ec__Iterator11__ctor_m178 (U3CCreateGameU3Ec__Iterator11_t45 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m199(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object ScrollItem/<CreateGame>c__Iterator11::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CCreateGameU3Ec__Iterator11_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m179 (U3CCreateGameU3Ec__Iterator11_t45 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_6);
		return L_0;
	}
}
// System.Object ScrollItem/<CreateGame>c__Iterator11::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCreateGameU3Ec__Iterator11_System_Collections_IEnumerator_get_Current_m180 (U3CCreateGameU3Ec__Iterator11_t45 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_6);
		return L_0;
	}
}
// System.Boolean ScrollItem/<CreateGame>c__Iterator11::MoveNext()
extern TypeInfo* WWWForm_t8_il2cpp_TypeInfo_var;
extern TypeInfo* WWW_t9_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern TypeInfo* CharU5BU5D_t58_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral93;
extern Il2CppCodeGenString* _stringLiteral11;
extern Il2CppCodeGenString* _stringLiteral77;
extern Il2CppCodeGenString* _stringLiteral94;
extern Il2CppCodeGenString* _stringLiteral95;
extern Il2CppCodeGenString* _stringLiteral7;
extern Il2CppCodeGenString* _stringLiteral3;
extern Il2CppCodeGenString* _stringLiteral4;
extern Il2CppCodeGenString* _stringLiteral1;
extern Il2CppCodeGenString* _stringLiteral74;
extern "C" bool U3CCreateGameU3Ec__Iterator11_MoveNext_m181 (U3CCreateGameU3Ec__Iterator11_t45 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WWWForm_t8_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		WWW_t9_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		CharU5BU5D_t58_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		_stringLiteral93 = il2cpp_codegen_string_literal_from_index(93);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		_stringLiteral77 = il2cpp_codegen_string_literal_from_index(77);
		_stringLiteral94 = il2cpp_codegen_string_literal_from_index(94);
		_stringLiteral95 = il2cpp_codegen_string_literal_from_index(95);
		_stringLiteral7 = il2cpp_codegen_string_literal_from_index(7);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		_stringLiteral4 = il2cpp_codegen_string_literal_from_index(4);
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		_stringLiteral74 = il2cpp_codegen_string_literal_from_index(74);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_5);
		V_0 = L_0;
		__this->___U24PC_5 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_008a;
		}
	}
	{
		goto IL_013f;
	}

IL_0021:
	{
		WWWForm_t8 * L_2 = (WWWForm_t8 *)il2cpp_codegen_object_new (WWWForm_t8_il2cpp_TypeInfo_var);
		WWWForm__ctor_m200(L_2, /*hidden argument*/NULL);
		__this->___U3CformU3E__0_0 = L_2;
		WWWForm_t8 * L_3 = (__this->___U3CformU3E__0_0);
		String_t* L_4 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral11, /*hidden argument*/NULL);
		NullCheck(L_3);
		WWWForm_AddField_m203(L_3, _stringLiteral93, L_4, /*hidden argument*/NULL);
		WWWForm_t8 * L_5 = (__this->___U3CformU3E__0_0);
		String_t* L_6 = (__this->___player2Name_1);
		NullCheck(L_5);
		WWWForm_AddField_m203(L_5, _stringLiteral77, L_6, /*hidden argument*/NULL);
		WWWForm_t8 * L_7 = (__this->___U3CformU3E__0_0);
		WWW_t9 * L_8 = (WWW_t9 *)il2cpp_codegen_object_new (WWW_t9_il2cpp_TypeInfo_var);
		WWW__ctor_m205(L_8, _stringLiteral94, L_7, /*hidden argument*/NULL);
		__this->___U3CwU3E__1_2 = L_8;
		WWW_t9 * L_9 = (__this->___U3CwU3E__1_2);
		__this->___U24current_6 = L_9;
		__this->___U24PC_5 = 1;
		goto IL_0141;
	}

IL_008a:
	{
		WWW_t9 * L_10 = (__this->___U3CwU3E__1_2);
		NullCheck(L_10);
		String_t* L_11 = WWW_get_error_m218(L_10, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_0128;
		}
	}
	{
		WWW_t9 * L_12 = (__this->___U3CwU3E__1_2);
		NullCheck(L_12);
		String_t* L_13 = WWW_get_text_m206(L_12, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_14 = String_Concat_m217(NULL /*static, unused*/, _stringLiteral95, L_13, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_14, /*hidden argument*/NULL);
		WWW_t9 * L_15 = (__this->___U3CwU3E__1_2);
		NullCheck(L_15);
		String_t* L_16 = WWW_get_text_m206(L_15, /*hidden argument*/NULL);
		__this->___U3CJsonStringU3E__2_3 = L_16;
		String_t* L_17 = (__this->___U3CJsonStringU3E__2_3);
		CharU5BU5D_t58* L_18 = ((CharU5BU5D_t58*)SZArrayNew(CharU5BU5D_t58_il2cpp_TypeInfo_var, 1));
		NullCheck(L_18);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_18, 0);
		*((uint16_t*)(uint16_t*)SZArrayLdElema(L_18, 0, sizeof(uint16_t))) = (uint16_t)((int32_t)45);
		NullCheck(L_17);
		StringU5BU5D_t16* L_19 = String_Split_m219(L_17, L_18, /*hidden argument*/NULL);
		__this->___U3CarrayU3E__3_4 = L_19;
		StringU5BU5D_t16* L_20 = (__this->___U3CarrayU3E__3_4);
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, 0);
		int32_t L_21 = 0;
		bool L_22 = String_op_Equality_m204(NULL /*static, unused*/, (*(String_t**)(String_t**)SZArrayLdElema(L_20, L_21, sizeof(String_t*))), _stringLiteral7, /*hidden argument*/NULL);
		if (!L_22)
		{
			goto IL_0123;
		}
	}
	{
		PlayerPrefs_SetString_m220(NULL /*static, unused*/, _stringLiteral3, _stringLiteral4, /*hidden argument*/NULL);
		StringU5BU5D_t16* L_23 = (__this->___U3CarrayU3E__3_4);
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, 1);
		int32_t L_24 = 1;
		PlayerPrefs_SetString_m220(NULL /*static, unused*/, _stringLiteral1, (*(String_t**)(String_t**)SZArrayLdElema(L_23, L_24, sizeof(String_t*))), /*hidden argument*/NULL);
		Application_LoadLevel_m241(NULL /*static, unused*/, _stringLiteral74, /*hidden argument*/NULL);
	}

IL_0123:
	{
		goto IL_0138;
	}

IL_0128:
	{
		WWW_t9 * L_25 = (__this->___U3CwU3E__1_2);
		NullCheck(L_25);
		String_t* L_26 = WWW_get_error_m218(L_25, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_26, /*hidden argument*/NULL);
	}

IL_0138:
	{
		__this->___U24PC_5 = (-1);
	}

IL_013f:
	{
		return 0;
	}

IL_0141:
	{
		return 1;
	}
	// Dead block : IL_0143: ldloc.1
}
// System.Void ScrollItem/<CreateGame>c__Iterator11::Dispose()
extern "C" void U3CCreateGameU3Ec__Iterator11_Dispose_m182 (U3CCreateGameU3Ec__Iterator11_t45 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_5 = (-1);
		return;
	}
}
// System.Void ScrollItem/<CreateGame>c__Iterator11::Reset()
extern TypeInfo* NotSupportedException_t55_il2cpp_TypeInfo_var;
extern "C" void U3CCreateGameU3Ec__Iterator11_Reset_m183 (U3CCreateGameU3Ec__Iterator11_t45 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t55_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t55 * L_0 = (NotSupportedException_t55 *)il2cpp_codegen_object_new (NotSupportedException_t55_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m208(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void ScrollItem::.ctor()
extern "C" void ScrollItem__ctor_m184 (ScrollItem_t46 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void ScrollItem::OnEnable()
extern "C" void ScrollItem_OnEnable_m185 (ScrollItem_t46 * __this, const MethodInfo* method)
{
	{
		Text_t47 * L_0 = (__this->___indexText_2);
		Transform_t3 * L_1 = Component_get_transform_m224(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = Object_get_name_m271(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(48 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_0, L_2);
		return;
	}
}
// System.Collections.IEnumerator ScrollItem::CreateGame(System.String)
extern TypeInfo* U3CCreateGameU3Ec__Iterator11_t45_il2cpp_TypeInfo_var;
extern "C" Object_t * ScrollItem_CreateGame_m186 (ScrollItem_t46 * __this, String_t* ___player2Name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CCreateGameU3Ec__Iterator11_t45_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(42);
		s_Il2CppMethodIntialized = true;
	}
	U3CCreateGameU3Ec__Iterator11_t45 * V_0 = {0};
	{
		U3CCreateGameU3Ec__Iterator11_t45 * L_0 = (U3CCreateGameU3Ec__Iterator11_t45 *)il2cpp_codegen_object_new (U3CCreateGameU3Ec__Iterator11_t45_il2cpp_TypeInfo_var);
		U3CCreateGameU3Ec__Iterator11__ctor_m178(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CCreateGameU3Ec__Iterator11_t45 * L_1 = V_0;
		String_t* L_2 = ___player2Name;
		NullCheck(L_1);
		L_1->___player2Name_1 = L_2;
		U3CCreateGameU3Ec__Iterator11_t45 * L_3 = V_0;
		String_t* L_4 = ___player2Name;
		NullCheck(L_3);
		L_3->___U3CU24U3Eplayer2Name_7 = L_4;
		U3CCreateGameU3Ec__Iterator11_t45 * L_5 = V_0;
		return L_5;
	}
}
// System.Void ScrollItem::OnRemoveMe()
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral19;
extern Il2CppCodeGenString* _stringLiteral20;
extern Il2CppCodeGenString* _stringLiteral24;
extern Il2CppCodeGenString* _stringLiteral74;
extern "C" void ScrollItem_OnRemoveMe_m187 (ScrollItem_t46 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(3);
		_stringLiteral19 = il2cpp_codegen_string_literal_from_index(19);
		_stringLiteral20 = il2cpp_codegen_string_literal_from_index(20);
		_stringLiteral24 = il2cpp_codegen_string_literal_from_index(24);
		_stringLiteral74 = il2cpp_codegen_string_literal_from_index(74);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	{
		GameObject_t5 * L_0 = Component_get_gameObject_m226(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		String_t* L_1 = Object_get_name_m271(L_0, /*hidden argument*/NULL);
		Debug_Log_m202(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		GameObject_t5 * L_2 = Component_get_gameObject_m226(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		String_t* L_3 = Object_get_name_m271(L_2, /*hidden argument*/NULL);
		V_0 = L_3;
		String_t* L_4 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_5 = String_op_Equality_m204(NULL /*static, unused*/, L_4, _stringLiteral20, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0048;
		}
	}
	{
		String_t* L_6 = V_0;
		Object_t * L_7 = ScrollItem_CreateGame_m186(__this, L_6, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m207(__this, L_7, /*hidden argument*/NULL);
		goto IL_006b;
	}

IL_0048:
	{
		String_t* L_8 = PlayerPrefs_GetString_m201(NULL /*static, unused*/, _stringLiteral19, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_9 = String_op_Equality_m204(NULL /*static, unused*/, L_8, _stringLiteral24, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_006b;
		}
	}
	{
		Application_LoadLevel_m241(NULL /*static, unused*/, _stringLiteral74, /*hidden argument*/NULL);
	}

IL_006b:
	{
		return;
	}
}
// System.Void Wall::.ctor()
extern "C" void Wall__ctor_m188 (Wall_t48 * __this, const MethodInfo* method)
{
	{
		__this->___hp_3 = 3;
		MonoBehaviour__ctor_m191(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Wall::Awake()
extern const MethodInfo* Component_GetComponent_TisSpriteRenderer_t50_m272_MethodInfo_var;
extern "C" void Wall_Awake_m189 (Wall_t48 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisSpriteRenderer_t50_m272_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483657);
		s_Il2CppMethodIntialized = true;
	}
	{
		SpriteRenderer_t50 * L_0 = Component_GetComponent_TisSpriteRenderer_t50_m272(__this, /*hidden argument*/Component_GetComponent_TisSpriteRenderer_t50_m272_MethodInfo_var);
		__this->___spriteRenderer_4 = L_0;
		return;
	}
}
// System.Void Wall::DamageWall(System.Int32)
extern "C" void Wall_DamageWall_m190 (Wall_t48 * __this, int32_t ___loss, const MethodInfo* method)
{
	{
		SpriteRenderer_t50 * L_0 = (__this->___spriteRenderer_4);
		Sprite_t49 * L_1 = (__this->___dmgSprite_2);
		NullCheck(L_0);
		SpriteRenderer_set_sprite_m273(L_0, L_1, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___hp_3);
		int32_t L_3 = ___loss;
		__this->___hp_3 = ((int32_t)((int32_t)L_2-(int32_t)L_3));
		int32_t L_4 = (__this->___hp_3);
		if ((((int32_t)L_4) > ((int32_t)0)))
		{
			goto IL_0037;
		}
	}
	{
		GameObject_t5 * L_5 = Component_get_gameObject_m226(__this, /*hidden argument*/NULL);
		NullCheck(L_5);
		GameObject_SetActive_m238(L_5, 0, /*hidden argument*/NULL);
	}

IL_0037:
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
