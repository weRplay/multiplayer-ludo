﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_58.h"

// System.Void System.Array/InternalEnumerator`1<System.UInt16>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17106_gshared (InternalEnumerator_1_t2285 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17106(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2285 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17106_gshared)(__this, ___array, method)
// System.Object System.Array/InternalEnumerator`1<System.UInt16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17107_gshared (InternalEnumerator_1_t2285 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17107(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2285 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17107_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.UInt16>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17108_gshared (InternalEnumerator_1_t2285 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17108(__this, method) (( void (*) (InternalEnumerator_1_t2285 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17108_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.UInt16>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17109_gshared (InternalEnumerator_1_t2285 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17109(__this, method) (( bool (*) (InternalEnumerator_1_t2285 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17109_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.UInt16>::get_Current()
extern "C" uint16_t InternalEnumerator_1_get_Current_m17110_gshared (InternalEnumerator_1_t2285 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17110(__this, method) (( uint16_t (*) (InternalEnumerator_1_t2285 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17110_gshared)(__this, method)
