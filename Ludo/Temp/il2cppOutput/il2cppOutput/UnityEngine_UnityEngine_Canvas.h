﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t311;

#include "UnityEngine_UnityEngine_Behaviour.h"

// UnityEngine.Canvas
struct  Canvas_t148  : public Behaviour_t354
{
};
struct Canvas_t148_StaticFields{
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t311 * ___willRenderCanvases_2;
};
