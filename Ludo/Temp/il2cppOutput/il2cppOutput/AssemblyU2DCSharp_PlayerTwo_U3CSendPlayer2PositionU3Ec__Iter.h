﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t8;
// UnityEngine.WWW
struct WWW_t9;
// System.Object
struct Object_t;

#include "mscorlib_System_Object.h"

// PlayerTwo/<SendPlayer2Position>c__IteratorF
struct  U3CSendPlayer2PositionU3Ec__IteratorF_t43  : public Object_t
{
	// UnityEngine.WWWForm PlayerTwo/<SendPlayer2Position>c__IteratorF::<form>__0
	WWWForm_t8 * ___U3CformU3E__0_0;
	// System.Int32 PlayerTwo/<SendPlayer2Position>c__IteratorF::a
	int32_t ___a_1;
	// System.Int32 PlayerTwo/<SendPlayer2Position>c__IteratorF::b
	int32_t ___b_2;
	// System.Int32 PlayerTwo/<SendPlayer2Position>c__IteratorF::diceNumber
	int32_t ___diceNumber_3;
	// System.Int32 PlayerTwo/<SendPlayer2Position>c__IteratorF::playerPosition
	int32_t ___playerPosition_4;
	// UnityEngine.WWW PlayerTwo/<SendPlayer2Position>c__IteratorF::<w>__1
	WWW_t9 * ___U3CwU3E__1_5;
	// System.Int32 PlayerTwo/<SendPlayer2Position>c__IteratorF::$PC
	int32_t ___U24PC_6;
	// System.Object PlayerTwo/<SendPlayer2Position>c__IteratorF::$current
	Object_t * ___U24current_7;
	// System.Int32 PlayerTwo/<SendPlayer2Position>c__IteratorF::<$>a
	int32_t ___U3CU24U3Ea_8;
	// System.Int32 PlayerTwo/<SendPlayer2Position>c__IteratorF::<$>b
	int32_t ___U3CU24U3Eb_9;
	// System.Int32 PlayerTwo/<SendPlayer2Position>c__IteratorF::<$>diceNumber
	int32_t ___U3CU24U3EdiceNumber_10;
	// System.Int32 PlayerTwo/<SendPlayer2Position>c__IteratorF::<$>playerPosition
	int32_t ___U3CU24U3EplayerPosition_11;
};
