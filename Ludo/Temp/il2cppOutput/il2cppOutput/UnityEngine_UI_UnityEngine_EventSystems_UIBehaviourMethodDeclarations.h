﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.EventSystems.UIBehaviour
struct UIBehaviour_t68;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.EventSystems.UIBehaviour::.ctor()
extern "C" void UIBehaviour__ctor_m374 (UIBehaviour_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::Awake()
extern "C" void UIBehaviour_Awake_m375 (UIBehaviour_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnEnable()
extern "C" void UIBehaviour_OnEnable_m376 (UIBehaviour_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::Start()
extern "C" void UIBehaviour_Start_m377 (UIBehaviour_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnDisable()
extern "C" void UIBehaviour_OnDisable_m378 (UIBehaviour_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnDestroy()
extern "C" void UIBehaviour_OnDestroy_m379 (UIBehaviour_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.UIBehaviour::IsActive()
extern "C" bool UIBehaviour_IsActive_m380 (UIBehaviour_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnRectTransformDimensionsChange()
extern "C" void UIBehaviour_OnRectTransformDimensionsChange_m381 (UIBehaviour_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnBeforeTransformParentChanged()
extern "C" void UIBehaviour_OnBeforeTransformParentChanged_m382 (UIBehaviour_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnTransformParentChanged()
extern "C" void UIBehaviour_OnTransformParentChanged_m383 (UIBehaviour_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnDidApplyAnimationProperties()
extern "C" void UIBehaviour_OnDidApplyAnimationProperties_m384 (UIBehaviour_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnCanvasGroupChanged()
extern "C" void UIBehaviour_OnCanvasGroupChanged_m385 (UIBehaviour_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.EventSystems.UIBehaviour::OnCanvasHierarchyChanged()
extern "C" void UIBehaviour_OnCanvasHierarchyChanged_m386 (UIBehaviour_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.EventSystems.UIBehaviour::IsDestroyed()
extern "C" bool UIBehaviour_IsDestroyed_m387 (UIBehaviour_t68 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
