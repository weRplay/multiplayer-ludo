﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.SpriteRenderer
struct SpriteRenderer_t50;
// UnityEngine.Sprite
struct Sprite_t49;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.SpriteRenderer::set_sprite(UnityEngine.Sprite)
extern "C" void SpriteRenderer_set_sprite_m273 (SpriteRenderer_t50 * __this, Sprite_t49 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.SpriteRenderer::SetSprite_INTERNAL(UnityEngine.Sprite)
extern "C" void SpriteRenderer_SetSprite_INTERNAL_m2487 (SpriteRenderer_t50 * __this, Sprite_t49 * ___sprite, const MethodInfo* method) IL2CPP_METHOD_ATTR;
