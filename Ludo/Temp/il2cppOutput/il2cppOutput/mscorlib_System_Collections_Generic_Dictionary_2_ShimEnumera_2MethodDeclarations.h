﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>
struct ShimEnumerator_t2129;
// System.Collections.Generic.Dictionary`2<System.Object,System.Int64>
struct Dictionary_2_t2116;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m15247_gshared (ShimEnumerator_t2129 * __this, Dictionary_2_t2116 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m15247(__this, ___host, method) (( void (*) (ShimEnumerator_t2129 *, Dictionary_2_t2116 *, const MethodInfo*))ShimEnumerator__ctor_m15247_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m15248_gshared (ShimEnumerator_t2129 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m15248(__this, method) (( bool (*) (ShimEnumerator_t2129 *, const MethodInfo*))ShimEnumerator_MoveNext_m15248_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Entry()
extern "C" DictionaryEntry_t1068  ShimEnumerator_get_Entry_m15249_gshared (ShimEnumerator_t2129 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m15249(__this, method) (( DictionaryEntry_t1068  (*) (ShimEnumerator_t2129 *, const MethodInfo*))ShimEnumerator_get_Entry_m15249_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m15250_gshared (ShimEnumerator_t2129 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m15250(__this, method) (( Object_t * (*) (ShimEnumerator_t2129 *, const MethodInfo*))ShimEnumerator_get_Key_m15250_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m15251_gshared (ShimEnumerator_t2129 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m15251(__this, method) (( Object_t * (*) (ShimEnumerator_t2129 *, const MethodInfo*))ShimEnumerator_get_Value_m15251_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,System.Int64>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m15252_gshared (ShimEnumerator_t2129 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m15252(__this, method) (( Object_t * (*) (ShimEnumerator_t2129 *, const MethodInfo*))ShimEnumerator_get_Current_m15252_gshared)(__this, method)
