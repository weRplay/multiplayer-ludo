﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1875;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_KeyCollecti_1.h"

// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m11805_gshared (Enumerator_t1881 * __this, Dictionary_2_t1875 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m11805(__this, ___host, method) (( void (*) (Enumerator_t1881 *, Dictionary_2_t1875 *, const MethodInfo*))Enumerator__ctor_m11805_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m11806_gshared (Enumerator_t1881 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m11806(__this, method) (( Object_t * (*) (Enumerator_t1881 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11806_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m11807_gshared (Enumerator_t1881 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m11807(__this, method) (( void (*) (Enumerator_t1881 *, const MethodInfo*))Enumerator_Dispose_m11807_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m11808_gshared (Enumerator_t1881 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m11808(__this, method) (( bool (*) (Enumerator_t1881 *, const MethodInfo*))Enumerator_MoveNext_m11808_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/KeyCollection/Enumerator<System.Int32,System.Object>::get_Current()
extern "C" int32_t Enumerator_get_Current_m11809_gshared (Enumerator_t1881 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m11809(__this, method) (( int32_t (*) (Enumerator_t1881 *, const MethodInfo*))Enumerator_get_Current_m11809_gshared)(__this, method)
