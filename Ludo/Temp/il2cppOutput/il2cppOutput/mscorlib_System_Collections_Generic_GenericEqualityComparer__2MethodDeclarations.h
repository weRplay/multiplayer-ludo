﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>
struct GenericEqualityComparer_1_t1781;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_TimeSpan.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m10441_gshared (GenericEqualityComparer_1_t1781 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m10441(__this, method) (( void (*) (GenericEqualityComparer_1_t1781 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m10441_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m17888_gshared (GenericEqualityComparer_1_t1781 * __this, TimeSpan_t977  ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m17888(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t1781 *, TimeSpan_t977 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m17888_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.TimeSpan>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m17889_gshared (GenericEqualityComparer_1_t1781 * __this, TimeSpan_t977  ___x, TimeSpan_t977  ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m17889(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t1781 *, TimeSpan_t977 , TimeSpan_t977 , const MethodInfo*))GenericEqualityComparer_1_Equals_m17889_gshared)(__this, ___x, ___y, method)
