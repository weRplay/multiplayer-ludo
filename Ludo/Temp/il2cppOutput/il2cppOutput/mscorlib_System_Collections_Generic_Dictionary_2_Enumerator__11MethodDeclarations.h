﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m14370(__this, ___dictionary, method) (( void (*) (Enumerator_t2071 *, Dictionary_2_t417 *, const MethodInfo*))Enumerator__ctor_m12153_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14371(__this, method) (( Object_t * (*) (Enumerator_t2071 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12154_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14372(__this, method) (( DictionaryEntry_t1068  (*) (Enumerator_t2071 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m12155_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14373(__this, method) (( Object_t * (*) (Enumerator_t2071 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m12156_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14374(__this, method) (( Object_t * (*) (Enumerator_t2071 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m12157_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m14375(__this, method) (( bool (*) (Enumerator_t2071 *, const MethodInfo*))Enumerator_MoveNext_m12158_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_Current()
#define Enumerator_get_Current_m14376(__this, method) (( KeyValuePair_2_t2068  (*) (Enumerator_t2071 *, const MethodInfo*))Enumerator_get_Current_m12159_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m14377(__this, method) (( String_t* (*) (Enumerator_t2071 *, const MethodInfo*))Enumerator_get_CurrentKey_m12160_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m14378(__this, method) (( int32_t (*) (Enumerator_t2071 *, const MethodInfo*))Enumerator_get_CurrentValue_m12161_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m14379(__this, method) (( void (*) (Enumerator_t2071 *, const MethodInfo*))Enumerator_VerifyState_m12162_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m14380(__this, method) (( void (*) (Enumerator_t2071 *, const MethodInfo*))Enumerator_VerifyCurrent_m12163_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Int32>::Dispose()
#define Enumerator_Dispose_m14381(__this, method) (( void (*) (Enumerator_t2071 *, const MethodInfo*))Enumerator_Dispose_m12164_gshared)(__this, method)
