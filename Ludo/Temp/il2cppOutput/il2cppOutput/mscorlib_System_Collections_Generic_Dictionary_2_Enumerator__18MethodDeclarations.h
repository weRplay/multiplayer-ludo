﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__17MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m16444(__this, ___dictionary, method) (( void (*) (Enumerator_t2216 *, Dictionary_2_t677 *, const MethodInfo*))Enumerator__ctor_m16345_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16445(__this, method) (( Object_t * (*) (Enumerator_t2216 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16346_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16446(__this, method) (( DictionaryEntry_t1068  (*) (Enumerator_t2216 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16347_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16447(__this, method) (( Object_t * (*) (Enumerator_t2216 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16348_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16448(__this, method) (( Object_t * (*) (Enumerator_t2216 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16349_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::MoveNext()
#define Enumerator_MoveNext_m16449(__this, method) (( bool (*) (Enumerator_t2216 *, const MethodInfo*))Enumerator_MoveNext_m16350_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::get_Current()
#define Enumerator_get_Current_m16450(__this, method) (( KeyValuePair_2_t2213  (*) (Enumerator_t2216 *, const MethodInfo*))Enumerator_get_Current_m16351_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m16451(__this, method) (( String_t* (*) (Enumerator_t2216 *, const MethodInfo*))Enumerator_get_CurrentKey_m16352_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m16452(__this, method) (( KeyValuePair_2_t678  (*) (Enumerator_t2216 *, const MethodInfo*))Enumerator_get_CurrentValue_m16353_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::VerifyState()
#define Enumerator_VerifyState_m16453(__this, method) (( void (*) (Enumerator_t2216 *, const MethodInfo*))Enumerator_VerifyState_m16354_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m16454(__this, method) (( void (*) (Enumerator_t2216 *, const MethodInfo*))Enumerator_VerifyCurrent_m16355_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Collections.Generic.KeyValuePair`2<System.Type,SimpleJson.Reflection.ReflectionUtils/SetDelegate>>::Dispose()
#define Enumerator_Dispose_m16455(__this, method) (( void (*) (Enumerator_t2216 *, const MethodInfo*))Enumerator_Dispose_m16356_gshared)(__this, method)
