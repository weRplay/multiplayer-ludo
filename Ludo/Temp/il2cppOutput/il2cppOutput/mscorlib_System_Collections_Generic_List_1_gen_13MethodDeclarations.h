﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_18MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::.ctor()
#define List_1__ctor_m1874(__this, method) (( void (*) (List_1_t212 *, const MethodInfo*))List_1__ctor_m3317_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::.ctor(System.Int32)
#define List_1__ctor_m13392(__this, ___capacity, method) (( void (*) (List_1_t212 *, int32_t, const MethodInfo*))List_1__ctor_m10626_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::.cctor()
#define List_1__cctor_m13393(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m10628_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13394(__this, method) (( Object_t* (*) (List_1_t212 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m10630_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m13395(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t212 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m10632_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m13396(__this, method) (( Object_t * (*) (List_1_t212 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m10634_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m13397(__this, ___item, method) (( int32_t (*) (List_1_t212 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m10636_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m13398(__this, ___item, method) (( bool (*) (List_1_t212 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m10638_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m13399(__this, ___item, method) (( int32_t (*) (List_1_t212 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m10640_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m13400(__this, ___index, ___item, method) (( void (*) (List_1_t212 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m10642_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m13401(__this, ___item, method) (( void (*) (List_1_t212 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m10644_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13402(__this, method) (( bool (*) (List_1_t212 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m10646_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m13403(__this, method) (( bool (*) (List_1_t212 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m10648_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m13404(__this, method) (( Object_t * (*) (List_1_t212 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m10650_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m13405(__this, method) (( bool (*) (List_1_t212 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m10652_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m13406(__this, method) (( bool (*) (List_1_t212 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m10654_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m13407(__this, ___index, method) (( Object_t * (*) (List_1_t212 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m10656_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m13408(__this, ___index, ___value, method) (( void (*) (List_1_t212 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m10658_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::Add(T)
#define List_1_Add_m13409(__this, ___item, method) (( void (*) (List_1_t212 *, CanvasGroup_t317 *, const MethodInfo*))List_1_Add_m10660_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m13410(__this, ___newCount, method) (( void (*) (List_1_t212 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m10662_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m13411(__this, ___collection, method) (( void (*) (List_1_t212 *, Object_t*, const MethodInfo*))List_1_AddCollection_m10664_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m13412(__this, ___enumerable, method) (( void (*) (List_1_t212 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m10666_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m13413(__this, ___collection, method) (( void (*) (List_1_t212 *, Object_t*, const MethodInfo*))List_1_AddRange_m10668_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::AsReadOnly()
#define List_1_AsReadOnly_m13414(__this, method) (( ReadOnlyCollection_1_t2003 * (*) (List_1_t212 *, const MethodInfo*))List_1_AsReadOnly_m10670_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::Clear()
#define List_1_Clear_m13415(__this, method) (( void (*) (List_1_t212 *, const MethodInfo*))List_1_Clear_m10672_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::Contains(T)
#define List_1_Contains_m13416(__this, ___item, method) (( bool (*) (List_1_t212 *, CanvasGroup_t317 *, const MethodInfo*))List_1_Contains_m10674_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m13417(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t212 *, CanvasGroupU5BU5D_t2002*, int32_t, const MethodInfo*))List_1_CopyTo_m10676_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::Find(System.Predicate`1<T>)
#define List_1_Find_m13418(__this, ___match, method) (( CanvasGroup_t317 * (*) (List_1_t212 *, Predicate_1_t2005 *, const MethodInfo*))List_1_Find_m10678_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m13419(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2005 *, const MethodInfo*))List_1_CheckMatch_m10680_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m13420(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t212 *, int32_t, int32_t, Predicate_1_t2005 *, const MethodInfo*))List_1_GetIndex_m10682_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::GetEnumerator()
#define List_1_GetEnumerator_m13421(__this, method) (( Enumerator_t2006  (*) (List_1_t212 *, const MethodInfo*))List_1_GetEnumerator_m10684_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::IndexOf(T)
#define List_1_IndexOf_m13422(__this, ___item, method) (( int32_t (*) (List_1_t212 *, CanvasGroup_t317 *, const MethodInfo*))List_1_IndexOf_m10686_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m13423(__this, ___start, ___delta, method) (( void (*) (List_1_t212 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m10688_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m13424(__this, ___index, method) (( void (*) (List_1_t212 *, int32_t, const MethodInfo*))List_1_CheckIndex_m10690_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::Insert(System.Int32,T)
#define List_1_Insert_m13425(__this, ___index, ___item, method) (( void (*) (List_1_t212 *, int32_t, CanvasGroup_t317 *, const MethodInfo*))List_1_Insert_m10692_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m13426(__this, ___collection, method) (( void (*) (List_1_t212 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m10694_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::Remove(T)
#define List_1_Remove_m13427(__this, ___item, method) (( bool (*) (List_1_t212 *, CanvasGroup_t317 *, const MethodInfo*))List_1_Remove_m10696_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m13428(__this, ___match, method) (( int32_t (*) (List_1_t212 *, Predicate_1_t2005 *, const MethodInfo*))List_1_RemoveAll_m10698_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m13429(__this, ___index, method) (( void (*) (List_1_t212 *, int32_t, const MethodInfo*))List_1_RemoveAt_m10700_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::Reverse()
#define List_1_Reverse_m13430(__this, method) (( void (*) (List_1_t212 *, const MethodInfo*))List_1_Reverse_m10702_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::Sort()
#define List_1_Sort_m13431(__this, method) (( void (*) (List_1_t212 *, const MethodInfo*))List_1_Sort_m10704_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m13432(__this, ___comparison, method) (( void (*) (List_1_t212 *, Comparison_1_t2007 *, const MethodInfo*))List_1_Sort_m10706_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::ToArray()
#define List_1_ToArray_m13433(__this, method) (( CanvasGroupU5BU5D_t2002* (*) (List_1_t212 *, const MethodInfo*))List_1_ToArray_m10708_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::TrimExcess()
#define List_1_TrimExcess_m13434(__this, method) (( void (*) (List_1_t212 *, const MethodInfo*))List_1_TrimExcess_m10710_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::get_Capacity()
#define List_1_get_Capacity_m13435(__this, method) (( int32_t (*) (List_1_t212 *, const MethodInfo*))List_1_get_Capacity_m10712_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m13436(__this, ___value, method) (( void (*) (List_1_t212 *, int32_t, const MethodInfo*))List_1_set_Capacity_m10714_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::get_Count()
#define List_1_get_Count_m13437(__this, method) (( int32_t (*) (List_1_t212 *, const MethodInfo*))List_1_get_Count_m10716_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::get_Item(System.Int32)
#define List_1_get_Item_m13438(__this, ___index, method) (( CanvasGroup_t317 * (*) (List_1_t212 *, int32_t, const MethodInfo*))List_1_get_Item_m10718_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.CanvasGroup>::set_Item(System.Int32,T)
#define List_1_set_Item_m13439(__this, ___index, ___value, method) (( void (*) (List_1_t212 *, int32_t, CanvasGroup_t317 *, const MethodInfo*))List_1_set_Item_m10720_gshared)(__this, ___index, ___value, method)
