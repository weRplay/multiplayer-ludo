﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// PlayerTwo/<SendPlayer2Position>c__IteratorF
struct U3CSendPlayer2PositionU3Ec__IteratorF_t43;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void PlayerTwo/<SendPlayer2Position>c__IteratorF::.ctor()
extern "C" void U3CSendPlayer2PositionU3Ec__IteratorF__ctor_m159 (U3CSendPlayer2PositionU3Ec__IteratorF_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerTwo/<SendPlayer2Position>c__IteratorF::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CSendPlayer2PositionU3Ec__IteratorF_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m160 (U3CSendPlayer2PositionU3Ec__IteratorF_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object PlayerTwo/<SendPlayer2Position>c__IteratorF::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CSendPlayer2PositionU3Ec__IteratorF_System_Collections_IEnumerator_get_Current_m161 (U3CSendPlayer2PositionU3Ec__IteratorF_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean PlayerTwo/<SendPlayer2Position>c__IteratorF::MoveNext()
extern "C" bool U3CSendPlayer2PositionU3Ec__IteratorF_MoveNext_m162 (U3CSendPlayer2PositionU3Ec__IteratorF_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerTwo/<SendPlayer2Position>c__IteratorF::Dispose()
extern "C" void U3CSendPlayer2PositionU3Ec__IteratorF_Dispose_m163 (U3CSendPlayer2PositionU3Ec__IteratorF_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void PlayerTwo/<SendPlayer2Position>c__IteratorF::Reset()
extern "C" void U3CSendPlayer2PositionU3Ec__IteratorF_Reset_m164 (U3CSendPlayer2PositionU3Ec__IteratorF_t43 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
