﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.WWWForm
struct WWWForm_t8;
// UnityEngine.WWW
struct WWW_t9;
// System.String
struct String_t;
// System.String[]
struct StringU5BU5D_t16;
// System.Object
struct Object_t;
// PlayerTwo
struct PlayerTwo_t12;

#include "mscorlib_System_Object.h"

// PlayerTwo/<GetPlayer2Position>c__Iterator10
struct  U3CGetPlayer2PositionU3Ec__Iterator10_t44  : public Object_t
{
	// UnityEngine.WWWForm PlayerTwo/<GetPlayer2Position>c__Iterator10::<form>__0
	WWWForm_t8 * ___U3CformU3E__0_0;
	// UnityEngine.WWW PlayerTwo/<GetPlayer2Position>c__Iterator10::<w>__1
	WWW_t9 * ___U3CwU3E__1_1;
	// System.String PlayerTwo/<GetPlayer2Position>c__Iterator10::<JsonString>__2
	String_t* ___U3CJsonStringU3E__2_2;
	// System.String[] PlayerTwo/<GetPlayer2Position>c__Iterator10::<array>__3
	StringU5BU5D_t16* ___U3CarrayU3E__3_3;
	// System.Single PlayerTwo/<GetPlayer2Position>c__Iterator10::<x>__4
	float ___U3CxU3E__4_4;
	// System.Single PlayerTwo/<GetPlayer2Position>c__Iterator10::<y>__5
	float ___U3CyU3E__5_5;
	// System.Int32 PlayerTwo/<GetPlayer2Position>c__Iterator10::<p>__6
	int32_t ___U3CpU3E__6_6;
	// System.Int32 PlayerTwo/<GetPlayer2Position>c__Iterator10::$PC
	int32_t ___U24PC_7;
	// System.Object PlayerTwo/<GetPlayer2Position>c__Iterator10::$current
	Object_t * ___U24current_8;
	// PlayerTwo PlayerTwo/<GetPlayer2Position>c__Iterator10::<>f__this
	PlayerTwo_t12 * ___U3CU3Ef__this_9;
};
