﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::.ctor(System.Collections.Generic.Stack`1<T>)
#define Enumerator__ctor_m16499(__this, ___t, method) (( void (*) (Enumerator_t2224 *, Stack_1_t690 *, const MethodInfo*))Enumerator__ctor_m10986_gshared)(__this, ___t, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Type>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16500(__this, method) (( Object_t * (*) (Enumerator_t2224 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m10987_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::Dispose()
#define Enumerator_Dispose_m16501(__this, method) (( void (*) (Enumerator_t2224 *, const MethodInfo*))Enumerator_Dispose_m10988_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Type>::MoveNext()
#define Enumerator_MoveNext_m16502(__this, method) (( bool (*) (Enumerator_t2224 *, const MethodInfo*))Enumerator_MoveNext_m10989_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Type>::get_Current()
#define Enumerator_get_Current_m16503(__this, method) (( Type_t * (*) (Enumerator_t2224 *, const MethodInfo*))Enumerator_get_Current_m10990_gshared)(__this, method)
