﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t359;
// UnityEngine.Object
struct Object_t54;
struct Object_t54_marshaled;
// UnityEngine.Object[]
struct ObjectU5BU5D_t599;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.AssetBundleRequest::.ctor()
extern "C" void AssetBundleRequest__ctor_m1986 (AssetBundleRequest_t359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern "C" Object_t54 * AssetBundleRequest_get_asset_m1987 (AssetBundleRequest_t359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
extern "C" ObjectU5BU5D_t599* AssetBundleRequest_get_allAssets_m1988 (AssetBundleRequest_t359 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
