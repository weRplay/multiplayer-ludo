﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// GameManager/<AutomaticMovePlayer2>c__Iterator7
struct U3CAutomaticMovePlayer2U3Ec__Iterator7_t25;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void GameManager/<AutomaticMovePlayer2>c__Iterator7::.ctor()
extern "C" void U3CAutomaticMovePlayer2U3Ec__Iterator7__ctor_m64 (U3CAutomaticMovePlayer2U3Ec__Iterator7_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager/<AutomaticMovePlayer2>c__Iterator7::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CAutomaticMovePlayer2U3Ec__Iterator7_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m65 (U3CAutomaticMovePlayer2U3Ec__Iterator7_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object GameManager/<AutomaticMovePlayer2>c__Iterator7::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CAutomaticMovePlayer2U3Ec__Iterator7_System_Collections_IEnumerator_get_Current_m66 (U3CAutomaticMovePlayer2U3Ec__Iterator7_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean GameManager/<AutomaticMovePlayer2>c__Iterator7::MoveNext()
extern "C" bool U3CAutomaticMovePlayer2U3Ec__Iterator7_MoveNext_m67 (U3CAutomaticMovePlayer2U3Ec__Iterator7_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager/<AutomaticMovePlayer2>c__Iterator7::Dispose()
extern "C" void U3CAutomaticMovePlayer2U3Ec__Iterator7_Dispose_m68 (U3CAutomaticMovePlayer2U3Ec__Iterator7_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void GameManager/<AutomaticMovePlayer2>c__Iterator7::Reset()
extern "C" void U3CAutomaticMovePlayer2U3Ec__Iterator7_Reset_m69 (U3CAutomaticMovePlayer2U3Ec__Iterator7_t25 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
