﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t110;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_7.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m11155_gshared (Enumerator_t1837 * __this, List_1_t110 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m11155(__this, ___l, method) (( void (*) (Enumerator_t1837 *, List_1_t110 *, const MethodInfo*))Enumerator__ctor_m11155_gshared)(__this, ___l, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m11156_gshared (Enumerator_t1837 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m11156(__this, method) (( Object_t * (*) (Enumerator_t1837 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11156_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C" void Enumerator_Dispose_m11157_gshared (Enumerator_t1837 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m11157(__this, method) (( void (*) (Enumerator_t1837 *, const MethodInfo*))Enumerator_Dispose_m11157_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::VerifyState()
extern "C" void Enumerator_VerifyState_m11158_gshared (Enumerator_t1837 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m11158(__this, method) (( void (*) (Enumerator_t1837 *, const MethodInfo*))Enumerator_VerifyState_m11158_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C" bool Enumerator_MoveNext_m11159_gshared (Enumerator_t1837 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m11159(__this, method) (( bool (*) (Enumerator_t1837 *, const MethodInfo*))Enumerator_MoveNext_m11159_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C" RaycastResult_t103  Enumerator_get_Current_m11160_gshared (Enumerator_t1837 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m11160(__this, method) (( RaycastResult_t103  (*) (Enumerator_t1837 *, const MethodInfo*))Enumerator_get_Current_m11160_gshared)(__this, method)
