﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ScrollItem : MonoBehaviour
{



    #region PUBLIC_VARIABLES
    public Text indexText;

    public DynamicScrollView dynamicScrollView;
    #endregion

    #region PRIVATE_VARIABLES
    #endregion

    #region UNITY_CALLBACKS
    void OnEnable()
    {
        indexText.text = transform.name;
    }
    #endregion

    #region PUBLIC_METHODS
    #endregion

    #region PRIVATE_METHODS
    #endregion

    #region PRIVATE_COROUTINES

	public IEnumerator CreateGame (string player2Name) 
	{	WWWForm form = new WWWForm();
		form.AddField("username", PlayerPrefs.GetString("Username"));
		form.AddField("player2", player2Name);
		WWW w = new WWW ("http://www.werplay.com/devs/Ludo/CreateGame.php",form);
		yield return w;
		if (w.error == null) 
		{
			Debug.Log("Game Created " + w.text);
			string JsonString = w.text;
			string[] array = JsonString.Split('-');
			if (array[0] == "Done")
			{
				PlayerPrefs.SetString ("Player", "Player1");
				PlayerPrefs.SetString("GameId",array[1] );
				Application.LoadLevel("MainScene");
			}

		} 
		else 
		{
			Debug.Log( w.error );
		}	
	}
    #endregion

    #region DELEGATES_CALLBACKS
    #endregion

    #region UI_CALLBACKS
    public void OnRemoveMe()
    {
		Debug.Log(gameObject.name);
		string Opponent = gameObject.name;
		if (PlayerPrefs.GetString("HasGame") == "No")
		{
			StartCoroutine(CreateGame(Opponent));
		}

		else if ( PlayerPrefs.GetString("HasGame") == "Yes")
		{
			Application.LoadLevel("MainScene");
		}


        //DestroyImmediate(gameObject);
        //dynamicScrollView.SetContentHeight();
    }
    #endregion


}
