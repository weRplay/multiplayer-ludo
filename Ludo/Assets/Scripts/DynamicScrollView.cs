﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class DynamicScrollView : MonoBehaviour
{

    #region PUBLIC_VARIABLES
    public int noOfItems;

	public  static GameObject heading;

    public GameObject item;

    public GridLayoutGroup gridLayout;

    public RectTransform scrollContent;

    public ScrollRect scrollRect;

	private static string[] players;
    #endregion

    #region PRIVATE_VARIABLES
    #endregion

    #region UNITY_CALLBACKS
    void OnEnable()
    {
     //   InitializeList();
		StartCoroutine("GetActiveUsers");
    }
    #endregion

    #region PUBLIC_METHODS
    #endregion

    #region PRIVATE_METHODS
    private void ClearOldElement()
    {
        for (int i = 0; i < gridLayout.transform.childCount; i++)
        {
            Destroy(gridLayout.transform.GetChild(i).gameObject);
        }
    }



    public void SetContentHeight()
    {
        float scrollContentHeight = (gridLayout.transform.childCount * gridLayout.cellSize.y) + ((gridLayout.transform.childCount - 1) * gridLayout.spacing.y);
        scrollContent.sizeDelta = new Vector2(400, scrollContentHeight);
    }

    private void InitializeList()
    {
        ClearOldElement();
     /*   for (int i = 0; i < noOfItems; i++)
            InitializeNewItem("" + (i + 1));
        SetContentHeight();*/


		for (int i = 0; i < players.Length - 1 ; i++)
		{
		//	Debug.Log(players[i]);
			InitializeNewItem(players[i]);
		}
		SetContentHeight();

    }

	private void InitializeListForGames(string Game)
	{
		ClearOldElement();

		InitializeNewItem(Game);
	
		SetContentHeight();
		
	}

    private void InitializeNewItem(string name)
    {
        GameObject newItem = Instantiate(item) as GameObject;
        newItem.name = name;
        newItem.transform.parent = gridLayout.transform;
        newItem.transform.localScale = Vector3.one;
        newItem.SetActive(true);
    }
    #endregion

    #region PRIVATE_COROUTINES
    private IEnumerator MoveTowardsTarget(float time,float from,float target) {
        float i = 0;
        float rate = 1 / time;
        while(i<1){
            i += rate * Time.deltaTime;
            scrollRect.verticalNormalizedPosition = Mathf.Lerp(from,target,i);
            yield return 0;
        }
    }

	public IEnumerator GetActiveUsers () 
	{	
		Debug.Log("Username is: " + PlayerPrefs.GetString ("Username"));
		WWWForm form = new WWWForm();
		string username = PlayerPrefs.GetString ("Username");
		form.AddField("Name", username);
		WWW w = new WWW ("http://www.werplay.com/devs/Ludo/GetActiveGames.php", form);
		yield return w;
		if (w.error == null) 
		{
			string response = w.text;
			string [] responseSplit = response.Split('%');
			Debug.Log ("Response: " + response);
			if (responseSplit[0] == "Players" )
			{
				Debug.Log("Players: " + responseSplit[0]);
				string playersName = responseSplit[1];
				if (responseSplit[1] == "")
				{
					heading = GameObject.Find("ActiveUsers");
					heading.GetComponent<Text>().text = "No Active Users";
				}
				else
				{
					players = playersName.Split('-');
					InitializeList();
					PlayerPrefs.SetString("HasGame","No");
					heading = GameObject.Find("ActiveUsers");
					heading.GetComponent<Text>().text = "Active Users";
				}

			}
			else if (responseSplit[0] == "Player2" )
			{
				Debug.Log("Player2: " + responseSplit[0] + " " + responseSplit[1]);
				InitializeListForGames(responseSplit[1]);
				string matchDetails = responseSplit[2];
				string [] details = matchDetails.Split('-');
				PlayerPrefs.SetString("HasGame","Yes");
				PlayerPrefs.SetString ("Player", "Player2");
				PlayerPrefs.SetString("GameId", details[10]);
		/*		int p = int.Parse(details[4]);
				GameManager.playerPosition = p;
				PlayerPrefs.SetInt("PlayerPos", p); */
				Debug.Log("Player1: " + responseSplit[0] + " " + responseSplit[1] + " " + details[10]);
				heading = GameObject.Find("ActiveUsers");
				heading.GetComponent<Text>().text = "Active Games";
			}
			else if (responseSplit[0] == "Player1")
			{

				InitializeListForGames(responseSplit[1]);
				string matchDetails = responseSplit[2];
				string [] details = matchDetails.Split('-');
				PlayerPrefs.SetString("HasGame","Yes");
				PlayerPrefs.SetString ("Player", "Player1");
				PlayerPrefs.SetString("GameId", details[10]);
	/*			int p = int.Parse(details[5]);
				GameManager.playerPosition = p;
				PlayerPrefs.SetInt("PlayerPos", p); */
				Debug.Log("Player1: " + responseSplit[0] + " " + responseSplit[1] + " " + details[10]);
				heading = GameObject.Find("ActiveUsers");
				heading.GetComponent<Text>().text = "Active Games";
			}
			else
			{
				heading = GameObject.Find("ActiveUsers");
				heading.GetComponent<Text>().text = "No Active Users";
			}
		//	

		} 
		else 
		{
			Debug.Log( w.error );
		}	
	}
    #endregion

    #region DELEGATES_CALLBACKS
    #endregion

    #region UI_CALLBACKS
    public void AddNewElement()
    {
        InitializeNewItem("" + (gridLayout.transform.childCount + 1) + ":" + "Samad");
        SetContentHeight();
        StartCoroutine(MoveTowardsTarget(0.2f, scrollRect.verticalNormalizedPosition, 0));
    }
    #endregion


}
