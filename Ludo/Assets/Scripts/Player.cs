﻿using UnityEngine;
using System.Collections;

public class Player : MovingObject {
	public int wallDamage = 1;		
//	private Animator animator;					//Used to store a reference to the Player's animator component.
	//Start overrides the Start function of MovingObject
	protected override void Start ()
	{
		//Get a component reference to the Player's animator component
		//animator = GetComponent<Animator>();
		
		//Call the Start function of the MovingObject base class.
	//	StartCoroutine("GetPlayer1Position");
		base.Start ();
	}

/*	private void Update ()
	{
		//If it's not the player's turn, exit the function.
		if(!GameManager.playersTurn) return;
		
		int horizontal = 0;  	//Used to store the horizontal move direction.
		int vertical = 0;		//Used to store the vertical move direction.

		//Get input from the input manager, round it to an integer and store in horizontal to set x axis move direction
		horizontal = (int) (Input.GetAxisRaw ("Horizontal"));

		//Get input from the input manager, round it to an integer and store in vertical to set y axis move direction
		vertical = (int) (Input.GetAxisRaw ("Vertical"));
		
		//Check if moving horizontally, if so set vertical to zero.
		if(horizontal != 0)
		{
			vertical = 0;
		}
		if(horizontal != 0 || vertical != 0)
		{
			AttemptMove (horizontal, vertical);
		}

	}
*/
	public void MoveThePlayer(int x, int y, int a, int b, int playerPosition, int diceNumber)
	{
		AttemptMove (x, y);
		StartCoroutine(SendPlayer1Position(a,b, playerPosition, diceNumber));
		//if(!GameManager.instance.playersTurn) return;
		/*Debug.Log("GameManager instance diceNumber: " +  GameManager.diceNumber);
		int horizontal = 0;  	//Used to store the horizontal move direction.
		int vertical =  GameManager.diceNumber;		//Used to store the vertical move direction.
		
		//Get input from the input manager, round it to an integer and store in horizontal to set x axis move direction
		horizontal = 0;
		
		//Get input from the input manager, round it to an integer and store in vertical to set y axis move direction
		//vertical = 2;
		
		//Check if moving horizontally, if so set vertical to zero.
		if(horizontal != 0)
		{
			vertical = 0;
		}
		if(horizontal != 0 || vertical != 0)
		{
			AttemptMove (4, 6);
		}*/
	}
	protected override void AttemptMove (int xDir, int yDir)
	{
		//Call the AttemptMove method of the base class, passing in the component T (in this case Wall) and x and y direction to move.
		base.AttemptMove (xDir, yDir);
		//Set the playersTurn boolean of GameManager to false now that players turn is over.
		GameManager.playersTurn = false;

	}

	public IEnumerator SendPlayer1Position (int a, int b, int playerPosition,  int diceNumber) 
	{	WWWForm form = new WWWForm();
		Debug.Log(PlayerPrefs.GetString("GameId"));
		form.AddField("gameId", PlayerPrefs.GetString("GameId"));
		form.AddField("player1", "Sameer");
		form.AddField("player2", "Samad");
		form.AddField("xPosition", a);
		form.AddField("yPosition", b);
		form.AddField("diceNumber", diceNumber);
		form.AddField("ArrayPosition", playerPosition);
		WWW w = new WWW ("http://www.werplay.com/devs/Ludo/Player1Move.php",form);
		yield return w;
		if (w.error == null) 
		{
			Debug.Log(w.text);
		} 
		else 
		{
			Debug.Log( w.error );
		}
		
	}
	public void ChangeTransform(float x, float y)
	{
		Debug.Log("Changing player 1 transform");
		transform.position = new Vector3(x, y, 0f);
	}
	public IEnumerator GetPlayer1Position () 
	{
		WWWForm form = new WWWForm();
		form.AddField("gameId", PlayerPrefs.GetString("GameId"));
		WWW w = new WWW ("http://www.werplay.com/devs/Ludo/GetPlayer1Position.php", form);
		yield return w;
		string JsonString = w.text;
		string[] array = JsonString.Split('-');
		float x = float.Parse(array[0]);
		float y = float.Parse(array[1]);
		int p = int.Parse(array[2]);
		GameManager.playerPosition = p;
		PlayerPrefs.SetInt("PlayerPos", p);
		transform.position = new Vector3(x, y, 0f);
		Debug.Log("Player 1 Position got: " + array[0] + " - " + array [1]);
	}

	public void CallExecuteAfterTime ()
	{
		Debug.Log("Calling delay function");
		StartCoroutine(ExecuteAfterTime(3));
	}
	
	IEnumerator ExecuteAfterTime(float time)
	{
		yield return new WaitForSeconds(time);
		Debug.Log("Done waiting");
		GameManager.ShowButtonBool = true;
	}


}
