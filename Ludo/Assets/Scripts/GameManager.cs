﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GameManager : MonoBehaviour {

	private static BoardManager boardScript;	
	private static Player playerScript;//Store a reference to our BoardManager which will set up the level.
	private static PlayerTwo playerTwoScript;
	private static CountdownTimer countdownTimerScript;
	//public static GameManager instance = null;				//Static instance of GameManager which allows it to be accessed by any other script.
	[HideInInspector] public static bool playersTurn = false;
	[HideInInspector] public static bool callCoroutineBool = true;
	[HideInInspector] public static bool gameOverBool = false;
	[HideInInspector] public static bool ShowButtonBool = true;
	private static bool MoveForward = true;
	private static bool MoveDice = true;

	public float turnDelay = 0.1f;
	private bool delayBool;	 
	public GameObject[] dices;	
	private static GameObject diceInstance;
	private static GameObject diceInstanceB;
	private static GameObject resetButton;
	private GameObject rollDiceButton;
	public  static GameObject turnText;

	private GameObject playerMoveButton;
	[HideInInspector] public static int diceNumber = 0;
	[HideInInspector] public static int playerPosition = 0;
//	private static GameObject toInstantiate;
	public Slider testslider;





	// Use this for initialization
	void Awake()
	{
		//Get a component reference to the attached BoardManager script
		boardScript = GetComponent<BoardManager>();
		playerScript = GameObject.Find("Player").GetComponent<Player>();
		playerTwoScript = GameObject.Find("Player2").GetComponent<PlayerTwo>();
		countdownTimerScript =  GameObject.Find("GameManager").GetComponent<CountdownTimer>();
		rollDiceButton = GameObject.Find("RollDice");
		resetButton = GameObject.Find("Reset");
		turnText = GameObject.Find("TurnText");

		StartCoroutine(playerScript.GetPlayer1Position());
		StartCoroutine(playerTwoScript.GetPlayer2Position());

		InitGame();

	}

	void Start()
	{

	}

	void InitGame()
	{
		Debug.Log(PlayerPrefs.GetString("GameId"));
		resetButton.SetActive(false);
		boardScript.SetupScene();
		countdownTimerScript.CountdownNumber = 10;
//		PlayerPrefs.DeleteAll();
//		PlayerPrefs.SetString ("Player", "Player1");
//		PlayerPrefs.SetString ("Player", "Player2");

/*		if (PlayerPrefs.HasKey ("UniquePlayerId")) 
		{
			Debug.Log("Found UID: " + PlayerPrefs.GetString("UniquePlayerId"));

	//		playerMoveButton = GameObject.Find("MovePlayer");
	//		playerMoveButton.SetActive(false);
			boardScript.SetupScene();
		} 
		else 
		{
			StartCoroutine (CreateUniquePlayer ());
			Debug.Log("NO UID");
		}*/
		InvokeRepeating("CallGetPlayerTurn", 1, 2);
	//	InvokeRepeating("Move_Dice", 1, 0.2f);
	}

	public void GameOver()
	{
		enabled = false;
	}
	
	private void GetNewPosition(int DiceRoll)
	{
		Debug.Log(boardScript.BluePath[DiceRoll]);
	}

	public void RollDice()
	{
		playersTurn = false;
		MoveDice = false;
//		playerPosition = PlayerPrefs.GetInt("PlayerPos");
//		Debug.Log( GameObject.Find ("Slider").GetComponent<Slider>().value );
		if (rollDiceButton == null)
		{
			rollDiceButton = GameObject.Find("RollDice");
		}
		/*float dNumber = GameObject.Find ("Slider").GetComponent<Slider>().value;
		diceNumber = (int)dNumber;*/

		PlayerPrefs.SetInt("diceNumber", diceNumber);
		rollDiceButton.SetActive(false);
		ShowButtonBool = false;

		playerScript.CallExecuteAfterTime ();

		turnText.GetComponent<Text>().text = "Opponents Turn";
		Debug.Log(diceNumber);
		Destroy(diceInstance);
	//	diceNumber = Random.Range (1,7);
	//	diceNumber = 1;

		GameObject toInstantiate = dices [diceNumber - 1];
		diceInstance = Instantiate (toInstantiate, new Vector3 (21, 15, 0f), Quaternion.identity) as GameObject;

		if (PlayerPrefs.GetString("Player") == "Player1")
		{
			if(playerPosition == 0)
			{
				Vector3 oldPosition = boardScript.BluePath[ playerPosition ];
				if (diceNumber == 6)
				{
					diceNumber = 1;
					playerPosition = 1;
					Vector3 NewPosition  = boardScript.BluePath[1 ];
					Vector3 MoveVector = NewPosition - oldPosition;
					Debug.Log("You Get out! " + playerPosition);
					playerScript.MoveThePlayer((int)MoveVector.x, (int)MoveVector.y, (int)NewPosition.x, (int)NewPosition.y, playerPosition, 6);
				}
				else
				{
					playerPosition = 0;
					Vector3 NewPosition  = boardScript.BluePath[playerPosition ];
					playerScript.MoveThePlayer(0, 0, (int)NewPosition.x, (int)NewPosition.y, playerPosition, diceNumber);
					Debug.Log("Less Than 6 " + playerPosition);
				}
			}
			else
			{
				Vector3 oldPosition = boardScript.BluePath[ playerPosition ];
				playerPosition = playerPosition + diceNumber;
				if ( playerPosition > 57)
				{
					playerPosition = playerPosition - diceNumber;
					Vector3 NewPosition  = boardScript.BluePath[playerPosition ];
					playerScript.MoveThePlayer(0, 0, (int)NewPosition.x, (int)NewPosition.y, playerPosition, diceNumber);
					Debug.Log("Larger than 57 " + playerPosition);
					
				}
				else if (playerPosition == 57)
				{
					if (diceNumber == 6)
					{
						playerPosition = playerPosition - diceNumber;
						Vector3 NewPosition  = boardScript.BluePath[playerPosition ];
						playerScript.MoveThePlayer(0, 0, (int)NewPosition.x, (int)NewPosition.y, playerPosition,diceNumber);
						Debug.Log("Larger than 57 " + playerPosition);
					}
					else
					{
						Vector3 NewPosition  = boardScript.BluePath[playerPosition ];
						Vector3 MoveVector = NewPosition - oldPosition;
						Debug.Log("You win! " + playerPosition);
						playerScript.MoveThePlayer((int)MoveVector.x, (int)MoveVector.y, (int)NewPosition.x, (int)NewPosition.y, playerPosition,diceNumber);
						turnText.GetComponent<Text>().text = "You Won!";
						gameOverBool = true;
					}
					
					
				}
				else
				{
					
					Vector3 NewPosition  = boardScript.BluePath[playerPosition ];
					Vector3 MoveVector = NewPosition - oldPosition;
					Debug.Log("Move The Player:" + diceNumber + " " + playerPosition + " " + MoveVector.x + MoveVector.y);
					playerScript.MoveThePlayer((int)MoveVector.x, (int)MoveVector.y, (int)NewPosition.x, (int)NewPosition.y, playerPosition, diceNumber);
					
				}
			}

		}
		else if (PlayerPrefs.GetString("Player") == "Player2")
		{
			if(playerPosition == 0)
			{
				Vector3 oldPosition = boardScript.RedPath[ playerPosition ];
				if (diceNumber == 6)
				{
					diceNumber = 1;
					playerPosition = 1;
					Vector3 NewPosition  = boardScript.RedPath[1 ];
					Vector3 MoveVector = NewPosition - oldPosition;
					Debug.Log("You Get out! " + playerPosition);
					playerTwoScript.MoveThePlayer((int)MoveVector.x, (int)MoveVector.y, (int)NewPosition.x, (int)NewPosition.y, playerPosition, 6);
				}
				else
				{
					playerPosition = 0;
					Vector3 NewPosition  = boardScript.RedPath[playerPosition ];
					playerTwoScript.MoveThePlayer(0, 0, (int)NewPosition.x, (int)NewPosition.y, playerPosition,diceNumber);
					Debug.Log("Less Than 6 " + playerPosition);
				}
			}
			else
			{
				Vector3 oldPosition = boardScript.RedPath[ playerPosition ];
				playerPosition = playerPosition + diceNumber;
				if ( playerPosition > 57)
				{
					playerPosition = playerPosition - diceNumber;
					Vector3 NewPosition  = boardScript.RedPath[playerPosition ];
					playerTwoScript.MoveThePlayer(0, 0, (int)NewPosition.x, (int)NewPosition.y, playerPosition,diceNumber);
					Debug.Log("Larger than 57 " + playerPosition);
					
				}
				else if (playerPosition == 57)
				{
					if (diceNumber == 6)
					{
						playerPosition = playerPosition - diceNumber;
						Vector3 NewPosition  = boardScript.RedPath[playerPosition ];
						playerTwoScript.MoveThePlayer(0, 0, (int)NewPosition.x, (int)NewPosition.y, playerPosition,diceNumber);
						Debug.Log("Larger than 57 " + playerPosition);
					}
					else
					{
						Vector3 NewPosition  = boardScript.RedPath[playerPosition ];
						Vector3 MoveVector = NewPosition - oldPosition;
						Debug.Log("You win! " + playerPosition);
						playerTwoScript.MoveThePlayer((int)MoveVector.x, (int)MoveVector.y, (int)NewPosition.x, (int)NewPosition.y,playerPosition,diceNumber);
						turnText.GetComponent<Text>().text = "You Won!";
						gameOverBool = true;

					}
				}
				else
				{
					
					Vector3 NewPosition  = boardScript.RedPath[playerPosition ];
					Vector3 MoveVector = NewPosition - oldPosition;
					Debug.Log(diceNumber + " " + playerPosition + " " + MoveVector.x + MoveVector.y);
					playerTwoScript.MoveThePlayer((int)MoveVector.x, (int)MoveVector.y, (int)NewPosition.x, (int)NewPosition.y, playerPosition, diceNumber);
					
				}
			}

		}
		PlayerPrefs.SetInt("PlayerPos", playerPosition);
		countdownTimerScript.ShiftTimer();
	
	}

	public void Move_Dice()
	{
	/*	DestroyImmediate();
		Destroy(toInstantiate);*/
		Destroy(diceInstance);
		diceNumber = Random.Range (1,7);
		GameObject toInstantiate = dices [diceNumber - 1];
		diceInstance = Instantiate (toInstantiate, new Vector3 (21, 15, 0f), Quaternion.identity) as GameObject;
		CountdownTimer.showTimer = true;
	}

	public void Move_OpponentsDice()
	{
		/*	DestroyImmediate();
		Destroy(toInstantiate);*/
		Destroy(diceInstanceB);
		diceNumber = Random.Range (1,7);
		GameObject toInstantiate = dices [diceNumber - 1];
		diceInstanceB = Instantiate (toInstantiate, new Vector3 (-10, 15, 0f), Quaternion.identity) as GameObject;
		CountdownTimer.showTimer = true;
	}

	public void Stop_Dice()
	{
		//Debug.Log("I am here");
		CancelInvoke("Move_Dice");

	}

	public void Stop_OpponentsDice()
	{
		//Debug.Log("I am here");
		CancelInvoke("Move_OpponentsDice");
	}

	// Update is called once per frame
	void Update()
	{
		if(playersTurn == true)
		{
//			Debug.Log("Show Button: " + ShowButtonBool);
			Stop_OpponentsDice();
			if (ShowButtonBool == true)
			{
				rollDiceButton.SetActive(true);
			}
		}
		else
		{

			Stop_Dice();
		//	testslider.value = PlayerPrefs.GetInt("diceNumber");
			rollDiceButton.SetActive(false);
		//	testslider.enabled = false;
		//	GameObject.Find ("Slider").SetActive(false);
			if (gameOverBool)
			{
				turnText.GetComponent<Text>().text = "Game Over";
				resetButton.SetActive(true);
			}
			else
			{
				turnText.GetComponent<Text>().text = "Opponents Turn";
			}

		}


	}

	public void ResetButtonPressed()
	{
		StartCoroutine("ResetBoard");
	}



	private IEnumerator ResetBoard () 
	{	
		WWWForm form = new WWWForm();
		Debug.Log(PlayerPrefs.GetString("GameId"));
		form.AddField("gameId", PlayerPrefs.GetString("GameId"));
		WWW w = new WWW ("http://www.werplay.com/devs/Ludo/DeleteGame.php", form);
		yield return w;
		if ( w.text == "Done")
		{
			Debug.Log("Resetting");
			gameOverBool = false;
			Application.LoadLevel ("ActiveUsers"); 
			//gameOverBool = false;
		}
	}

	public void CallGetPlayerTurn()
	{
/*		Debug.Log("CallGetPlayerTurn");
		if(playersTurn )
		{
			
		}
		else
		{*/
			Debug.Log("Calling");
			if (PlayerPrefs.GetString("Player") == "Player1")
			{
				StartCoroutine("GetPlayer1Turn");
			}
			else if (PlayerPrefs.GetString("Player") == "Player2")
			{
				StartCoroutine("GetPlayer2Turn");
			}
		//}	

		//CountdownNumber
	}

	public IEnumerator GetPlayer1Turn () 
	{ //Debug.Log(PlayerPrefs.GetString("GameId"));
		WWWForm form = new WWWForm();
		form.AddField("gameId", PlayerPrefs.GetString("GameId"));
		WWW w = new WWW ("http://www.werplay.com/devs/Ludo/GetPlayer1Turn.php", form);
		yield return w;
		Debug.Log("Player 1 turn: " + w.text);
		string JsonString = w.text;
		string[] array = JsonString.Split('-');
		if (array[0] == "True")
		{
			string ResetBool = array[7];
			if (ResetBool == "True")
			{
				countdownTimerScript.CountdownNumber = 10;
			}
			countdownTimerScript.CountdownNumber--;
			Debug.Log(countdownTimerScript.CountdownNumber);
			if(countdownTimerScript.CountdownNumber == 0)
			{
				StartCoroutine(AutomaticMovePlayer1 ());
				countdownTimerScript.CountdownNumber = 10;
			}
			//StartCoroutine(ExecuteAfterTime(0));
		
			turnText.GetComponent<Text>().text = "Your Turn!";
			playersTurn = true;
			InvokeRepeating("Move_Dice", 0, 0.3f);
			//timeLeft = 10.0f;
			MoveDice = true;
			//rollDiceButton.SetActive(true);
			Debug.Log("Player 2 position " + array[1] + "-" + array[2]);
			float x = float.Parse(array[1]);
			float y = float.Parse(array[2]);
			int dn = int.Parse(array[3]);
			float Playerx = float.Parse(array[4]);
			float Playery = float.Parse(array[5]);
			int p = int.Parse(array[6]);
			GameManager.playerPosition = p;
			GameObject.Find("Player").transform.position = new Vector3(Playerx, Playery, 0f);
			PlayerPrefs.SetInt("PlayerPos", p);
			if(dn <= 0 || dn > 6)
			{
				Destroy(diceInstanceB);
				GameObject toInstantiate = dices [1];
				diceInstanceB = Instantiate (toInstantiate, new Vector3 (-10, 15, 0f), Quaternion.identity) as GameObject;
			}
			else
			{
				Destroy(diceInstanceB);
				GameObject toInstantiate = dices [dn - 1];
				diceInstanceB = Instantiate (toInstantiate, new Vector3 (-10, 15, 0f), Quaternion.identity) as GameObject;
			}

			playerTwoScript.ChangeTransform(x,y);
		//	CancelInvoke("CallGetPlayerTurn");
		//	rollDiceButton.SetActive(true);
		}
		if (array[0] == "False")
		{
			rollDiceButton.SetActive(false);
			string ResetBool = array[7];
			if (ResetBool == "True")
			{
				countdownTimerScript.CountdownNumber = 10;
			}
			countdownTimerScript.CountdownNumber--;
			Debug.Log(countdownTimerScript.CountdownNumber);
			if(countdownTimerScript.CountdownNumber == 0)
			{
				StartCoroutine(AutomaticMovePlayer2 ());
				countdownTimerScript.CountdownNumber = 10;
			}
			if(int.Parse(array[1]) == 6 && int.Parse(array[2]) == 7)
			{
				gameOverBool = true;
				turnText.GetComponent<Text>().text = "Game Over";
				float x = float.Parse(array[1]);
				float y = float.Parse(array[2]);
				int dn = int.Parse(array[3]);
				playerTwoScript.ChangeTransform(x,y);
			}
			else
			{
				turnText.GetComponent<Text>().text = "Opponents Turn";
				float Playerx = float.Parse(array[4]);
				float Playery = float.Parse(array[5]);
				int p = int.Parse(array[6]);
				GameManager.playerPosition = p;
				GameObject.Find("Player").transform.position = new Vector3(Playerx, Playery, 0f);
				PlayerPrefs.SetInt("PlayerPos", p);
				if (Playerx == 7 && Playery == 6)
				{
					gameOverBool = true;
					turnText.GetComponent<Text>().text = "Game Over";
				}
				else
				{
					InvokeRepeating("Move_OpponentsDice", 1, 0.3f);
				}

			}

			Destroy(diceInstance);
			diceNumber = int.Parse(array[8]);
			GameObject toInstantiate = dices [diceNumber - 1];
			diceInstance = Instantiate (toInstantiate, new Vector3 (21, 15, 0f), Quaternion.identity) as GameObject;

		//	CallGetPlayerTurn();
			callCoroutineBool = true;
			playersTurn = false;
		}

	}
	

	public IEnumerator GetPlayer2Turn () 
	{
		WWWForm form = new WWWForm();
		form.AddField("gameId", PlayerPrefs.GetString("GameId"));
		WWW w = new WWW ("http://www.werplay.com/devs/Ludo/GetPlayer2Turn.php", form);
		yield return w;
	// 	Debug.Log("Player 2 bool: " + w.text);
		string JsonString = w.text;
		Debug.Log("GetPlayer2Turn " + w.text);
		string[] array = JsonString.Split('-');
		if (array[0] == "True")
		{
			string ResetBool = array[7];
			if (ResetBool == "True")
			{
				countdownTimerScript.CountdownNumber = 10;
			}
			countdownTimerScript.CountdownNumber--;
			Debug.Log(countdownTimerScript.CountdownNumber);
			if(countdownTimerScript.CountdownNumber == 0)
			{
				StartCoroutine(AutomaticMovePlayer2 ());
				countdownTimerScript.CountdownNumber = 10;
			}
		
			turnText.GetComponent<Text>().text = "Your Turn!";
//			StartCoroutine(ExecuteAfterTime(0));
			playersTurn = true;
			InvokeRepeating("Move_Dice", 0, 0.3f);
	//		timeLeft = 10.0f;
			MoveDice = true;
			//rollDiceButton.SetActive(true);
			Debug.Log("Player 1 position " + array[1] + "-" + array[2]);
			float x = float.Parse(array[1]);
			float y = float.Parse(array[2]);
			int dn = int.Parse(array[3]);
			float Playerx = float.Parse(array[4]);
			float Playery = float.Parse(array[5]);
			int p = int.Parse(array[6]);
			GameManager.playerPosition = p;
			GameObject.Find("Player2").transform.position = new Vector3(Playerx, Playery, 0f);
			PlayerPrefs.SetInt("PlayerPos", p);

			if(dn <= 0 || dn > 6)
			{
				Destroy(diceInstanceB);
				GameObject toInstantiate = dices [ 1];
				diceInstanceB = Instantiate (toInstantiate, new Vector3 (-10, 15, 0f), Quaternion.identity) as GameObject;
			}
			else
			{
				Destroy(diceInstanceB);
				GameObject toInstantiate = dices [dn - 1];
				diceInstanceB = Instantiate (toInstantiate, new Vector3 (-10, 15, 0f), Quaternion.identity) as GameObject;
			}
			playerScript.ChangeTransform(x,y);
		//	CancelInvoke("CallGetPlayerTurn");
		//	rollDiceButton.SetActive(true);
		}
		if (array[0] == "False")
		{
			rollDiceButton.SetActive(false);
			string ResetBool = array[7];
			if (ResetBool == "True")
			{
				countdownTimerScript.CountdownNumber = 10;
			}
			countdownTimerScript.CountdownNumber--;
			Debug.Log(countdownTimerScript.CountdownNumber);
			if(countdownTimerScript.CountdownNumber == 0)
			{
				StartCoroutine(AutomaticMovePlayer1 ());
				countdownTimerScript.CountdownNumber = 10;
			}
			if(int.Parse(array[1]) == 7 && int.Parse(array[2]) == 6)
			{
				gameOverBool = true;
				turnText.GetComponent<Text>().text = "Game Over";
				float x = float.Parse(array[1]);
				float y = float.Parse(array[2]);
				playerScript.ChangeTransform(x,y);
			}
			else
			{
				turnText.GetComponent<Text>().text = "Opponents Turn";
				float Playerx = float.Parse(array[4]);
				float Playery = float.Parse(array[5]);
				int p = int.Parse(array[6]);
				GameManager.playerPosition = p;
				GameObject.Find("Player2").transform.position = new Vector3(Playerx, Playery, 0f);
				PlayerPrefs.SetInt("PlayerPos", p);
				if (Playerx == 6 && Playery == 7)
				{
					gameOverBool = true;
					turnText.GetComponent<Text>().text = "Game Over";
				}
				else
				{
					InvokeRepeating("Move_OpponentsDice", 1, 0.3f);
				}

			}
			Destroy(diceInstance);
			diceNumber = int.Parse(array[8]);
			GameObject toInstantiate = dices [diceNumber - 1];
			diceInstance = Instantiate (toInstantiate, new Vector3 (21, 15, 0f), Quaternion.identity) as GameObject;

			if (rollDiceButton == null)
			{
				//rollDiceButton = GameObject.Find("RollDice");
			}
			else
			{
				rollDiceButton.SetActive(false);
			}
		//	CallGetPlayerTurn();
			callCoroutineBool = true;
			playersTurn = false;
		}
		
	}

	public IEnumerator AutomaticMovePlayer1 () 
	{
		WWWForm form = new WWWForm();
		form.AddField("gameId", PlayerPrefs.GetString("GameId"));
		WWW w = new WWW ("http://www.werplay.com/devs/Ludo/Player1MoveAutomatic.php", form);
		yield return w;
		Debug.Log("Automatic turn result: " + w.text);

	}


	public IEnumerator AutomaticMovePlayer2 () 
	{
		WWWForm form = new WWWForm();
		form.AddField("gameId", PlayerPrefs.GetString("GameId"));
		WWW w = new WWW ("http://www.werplay.com/devs/Ludo/Player2MoveAutomatic.php", form);
		yield return w;
		Debug.Log("Automatic turn result: " + w.text);
		Destroy(diceInstance);
		diceNumber = 1;
		GameObject toInstantiate = dices [diceNumber - 1];
		diceInstance = Instantiate (toInstantiate, new Vector3 (21, 15, 0f), Quaternion.identity) as GameObject;
	}


}
