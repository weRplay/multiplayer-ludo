﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic; 		//Allows us to use Lists.
using Random = UnityEngine.Random; 		//Tells Random to use the Unity Engine random number generator.

public class BoardManager : MonoBehaviour {
	public int columns = 15; 										//Number of columns in our game board.
	public int rows = 15;											//Number of rows in our game board.
	private Transform boardHolder;									//A variable to store a reference to the transform of our Board object.


	public GameObject[] floorTiles;									
	public GameObject[] outerWallTiles;	
	public GameObject Player;
	public List <Vector3> BluePath;
	public List <Vector3> RedPath;
	//public GameObject Bhome;
	void Start () 
	{
	
	}

	void InitializeRedPath()
	{

		RedPath.Add (new Vector3(0, 9, 0f));
		int y = 8; 
		int x = 1;

		for (int a = 1; a < 6 ; a++)
		{
			RedPath.Add (new Vector3(x, 8, 0f));
			x++;
		}
		y++;
		for (int a = 6; a < 12; a++)
		{
			RedPath.Add (new Vector3(x, y, 0f));
			y++;
		}
		y--;
		x++;
		RedPath.Add (new Vector3(x, y, 0f));

		x++;

		for (int a = 13; a < 19; a++)
		{
			RedPath.Add (new Vector3(x, y, 0f));
			y--;
		}

		x++;

		for (int a = 19; a < 25; a++)
		{
			RedPath.Add (new Vector3(x, y, 0f));
			x++;
		}

		y--;
		x--;
		RedPath.Add (new Vector3(x, y, 0f));
		y--;

		for (int a = 26; a < 32; a++)
		{
			RedPath.Add (new Vector3(x, y, 0f));
			x--;
		}

		for (int a = 32; a < 38; a++)
		{
			y--;
			RedPath.Add (new Vector3(x, y, 0f));
		}

		x--;
		RedPath.Add (new Vector3(x, y, 0f));
		x--;

		for (int a = 39; a < 45; a++)
		{
			RedPath.Add (new Vector3(x, y, 0f));
			y++;
		}

		for (int a = 45; a < 51; a++)
		{
			x--;
			RedPath.Add (new Vector3(x, y, 0f));
		}

		y++;
		for (int a = 51; a < 58; a++)
		{
			RedPath.Add (new Vector3(x, y, 0f));
			x++;
		}


	/*	for (int i = 0; i < RedPath.Count; i++)
		{
			Debug.Log(RedPath[i]);
		}*/
	}



	void InitializeBluePath()
	{

		BluePath.Add (new Vector3(5, 0, 0f));
		int y = 1; 
		int x = 0;
		for (int a = 1; a < 6 ; a++)
		{
			BluePath.Add (new Vector3(6, y, 0f));
			y++;
		}
		x = 5;
		for (int a = 6; a < 12; a++)
		{
			BluePath.Add (new Vector3(x, y, 0f));
			x--;
		}
		x++;
		y++;
		BluePath.Add (new Vector3(x, y, 0f));
		y++;

		for (int a = 13; a < 19; a++)
		{
			BluePath.Add (new Vector3(x, y, 0f));
			x++;
		}

		y++;

		for (int a = 19; a < 25; a++)
		{
			BluePath.Add (new Vector3(x, y, 0f));
			y++;
		}
		x++;
		y--;
		BluePath.Add (new Vector3(x, y, 0f));
		x++;
		for (int a = 26; a < 32; a++)
		{
			BluePath.Add (new Vector3(x, y, 0f));
			y--;
		}

		for (int a = 32; a < 38; a++)
		{
			x++;
			BluePath.Add (new Vector3(x, y, 0f));
		}
		y--;

		BluePath.Add (new Vector3(x, y, 0f));
		y--;
		for (int a = 39; a < 45; a++)
		{
			BluePath.Add (new Vector3(x, y, 0f));
			x--;
		}


		for (int a = 45; a < 51; a++)
		{
			y--;
			BluePath.Add (new Vector3(x, y, 0f));
		}
		x--;
		BluePath.Add (new Vector3(x, y, 0f));

		for (int a = 52; a < 58; a++)
		{
			y++;
			BluePath.Add (new Vector3(x, y, 0f));
		}



	/*	for (int i = 0; i < BluePath.Count; i++)
		{
			Debug.Log(BluePath[i]);
		}*/
	}

	void BoardSetup ()
	{
		//Instantiate Board and set boardHolder to its transform.
		boardHolder = new GameObject ("Board").transform;
		
		//Loop along x axis, starting from -1 (to fill corner) with floor or outerwall edge tiles.
		for(int x = -1; x < columns + 1; x++)
		{
			//Loop along y axis, starting from -1 to place floor or outerwall tiles.
			for(int y = -1; y < rows + 1; y++)
			{
				//Choose a random tile from our array of floor tile prefabs and prepare to instantiate it.
				GameObject toInstantiate = outerWallTiles [Random.Range (0, outerWallTiles.Length)];
				
				//Check if we current position is at board edge, if so choose a random outer wall prefab from our array of outer wall tiles.
				if(x == -1 || x == columns || y == -1 || y == rows)
				{
					toInstantiate = outerWallTiles [Random.Range (0, outerWallTiles.Length)];
				}	
				if (x > 5 && x < 9)
				{
					if (y > -1 && y < 6 )
					{
						toInstantiate = floorTiles [3];
						if ( x == 6 && y == 1)
						{
							//GameObject p = Instantiate (Player, new Vector3 (x, y, 0f), Quaternion.identity) as GameObject;
							toInstantiate = floorTiles [1];
						}
						if (x == 8 && y == 2)
						{
							toInstantiate = floorTiles [2];
						}
						if (x == 7 && y > 0)
						{
							toInstantiate = floorTiles [2];
						}
					}
					if ( y > 8 && y < 15)
					{
						toInstantiate = floorTiles [3];
						if ( x == 8 && y == 13)
						{
							toInstantiate = floorTiles [6];
						}
						if (x == 6 && y == 12)
						{
							toInstantiate = floorTiles [9];
						}
						if (x == 7 && y < 14)
						{
							toInstantiate = floorTiles [9];
						}
					}

				}
				if (y > 5 && y < 9)
				{
					if (x > -1 && x < 6 )
					{
						toInstantiate = floorTiles [3];
						if ( y == 6 && x == 2)
						{
							toInstantiate = floorTiles [5];
						}
						if (y == 8 && x == 1)
						{
							toInstantiate = floorTiles [4];
						}
						if (y == 7 && x > 0)
						{
							toInstantiate = floorTiles [5];
						}

					}
					if ( x > 8 && x < 15)
					{
						toInstantiate = floorTiles [3];
						if ( y == 8 && x == 12)
						{
							toInstantiate = floorTiles [8];
						}
						if (y == 6 && x == 13)
						{
							toInstantiate = floorTiles [0];
						}
						if (y == 7 && x > 8)
						{
							toInstantiate = floorTiles [8];
						}
					}
					
				}

				GameObject instance = Instantiate (toInstantiate, new Vector3 (x, y, 0f), Quaternion.identity) as GameObject;
				instance.transform.SetParent (boardHolder);
			}
		}

	}


	public void SetupScene ()
	{
		BoardSetup ();
		InitializeBluePath();
		InitializeRedPath();
	}
}
