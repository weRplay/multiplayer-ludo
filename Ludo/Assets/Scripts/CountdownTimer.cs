﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class CountdownTimer : MonoBehaviour {

	public  GameObject myTimeLeftText;
	public  GameObject opponentsTimeLeftText;
	private static float timeLeft = 10.0f;
	public static bool showTimer = false;
	private static Player playerScript;
	private static PlayerTwo playerTwoScript;
	public int CountdownNumber = 10;

	// Use this for initialization
	void Start () 
	{
	/*	myTimeLeftText = GameObject.Find("MyTime");
		opponentsTimeLeftText = GameObject.Find("OpponentsTime");*/
		playerScript = GameObject.Find("Player").GetComponent<Player>();
		playerTwoScript = GameObject.Find("Player2").GetComponent<PlayerTwo>();
		myTimeLeftText = GameObject.Find("MyTime");
	}
	
	// Update is called once per frame
	void Update () 
	{
		float dTime = Time.deltaTime;
		myTimeLeftText.GetComponent<Text>().text = "Time Left:" + CountdownNumber;
	//	Debug.Log("playersTurn: " + GameManager.playersTurn + " " + "showTimer" + showTimer);
	/*	if(GameManager.playersTurn == false && showTimer == true)
		{
			Debug.Log("timeLeft" + timeLeft);
			//Debug.Log("playersTurn: " + GameManager.playersTurn + " " + "showTimer" + showTimer);
			myTimeLeftText.SetActive(false);
			opponentsTimeLeftText.SetActive(true);
			timeLeft -= dTime;
			opponentsTimeLeftText.GetComponent<Text>().text = "Time Left:" + Mathf.Round(timeLeft);

			if(timeLeft < 0)
			{

				opponentsTimeLeftText.SetActive(false);
				myTimeLeftText.SetActive(false);
				StartCoroutine("AutomaticTurn");
				GameManager.playersTurn = true;
				timeLeft = 10.0f;
				showTimer = false;
			}
		}
		else*/ 
/*		if (GameManager.playersTurn == true && showTimer == true)
		{
			Debug.Log("timeLeft" + timeLeft);
			opponentsTimeLeftText.SetActive(false);
			myTimeLeftText.SetActive(true);
			timeLeft -= dTime;
			myTimeLeftText.GetComponent<Text>().text = "Time Left:" + Mathf.Round(timeLeft);
			
			if(timeLeft < 0)
			{
				opponentsTimeLeftText.SetActive(false);
				myTimeLeftText.SetActive(false);
				GameManager.playersTurn = false;
				timeLeft = 10.0f;
				showTimer = false;
				StartCoroutine(AutomaticTurn());
			}
		}*/
	}

	public void ShiftTimer()
	{
	/*	opponentsTimeLeftText.SetActive(false);
		myTimeLeftText.SetActive(false);*/
		GameManager.playersTurn = false;
		timeLeft = 10.0f;
	//	opponentsTimeLeftText.SetActive(false);
	//	myTimeLeftText.SetActive(false);
		showTimer = false;
	}

	private IEnumerator AutomaticTurn () 
	{	
		WWWForm form = new WWWForm();
		Debug.Log(PlayerPrefs.GetString("GameId"));
		form.AddField("gameId", PlayerPrefs.GetString("GameId"));
		if (PlayerPrefs.GetString("Player") == "Player1")
		{
			form.AddField("Player", PlayerPrefs.GetString("Player"));
		}
		else if (PlayerPrefs.GetString("Player") == "Player2")
		{
			form.AddField("Player", PlayerPrefs.GetString("Player"));
		}
		WWW w = new WWW ("http://www.werplay.com/devs/Ludo/GetAutomaticTurn.php", form);
		yield return w;
		if ( w.text == "Done")
		{
			//GameManager.playersTurn = true;
			StartCoroutine(playerScript.GetPlayer1Position());
			StartCoroutine(playerTwoScript.GetPlayer2Position());
		}
	}
}
