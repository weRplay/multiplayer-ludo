﻿using UnityEngine;
using System.Collections;

public class PlayerTwo : MovingObject {

	// Use this for initialization
	protected override void Start ()
	{
	//	StartCoroutine("GetPlayer2Position");
		base.Start ();

	}
	
	public void MoveThePlayer(int x, int y, int a, int b, int playerPosition, int diceNumber)
	{
		AttemptMove (x, y);
		StartCoroutine(SendPlayer2Position(a,b, playerPosition, diceNumber));
	}

	protected override void AttemptMove (int xDir, int yDir)
	{
		//Call the AttemptMove method of the base class, passing in the component T (in this case Wall) and x and y direction to move.
		base.AttemptMove (xDir, yDir);
		//Set the playersTurn boolean of GameManager to false now that players turn is over.
	//	GameManager.playersTurn = false;
		
	}

	public IEnumerator SendPlayer2Position (int a, int b, int playerPosition, int diceNumber) 
	{	
			WWWForm form = new WWWForm();
			Debug.Log(PlayerPrefs.GetString("GameId"));
			form.AddField("gameId", PlayerPrefs.GetString("GameId"));
			form.AddField("player1", "Sameer");
			form.AddField("player2", "Samad");
			form.AddField("xPosition", a);
			form.AddField("yPosition", b);
			form.AddField("diceNumber", diceNumber);
			form.AddField("ArrayPosition", playerPosition);
			WWW w = new WWW ("http://www.werplay.com/devs/Ludo/Player2Move.php",form);
			yield return w;
			if (w.error == null) 
			{
				Debug.Log(w.text);
			} 
			else 
			{
				Debug.Log( w.error );
			}
		
	}

	public void ChangeTransform(float x, float y)
	{
		Debug.Log("Changing player 2 transform");
		transform.position = new Vector3(x, y, 0f);
	}

	public IEnumerator GetPlayer2Position () 
	{
		WWWForm form = new WWWForm();
		form.AddField("gameId", PlayerPrefs.GetString("GameId"));
		WWW w = new WWW ("http://www.werplay.com/devs/Ludo/GetPlayer2Position.php", form);
		yield return w;
		string JsonString = w.text;
		string[] array = JsonString.Split('-');
		float x = float.Parse(array[0]);
		float y = float.Parse(array[1]);
		int p = int.Parse(array[2]);
		GameManager.playerPosition = p;
		PlayerPrefs.SetInt("PlayerPos", p);
		transform.position = new Vector3(x, y, 0f);
		Debug.Log("Player 2 Position got: " + array[0] + " - " + array [1]);

	}
}
