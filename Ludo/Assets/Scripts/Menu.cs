﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Menu : MonoBehaviour {

	public GameObject createUserButton;
	public GameObject userNameTextEdit;

	public InputField username;
	// Use this for initialization
	void Start () 
	{
//		PlayerPrefs.DeleteAll();
//		PlayerPrefs.SetString("Username","Sameer");
		if (PlayerPrefs.HasKey ("Username")) 
		{
			Debug.Log("Found User: " + PlayerPrefs.GetString("UniquePlayerId"));
			Application.LoadLevel ("ActiveUsers"); 
		} 
		else 
		{
			Debug.Log("NO UID");	
		}

	}

	public void CreateUser()
	{
	//	userNameTextEdit.
	//	string UN	= userNameTextEdit.GetComponent<Text>().text;
		Debug.Log(username.text);
		StartCoroutine(CreateUniquePlayer(username.text.ToString()));
	}


	public void PlayGame()
	{
		Application.LoadLevel ("MainScene"); 
	}

	public IEnumerator CreateUniquePlayer (string username) 
	{	
		WWWForm form = new WWWForm();
		form.AddField("Name", username);
		WWW w = new WWW ("http://www.werplay.com/devs/Ludo/CreateUser.php",form);
		yield return w;
		if (w.error == null) 
		{
			string newID = w.text;
			Debug.Log ("Creating Player" + newID);
			PlayerPrefs.SetString ("Username", username);
			Application.LoadLevel ("ActiveUsers"); 
		} 
		else 
		{
			Debug.Log( w.error );
		}	
	}


}
