﻿using UnityEngine;
using System.Collections;

public class GameMenu : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
	
		InvokeRepeating("CallSetActiveUsers", 1, 10f);
	}


	public void CallSetActiveUsers()
	{
		StartCoroutine("SetUserOnline");
	}

	public IEnumerator SetUserOnline () 
	{	WWWForm form = new WWWForm();
		string username = PlayerPrefs.GetString ("Username");
		form.AddField("Name", username);
		WWW w = new WWW ("http://www.werplay.com/devs/Ludo/SetUserOnline.php", form);
		yield return w;
		if (w.error == null) 
		{
		/*	string Players = w.text;
			Debug.Log ("Creating Player" + Players);*/
		} 
		else 
		{
			Debug.Log( w.error );
		}	
	}


	public IEnumerator GetActiveUsers () 
	{	WWWForm form = new WWWForm();
		string username = PlayerPrefs.GetString ("Username");
		form.AddField("Name", username);
		WWW w = new WWW ("http://www.werplay.com/devs/Ludo/GetActiveUsers.php", form);
		yield return w;
		if (w.error == null) 
		{
			string Players = w.text;
			Debug.Log ("Creating Player" + Players);
		} 
		else 
		{
			Debug.Log( w.error );
		}	
	}

	public void Refresh()
	{
		Application.LoadLevel ("ActiveUsers"); 
	}
}
